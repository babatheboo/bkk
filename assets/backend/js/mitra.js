var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    $("#bidangx").select2();
    jQuery("#filter_pencarian").hide();
    table = $('#data-mitra').DataTable({
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "mitra/getData",
            "type": "POST",
            "data": function ( data ) {
                data.bidang = $('#bidangx').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    jQuery("#bidangx").change(function(){
        table.ajax.reload(null,false); 
    });
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}

function hapus_mitra(wk,page,link,id){
    bootbox.confirm("Apakah anda yakin akan menghapus data "+wk+" ini ?",  
    function(result) {
        if(result){
        setTimeout(function () {
            jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
            $.ajax({
                url : $BASE_URL+page+"/"+link+"/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    if(data.status == true ) {
                         $.gritter.add({
                            title: "notifikasi hapus data",
                            text: 'data berhasil di hapus',
                            class_name: "gritter-light"
                        });
                        jQuery.unblockUI();
                        reload_table()
                    }else{
                        $.gritter.add({
                            title: "notifikasi hapus data",
                            text: 'data tidak berhasil di hapus terkait dengan data lain ',
                            class_name: "gritter-light"
                        });
                        jQuery.unblockUI();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error deleting data');
                }
            });
        }, 1000);
        }
    });
}

function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery('[name="bidangx"]').select2("val", "");
        jQuery("#filter_pencarian").hide();
        jQuery.unblockUI();
    }, 100);
}

function eksReady(){
    alert('oke');
}