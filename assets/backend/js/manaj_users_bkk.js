var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    table = $('#data-user').DataTable({ 
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "responsive": true, 
        "order": [],
        "pageLength": 20,
        "ajax": {
            "url": $BASE_URL+ "users_bkk/getData",
            "type": "POST"
        },
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});
function tambah_data(){
    save_method = 'add';
    $('#form')[0].reset();
    $("#nip").prop('disabled',false);
    $("#username").prop('disabled',false);
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data User');
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function edit_data(page,link,action,id){ 
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
    	    $("#nip").prop('disabled',true);
            $("#username").prop('disabled',true);
            $('[name="nama"]').val(data.nama);
            $('[name="nip"]').val(data.nip);
            $('[name="username"]').val(data.username);
            $('[name="mail"]').val(data.email);
            $('[name="tlp"]').val(data.tlp);
            $('[name="id"]').val(data.id);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Data User');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function save(link){
    $('#btnSave').text('Proses...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add";
    } else {
        url = $BASE_URL+link+"/proses_edit";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        }
    });
}
function hapus_data(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+link+"/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#modal_form').modal('hide');
                        reload_table();
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}
