var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(document).ready(function(){
    $("#boxstatus").hide();
    $("#tombol").hide();
    $("#divk").hide();
    $("#divm").hide();
    $("#divw").hide();
    $("#div-mitra").hide();
    $('[data-toggle="tooltip"]').tooltip();
    var tbl = '#data-kerja';
    var tbl2 = '#data-kuliah';
    var tbl3 = '#data-wira';
    var tbl4 = '#data-mitra';

    $("#jenis").change(function(){
        var nilai = $("#jenis").val();
        switch (nilai){
            case "0":
            $("#boxstatus").hide('slow');
            $("#tombol").hide('slow');
            $("#divk").hide('slow');
            $("#divm").hide('slow');
            $("#divw").hide('slow');
            $("#div-mitra").hide('slow');
            break;
            case "1":
            $("#boxstatus").show('slow');
            $("#tombol").show('slow');
            $("#divk").show('slow');
            $("#divm").hide('slow');
            $("#divw").hide('slow');
            $("#div-mitra").hide('slow');
            break;
            case "2":
            $("#boxstatus").hide('slow');
            $("#tombol").show('slow');
            $("#divk").hide('slow');
            $("#divm").hide('slow');
            $("#divw").hide('slow');
            $("#div-mitra").show('slow');
            break;
            default :
            $("#boxstatus").hide('slow');
            $("#tombol").hide('slow');
            $("#divk").hide('slow');
            $("#divm").hide('slow');
            $("#divw").hide('slow');
            $("#div-mitra").hide('slow');
            break;
        }
    });

    table = $('#data-kerja').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "pageLength": 10,
        "retrieve":true,
        "ajax": {
            "url": $BASE_URL+"lapbkk/getData/k",
            "type": "POST",
            "data": function ( data ) {
                data.status = $('#status').val();
                data.tahun = $('#tahun').val();
                data.jurusan = $('#jurusan').val();
            }
        },
        "order": [[ 4, 'asc' ]],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "displayLength": 25
    });

    table2 = $('#data-kuliah').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "pageLength": 10,
        "retrieve":true,
        "ajax": {
            "url": $BASE_URL+"lapbkk/getData/ku",
            "type": "POST",
            "data": function ( data ) {
                data.status = $('#status').val();
                data.tahun = $('#tahun').val();
                data.jurusan = $('#jurusan').val();
            }
        },
        "order": [[ 4, 'asc' ]],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "displayLength": 25
    });

    table3 = $('#data-wira').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "pageLength": 10,
        "retrieve":true,
        "ajax": {
            "url": $BASE_URL+"lapbkk/getData/w",
            "type": "POST",
            "data": function ( data ) {
                data.tahun = $('#tahun').val();
                data.jurusan = $('#jurusan').val();
            }
        },
        "order": [[ 4, 'asc' ]],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "displayLength": 25
    });

    table4 = $('#data-mitra').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "pageLength": 10,
        "retrieve":true,
        "ajax": {
            "url": $BASE_URL+"lapbkk/getData/mitra",
            "type": "POST",
            "data": function ( data ) {
                data.bidang = $('#bidang').val();
            }
        },
        "order": [[ 3, 'asc' ]],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf'],
        "displayLength": 25
    });

    $("#status").change(function(){
        var s = $("#status").val();
        if(s=='1')
        {
            $("#divk").show('slow');
            $("#divm").hide('slow');
            $("#divw").hide('slow');
        }
        else if(s=='2')
        {
            $("#divk").hide('slow');
            $("#divm").show('slow');
            $("#divw").hide('slow');
        }
        else
        {
            $("#divk").hide('slow');
            $("#divm").hide('slow');
            $("#divw").show('slow');
        }
    });

    $("#tahun").change(function(){
        var n = $("#status").val();
        if(n=='1')
        {
            jQuery.unblockUI();
            reload_table(tbl);
        }
        else if(n=='2')
        {
            jQuery.unblockUI();
            reload_table(tbl2);
        }
        else
        {
            jQuery.unblockUI();
            reload_table(tbl3);
        }
    });

    $("#jurusan").change(function(){
        var n = $("#status").val();
        if(n=='1')
        {
            jQuery.unblockUI();
            reload_table(tbl);
        }
        else if(n=='2')
        {
            jQuery.unblockUI();
            reload_table(tbl2);
        }
        else
        {
            jQuery.unblockUI();
            reload_table(tbl3);
        }
    });

    $("#bidang").change(function(){
        var n = $("#status").val();
        if(n=='1')
        {
            jQuery.unblockUI();
            reload_table(tbl);
        }
        else if(n=='2')
        {
            jQuery.unblockUI();
            reload_table(tbl2);
        }
        else
        {
            jQuery.unblockUI();
            reload_table(tbl3);
        }
    });
    function info(){
        $('#modal_info').modal('show');
    }
    function reload_table(tbl){
        var oTable = $(tbl).DataTable();
oTable.ajax.reload(null,false); //reload datatable ajax
}
});

function exp(){
    var stt = $("#status").val();
    $.ajax({
        url : $BASE_URL+'lapbkk/expLap',
        method: "POST",
        data: {stt:stt},
        success: function(data){
            var data = $.parseJSON(JSON.stringify(data));
            if (data.response=='kerja') {
                alert('kerja');
            }else if (data.response=='kulyah') {
                alert('kuly');
            }else if (data.response=='wiras') {
                alert('wiraus')
            }else{
                alert('ups');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Terjadi kesalahan. Ulangi proses');
        }
    });
}

function bukaNaker(){
    $("#modal_naker").modal('show');
}

// function exportNaker(){
//     var bulan = $("#bulan").val();
//     var tahun = $("#tahunNaker").val();
//     if (bulan=='') {
//         $.notify({icon:'fas fa-info',message:"Pilih bulan ❌"},{type:"danger",offset:{x:3,y:555}});
//     }else if (tahun=='') {
//         $.notify({icon:'fas fa-info',message:"Pilih tahun ❌"},{type:"danger",offset:{x:3,y:555}});
//     }else{
//         $.ajax({
//                 url: $BASE_URL + "lapbkk/dataDisnaker/",
//                 method: "POST",
//                 data: { bulan: bulan, tahun: tahun },
//                 cache: false,
//                 beforeSend: function(){
//                     jQuery.blockUI({
//                 css: {
//                     border: "none",
//                     padding: "15px",
//                     backgroundColor: "#000",
//                     "-webkit-border-radius": "10px",
//                     "-moz-border-radius": "10px",
//                     opacity: 2,
//                     color: "#fff"
//                 },
//                 message: "Mohon menunggu ... "
//             });
//                 },
//                 success: function(data) {
//                     window.location.href = $BASE_URL + "lapbkk/dataDisnaker/";
//                 }
//             });
//     }
// }