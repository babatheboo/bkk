var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    jQuery("#filter_pencarian").hide();
    table = $('#data-mitra').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "affiliate/getIndustri",
            "type": "POST",
            "data": function ( data ) {
                data.bidang = $('#bidangx').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    jQuery("#bidangx").change(function(){
        table.ajax.reload(null,false); 
    });
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery('[name="bidangx"]').select2("val", "");
        jQuery("#filter_pencarian").hide();
        jQuery.unblockUI();
    }, 100);
}
