var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    jQuery("#filter_pencarian").hide();
    table = $('#data-riwayat').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "caker/getData_riwayat",
            "type": "POST",
            "data": function ( data ) {
                // data.tahun_keluar = $('#tahun_keluar').val();
                // data.jns_kel = $('#jns_kel').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table.ajax.reload(null,false); 
    });
    // jQuery("#tahun_keluar").change(function(){
    //     table.ajax.reload(null,false); 
    // });
    // jQuery("#jns_kel").change(function(){
    //     table.ajax.reload(null,false); 
    // });
    $('#btn-reset').click(function(){
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false); 
    });
});
function filter(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        // jQuery('[name="tahun_keluar"]').select2("val", "");
        // jQuery('[name="jns_kel"]').select2("val", "");
        jQuery("#filter_pencarian").hide("slow");
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
