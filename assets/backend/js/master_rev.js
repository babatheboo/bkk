var save_method;
var table;
$(function() {
    table = $('#data-master').DataTable({ 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
        "processing": true, 
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": $BASE_URL+ "master_revitalisasi/getData",
            "type": "POST",
            "data": function ( data ) {

            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table.ajax.reload(null,false); 
    });
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function tambah_data(){
    // save_method = 'add';
    // $('#forminp')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Revitalisasi');
}

$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:$BASE_URL+"master_revitalisasi/cari",
   method:"POST",
   data:{query:query},
   success:function(data){
    $('#result').html(data);
   }
  })
 }

 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   $("#result").show();
   load_data(search);
  }
  else
  {
  $("#result").hide();
   load_data();
  }
})
 });