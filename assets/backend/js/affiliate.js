var save_method;
var table;
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(function() {
    table = $('#data-sekolah').DataTable({ 
        autoWidth: false,
        columnDefs: [{
                width: '5',
                targets: 0,
                className: "text-center",
            },{            
                width: '10%',           
                targets: 1,
                className: "text-center",
            },{            
                width: '50%',           
                targets: 2
            },{
                width: '30%',
                targets: 3
            },{
                width: '5%',
                targets: 4,
                className: "text-center"
            }                   
        ],
        "processing": true,
        "serverSide": true,
        "responsive": true, 
        "order": [],
        "pageLength": 10,
        "ajax": {
            "url": $BASE_URL+ "affiliate/getSekolah",
            "type": "POST"
        },
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('#data-sekolah tbody').on('change', 'input[type="checkbox"]', function(){
        if(!this.checked){
            var el = $('#select-all').get(0);
            if(el && el.checked && ('indeterminate' in el)){
                el.indeterminate = true;
            }
        }
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');

});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
