var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
var table2;
var table3;
$(function() {
    jQuery("#filter_pencarian").hide();
    table = $('#data-bkk-d').DataTable({
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lap_bkk_terdaftar/getData",
            "type": "POST",
            "data": function ( data ) {
                data.wilayah = $('#wilayahx').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    jQuery("#wilayahx").change(function(){
        table.ajax.reload(null,false); 
    });
});

$(function() {
    table2 = $('#data-bkk-p').DataTable({
        "search": {
            "caseInsensitive": false
        },
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lap_bkk_terdaftar/getProv",
            "type": "POST",
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
});

$(function() {
    table3 = $('#data-bkk-p2').DataTable({
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lap_bkk_terdaftar/getProv2",
            "type": "POST",
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery('[name="wilayahx"]').select2("val", "");
        jQuery("#filter_pencarian").hide();
        jQuery.unblockUI();
    }, 100);
}

function img(){
    var myImg = document.getElementById('myImg');
    $('#modal_img').modal('show');
}