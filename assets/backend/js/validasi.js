var host = window.location.host;
$BASE_URL = "http://" + host + "/";
var save_method;
var table;
$(function() {
    table = $("#data-validasi").DataTable({
        autoWidth: true,
        processing: true,
        serverSide: true,
        responsive: true,
        order: [],
        pageLength: 20,
        ajax: {
            url: $BASE_URL + "dashboard/getValid",
            type: "POST"
        },
        // order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4
    });
    $(".dataTables_filter input[type=search]").attr(
        "placeholder",
        "Filter Pencarian"
    );
    $("input").change(function() {
        $(this)
            .parent()
            .parent()
            .removeClass("has-error");
        $(this)
            .next()
            .empty();
    });
});

function convertDateDBtoIndo(string) {
    bulanIndo = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September' , 'Oktober', 'November', 'Desember'];
 
    tanggal = string.split("-")[2];
    bulan = string.split("-")[1];
    tahun = string.split("-")[0];
 
    return tanggal + "-" + bulanIndo[Math.abs(bulan)] + "-" + tahun;
}

function validate(scan, izin, sk, id, nama, notif, exp) {
    $("#valid-modal").modal();
    $("#valid-modal").block({
        message: '<div class="la la-spinner icon-spin font-medium-2 blue"></div><span class="semibold"> Pemeriksaan data...</span>',
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: 0.8,
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "transparent"
        }
    });
    $("#fotoscan").attr("src", "assets/foto/scan/" + scan);
    $("#id").val(id);
    $("#nama").val(nama);
    document.getElementById("izin").innerHTML = izin;
    document.getElementById("sk").innerHTML = sk;
    if (exp=='x') {
        document.getElementById("exp").innerHTML = 'Belum diisi';
    }else{
        document.getElementById("exp").innerHTML = convertDateDBtoIndo(exp);
    }
    document.getElementById("notif").innerHTML = notif;
    $("#valid-modal").unblock();
}

function validated() {
    var name = $("#nama").val();
    bootbox.confirm("Validasi " + name + " ?", function(result) {
        if (result) {
            var idna = $("#id").val();
            var resp = "";
            $("#valid-modal").modal("hide");
            jQuery.blockUI({
                css: {
                    border: "none",
                    padding: "15px",
                    backgroundColor: "#000",
                    "-webkit-border-radius": "10px",
                    "-moz-border-radius": "10px",
                    opacity: 2,
                    color: "#fff"
                },
                message: "Mohon menunggu ... "
            });
            $.ajax({
                url: $BASE_URL + "dashboard/validasiData/" + idna,
                method: "POST",
                data: { idna: idna },
                cache: false,
                success: function(data) {
                    resp = $.parseJSON(data);
                    if (resp == "ok") {
                        jQuery.unblockUI();
                        $.notify({ icon: "fa fa-check", message: "Validasi berhasil" }, { type: "success", offset: { x: 3, y: 2 } });
                        reload_table();
                    } else {
                        jQuery.unblockUI();
                        $.notify({ icon: "fa fa-info", message: "Terjadi kesalahan" }, { type: "danger", offset: { x: 3, y: 2 } });
                        reload_table();
                    }
                }
            });
        } else {
            $("#valid-modal").modal("hide");
        }
    });
}

function catatan() {
    var note = $("#info").val();
    var idna = $("#id").val();
    if (note == '') {
        jQuery("#info").effect('shake', '1500').attr('placeholder', 'Catatan tidak boleh kosong');
    } else {
        $.ajax({
            url: $BASE_URL + "dashboard/catatan/" + idna,
            method: "POST",
            data: {
                idna: idna,
                note: note
            },
            cache: false,
            success: function(data) {
                resp = $.parseJSON(data);
                if (resp == "ok") {
                    jQuery.unblockUI();
                    $("#valid-modal").modal('hide');
                    $.notify({ icon: "fa fa-check", message: "Catatan terkirim" }, { type: "success", offset: { x: 3, y: 2 } });
                    $("#info").val('');
                    reload_table();
                } else {
                    jQuery.unblockUI();
                    $("#valid-modal").modal('hide');
                    $.notify({ icon: "fa fa-info", message: "Catatan gagal terkirim" }, { type: "danger", offset: { x: 3, y: 2 } });
                    reload_table();
                }
            }
        });
    }
}

function izinE() {
    var id = $("#id").val();
    $.ajax({
        url: 'dashboard/cekIzin/' + id,
        'type': 'post',
        success: function(data) {
            var data = $.parseJSON(data);
            $('#izinEdit').val(data.no_izin);
            $("#izin-modal").modal('show');
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function sipIzin() {
    var id = $("#id").val();
    var wkwk = $("#izinEdit").val();
    $.ajax({
        url: 'dashboard/saveIzin/' + id,
        method: "POST",
        data: { wkwk: wkwk },
        cache: false,
        success: function(data) {
            var data = $.parseJSON(data);
            if (data.response == 'true') {
                document.getElementById("izin").innerHTML = wkwk;
                $("#izin-modal").modal('hide');
                reload_table();
            } else {
                alert("Terjadi Kesalahan, ulangi proses");
            }
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function skE() {
    var id = $("#id").val();
    $.ajax({
        url: 'dashboard/cekSK/' + id,
        'type': 'post',
        success: function(data) {
            var data = $.parseJSON(data);
            $('#skEdit').val(data.no_sk);
            $("#sk-modal").modal('show');
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function sipSK() {
    var id = $("#id").val();
    var hehe = $("#skEdit").val();
    $.ajax({
        url: 'dashboard/saveSK/' + id,
        method: "POST",
        data: { hehe: hehe },
        cache: false,
        success: function(data) {
            var data = $.parseJSON(data);
            if (data.response == 'true') {
                document.getElementById("sk").innerHTML = hehe;
                $("#sk-modal").modal('hide');
                reload_table();
            } else {
                alert("Terjadi Kesalahan, ulangi proses");
            }
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function expE() {
    var id = $("#id").val();
    $.ajax({
        url: 'dashboard/cekExp/' + id,
        'type': 'post',
        success: function(data) {
            var data = $.parseJSON(data);
            $('#expEdit').val(data.kadaluarsa);
            $("#exp-modal").modal('show');
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function sipExp() {
    var id = $("#id").val();
    var haha = $("#expEdit").val();
    $.ajax({
        url: 'dashboard/saveExp/' + id,
        method: "POST",
        data: { haha: haha },
        cache: false,
        success: function(data) {
            var data = $.parseJSON(data);
            if (data.response == 'true') {
                document.getElementById("exp").innerHTML = haha;
                $("#exp-modal").modal('hide');
                reload_table();
            } else {
                alert("Terjadi Kesalahan, ulangi proses");
            }
        },
        error: function() {
            alert("TERJADI KESALAHAN");
        }
    });
}

function reload_table() {
    jQuery.blockUI({
        css: {
            border: "none",
            padding: "15px",
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            opacity: 2,
            color: "#fff"
        },
        message: "Mohon menunggu ... "
    });
    setTimeout(function() {
        table.ajax.reload(null, false);
        jQuery.unblockUI();
    }, 100);
}