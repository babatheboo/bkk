var save_method;
var table;
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(function() {
    $('#tgl_buka').datepicker();
    $('#tgl_tutup').datepicker();
    $('#data-news').hide();
    TableManageResponsive.init();
    var count = 0;
        $("#add_btn").click(function(){
            // count += 1;
            // if(count == 2){
            //     return false;
            // }
            // else{
                $('#data-news').show('slow');
                $('#container').append(
                    '<tr class="records">'
                     + '<td><center><div id="'+count+'">' + count + "." + '</center></div></td>'
                     + '<td><select name="jns_kel_'+count+'" name="jns_kel_'+ count +'" data-live-search="true" data-style="btn-white" class="default-select2 form-control"><option value="" selected="selected">Pilih Jenis Kelamin</option><option value="0">SEMUA<option value="1">Laki-Laki</option><option value="2">Perempuan</option></select></td>'
                     + '<td><input class="form-control" onkeyup="validAngka(this)" style="text-align: right;" id="jml_' + count + '" name="jml_' + count + '" type="text"></td>'
                     + '<td><input maxlength="3" class="form-control" onkeyup="validAngka(this)" style="text-align: right;" id="tinggi_' + count + '" name="tinggi_' + count + '" type="text"></td>'
                     + '<td><input maxlength="3" class="form-control" onkeyup="validAngka(this)" style="text-align: right;" id="berat_' + count + '" name="berat_' + count + '" type="text"></td>'
                     + '<td><input maxlength="2" class="form-control" onkeyup="validAngka(this)" style="text-align: right;" id="umur_' + count + '" name="umur_' + count + '" type="text"></td>'
                     + '<td><center><a class="btn btn-xs m-r-5 btn-danger remove_item" href="JavaScript:void(0)"><i class="fas fa-trash"></i></center></a>'
                     + '<input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
                );
            // }
            });
            $(".remove_item").live('click', function (ev) {
            if (ev.type == 'click') {
            $(this).parents(".records").hide('slow');
                    $(this).parents(".records").remove();
        }
    });
    table = $('#data-sekolah').DataTable({ 
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ajax": {
            "url": $BASE_URL+ "loker/getSakola",
            "type": "POST",
            "data": function ( data ) {
                data.provinsi = $('#provinsi_filter').val();
                data.kota = $('#kota_filter').val();
            }
        },
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'width':'1%',
            'className': 'text-center',
            'render': function (data, type, row){
                return '<input type="checkbox" name="tujuan_sekolah[]" value="' + row[0] + '">';
            }
        },{
            'targets': 1,
            'searchable': true,
            'orderable': true
        },{
            'targets': 2,
            'searchable': true,
            'orderable': true
        },{
            'targets': 3,
            'searchable': true,
            'orderable': true
        }],
      'order': [[1, 'asc']]
    });
    $('#select-all').on('click', function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    $('#data-sekolah tbody').on('change', 'input[type="checkbox"]', function(){
        if(!this.checked){
            var el = $('#select-all').get(0);
            if(el && el.checked && ('indeterminate' in el)){
                el.indeterminate = true;
            }
        }
    });
    jQuery("#provinsi_filter").change(function(){
        table.ajax.reload(null,false); 
    });
    jQuery("#kota_filter").change(function(){
        table.ajax.reload(null,false); 
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });

});
$(document).ready(function() {
    jQuery("#filter_pencarian").hide();
    jQuery("#filter_sekolah").hide();
    jQuery("#tombol_").hide();
    $("#provinsi_filter").change(function(){
        var deptid = $(this).val();
        document.getElementById("select-all").checked = false;
        $.ajax({
            url: $BASE_URL+'loker/getKabkot_/'+deptid,
            type: 'post',
            data: {depart:deptid},
            dataType: 'json',
            success:function(response){
                var len = response.length;
                jQuery('[name="kota_filter"]').select2("val", "");
                for( var i = 0; i<len; i++){
                    var id = response[i]['id'];
                    var name = response[i]['kota'];
                    $("#kota_filter").append("<option value='"+id+"'>"+name+"</option>");
                }
            }
        });
    });
    $("#tujuan").change(function(){
        var x = $(this).val();
        if(x=='0' || x=='9' || x=='99'){
            jQuery("#filter_sekolah").hide();
        }else{
            jQuery("#filter_sekolah").show("slow");
        }
        jQuery("#tombol_").show("slow");
    });
});
function validAngka(a){
    if(!/^[0-9.]+$/.test(a.value)){
        a.value = a.value.substring(0,a.value.length-1000);
    }
}
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="provinsi_filter"]').select2("val", "");
        jQuery('[name="kota_filter"]').select2("val", "");
        jQuery("#filter_pencarian").hide("slow");
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}