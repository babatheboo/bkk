function buatlap(){
    var jenis = $("#jenis").val();
    var tahun = $("#tahun").val();

    if (jenis!='0') {
        $("#tblS").hide('slow');
        $("#tbl").show("slow");
        $("#data-hasil").dataTable().fnDestroy();
        $('#data-hasil').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            dom: 'lBfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            "processing": true, 
            "serverSide": false,
            "order": [],
            "ajax": {
                "url": $BASE_URL+ "lulusan/getHasil/" + jenis + '/' + tahun,
                "type": "POST"
            },
            "columnDefs": [{ 
                "targets": [],
                "orderable": false, 
            },
            ],
        });
    }else{
        var table;
        $("#tblS").show('slow');
        $("#tbl").hide("slow");
$(function() {
    $(".datepicker").datepicker({
        todayHighlight: !0
    });
    jQuery("#filter_pencarian").hide();
    jQuery("#bekerja_").hide();
    jQuery("#kuliah_").hide();
    jQuery("#wira_").hide();
    jQuery("#tombol").hide();
    jQuery("#status_siswax").change(function(){
        var status_siswax = jQuery("#status_siswax").val();
        if(status_siswax=='1'){
            jQuery("#bekerja_").show('slow');
            jQuery("#tombol").show('slow');
            jQuery("#kuliah_").hide('slow');
            jQuery("#wira_").hide('slow');
        }else if(status_siswax=='2'){
            jQuery("#bekerja_").hide('slow');
            jQuery("#kuliah_").show('slow');
            jQuery("#tombol").show('slow');
            jQuery("#wira_").hide('slow');
        }else if(status_siswax=='3'){
            jQuery("#bekerja_").hide('slow');
            jQuery("#kuliah_").hide('slow');
            jQuery("#wira_").show('slow');
            jQuery("#tombol").show('slow');
        }else{
            jQuery("#bekerja_").hide('slow');
            jQuery("#kuliah_").hide('slow');
            jQuery("#wira_").hide('slow');
            jQuery("#tombol").hide('slow');
        }
    });
            $("#data-lulusan").dataTable().fnDestroy();
    table = $('#data-lulusan').DataTable({
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lulusan/getData",
            "type": "POST",
            "data": function ( data ) {
                data.tahun_keluar = $('#tahun_keluar').val();
                data.jurusan = $('#jurusan').val();
                data.status_siswa = $('#status_siswa').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table.ajax.reload(null,false); 
    });
    jQuery("#tahun_keluar").change(function(){
        table.ajax.reload(null,false); 
    });
    jQuery("#jurusan").change(function(){
        table.ajax.reload(null,false); 
    });
    jQuery("#status_siswa").change(function(){
        table.ajax.reload(null,false); 
    });
    jQuery("#filter_pencarian_kul").hide();
    // table_kul = $('#data-lulusan-kul').DataTable({ 
    //     "processing": true, 
    //     "serverSide": true,
    //     "order": [], 
    //     "ajax": {
    //         "url": $BASE_URL+ "lulusan/getDataKul",
    //         "type": "POST",
    //         "data": function ( data ) {
    //             data.tahun_keluar = $('#tahun_keluar_kul').val();
    //             data.jns_kel = $('#jns_kel_kul').val();
    //         }
    //     },
    //     "columnDefs": [{ 
    //         "targets": [ 0 ],
    //         "orderable": false, 
    //     },
    //     ],
    // });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table_kul.ajax.reload(null,false); 
    });
    jQuery("#tahun_keluar_kul").change(function(){
        table_kul.ajax.reload(null,false); 
    });
    jQuery("#jurusan_kul").change(function(){
        table_kul.ajax.reload(null,false); 
    });
    $('#btn-reset').click(function(){
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false); 
    });
    jQuery("#filter_pencarian_wira").hide();
    // table_wira = $('#data-lulusan-wira').DataTable({ 
    //     "processing": true, 
    //     "serverSide": true,
    //     "order": [], 
    //     "ajax": {
    //         "url": $BASE_URL+ "lulusan/getDataWira",
    //         "type": "POST",
    //         "data": function ( data ) {
    //             data.tahun_keluar = $('#tahun_keluar_wira').val();
    //             data.jns_kel = $('#jns_kel_wira').val();
    //         }
    //     },
    //     "columnDefs": [{ 
    //         "targets": [ 0 ],
    //         "orderable": false, 
    //     },
    //     ],
    // });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table_wira.ajax.reload(null,false); 
    });
    jQuery("#tahun_keluar_wira").change(function(){
        table_wira.ajax.reload(null,false); 
    });
    jQuery("#jurusan_wira").change(function(){
        table_wira.ajax.reload(null,false); 
    });
});
    }
    

    // if (jenis == 0) {
    //     $("#tbl").hide('slow');
    //     $("#tblS").show("slow");
    //     $("#data-semua").dataTable().fnDestroy();
    //     $('#data-semua').DataTable({
    //         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
    //         dom: 'lBfrtip',
    //         buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
    //         "processing": true, 
    //         "serverSide": false,
    //         "order": [],
    //         "ajax": {
    //             "url": $BASE_URL+ "lap_keterserapan_lulusan/getSemua/" + jenis + '/' + tahun,
    //             "type": "POST"
    //         },
    //         "columnDefs": [{ 
    //             "targets": [],
    //             "orderable": false, 
    //         },
    //         ],
    //     });
    // }else{
        
    // }
}