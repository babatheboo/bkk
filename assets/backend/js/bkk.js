var save_method;
var table;
$(function() {
    table = $('#data-bkk').DataTable({
        dom: 'lBfrtip',
        buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lap_industri/getMitra",
            "type": "POST",
            "data": function ( data ) {

            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table.ajax.reload(null,false); 
    });
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
