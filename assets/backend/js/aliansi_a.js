var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var table_;
$(function() {
    jQuery("#filter_pencarian_").hide();
    table_ = $('#data-affiliate-a').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "affiliate/getDataTunggu",
            "type": "POST",
            "data": function ( data ) {
                // data.provinsi = $('#provinsi_filter').val();
                // data.kota = $('#kota_filter').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    
});

function reload_table_(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table_.ajax.reload(null,false);
        // jQuery('[name="provinsi_filter"]').select2("val", "");
        // jQuery('[name="kota_filter"]').select2("val", "");
        // jQuery("#filter_pencarian_").hide("slow");
        jQuery.unblockUI();
    }, 100);
}
