var save_method;
var table;
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(function() {
    $('#tgl_buka').datepicker();
    $('#tgl_tutup').datepicker();
    table = $('#data-pelamar').DataTable({ 
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ajax": {
            "url": $BASE_URL+ "jadwal_test/getPelamar",
            "type": "POST",
            "data": function ( data ) {
                data.kota = $('#sekolah_filter').val();
            }
        },
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'width':'1%',
            'className': 'text-center',
            'render': function (data, type, row){
                return '<input type="checkbox" name="dat_siswa[]" value="' + row[0] + '">';
            }
        }],
      'order': [[1, 'asc']]
    });
    $('#select-all').on('click', function(){
        var rows = table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
    $('#data-sekolah tbody').on('change', 'input[type="checkbox"]', function(){
        if(!this.checked){
            var el = $('#select-all').get(0);
            if(el && el.checked && ('indeterminate' in el)){
                el.indeterminate = true;
            }
        }
    });
    jQuery("#sekolah_filter").change(function(){
        table.ajax.reload(null,false); 
    });
    jQuery("#filter_pencarian").hide();
    jQuery("#filter_sekolah").hide();
    jQuery("#tombol_").show();
    $("#provinsi").change(function(){
        var deptid = $(this).val();
        $.ajax({
            url: $BASE_URL+'jadwal_test/getKabkot/'+deptid,
            type: 'post',
            data: {depart:deptid},
            dataType: 'json',
            success:function(response){
                var len = response.length;
                $("#kabupaten").empty();
                for( var i = 0; i<len; i++){
                    var id = response[i]['id'];
                    var name = response[i]['kota'];
                    $("#kabupaten").append("<option value='"+id+"'>"+name+"</option>");
                }
            }
        });
    });
    $("#provinsi_filter").change(function(){
        var deptid = $(this).val();
        document.getElementById("select-all").checked = false;
        $.ajax({
            url: $BASE_URL+'jadwal_test/getKabkot_/'+deptid,
            type: 'post',
            data: {depart:deptid},
            dataType: 'json',
            success:function(response){
                var len = response.length;
                jQuery('[name="kota_filter"]').select2("val", "");
                for( var i = 0; i<len; i++){
                    var id = response[i]['id'];
                    var name = response[i]['kota'];
                    $("#kota_filter").append("<option value='"+id+"'>"+name+"</option>");
                }
            }
        });
    });
    jQuery("#address").keyup(function(){
        var address = jQuery("#address").val();
        if(jQuery("#provinsi").val()==""){
            $.gritter.add({title:"Informasi !",text: " Pilih Provinsi Terlebih dahulu."});
        }else if(jQuery("#kabupaten").val()==""){
            $.gritter.add({title:"Informasi !",text: " Pilih Kabupaten / Kota Terlebih dahulu."});
        }else{
            if(address != ""){
                var address = document.getElementById("address").value;
                var propinsi = jQuery("#propinsi option:selected").text();
                var kabupaten = jQuery("#kabupaten option:selected").text();
                // geocoder.geocode({ 'address': address + " " + propinsi + " " +kabupaten}, function(results, status){
                //     if(status == google.maps.GeocoderStatus.OK){
                //         map.setCenter(results[0].geometry.location);
                //         if(marker != undefined)
                //             marker.setPosition(results[0].geometry.location);
                //         else
                //             marker = new google.maps.Marker({
                //                 map: map,
                //                 draggable: true,
                //                 position: results[0].geometry.location
                //             });
                //         updateMarkerPosition(results[0].geometry.location);
                //         geocodePosition(results[0].geometry.location);
                //         aksi();
                //     }else{
                //         // alert("Geocode tidak berhasil dengan alasan karena: " + status);
                //     }
                // });
                jQuery("#tombol_").show('slow');
                // jQuery("#carialamat").show('slow');
            }else{
                jQuery("#tombol_").hide('slow');
            }
        }
    });
});
function simpan_jadwal(link){
        if(jQuery("#judul").val()==""){
                $.gritter.add({title:"Informasi !",text: " Judul Jadwal Tes Harus di isi."});
                jQuery('#judul').focus();
        }else if(jQuery("#provinsi").val()==""){
                $.gritter.add({title:"Informasi !",text: " Provinsi Harus di isi."});
                jQuery('#provinsi').focus();
        }else if(jQuery("#kabupaten").val()==""){
                $.gritter.add({title:"Informasi !",text: " Kabupaten / Kota Harus di isi."});
                jQuery('#kabupaten').focus();
        }else if(jQuery("#address").val()==""){
                $.gritter.add({title:"Informasi !",text: " Alamat Lokasi Tes Harus di isi."});
                jQuery('#address').focus();
        }else{
            var url;
            url = $BASE_URL+link+"/buat_jadwal";
            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data){
                    if(data.status){
                        $.gritter.add({title:"Informasi !",text: " Jadwal Tes berhasil di simpan."});
                        window.location.href=$BASE_URL+"jadwal_test";
                    }else{
                        for (var i = 0; i < data.inputerror.length; i++) {
                            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                        }
                    }
                }
            });
        }
}
$(document).ready(function() {
    jQuery("#filter_pencarian").hide();
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="sekolah_filter"]').select2("val", "");
        jQuery("#filter_pencarian").hide("slow");
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}