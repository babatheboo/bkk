var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    table = $('#data-loker').DataTable({ 
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "responsive": true, 
        "order": [],
        "pageLength": 20,
        "ajax": {
            "url": $BASE_URL+ "loker/getData",
            "type": "POST"
        },
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
