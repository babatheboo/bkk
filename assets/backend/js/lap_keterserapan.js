function buatlap(){
    var jenis = $("#jenis").val();
    var tahun = $("#tahun").val();
    if (jenis == 0) {
        $("#tbl").hide('slow');
        $("#tblS").show("slow");
        $("#data-semua").dataTable().fnDestroy();
        $('#data-semua').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            dom: 'lBfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            "processing": true, 
            "serverSide": false,
            "order": [],
            "ajax": {
                "url": $BASE_URL+ "lap_keterserapan_lulusan/getSemua/" + jenis + '/' + tahun,
                "type": "POST"
            },
            "columnDefs": [{ 
                "targets": [],
                "orderable": false, 
            },
            ],
        });
    }else{
        $("#tblS").hide('slow');
        $("#tbl").show("slow");
        $("#data-hasil").dataTable().fnDestroy();
        $('#data-hasil').DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            dom: 'lBfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            "processing": true, 
            "serverSide": false,
            "order": [],
            "ajax": {
                "url": $BASE_URL+ "lap_keterserapan_lulusan/getHasil/" + jenis + '/' + tahun,
                "type": "POST"
            },
            "columnDefs": [{ 
                "targets": [],
                "orderable": false, 
            },
            ],
        });
    }
}

$('#selnm').on("select2:select", function(e) { 
 var skl = $("#selnm").val();
 $.ajax({
    method: "POST",
    data : {skl:skl},
    url: $BASE_URL+"lap_keterserapan_lulusan/cekS",
    success: function(data){
      $("#boxl").show();
      $("#listpenerima").html(data);
  }
});
});