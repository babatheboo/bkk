var host = window.location.host;
$BASE_URL = 'http://' + host + '/';
var current_page = 1;
function postKomen(){
  var idkat = $("#idkat").val();
  var idpos = $("#idpost").val();
  var ket = CKEDITOR.instances.komentar.getData();
  var resp = "";
  if ( ket ==  "") {
    $.notify({icon:'fa fa-info',message:"Isi komentar Anda"},{type:"danger",offset:{x:3,y:66}});
  }else{
    $.ajax({
     url: $BASE_URL+'forum/postKomen',
     method:"POST",
     data: {idkat:idkat,idpos:idpos,ket:ket},
     cache: false,
     success:function(data)
     {
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Mengirim komentar"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, komentar anda gagal terkirim"},{type:"danger",offset:{x:3,y:66}});
      }
    }
  });
  }
}

function hpsKomen(id){
  var resp = "";
  bootbox.confirm("Yakin Akan Menghapus Komentar Berikut ?", function(result) {
    if (result) {
      setTimeout(function () {
        jQuery.blockUI({
          css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
          },
          message : 'Mohon menunggu ... '
        });
        $.ajax({
          url : $BASE_URL+"forum/hpsKomen/"+id,
          type: "POST",
          success: function(data){
            resp = $.parseJSON(data);
            if (resp=="ok") {
              jQuery.unblockUI();
              $.notify({icon:'fa fa-check',message:"Menghapus komentar"},{type:"success",offset:{x:3,y:66}});
              location.reload();
            }else{  
              $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, komentar gagal dihapus"},{type:"danger",offset:{x:3,y:66}});
            }
          }
        });
      }, 100);
    }
  });
}

function cekKomen(id){
  $.ajax({
    url : $BASE_URL+'forum/cekKomen/'+id,
    'type':'post',
    success:function(data){
      var data    =   $.parseJSON(data);
      CKEDITOR.instances.komenE.setData(data.isi);
      $("#modalEdit").modal('show');
    },
    error: function() {
      bootbox.alert("TERJADI KESALAHAN");
    }
  });
}

function edtKomen(id){
  var isi = CKEDITOR.instances.komenE.getData();
  var idkat = $("#edtPost").val();
  setTimeout(function () {
    jQuery.blockUI({
      css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: 2, 
        color: '#fff' 
      },
      message : 'Mohon menunggu ... '
    });
    $.ajax({
      url : $BASE_URL+"forum/edtKomen/"+id,
      data: {isi:isi, idkat:idkat},
      type: "POST",
      success: function(data){
        resp = $.parseJSON(data);
        if (resp=="ok") {
          jQuery.unblockUI();
          $.notify({icon:'fa fa-check',message:"Mengedit komentar"},{type:"success",offset:{x:3,y:66}});
          $("#modalEdit").modal('hide');
          location.reload();
        }else{  
          $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, Pengeditan gagal"},{type:"danger",offset:{x:3,y:66}});
        }
      }
    });
  }, 100);
}

function openRep(){
  $('#reply').show('slow');
  CKEDITOR.instances.balas.setData();
}
function closeRep(){
  $('#reply').hide('slow');
  CKEDITOR.instances.balas.setData();
}

function postRep(){
  var kat = $('#repKat').val();
  var com = $('#repCom').val();
  var balasan = CKEDITOR.instances.balas.getData();
  if (balasan == "") {
    $.notify({icon:'fa fa-info',message:"Isi balasan"},{type:"danger",offset:{x:3,y:66}});
  }else{
    $.ajax({
      url : $BASE_URL+"forum/postRep/",
      data: {kat:kat, com:com, balasan:balasan},
      type: "POST",
      success: function(data){
        resp = $.parseJSON(data);
        if (resp=="ok") {
          jQuery.unblockUI();
          $.notify({icon:'fa fa-check',message:"Mengirim balasan"},{type:"success",offset:{x:3,y:66}});
          $("#reply").hide('slow');
          location.reload();
        }else{  
          $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, gagal membalas"},{type:"danger",offset:{x:3,y:66}});
        }
      }
    });
  }
}