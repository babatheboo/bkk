var save_method;
var table;
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(function() {
    table = $('#data-syarat').DataTable({ 
        autoWidth: false,
        columnDefs: [{
                width: '5',
                targets: 0,
                className: "text-center",
            },{            
                width: '25%',           
                targets: 1
            },{            
                width: '25%',           
                targets: 2,
                className: "text-center",
            },{
                width: '25%',
                targets: 3,
                className: "text-center"
            },{
                width: '25%',
                targets: 4,
                className: "text-center"
            },{
                width: '25%',
                targets: 5,
                className: "text-center"
            }     
        ],
        "processing": false,
        "serverSide": false,
        "responsive": true, 
        "order": [],
        "pageLength": 10,
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    table = $('#data-pelamar').DataTable({ 
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ajax": {
            "url": $BASE_URL+ "loker/getPelamar",
            "type": "POST",
            "data": function ( data ) {
                
            }
        },
      'order': [[1, 'asc']]
    });
    $('.select').select2({
        minimumResultsForSearch: 20,
        width: '250px'
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
});
$(document).ready(function() {
    jQuery("#filter_pencarian").hide();
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="provinsi_filter"]').select2("val", "");
        jQuery('[name="kota_filter"]').select2("val", "");
        jQuery("#filter_pencarian").hide("slow");
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function save(link){
    $('#btnSimpan').text('Proses...'); 
    $('#btnSimpan').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add_kat";
    } else {
        url = $BASE_URL+link+"/proses_edit_kat";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_form').modal('hide');
                window.location.reload();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    jQuery('[name="'+data.inputerror[i]+'"]').show().addClass('default-select2 form-control');
                }
            }
            $('#btnSimpan').text('Simpan');
            $('#btnSimpan').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSimpan').text('Simpan');
            $('#btnSimpan').attr('disabled',false);
        }
    });
}
function update_det(link){
    $('#btnSimpan_det').text('Proses...'); 
    $('#btnSimpan_det').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add_det";
    } else {
        url = $BASE_URL+link+"/proses_edit_det";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_det').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_det').modal('hide');
                window.location.reload();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    jQuery('[name="'+data.inputerror[i]+'"]').show().addClass('default-select2 form-control');
                }
            }
            $('#btnSimpan_det').text('Simpan');
            $('#btnSimpan_det').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSimpan_det').text('Simpan');
            $('#btnSimpan_det').attr('disabled',false);
        }
    });
}
function update_lokasi(link){
    $('#btnSimpan_lokasi').text('Proses...'); 
    $('#btnSimpan_lokasi').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add_lokasi";
    } else {
        url = $BASE_URL+link+"/proses_edit_lokasi";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_lokasi').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_lokasi').modal('hide');
                window.location.reload();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    jQuery('[name="'+data.inputerror[i]+'"]').show().addClass('default-select2 form-control');
                }
            }
            $('#btnSimpan_lokasi').text('Simpan');
            $('#btnSimpan_lokasi').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSimpan_lokasi').text('Simpan');
            $('#btnSimpan_lokasi').attr('disabled',false);
        }
    });
}
function edit_kat(page,link,action,id){ 
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="kategori"]').select2("val", data.kode_bidang);
            $('[name="id"]').val(data.id);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Data Kategori');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function edit_lokasi(page,link,action,id){ 
    save_method = 'update';
    $('#form_lokasi')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="lokasi"]').select2("val", data.kode_prov);
            $('[name="id"]').val(data.id);
            $('#modal_lokasi').modal('show');
            $('.modal-title').text('Edit Data Lokasi Lowongan');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function edit_det(page,link,action,id){ 
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="jurusan"]').select2("val", data.kode_jurusan);
            $('[name="id"]').val(data.id);
            $('#modal_det').modal('show');
            $('.modal-title').text('Edit Data Spesifikasi Jurusan');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function tambah_data(){
    save_method = 'add';
    $('[name="kategori"]').select2("val", "0");
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('#btnSimpan').text('Simpan');
    $('#btnSimpan').attr('disabled',false);
    $('.modal-title').text('Tambah Data Kategori Lowongan Pekerjaan');
}
function tambah_data_det(){
    save_method = 'add';
    $('[name="jurusan"]').select2("val", "0");
    $('#form_det')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_det').modal('show');
    $('#btnSimpan_det').text('Simpan');
    $('#btnSimpan_det').attr('disabled',false);
    $('.modal-title').text('Tambah Data Spesifikasi Lowongan Pekerjaan');
}
function tambah_data_lokasi(){
    save_method = 'add';
    $('[name="lokasi"]').select2("val", "0");
    $('#form_lokasi')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_lokasi').modal('show');
    $('#btnSimpan_lokasi').text('Simpan');
    $('#btnSimpan_lokasi').attr('disabled',false);
    $('.modal-title').text('Tambah Data Lokasi Lowongan Pekerjaan');
}