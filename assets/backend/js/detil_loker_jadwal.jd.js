var save_method;
var table;
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(function() {
    table = $('#data-syarat').DataTable({ 
        autoWidth: false,
        columnDefs: [{
                width: '5',
                targets: 0,
                className: "text-center",
            },{            
                width: '25%',           
                targets: 1
            },{            
                width: '25%',           
                targets: 2,
                className: "text-center",
            },{
                width: '25%',
                targets: 3,
                className: "text-center"
            },{
                width: '25%',
                targets: 4,
                className: "text-center"
            },{
                width: '25%',
                targets: 5,
                className: "text-center"
            }     
        ],
        "processing": false,
        "serverSide": false,
        "responsive": true, 
        "order": [],
        "pageLength": 10,
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    table = $('#data-pelamar').DataTable({ 
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ajax": {
            "url": $BASE_URL+ "jadwal_test/getPelamar",
            "type": "POST",
            "data": function ( data ) {
                
            }
        },
      'order': [[1, 'asc']]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
});
$(document).ready(function() {
    jQuery("#filter_pencarian").hide();
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="provinsi_filter"]').select2("val", "");
        jQuery('[name="kota_filter"]').select2("val", "");
        jQuery("#filter_pencarian").hide("slow");
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}