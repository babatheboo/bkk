var save_method;
var table;
$(function() {
    // TableManageResponsive.init();
    jQuery("#filter_pencarian").hide();
    table = $('#data-ketua').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+"ketua_bkk/getData",
            "type": "POST",
            "data": function ( data ) {
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
            "width": '1%',
        },{ 
            "targets": [ 1 ],
            "orderable": false, 
            "width": '10%',
        },{ 
            "targets": [ 2 ],
            "orderable": true, 
            "width": '50%',
        },{ 
            "targets": [ 3 ],
            "orderable": true, 
            "width": '10%',
        },{ 
            "targets": [ 4 ],
            "orderable": false, 
            "width": '10%',
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        mikodeumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.select').select2({
        mikodeumResultsForSearch: Infinity,
        width: '250px'
    });
});

function cek(id){
$("#modal-edit").modal('show');
 $.ajax({
        url: 'ketua_bkk/cek/' + id,
        'type': 'post',
        success: function(data) {
            var data = $.parseJSON(data);
            $('#idna').val(data.idna);
            $('#nama').val(data.nama);
            $('#tlp').val(data.tlp);
            $('#email').val(data.email);
            $('#smk').val(data.smk);
        },
        error: function() {
            $.gritter.add({title:"Terjadi Kesalahan !",text: " Ulangi proses !"});return false;
        }
    });
}

function edit(){
    var idna = $("#idna").val();
    var smk = $("#smk").val();
    var nama = $("#nama").val();
    var tlp = $("#tlp").val();
    var email = $("#email").val();
    if (smk=='') {
        $.gritter.add({title:"Data tidak sesuai !",text: " Nama tidak boleh kosong"});return false;
    }else if (nama=='') {
        $.gritter.add({title:"Data tidak sesuai !",text: " Nama ketua BKK tidak boleh kosong"});return false;
    }else if (tlp=='') {
        $.gritter.add({title:"Data tidak sesuai !",text: " Nomor tlp tidak boleh kosong"});return false;
    }else if (email=='') {
        $.gritter.add({title:"Data tidak sesuai !",text: " Email tidak boleh kosong"});return false;
    }else if (idna=='') {
        $.gritter.add({title:"Terjadi kesalahan !",text: " ID Data tidak ditemukan"});return false;
    }else{
        $.ajax({
            url: $BASE_URL + "ketua_bkk/edit",
            method: "POST",
            dataType: "json",
            data: { idna: idna, nama: nama, email: email, tlp: tlp },
            beforeSend: function() {
                jQuery.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: 2,
                        color: '#fff'
                    },
                    message: 'Pemeriksaan data <br/> silahkan tunggu ...'
                });
            },
            success: function(data) {
                jQuery.unblockUI();
                $('#modal-edit').modal('hide');
                var data = $.parseJSON(JSON.stringify(data));
                if (data.response == 'fun') {
                    $.gritter.add({title:"Berhasil ✔",text: " Data telah diperbarui"});return false;
                    reload_table();
                    $("#smk").val('');
                    $("#nama").val('');
                    $("#tlp").val('');
                    $("#email").val('');
                }else if (data.response == 'nuf') {
                    $.gritter.add({title:"Gagal !",text: " Ulangi proses"});return false;
                }else{
                    $.gritter.add({title:"Terjadi kesalahan !",text: " Periksa koneksi interne"});return false;
                }
            },
            error: function() {
                jQuery.unblockUI();
                $('#modal-edit').modal('hide');
                $.gritter.add({title:"Terjadi kesalahan !",text: " Gagal menyimpan data"});return false;
            }
        });
    }
}

function befHapus(id, nama){
    $("#modal-hapus").modal('show');
    $("#idh").val(id);
    document.getElementById("nm").innerHTML = nama;
}

function hapus(){
    var idh = $("#idh").val();
    $.ajax({
            url: $BASE_URL + "ketua_bkk/hapus",
            method: "POST",
            dataType: "json",
            data: { idh: idh },
            beforeSend: function() {
                jQuery.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: 2,
                        color: '#fff'
                    },
                    message: 'Pemeriksaan data <br/> silahkan tunggu ...'
                });
            },
            success: function(data) {
                jQuery.unblockUI();
                $('#modal-hapus').modal('hide');
                var data = $.parseJSON(JSON.stringify(data));
                if (data.response == 'fun') {
                    $.gritter.add({title:"Berhasil ✔",text: " Data berhasil dihapus"});return false;
                    reload_table();
                }else if (data.response == 'nuf') {
                    $.gritter.add({title:"Gagal !",text: " Ulangi proses"});return false;
                }else{
                    $.gritter.add({title:"Terjadi kesalahan !",text: " Periksa koneksi interne"});return false;
                }
            },
            error: function() {
                jQuery.unblockUI();
                $('#modal-hapus').modal('hide');
                $.gritter.add({title:"Terjadi kesalahan !",text: " Gagal menyimpan data"});return false;
            }
        });
}

function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}