var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    jQuery("#filter_pencarian").hide();
    table = $('#data-total').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        "processing": true, 
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": $BASE_URL+ "revitalisasi/getData",
            "type": "POST",
            "data": function ( data ) {
                data.tahun_keluar = $('#tahun_keluar').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    jQuery("#tahun_keluar").change(function(){
        table.ajax.reload(null,false); 
    });
});
function filter_data(){
    jQuery("#filter_pencarian").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery('[name="tahun_keluar"]').select2("val", "");
        jQuery("#filter_pencarian").hide();
        jQuery.unblockUI();
    }, 100);
}
