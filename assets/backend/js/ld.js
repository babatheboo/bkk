var save_method;
var table;
$(function() {
    // TableManageResponsive.init();
    jQuery("#filter_pencarian").hide();
    table = $('#data-iklan').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+"loker_d/getData",
            "type": "POST",
            "data": function ( data ) {
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
            "width": '1%',
        },{ 
            "targets": [ 1 ],
            "orderable": false, 
            "width": '10%',
        },{ 
            "targets": [ 2 ],
            "orderable": true, 
            "width": '50%',
        },{ 
            "targets": [ 3 ],
            "orderable": true, 
            "width": '10%',
        },{ 
            "targets": [ 4 ],
            "orderable": false, 
            "width": '10%',
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        mikodeumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $('.select').select2({
        mikodeumResultsForSearch: Infinity,
        width: '250px'
    });
});
function tambah_data(){
    save_method = 'add';
    jQuery("#filter_pencarian").hide("slow");
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('#label-photo').text('Upload Photo');
    $('.modal-title').text('Tambah Data Iklan');
    $('#photo-preview').hide();
}

function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function edit_data(page,link,action,id){ 
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="deskripsi"]').val(data.deskripsi);
            $('[name="judul"]').val(data.judul);
            $('[name="perusahaan"]').val(data.perusahaan);
            $('[name="tgl_tutup"]').val(data.tgl_tutup);
            $('[name="id"]').val(data.id);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Data Iklan');
            $('[name="kode"]').attr('disabled',false);
            $('#photo-preview').show();
            if(data.gambar){
                $('#label-photo').text('Ganti Photo'); 
                $('#photo-preview div').html('<img src="'+$BASE_URL+'foto/iklan/'+data.gambar+'" class="img-responsive">'); 
            }else{
                $('#label-photo').text('Upload Foto'); 
                $('#photo-preview div').text('(No Foto)');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function save_iklan(page,link,formid){
    $('#btnSave').text('proses menyimpan data...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+ page+"/save_"+link; 
    } else {
        url = $BASE_URL+ page+"/update_"+link; 
    }
    var formData = new FormData($(formid)[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
            if(data.status ==  true ) {
                $('#modal_form').modal('hide');
                reload_table();
            }else if(data.status == false ){
                for (var i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }else if(data.status == 'NoAktived'){
                $.gritter.add({title:"Informasi !",text: " Data Tidak Boleh Kosong !"});return false;
            }else{
                alert('tidak bisa di update data sudah tersedia');              
            }
            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error adding / update data');
            $('#btnSave').text('Simpan'); 
            $('#btnSave').attr('disabled',false); 
        }
    });
}