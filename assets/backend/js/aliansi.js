var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var table;
$(function() {
    jQuery("#filter_pencarian_").hide();
    table = $('#data-affiliate').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "affiliate/getData",
            "type": "POST",
            "data": function ( data ) {
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}

function reload_recom_() {
    $(".recommend-list").empty();
    var page = '0';
    getRecommend_(page);
}

jQuery(document).ready(function(){
    getRecommend_(0);
    $("#reload_recom_").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getRecommend_(page);
    });
})

var getRecommend_ = function(page){
    $("#loader__").show();
    var recommend = jQuery("#recommend").data();
    $.ajax({
        url:$BASE_URL+"affiliate/recommendAff",
        type:'GET',
        data: {page:page,recommend:recommend}
    }).done(function(response){
        $(".recommend-list").append(response);
        $("#loader__").hide();
        $('#reload_recom_').data('val', ($('#reload_recom_').data('val')+1));
    });
};

$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:$BASE_URL+"affiliate/fetch",
   method:"POST",
   data:{query:query},
   success:function(data){
    $('#result').html(data);
   }
  })
 }

 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   $("#result").show();
   load_data(search);
  }
  else
  {
  $("#result").hide();
   load_data();
  }
})
 });

/*aff*/
$(document).ready(function(){
    var limit = 8;
    var start = 0;
    var action = 'inactive';
    function lazzy_loaderAff(limit){
      for(var count=0; count<limit; count++)
      {
        $("#loaderAff").show('slow');
        $("#eep").hide('slow');
      }
    }
    lazzy_loaderAff(limit);
    function load_aff(limit, start){
      $.ajax({
        url:$BASE_URL+"affiliate/getData",
        method:"POST",
        data:{limit:limit, start:start},
        cache: false,
        success:function(data){
          if(data == ''){
            $("#loaderAff").hide('slow');
            $("#eep").show('slow');
            action = 'active';
          }else{
            $('#load_aff').append(data);
            $("#loaderAff").hide('slow');
            $("#eep").show('slow');
            action = 'inactive';
          }
        }
      });
    }
    if(action == 'inactive'){
      action = 'active';
      load_aff(limit, start);
    }
    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > $("#aff").height() && action == 'inactive')
      {
        lazzy_loaderAff(limit);
        action = 'active';
        start = start + limit;
        setTimeout(function(){
          load_aff(limit, start);
        }, 1000);
      }
    });
  });
/*mohon*/
$(document).ready(function(){
    var limit = 8;
    var start = 0;
    var action = 'inactive';
    function lazzy_loaderMohon(limit){
      var output = '';
      for(var count=0; count<limit; count++){
        $("#loaderMohon").show('slow');
        $("#seep").hide('slow');
      }
    }
    lazzy_loaderMohon(limit);
    function load_mohon(limit, start){
      $.ajax({
        url:$BASE_URL+"affiliate/getDataTunggu",
        method:"POST",
        data:{limit:limit, start:start},
        cache: false,
        success:function(data){
          if(data == ''){
            $("#loaderMohon").hide('slow');
            $("#seep").show('slow')
            action = 'active';
          }else{
            $('#load_mohon').append(data);
            $("#loaderMohon").hide('slow');
            $("#seep").show('slow');
            action = 'inactive';
          }
        }
      });
    }
    if(action == 'inactive'){
      action = 'active';
      load_mohon(limit, start);
    }
    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > $("#mohon").height() && action == 'inactive')
      {
        lazzy_loaderMohon(limit);
        action = 'active';
        start = start + limit;
        setTimeout(function(){
          load_mohon(limit, start);
        }, 1000);
      }
    });
  });
/*minta*/
$(document).ready(function(){
    var limit = 8;
    var start = 0;
    var action = 'inactive';

    function lazzy_loaderMinta(limit){
      var output = '';
      for(var count=0; count<limit; count++){
        $("#loaderMinta").show('slow');
        $("#abis").hide('slow');
      }
    }
    lazzy_loaderMinta(limit);
    function load_minta(limit, start){
      $.ajax({
        url:$BASE_URL+"affiliate/getDataReq",
        method:"POST",
        data:{limit:limit, start:start},
        cache: false,
        success:function(data){
          if(data == ''){
            $("#abis").show('slow');
            $("#loaderMinta").hide('slow');
            action = 'active';
          }else{
            $('#load_minta').append(data);
            $("#loaderMinta").hide('slow');
            $("#abis").show('slow');
            action = 'inactive';
          }
        }
      });
    }
    if(action == 'inactive'){
      action = 'active';
      load_minta(limit, start);
    }
    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > $("#minta").height() && action == 'inactive')
      {
        lazzy_loaderMinta(limit);
        action = 'active';
        start = start + limit;
        setTimeout(function(){
          load_minta(limit, start);
        }, 1000);
      }
    });
  });