jQuery(document).ready(function(){
    getLoker(0);
    $("#reload_loker").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getLoker(page);
    });
    getRekomend(0);
    $("#reload_rekomend").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getRekomend(page);
    });
    getTotalLoker();
    $("#reload_rekomend").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getRekomend(page);
    });
    getAliansi(0);
    $("#reload_aliansi").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getAliansi(page);
    });
})
function hapus_datax(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+link+"/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        reload_aliansix();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di hapus."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        getAliansi(0);
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}
var getTotalLoker = function(){
    $.ajax({
        url:$BASE_URL+"dashboard/getTotLoker",
        type:'GET'
    }).done(function(response){
        $("#tot_low").append(response);
    });
};
var getLoker = function(page){
 //   $("#loader").show();
  //  $.ajax({
    //    url:$BASE_URL+"dashboard/getLoker",
      //  type:'GET',
      //  data: {page:page}
   // }).done(function(response){
     //   $("#result-loker").append(response);
     //   $("#loader").hide();
     //   $('#reload_loker').data('val', ($('#reload_loker').data('val')+1));
  //  });
};
function approve_x(id) {
    bootbox.confirm("Yakin Akan Menambahkan Aliansi BKK Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+"affiliate/approve_req/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        reload_aliansix();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di approve."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error approve data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
    
}
var getAliansi = function(page){
    $("#loader").show();
    $.ajax({
        url:$BASE_URL+"dashboard/getAliansi",
        type:'GET',
        data: {page:page}
    }).done(function(response){
        $("#result-aliansi").append(response);
        $("#loader").hide();
        $('#reload_aliansi').data('val', ($('#reload_aliansi').data('val')+1));
    });
};
var getRekomend = function(page){
    $("#loader").show();
    $.ajax({
        url:$BASE_URL+"dashboard/getRekomend",
        type:'GET',
        data: {page:page}
    }).done(function(response){
        $("#result-rekomend").append(response);
        $("#loader").hide();
        $('#reload_rekomend').data('val', ($('#reload_rekomend').data('val')+1));
    });
};
function acc_loker (jns,page,link,id){
    if(jns=="aktif"){
        bootbox.confirm("Non Aktifkan " +page+ " Berikut ?", function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Mohon menunggu ... '
                    });
                    jQuery.post($BASE_URL+link+"/ubah_status/"+jns+"/"+id, jQuery("#form1").serialize(),
                        function(data){
                            $.unblockUI();
                            reload_loker();
                            reload_loker_()
                        });
                }, 500);
            }
        });
    }else{
        bootbox.confirm("Aktifkan " +page+ " Berikut ?", function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Mohon menunggu ... '
                    });
                    jQuery.post($BASE_URL+link+"/ubah_status/"+jns+"/"+id, jQuery("#form1").serialize(),
                        function(data){
                            $.unblockUI();
                            reload_loker();
                            reload_loker_()
                        });
                }, 500);
            }
        });
    }
}
function reload_loker() {
    $("#result-loker").empty();
    $("#result-rekomend").empty();
    $("#tot_low").empty();
    var page = '0';
    getLoker(page);
    var page = '0';
    getRekomend(page);
    getTotalLoker();
}
function reload_aliansix() {
    $("#result-aliansi").empty();
    var page = '0';
    getAliansi(page);
}

jQuery(document).ready(function(){
    jQuery("#filter-kerja").hide();
    getLoker_(0);
    $("#reload_loker_").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getLoker_(page);
    });
    $("#kat_loker").change(function(){
        setTimeout(function () {
            jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
            $(".result-list").empty();
            var page = '0';
            getLoker_(page);
            jQuery.unblockUI();
        }, 100);
    });
    $("#industri").change(function(){
        setTimeout(function () {
            jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
            $(".result-list").empty();
            var page = '0';
            getLoker_(page);
            jQuery.unblockUI();
        }, 100);
    });
})
function cari_loker() {
    setTimeout(function () {
        jQuery.blockUI({
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 2, 
                color: '#fff' 
            },
            message : 'Mohon menunggu ... '
        });
        $(".result-list").empty();
        var page = '0';
        getLoker_(page);
        jQuery.unblockUI();
    }, 100);
}
function filter_search() {
    jQuery("#filter-kerja").show("slow");
}
function hide_() {
    jQuery("#filter-kerja").hide("slow");
}
var getLoker_ = function(page){
    $("#loader_").show();
    var kat = jQuery("#kat_loker").val();
    var industri = jQuery("#industri").val();
    var cari = jQuery("#cari").val();
    $.ajax({
        url:$BASE_URL+"loker/getLoker",
        type:'GET',
        data: {page:page,kat:kat,industri:industri,cari:cari}
    }).done(function(response){
        $(".result-list").append(response);
        $("#loader_").hide();
        $('#reload_loker_').data('val', ($('#reload_loker_').data('val')+1));
    });
};
function reload_loker_() {
    $(".result-list").empty();
    var page = '0';
    getLoker_(page);
}

function apply_jobsis(job,prm){
    bootbox.confirm("klik apply untuk melamar "+ job + " ? ", 
        function(result) {
            if(result==true){
                setTimeout(function () {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Sedang Melakukan Proses Bidding Loker, Mohon menunggu ... '
                    });
                    $.ajax({
                        url : $BASE_URL+'loker/apply_jobsis/'+prm,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data){
                            if(data.status == true ) {
                             $.gritter.add({
                                title: "notifikasi ",
                                text: 'loker berhasil di bid cek menu bid loker untuk detail',
                                class_name: "gritter-light"
                            });
                             window.location.reload();        
                         }else{
                            $.gritter.add({
                                title: "notifikasi",
                                text: 'bidding loker gagal',
                                class_name: "gritter-light"
                            });
                        }
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error deleting data');
                    }
                });
                }, 500);    
            }     
        });
}

function unbid_jobsis(job,prm){
    bootbox.confirm("batal apply lamaran "+ job + " ? ", 
        function(result) {
            if(result==true){           
                setTimeout(function() {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Sedang Melakukan Proses Bidding Loker, Mohon menunggu ... '
                    });
                    $.ajax({
                        url : $BASE_URL+'loker/unbid_jobsis/'+prm,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data){
                            if(data.status == true ) {
                             $.gritter.add({
                                title: "notifikasi ",
                                text: 'loker berhasil unbid loker',
                                class_name: "gritter-light"
                            });
                             window.location.reload();    
                         }else{
                            $.gritter.add({
                                title: "notifikasi",
                                text: 'unbid loker gagal',
                                class_name: "gritter-light"
                            });
                        }
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error deleting data');
                    }
                });
                }, 500);    
            }
        });
}

function bookmark(job,prm){
    bootbox.confirm("bookmark "+ job + " ? ", 
        function(result) {
            if(result==true){           
                setTimeout(function() {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Sedang Melakukan Proses Bookmark Loker, Mohon menunggu ... '
                    });
                    $.ajax({
                        url : $BASE_URL+'loker/bookmark_jobsis/'+prm,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data){
                            if(data.status == true ) {
                             $.gritter.add({
                                title: "notifikasi ",
                                text: 'loker berhasil bookmark loker',
                                class_name: "gritter-light"
                            });
                             window.location.reload();    
                         }else{
                            $.gritter.add({
                                title: "notifikasi",
                                text: 'bookmark loker sudah di bookmark sebelumnya',
                                class_name: "gritter-light"
                            });
                        }
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error bookmark data');
                    }
                });
                }, 500);    
            }
        });
}

function unbookmark(job,prm){
    bootbox.confirm("unbookmark "+ job + " ? ", 
        function(result) {
            if(result==true){           
                setTimeout(function() {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Sedang Melakukan Proses Unbookmark Loker, Mohon menunggu ... '
                    });
                    $.ajax({
                        url : $BASE_URL+'loker/unbookmark_jobsis/'+prm,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data){
                            if(data.status == true ) {
                             $.gritter.add({
                                title: "notifikasi ",
                                text: 'loker berhasil unbookmark loker',
                                class_name: "gritter-light"
                            });
                             window.location.reload();    
                         }else{
                            $.gritter.add({
                                title: "notifikasi",
                                text: 'unbookmark loker gagal',
                                class_name: "gritter-light"
                            });
                        }
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error unbookmark data');
                    }
                });
                }, 500);    
            }
        });
}