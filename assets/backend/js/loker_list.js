jQuery(document).ready(function(){
    jQuery("#filter-kerja").hide();
    getLoker(0);
    $("#reload_loker").click(function(e){
        e.preventDefault();
        var page = $(this).data('val');
        getLoker(page);
    });
    $("#kat_loker").change(function(){
        setTimeout(function () {
            jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
            $(".result-list").empty();
            var page = '0';
            getLoker(page);
            jQuery.unblockUI();
        }, 100);
    });
    $("#industri").change(function(){
        setTimeout(function () {
            jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
            $(".result-list").empty();
            var page = '0';
            getLoker(page);
            jQuery.unblockUI();
        }, 100);
    });
})
function cari_loker() {
    setTimeout(function () {
        jQuery.blockUI({
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 2, 
                color: '#fff' 
            },
            message : 'Mohon menunggu ... '
        });
        $(".result-list").empty();
        var page = '0';
        getLoker(page);
        jQuery.unblockUI();
    }, 100);
}
function filter_search() {
    jQuery("#filter-kerja").show("slow");
}
function hide_() {
    jQuery("#filter-kerja").hide("slow");
}
var getLoker = function(page){
    $("#loader").show();
    var kat = jQuery("#kat_loker").val();
    var industri = jQuery("#industri").val();
    var cari = jQuery("#cari").val();
    $.ajax({
        url:$BASE_URL+"loker/lokerku",
        type:'GET',
        data: {page:page,kat:kat,industri:industri,cari:cari}
    }).done(function(response){
        $(".result-list").append(response);
        $("#loader").hide();
        $('#reload_loker').data('val', ($('#reload_loker').data('val')+1));
    });
};
function reload_loker() {
    $(".result-list").empty();
    var page = '0';
    getLoker(page);
}