var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    table = $('#data-upsmk').DataTable({ 
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "responsive": true, 
        "order": [],
        "pageLength": 20,
        "ajax": {
            "url": $BASE_URL+ "upsmk/getData",
            "type": "POST"
        },
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});

function tambah_data(){
    save_method = 'add';
    $('#forminp')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data User');
}

function ubah_pass(id){
    save_method = 'add';
    $('#formch')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#idna').val(id);
    $('#modal_form_pass').modal('show');
    $('.modal-title').text('Rubah password');
}


function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function edit_data(page,link,action,id){ 
    save_method = 'update';
    $('#forminp')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="nama"]').val(data.nama);
            $('[name="nip"]').val(data.nip);
            $('[name="mail"]').val(data.email);
            $('[name="tlp"]').val(data.tlp);
            $('[name="id"]').val(data.user_id);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Data User');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error getting data from ajax');
        }
    });
}
function save(link){
    $('#btnSave').text('Proses...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add";
    } else {
        url = $BASE_URL+link+"/proses_edit";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#forminp').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        }
    });
}
function save_pass(link,id){
    $('#btnSave').text('Proses...'); 
    $('#btnSave').attr('disabled',true);
    var plama = $('#psatu').val();
    var pbaru = $('#pdua').val();
    if(plama!=pbaru || plama=='' || pbaru=='')
    {
        alert("Password tidak sama atau password kosong !, ulangi proses");
        return true;
    }
    else
    {
        var url;
        if(save_method == 'add') {
            url = $BASE_URL+link+"/proses_ch_pass";
        } 
        $.ajax({
            url : url,
            type: "POST",
            data: $('#formch').serialize(),
            dataType: "JSON",
            success: function(data){
                if(data.status){
                    $('#modal_form_pass').modal('hide');
                    reload_table();
                }else{
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
            }
        });

    }
}


function hapus_data(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+"upsmk/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#modal_form').modal('hide');
                        reload_table();
                        jQuery.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });


   
}
