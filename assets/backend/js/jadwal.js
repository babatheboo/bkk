var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    table = $('#data-jadwal').DataTable({ 
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "jadwal_test/getJadwalTes",
            "type": "POST",
            "data": function ( data ) {
                data.bidang = $('#bidangx').val();
            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false, 
        },
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: '60px'
    });
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    jQuery("#bidangx").change(function(){
        table.ajax.reload(null,false); 
    });
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
