var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  

function save(link){
    $('#btnSave').text('Proses...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+link+"/proses_add";
    } else {
        url = $BASE_URL+link+"/proses_edit";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    jQuery('[name="'+data.inputerror[i]+'"]').show().addClass('default-select2 form-control');
                }
            }
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            //return false;
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        }
    });
}
function validAngka(a){
    if(!/^[0-9.]+$/.test(a.value)){
        a.value = a.value.substring(0,a.value.length-1000);
    }
}
function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}
function hapus_data(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+link+"/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#modal_form').modal('hide');
                        reload_table();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di hapus."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        // return false;
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}
function hapus_kat(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+link+"/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#modal_form').modal('hide');
                        jQuery.unblockUI();
                        window.location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}
function hapus_loker(page,link,action,id){
    bootbox.confirm("Yakin Akan Menghapus " +page+ " Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff'
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+link+"/"+action+"/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        if(data.status==false){
                            jQuery.unblockUI();
                            $.gritter.add({title:"Informasi !",text: " Data gagal di hapus."});return false;
                        }else{
                            jQuery.unblockUI();
                            $.gritter.add({title:"Informasi !",text: " Data berhasil di hapus."});
                            window.location.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error deleting data."});
                        // return false;
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}
function rbstatus (jns,page,link,id){
    if(jns=="aktif"){
        bootbox.confirm("Non Aktifkan " +page+ " Berikut ?", function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Mohon menunggu ... '
                    });
                    jQuery.post($BASE_URL+link+"/ubah_status/"+jns+"/"+id, jQuery("#form1").serialize(),
                    function(data){
                        $.unblockUI();
                        reload_table();
                    });
                }, 500);
            }
        });
    }else{
        bootbox.confirm("Aktifkan " +page+ " Berikut ?", function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Mohon menunggu ... '
                    });
                    jQuery.post($BASE_URL+link+"/ubah_status/"+jns+"/"+id, jQuery("#form1").serialize(),
                    function(data){
                        $.unblockUI();
                        reload_table();
                    });
                }, 500);
            }
        });
    }
}
function logout(nama){
    bootbox.confirm(nama+" apakah yakin akan keluar ?", function(result) {
        if (result) {
            $.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Sedang Melakukan Proses Log Out, Mohon menunggu ... '
            });
            setTimeout(function(){
                $.unblockUI();
            },500);
            $.ajax({
                url : $BASE_URL+'login/doLogout',
                complete : function(response) {
                    $.unblockUI();
                    window.location.href = $BASE_URL;
                }
            });             
        }
    });
}
function date_time(id){
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    h = date.getHours();
    if(h<10){
        h = "0"+h;
    }
    m = date.getMinutes();
    if(m<10){
        m = "0"+m;
    }
    s = date.getSeconds();
    if(s<10){
        s = "0"+s;
    }
    result = ''+days[day]+', '+d+'-'+months[month]+'-'+year+' '+h+':'+m+':'+s;
    document.getElementById(id).innerHTML = result;
    setTimeout('date_time("'+id+'");','500');
    return true;
}
function waktos(id){
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    h = date.getHours();
    if(h<10){
        h = "0"+h;
    }
    m = date.getMinutes();
    if(m<10){
        m = "0"+m;
    }
    s = date.getSeconds();
    if(s<10){
        s = "0"+s;
    }
    result = ' '+h+':'+m+':'+s;
    document.getElementById(id).innerHTML = result;
    setTimeout('waktos("'+id+'");','500');
    return true;
}
function kaping(id){
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('Januari', 'Pebruari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
    h = date.getHours();
    if(h<10){
        h = "0"+h;
    }
    m = date.getMinutes();
    if(m<10){
        m = "0"+m;
    }
    s = date.getSeconds();
    if(s<10){
        s = "0"+s;
    }
    result = ''+days[day]+', '+d+'-'+months[month]+'-'+year+'';
    document.getElementById(id).innerHTML = result;
    setTimeout('kaping("'+id+'");','500');
    return true;
}

function cek_nisn(id,link,cek_action){
    
    $.ajax({
        url : $BASE_URL+link+'/'+cek_action+'/'+$('#'+id).val(),
        dataType : 'json',
        type : 'post',
        success : function(json) {
            $('#nss_warning').empty();
            $('#'+id+'_warning').empty();
            if(json.say == "ok") {
                $('#'+id+'_warning').empty();
                $('#nama').val(json.nama);
                $('#nama_sekolah').val(json.nama_sekolah);
                $('#peserta_didik_id').val(json.peserta_didik_id);
                if(json.sekolah_terdaftar != 'terdaftar'){
                    $('#nss_warning').append(json.sekolah_terdaftar);
                    $('#tombol').hide();
                    $('#form_password').hide();
                }else{
                    $('#tombol').show();
                    $('#form_password').show();
                }
                return true;
            }else{
                $('#'+id+'_warning').append('NISN Tidak Ditemukan!');
                $('#nama').val("");
                $('#nama_sekolah').val("");
                $('#tombol').hide();
                $('#form_password').hide();
                return false;
            }

        }
    });  
}

function cek_npsn(id,link,cek_action){
    
    $.ajax({
        url : $BASE_URL+link+'/'+cek_action+'/'+$('#'+id).val(),
        dataType : 'json',
        type : 'post',
        success : function(json) {
            if(json.say == "ok") {
                $('#'+id+'_warning').empty();
                $('#nama').val(json.nama);
                
                if(json.sekolah_terdaftar != 'Sudah Terdaftar'){
                    $('#nss_warning').append(json.sekolah_terdaftar);
                    $('#tombol').hide();
                    $('#form_password').hide();
                }else{
                    $('#tombol').show();
                    $('#form_password').show();
                }

                return true;
            }else{
                $('#'+id+'_warning').append('NSS Tidak Ditemukan!');
                $('#nama').val("");
                return false;
            }

        }
    });  
}

function cek_prov(id,link,cek_action){
    
    $.ajax({
        url : $BASE_URL+link+'/'+cek_action+'/'+$('#'+id).val(),
        dataType : 'json',
        type : 'post',
        success : function(json) {
            if(json.say != '') {
                $('#kota').empty();
                $('#kota').append(json.say);
                $('#form_kota').show();
                $('#tombol').hide();
                return true;
            }else{
                return false;
            }

        }
    });  
}

function cek_sekolah(id,link,cek_action){
    
    $.ajax({
        url : $BASE_URL+link+'/'+cek_action+'/'+$('#'+id).val(),
        dataType : 'json',
        type : 'post',
        success : function(json) {
            if(json.say != '') {
                $('#sekolah').empty();
                $('#sekolah').append(json.say);                
                $('#form_sekolah').show();
                $('#tombol').hide();
                return true;
            }else{
                $('#sekolah_id').val();
            }

        }
    });  
}

function mitra_detil(kode){ 
    jQuery.post($BASE_URL+"affiliate/detil_sekolah/"+kode, 
    function(data){ 
        jQuery("#hasil").html(data); 
    }); 
}
function app(id) {
    $.ajax({
        url : $BASE_URL+"affiliate/approve/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data){
            $('#detil_mitra').modal('hide');
            reload_table();
            jQuery.unblockUI();
            $.gritter.add({title:"Informasi !",text: " Data berhasil di approve." +"<br/>"+"Sedang Menunggu Konfirmasi Dari Pihak BKK."});return false;
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: " Error approve data."});
            jQuery.unblockUI();
        }
    });
}
var getAliansi = function(page){
    $("#loader").show();
    $.ajax({
        url:$BASE_URL+"dashboard/getAliansi",
        type:'GET',
        data: {page:page}
    }).done(function(response){
        jQuery.unblockUI();
        $("#result-aliansi").append(response);
        $("#loader").hide();
        $('#reload_aliansi').data('val', ($('#reload_aliansi').data('val')+1));
    });
};
function approve_(id) {
    bootbox.confirm("Yakin Akan Menambahkan Aliansi BKK Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+"affiliate/approve_req/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#detil_mitra').modal('hide');
                        reload_table();
                        getAliansi(0);
                        reload_table_();
                        reload_table__();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di approve."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error approve data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
    
}
function app_(id) {
    bootbox.confirm("Yakin Akan Menambahkan Aliansi BKK Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+"affiliate/approve/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#detil_mitra').modal('hide');
                        reload_table();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di approve." +"<br/>"+"Sedang Menunggu Konfirmasi Dari Pihak BKK."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error approve data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });

}
function app_req(id) {
    $.ajax({
        url : $BASE_URL+"affiliate/app_req/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data){
            $('#detil_mitra').modal('hide');
            reload_table();
            jQuery.unblockUI();
            $.gritter.add({title:"Informasi !",text: " Data berhasil di approve." +"<br/>"+"Sedang Menunggu Konfirmasi Dari Pihak BKK."});return false;
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: " Error approve data."});
            jQuery.unblockUI();
        }
    });
}
function app_req(id) {
    bootbox.confirm("Yakin Akan Menambahkan Aliansi BKK Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                $.ajax({
                    url : $BASE_URL+"affiliate/app_req/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data){
                        $('#detil_mitra').modal('hide');
                        reload_table();
                        jQuery.unblockUI();
                        $.gritter.add({title:"Informasi !",text: " Data berhasil di approve." +"<br/>"+"Sedang Menunggu Konfirmasi Dari Pihak BKK."});return false;
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        $.gritter.add({title:"Informasi !",text: " Error approve data."});
                        jQuery.unblockUI();
                    }
                });
            }, 100);
        }
    });
}

function info(){
    bootbox.alert("<h5><center>Tampilkan semua data sebelum melakukan ekspor ✔</center></h5>", function() {
    });
}