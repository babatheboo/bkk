var save_method;
var table;
$(function() {
    table = $('#list-loker').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
        "processing": true,
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+ "lap_list_loker/getList",
            "type": "POST",
            "data": function ( data ) {

            }
        },
        "columns": [
            null,
            { orderable: false},
            { orderable: false},
            { orderable: false},
            { orderable: false},
            { orderable: false},
            { orderable: false},
        ],
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
});
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
