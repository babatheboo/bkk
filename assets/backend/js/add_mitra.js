var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
$(document).ready(function() {
    $("#bidang_industri").select2();
    $("#provinsi").select2();
    $("#kabupaten").select2();
    $("#provinsi").change(function(){
        var status = jQuery("#status").val();
        if(status!="ada"){
            var deptid = $(this).val();
            $.ajax({
                url: $BASE_URL+'loker/getKabkot/'+deptid,
                type: 'post',
                data: {depart:deptid},
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    $("#kabupaten").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['kota'];
                        $("#kabupaten").append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
            $.ajax({
                url: $BASE_URL+'mitra/getBidang',
                type: 'post',
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    $("#bidang_industri").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['bidang_usaha_id'];
                        var name = response[i]['nama_bidang_usaha'];
                        $("#bidang_industri").append("<option value='"+id+"'>"+name+"</option>");
                    }
                }
            });
        }
    });
    jQuery("#carialamat").hide();
    jQuery("#map_canvas").hide();
    jQuery("#address").keyup(function(){
        var address = jQuery("#address").val();
        if(address != ""){
            var address = document.getElementById("address").value;
            var propinsi = jQuery("#propinsi option:selected").text();
            var kabupaten = jQuery("#kabupaten option:selected").text();
            geocoder.geocode({ 'address': address + " " + propinsi + " " +kabupaten}, function(results, status){
                if(status == google.maps.GeocoderStatus.OK){
                    map.setCenter(results[0].geometry.location);
                    if(marker != undefined)
                        marker.setPosition(results[0].geometry.location);
                    else
                        marker = new google.maps.Marker({
                            map: map,
                            draggable: true,
                            position: results[0].geometry.location
                        });
                    updateMarkerPosition(results[0].geometry.location);
                    geocodePosition(results[0].geometry.location);
                    aksi();
                }else{
                    // alert("Geocode tidak berhasil dengan alasan karena: " + status);
                }
            });
        }
    });
    // $("#industri").autocomplete({  
    //     source: function(req, add){
    //         jQuery.blockUI({
    //         css: { 
    //             border: 'none', 
    //             padding: '15px', 
    //             backgroundColor: '#000', 
    //             '-webkit-border-radius': '10px', 
    //             '-moz-border-radius': '10px', 
    //             opacity: 0.5, 
    //             color: '#fff' 
    //         },
    //             message : 'Sedang Melakukan Pengecekan Data, Mohon menunggu ... '
    //         });
    //         jQuery.ajax({
    //             url:$BASE_URL+"mitra/getIndustri/",
    //             dataType:'json',
    //             type:'POST',
    //             data:req,                                                   
    //             success:function(data){
    //                 if(data.response=='true'){
    //                     jQuery.unblockUI();
    //                     add(data.message);                             
    //                 }else{
    //                     $.gritter.add({title:"Informasi !",text: "Data tidak ditemukan"+"</br>Silahkan Input Data Industri Baru."});
    //                     jQuery.unblockUI();
    //                     jQuery('#industri_id').val("");
    //                     jQuery('#no_tlp').val("");
    //                     jQuery('#alamat').val("");
    //                     jQuery('#email').val("");
    //                     jQuery('#status').val("");
    //                     $("#provinsi").select2({
    //                         placeholder:"Pilih Provinsi",
    //                         ajax:{
    //                             url:$BASE_URL+"mitra/getPro",
    //                             dataType: 'json',
    //                             data: function (params) {
    //                                 var queryParameters = {
    //                                     text: params.term
    //                                 }
    //                                 return queryParameters;
    //                             }   
    //                         },
    //                         cache: true,
    //                         minimumInputLength: 2,
    //                         formatResult: format,
    //                         formatSelection: format,
    //                         escapeMarkup: function(m) { return m; }
    //                     });
    //                     $("#bidang_industri").select2({
    //                         placeholder:"Pilih Bidang Usaha",
    //                         ajax:{
    //                             url:$BASE_URL+"mitra/getBid",
    //                             dataType: 'json',
    //                             data: function (params) {
    //                                 var queryParameters = {
    //                                     text: params.term
    //                                 }
    //                                 return queryParameters;
    //                             }   
    //                         },
    //                         cache: true,
    //                         minimumInputLength: 2,
    //                         formatResult: format,
    //                         formatSelection: format,
    //                         escapeMarkup: function(m) { return m; }
    //                     });
    //                     var pro = jQuery("#provinsi").val();
    //                     $("#kabupaten").select2({
    //                         placeholder:"Pilih Kabupaten",
    //                         ajax:{
    //                             url:$BASE_URL+"mitra/getKab",
    //                             type: 'post',
    //                             data: {pro:pro},
    //                             dataType: 'json',
    //                             data: function (params) {
    //                                 var queryParameters = {
    //                                     text: params.term
    //                                 }
    //                                 return queryParameters;
    //                             }   
    //                         },
    //                         cache: true,
    //                         minimumInputLength: 2,
    //                         formatResult: format,
    //                         formatSelection: format,
    //                         escapeMarkup: function(m) { return m; }
    //                     });
    //                 }
    //             },
    //             error:function(XMLHttpRequest){
    //                 alert(XMLHttpRequest.responseText);
    //             }
    //         })
    //     },
    //     minLength:3,
    //     select: function(event,ui){
    //         jQuery('#industri').val(ui.item.nama);
    //         jQuery('#alamat').val(ui.item.alamat);
    //         jQuery('#email').val(ui.item.email);
    //         jQuery('#no_tlp').val(ui.item.no_tlp);
    //         jQuery("#provinsi").empty().append('<option value='+ui.item.province_id+'>'+ui.item.nama_provinsi+'</option>').val(ui.item.province_id).trigger('change');
    //         jQuery('#status').val('ada');
    //         jQuery('#industri_id').val(ui.item.industri_id);
    //         get_data(ui.item.industri_id);
    //     },
    // });
});
function format(x){
    return x.text;
}
function get_data(v_id){
    $.ajax({
        url :$BASE_URL+"mitra/getInfo",
        data:{id : v_id},
        success: function(data){
            var dt = JSON.parse(data);
            jQuery("#kabupaten").select2().select2('val',dt.kode_wilayah);
            jQuery("#address").val(dt.alamat);
            jQuery("#bidang_industri").select2().select2('val',dt.bidang_usaha_id);
        }
    });
}
function validAngka(a){
    if(!/^[0-9.]+$/.test(a.value)){
        a.value = a.value.substring(0,a.value.length-1000);
    }
}
