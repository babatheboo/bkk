var host = window.location.host;
$BASE_URL = 'http://' + host + '/';
var current_page = 1;

function reload_table(){
  jQuery.blockUI({
    css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: 2, 
      color: '#fff' 
    },
    message : 'Mohon menunggu ... '
  });
  setTimeout(function () {
    table.ajax.reload(null,false);
    jQuery.unblockUI();
  }, 100);
}

function ceknpsn(npsn){
  bootbox.confirm("Reset password User berikut?", function(result) {
    if (result) {
      jQuery.blockUI({
        css: { 
          border: 'none', 
          padding: '15px', 
          backgroundColor: '#000', 
          '-webkit-border-radius': '10px', 
          '-moz-border-radius': '10px', 
          opacity: 2, 
          color: '#fff' 
        },
        message : 'Mohon menunggu ... '
      });
      $.ajax({
        url: $BASE_URL+ "resetpass/rubahbyNpsn/"+npsn,
        method:"POST",
        data:{npsn:npsn},
        cache: false,
        success:function(data)
        {
          jQuery.unblockUI();
          $.notify({icon:'fa fa-check',message:"Password berhasil direset"},{type:"success",offset:{x:3,y:2}});
      }
    });
    }
  });
}

$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
    url:$BASE_URL+"resetpass/fetch",
    method:"POST",
    data:{query:query},
    success:function(data){
      $('#result').html(data);
    }
  })
}

$('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
    $("#result").show('slow');
    $("#infoReset").hide('slow');
    load_data(search);
  }
  else
  {
    $("#result").hide('slow');
    $("#infoReset").show('slow');
    load_data();
  }
})
});