var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
        // $("#tbl").hide("slow");
        $(function() {
            $(".datepicker").datepicker({
                autoclose: true,
                todayHighlight: !0
            });
            // jQuery("#beja").hide();
            // jQuery("#tabs").hide();
            jQuery("#tblS").hide();
            // jQuery("#sinBtn").hide();
            jQuery("#bekerja_").hide();
            jQuery("#kuliah_").hide();
            jQuery("#wira_").hide();
            jQuery("#tombol").hide();
            jQuery("#status_siswax").change(function(){
                var status_siswax = jQuery("#status_siswax").val();
                if(status_siswax=='1'){
                    jQuery("#bekerja_").show('slow');
                    jQuery("#tombol").show('slow');
                    jQuery("#kuliah_").hide('slow');
                    jQuery("#wira_").hide('slow');
                }else if(status_siswax=='2'){
                    jQuery("#bekerja_").hide('slow');
                    jQuery("#kuliah_").show('slow');
                    jQuery("#tombol").show('slow');
                    jQuery("#wira_").hide('slow');
                }else if(status_siswax=='3'){
                    jQuery("#bekerja_").hide('slow');
                    jQuery("#kuliah_").hide('slow');
                    jQuery("#wira_").show('slow');
                    jQuery("#tombol").show('slow');
                }else{
                    jQuery("#bekerja_").hide('slow');
                    jQuery("#kuliah_").hide('slow');
                    jQuery("#wira_").hide('slow');
                    jQuery("#tombol").hide('slow');
                }
            });
            // $("#data-lulusan").dataTable().fnDestroy();
            table = $('#data-lulusan').DataTable({
                dom: 'lBfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                "processing": true, 
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": $BASE_URL+ "lulusan/getData",
                    "type": "POST",
                    "data": function ( data ) {
                        data.tahun_keluar = $('#tahun_keluar').val();
                        data.status_siswa = $('#status_siswa').val();
                    }
                },
                "columnDefs": [{ 
                    "targets": [ 0 ],
                    "orderable": false, 
                },
                ],
            });
            $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
            $('#btn-filter').click(function(){ 
                table.ajax.reload(null,false); 
            });
            jQuery("#tahun_keluar").change(function(){
             jQuery.blockUI({
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: 2, 
                    color: '#fff' 
                },
                message : 'Mohon menunggu ... '
            });
             $('#data-lulusan').on( 'draw.dt', function(){
                 if (! table.data().any() ) {
                    jQuery.unblockUI();
                    jQuery("#tabs").show('slow');
                    $("#tblS").show('slow');
                }else{
                    jQuery.unblockUI();
                    jQuery("#tabs").show('slow');
                    $("#tblS").show('slow');
                }
            });
             table.ajax.reload(null,false); 
         });
            jQuery("#filter_pencarian_kul").hide();
    // table_kul = $('#data-lulusan-kul').DataTable({ 
    //     "processing": true, 
    //     "serverSide": true,
    //     "order": [], 
    //     "ajax": {
    //         "url": $BASE_URL+ "lulusan/getDataKul",
    //         "type": "POST",
    //         "data": function ( data ) {
    //             data.tahun_keluar = $('#tahun_keluar_kul').val();
    //             data.jns_kel = $('#jns_kel_kul').val();
    //         }
    //     },
    //     "columnDefs": [{ 
    //         "targets": [ 0 ],
    //         "orderable": false, 
    //     },
    //     ],
    // });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table_kul.ajax.reload(null,false); 
    });
    jQuery("#tahun_keluar_kul").change(function(){
        table_kul.ajax.reload(null,false); 
    });
    jQuery("#jurusan_kul").change(function(){
        table_kul.ajax.reload(null,false); 
    });
    $('#btn-reset').click(function(){
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false); 
    });
    jQuery("#filter_pencarian_wira").hide();
    // table_wira = $('#data-lulusan-wira').DataTable({ 
    //     "processing": true, 
    //     "serverSide": true,
    //     "order": [], 
    //     "ajax": {
    //         "url": $BASE_URL+ "lulusan/getDataWira",
    //         "type": "POST",
    //         "data": function ( data ) {
    //             data.tahun_keluar = $('#tahun_keluar_wira').val();
    //             data.jns_kel = $('#jns_kel_wira').val();
    //         }
    //     },
    //     "columnDefs": [{ 
    //         "targets": [ 0 ],
    //         "orderable": false, 
    //     },
    //     ],
    // });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $('#btn-filter').click(function(){ 
        table_wira.ajax.reload(null,false); 
    });
    jQuery("#tahun_keluar_wira").change(function(){
        table_wira.ajax.reload(null,false); 
    });
    jQuery("#jurusan_wira").change(function(){
        table_wira.ajax.reload(null,false); 
    });
});
function filter(){
    jQuery("#filter_pencarian").show("slow");
}
function filter_kul(){
    jQuery("#filter_pencarian_kul").show("slow");
}
function filter_wira(){
    jQuery("#filter_pencarian_wira").show("slow");
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function reload_table_kul(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="tahun_keluar_kul"]').select2("val", "");
        jQuery('[name="jurusan_kul"]').select2("val", "");
        jQuery("#filter_pencarian_kul").hide("slow");
        table_kul.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function reload_table_wira(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        jQuery('[name="tahun_keluar_wira"]').select2("val", "");
        jQuery('[name="jurusan_wira"]').select2("val", "");
        jQuery("#filter_pencarian_wira").hide("slow");
        table_wira.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
function edit_status(nisn){
    jQuery("#bekerja_").hide('slow');
    jQuery("#kuliah_").hide('slow');
    jQuery("#wira_").hide('slow');
    jQuery("#tombol").hide('slow');
    jQuery("#id").val(nisn);
    $('#modal_form').modal('show');
    $('.modal-title').text('Tetapkan Status Lulusan');
}
function save(page,link,formid,mdlid){
    $('#btnSave').text('saving...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    var status_siswax = jQuery("#status_siswax").val();
    if(status_siswax=='1'){
        url = $BASE_URL+ page+"/save_kerja"; 
    }else if(status_siswax=='2'){
        url = $BASE_URL+ page+"/save_kuliah"; 
    }else{
        url = $BASE_URL+ page+"/save_wira"; 
    }
    var formData = new FormData($(formid)[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
            if(data.status ==  true ) {
                $('#modal_form').modal('hide');
                $.notify({icon:'fas fa-thumbs-o-up',message:"Data berhasil diubah ✔"},{type:"success",offset:{x:3,y:2}});
                table.ajax.reload(null,false);
                // table_kul.ajax.reload(null,false);
                // table_wira.ajax.reload(null,false);
            }else if(data.status == false ){
                for (var i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }else{
                $.notify({icon:'fas fa-info',message:"Data sudah tersedia ❌"},{type:"danger",offset:{x:3,y:2}});
            }
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 

        },
        error: function (jqXHR, textStatus, errorThrown){
            $.notify({icon:'fas fa-info',message:"Terjadi Kesalahan ❌"},{type:"danger",offset:{x:3,y:2}});
            $('#btnSave').text('save'); 
            $('#btnSave').attr('disabled',false); 
        }
    });
}
function lihat_status(id){
    bootbox.confirm("Verifikasi Status Lulusan Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                jQuery.post($BASE_URL+"lulusan/ubah_status_kul/"+id, jQuery("#form1").serialize(),
                    function(data){
                        $.unblockUI();
                        reload_table();
                    });
            }, 500);
        }
    });
}
function ubah_status_wira(id){
    bootbox.confirm("Verifikasi Status Lulusan Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                jQuery.post($BASE_URL+"lulusan/ubah_status_wira/"+id, jQuery("#form1").serialize(),
                    function(data){
                        $.unblockUI();
                        reload_table();
                    });
            }, 500);
        }
    });
}
function ubah_status_kerja(id){
    bootbox.confirm("Verifikasi Status Lulusan Berikut ?", function(result) {
        if (result) {
            setTimeout(function () {
                jQuery.blockUI({
                    css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: 2, 
                        color: '#fff' 
                    },
                    message : 'Mohon menunggu ... '
                });
                jQuery.post($BASE_URL+"lulusan/ubah_status_wira/"+id, jQuery("#form1").serialize(),
                    function(data){
                        $.unblockUI();
                        reload_table();
                    });
            }, 500);
        }
    });
}