var host = window.location.host;
$BASE_URL = 'http://'+host+'/';

function daftar(){
    var nama = $("#nama").val();
    var nisn = $("#nisn").val();
    var tgl = $("#tgllahir").val();
    var email = $("#email").val();
    var sekolah = $("#sekolah :selected").val();
    if (nama=="") {
        jQuery("#nama").effect('shake', '1500').attr('placeholder', 'Masukan nama Anda');
    }else if (nisn=="") {
        jQuery("#nisn").effect('shake', '1500').attr('placeholder', 'Masukan NISN Anda');
    }else if (tgl=="") {
        jQuery("#tgllahir").effect('shake', '1500');
        $.notify({icon:'fa fa-info',message:"Masukan tanggal lahir Anda"},{type:"danger",offset:{x:3,y:2}});
    }else if (email=="") {
        jQuery("#email").effect('shake', '1500').attr('placeholder', 'Masukan email Anda');
    }else if (sekolah == ""){
       $("#sekolah").effect('shake', '1500');
    } else {
        $.ajax({
            url: $BASE_URL + "login/register",
            method: "POST",
            dataType: "json",
            data : $("#fregister").serialize(),
            beforeSend: function(){
                $('#mdlinfo').modal('show');
            },
            success: function(data){
                $('#mdlinfo').modal('hide');
                var data = $.parseJSON(JSON.stringify(data));
                alert(data.pesan);
            },
            error: function(){
                $('#mdlinfo').modal('hide');
		$.notify({icon:'fa fa-info',message:"Data tidak ditemukan, periksa kembali data Anda."},{type:"danger",offset:{x:3,y:2}});
            }
        });
    }
}

$(document).ready(function(){
        $("#prov").change(function(){
            var idprov = $("#prov").val();
            $.ajax({
                    url:"login/getkota",
                    method:"POST",
                    data:{idpr:idprov},
                    cache: false,
                    success:function(data)
                    {
                      $("#kotax").html(data);
                      $("#kotax").selectpicker('refresh');
                    }
            })
        });
         $("#kotax").change(function(){
            var id = $("#kotax").val();
            $.ajax({
                    url:"login/getsekolah",
                    method:"POST",
                    data:{idpr:id},
                    cache: false,
                    success:function(data)
                    {
                      $("#sekolah").html(data);
                      $("#sekolah").selectpicker('refresh');
                    }
            })
        });
    });

function login(){
    var username = $("#username").val(), password=$("#password").val();
    if(username!='' && password!=''){
        $.ajax({
            method: "POST",
            url: $BASE_URL + "login/doLogin",
            datType: "json",
            data : $("#flogin").serialize(),
            beforeSend: function(){
                $('#mdlinfo').modal('show');
            },
            success: function(data){
                var data = $.parseJSON(data);
                $('#mdlinfo').modal('hide');
                if(data.response=='true'){
                    $.notify({icon:'fa fa-check',message:"Mohon menunggu, Anda sedang dialihkan"},{type:"success",offset:{x:3,y:2}});
                    window.location.reload();
                } else {
                    $.notify({icon:'fa fa-info',message:"Silahkan periksa kembali Username & Password Anda"},{type:"danger",offset:{x:3,y:2}});
                }

            },
            error: function(){
                $('#mdlinfo').modal('hide');
		$.notify({icon:'fa fa-info',message:"Terjadi kesalahan. Ulangi proses"},{type:"danger",offset:{x:3,y:2}});
            }
        });
    } else if(username=='') {
        jQuery("#username").effect('shake', '1500').attr('placeholder', 'username tidak boleh kosong');
    }else{
        jQuery("#password").effect('shake', '1500').attr('placeholder', 'password tidak boleh kosong');
    }
}

function doKonfir(){
    var email = jQuery("#emailKonfir").val();
    var uname = jQuery("#unameKonfir").val();
    var jns = jQuery("#jns").val();
    if (uname == "") {
        jQuery("#unameKonfir").effect('shake', '1500').attr('placeholder', 'Isi dengan Nomor Induk Siswa Nasional Anda');
    }else if (email == "") {
        jQuery("#emailKonfir").effect('shake', '1500').attr('placeholder', 'Isi dengan email yang Anda pakai saat mendaftar');
    }else if (jns == "") {
            $.notify({icon:'fa fa-info',message:"Pilih jenis pengguna."},{type:"danger",offset:{x:3,y:2}});
    } else {
        jQuery.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 2,
                color: '#fff'
            },
            message: 'Memulai pengecekan <br/> silahkan tunggu ...'
        });
        jQuery.ajax({
            url: $BASE_URL + 'forgot/do_konfir',
            data: {
                txtemailkon: email,
                txtunamekon: uname,
                txtjenis: jns
            },
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                if (data.response =='sip') {
                    jQuery.unblockUI();
                    $("#sipDlg").dialog({
                        modal: true,
                        buttons: {
                            Tutup: function(){
                                window.location = $BASE_URL+'login';
                            }
                        }
                    });
                }else if (data.response == 'ups') {
                    jQuery.unblockUI();
                    $.notify({icon:'fa fa-info',message:"Username/email tidak dikenali, mohon cek kembali."},{type:"danger",offset:{x:3,y:2}});                  
                }else if (data.response == 'no') {
                    $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, pastikan pilihan pengguna benar."},{type:"danger",offset:{x:3,y:2}});
                } else {
                    jQuery.unblockUI();
                    $.notify({icon:'fa fa-info',message:"Terjadi kesalahan! Silahkan ulangi proses."},{type:"danger",offset:{x:3,y:2}});
                    jQuery("#emailkonfir").val('');
                    jQuery("#unamekonfir").val('');
                    jQuery("#jns").val('');
                }
            }
        });
    }
}
