var host = window.location.host;
$BASE_URL = 'http://' + host + '/';

function cekPass() {
    var iduser = jQuery("#iduser").val();
    var pass1 = jQuery("#pw1").val();
    var pass2 = jQuery("#pw2").val();
    if (pass1 == "") {
        jQuery("#pw1").effect('shake', '1500').attr('placeholder', 'Isi Password baru !!');
    } else if (pass2 != pass1) {
        jQuery("#pw2").effect('shake', '1500').attr('placeholder', 'Password tidak sama !!');
    } else {
        
        jQuery.ajax({
            url: $BASE_URL + 'forgot/proses_gantipass',
            data: {
                iduser : iduser,
                txtpass1: pass1,
                txtpass2: pass2
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                // jQuery.unblockUI();
                if (data.response=='true') {
                    alert("Password berhasil diganti !!");
                    window.location = $BASE_URL+'login';
                    setTimeout(function() {
                        // jQuery.unblockUI();
                    }, 50);
                } else {
                    alert("Ups, terjadi kesalahan :(");
                    jQuery("#pass1").val('');
                    jQuery("#pass2").val('');
                }
            }
        });
    }
}

