  $(document).ready(function(){
            var limit = 5;
            var start = 0;
            var action = 'inactive';

            function loadlow(limit) {
                var output = "";
                for(var count=0; count<limit; count++){
                    output += '<div class="post_data">';
                    output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
                    output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
                    output += '</div>';
                }
                $('#load_data_message').html(output);
            }

            loadlow(limit);

            function loadMoreData(limit, start){
                 $.ajax({
                    url:"<?php echo base_url(); ?>frontpage/loadMore",
                    method:"POST",
                    data:{limit:limit, start:start},
                    cache: false,
                    success:function(data)
                    {
                      if(data == '')
                      {
                        $('#load_data_message').html('<h3>Tidak ada data lagi</h3>');
                        action = 'active';
                      }
                      else
                      {
                        $('#load_lowbkk').append(data);
                        $('#load_data_message').html("");
                        action = 'inactive';
                      }
                    }
                  })
            }

            if(action == 'inactive')
            {
              action = 'active';
              loadMoreData(limit, start);
            }

            $(window).scroll(function(){
              if($(window).scrollTop() + $(window).height() > $("#load_lowbkk").height() && action == 'inactive')
              {
                loadlow(limit);
                action = 'active';
                start = start + limit;
                setTimeout(function(){
                  loadMoreData(limit, start);
                }, 1000);
              }
            });

        });