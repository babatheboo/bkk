function rubah(id,status){
  var r = confirm("Rubah status pekerjaan ini ?");
  var resp = "";
  if(r == true ){
    if(status==0){
      $("#kdjob").val(id);
      dialogrk.dialog( "open" );
    } else {
      $.ajax({
       url:"profile/rubahStatus",
       method:"POST",
       data:{id:id, status:status},
       cache: false,
       success:function(data)
       {
        resp = $.parseJSON(data);
        if(resp=="ok"){
          $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
          dialogrk.dialog( "close" );
          location.reload();
        } else {
          $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
        }
      }
    });
    }
  }
}

function rubahw(id,status){
  var r = confirm("Rubah status wirausaha ini ?");
  var resp = "";
  if(r == true ){
    if(status==0){
      $("#kdwir").val(id);
      dialogrw.dialog( "open" );
    } else {
      $.ajax({
       url:"profile/rubahStatusW",
       method:"POST",
       data:{id:id, status:status},
       cache: false,
       success:function(data)
       {
        resp = $.parseJSON(data);
        if(resp=="ok"){
          $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
          dialogrw.dialog( "close" );
          location.reload();
        } else {
          $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
          dialogrw.dialog( "close" );
        }
      }
    });
    }
  }
}

function rubahk(id,status){
  var r = confirm("Rubah status kuliah ini ?");
  var resp = "";
  if(r == true ){
    if(status==0){
      $("#kdkul").val(id);
      dialogrkr.dialog( "open" );
    } else {
      $.ajax({
       url:"profile/rubahStatusK",
       method:"POST",
       data:{id:id, status:status},
       cache: false,
       success:function(data)
       {
        resp = $.parseJSON(data);
        if(resp=="ok"){
          $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
          dialogrkr.dialog( "close" );
          location.reload();
        } else {
          $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
          dialogrkr.dialog( "close" );
        }
      }
    });
    }
  }
}

function simpanRbh(){
  var resp = "";
  $.ajax({
   url:"profile/rbhStatus",
   method:"POST",
   data: $("#fmRbhKerja").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
      dialogrk.dialog("close");
      location.reload();
    } else {
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialogrk.dialog("close");
    }
  }
});
}

function simpanRbhW(){
  var resp = "";
  $.ajax({
   url:"profile/rbhStatusW",
   method:"POST",
   data: $("#fmRbhWira").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
      dialogrw.dialog( "close" );
      location.reload();
    } else {
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialogrw.dialog( "close" );
    }
  }
});
}

function simpanRbhK(){
  var resp = "";
  $.ajax({
   url:"profile/rbhStatusK",
   method:"POST",
   data: $("#fmRbhKul").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $.notify({icon:'fa fa-check',message:"Status berhasil dirubah"},{type:"success",offset:{x:3,y:66}});
      dialogrkr.dialog( "close" );
      location.reload();
    } else {
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, perubahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialogrkr.dialog( "close" );
    }
  }
});
}

function simpanEmail(){
  var eml = $("#email").val();
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (eml=='') {
    jQuery("#email").effect('shake', '1500').attr('placeholder', 'Masukan email baru Anda');
  }else if (!filter.test(email.value)) {
    $.notify({icon:'fa fa-info',message:"Masukan format email yang sesuai"},{type:"danger",offset:{x:3,y:66}});
  }else{
    var resp = "";
    $.ajax({
     url:"profile/updateEmail",
     method:"POST",
     data: $("#fmEmail").serialize(),
     cache: false,
     success:function(data)
     {
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Email berhasil di simpan"},{type:"success",offset:{x:3,y:66}});
        dialogemail.dialog("close");
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Maaf, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
        dialogemail.dialog("close");
      }
    }
  });
  }
}


function addKerja(){
  var resp = "";
  $.ajax({
   url:"profile/addKerja",
   method:"POST",
   data: $("#fmAddKerja").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $('#fmAddKerja').each(function(){
        this.reset();
      });
      $.notify({icon:'fa fa-check',message:"Penambahan data kerja berhasil"},{type:"success",offset:{x:3,y:66}});
      dialog.dialog( "close" );
      location.reload();
    } else {
      $('#fmAddKerja')[0].reset();
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, penambahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialog.dialog( "close" );
    }
  }
});
}

function addWira(){
  var resp = "";
  $.ajax({
   url:"profile/addWira",
   method:"POST",
   data: $("#fmAddWira").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $.notify({icon:'fa fa-check',message:"Penambahan wira berhasil"},{type:"success",offset:{x:3,y:66}});
      dialogw.dialog( "close" );
      location.reload();
    } else {
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, penambahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialogw.dialog( "close" );
    }
  }
});
}

function addKul(){
  var resp = "";
  $.ajax({
   url:"profile/addKul",
   method:"POST",
   data: $("#fmAddKul").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      $.notify({icon:'fa fa-check',message:"Penambahan kuliah berhasil"},{type:"success",offset:{x:3,y:66}});
      dialogk.dialog( "close" );
      location.reload();
    } else {
      alert("Perubahan gagal");
      $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, penambahan gagal"},{type:"danger",offset:{x:3,y:66}});
      dialogk.dialog("close");
    }
  }
});
}

function edKerja(){
  dialog.dialog("open");
}

function edWira(){
  dialogw.dialog( "open" );
}

function edKuliah(){
  dialogk.dialog( "open" );
}

function gimage(){
  dialogimg.dialog( "open" );
}

function eEmail(){
  dialogemail.dialog( "open" );
}



$(document).ready(function(){
 $('#fmAddKerja').each(function(){
  this.reset();
});

 $('#fmAddWira').each(function(){
  this.reset();
});

 $('#fmAddKul').each(function(){
  this.reset();
});

 $('#fmRbhKerja').each(function(){
  this.reset();
});

 $('#fmRbhWira').each(function(){
  this.reset();
});

 $('#fmAddKul').each(function(){
  this.reset();
});



 dialog = $( "#mAddKerja" ).dialog({
  autoOpen: false,
  height: 450,
  width: 450,
  modal: true
});

 dialogw = $( "#mAddWira" ).dialog({
  autoOpen: false,
  height: 450,
  width: 450,
  modal: true
});

 dialogk = $( "#mAddKul" ).dialog({
  autoOpen: false,
  height: 450,
  width: 450,
  modal: true
});

 dialogrk = $( "#mRbhKerja" ).dialog({
  autoOpen: false,
  height: 200,
  width: 450,
  modal: true
});

 dialogrw = $( "#mRbhWira" ).dialog({
  autoOpen: false,
  height: 200,
  width: 450,
  modal: true
});

 dialogrkr = $( "#mRbhKul" ).dialog({
  autoOpen: false,
  height: 200,
  width: 450,
  modal: true
});

 dialogimg = $( "#mImg" ).dialog({
  autoOpen: false,
  height: 200,
  width: 450,
  modal: true
});


 dialogemail= $( "#mEmail" ).dialog({
  autoOpen: false,
  height: 200,
  width: 450,
  modal: true
});

 dialogeditKer= $( "#mEditKer" ).dialog({
  autoOpen: false,
  height: 500,
  width: 450,
  modal: true
});

 dialogeditWir= $( "#mEditWir" ).dialog({
  autoOpen: false,
  height: 500,
  width: 450,
  modal: true
});
});

function cek_kerja(id){
  $.ajax({
    url : 'profile/cekKerja/'+id,
    'type':'post',
    success:function(data){
      var data = $.parseJSON(data);
      $('#idK').val(data.id);
      $('#kePosisi').val(data.posisi);
      $('#keMulai').val(data.mulai);
      $('#keAkhir').val(data.akhir);
      $('#keStatus').val(data.status);
      $('#keRange').val(data.range);
      $('#keBidang').val(data.bidang);
      $('#keLokasi').val(data.lokasi);
      dialogeditKer.dialog('open');
    },
    error: function() {
      alert("TERJADI KESALAHAN");
    }
  });
}

function editKerja(){
  var id = $("#idK").val();
  var pos = $("#kePosisi").val();
  var tglM = $("#keMulai").val();
  var tglT = $("#keAkhir").val();
  var sts = $("#keStatus").val();
  var range = $("#keRange").val();
  var bidang = $("#keBidang").val();
  var lokasi = $("#keLokasi").val();
  var resp = "";
  if (pos=="") {
    jQuery("#kePosisi").effect('shake', '1500').attr('placeholder', 'Masukan posisi kerja');
  }else if (tglM=="") {
    jQuery("#keMulai").effect('shake', '1500').attr('placeholder', 'Masukan tanggal masuk kerja');
  }else if (tglT=="") {
    jQuery("#keAkhir").effect('shake', '1500').attr('placeholder', 'Masukan tanggal selesai kerja');
  }else if (sts=="") {
    jQuery("#keStatus").effect('shake', '1500').attr('placeholder', 'Masukan status kerja');
  }else if (range=="") {
    jQuery("#keRange").effect('shake', '1500').attr('placeholder', 'Masukan penghasilan perbulan');
  }else if (bidang=="") {
    jQuery("#keBidang").effect('shake', '1500').attr('placeholder', 'Masukan bidang usaha');
  }else if (lokasi=="") {
    jQuery("#keLokasi").effect('shake', '1500').attr('placeholder', 'Masukan penempatan kerja');
  }else{
  // $("#sv").click(function(){
    $.ajax({
     url:"profile/editKerja/"+id,
     method:"POST",
     data: $("#fmEditKer").serialize(),
     cache: false,
     success:function(data){
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-checl',message:"Pengeditan kerja berhasil"},{type:"success",offset:{x:250,y:50}});
        dialogeditKer.dialog("close");
        location.reload();
      }else{
        $.notify({icon:'fa fa-info',message:"Terjadi kesalahan, Pengeditan gagal"},{type:"danger",offset:{x:3,y:66}});
        dialogeditKer.dialog("close");
      }
    }
  });
  }
}

function cek_wira(id){
  $.ajax({
    url : 'profile/cekWira/'+id,
    'type':'post',
    success:function(data){
      var data    =   $.parseJSON(data);
      $('#idW').val(data.idW);
      $('#wiPosisi').val(data.posisiW);
      $('#wiMulai').val(data.mulaiW);
      $('#wiAkhir').val(data.akhirW);
      $('#wiStatus').val(data.statusW);
      $('#wiRange').val(data.rangeW);
      dialogeditWir.dialog('open');
    },
    error: function() {
      alert("TERJADI KESALAHAN");
    }
  }); 
}

function editWira(){
  var id = $("#idW").val();
  var posisi = $("#wiPosisi").val();
  var mulai = $("#wiMulai").val();
  var akhir = $("#wiAkhir").val();
  var status = $("#wiStatus").val();
  var range = $("#wiRange").val();
  var resp = "";
  if (posisi=="") {
    jQuery("#wiPosisi").effect('shake', '1500').attr('placeholder', 'Masukan wirausaha Anda');
  }else if (mulai=="") {
    jQuery("#wiMulai").effect('shake', '1500').attr('placeholder', 'Masukan tanggal mulai wirausaha');
  }else if (akhir=="") {
    jQuery("#wiAkhir").effect('shake', '1500').attr('placeholder', 'Masukan akhir wirausaha');
  }else if (status=="") {
    jQuery("#wiStatus").effect('shake', '1500').attr('placeholder', 'Masukan status wirausaha Anda');
  }else if (range=="") {
    jQuery("#wiRange").effect('shake', '1500').attr('placeholder', 'Masukan pendapatan perbulan Anda');
  }else{
    $.ajax({
     url:"profile/editWira/"+id,
     method:"POST",
     data: $("#fmEditWir").serialize(),
     cache: false,
     success:function(data){
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Pengeditan wirausaha berhasil"},{type:"success",offset:{x:3,y:66}});
        dialogeditWir.dialog("close");
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Terjadi kesalaha, Pengeditan gagal"},{type:"danger",offset:{x:3,y:66}});
        dialogeditWir.dialog("close");
        location.reload();
      }
    }
  });
  }
}

function hpsKerja(id){
  var r = confirm("Hapus pekerjaan ini ?");
  var resp = "";
  if(r == true ){
    $.ajax({
     url:"profile/hpsKerja/"+id,
     method:"POST",
     cache: false,
     success:function(data)
     {
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Riwayat berhasil dihapus"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Penghapusan gagal, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
        location.reload();
      }
    }
  });
  }
}


function hpsWira(id){
  var r = confirm("Hapus wirausaha ini ?");
  var resp = "";
  if(r == true ){
    $.ajax({
     url:"profile/hpsWira/"+id,
     method:"POST",
     cache: false,
     success:function(data)
     {
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Riwayat berhasil dihapus"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Penghapusan gagal, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
        location.reload();
      }
    }
  });
  }
}


function hpsKuliah(id){
  var r = confirm("Hapus riwayat kuliah ini ?");
  var resp = "";
  if(r == true ){
    $.ajax({
     url:"profile/hpsKuliah/"+id,
     method:"POST",
     cache: false,
     success:function(data)
     {
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Riwayat berhasil dihapus"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      } else {
        $.notify({icon:'fa fa-info',message:"Penghapusan gagal, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
        location.reload();
      }
    }
  });
  }
}

function hpsLamaran(id){
  var r = confirm("Hapus lamaran ini ?");
  var resp = "";
  if(r == true ){
    $.ajax({
     url:"profile/hpsLamaran/"+id,
     method:"POST",
     cache: false,
     success:function(data){
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Lamaran berhasil dihapus"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      }else{
        $.notify({icon:'fa fa-info',message:"Penghapusan gagal, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
      }
    }
  });
  }
}

function hpsInbox(id){
  var r = confirm("Hapus pesan ini ?");
  var resp = "";
  if(r == true ){
    $.ajax({
     url:"profile/hpsInbox/"+id,
     method:"POST",
     cache: false,
     success:function(data){
      resp = $.parseJSON(data);
      if(resp=="ok"){
        $.notify({icon:'fa fa-check',message:"Pesan berhasil dihapus"},{type:"success",offset:{x:3,y:66}});
        location.reload();
      }else{
        $.notify({icon:'fa fa-info',message:"Penghapusan gagal, terjadi kesalahan"},{type:"danger",offset:{x:3,y:66}});
      }
    }
  });
  }
}

function rbhpass(){
  $.ajax({
   url:"profile/rbhPass/",
   method:"POST",
   data: $("#fpass").serialize(),
   cache: false,
   success:function(data)
   {
    resp = $.parseJSON(data);
    if(resp=="ok"){
      alert("Perubahan berhasil");
      window.location.href = "../login/doLogout";
    } else if (resp=="not") {
      alert("Password lama salah");
      location.reload();
    } else if (resp=="nok") {
      alert("Perubahan gagal");
      location.reload();
    } else {
      alert("Data tidak boleh kososng");
    }
  }
});
}