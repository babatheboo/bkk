
$(window).on("load",function() {
    "use strict";
    $("#loading").fadeOut()
}), $(document).ready(function() {
    $(".wpb-mobile-menu").slicknav({
        prependTo: ".navbar-header",
        parentTag: "jobs",
        allowParentLinks: !0,
        duplicate: !0,
        label: "",
        closedSymbol: '<i class="fa fa-angle-right"></i>',
        openedSymbol: '<i class="fa fa-angle-down"></i>'
    })
}), $(".nav > li:has(ul)").addClass("drop"), $(".nav > li.drop > ul").addClass("dropdown"), $(".nav > li.drop > ul.dropdown ul").addClass("sup-dropdown"), $(document).ready(function() {
    jQuery(".tp-banner").show().revolution({
        dottedOverlay: "none",
        delay: 9e3,
        startwidth: 1170,
        startheight: 540,
        hideThumbs: 200,
        thumbWidth: 100,
        thumbHeight: 50,
        thumbAmount: 5,
        navigationType: "bullet",
        navigationArrows: "solo",
        navigationStyle: "preview3",
        touchenabled: "on",
        onHoverStop: "on",
        swipe_velocity: .7,
        swipe_min_touches: 1,
        swipe_max_touches: 1,
        drag_block_vertical: !1,
        parallax: "mouse",
        parallaxBgFreeze: "on",
        parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],
        keyboardNavigation: "off",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 0,
        navigationVOffset: 20,
        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,
        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,
        shadow: 0,
        fullWidth: "on",
        fullScreen: "off",
        spinner: "spinner1",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        forceFullWidth: "off",
        hideThumbsOnMobile: "off",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "off",
        hideArrowsOnMobile: "off",
        hideThumbsUnderResolution: 0,
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        startWithSlide: 0,
        fullScreenOffsetContainer: ""
    })
});


var offset = 200,
    duration = 500;

function cari() {
    var b = $("#bidang").val(),
        i = $("#keyword").val(),
        t = $("#bkk").val();
    if (b == '' && i == '' && t=='') {
        alert('masukan kata pencarian');
    }else{
        $.ajax({
        method: "POST",
        url: "frontpage/search",
        data: $("#fsearch").serialize(),
        success: function(e) {
            $("#isi").replaceWith(e)
        }
    });
    }
}
$(window).scroll(function() {
    $(this).scrollTop() > offset ? $(".back-to-top").fadeIn(400) : $(".back-to-top").fadeOut(400)
}), $(".back-to-top").click(function(e) {
    return e.preventDefault(), $("html, body").animate({
        scrollTop: 0
    }, 600), !1
}), $(".list,switchToGrid").click(function(e) {
    e.preventDefault(), $(".grid").removeClass("active"), $(".list").addClass("active"), $(".item-list").addClass("make-list"), $(".item-list").removeClass("make-grid"), $(".item-list").removeClass("make-compact"), $(".item-list .add-desc-box").removeClass("col-sm-9"), $(".item-list .add-desc-box").addClass("col-sm-7")
}), $(".grid").click(function(e) {
    e.preventDefault(), $(".list").removeClass("active"), $(this).addClass("active"), $(".item-list").addClass("make-grid"), $(".item-list").removeClass("make-list"), $(".item-list").removeClass("make-compact"), $(".item-list .add-desc-box").removeClass("col-sm-9"), $(".item-list .add-desc-box").addClass("col-sm-7")
}), jQuery(document).ready(function(e) {
    var i = e(".cd-user-modal"),
        t = i.find("#cd-login"),
        o = i.find("#cd-signup"),
        a = i.find("#cd-reset-password"),
        l = e(".cd-switcher"),
        s = l.children("li").eq(0).children("a"),
        n = l.children("li").eq(1).children("a"),
        r = t.find(".cd-form-bottom-message a"),
        d = a.find(".cd-form-bottom-message a");

    function c() {
        t.addClass("is-selected"), o.removeClass("is-selected"), a.removeClass("is-selected"), s.addClass("selected"), n.removeClass("selected")
    }
    l.on("click", function(i) {
        i.preventDefault(), e(i.target).is(s) ? c() : (t.removeClass("is-selected"), o.addClass("is-selected"), a.removeClass("is-selected"), s.removeClass("selected"), n.addClass("selected"))
    }), r.on("click", function(e) {
        e.preventDefault(), t.removeClass("is-selected"), o.removeClass("is-selected"), a.addClass("is-selected")
    }), d.on("click", function(e) {
        e.preventDefault(), c()
    })
});
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i)
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i)
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i)
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i)
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i)
    },
    any: function() {
        return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()
    }
};

function helpdesk() {
    isMobile.any() ? window.location.href = "whatsapp://send?phone=628119252424" : window.open("https://web.whatsapp.com/send?phone=628119252424", "_blank")
}


