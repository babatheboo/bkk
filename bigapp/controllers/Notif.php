<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Notif extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }

  public function index(){
	echo "wik wik";
  }

  public function getinbox($sid){
   $d = $this->db->query("select distinct a.id, a.peserta_didik_id, b.judul,b.tgl, b.keterangan, b.tgl_mulai, b.tgl_selesai, b.alamat, c.prov, d.kota ,  f.nama
FROM inbox a join jadwal_tes b on a.jadwal_id = b.id join ref.view_wilayah c on b.kode_prov=c.kode_prov join ref.view_wilayah d on b.kode_kota=d.kode_kota
join lowongan e on e.id = b.id_loker join ref.sekolah f on f.sekolah_id=e.sekolah_pembuat WHERE a.peserta_didik_id='$sid'")->result();
   $response = array();
   foreach($d as $key){
	$alm = $key->alamat . ", " . $key->kota . ", " . $key->prov;
	$dt = array("id"=>$key->id, "dari"=>$key->nama,"judul"=>$key->judul,"ket"=>trim($key->keterangan),"tanggal"=>date('d-m-Y', strtotime($key->tgl)),"tmulai"=>date('d-m-Y', 
strtotime($key->tgl_mulai)), "tselesai"=>date('d-m-Y', strtotime($key->tgl_selesai)),"alamat"=>$alm);
	array_push($response, $dt);
   }
	$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
      	exit;
  }

 public function hapus(){
	$id = $this->input->post('id');
	if($this->db->delete('inbox', array('id'=>$id))){
  		$response["error"] = FALSE;
                $response["error_msg"] = "Pesan berhasil dihapus";

	} else {
  		$response["error"] = TRUE;
                $response["error_msg"] = "Pesan gagal dihapus";
	}
   $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
        exit;
}

}
