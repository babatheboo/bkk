<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Restapi extends REST_Controller{

  	public function __construct(){
    	parent::__construct();
  	}		

	public function index_get(){
		$low = $this->db->query("SELECT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() and tujuan='9' order BY tgl_tutup limit 10 offset 0")->result();
		$response['status']=200;
	    $response['error']=false;
	    $response['person']=$low;
	    $this->response($response);
	}
}
