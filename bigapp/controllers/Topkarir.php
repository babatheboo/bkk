<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Topkarir extends REST_Controller{

  	public function __construct(){
    	parent::__construct();
  	}		

	public function index(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
  		CURLOPT_HEADER => false,
  		CURLOPT_URL => 'https://smk:smkxtkid@www.topkarir.com/api/jobs/kerjasmk',
  		CURLOPT_RETURNTRANSFER => true,
  		CURLOPT_ENCODING => "",
  		CURLOPT_MAXREDIRS => 10,
  		CURLOPT_TIMEOUT => 30,
  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  		CURLOPT_CUSTOMREQUEST => "GET",
  		CURLOPT_HTTPHEADER => array("X-API-KEY: tkidxsmk","Accept: application/json"
  		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$hasil = json_decode($response,true);
		foreach ($hasil as $key => $value) {
			$data[]  =   array('perusahaan'=>$value['company_name'],'link'=>$value['permalink'],'judul'=>$value['job_title'],'tgl_akhir'=>$value['end']);
		}

    		$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($data, JSON_PRETTY_PRINT))->_display();
      		exit;
	}
}

