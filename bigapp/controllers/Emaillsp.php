<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emaillsp extends CI_Controller {
	public function __construct(){
		parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
  }

  public function genPass($length, $count, $characters){
    $symbols = array();
    $passwords = array();
    $used_symbols = '';
    $pass = '';
    $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
    $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $symbols["numbers"] = '1234567890';
    $symbols["special_symbols"] = '!?~@#-_+<>[]{}';
    $characters = explode(",",$characters); // get characters types to be used for the passsword
    foreach ($characters as $key=>$value) {
        $used_symbols .= $symbols[$value]; // build a string with all characters
      }
    $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
    for ($p = 0; $p < $count; $p++) {
      $pass = '';
      for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
          }
          $passwords[] = $pass;
        }
    return $passwords; // return the generated password
  }


  public function okeDaftar($email){
    $config = array(
      'protocol' => 'sendmail',
      'smtp_host' => 'smtp.gmail.com',
      'smtp_port' => 465,
      'smtp_user' => 'subdit.lasjurin01@gmail.com',
      'smtp_pass' => 'psmk.lasjurin01',
      'smtp_timeout' => '4',
      'mailtype' => 'html',
      'charset' => 'iso-8859-1'
    );
    $this->email->initialize($config);
    $this->email->from('no-reply@aksilspsmk', 'Link Aktifasi');
    $this->email->to($email);
    $this->email->subject('Link Aktifasi Akun');
    $message = '<html>
    Klik di sini
    </html>';
    $this->email->message($message);
    $this->email->send();
  }
}