<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowbkk extends CI_Controller {

	 public function __construct(){
      		parent::__construct();
  	}


        public function index()
        {
	 $this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
	$low = $this->db->query("SELECT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() and tujuan='9' order BY tgl_tutup limit 10 offset 0")->result();
                $response = array();
                foreach($low as $key) {
       $dt = array('judul'=>$key->judul, 'deskripsi'=>$key->deskripsi,'foto'=>$key->logo,'id'=>$key->id,'nama'=>$key->nama, 
'tgl'=>$key->tgl_tutup,"bidang"=>$key->nama_bidang_usaha);
                    array_push($response, $dt);
                }

	$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
      		exit;

        }

	public function byid($id){
		$lok = "";
		$kp = "";
		$per = "";
		$butuh = "";
		$jk = "";
		$tb = "";
		$bb = "";
		$um = "";
		$gj = "";
		$gaji= "";

	 	$this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
 $low = $this->db->query("SELECT distinct kode_perusahaan, gaji, judul,logo,id,nama,tgl_tutup, deskripsi, nama_bidang_usaha, tgl_buat, bidang_usaha_id, tujuan 
FROM mview_lowongan_frontpage WHERE id='$id' limit 1")->result();

                $response = array();
                foreach($low as $key) {
 			$kp= $key->kode_perusahaan;
 			$gj =  $key->gaji;
			$kt = $this->db->query("SELECT distinct a.prov FROM ref.view_provinsi a left join lowongan_lokasi b on a.kode_prov=b.kode_prov WHERE b.id_loker='$id'")->result();
        		if(count($kt)>0){
                	   foreach ($kt as $keys) {
                        	$lok = $keys->prov;
                	   }
        	        }

	$ind = $this->db->query("SELECT distinct nama FROM ref.industri where industri_id='$kp'")->result();
	foreach ($ind as $keys) {
		$per =  $keys->nama;
	}

$kl = $this->db->query("SELECT * FROM lowongan_syarat WHERE id_loker='$id'")->result();
if (count($kl)>0){
	foreach ($kl as $keya ) {
		$butuh = $keya->dibutuhkan;
		$jk =  $keya->jns_kel;
		$tb =  $keya->tinggi_badan ;
		$bb = $keya->berat_badan ;
		$um = $keya->umur;
	}
}

$g = $this->db->query("SELECT * from ref.range_gaji WHERE id='$gj'")->result();
	if(count($g)>0){
		foreach ($g as $keys) {
			$gaji =  $keys->range;
		}
	}

 $dt = array('judul'=>$key->judul,'deskripsi'=>$key->deskripsi,'foto'=>$key->logo,'id'=>$key->id,'nama'=>$key->nama,'tgl'=>date('d-m-Y', 
strtotime($key->tgl_tutup)),'bidang'=>$key->nama_bidang_usaha,'tglbuat'=>date('d-m-Y', strtotime($key->tgl_buat)),'lokasi'=>$lok,'perusahaan'=>$per, 
'butuh'=>$butuh,'jk'=>$jk,'tb'=>$tb,'bb'=>$tb,'um'=>$um, 'gaji'=>$gaji);

                    array_push($response, $dt);
                }


    $this->output->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;


	}

public function psmk(){
		$lpsmk = $this->db->query("SELECT * FROM lowongan_psmk WHERE  tgl_tutup>=now() AND status='1' order BY tgl_tutup ")->result();
		$response = array();
		if(count($lpsmk)>0){
			foreach ($lpsmk as $key ) {
		$dt = array("judul"=>$key->judul, "deskripsi"=>$key->deskripsi, "foto"=>$key->gambar,"tgl"=>date('d-m-Y', strtotime($key->tgl_tutup)));
			  array_push($response, $dt);
		}

		}

		
    $this->output->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;

	}

	public function lamar(){
		$idjob = $this->input->post('idjob');
		$idsiswa =  $this->input->post('idsiswa');
		$tgl = date('Y-m-d');
		$ck = $this->db->query("SELECT id_siswa FROM bidding_lulusan WHERE id_loker='$idjob' and id_siswa='$idsiswa'")->result();
if(count($ck)>0){
		$response["error"] = TRUE;
		$response["error_msg"] = "Anda suda melamar pada pekerjaan ini";
} else {
	$data = array('id_siswa'=>$idsiswa,'tgl_bid'=>$tgl,'status'=>"0",'id_loker'=>$idjob);
	if($this->db->insert('bidding_lulusan', $data)){
		$response["error"] = FALSE;
                $response["error_msg"] = "Lamaran telah dimasukan !";
	} else {
		$response["error"] = TRUE;
                $response["error_msg"] = "Gagal memasukan lamaran";

	}
}

 $this->output->set_status_header(200)
      ->set_content_type('application/json', 'utf-8')
      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
      ->_display();
      exit;



	}

}

