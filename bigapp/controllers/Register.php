<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends CI_Controller {

  public function __construct(){
    parent::__construct();
  }

  public function index(){
   echo "Get out";
 }


 public function register(){
   $nisn = $this->input->get('nisn');
   $nama = strtoupper($this->input->get('nama'));
   $email = $this->input->get('email');
   $ttl = date('Y-m-d', strtotime($this->input->get('ttl')));
   $dt = $this->db->query("select * From ref.peserta_didik where nisn='$nisn' and nama LIKE '%$nama%'")->result();
   if(count($dt)>0){
    foreach ($dt as $key) {
      $tgl = date('Y-m-d', strtotime($key->tanggal_lahir));
      if($tgl == $ttl){

        $userid = $key->peserta_didik_id;
        if($key->email == NULL ) {
          $dem = array('email'=>$email);
          $this->db->where('nisn', $nisn);
          $this->db->update('ref.peserta_didik', $dem);
        }

        $cek = $this->db->query("SELECT * FROM app.username WHERE username='$nisn'")->result();
        if(count($cek)>0){
				// generate password
          $pass = $this->genPass(8,1,"lower_case,upper_case,numbers");
          $pas =  md5(md5($pass[0]));
          $paskirim = $pass[0];
          $data = array('password'=>$pas);
          $this->db->where('username', $nisn);
          if($this->db->update('app.username',$data)){
            $response["error"] = FALSE;
            $response["error_msg"] = "Pendaftaran berhasil, silahkan cek email untuk informasi login anda !";

          } else {
           $response["error"] = TRUE;
           $response["error_msg"] = "Pendaftaran gagal, cek data yang anda masukan !";
         }
       } else {
         $pass = $this->genPass(8,1,"lower_case,upper_case,numbers");
         $pas =  md5(md5($pass[0]));
         $paskirim = $pass[0];
         $data = array('user_id'=>$userid, 'username'=>$nisn, 'password'=>$pas,'level'=>'3','login'=>'1');
         if($this->db->insert('app.username', $data)){
           $response["error"] = FALSE;
           $response["error_msg"] = "Pendaftaran berhasil, silahkan cek email untuk informasi login anda !";

         } else {
          $response["error"] = TRUE;
          $response["error_msg"] = "Pendaftaran gagal, cek data yang anda masukan !";
        }
      }
    } else {
      $response["error"] = TRUE;
      $response["error_msg"] = "Pendaftaran gagal, cek data yang anda masukan !";
    }

  }
} else {
	/* cari data di dapodik, jika ditemukan, download data, masukan ke ref.peserta_didik, buat username dan password, kirim email terdaftar informasi login */
//	   echo "tidak ditemukan";
 $response["error"] = TRUE;
 $response["error_msg"] = "Pendaftaran gagal, cek data yang anda masukan !";
}
$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
exit;

}


public function genPass($length, $count, $characters){
  $symbols = array();
  $passwords = array();
  $used_symbols = '';
  $pass = '';
  $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
  $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $symbols["numbers"] = '1234567890';
  $symbols["special_symbols"] = '!?~@#-_+<>[]{}';
    $characters = explode(",",$characters); // get characters types to be used for the passsword
    foreach ($characters as $key=>$value) {
        $used_symbols .= $symbols[$value]; // build a string with all characters
      }
    $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
    for ($p = 0; $p < $count; $p++) {
      $pass = '';
      for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
          }
          $passwords[] = $pass;
        }
    return $passwords; // return the generated password
  }


}
