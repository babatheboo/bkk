<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kuliah extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }

  public function index(){
		echo "Uhuyyy";
	}

	public function getkuliah($nisn){
        $kerja = $this->db->query("SELECT * FROM kuliah_siswa WHERE nisn='$nisn'")->result();
        $n=0;
        $response = array();
        if (count($kerja)>0){
                foreach($kerja as $key){
                $n++;
                if($key->akhir_kuliah == null || $key->akhir_kuliah==""){
                        $sampai = "Sekarang";
                } else {
                        $sampai = date('d-m-Y', strtotime($key->akhir_kuliah));
                }
                if($key->ver_status=="2"){
                        $ver = "Terverifikasi";
                } else {
                        $ver = "Belum diverifikasi";
                }

                if($key->status=="0"){
                        $status = "Tidak aktif";
                } else {
                        $status = "Aktif";
                }
$dt = array('no'=>$n,'perusahaan'=>$key->nama_pt,'jurusan'=>$key->jurusan,'mulai'=>date('d-m-Y',
strtotime($key->mulai_kuliah)),'sampai'=>$sampai,'ver'=>$ver,'id'=>$key->id,'status'=>$status);
        array_push($response, $dt);
            }
        }
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
                exit;

	}


 public function addKuliah(){
                $nisn = $this->input->post('nisn');
                $perusahaan = $this->input->post('perusahaan');
                $posisi = $this->input->post('posisi');
                $mulai = date('Y-m-d', strtotime($this->input->post('mulai_kerja')));
                $sisid = $this->input->post('sisid');
                $gaji = $this->input->post('gaji');
        $data = array('nisn'=>$nisn,'nama_pt'=>$perusahaan,'jurusan'=>$posisi,'mulai_kuliah'=>$mulai,'peserta_didik_id'=>$sisid,'ver_status'=>'0','status'=>'1');
        if($this->db->insert('kuliah_siswa',$data)){
                $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil disimpan";
        } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Penambahan data gagal";
        }
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
                exit;
        }


	 public function update(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');

        if($status=="1"){
          $data = array("status"=>"1","akhir_kuliah"=>NULL);
        } else {
          $tgl = date('Y-m-d');
           $data = array("status"=>"0","akhir_kuliah"=>$tgl);
        }

        $this->db->where("id", $id);
        if($this->db->update("kuliah_siswa", $data)){
                $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil diupdate !";
        } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Gagal update data !";
        }


        }




        public function delkuliah($id){
            if($this->db->delete('kuliah_siswa', array('id'=>$id))){
                 $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil dihapus !";
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Gagal menghapus data !";

            }
        }


}
