<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '0');

class Jurusan extends CI_Controller {
	
	public function __construct(){
  		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->db1 = $this->load->database('mssql', TRUE);
		$this->load->library('bcrypt');
 	}

	public function siswa($offset) {
		$data = $this->db->query("SELECT * FROM ref.peserta_didik order by peserta_didik_id asc LIMIT 10000 OFFSET $offset")->result();
		foreach($data as $key) {
		    if(strlen($key->nisn)==10){
			$pd = $key->peserta_didik_id;
			$uname = $key->nisn;
			$dd = $this->db->query("SELECT username FROM app.username WHERE user_id='$pd'")->result();
		  	if (count($dd)==0) {
				$din = array('user_id'=>$pd,'username'=>$uname,'password'=>'23f86cc2c670b465198a4da285e5e890','level'=>'3','login'=>'1','status'=>'1');
//		$this->db->insert('app.username',$din);
	if ($this->db->insert('app.username',$din)) {
				   echo "sukses" . "<br/>";
				} else {
					echo "gagal<br/>";
				} 
		    	}
		    }
		}
	}
	public function cekms($offset)
 	{
 		$datasiswa = $this->db1->query("SELECT * FROM ref.peserta_didik WHERE LEFT(tanggal_keluar,4)='2018' ORDER BY peserta_didik_id ASC OFFSET $offset ROWS FETCH NEXT 1000 ROWS ONLY")->result();
 		foreach ($datasiswa as $key) {
 			$pdid = $key->peserta_didik_id;
 			$nama = $key->nama;
 			$jenkel = $key->jenis_kelamin;
 			$nisn = $key->nisn;
 			$nik = $key->nik;
 			$temlah = $key->tempat_lahir;
 			$taglah = $key->tanggal_lahir;
 			$relig = $key->agama_id;
 			$kebhus = $key->kebutuhan_khusus_id;
 			$skl = $key->sekolah_id;
 			$jns_kel = $key->jenis_keluar_id;
 			$tgl_masuk = $key->tanggal_masuk_sekolah;
 			$tgl_keluar = $key->tanggal_keluar;
 			$jur = $key->jurusan_id;
 			$dd = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$pdid'")->result();
 			if (count($dd)==0){
 				$data = array('peserta_didik_id'=>$pdid, 'nama'=>$nama, 'jenis_kelamin'=>$jenkel, 'nisn'=>$nisn, 'nik'=>$nik, 'tempat_lahir'=>$temlah, 'agama_id'=>$relig, 'kebutuhan_khusus_id'=>$kebhus, 'sekolah_id'=>$skl, 'jenis_keluar_id'=>$jns_kel, 'tanggal_masuk_sekolah'=>$tgl_masuk, 'tanggal_keluar'=>$tgl_keluar, 'jurusan_id'=>$jur);
 				if($this->db->insert('ref.peserta_didik',$data)){
					echo "sukses" . "<br/>";
				}else{
					echo "gagal" . "<br/>";
				}
 			} else {
				echo "Data sudah ada" . "<br/>";
			}
 		}

 	}

 	public function sink($prov)
 	{

	    $dp = $this->db->query("select * from ref.view_sekolah_wilayah where kode_prov='$prov'")->result();
 		if(!empty($dp))
 		{
 			foreach ($dp as $row) 
 			{
 				$sid = trim($row->sekolah_id) ;
 				$dataS = $this->getJurusan($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
			        {
			        	$jip = trim($dataS[$a]['jurusan_sp_id']);
			            $cek = $this->db->query("SELECT jurusan_sp_id FROM ref.jurusan_sp WHERE jurusan_sp_id='$jip'")->result();
			            if(count($cek)==0)
			            {			        	
				            $dataInsert = array(
				              'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
				              'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
				              'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
				              'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
				              'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
				            );
				          	    $this->db->insert('ref.jurusan_sp',$dataInsert);
								unset($dataInsert);
			            }
			        }
					unset($dataInsert);			        
					unset($dataS);
				}
 			}
 		}
 	}

 	function getJurusan($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

}
