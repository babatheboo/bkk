<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '0');
class Sinkron extends CI_Controller {
	
	public function __construct(){
  		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->load->library('bcrypt');
 	}

 	public function sink($prov)
 	{

	    $dp = $this->db->query("select * from ref.view_sekolah_wilayah where kode_prov='$prov'")->result();
 		if(!empty($dp))
 		{
 			foreach ($dp as $row) 
 			{
 				$sid = trim($row->sekolah_id) ;
 				$data = $this->GetPeserta($sid);
		 		if(!empty($data))
		 		{
		 			foreach ($data as $key) 
		 			{
		 				$id = trim($key['peserta_didik_id']);
		 				$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
		          		if(count($cek)==0)
		          		{
			 				$b = $key['nama'];
				            $c = $key['jenis_kelamin'];
				            $d = $key['nisn'];
				            $e = $key['nik'];
				            $f = $key['tempat_lahir'];
				            $g = $key['tanggal_lahir'];
				            $h = $key['agama_id'];
				            $i = $key['kebutuhan_khusus_id'];
				            $j = $key['sekolah_id'];
				            $k = $key['jenis_keluar_id'];
				            $l = $key['tanggal_masuk_sekolah'];
				            $m = $key['tanggal_keluar'];
				            $n = $key['jurusan_id'];

				            $b = trim($b);
				            $c = trim($c);
				            $d = trim($d);
				            $e = trim($e);
				            $f = trim($f);
				            $g = trim($g);
				            $h = trim($h);
				            $i = trim($i);
				            $j = trim($j);
				            $k = trim($k);
				            $l = trim($l);
				            $m = trim($m);
				            $n = trim($n);

			 				$dataInsert = array(
					              'peserta_didik_id' =>$id,
					              'nama' =>$b,
					              'jenis_kelamin' =>$c,
					              'nisn' =>$d,
					              'nik' =>$e,
					              'tempat_lahir' =>$f,
					              'tanggal_lahir' =>$g,
					              'agama_id' =>$h,
					              'kebutuhan_khusus_id' =>$i,
					              'sekolah_id'=>$j,
					              'jenis_keluar_id'=>$k,
					              'tanggal_masuk_sekolah' =>$l,
					              'tanggal_keluar'=>$m,
					              'jurusan_id'=>$n
					        );
					        $this->db->insert('ref.peserta_didik',$dataInsert);
			 			}


						$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
						if(count($cek)==0)
						{
							$di = array(
							  'peserta_didik_id' =>$id,
							  'alamat_jalan' =>trim($key['alamat_jalan']),
				              'rt' =>trim($key['rt']),
				              'rw' =>trim($key['rw']),
				              'nama_dusun' =>trim($key['nama_dusun']),
				              'desa_kelurahan' =>trim($key['desa_kelurahan']),
				              'kode_wilayah' =>trim($key['kode_wilayah']),
				              'kode_pos' =>trim($key['kode_pos']),
				              'nomor_telepon_rumah' =>trim($key['nomor_telepon_rumah']),
				              'nomor_telepon_seluler' =>trim($key['nomor_telepon_seluler']),
				              'email' =>trim($key['email']),
				              'nik_ayah' =>trim($key['nik_ayah']),
				              'nik_ibu' =>trim($key['nik_ibu']),
				              'penerima_KPS' =>trim($key['penerima_KPS']),
				              'no_KPS' =>trim($key['no_KPS']),
				              'layak_PIP' =>trim($key['layak_PIP']),
				              'penerima_KIP' =>trim($key['penerima_KIP']),
				              'no_KIP'=>trim($key['no_KIP']),
				              'nm_KIP' =>trim($key['nm_KIP']),
				              'no_KKS' =>trim($key['no_KKS']),
				              'id_layak_pip' =>trim($key['id_layak_pip']),
				              'nama_ayah' =>trim($key['nama_ayah']),
				              'tahun_lahir_ayah' =>trim($key['tahun_lahir_ayah']),
				              'jenjang_pendidikan_ayah' =>trim($key['jenjang_pendidikan_ayah']),
				              'pekerjaan_id_ayah' =>trim($key['pekerjaan_id_ayah']),
				              'penghasilan_id_ayah' =>trim($key['penghasilan_id_ayah']),
				              'nama_ibu_kandung' =>trim($key['nama_ibu_kandung']),
				              'tahun_lahir_ibu' =>trim($key['tahun_lahir_ibu']),
				              'jenjang_pendidikan_ibu' =>trim($key['jenjang_pendidikan_ibu']),
				              'penghasilan_id_ibu' =>trim($key['penghasilan_id_ibu']),
				              'pekerjaan_id_ibu' =>trim($key['pekerjaan_id_ibu'])
								);
							$this->db->insert('ref.peserta_didik_detil',$di);
						}

						$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'")->result();
		          		if(count($cek)==0)
		          		{
							echo "Insert to username " . "<br/>";
							$pass = '$2a$08$W/P/II1U5H7MaLGHA5YwgeON13AI7XJq9A7cV.o9apzHiU2fosFb2';
							$s_username = array('user_id'=>$id,
								'username'=>$id,
								'password'=>$pass,
								'level'=>'3',
								'login'=>'1');
							
								$this->db->insert('app.username',$s_username);
		          		}
		 			}
		 		}
 			}
 		}
 	}

 	function GetPeserta($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=bkk&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

}