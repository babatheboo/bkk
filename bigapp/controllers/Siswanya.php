<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '0');
class Siswanya extends CI_Controller {
	
public function __construct()
{
	parent::__construct();
	$this->db = $this->load->database('default', TRUE);
	$this->load->library('bcrypt');
}

public function get($sid)
{
	$filePeserta = "logpeserta.txt";
	$fileRombel = "logrombel.txt";
	$fileARombel = "logarombel.txt";
	$handle = fopen($filePeserta, 'a') or die('Cannot open file:  '.$filePeserta);
	$handle1 = fopen($fileARombel, 'a') or die('Cannot open file:  '.$fileARombel);
	$handle2 = fopen($fileRombel, 'a') or die('Cannot open file:  '.$fileRombel);
	$data = $this->GetPeserta($sid);
	if(!empty($data))
	{
		foreach ($data as $key) 
		{
			$id = trim($key['peserta_didik_id']);
			$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
  			if(count($cek)==0)
  			{
				$b = $key['nama'];
	            $c = $key['jenis_kelamin'];
	            $d = $key['nisn'];
	            $e = $key['nik'];
	            $f = $key['tempat_lahir'];
	            $g = $key['tanggal_lahir'];
	            $h = $key['agama_id'];
	            $i = $key['kebutuhan_khusus_id'];
	            $j = $key['sekolah_id'];
	            $k = $key['jenis_keluar_id'];
	            $l = $key['tanggal_masuk_sekolah'];
	            $m = $key['tanggal_keluar'];
	            $n = $key['jurusan_id'];

	            $b = trim($b);
	            $c = trim($c);
	            $d = trim($d);
	            $e = trim($e);
	            $f = trim($f);
	            $g = trim($g);
	            $h = trim($h);
	            $i = trim($i);
	            $j = trim($j);
	            $k = trim($k);
	            $l = trim($l);
	            $m = trim($m);
	            $n = trim($n);

				$dataInsert = array(
	              'peserta_didik_id' =>$id,
	              'nama' =>$b,
	              'jenis_kelamin' =>$c,
	              'nisn' =>$d,
	              'nik' =>$e,
	              'tempat_lahir' =>$f,
	              'tanggal_lahir' =>$g,
	              'agama_id' =>$h,
	              'kebutuhan_khusus_id' =>$i,
	              'sekolah_id'=>$j,
	              'jenis_keluar_id'=>$k,
	              'tanggal_masuk_sekolah' =>$l,
	              'tanggal_keluar'=>$m,
	              'jurusan_id'=>$n
	        	);
	        	$this->db->insert('ref.peserta_didik',$dataInsert);
			}
			else
			{
	            $d = $key['nisn'];
				$e = $key['nik'];
	            $g = $key['tanggal_lahir'];
	            $j = $key['sekolah_id'];
	            $k = $key['jenis_keluar_id'];
	            $m = $key['tanggal_keluar'];
	            $d = trim($d);
	            $e = trim($e);
	            $g = trim($g);
	            $j = trim($j);
	            $k = trim($k);
	            $m = trim($m);
	            $dataInsert = array(
	              'nisn' =>$d,
	              'nik' =>$e,
	              'tanggal_lahir' =>$g,
	              'sekolah_id'=>$j,
	              'jenis_keluar_id'=>$k,
	              'tanggal_keluar'=>$m,
	        	);
	        	$this->db->where('peserta_didik_id',$id);
	        	$this->db->update('ref.peserta_didik',$dataInsert);
			}


			$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
			if(count($cek)==0)
			{
				$di = array(
				  'peserta_didik_id' =>$id,
				  'alamat_jalan' =>trim($key['alamat_jalan']),
	              'rt' =>trim($key['rt']),
	              'rw' =>trim($key['rw']),
	              'nama_dusun' =>trim($key['nama_dusun']),
	              'desa_kelurahan' =>trim($key['desa_kelurahan']),
	              'kode_wilayah' =>trim($key['kode_wilayah']),
	              'kode_pos' =>trim($key['kode_pos']),
	              'nomor_telepon_rumah' =>trim($key['nomor_telepon_rumah']),
	              'nomor_telepon_seluler' =>trim($key['nomor_telepon_seluler']),
	              'email' =>trim($key['email']),
	              'nik_ayah' =>trim($key['nik_ayah']),
	              'nik_ibu' =>trim($key['nik_ibu']),
	              'penerima_KPS' =>trim($key['penerima_KPS']),
	              'no_KPS' =>trim($key['no_KPS']),
	              'layak_PIP' =>trim($key['layak_PIP']),
	              'penerima_KIP' =>trim($key['penerima_KIP']),
	              'no_KIP'=>trim($key['no_KIP']),
	              'nm_KIP' =>trim($key['nm_KIP']),
	              'no_KKS' =>trim($key['no_KKS']),
	              'id_layak_pip' =>trim($key['id_layak_pip']),
	              'nama_ayah' =>trim($key['nama_ayah']),
	              'tahun_lahir_ayah' =>trim($key['tahun_lahir_ayah']),
	              'jenjang_pendidikan_ayah' =>trim($key['jenjang_pendidikan_ayah']),
	              'pekerjaan_id_ayah' =>trim($key['pekerjaan_id_ayah']),
	              'penghasilan_id_ayah' =>trim($key['penghasilan_id_ayah']),
	              'nama_ibu_kandung' =>trim($key['nama_ibu_kandung']),
	              'tahun_lahir_ibu' =>trim($key['tahun_lahir_ibu']),
	              'jenjang_pendidikan_ibu' =>trim($key['jenjang_pendidikan_ibu']),
	              'penghasilan_id_ibu' =>trim($key['penghasilan_id_ibu']),
	              'pekerjaan_id_ibu' =>trim($key['pekerjaan_id_ibu'])
					);
				$this->db->insert('ref.peserta_didik_detil',$di);
			}

			$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'")->result();
	  		if(count($cek)==0)
	  		{
				$pass = '$2a$08$W/P/II1U5H7MaLGHA5YwgeON13AI7XJq9A7cV.o9apzHiU2fosFb2';
				$s_username = array('user_id'=>$id,
					'username'=>$id,
					'password'=>$pass,
					'level'=>'3',
					'login'=>'1');	
					$this->db->insert('app.username',$s_username);
	  		}
		}
	}
	else
	{
		fwrite($handle, "Gagal =>" . $sid . "\n");
	}

	unset($data);
	$dataS = $this->GetARombel($sid);
	  if(!empty($dataS))
	  {
	  	for($a=0;$a<count($dataS);$a++)
		{
	  	  $ari = $dataS[$a]['anggota_rombel_id'];
	  	  if($ari=="")
	  	  {
	  	  	$ari = NULL;
	  	  }
	  	  else
	  	  {
	  	  	$ari=trim($dataS[$a]['anggota_rombel_id']);
	  	  }
	  	  $rid = $dataS[$a]['rombongan_belajar_id'];
	  	  if($rid=="")
	  	  {
	  	  	$rid = NULL;
	  	  }
	  	  else
	  	  {
	  	  	$rid = trim($dataS[$a]['rombongan_belajar_id']);
	  	  }
	  	  $pid = $dataS[$a]['peserta_didik_id'];
	  	  if($pid=="")
	  	  {
	  	  	$pid = NULL;
	  	  }
	  	  else
	  	  {
	  	  	$pid = trim($dataS[$a]['peserta_didik_id']);
	  	  }
	  	  $jid = $dataS[$a]['jenis_pendaftaran_id'];
	  	  if($jid=="")
	  	  {
	  	  	$jid = NULL;
	  	  }
	  	  else
	  	  {
	  	  	$jid = trim($dataS[$a]['jenis_pendaftaran_id']);
	  	  }
	      $cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
	      if(count($cek)==0)
	      {
	      	$dataInsert = array(
	          'anggota_rombel_id'=>$ari,
	          'rombongan_belajar_id'=>$rid,
	          'peserta_didik_id'=>$pid,
	          'jenis_pendaftaran_id'=>$jid
	        );
	      	$this->db->insert('ref.anggota_rombel',$dataInsert);
	      }
	  	}
	  }
	  else
	  {
	  		fwrite($handle1, "Gagal =>" . $sid . "\n");
	  }

	unset($dataS);

	$dataS = $this->GetRombel($sid);
	  if(!empty($dataS))
	  {
	  	  for($a=0;$a<count($dataS);$a++)
	        {
        		$jid = trim($dataS[$a]['rombongan_belajar_id']);
        		if(trim($dataS[$a]['jurusan_sp_id'])=="")
				{
					$jip  = NULL;
				}
				else
				{
					$jip = trim($dataS[$a]['jurusan_sp_id']);
				}

				$smid  = $dataS[$a]['semester_id'];
				if($smid=="")
				{
					$smid = "";
				}
				else
				{
					$smid = trim($dataS[$a]['semester_id']);
				}

				$tpid = $dataS[$a]['tingkat_pendidikan_id'] ;
				if($tpid=="")
				{
					$tpid="0";
				}
				else
				{
					$tpid=$dataS[$a]['tingkat_pendidikan_id'];
				}
				$kid = $dataS[$a]['kebutuhan_khusus_id'];
				if($kid=="")
				{
					$kid = 0;
				}
				else
				{
					$kid = strval($dataS[$a]['kebutuhan_khusus_id']);
				}
				$q = $this->db->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$jid'")->result();
				if(count($q)==0)
				{
					$dataInsert = array(
		              'rombongan_belajar_id' =>$jid,
		              'semester_id'=>$smid,
		              'sekolah_id' =>$sid,
		              'tingkat_pendidikan_id' =>$tpid,
		              'jurusan_sp_id'=>$jip,
		              'nama'=>trim($dataS[$a]['nama']),
		              'kebutuhan_khusus_id'=>$kid
	            	);

					$this->db->insert('ref.rombongan_belajar',$dataInsert);
				}	
	        }
	  }
	  else
	  {
	  		fwrite($handle1, "Gagal =>" . $sid . "\n");
	  }	
}

public function getp()
{
	$filePeserta = "logpeserta.txt";
	$fileRombel = "logrombel.txt";
	$fileARombel = "logarombel.txt";
	$handle = fopen($filePeserta, 'a') or die('Cannot open file:  '.$filePeserta);
	$handle1 = fopen($fileARombel, 'a') or die('Cannot open file:  '.$fileARombel);
	$handle2 = fopen($fileRombel, 'a') or die('Cannot open file:  '.$fileRombel);

	$sql = "SELECT * FROM ref.sekolah";
	$data = $this->db->query($sql)->result();
	foreach ($data as $key) 
	{
		$sid = trim($key->sekolah_id);
	
		$data = $this->GetPeserta($sid);
		if(!empty($data))
		{
			foreach ($data as $key) 
			{
				$id = trim($key['peserta_didik_id']);
				$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
	  			if(count($cek)==0)
	  			{
					$b = $key['nama'];
		            $c = $key['jenis_kelamin'];
		            $d = $key['nisn'];
		            $e = $key['nik'];
		            $f = $key['tempat_lahir'];
		            $g = $key['tanggal_lahir'];
		            $h = $key['agama_id'];
		            $i = $key['kebutuhan_khusus_id'];
		            $j = $key['sekolah_id'];
		            $k = $key['jenis_keluar_id'];
		            $l = $key['tanggal_masuk_sekolah'];
		            $m = $key['tanggal_keluar'];
		            $n = $key['jurusan_id'];

		            $b = trim($b);
		            $c = trim($c);
		            $d = trim($d);
		            $e = trim($e);
		            $f = trim($f);
		            $g = trim($g);
		            $h = trim($h);
		            $i = trim($i);
		            $j = trim($j);
		            $k = trim($k);
		            $l = trim($l);
		            $m = trim($m);
		            $n = trim($n);

					$dataInsert = array(
		              'peserta_didik_id' =>$id,
		              'nama' =>$b,
		              'jenis_kelamin' =>$c,
		              'nisn' =>$d,
		              'nik' =>$e,
		              'tempat_lahir' =>$f,
		              'tanggal_lahir' =>$g,
		              'agama_id' =>$h,
		              'kebutuhan_khusus_id' =>$i,
		              'sekolah_id'=>$j,
		              'jenis_keluar_id'=>$k,
		              'tanggal_masuk_sekolah' =>$l,
		              'tanggal_keluar'=>$m,
		              'jurusan_id'=>$n
		        	);
		        	$this->db->insert('ref.peserta_didik',$dataInsert);
				}
				else
				{
		            $d = $key['nisn'];
					$e = $key['nik'];
		            $g = $key['tanggal_lahir'];
		            $j = $key['sekolah_id'];
		            $k = $key['jenis_keluar_id'];
		            $m = $key['tanggal_keluar'];
		            $d = trim($d);
		            $e = trim($e);
		            $g = trim($g);
		            $j = trim($j);
		            $k = trim($k);
		            $m = trim($m);
		            $dataInsert = array(
		              'nisn' =>$d,
		              'nik' =>$e,
		              'tanggal_lahir' =>$g,
		              'sekolah_id'=>$j,
		              'jenis_keluar_id'=>$k,
		              'tanggal_keluar'=>$m,
		        	);
		        	$this->db->where('peserta_didik_id',$id);
		        	$this->db->update('ref.peserta_didik',$dataInsert);
				}


				$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
				if(count($cek)==0)
				{
					$di = array(
					  'peserta_didik_id' =>$id,
					  'alamat_jalan' =>trim($key['alamat_jalan']),
		              'rt' =>trim($key['rt']),
		              'rw' =>trim($key['rw']),
		              'nama_dusun' =>trim($key['nama_dusun']),
		              'desa_kelurahan' =>trim($key['desa_kelurahan']),
		              'kode_wilayah' =>trim($key['kode_wilayah']),
		              'kode_pos' =>trim($key['kode_pos']),
		              'nomor_telepon_rumah' =>trim($key['nomor_telepon_rumah']),
		              'nomor_telepon_seluler' =>trim($key['nomor_telepon_seluler']),
		              'email' =>trim($key['email']),
		              'nik_ayah' =>trim($key['nik_ayah']),
		              'nik_ibu' =>trim($key['nik_ibu']),
		              'penerima_KPS' =>trim($key['penerima_KPS']),
		              'no_KPS' =>trim($key['no_KPS']),
		              'layak_PIP' =>trim($key['layak_PIP']),
		              'penerima_KIP' =>trim($key['penerima_KIP']),
		              'no_KIP'=>trim($key['no_KIP']),
		              'nm_KIP' =>trim($key['nm_KIP']),
		              'no_KKS' =>trim($key['no_KKS']),
		              'id_layak_pip' =>trim($key['id_layak_pip']),
		              'nama_ayah' =>trim($key['nama_ayah']),
		              'tahun_lahir_ayah' =>trim($key['tahun_lahir_ayah']),
		              'jenjang_pendidikan_ayah' =>trim($key['jenjang_pendidikan_ayah']),
		              'pekerjaan_id_ayah' =>trim($key['pekerjaan_id_ayah']),
		              'penghasilan_id_ayah' =>trim($key['penghasilan_id_ayah']),
		              'nama_ibu_kandung' =>trim($key['nama_ibu_kandung']),
		              'tahun_lahir_ibu' =>trim($key['tahun_lahir_ibu']),
		              'jenjang_pendidikan_ibu' =>trim($key['jenjang_pendidikan_ibu']),
		              'penghasilan_id_ibu' =>trim($key['penghasilan_id_ibu']),
		              'pekerjaan_id_ibu' =>trim($key['pekerjaan_id_ibu'])
						);
					$this->db->insert('ref.peserta_didik_detil',$di);
				}

				$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$id' and username = '$d' ")->result();
		  		if(empty($cek))
		  		{
					$pass = md5(md5('siswa'));
					$s_username = array('user_id'=>$id,
						'username'=>$d,
						'password'=>$pass,
						'level'=>'3',
						'login'=>'1');	
						$this->db->insert('app.username',$s_username);
		  		}
			}
		}
		else
		{
			fwrite($handle, "Gagal =>" . $sid . "\n");
		}

		unset($data);
		$dataS = $this->GetARombel($sid);
		  if(!empty($dataS))
		  {
		  	for($a=0;$a<count($dataS);$a++)
			{
		  	  $ari = $dataS[$a]['anggota_rombel_id'];
		  	  if($ari=="")
		  	  {
		  	  	$ari = NULL;
		  	  }
		  	  else
		  	  {
		  	  	$ari=trim($dataS[$a]['anggota_rombel_id']);
		  	  }
		  	  $rid = $dataS[$a]['rombongan_belajar_id'];
		  	  if($rid=="")
		  	  {
		  	  	$rid = NULL;
		  	  }
		  	  else
		  	  {
		  	  	$rid = trim($dataS[$a]['rombongan_belajar_id']);
		  	  }
		  	  $pid = $dataS[$a]['peserta_didik_id'];
		  	  if($pid=="")
		  	  {
		  	  	$pid = NULL;
		  	  }
		  	  else
		  	  {
		  	  	$pid = trim($dataS[$a]['peserta_didik_id']);
		  	  }
		  	  $jid = $dataS[$a]['jenis_pendaftaran_id'];
		  	  if($jid=="")
		  	  {
		  	  	$jid = NULL;
		  	  }
		  	  else
		  	  {
		  	  	$jid = trim($dataS[$a]['jenis_pendaftaran_id']);
		  	  }
		      $cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
		      if(count($cek)==0)
		      {
		      	$dataInsert = array(
		          'anggota_rombel_id'=>$ari,
		          'rombongan_belajar_id'=>$rid,
		          'peserta_didik_id'=>$pid,
		          'jenis_pendaftaran_id'=>$jid
		        );
		      	$this->db->insert('ref.anggota_rombel',$dataInsert);
		      }
		  	}
		  }
		  else
		  {
		  		fwrite($handle1, "Gagal =>" . $sid . "\n");
		  }

		unset($dataS);

		$dataS = $this->GetRombel($sid);
		  if(!empty($dataS))
		  {
		  	  for($a=0;$a<count($dataS);$a++)
		        {
	        		$jid = trim($dataS[$a]['rombongan_belajar_id']);
	        		if(trim($dataS[$a]['jurusan_sp_id'])=="")
					{
						$jip  = NULL;
					}
					else
					{
						$jip = trim($dataS[$a]['jurusan_sp_id']);
					}

					$smid  = $dataS[$a]['semester_id'];
					if($smid=="")
					{
						$smid = "";
					}
					else
					{
						$smid = trim($dataS[$a]['semester_id']);
					}

					$tpid = $dataS[$a]['tingkat_pendidikan_id'] ;
					if($tpid=="")
					{
						$tpid="0";
					}
					else
					{
						$tpid=$dataS[$a]['tingkat_pendidikan_id'];
					}
					$kid = $dataS[$a]['kebutuhan_khusus_id'];
					if($kid=="")
					{
						$kid = 0;
					}
					else
					{
						$kid = strval($dataS[$a]['kebutuhan_khusus_id']);
					}
					$q = $this->db->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$jid'")->result();
					if(count($q)==0)
					{
						$dataInsert = array(
			              'rombongan_belajar_id' =>$jid,
			              'semester_id'=>$smid,
			              'sekolah_id' =>$sid,
			              'tingkat_pendidikan_id' =>$tpid,
			              'jurusan_sp_id'=>$jip,
			              'nama'=>trim($dataS[$a]['nama']),
			              'kebutuhan_khusus_id'=>$kid
		            	);

						$this->db->insert('ref.rombongan_belajar',$dataInsert);
					}	
		        }
		  }
		  else
		  {
		  		fwrite($handle1, "Gagal =>" . $sid . "\n");
		  }	

	}
}


function GetPeserta($sid)
{
	$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=bkk&sekolah_id=$sid";
	$ch=curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$cexecute=curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	if($httpcode==200)
	{
		$data = json_decode($cexecute,true);
	}
	return $data;
}

function GetArombel($sid)
{
	$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=$sid";
	$ch=curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$cexecute=curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	if($httpcode==200)
	{
		$data = json_decode($cexecute,true);
	}
	return $data;
}

function GetRombel($sid)
{
	$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id=$sid";
	$ch=curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$cexecute=curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	if($httpcode==200)
	{
		$data = json_decode($cexecute,true);
	}
	return $data;
}



}
