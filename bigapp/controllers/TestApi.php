<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestApi extends CI_Controller {


 	public function __construct(){
      	parent::__construct();
  	}


	public function index(){
		$username = $this->input->post('username');
		$password = md5(md5($this->input->post('password')));
		$ceklogin = $this->db->get_where('app.username',array('username'=>$username,'password'=>$password,'login'=>'1'));
		if(count($ceklogin->result())>0){
			$rows 		= $ceklogin->row();
			$level 		= $rows->level;
			$userid 	= $rows->user_id;
			if($level=="3"){
				$userid_ 	= $rows->username;
				$ckdata 	= $this->db->query("SELECT a.nama as nama_siswa,a.peserta_didik_id,a.sekolah_id,b.nama as nama_sekolah, a.email, a.photo, c.logo  FROM ref.peserta_didik a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id JOIN sekolah_terdaftar c on a.sekolah_id=c.sekolah_id WHERE b.npsn = '12345678' LIMIT 1");
				$row 		= $ckdata->row();
				$nama 		= $row->nama_siswa;
				$userid 	= $row->peserta_didik_id;
				$nama_ 		= $row->nama_sekolah;
				$sekolah_id = $row->sekolah_id;
				$fotosiswa = $row->photo;
				$email = $row->email;
				$logo = $row->logo;
				
				$response['error'] = FALSE;
                $response['user']['level'] = $level;
                $response['user']['user_id'] = $userid;
                $response['user']['username'] = $username;
                $response['user']['namasekolah'] = $nama_;
                $response['user']['sekolah_id'] = $sekolah_id;
                $response['user']['nama']  = $nama;		
                $response['user']['email'] = $email;
                $response['user']['fotosiswa'] = $fotosiswa;		
                $response['user']['logobkk'] = $logo;
			}
		} else {
			$response["error"] = TRUE;
			$response["error_msg"] = "Username tidak ditemukan";
		}
			$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
      		exit;
	}
}
