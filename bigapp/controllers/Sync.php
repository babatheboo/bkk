<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->load->library('bcrypt');
	}


	public function get($prov)
	{
		$sid = "";
		$npsn = "";
		
		$fileLogJSP  = "logjursp.txt";
		$fileRombel = "logrombel.txt";
		$fileARombel = "logarombel.txt";
		$fileRPeserta = "logrpeserta.txt";
		$filePeserta = "logpeserta.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$handle2 = fopen($fileRombel, 'a') or die('Cannot open file:  '.$fileRombel);
		$handle3 = fopen($fileARombel, 'a') or die('Cannot open file:  '.$fileARombel);
		$handle4 = fopen($fileRPeserta, 'a') or die('Cannot open file:  '.$fileRPeserta);
		$handle5 = fopen($filePeserta, 'a') or die('Cannot open file:  '.$fileRPeserta);

		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}


				
				// peserta dididk

				// rombel 

				// anggota rombel
				

				$cksekolah = $this->db->get_where('ref.peserta_didik',array('sekolah_id'=>$sid))->result();
				if(count($cksekolah)==0)
				{
					foreach ($cksekolah as $row) {
						$x = date("d-m-Y",strtotime($row->tanggal_lahir));
						$tgl = explode("-", $x);
						$a = $tgl[0];
						$b = $tgl[1];
						$c = $tgl[2];
						$thn = substr($c, 2);
						$pass = $this->bcrypt->hash_password($a.$b.$thn);
						$id = trim($row->peserta_didik_id);
						$s_username = array('user_id'=>trim($row->peserta_didik_id),
							'username'=>$row->nisn,
							'password'=>$pass,
							'level'=>'3',
							'login'=>'1');
						$qry = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'");
						if($qry->num_rows()==0)
						{
							$this->db->insert('app.username',$s_username);
						}
					}
				}

				$nama = "Admin BKK";
				$nip = "20171717";
				$email = "email@adminbkk.com";
				$hp = "0888888888888";
				$uname = $npsn;
				$pass ='$2a$08$8pqI0endkEiOoaNXJRFQ5OXui6I0tY6riabytHLOVkgyLL.K2.28m';
				$dat = array(
					'sekolah_id' => $sid,
					'nip' => $nip,
					'nama' => $nama,
					'email' =>$email,
					'tlp'=>$hp
				);

				$q = "SELECT * FROM app.user_sekolah WHERE sekolah_id='$sid'";
				$qry = $this->db->query($q)->result();
				if(count($qry)==0)
				{
					$this->db->insert('app.user_sekolah',$dat);

				}

				$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
				$qry = $this->db->query($q)->result();
				foreach ($qry as $key) {
					$user_id = $key->user_id;
				}

				$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
				);

				$qry = $this->db->query("SELECT * FROM app.username WHERE user_id='$user_id'");
				if($qry->num_rows()==0)
				{
					$this->db->insert('app.username',$dd);
				}

				$tgla = date('Y-m-d');
				$dataInsert=array(
					'sekolah_id'=>$sid,
					'tgl_aktivasi'=>$tgla,
					'logo' => 'no.jpg',
					'no_izin_bkk'=>"",
					'approved_by'=>"be988f3c-f18a-46a9-a8d4-46726b00d57d"
				);
				$cek = $this->db->get_where('sekolah_terdaftar',array('sekolah_id'=>$sid))->result();
				if(count($cek)==0)
				{
					$this->db->insert('sekolah_terdaftar',$dataInsert);
				}
		// end loop		
			}
		}
	}	
	
	public function getOne($id)
	{
		$sid = "";
		$npsn = "";
		
		$fileLogJSP  = "logjursp.txt";
		$fileRombel = "logrombel.txt";
		$fileARombel = "logarombel.txt";
		$fileRPeserta = "logrpeserta.txt";
		$filePeserta = "logpeserta.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$handle2 = fopen($fileRombel, 'a') or die('Cannot open file:  '.$fileRombel);
		$handle3 = fopen($fileARombel, 'a') or die('Cannot open file:  '.$fileARombel);
		$handle4 = fopen($fileRPeserta, 'a') or die('Cannot open file:  '.$fileRPeserta);
		$handle5 = fopen($filePeserta, 'a') or die('Cannot open file:  '.$fileRPeserta);

		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE sekolah_id='$id'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}

				// jurusan sp
				$dataS = $this->getJurusan($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$jip = trim($dataS[$a]['jurusan_sp_id']);
						$cek = $this->db->query("SELECT jurusan_sp_id FROM ref.jurusan_sp WHERE jurusan_sp_id='$jip'")->result();
						if(count($cek)==0)
						{			        	
							$dataInsert = array(
								'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
								'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
								'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
								'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
								'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
							);
							$this->db->insert('ref.jurusan_sp',$dataInsert);
							unset($dataInsert);
						}
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					echo "gagal koneksii";
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}

				// registrasi peserta didik 
				$dataS = $this->getRpeserta($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$cek = $this->db->get_where("ref.registrasi_peserta_didik",array('registrasi_id'=>trim($dataS[$a]['registrasi_id'])))->result();
						if(count($cek)==0)
						{
							if($dataS[$a]['jenis_keluar_id']=="")
							{
								$jk = NULL;
							}
							else
							{
								$jk = trim($dataS[$a]['jenis_keluar_id']);
							}
							if($dataS[$a]['tanggal_masuk_sekolah']=="")
							{
								$tm = NULL;
							}
							else
							{
								$tm = trim($dataS[$a]['tanggal_masuk_sekolah']);
							}

							if($dataS[$a]['tanggal_keluar']=="")
							{
								$tk = NULL;
							}
							else
							{
								$tk = trim($dataS[$a]['tanggal_keluar']);
							}

							if($dataS[$a]['jurusan_sp_id']=="")
							{
								$jp = NULL;
							}
							else
							{
								$jp = trim($dataS[$a]['jurusan_sp_id']);
							}

							$dataInsert = array(
								'registrasi_id' =>trim($dataS[$a]['registrasi_id']),
								'jurusan_sp_id' =>$jp,
								'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
								'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
								'jenis_pendaftaran_id' =>trim($dataS[$a]['jenis_pendaftaran_id']),
								'nipd' =>trim($dataS[$a]['nipd']),
								'tanggal_masuk_sekolah' =>$tm,
								'jenis_keluar_id' =>$jk,
								'tanggal_keluar' =>$tk,
								'keterangan'=>trim($dataS[$a]['keterangan'])
							);
							$this->db->insert('ref.registrasi_peserta_didik',$dataInsert);
							unset($dataInsert);
						}
					}	
					unset($dataInsert);
					unset($dataS);
				}
				else
				{
					fwrite($handle4, "Gagal =>" . $sid . "\n");
				}

				$dataS = $this->GetPeserta($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$id = trim($dataS[$a]['peserta_didik_id']);
						$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
						if(count($cek)==0)
						{
							$b = $dataS[$a]['nama'];
							$c = $dataS[$a]['jenis_kelamin'];
							$d = $dataS[$a]['nisn'];
							$e = $dataS[$a]['nik'];
							$f = $dataS[$a]['tempat_lahir'];
							$g = $dataS[$a]['tanggal_lahir'];
							$h = $dataS[$a]['agama_id'];
							$i = $dataS[$a]['kebutuhan_khusus_id'];
							$j = $dataS[$a]['sekolah_id'];
							$k = $dataS[$a]['jenis_keluar_id'];
							$l = $dataS[$a]['tanggal_masuk_sekolah'];
							$m = $dataS[$a]['tanggal_keluar'];
							$n = $dataS[$a]['jurusan_id'];

							$b = trim($b);
							$c = trim($c);
							$d = trim($d);
							$e = trim($e);
							$f = trim($f);
							$g = trim($g);
							$h = trim($h);
							$i = trim($i);
							$j = trim($j);
							$k = trim($k);
							$l = trim($l);
							$m = trim($m);
							$n = trim($n);
							$dataInsert = array(
								'peserta_didik_id' =>$id,
								'nama' =>$b,
								'jenis_kelamin' =>$c,
								'nisn' =>$d,
								'nik' =>$e,
								'tempat_lahir' =>$f,
								'tanggal_lahir' =>$g,
								'agama_id' =>$h,
								'kebutuhan_khusus_id' =>$i,
								'sekolah_id'=>$j,
								'jenis_keluar_id'=>$k,
								'tanggal_masuk_sekolah' =>$l,
								'tanggal_keluar'=>$m,
								'jurusan_id'=>$n
							);
							$this->db->insert('ref.peserta_didik',$dataInsert);
						}

						$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
						if(count($cek)==0)
						{
							$di = array(
								'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
								'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
								'rt' =>trim($dataS[$a]['rt']),
								'rw' =>trim($dataS[$a]['rw']),
								'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
								'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
								'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
								'kode_pos' =>trim($dataS[$a]['kode_pos']),
								'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
								'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
								'email' =>trim($dataS[$a]['email']),
								'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
								'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
								'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
								'no_KPS' =>trim($dataS[$a]['no_KPS']),
								'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
								'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
								'no_KIP'=>trim($dataS[$a]['no_KIP']),
								'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
								'no_KKS' =>trim($dataS[$a]['no_KKS']),
								'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
								'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
								'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
								'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
								'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
								'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
								'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
								'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
								'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
								'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
								'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
							);
							$this->db->insert('ref.peserta_didik_detil',$di);
						}
					}	
					unset($dataInsert);
					unset($dataS);
				}
				else
				{
					fwrite($handle5, "Gagal =>" . $sid . "\n");
				}

				// rombel 
				$dataS = $this->getRombel($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$jid = trim($dataS[$a]['rombongan_belajar_id']);
						if(trim($dataS[$a]['jurusan_sp_id'])=="")
						{
							$jip  = NULL;
						}
						else
						{
							$jip = trim($dataS[$a]['jurusan_sp_id']);
						}

						$q = $this->db->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$jid'")->result();
						if(count($q)==0)
						{
							$dataInsert = array(
								'rombongan_belajar_id' =>$jid,
								'semester_id'=>$dataS[$a]['semester_id'],
								'sekolah_id' =>$sid,
								'tingkat_pendidikan_id' =>trim($dataS[$a]['tingkat_pendidikan_id']),
								'jurusan_sp_id'=>$jip,
								'nama'=>$dataS[$a]['nama'],
								'kebutuhan_khusus_id'=>trim($dataS[$a]['kebutuhan_khusus_id'])
							);

							$this->db->insert('ref.rombongan_belajar',$dataInsert);
						}	
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					fwrite($handle2, "Gagal =>" . $sid . "\n");
				}

				// anggota rombel
				$dataS = $this->getArombel($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{	        	        		
						$ari = trim($dataS[$a]['anggota_rombel_id']);
						$cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
						if(count($cek)==0)
						{
							$dataInsert = array(
								'anggota_rombel_id'=>trim($dataS[$a]['anggota_rombel_id']),
								'rombongan_belajar_id'=>trim($dataS[$a]['rombongan_belajar_id']),
								'peserta_didik_id'=>trim($dataS[$a]['peserta_didik_id']),
								'jenis_pendaftaran_id'=>trim($dataS[$a]['jenis_pendaftaran_id'])
							);
							$this->db->insert('ref.anggota_rombel',$dataInsert);
						}
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					fwrite($handle3, "Gagal =>" . $sid . "\n");
				}

				$cksekolah = $this->db->get_where('ref.peserta_didik',array('sekolah_id'=>$sid))->result();
				if(count($cksekolah)==0)
				{
					foreach ($cksekolah as $row) {
						$pass = '$2a$08$W/P/II1U5H7MaLGHA5YwgeON13AI7XJq9A7cV.o9apzHiU2fosFb2';
						$id = trim($row->peserta_didik_id);
						$s_username = array('user_id'=>trim($row->peserta_didik_id),
							'username'=>$row->nisn,
							'password'=>$pass,
							'level'=>'3',
							'login'=>'1');
						$qry = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'");
						if($qry->num_rows()==0)
						{
							$this->db->insert('app.username',$s_username);
						}
					}
				}

				$nama = "Admin BKK";
				$nip = "20171717";
				$email = "email@adminbkk.com";
				$hp = "0888888888888";
				$uname = $npsn;
				$pass ='$2a$08$8pqI0endkEiOoaNXJRFQ5OXui6I0tY6riabytHLOVkgyLL.K2.28m';
				$dat = array(
					'sekolah_id' => $sid,
					'nip' => $nip,
					'nama' => $nama,
					'email' =>$email,
					'tlp'=>$hp
				);

				$q = "SELECT * FROM app.user_sekolah WHERE sekolah_id='$sid'";
				$qry = $this->db->query($q)->result();
				if(count($qry)==0)
				{
					$this->db->insert('app.user_sekolah',$dat);

				}

				$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
				$qry = $this->db->query($q)->result();
				foreach ($qry as $key) {
					$user_id = $key->user_id;
				}

				$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
				);

				$qry = $this->db->query("SELECT * FROM app.username WHERE user_id='$user_id'");
				if($qry->num_rows()==0)
				{
					$this->db->insert('app.username',$dd);
				}

				$tgla = date('Y-m-d');
				$dataInsert=array(
					'sekolah_id'=>$sid,
					'tgl_aktivasi'=>$tgla,
					'logo' => 'no.jpg',
					'no_izin_bkk'=>"",
					'approved_by'=>"be988f3c-f18a-46a9-a8d4-46726b00d57d"
				);
				$cek = $this->db->get_where('sekolah_terdaftar',array('sekolah_id'=>$sid))->result();
				if(count($cek)==0)
				{
					$this->db->insert('sekolah_terdaftar',$dataInsert);
				}

		// end loop		
			}
		}
	}	

	function ambiljursp($prov)
	{
		$sid = "";
		$npsn = "";
		
		$fileLogJSP  = "logjursp.txt";

		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);


		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}

				// jurusan sp
				$dataS = $this->getJurusan($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$jip = trim($dataS[$a]['jurusan_sp_id']);
						$cek = $this->db->query("SELECT jurusan_sp_id FROM ref.jurusan_sp WHERE jurusan_sp_id='$jip'")->result();
						if(count($cek)==0)
						{			        	
							$dataInsert = array(
								'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
								'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
								'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
								'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
								'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
							);
							$this->db->insert('ref.jurusan_sp',$dataInsert);
							unset($dataInsert);
						}
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}
			}
		}
	}

	function ambil_jursp($sid)
	{
		$dataS = $this->getJurusan($sid);
		if(!empty($dataS)){
			$this->db->query("DELETE FROM ref.jurusan_sp WHERE sekolah_id='$sid'");
			for($a=0;$a<count($dataS);$a++){
				$jip = trim($dataS[$a]['jurusan_sp_id']);
				$cek = $this->db->query("SELECT jurusan_sp_id FROM ref.jurusan_sp WHERE jurusan_sp_id='$jip'")->result();
				if(count($cek)==0){
					$dataInsert = array(
						'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
						'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
						'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
						'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
						'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
					);
					$this->db->insert('ref.jurusan_sp',$dataInsert);
					unset($dataInsert);
				}
			}
			unset($dataInsert);			        
			unset($dataS);
			echo "Sukses !\n";
		}
		else
		{
			echo "Error ! \n";
		}
	}

	function ambilRpeserta($prov)
	{
		$sid = "";
		$npsn = "";
		$fileLogJSP  = "logrpeserta.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}
				$dataS = $this->getRpeserta($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$cek = $this->db->get_where("ref.registrasi_peserta_didik",array('registrasi_id'=>trim($dataS[$a]['registrasi_id'])))->result();
						if(count($cek)==0)
						{
							if($dataS[$a]['jenis_keluar_id']=="")
							{
								$jk = NULL;
							}
							else
							{
								$jk = trim($dataS[$a]['jenis_keluar_id']);
							}
							if($dataS[$a]['tanggal_masuk_sekolah']=="")
							{
								$tm = NULL;
							}
							else
							{
								$tm = trim($dataS[$a]['tanggal_masuk_sekolah']);
							}

							if($dataS[$a]['tanggal_keluar']=="")
							{
								$tk = NULL;
							}
							else
							{
								$tk = trim($dataS[$a]['tanggal_keluar']);
							}

							if($dataS[$a]['jurusan_sp_id']=="")
							{
								$jp = NULL;
							}
							else
							{
								$jp = trim($dataS[$a]['jurusan_sp_id']);
							}

							$dataInsert = array(
								'registrasi_id' =>trim($dataS[$a]['registrasi_id']),
								'jurusan_sp_id' =>$jp,
								'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
								'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
								'jenis_pendaftaran_id' =>trim($dataS[$a]['jenis_pendaftaran_id']),
								'nipd' =>trim($dataS[$a]['nipd']),
								'tanggal_masuk_sekolah' =>$tm,
								'jenis_keluar_id' =>$jk,
								'tanggal_keluar' =>$tk,
								'keterangan'=>trim($dataS[$a]['keterangan'])
							);
							$this->db->insert('ref.registrasi_peserta_didik',$dataInsert);
							unset($dataInsert);
						}
					}	
					unset($dataInsert);
					unset($dataS);
				}
				else
				{
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}
			}
		}
	}

	function ambil_Rpeserta($sid)
	{
		$dataS = $this->getRpeserta($sid);
		if(!empty($dataS))
		{
			for($a=0;$a<count($dataS);$a++)
			{
				$cek = $this->db->get_where("ref.registrasi_peserta_didik",array('registrasi_id'=>trim($dataS[$a]['registrasi_id'])))->result();
				if(count($cek)==0)
				{
					if($dataS[$a]['jenis_keluar_id']=="")
					{
						$jk = NULL;
					}
					else
					{
						$jk = trim($dataS[$a]['jenis_keluar_id']);
					}
					if($dataS[$a]['tanggal_masuk_sekolah']=="")
					{
						$tm = NULL;
					}
					else
					{
						$tm = trim($dataS[$a]['tanggal_masuk_sekolah']);
					}

					if($dataS[$a]['tanggal_keluar']=="")
					{
						$tk = NULL;
					}
					else
					{
						$tk = trim($dataS[$a]['tanggal_keluar']);
					}

					if($dataS[$a]['jurusan_sp_id']=="")
					{
						$jp = NULL;
					}
					else
					{
						$jp = trim($dataS[$a]['jurusan_sp_id']);
					}

					$dataInsert = array(
						'registrasi_id' =>trim($dataS[$a]['registrasi_id']),
						'jurusan_sp_id' =>$jp,
						'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
						'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
						'jenis_pendaftaran_id' =>trim($dataS[$a]['jenis_pendaftaran_id']),
						'nipd' =>trim($dataS[$a]['nipd']),
						'tanggal_masuk_sekolah' =>$tm,
						'jenis_keluar_id' =>$jk,
						'tanggal_keluar' =>$tk,
						'keterangan'=>trim($dataS[$a]['keterangan'])
					);
					$this->db->insert('ref.registrasi_peserta_didik',$dataInsert);
					unset($dataInsert);
				}
			}	
			unset($dataInsert);
			unset($dataS);
			echo "Sukses !\n";

		}
		else
		{
			echo "Error ! \n";
		}
	}

	function ambilPeserta($prov)
	{
		$sid = "";
		$npsn = "";
		$fileLogJSP  = "logpeserta.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}

				$dataS = $this->GetPeserta($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$id = trim($dataS[$a]['peserta_didik_id']);
						$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
						if(count($cek)==0)
						{
							$b = $dataS[$a]['nama'];
							$c = $dataS[$a]['jenis_kelamin'];
							$d = $dataS[$a]['nisn'];
							$e = $dataS[$a]['nik'];
							$f = $dataS[$a]['tempat_lahir'];
							$g = $dataS[$a]['tanggal_lahir'];
							$h = $dataS[$a]['agama_id'];
							$i = $dataS[$a]['kebutuhan_khusus_id'];
							$j = $dataS[$a]['sekolah_id'];
							$k = $dataS[$a]['jenis_keluar_id'];
							$l = $dataS[$a]['tanggal_masuk_sekolah'];
							$m = $dataS[$a]['tanggal_keluar'];
							$n = $dataS[$a]['jurusan_id'];

							$b = trim($b);
							$c = trim($c);
							$d = trim($d);
							$e = trim($e);
							$f = trim($f);
							$g = trim($g);
							$h = trim($h);
							$i = trim($i);
							$j = trim($j);
							$k = trim($k);
							$l = trim($l);
							$m = trim($m);
							$n = trim($n);
							$dataInsert = array(
								'peserta_didik_id' =>$id,
								'nama' =>$b,
								'jenis_kelamin' =>$c,
								'nisn' =>$d,
								'nik' =>$e,
								'tempat_lahir' =>$f,
								'tanggal_lahir' =>$g,
								'agama_id' =>$h,
								'kebutuhan_khusus_id' =>$i,
								'sekolah_id'=>$j,
								'jenis_keluar_id'=>$k,
								'tanggal_masuk_sekolah' =>$l,
								'tanggal_keluar'=>$m,
								'jurusan_id'=>$n
							);
							$this->db->insert('ref.peserta_didik',$dataInsert);
						}

						$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
						if(count($cek)==0)
						{
							$di = array(
								'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
								'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
								'rt' =>trim($dataS[$a]['rt']),
								'rw' =>trim($dataS[$a]['rw']),
								'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
								'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
								'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
								'kode_pos' =>trim($dataS[$a]['kode_pos']),
								'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
								'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
								'email' =>trim($dataS[$a]['email']),
								'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
								'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
								'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
								'no_KPS' =>trim($dataS[$a]['no_KPS']),
								'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
								'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
								'no_KIP'=>trim($dataS[$a]['no_KIP']),
								'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
								'no_KKS' =>trim($dataS[$a]['no_KKS']),
								'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
								'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
								'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
								'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
								'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
								'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
								'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
								'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
								'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
								'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
								'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
							);
							$this->db->insert('ref.peserta_didik_detil',$di);
						}
					}	
					unset($dataInsert);
					unset($dataS);
				}
				else
				{
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}
			}
		}
	}

	function ambil_Peserta($sid)
	{
		$dataS = $this->GetPeserta($sid);
		if(!empty($dataS))
		{
			for($a=0;$a<count($dataS);$a++)
			{
				$id = trim($dataS[$a]['peserta_didik_id']);
				$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
				if(count($cek)==0)
				{
					$b = $dataS[$a]['nama'];
					$c = $dataS[$a]['jenis_kelamin'];
					$d = $dataS[$a]['nisn'];
					$e = $dataS[$a]['nik'];
					$f = $dataS[$a]['tempat_lahir'];
					$g = $dataS[$a]['tanggal_lahir'];
					$h = $dataS[$a]['agama_id'];
					$i = $dataS[$a]['kebutuhan_khusus_id'];
					$j = $dataS[$a]['sekolah_id'];
					$k = $dataS[$a]['jenis_keluar_id'];
					$l = $dataS[$a]['tanggal_masuk_sekolah'];
					$m = $dataS[$a]['tanggal_keluar'];
					$n = $dataS[$a]['jurusan_id'];

					$b = trim($b);
					$c = trim($c);
					$d = trim($d);
					$e = trim($e);
					$f = trim($f);
					$g = trim($g);
					$h = trim($h);
					$i = trim($i);
					$j = trim($j);
					$k = trim($k);
					$l = trim($l);
					$m = trim($m);
					$n = trim($n);
					$dataInsert = array(
						'peserta_didik_id' =>$id,
						'nama' =>$b,
						'jenis_kelamin' =>$c,
						'nisn' =>$d,
						'nik' =>$e,
						'tempat_lahir' =>$f,
						'tanggal_lahir' =>$g,
						'agama_id' =>$h,
						'kebutuhan_khusus_id' =>$i,
						'sekolah_id'=>$j,
						'jenis_keluar_id'=>$k,
						'tanggal_masuk_sekolah' =>$l,
						'tanggal_keluar'=>$m,
						'jurusan_id'=>$n
					);
					$this->db->insert('ref.peserta_didik',$dataInsert);
				}

				$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
				if(count($cek)==0)
				{
					$di = array(
						'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
						'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
						'rt' =>trim($dataS[$a]['rt']),
						'rw' =>trim($dataS[$a]['rw']),
						'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
						'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
						'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
						'kode_pos' =>trim($dataS[$a]['kode_pos']),
						'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
						'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
						'email' =>trim($dataS[$a]['email']),
						'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
						'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
						'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
						'no_KPS' =>trim($dataS[$a]['no_KPS']),
						'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
						'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
						'no_KIP'=>trim($dataS[$a]['no_KIP']),
						'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
						'no_KKS' =>trim($dataS[$a]['no_KKS']),
						'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
						'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
						'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
						'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
						'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
						'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
						'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
						'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
						'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
						'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
						'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
					);
					$this->db->insert('ref.peserta_didik_detil',$di);
				}

				$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'")->result();
				if(count($cek)=="")
				{
					$d = $dataS[$a]['nisn'];
					$g = $dataS[$a]['tanggal_lahir'];

					$d = trim($d);
					$g = trim($g);


					$x = date("d-m-Y",strtotime($g));
					$tgl = explode("-", $x);
					$a = $tgl[0];
					$b = $tgl[1];
					$c = $tgl[2];
					$thn = substr($c, 2);
					$pass = $this->bcrypt->hash_password($a.$b.$thn);
					$s_username = array('user_id'=>$id,
						'username'=>$d,
						'password'=>$pass,
						'level'=>'3',
						'login'=>'1');
					
					$this->db->insert('app.username',$s_username);
				}
			}	
			unset($dataInsert);
			unset($dataS);
			echo "Sukses !\n";

		}
		else
		{
			echo "Error ! \n";
		}
	}

	function ambilRombel($prov)
	{
		$sid = "";
		$npsn = "";
		$fileLogJSP  = "logrombel.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}

				$dataS = $this->getRombel($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{
						$jid = trim($dataS[$a]['rombongan_belajar_id']);
						if(trim($dataS[$a]['jurusan_sp_id'])=="")
						{
							$jip  = NULL;
						}
						else
						{
							$jip = trim($dataS[$a]['jurusan_sp_id']);
						}

						$q = $this->db->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$jid'")->result();
						if(count($q)==0)
						{
							$dataInsert = array(
								'rombongan_belajar_id' =>$jid,
								'semester_id'=>$dataS[$a]['semester_id'],
								'sekolah_id' =>$sid,
								'tingkat_pendidikan_id' =>trim($dataS[$a]['tingkat_pendidikan_id']),
								'jurusan_sp_id'=>$jip,
								'nama'=>$dataS[$a]['nama'],
								'kebutuhan_khusus_id'=>trim($dataS[$a]['kebutuhan_khusus_id'])
							);

							$this->db->insert('ref.rombongan_belajar',$dataInsert);
						}	
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}
			}
		}
	}

	function ambil_Rombel($sid)
	{
		$dataS = $this->getRombel($sid);
		if(!empty($dataS))
		{
			for($a=0;$a<count($dataS);$a++)
			{
				$jid = trim($dataS[$a]['rombongan_belajar_id']);
				if(trim($dataS[$a]['jurusan_sp_id'])=="")
				{
					$jip  = NULL;
				}
				else
				{
					$jip = trim($dataS[$a]['jurusan_sp_id']);
				}

				$q = $this->db->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$jid'")->result();
				if(count($q)==0)
				{
					$dataInsert = array(
						'rombongan_belajar_id' =>$jid,
						'semester_id'=>$dataS[$a]['semester_id'],
						'sekolah_id' =>$sid,
						'tingkat_pendidikan_id' =>trim($dataS[$a]['tingkat_pendidikan_id']),
						'jurusan_sp_id'=>$jip,
						'nama'=>$dataS[$a]['nama'],
						'kebutuhan_khusus_id'=>trim($dataS[$a]['kebutuhan_khusus_id'])
					);

					$this->db->insert('ref.rombongan_belajar',$dataInsert);
				}	
			}
			unset($dataInsert);			        
			unset($dataS);
			echo "Sukses !\n";

		}
		else
		{
			echo "Error ! \n";
		}
	}

	function ambilArombel($prov)
	{
		$sid = "";
		$npsn = "";
		$fileLogJSP  = "logarombel.txt";
		$handle1 = fopen($fileLogJSP, 'a') or die('Cannot open file:  '.$fileLogJSP);
		$sk = $this->db->query("SELECT * FROM ref.view_sekolah_wilayah WHERE kode_prov='$prov'")->result();
		if(count($sk)>0)
		{
			foreach ($sk as $key ) 
			{
				$sid = trim($key->sekolah_id);
				$skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
				foreach ($skl as $ke) {
					$npsn = trim($ke->npsn);
				}
				$dataS = $this->getArombel($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
					{	        	        		
						$ari = trim($dataS[$a]['anggota_rombel_id']);
						$cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
						if(count($cek)==0)
						{
							$dataInsert = array(
								'anggota_rombel_id'=>trim($dataS[$a]['anggota_rombel_id']),
								'rombongan_belajar_id'=>trim($dataS[$a]['rombongan_belajar_id']),
								'peserta_didik_id'=>trim($dataS[$a]['peserta_didik_id']),
								'jenis_pendaftaran_id'=>trim($dataS[$a]['jenis_pendaftaran_id'])
							);
							$this->db->insert('ref.anggota_rombel',$dataInsert);
						}
					}
					unset($dataInsert);			        
					unset($dataS);
				}
				else
				{
					fwrite($handle1, "Gagal =>" . $sid . "\n");
				}
			}
		}
	}

	function ambil_Arombel($sid)
	{
		$dataS = $this->getArombel($sid);
		if(!empty($dataS))
		{
			for($a=0;$a<count($dataS);$a++)
			{	        	        		
				$ari = trim($dataS[$a]['anggota_rombel_id']);
				$cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
				if(count($cek)==0)
				{
					$dataInsert = array(
						'anggota_rombel_id'=>trim($dataS[$a]['anggota_rombel_id']),
						'rombongan_belajar_id'=>trim($dataS[$a]['rombongan_belajar_id']),
						'peserta_didik_id'=>trim($dataS[$a]['peserta_didik_id']),
						'jenis_pendaftaran_id'=>trim($dataS[$a]['jenis_pendaftaran_id'])
					);
					$this->db->insert('ref.anggota_rombel',$dataInsert);
				}
			}
			unset($dataInsert);			        
			unset($dataS);
			echo "Sukses !\n";

		}
		else
		{
			echo "Error ! \n";
		}
	}

	function ambilSatu($sid)
	{
		$this->ambil_jursp($sid);
		$this->ambil_Rombel($sid);
		$this->ambil_Rombel($sid);
		$this->ambil_Peserta($sid);
		$this->ambil_Rpeserta($sid);
	}

	function getJurusan($sid){
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=admin123&utab=psnasuruj&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200){
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

	function getRpeserta($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

	function GetPeserta($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=bkk&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

	function GetRombel($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

	function GetArombel($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

	function get_peserta($sid)
	{
		$hasil = array();
		$lengkap = array();
		$dumpJurSp = array();
		$dumpRegPst = array();
		$dumpPeserta = array();
		$dumpAromBel = array();
		$dumpRomBel = array();

		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$dataS = json_decode($cexecute,true);
			if(!empty($dataS))
			{
				for($a=0;$a<count($dataS);$a++)
				{
					$dataInsert = array(
						'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
						'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
						'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
						'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
						'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
					);
					array_push($dumpJurSp, $dataInsert);
				}
			}
		}
		
		unset($dataS);

		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$dataS = json_decode($cexecute,true);
			if(!empty($dataS))
			{
				for($a=0;$a<count($dataS);$a++)
				{
					if($dataS[$a]['jenis_keluar_id']=="")
					{
						$jk = NULL;
					}
					else
					{
						$jk = trim($dataS[$a]['jenis_keluar_id']);
					}
					if($dataS[$a]['tanggal_masuk_sekolah']=="")
					{
						$tm = NULL;
					}
					else
					{
						$tm = trim($dataS[$a]['tanggal_masuk_sekolah']);
					}

					if($dataS[$a]['tanggal_keluar']=="")
					{
						$tk = NULL;
					}
					else
					{
						$tk = trim($dataS[$a]['tanggal_keluar']);
					}

					if($dataS[$a]['jurusan_sp_id']=="")
					{
						$jp = NULL;
					}
					else
					{
						$jp = trim($dataS[$a]['jurusan_sp_id']);
					}

					$dataInsert = array(
						'registrasi_id' =>trim($dataS[$a]['registrasi_id']),
						'jurusan_sp_id' =>$jp,
						'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
						'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
						'jenis_pendaftaran_id' =>trim($dataS[$a]['jenis_pendaftaran_id']),
						'nipd' =>trim($dataS[$a]['nipd']),
						'tanggal_masuk_sekolah' =>$tm,
						'jenis_keluar_id' =>$jk,
						'tanggal_keluar' =>$tk,
						'keterangan'=>trim($dataS[$a]['keterangan'])
					);
					array_push($dumpRegPst, $dataInsert);
				}
				$hasil = $this->inner_join($dumpRegPst,$dumpJurSp,'jurusan_sp_id');
				unset($dataInsert);
				unset($dataS);
			}
		}

		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresep&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$dataS = json_decode($cexecute,true);
			if(!empty($dataS))
			{
				for($a=0;$a<count($dataS);$a++)
				{
					$dataInsert = array(
						'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
						'nama' =>trim($dataS[$a]['nama']),
						'jenis_kelamin' =>trim($dataS[$a]['jenis_kelamin']),
						'nisn' =>trim($dataS[$a]['nisn']),
						'nik' =>trim($dataS[$a]['nik']),
						'tempat_lahir' =>trim($dataS[$a]['tempat_lahir']),
						'tanggal_lahir' =>trim($dataS[$a]['tanggal_lahir']),
						'agama_id' =>trim($dataS[$a]['agama_id']),
						'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
						'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
						'rt' =>trim($dataS[$a]['rt']),
						'rw' =>trim($dataS[$a]['rw']),
						'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
						'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
						'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
						'kode_pos' =>trim($dataS[$a]['kode_pos']),
						'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
						'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
						'email' =>trim($dataS[$a]['email']),
						'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
						'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
						'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
						'no_KPS' =>trim($dataS[$a]['no_KPS']),
						'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
						'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
						'no_KIP'=>trim($dataS[$a]['no_KIP']),
						'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
						'no_KKS' =>trim($dataS[$a]['no_KKS']),
						'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
						'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
						'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
						'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
						'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
						'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
						'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
						'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
						'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
						'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
						'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
					);
					array_push($dumpPeserta, $dataInsert);
				}
				$lengkap = $this->inner_join($dumpPeserta,$hasil,'peserta_didik_id');
			}
		}
		unset($dataS);


		$dataS = $lengkap;

		if(!empty($dataS))
		{
			for($a=0;$a<count($dataS);$a++)
			{
				$id = trim($dataS[$a]['peserta_didik_id']);
				$cek = $this->db->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id'")->result();
				if(count($cek)==0)
				{
					$b = $dataS[$a]['nama'];
					$c = $dataS[$a]['jenis_kelamin'];
					$d = $dataS[$a]['nisn'];
					$e = $dataS[$a]['nik'];
					$f = $dataS[$a]['tempat_lahir'];
					$g = $dataS[$a]['tanggal_lahir'];
					$h = $dataS[$a]['agama_id'];
					$i = $dataS[$a]['kebutuhan_khusus_id'];
					$j = $dataS[$a]['sekolah_id'];
					$k = $dataS[$a]['jenis_keluar_id'];
					$l = $dataS[$a]['tanggal_masuk_sekolah'];
					$m = $dataS[$a]['tanggal_keluar'];
					$n = $dataS[$a]['jurusan_id'];

					$b = trim($b);
					$c = trim($c);
					$d = trim($d);
					$e = trim($e);
					$f = trim($f);
					$g = trim($g);
					$h = trim($h);
					$i = trim($i);
					$j = trim($j);
					$k = trim($k);
					$l = trim($l);
					$m = trim($m);
					$n = trim($n);
					$dataInsert = array(
						'peserta_didik_id' =>$id,
						'nama' =>$b,
						'jenis_kelamin' =>$c,
						'nisn' =>$d,
						'nik' =>$e,
						'tempat_lahir' =>$f,
						'tanggal_lahir' =>$g,
						'agama_id' =>$h,
						'kebutuhan_khusus_id' =>$i,
						'sekolah_id'=>$j,
						'jenis_keluar_id'=>$k,
						'tanggal_masuk_sekolah' =>$l,
						'tanggal_keluar'=>$m,
						'jurusan_id'=>$n
					);
					$this->db->insert('ref.peserta_didik',$dataInsert);
				}

				$cek = $this->db->get_where("ref.peserta_didik_detil",array('peserta_didik_id'=>$id))->result();
				if(count($cek)==0)
				{
					$di = array(
						'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
						'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
						'rt' =>trim($dataS[$a]['rt']),
						'rw' =>trim($dataS[$a]['rw']),
						'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
						'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
						'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
						'kode_pos' =>trim($dataS[$a]['kode_pos']),
						'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
						'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
						'email' =>trim($dataS[$a]['email']),
						'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
						'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
						'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
						'no_KPS' =>trim($dataS[$a]['no_KPS']),
						'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
						'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
						'no_KIP'=>trim($dataS[$a]['no_KIP']),
						'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
						'no_KKS' =>trim($dataS[$a]['no_KKS']),
						'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
						'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
						'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
						'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
						'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
						'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
						'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
						'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
						'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
						'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
						'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
					);
					$this->db->insert('ref.peserta_didik_detil',$di);
				}

				$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$id'")->result();
				if(count($cek)=="")
				{
					$d = $dataS[$a]['nisn'];
					$g = $dataS[$a]['tanggal_lahir'];

					$d = trim($d);
					$g = trim($g);


					$x = date("d-m-Y",strtotime($g));
					$tgl = explode("-", $x);
					$a = $tgl[0];
					$b = $tgl[1];
					$c = $tgl[2];
					$thn = substr($c, 2);
					//$pass = $this->bcrypt->hash_password($a.$b.$thn);
					$pass = $this->bcrypt->hash_password('$2a$08$PW7YZGgZnY69qcgTIsvNmOfXtNULGks34h7PPJS4vdkxCR9vmE1aC');
					$s_username = array('user_id'=>$id,
						'username'=>$d,
						'password'=>$pass,
						'level'=>'3',
						'login'=>'1');
					
					$this->db->insert('app.username',$s_username);
				}
			}	
			unset($dataInsert);
			unset($dataS);
			echo "Sukses !\n";

		}



	}

	function inner_join(array $left, array $right, $on) 
	{
		$out = array();
		foreach ($left as $left_record) 
		{
			foreach ($right as $right_record) 
			{
				if ($left_record[$on] == $right_record[$on]) 
				{
					$out[] = array_merge($left_record, $right_record);
				}
			}
		}
		return $out;
	}

}
