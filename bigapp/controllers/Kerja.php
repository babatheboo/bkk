<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kerja extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }

  	public function index(){
		echo "haiiiii";
	}

	public function getkerja($nisn){
        $kerja = $this->db->query("SELECT * FROM kerja_siswa WHERE nisn='$nisn'")->result();
        $n=0;
        $response = array();
        if (count($kerja)>0){
                foreach($kerja as $key){
                $n++;
                if($key->akhir_kerja == null || $key->akhir_kerja==""){
                        $sampai = "Sekarang";
                } else {
                        $sampai = date('d-m-Y', strtotime($key->akhir_kerja));
                }
                if($key->ver_status=="2"){
                        $ver = "Terverifikasi";
                } else {
                        $ver = "Belum diverifikasi";
                }

                if($key->status=="0"){
                        $status = "Tidak aktif";
                } else {
                        $status = "Aktif";
                }
$dt = array('no'=>$n,'perusahaan'=>$key->perusahaan,'posisi'=>$key->posisi,'mulai'=>date('d-m-Y', strtotime($key->mulai_kerja)),'sampai'=>$sampai,'ver'=>$ver,'id'=>$key->id,'status'=>$status,'gaji'=>$key->range_gaji);

        array_push($response, $dt);
            }
        }
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
                exit;
	}

	public function addKerja(){
		$nisn = $this->input->post('nisn');
		$perusahaan = $this->input->post('perusahaan');
		$posisi = $this->input->post('posisi');
		$mulai = date('Y-m-d', strtotime($this->input->post('mulai_kerja')));
		$sisid = $this->input->post('sisid');
		$gaji = $this->input->post('gaji');
	$data = array('nisn'=>$nisn, 
'perusahaan'=>$perusahaan,'posisi'=>$posisi,'mulai_kerja'=>$mulai,'peserta_didik_id'=>$sisid,'ver_status'=>'0','range_gaji'=>$gaji,'status'=>'1');
	if($this->db->insert('kerja_siswa',$data)){
		$response["error"] = FALSE;
		$response["error_msg"] = "Data berhasil disimpan";
	} else {
		$response["error"] = TRUE;
		$response["error_msg"] = "Penambahan data gagal";
	}
	$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
      		exit;
	}


	public function update(){
	$id = $this->input->post('id');
	$posisi = $this->input->post('posisi');
	$mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
	$gaji = $this->input->post('gaji');
	$status = $this->input->post('status');

	if($status=="1"){
	  $data = array("posisi"=>$posisi,"mulai_kerja"=>$mulai,"range_gaji"=>$gaji,"status"=>"1","akhir_kerja"=>NULL);
	} else {
	  $tgl = date('Y-m-d');
	   $data = array("posisi"=>$posisi,"mulai_kerja"=>$mulai,"range_gaji"=>$gaji,"status"=>"0","akhir_kerja"=>$tgl);
	}

	$this->db->where("id", $id);
	if($this->db->update("kerja_siswa", $data)){
		$response["error"] = FALSE;
		$response["error_msg"] = "Data berhasil diupdate !";
	} else {
		$response["error"] = TRUE;
		$response["error_msg"] = "Gagal update data !";
	}

	
	}


	public function delkerja($id){
	    if($this->db->delete('kerja_siswa', array('id'=>$id))){
		 $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil dihapus !";
	    } else {
	  	$response["error"] = TRUE;
                $response["error_msg"] = "Gagal menghapus data !";

	    }
	}

}
