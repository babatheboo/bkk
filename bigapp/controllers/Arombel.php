<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', '0');

class Arombel extends CI_Controller {
	
	public function __construct(){
  		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->load->library('bcrypt');
 	}

 	public function sink($prov)
 	{

	    $dp = $this->db->query("select * from ref.view_sekolah_wilayah where kode_prov='$prov'")->result();
 		if(!empty($dp))
 		{
 			foreach ($dp as $row) 
 			{
 				$sid = trim($row->sekolah_id) ;
 				$dataS = $this->getArombel($sid);
				if(!empty($dataS))
				{
					for($a=0;$a<count($dataS);$a++)
			        {	        	        		
			          $ari = trim($dataS[$a]['anggota_rombel_id']);
			          $cek = $this->db->get_where("ref.anggota_rombel",array('anggota_rombel_id'=>$ari))->result();
			          if(count($cek)==0)
			          {
			          	$dataInsert = array(
			              'anggota_rombel_id'=>$ari,
			              'rombongan_belajar_id'=>trim($dataS[$a]['rombongan_belajar_id']),
			              'peserta_didik_id'=>trim($dataS[$a]['peserta_didik_id']),
			              'jenis_pendaftaran_id'=>trim($dataS[$a]['jenis_pendaftaran_id'])
			            );
			          	$this->db->insert('ref.anggota_rombel',$dataInsert);
			          }
			        }
					unset($dataInsert);			        
					unset($dataS);
				}
 			}
 			echo "Selesai ";
 		}
 	}

 	function GetArombel($sid)
	{
		$url = "http://172.18.4.11/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=$sid";
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$cexecute=curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode==200)
		{
			$data = json_decode($cexecute,true);
		}
		return $data;
	}

}