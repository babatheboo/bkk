<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require APPPATH . 'libraries/REST_Controller.php';

class Dapodik extends CI_Controller{

    public function __construct(){
      parent::__construct();
    }   

  public function oauth_test(){
    $provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'redirectUri'             => 'http://helpdesk.dikdasmen.kemdikbud.go.id:5000/api/smk/lulusan/db9cc202-81ae-4913-945c-10000e40e3b1/2019',
    'urlAuthorize'            => 'http://helpdesk.dikdasmen.kemdikbud.go.id:5000/authorize',
    'urlAccessToken'          => 'http://helpdesk.dikdasmen.kemdikbud.go.id:5000/token',
    'urlResourceOwnerDetails' => 'http://helpdesk.dikdasmen.kemdikbud.go.id:5000/resource'
    ]);

    try {
        $accessToken = $provider->getAccessToken('password', [
            'username' => 'admin',
            'password' => '6511A68B-3FB8-4537-8E31-384A7CCF7C38'
        ]);
        $request = $provider->getAuthenticatedRequest(
            'GET',
            'http://helpdesk.dikdasmen.kemdikbud.go.id:5000/api/smk/lulusan/47596d27-8179-4c77-85ed-0085ab47a3d1/2019',
            $accessToken
        );
        $client = new GuzzleHttp\Client();
        $promise = $client->sendAsync($request)->then(function ($response) {
            $a = $response->getBody()->getContents();
            $kode = $response->getStatusCode();
            if ($kode==200) {
            $data = json_decode($a, true);
            // echo md5(md5('siswa'));
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            // foreach ($data as $key) {
            //     echo $key['tanggal_sk_izin'].'<br>';
            // }
            }else{
                echo "Terjadi kesalahan. Ulangi proses";
            }
        });

        $promise->wait();

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        exit($e->getMessage());
        $data = 'Sesi pengambilan data habis, jaringan internet yang digunakan kurang stabil. Silahkan perbarui laman ini dan ulangi proses. Terima kasih.';
    }
    echo $data;
  }
}