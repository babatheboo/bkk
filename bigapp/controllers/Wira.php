<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wira extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }

  public function index(){
		echo "muuuacchhhh";
	}

	public function getwira($nisn){
        $kerja = $this->db->query("SELECT * FROM wira_siswa WHERE nisn='$nisn'")->result();
        $n=0;
        $response = array();
        if (count($kerja)>0){
                foreach($kerja as $key){
                $n++;
                if($key->akhir_usaha == null || $key->akhir_usaha==""){
                        $sampai = "Sekarang";
                } else {
                        $sampai = date('d-m-Y', strtotime($key->akhir_usaha));
                }
                if($key->ver_status=="2"){
                        $ver = "Terverifikasi";
                } else {
                        $ver = "Belum diverifikasi";
                }

                if($key->status=="0"){
                        $status = "Tidak aktif";
                } else {
                        $status = "Aktif";
                }

        $dt = array('no'=>$n,'perusahaan'=>$key->perusahaan,'jenis'=>$key->jenis_usaha,'mulai'=>date('d-m-Y',strtotime($key->mulai_usaha)),'sampai'=>$sampai,'ver'=>$ver,'id'=>$key->id,'status'=>$status,'pendapatan'=>$key->pendapatan);
        array_push($response, $dt);
            }
        }
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
	exit;
	}


  public function addWira(){
                $nisn = $this->input->post('nisn');
                $perusahaan = $this->input->post('perusahaan');
                $posisi = $this->input->post('posisi');
                $mulai = date('Y-m-d', strtotime($this->input->post('mulai_kerja')));
                $sisid = $this->input->post('sisid');
                $gaji = $this->input->post('gaji');
        $data = array('nisn'=>$nisn,'perusahaan'=>$perusahaan,'jenis_usaha'=>$posisi,'mulai_usaha'=>$mulai,'peserta_didik_id'=>$sisid,'ver_status'=>'0','pendapatan'=>$gaji,'status'=>'1');
        if($this->db->insert('wira_siswa',$data)){
                $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil disimpan";
        } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Penambahan data gagal";
        }
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
                exit;
        }




     public function update(){
        $id = $this->input->post('id');
        $posisi = $this->input->post('posisi');
        $mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
        $gaji = $this->input->post('gaji');
        $status = $this->input->post('status');

        if($status=="1"){
          $data = array("jenis_usaha"=>$posisi,"mulai_usaha"=>$mulai,"pendapatan"=>$gaji,"status"=>"1","akhir_usaha"=>NULL);
        } else {
          $tgl = date('Y-m-d');
           $data = array("jenis_usaha"=>$posisi,"mulai_usaha"=>$mulai,"pendapatan"=>$gaji,"status"=>"0","akhir_usaha"=>$tgl);
        }

        $this->db->where("id", $id);
        if($this->db->update("wira_siswa", $data)){
                $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil diupdate !";
        } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Gagal update data !";
        }


        }





        public function delkerja($id){
            if($this->db->delete('wira_siswa', array('id'=>$id))){
                 $response["error"] = FALSE;
                $response["error_msg"] = "Data berhasil dihapus !";
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Gagal menghapus data !";

            }
        }


}
