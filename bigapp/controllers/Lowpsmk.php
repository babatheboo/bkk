<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lowpsmk extends CI_Controller {

	public function __construct(){
      		parent::__construct();
  	}


	public function index(){
		$lpsmk = $this->db->query("SELECT * FROM lowongan_psmk WHERE  tgl_tutup>=now() AND status='1' order BY tgl_tutup ")->result();
		$response = array();
		if(count($lpsmk)>0){
		   foreach ($lpsmk as $key) {
		    $dt = array('judul'=>$key->judul,'deskripsi'=>$key->deskripsi,'perusahaan'=>$key->perusahaan,'foto'=>$key->gambar,'tgl'=>date('d-m-Y', strtotime($key->tgl_tutup)));
		    array_push($response, $dt);
		   }
		}
		$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
      		exit;
	}



}
