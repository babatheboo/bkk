<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lamaran extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }

  public function index(){
	echo "Ahhh.......";
  }

  public function getlamaran($sid){
  $this->db->query("REFRESH MATERIALIZED VIEW mview_lowongan_frontpage");
  $d =  $this->db->query("SELECT * FROM bidding_lulusan WHERE id_siswa='$sid'")->result();
  $response = array();
  $status = "";
  if(count($d)>0){
    foreach ($d as $key) {
      $low = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE id='$key->id_loker'")->result();
	    foreach ($low as $keys) {
        if($keys->status=="0" || $keys->status == null){
	         $status = "Proses rekruitment";
        } else if ($keys->status==1){
            $status = "Testing";
        }  else {
	         $status = "Selesai";
	     }

    	if($key->status=="0"){
    	  $posisi = "Tidak diterima";
    	} else if ($key->status =="1") {
    	  $posisi = "Proses seleksi";
    	} else if ($key->status == "4") {
    	  $posisi = "Diterima";
    	}
        
	$dt = array("judul"=>$keys->judul, "tanggal"=>date('d-m-Y', strtotime($key->tgl_bid)),"status"=>$status,"kode"=>$key->id_loker,"posisi"=>$posisi,"id_bidding"=>$key->id);
        array_push($response, $dt);
      } 
     }
   }

   $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
   exit;

  }


  public function hapus(){
    $id = $this->input->post('kode');
    if($this->db->delete('bidding_lulusan', array('id'=>$id))){
      $response["error"] = FALSE;
                $response["error_msg"] = "Lamaran berhasil dihapus";

  } else {
      $response["error"] = TRUE;
                $response["error_msg"] = "Lamaran gagal dihapus";
  }
   $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($response, JSON_PRETTY_PRINT))->_display();
        exit;
  }
}
