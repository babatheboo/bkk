<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QueryTester extends CI_Controller {

   public function __construct(){
          parent::__construct();
    }
  	public function index(){
  		$q = $this->db->query("SELECT c.nama as nama_sekolah, c.npsn, count(b.peserta_didik_id) as totker,
          count(d.peserta_didik_id) as totwir,
          count(e.peserta_didik_id) as totkul,
          count(a.peserta_didik_id) as totsiw
          from ref.peserta_didik a
          left join kerja_siswa b on a.peserta_didik_id=b.peserta_didik_id
          left join ref.sekolah c on a.sekolah_id=c.sekolah_id
          left join wira_siswa d on a.peserta_didik_id=d.peserta_didik_id
          left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
          where left(a.tanggal_keluar,4)>='2017' and left(a.tanggal_keluar,4)<='2020'
          group by c.nama, c.npsn
          order by count(a.peserta_didik_id) + count(d.peserta_didik_id) + count(e.peserta_didik_id) - count(b.peserta_didik_id) - count(d.peserta_didik_id)- count(e.peserta_didik_id)-count(b.peserta_didik_id) desc limit 1");
      echo $q;
  	}
  }