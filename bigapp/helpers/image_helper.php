<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Thumb()
 * A TimThumb-style function to generate image thumbnails on the fly.
 *
 * @access public
 * @param string $fullname
 * @param int $width
 * @param int $height
 * @param string $image_thumb
 * @return String
 *
 */
function thumbnail($fullname, $width, $height, $crop = TRUE)
{
  // Path to image thumbnail in your root
  $dir = './assets/uploads/';
  $url = base_url() . 'assets/uploads/thumbs/';
  // Get the CodeIgniter super object
  $CI = &get_instance();

  // get src file's extension and file name
  $extension = pathinfo($fullname, PATHINFO_EXTENSION);
  $filename = pathinfo($fullname, PATHINFO_FILENAME);
  $image_org = $dir . $filename . "." . $extension;
  $image_thumb = $dir . 'thumbs/'. $filename . "-" . $height . '_' . $width . "." . $extension;
  $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;

  if (!file_exists($image_thumb))
  {
    //var_dump($filename, $image_org, $image_thumb); exit;
    $CI->load->library('Image_moo');

		$CI->image_moo->load($image_org);

    if ($crop)
      $CI->image_moo->resize_crop($width, $height);
    else
      $CI->image_moo->resize($width, $height, false);

    $CI->image_moo->save($image_thumb, true);

    /* LOAD LIBRARY
    $CI->load->library('image_lib');
    // CONFIGURE IMAGE LIBRARY
    $config['source_image'] = $image_org;
    $config['new_image'] = $image_thumb;
    $config['width'] = $width;
    $config['height'] = $height;
    $CI->image_lib->initialize($config);
    $CI->image_lib->resize();
    $CI->image_lib->clear();
    */
  }
  return $image_returned;
}

function excerpt($string, $limit = NULL)
{
  $return = mb_strcut($string, 0, $limit);

  if (strlen($string)<=$limit || is_null($limit))
    return $return;
  return $return."...";
}
