<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * helper for CRUD
 * created by arifrahmanhakim.net@gmail.com
 */
 /*
 * to display field in better view */
function display_as($fieldname, $ucwords = TRUE)
{
  $display_as = str_ireplace(['_'],[' '],$fieldname);
  if (!$ucwords)
    return $display_as;
  return ucwords(strtolower($display_as));
}
