<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Asn{
  protected $CI;
    function __construct(){
      $this->CI =& get_instance();
       $this->CI->load->database();
      date_default_timezone_set('Asia/Jakarta');
    }
    function keurlogin(){
      if($this->CI->session->userdata('login') == FALSE){
        return FALSE;
      }
      return TRUE;
    }
    function login(){
      if($this->keurlogin() == FALSE){
          redirect('login','refresh');
      }
    }
    public function role_akses($akses){
      $ak = explode("|", $akses);
      if(count($ak)>1)
      {
        if($this->CI->session->userdata('level')!=$ak[0] && $this->CI->session->userdata('level')!=$ak[1])
        {
         redirect(site_url('_404'));
        }
        else
        {
           return TRUE;
        }
         
      }
      else
      {
        if($this->CI->session->userdata('level')!=$ak[0]){
         redirect(site_url('_404'));
        }else{
          return TRUE;  
        }
        
      }
      
    }
    function anti($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);
        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }
        return $inp;
    } 
    function do_logout(){
		    $this->CI->session->sess_destroy();
	  }

    function proses_register(){
      $npsn="";
      $schoolId = $this->CI->session->userdata('sakolana'); 
      $tgl = date("Y-m-d H:i:s");
      $query =  $this->CI->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$schoolId'")->result();
      foreach ($query as $row) {
        $data = array(
          'sekolah_id'=>$row->sekolah_id,
                'tgl_aktivasi'=>$tgl,
                'logo' => 'no.jpg',
                'no_izin_bkk'=>"",
                'approved_by'=>$this->CI->session->userdata('user_id')
              );  
        $npsn=$row->npsn;
      }
      $this->CI->db->insert('sekolah_terdaftar',$data);
      $cksekolah = $this->CI->db->get_where('ref.view_lulusan_kls12',array('sekolah_id'=>$schoolId))->result();
      if(count($cksekolah)>0){
        foreach ($cksekolah as $row) {
          $x = date("d-m-Y",strtotime($row->tanggal_lahir));
          $tgl = explode("-", $x);
          $a = $tgl[0];
          $b = $tgl[1];
          $c = $tgl[2];
          $thn = substr($c, 2);
          $pass = $this->CI->bcrypt->hash_password($a.$b.$thn);
          $s_username = array('user_id'=>$row->peserta_didik_id,
            'username'=>$row->nisn,
            'password'=>$pass,
            'level'=>'3',
            'login'=>'1','status'=>'1');
          $this->CI->db->insert('app.username',$s_username);
        }
      }
    }
    public function cekSekolah($sekolah_id){
      $ckdata = $this->CI->db->get_where('sekolah_terdaftar',array('sekolah_id'=>$sekolah_id));
      if(count($ckdata->result())>0){
        $this->CI->session->set_userdata('cek_terdaftar_','Y');
      }else{
        $this->CI->session->set_userdata('cek_terdaftar_','N');
      }
    }
    var $skey = "G@W351T35.cz#\xE2\x82\xAC2011GAMESITES\0\0\0";
    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value) {

        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value) {

        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }


    public function dec_enc($action, $string) {
    $output = false;
 
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
 
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
 
    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
 
    return $output;
  }
}