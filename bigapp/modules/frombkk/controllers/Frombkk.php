<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Frombkk extends CI_Controller {

  public function __construct(){
      parent::__construct();
  }
  
  public function index(){
    redirect('frontpage');
  }

  public function detil($id){
    $isi['content'] = "frombkk_view";
     $isi['menu'] = "frombkk";
    $isi['id'] = $id;
    $this->load->view('frontpage/front_view', $isi);
  }

 public function bidang($id){
    $isi['content'] = "bidang_view";
     $isi['menu'] = "frombkk";
    $isi['id'] = $id;
    $this->load->view('frontpage/front_view', $isi);
  }

 public function ldetil($id){
    $isi['content'] = "detil_view";
    $isi['menu'] = "frombkk";
    $isi['id'] = $id;
    $this->load->view('frontpage/front_view', $isi);
  }

  public function lamar(){
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
        $resp="";
        $id = $this->input->post('id');
        $idsiswa = $this->session->userdata('user_id');
        $tgl = date('Y-m-d');
        $ck = $this->db->query("SELECT id_siswa FROM bidding_lulusan WHERE id_loker='$id' and id_siswa='$idsiswa'")->result();
        if(count($ck)>0){
          $resp = "dobel";
        } else {
            $data = array('id_siswa'=>$idsiswa,'tgl_bid'=>$tgl,'status'=>"0",'id_loker'=>$id);
            if($this->db->insert('bidding_lulusan', $data)){
               $resp = "ok";
            } else {
               $resp = "not";
            }
        }
        echo json_encode($resp);
    } else {
      redirect('frontpage', 'refresh');
    }
  }


}