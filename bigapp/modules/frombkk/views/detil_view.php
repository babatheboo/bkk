<!-- <script type="text/javascript">
	function lamar(id){
		var r = confirm("Yakin melamar lowongan ini ?");
		if(r == true ){
			$.ajax({
					 url:"<?php echo base_url(); ?>frombkk/lamar",
		             method:"POST",
		             data:{id:id},
		             cache: false,
		             success:function(data)
					 {
					 	if(data=="ok"){
					 		alert("Lowongan telah dimasukan !");
					 		window.location.href = '<?php echo base_url();?>profile/lamaran';
					 	} else {
					 		alert("Lowongan gagal dimasukan !");
					 		location.reload();
					 	}
					 }
					});
		}
	}
</script> -->

<div id="isi">
<?php
$dt = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE id='$id'")->result();
$sid = "";
if(count($dt)>0){
	$judul = "";
	$tgbuat = "";
	$tgttp = "";
	$namabkk = "";
	$desc = "";
	$gj = "";
	$ft = "";
	$kp = "";
	$buid = "";
	foreach ($dt as $key) {
		$judul = $key->judul;
		$tgbuat = date("d-m-Y", strtotime($key->tgl_buat));
		$namabkk = $key->nama;
		$desc = $key->deskripsi;
		$gj = $key->gaji;
		$tgttp = date("d-m-Y", strtotime($key->tgl_tutup));
		$ft = $key->logo;
		$kp = $key->kode_perusahaan;
		$buid = $key->bidang_usaha_id;
		$sid = $key->sekolah_pembuat;
	}

?>
<section class="job-detail section">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="header-detail">
<div class="col-md-8 col-sm-8 col-xs-8">
<div class="header-content pull-left">
<h3><a href="#"><?php echo $judul;?></a></h3>
<p><span>Tanggal dibuat : <?php echo $tgbuat;?> </span></p>
<p><span>Tanggal berakhir : <?php echo $tgttp;?> </span></p>
<p>Rentang gaji : <strong class="price">
<?php
	$g = $this->db->query("SELECT * from ref.range_gaji WHERE id='$gj'")->result();
	if(count($g)>0){
		foreach ($g as $keys) {
			echo "Rp. " .  $keys->range;
		}
	}
?>
</strong></p>
</div>
</div>

<div class="col-md-4 col-sm-4 col-xs-4">
<div class="detail-company pull-right text-right">
<div class="img-thum">
<img class="img-responsive" src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ft;?>" alt="">
</div>
<div class="name">
<h4><?php echo $namabkk;?></h4>

<?php
$kt = $this->db->query("SELECT a.kota FROM ref.view_wilayah a left join ref.view_sekolah_wilayah b on a.kode_kota=b.kode_kota WHERE b.sekolah_id='$sid' limit 1")->result();
foreach ($kt as $ke) {
	echo "<h5>" . $ke->kota . "</h5>";
}
?>

<p><?php 
$dt = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE sekolah_pembuat='$sid' and tgl_tutup>=now()")->result();
echo count($dt);?> lamaran terbuka</p>
</div>
</div>
</div>

<div class="clearfix"></div>
</div>
</div>


<div class="col-md-8 col-sm-12 col-xs-12">
<div class="content-area">
<div class="clearfix">
<div class="box">
<h4>Deskripsi Lowongan</h4>
<p>
<?php 
	echo $desc;
?>
</p>
<?php 
                    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
                      ?>
                      <a href="javascript:void(0)" onclick="lamar('<?php echo $key->id;?>')" class="btn btn-common">Lamar</a>
                    <?php
                      } else { ?>
                         <a href="<?php echo base_url();?>login/" class="btn btn-common">Lamar</a>
                    <?php  }
?>
</div>
</div>
</div>
<h2 class="medium-title">Lowongan serupa</h2>

<div class="job-post-wrapper">
<div class="row">
<div class="col-md-12">
<?php
  $this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
  $low = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE bidang_usaha_id='$buid' AND tgl_tutup>=now() and id='$id' order BY 
tgl_tutup")->result();
  if(count($low)>0){
    foreach ($low as $key) {
      ?>
          <div class="job-list">
			<div class="thumb">
                <a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $key->logo;?>" alt="" style="width:100px;height:100px"></a>
			</div>
			<div class="job-list-content">
			 <h4><a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>"><?php echo $key->judul;?></a></h4>
<p><?php echo substr($key->deskripsi, 0, 50) . "....." ;?></p>
			<div class="job-tag">
			<div class="pull-left">
			<div class="meta-tag">
			
			<span>Bidang Usaha : <a href="<?php echo base_url();?>frombkk/bidang/<?php echo $key->bidang_usaha_id;?>"><?php echo $key->nama_bidang_usaha;?></a></span><br/>
                      <span>Diposting BKK :<i class="ti-location-pin"></i><a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><?php echo $key->nama;?></a></span><br/>
                      <?php
                        $newDate = date("d-m-Y", strtotime($key->tgl_tutup));
                      ?>
                      <span><i class="ti-time"></i>Lamaran ditutup pada : <?php echo $newDate ;?></span>
			</div>
			</div>

			<div class="pull-right">
			 		<a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>" class="btn btn-common btn-rm">Lihat</a>
            <a href="<?php echo base_url();?>lowongan/lamar/<?php echo $key->id;?>" class="btn btn-common btn-rm">Lamar</a>
			</div>

			</div>
			</div>
			</div>

      <?php
    }
  }
?>

</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
<aside>
<div class="sidebar">
<div class="box">
<h2 class="small-title">Informasi Lainnya</h2>
<ul class="detail-list">
<li>Kode Lowongan<span class="type-posts"><?php echo $id;?></span></li>
<li>Posisi Lowongan<span class="type-posts"><?php echo $key->posisi;?></span></li>
<li>Lokasi Penempatan<span class="type-posts">
<?php
$kt = $this->db->query("SELECT a.prov FROM ref.view_provinsi a left join lowongan_lokasi b on a.kode_prov=b.kode_prov WHERE b.id_loker='$id'")->result();
if(count($kt)>0){
	foreach ($kt as $key) {
		echo $key->prov ;
	}
}
?>
</span>
</li>

<li>
Perusahaan
<span class="type-posts">
<?php
	$ind = $this->db->query("SELECT nama FROM ref.industri where industri_id='$kp'")->result();
	foreach ($ind as $key) {
		echo $key->nama;
	}
?>
</span>
</li>
<?php
$kl = $this->db->query("SELECT * FROM lowongan_syarat WHERE id_loker='$id'")->result();
if (count($kl)>0){
	foreach ($kl as $key ) {
		echo "<li>Jumlah dibutuhkan<span class='type-posts'>" . $key->dibutuhkan . "</span></li>";
		echo "<li>Jenis kelamin<span class='type-posts'> " ;
		if($key->jns_kel==0){
			echo "Laki-Laki" ; 
		} else {
			echo "Perempuan";
		} 
		echo  "</span></li>";
		echo "<li>Tinggi badan<span class='type-posts'>" . $key->tinggi_badan . " CM</span></li>";
		echo "<li>Berat badan<span class='type-posts'>" . $key->berat_badan . " Kg</span></li>";
		echo "<li>Umur<span class='type-posts'>" . $key->umur . " Tahun</span></li>";
		
	}
} else {
	echo "<p>Tidak ada persyaratan khusus</p>";
}

?>
</ul>
</div>
</section>



<?php
} else {

}

?>

</div>
