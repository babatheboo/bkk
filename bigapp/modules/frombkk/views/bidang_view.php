<div id="isi">
<?php 
  		$dn = $this->db->query("SELECT nama_bidang_usaha as nama FROM ref.bidang_usaha WHERE bidang_usaha_id='$id'")->result();
    $namabkk = "";
    if (count($dn)>0){
    	foreach ($dn as $key ) {
     	 $namabkk = $key->nama;
    	}
?>
<section class="find-job section">
	<div class="container">
    	<div class="row">
    	 <div class="col-md-12">
    	 <h2 class="section-title">Lowongan bidang usaha <?php echo $namabkk;?></h2>   
<?php
  $this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
  $low = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() AND bidang_usaha_id='$id' order BY tgl_tutup")->result();
  if(count($low)>0){
    foreach ($low as $key) {
      ?>
          <div class="job-list col-md-12">
              <div class="thumb">
                <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $key->logo;?>" alt="" style="width:100px;height:100px">
              </div>
              <div class="job-list-content">
                <h4><a href="#"><?php echo $key->judul;?></a></h4>
                <p><?php echo substr($key->deskripsi, 0, 50) . "....." ;?></p>
                <div class="job-tag">
                  <div class="pull-left">
                    <div class="meta-tag">
<span>Posisi</span>
                            <span>Bidang Usaha : <a href="<?php echo base_url();?>frombkk/bidang/<?php echo $key->bidang_usaha_id;?>"><?php echo $key->nama_bidang_usaha;?></a></span><br/>
                      <span>Diposting BKK :<i class="ti-location-pin"></i><a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><?php echo $key->nama;?></a></span><br/>
                <?php
                        $newDate = date("d-m-Y", strtotime($key->tgl_tutup));
                      ?>
                      <span><i class="ti-time"></i>Lamaran ditutup pada : <?php echo $newDate ;?></span>
                    </div>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>" class="btn btn-common btn-rm">Lihat</a>&nbsp;&nbsp;
                    <?php 
                    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
                      ?>
                      <a href="javascript:void()" onclick="lamar('<?php echo $key->id;?>')" class="btn btn-common">Lamar</a>
                    <?php
                      } else { ?>
                         <a href="<?php echo base_url();?>login/" class="btn btn-common">Lamar</a>
                    <?php  }
?>
                    
                  </div>
                </div>
              </div>
            </div>

      <?php
    }
  }


?>
</div>
        </div>
     </div>
</section>

<?php 
    } else {
    		echo "Data tidak ditemukan";
    }
    
?>

</div>
