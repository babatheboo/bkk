<div class="row">
  <div class="col-lg-12 ui-sortable">
    <div class="panel panel-inverse">
      <div class="panel-heading ui-sortable-handle">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Validasi Data BKK</h4>
      </div>
      <div class="panel-body"> 
        <div class="table-responsive"> 
          <table id="data-validasi" class="table table-striped table-bordered nowrap" width="100%"> 
            <thead> 
              <tr> 
                <th style="text-align:center" width="1%">No.</th> 
                <th style="text-align:center" width="15%">NPSN</th>
                <th style="text-align:center" width="15%">Nama Sekolah</th>
                <th style="text-align:center" width="20%">No Reg BKK</th>
                <th style="text-align:center" width="10%">No SK Pendirian</th>
                <th style="text-align:center" width="10%">Tgl Upload</th>
                <th style="text-align:center" width="1%">Detail</th>
              </tr> 
            </thead> 
            <tbody> 
            </tbody> 
          </table> 
        </div> 
      </div>

      <!-- modal validasi -->
      <div id="valid-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
            <h4 class="modal-title" id="myModalLabel">Detail Validasi</h4>
          </div>
          <div class="modal-body">
            <b><i class="fas fa-list-alt"></i> No Reg: <label id="izin"></label> <button onclick="izinE(id)" class="btn btn-xs btn-success"><i class="fas fa-pencil-alt"></i></button></b><br>
            <b><i class="fas fa-id-badge"></i> No SK: <label id="sk"></label> <button onclick="skE(id)" class="btn btn-xs btn-success"><i class="fas fa-pencil-alt"></i></button></b>
            <img src="" style="width: 100%;" id="fotoscan">
            <b><i class="fas fa-sticky-note"></i> Catatan:</b><p class="modal-header" name="notif" id="notif"></p>
            <input hidden="" type="text" name="id" id="id">
            <input hidden="" type="text" name="nama" id="nama">
            <input type="text" name="info" id="info" class="form-control m-t-15" placeholder="Masukan Catatan">
          </div>
          <div class="modal-footer">
            <button type="button" onclick="catatan()" class="btn btn-default">Kirim</button>
            <button type="button" onclick="validated()" class="btn btn-primary">Validasi</button>
          </div>
        </div>
      </div>
    </div>

    <!-- modal izin -->
    <div id="izin-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalIzin" aria-hidden="true">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
          <h4 class="modal-title" id="myModalIzin">Perbaiki Nomor Izin</h4>
        </div>
        <div class="modal-body">
          <input class="form-control" type="text" name="izinEdit" id="izinEdit">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" onclick="sipIzin(id)" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>

  <!-- modal sk -->
  <div id="sk-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalSK" aria-hidden="true">
   <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
        <h4 class="modal-title" id="myModalSK">Perbaiki Nomor SK</h4>
      </div>
      <div class="modal-body">
        <input class="form-control" type="text" name="skEdit" id="skEdit">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" onclick="sipSK(id)" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script> 
<script src="<?php echo base_url();?>assets/backend/js/validbyprov.js"></script>