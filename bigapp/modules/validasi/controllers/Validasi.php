<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Validasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('validasi_model');
	}

	public function index(){
		if ($this->session->userdata('level')=='6') {
			$this->_content();
		}else{
			redirect('login','refresh');
		}
	}

	public function _content(){
		$isi['link'] = 'validasi';
		$isi['halaman'] = "Manajemen Validasi Data";
		$isi['page'] = "validasi(array)";
		$isi['judul'] = "Halaman Validasi Data";
		$isi['content'] = "validasi_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}

	public function getValid(){
		if($this->input->is_ajax_request()){
			$list = $this->validasi_model->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[] = $rowx->npsn;
				$row[]  = $rowx->nama;
				$row[]  = $rowx->no_izin_bkk;
				$row[]  = $rowx->no_sk_pendirian;
				$row[]  = $rowx->tgl_upload_ulang;
				switch ($rowx->notif) {
					case '':
					$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" data-toggle="modal" title="Validasi" onclick="validate(\''. 
					$rowx->scan .'\' , \''. $rowx->no_izin_bkk .'\' , \''. $rowx->no_sk_pendirian .'\' , \''. $rowx->id .'\' , \''. $rowx->nama .'\' , \''. $rowx->notif .'\')"><i 
					class="fa fa-eye"></i></a>';
					break;

					default:
					$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" data-toggle="modal" title="Validasi" onclick="validate(\''. 
					$rowx->scan .'\' , \''. $rowx->no_izin_bkk .'\' , \''. $rowx->no_sk_pendirian .'\' , \''. $rowx->id .'\' , \''. $rowx->nama .'\' , \''. $rowx->notif .'\')"><i 
					class="fa fa-eye"></i></a>' . '<i class="fas fa-check-square"></i>';
					break;
				}
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->validasi_model->count_all(),
				"recordsFiltered" => $this->validasi_model->count_filtered(),
				"data"            => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}

	public function validasiData($id){
  if($this->input->is_ajax_request()){
    if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="6"){
	$skrng = date('Y-m-d');
	$vld = $this->session->userdata('role_');
      if($this->db->query("UPDATE sekolah_terdaftar set valid='1', tgl_aktivasi='$skrng', approved_by='$vld' WHERE id='$id'")){
        $resp = "ok";
      }   else {
        $resp = "no";
      }
      echo json_encode($resp);
    } else {
      redirect('_404','refresh');
    }
  }
}
}