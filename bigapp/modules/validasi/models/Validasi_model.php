<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Validasi_model extends CI_Model {
    var $table = 'valid_prov';
    var $column_order = array('nama','no_izin_bkk', 'no_sk_pendirian', 'scan', 'notif', 'id', 'tgl_upload_ulang');
    var $column_search = array('nama'); 
    var $order = array('tgl_upload_ulang' => 'desc');
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    private function _get_datatables_query(){
        $uid = $this->session->userdata('role_');
        $prv = $this->db->query("SELECT kode_prov FROM app.user_dinas WHERE user_id='$uid'")->row();
        $this->db->from($this->table);
        $noi = 'no_izin_bkk IS NOT NULL';
        $nos = 'no_sk_pendirian IS NOT NULL';
        $this->db->where($noi);
        $this->db->where($nos);
        $this->db->where('kode_prov', $prv->kode_prov);
        if($_POST['search']['value']!=''){
            $word = explode(" ", $_POST['search']['value']);
            $formatted_word = array();
            foreach ($word as $keyword) {
                array_push($formatted_word, '('.$keyword.')');
       
            }
            $formulated_word='';
            for ($i=0; $i < count($formatted_word); $i++) { 
            if($i<count($formatted_word)-1){
                    $formulated_word.=$formatted_word[$i].'|';
                }else{
                    $formulated_word.=$formatted_word[$i];
                }
            }
            $where_str='';
            if($formulated_word!=''){
                foreach ($this->column_search as $item) {
                    if($where_str!=''){
                        $where_str.=" or lower(".$item.") similar to lower('%(".$formulated_word.")%')";
                    }else{
                        $where_str.=" lower(".$item.") similar to lower('%(".$formulated_word.")%')";
                    }
                }
                $this->db->where($where_str);
            }
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}