<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rbpw extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
		$this->db1 = $this->load->database('default', TRUE);
 	}
	public function index(){
		if ($this->session->userdata('level')=="5" || $this->session->userdata('level')=="6") {
			$this->_content();
		}else{
			redirect('_404');
		}
	}
	public function _content(){
		$isi['link'] = 'rbpw';
		$isi['halaman'] = "Pengaturan Akun";
		$isi['page'] = "reset(array)";
		$isi['judul'] = "Halaman Pengaturan Akun";
		$isi['content'] = "rbpw_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}

	public function cpw(){
		$pwna = md5(md5($this->input->post('pw2')));
		$email = $this->input->post('email');
		$tlp = $this->input->post('tlp');
		$usr = $this->session->userdata('role_');
		if ($this->db->query("UPDATE app.username SET password='$pwna' WHERE user_id='$usr'")) {
			$this->db->query("UPDATE app.user_dinas SET email='$email', tlp='$tlp' WHERE user_id='$usr'");
			$data['response'] = 'sip';
		}else{
			$data['response'] = 'ups';
		}
		echo json_encode($data);
	}
}

