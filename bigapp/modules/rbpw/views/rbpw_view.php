<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<div class="panel panel-inverse" data-sortable-id="form-validation-2">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo $halaman;?></h4>
	</div>
	<div class="panel-body panel-form">
		<form class="form-horizontal form-bordered" action="javascript:cpw()" method="post">
			<div class="form-group row">
				<div class="col-md-5 col-sm-5">
					<label class="control-label">Password * :</label>
					<input type="password" name="pw1" id="pw1" placeholder="Masukan Password Baru" class="form-control"/>
				<!-- </div> -->
				<!-- </div> -->
				<!-- <div class="form-group"> -->
					<!-- <div class="col-md-5 col-sm-5"> -->
						<label class="control-label">Konfirmasi Password * :</label>
						<input type="password" name="pw2" id="pw2" placeholder="Konfirmasi Password Baru" class="form-control"/>
					</div>
					<div class="col-md-5 col-sm-5">
						<label class="control-label">Email Pengguna * :</label>
						<input type="email" name="email" id="email" placeholder="Masukan email" class="form-control"/>
						<label class="control-label">Nomor Telepon * :</label>
						<input type="text" name="tlp" id="tlp" placeholder="Masukan nomor telepon" class="form-control"/>
					<!-- </div> -->
					<!-- </div> -->
					<!-- <div class="form-group"> -->
						</div>
						<div class="col-md-2 col-sm-2">
							<button onclick="cpw()" type="button" class="btn btn-success btn-lg m-t-40"><i class="fas fa-lg fa-fw m-r-10 fa-save"></i>Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<script type="text/javascript">
			function cpw(){
				var pw1 = $("#pw1").val();
				var pw2 = $("#pw2").val();
				var email = $("#email").val();
				var tlp = $("#tlp").val();
				if (pw1 == '') {
					$.notify({icon:'fas fa-info',message:"Masukan password baru ❌"},{type:"danger",offset:{x:3,y:2}});
				}else if (pw2 == '') {
					$.notify({icon:'fas fa-info',message:"Konfirmasi password baru ❌"},{type:"danger",offset:{x:3,y:2}});
				}else if (pw2!=pw1) {
					$.notify({icon:'fas fa-info',message:"Password tidak sesuai ❌"},{type:"danger",offset:{x:3,y:2}});
				}else if (email == '') {
					$.notify({icon:'fas fa-info',message:"Email tidak boleh kosong ❌"},{type:"danger",offset:{x:3,y:2}});
				}else if (tlp == '') {
					$.notify({icon:'fas fa-info',message:"Nomor telepon tidak boleh kosong ❌"},{type:"danger",offset:{x:3,y:2}});
				}
				else{
					jQuery.blockUI({
						css: { 
							border: 'none', 
							padding: '15px', 
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: 2, 
							color: '#fff' 
						},
						message : 'Sedang memproses data ... '
					});
					$.ajax({
						url: '<?php echo base_url();?>'+ "rbpw/cpw",
						method:"POST",
						data:{pw2:pw2, email:email, tlp:tlp},
						cache: false,
						success:function(data){
							var data = $.parseJSON(data);
							if (data.response=='sip') {
								jQuery.unblockUI();
							$.notify({icon:'fas fa-thumbs-o-up',message:"proses berhasil ✔"},{type:"success",offset:{x:3,y:2}});
							$("#pw1").val('');
							$("#pw2").val('');
							$("#email").val('');
							$("#tlp").val('');
							}else if (data.response=='ups') {
								jQuery.unblockUI();
								$.notify({icon:'fas fa-info',message:"Proses gagal. Coba lagi ❌"},{type:"danger",offset:{x:3,y:2}});
							}
						},
						error: function(data){
							jQuery.unblockUI();
							$.notify({icon:'fas fa-info',message:"Terjadi kesalahan. Ulangi proses ❌"},{type:"danger",offset:{x:3,y:2}});
						}
					});
				}
			}
		</script>