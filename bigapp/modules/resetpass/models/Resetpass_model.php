<?php
class Resetpass_model extends CI_Model {
        public function __construct()
        {
                $this->load->database();
        }
        public function GetRowSkl($keyword) {        
                $this->db->order_by('sekolah_id', 'DESC');
                $this->db->limit('5');
                $this->db->like("nama", $keyword);
                return $this->db->get('ref.sekolah')->result_array();
        }
        public function GetRowNpsn($keyword) {        
                $this->db->order_by('sekolah_id', 'DESC');
                $this->db->limit('5');
                $this->db->like("npsn", $keyword);
                return $this->db->get('ref.sekolah')->result_array();
        }

        function fetch_data($query)
        {
                $sess_sekolah  = $this->session->userdata('role_');
                $this->db->select("*");
                $this->db->from("ref.sekolah");
                $this->db->limit('3');
                if($query != '')
                {
                        $this->db->like('nama', $query);
                        $this->db->or_like('npsn', $query);

                }
                $this->db->order_by('nama', 'ASC');
                return $this->db->get();
        }
}