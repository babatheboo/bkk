<!-- <script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/css/default/notifIt.css">
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery.blockUI.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/resetbyprov.js"></script>
<!-- <script src="<?php echo base_url();?>assets/js/resetpass2.js"></script> -->

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-success" data-sortable-id="form-validation-2">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				</div>
				<h4 class="panel-title">Manajemen Reset Password Per-Username</h4>
			</div>
			<div class="panel-body" >
                <div class="form-group">
                    <div class="col-md-12">
                        <!-- <div class="panel panel-inverse"> -->
                            <div class="row" style="margin-top: 15px;">
                                <form method="post" id="search_form">
                                    <div class="row col-md-12">
                                       <!-- <div class="col-md-6"> -->
                                           <label class="control-label">NPSN * :</label>
                                           <input value="<?php echo $this->session->userdata('kode_prov');?>" placeholder="Masukan NPSN" class="form-control" type="text" name="search_text" id="search_text">
                                       </div>
                                   </form>
                               </div>
                               <!-- </div> -->
                           </div>
                       </div> 
                   </div>
                   <div style="display: none;" id="result"></div>
               </div>
           </div>
       </div>

       <div id="hasil">
           
       </div>
       <div id="hasil_npsn">

       </div>
       <div id="infoReset" class="alert alert-danger m-b-0">
           <h5><i class="fa fa-info-circle"></i> Informasi</h5>
           <p>Password default setelah direset adalah: <strong><i>bkklasjurin</i></strong></p>
       </div>
