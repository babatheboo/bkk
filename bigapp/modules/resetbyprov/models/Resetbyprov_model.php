<?php
class Resetbyprov_model extends CI_Model {
        public function __construct()
        {
                $this->load->database();
                $this->db->query("REFRESH MATERIALIZED VIEW ref.view_sekolah_wilayah");
        }
        function fetch_data($query){
                $uid = $this->session->userdata('role_');
                $prv = $this->db->query("SELECT kode_prov FROM app.user_dinas WHERE user_id='$uid'")->row();
                $this->db->select("*");
                $this->db->from("ref.view_resetprov");
                $this->db->limit('3');
                if($query != '')
                {
                        $this->db->like('nama', $query);
                        $this->db->or_like('npsn', $query);

                }
                $this->db->where("kode_prov",$prv->kode_prov);
                $this->db->order_by('nama', 'ASC');
                return $this->db->get();
        }
}