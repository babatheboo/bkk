<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resetbyprov extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('6');
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('resetbyprov_model');
 	}
	public function index(){
    if ($this->session->userdata('level')=='6') {
		$this->_content();
    }else{
      redirect('login','refresh');
    }
	}
	public function _content(){
		$isi['link'] = 'resetpass';
		$isi['halaman'] = "Manajemen Reset Password";
		$isi['page'] = "resetpass(array)";
		$isi['judul'] = "Halaman Reset Password";
		$isi['content'] = "resetpass_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function rubahbyNpsn($npsn){
		// $ieu = $this->input->get('user_id');
		$data = array('password'=>md5(md5("bkklasjurin")));
		$this->db->where('username',$npsn);
		$this->db->update('app.username',$data);
		redirect('resetpass');
	}

	public function fetch(){
  $output = '';
  $query = '';
  // $this->load->model('ajaxsearch_model');
  if($this->input->post('query')!='')
  {
   $query = $this->input->post('query');
  }
  $data = $this->resetbyprov_model->fetch_data($query);
  $output .= '<span class="label label-default f-s-10"><strong style="color: #000000;"> Hasil pencarian</strong></span>
  <div class="list-group list-group-lg no-radius list-email">
     <div class="list-group-item primary">';
  if($data->num_rows() > 0)
  {
   foreach($data->result() as $row)
   {
    $output .= '
    <table class="table table-striped table-bordered nowrap">
    <tr><td>'. $row->npsn . '</td><td>' . $row->nama . '</td><td align="center" width="5%"><a class="btn btn-xs m-r-5 btn-primary" title="Reset Password" onclick="ceknpsn('.$row->npsn.')" style="color: #ffffff;"><i class="fas fa-cog fa-spin"></i></a></td></tr>';
   }
  }
  else
  {
   $output .= '<span class="label label-primary f-s-10"><strong> Data tidak ditemukan</strong></span>';
  }
  $output .= '</table><br><br>';
  echo $output;
 }
}