<?php
	$saiki = date('Y-m-d');
	if ($saiki>=$a->tgl_kadaluarsa) {
		echo '<div class="alert alert-danger fade show m-b-10">
	<span class="close" data-dismiss="alert">×</span>
	Dokumen anda sudah <b>kadaluarsa</b>! Silahkan upload ulang dokumen.
</div>';
	}else{
		echo '<div class="alert alert-success fade show m-b-10">
	<span class="close" data-dismiss="alert">×</span>
	BKK Anda berhasil divalidasi. Update dokumen jiga sudah melebihi batas kadaluarsa
</div>';
	}
	?>
<div class="row">
	<div class="col-lg-12 ui-sortable" id="divPreview">
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<!-- begin panel-heading -->
			<div class="panel-heading">
				<div id="btnValidasi" class="panel-heading-btn">
					<button class="btn btn-primary btn-xs" onclick="validasiUlang()"><i class="fas fa-pencil-alt"></i> Perbarui Data</button>
				</div>
				<h4 class="panel-title">Data yang anda upload</h4>
			</div>
			<!-- end panel-heading -->
			<!-- begin panel-body -->
			<div class="panel-body">
				<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong><?php echo $a->no_izin_bkk;?></strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong><?php echo $a->no_sk_pendirian;?></strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong><?php echo bulan_indo($a->tgl_kadaluarsa);?></strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<img src='../assets/foto/scan/<?php echo $a->scan;?>' style='width:100%;'/>
					</div>
					</div>
				<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Catatan dari Admin:</label>
					<div class='col-md-6 col-sm-6 alert alert-warning'>
						<p style="text-align: justify;"><?php echo $a->notif;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="divForm" style="display: none;" class="col-lg-6 ui-sortable">
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<!-- begin panel-heading -->
			<div class="panel-heading">
				<h4 class="panel-title">Perbarui data registrasi BKK</h4>
			</div>
			<!-- end panel-heading -->
			<!-- begin panel-body -->
			<div class="panel-body">
				<div class="alert alert-secondary">
					Harap diisi dengan data yang benar, untuk menghindari pemblokiran akun anda !. Hubungi call center BKK jika anda sudah melakukan perbaikan data BKK anda.
				</div>
				<div class="panel-body panel-form">
					<form action="<?php echo base_url();?>dashboard/proses_edit" method="post" enctype="multipart/form-data" data-parsley-validate="true">						
						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">No Reg BKK * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="text" name="no_izin" title="No Izin BKK" value="<?php echo 
set_value('no_izin',isset($default['no_izin']) ? $default['no_izin'] : ''); ?>" id="no_izin" placeholder="Masukan No Registrasi BKK" 
data-parsley-required="true" data-parsley-required-message="Nomor registrasi tidak boleh kosong" class="form-control"/>
								<span style="color:red;"><?php echo form_error('no_izin');?></span>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">SK Pendirian * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="text" title="Surat Keputusan Pendirian BKK" name="sk_pendirian" value="<?php echo set_value('sk_pendirian',isset($default['sk_pendirian']) ? $default['sk_pendirian'] : ''); ?>" id="sk_pendirian" placeholder="Masukan SK Pendirian" class="form-control" data-parsley-required="true" data-parsley-required-message="Nomor SK tidak boleh kosong"/>
								<span style="color:red;"><?php echo form_error('sk_pendirian');?></span>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">Tgl Kadaluarsa * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="date" title="Tanggal Kadaluarsa" name="expired" value="<?php echo set_value('expired',isset($default['expired']) ? $default['expired'] : ''); ?>" id="expired" placeholder="Masukan Tanggal Kadaluarsa" class="form-control" data-parsley-required="true" data-parsley-required-message="Tentukan tanggal kadaluarsa izin BKK"/>
								<span style="color:red;"><?php echo form_error('expired');?></span>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="control-label col-md-4 col-sm-4">Scan Dokumen Izin :<br/>
								<b><span class="small"><font color="red">* Type Format jpg | png <br/> Max Size : 1 Mb</font></span></b></label>
								<div class="col-md-3 col-sm-3">
									<input name="MAX_FILE_SIZE" value="9999999999" type="hidden">
									<input type="file" id="scan" name="scan" data-parsley-required="true" data-parsley-required-message="Pilih scan dokumen"/>
								</div>
							</div>
							<div class="form-group row m-b-15">
								<label class="control-label col-md-3 col-sm-3"></label>
								<div class="col-md-3 col-sm-3">
									<button type="submit" class="btn btn-success btn-sm">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</div>
	<script type="text/javascript">
		function validasiUlang(){
			document.getElementById("divPreview").className = "col-lg-6 ui-sortable";
			$("#btnValidasi").hide('slow');
			$("#divForm").show('slow');
		}
	</script>