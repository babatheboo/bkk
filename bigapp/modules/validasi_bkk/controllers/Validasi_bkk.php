<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validasi_bkk extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('1');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$sid = $this->session->userdata('role_');
		$a = $this->db->query("SELECT * FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->row();
		$isi['namamenu'] = "";
		$isi['page'] = "validasi_bkk";
		$isi['kelas'] = "validasi_bkk";
		$isi['link'] = 'validasi_bkk';
		$isi['halaman'] = "Validasi BKK";
		$isi['judul'] = "Halaman Validasi BKK";
		$isi['a'] = $a;
		if ($a->valid==0 || $a->valid==NULL) {
		$isi['content'] = "validasiB_view";
		}else{
		$isi['content'] = "valid_view";
		}
		$this->load->view("dashboard/dashboard_view",$isi);
	}
}
