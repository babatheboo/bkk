<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<?php
$bln = date('n');
if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
}else{
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
}

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-sortable-id="ui-widget-15">
            <div class="panel-heading">
                <h4 class="panel-title">Data alumni</h4>
            </div>
            <div class="form-group">
                <div class="panel-body">
                    <form method="post" action="">  
                        <div class="row">
                            <div class="col-md-2">
                               <label class="control-label">Data Lulusan :</label><br>
                               <select id="tahun_keluar" name="tahun_keluar" class="form-control form-control-sm">
                                   <option value="" selected="selected">Pilih Tahun Lulusan</option>
                                   <option value="<?php echo $t1;?>"><?php echo $t1;?></option>
                                   <option value="<?php echo $t2;?>"><?php echo $t2;?></option>
                                   <option value="<?php echo $t3;?>"><?php echo $t3;?></option>
                                   <option value="<?php echo $t4;?>"><?php echo $t4;?></option>
                               </select>
                           </div>
                        <!-- <div class="col-md-2">
                            <label class="control-label">Jenis Lulusan :</label><br>
                             <select name="status_siswa" class="form-control form-control-sm" id="status_siswa">
                                <option value="" selected="selected">Semua Lulusan</option>
                                <option value="1">Bekerja</option>
                                <option value="2">Kuliah</option>
                                <option value="3">Berwirausaha</option>
                            </select>
                        </div> -->

                        <div class="col-md-1">
                           <button id="sinBtn" onclick="sinkron('<?php echo $this->session->userdata('role_');?>')" class="btn btn-primary btn-lg" type="button">Sinkron<br>
                            <i class="fas fa-sync"></i></button>
                        </div>
                        <div class="col-md-6 m-l-25" >
                           <div style="height: 80px;" id="beja" class="note note-primary note-with-right-icon">
                            <div class="note-icon"><i class="fa fa-info"></i></div>
                            <div class="note-content text-right">
                                <h4><b>Petunjuk</b></h4>
                                <p>Pilih data lulusan lalu klik tombol <i>sinkron</i> untuk mendapatkan data lulusan terbaru.</p>
                            </div>
                        </div>
                    </div>

                    <!-- <button type="button" onclick="condon()">cek</button>
                    <p id="time"></p> -->

                </div>
            </form>
        </div> 
    </div>
</div>
</div>
</div>



<!-- tbl -->
<ul class="nav nav-pills mb-2" id="tabs" style="display: none;">
    <li class="nav-item">
        <a href="#td" data-toggle="tab" class="nav-link active">
            <span class="d-sm-none">Terdata</span>
            <span class="d-sm-block d-none">Terdata</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="#btd" data-toggle="tab" class="nav-link">
            <span class="d-sm-none">Belum Terdata</span>
            <span class="d-sm-block d-none">Belum Terdata</span>
        </a>
    </li>
</ul>

<!-- <div class="tab-content p-15 rounded bg-white mb-4"> -->
    <!-- begin tab-pane -->
    <div class="tab-pane fade active show" id="td">
        <div class="row" id="tblS" style="display: none;">
            <div class="col-md-12"> 
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fas fa-sync-alt"></i> Reload Data</button> 
                        </div>
                        <h4 class="panel-title">Sinkronisasi Data Lulusan</h4>
                    </div>
                    <div class="panel-body"> 
                        <div class="table-responsive"> 
                            <table id="data-lulusan" class="table table-striped table-bordered nowrap" width="100%"> 
                                <thead> 
                                    <tr> 
                                        <th style="text-align:center" >No.</th> 
                                        <th style="text-align:center" >NISN</th> 
                                        <th style="text-align:center" >Nama Siswa</th> 
                                        <th style="text-align:center" >Jurusan</th>
                                        <th style="text-align:center" >Tahun Keluar</th> 
                                        <th style="text-align:center" >Status</th> 
                                        <th style="text-align:center" >Linearitas</th> 
                                        <th style="text-align:center" >Action</th> 
                                    </tr> 
                                </thead> 
                                <tbody> 
                                </tbody> 
                            </table> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end tab-pane -->
    <!-- begin tab-pane -->
    <div class="tab-pane fade" id="btd">
        <h3 class="m-t-10">Nav Pills Tab 2</h3>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor, 
            est diam sagittis orci, a ornare nisi quam elementum tortor. 
            Proin interdum ante porta est convallis dapibus dictum in nibh. 
            Aenean quis massa congue metus mollis fermentum eget et tellus. 
            Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien, 
            nec eleifend orci eros id lectus.
        </p>
    </div>
    <!-- end tab-pane -->
    <!-- end tab-pane -->
<!-- </div> -->
<!-- /tbl -->

<div id="modal_form" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" name="id" id="id" /> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status Lulusan</label>
                            <div class="col-md-6">
                                <select name="status_siswax" class="selectpicker" style="width:100%" id="status_siswax" data-style="btn-white">
                                    <option selected="selected">Pilih Status</option>
                                    <option value="1">Bekerja</option>
                                    <option value="2">Kuliah</option>
                                    <option value="3">Berwirausaha</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="bekerja_">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Perusahaan/Instansi</label>
                                    <input type="hidden" value="" name="kode"/>
                                    <input name="namaper" placeholder="nama perusahaan" class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Posisi Kerja</label>
                                    <input name="posker" placeholder="masukan posisi kerja" class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Mulai Kerja</label>
                                    <input id="tglker" name="tglker" placeholder="tgl mulai kerja" class="form-control input-sm datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Status Kerja * :</label>
                                    <select name="statusker" id="statusker" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status Kerja --</option>
                                        <option value="1">Aktif</option>
                                        <option value="0">Non Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label">Pekerjaan sesuai dengan bidang keahlian</label>
                                    <div class="col-md-12">
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline" id="ya" value="1" checked="">
                                            <label for="ya">Ya</label>
                                        </div>
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline" id="no" value="0">
                                            <label for="no">Tidak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-form-label">Perjanjian Kerja</label>
                                    <div class="col-md-12">
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline2" id="kontrak" value="1" checked="">
                                            <label for="kontrak">Kontrak</label>
                                        </div>
                                        <div class="radio radio-css radio-inline">
                                            <input type="radio" name="radio_css_inline2" id="tetap" value="0">
                                            <label for="tetap">Tetap</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Bidang Usaha * :</label>
                                    <select class="form-control input-sm" name="bidang" id="bidang">
                                        <option>-- Pilih Bidang Usaha --</option>
                                        <?php $bid = $this->db->query("SELECT * from ref.bidang_usaha")->result();
                                        foreach ($bid as $bdn) {
                                            ?>
                                            <option value="<?php echo $bdn->bidang_usaha_id;?>"><?php echo $bdn->nama_bidang_usaha;?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Lokasi * :</label>
                                    <select name="lokasi" id="lokasi" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Lokasi Penempatan --</option>
                                        <option value="350000">Luar Negeri</option>
                                        <optgroup label="Dalam Negeri">
                                            <?php $prov = $this->db->query("SELECT * FROM ref.view_provinsi")->result();
                                            foreach ($prov as $key) {
                                                $prv = trim($key->kode_prov);
                                                if ($prv!='350000') {

                                                    ?>
                                                    <!-- <option value="1">Luar Negeri</option> -->
                                                    <option value="<?php echo $prv;?>"><?php echo $key->prov;?></option>
                                                <?php } 
                                            } ?></optgroup>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Penghasilan Perbulan * :</label>
                                        <select name="gaji" id="gaji" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Range Gaji --</option>
                                            <option value="<1.000.000">< 1.000.000</option>
                                            <option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
                                            <option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
                                            <option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
                                            <option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
                                            <option value=">5.000.000">> 5.000.000</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                <!-- <div class="form-group col-md-6" id="forProv" style="display: none;">
                                    <label class="control-label">Provinsi * :</label>
                                    <select name="prov" id="prov" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Provinsi Penempatan --</option>
                                        <option value="">Provinsi 1</option>
                                        <option value="">Provinsi 2</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div id="kuliah_">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Perguruan Tinggi</label>
                                    <input name="kodept" type="hidden">
                                    <input name="namapt" placeholder="masukan nama perguruan tinggi " class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Jurusan Kuliah</label>
                                    <input name="jukul" placeholder="masukan jurusan kuliah" class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Mulai Kuliah</label>
                                    <input id="tglkul" name="tglkul" placeholder="Mulai Kuliah" class="form-control input-sm datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Status Kuliah</label>
                                    <select name="statuskul" id="statuskul" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status Kuliah --</option>
                                        <option value="0">Non Aktif</option>
                                        <option value="1">Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>    
                            </div> 
                        </div>
                    </div>
                    <div id="wira_">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Perusahaan</label>
                                    <input name="kodewir" type="hidden">
                                    <input name="namawir" placeholder="masukan nama wira usaha " class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Masukan Jenis Usaha</label>
                                    <input id="jwirakode" name="jwirakode" type="hidden">
                                    <input id="jwira" name="jwira" placeholder="masukan jenis usaha" class="form-control input-sm" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Mulai usaha</label>
                                    <input id="tgl_usaha" name="tgl_usaha" placeholder="Mulai wirausaha" class="form-control input-sm datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Status wira</label>
                                    <select name="status_wira" id="status_wira" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status wira --</option>
                                        <option value="0">Non Aktif</option>
                                        <option value="1">Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Penghasilan Perbulan * :</label>
                                    <select name="penghasilan" id="penghasilan" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Range Penghasilan --</option>
                                        <option value="<1.000.000">< 1.000.000</option>
                                        <option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
                                        <option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
                                        <option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
                                        <option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
                                        <option value=">5.000.000">> 5.000.000</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>  
                        </div>
                    </form>                 
                </div>
            </div>
            <div id="tombol">
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save('lulusan','kerja','#form','#modal_form')" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
<div id="modal_form_status" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form_" class="form-horizontal">
                    <input type="hidden" name="id_na" id="id_na" /> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-8">Nama Perguruan Tinggi</label>
                            <div class="col-md-8">
                                <input name="kodept" type="hidden">
                                <input name="namapt" placeholder="masukan nama perguruan tinggi" readonly="readonly" class="form-control input-sm" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Jurusan Kuliah</label>
                            <div class="col-md-8">
                                <input name="jukul" placeholder="masukan jurusan kuliah" class="form-control input-sm" readonly="readonly" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Mulai Kuliah</label>
                            <div class="col-md-8">
                                <input id="tglkul" readonly="readonly" name="tglkul" placeholder="Mulai Kuliah" class="form-control input-sm datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Status Kuliah</label>
                            <div class="col-md-8">
                                <select name="statuskul" readonly="readonly" id="statuskul" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                    <option value="0">Non Aktif</option>
                                    <option value="1">Aktif</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>    
                    </div> 
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="acc" onclick="acc('lulusan','kerja','#form_','#modal_form_status')" class="btn btn-primary">Save</button>
                <button type="button" id="tolak" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<style>
  #chartdiv {
    width: 100%;
    height: 500px;
}
</style>
<!-- Resources -->
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/lulusan.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<!-- <script src="<?php echo base_url();?>assets/backend/plugins/amcharts/core.js"></script> -->
<!-- <script src="<?php echo base_url();?>assets/backend/plugins/amcharts/charts.js"></script> -->
<!-- <script src="<?php echo base_url();?>assets/backend/plugins/amcharts/animated.js"></script> -->
<script src="<?php echo base_url();?>assets/backend/plugins/timer/easytimer.min.js"></script>
<!-- HTML -->
<!-- <div id="chartdiv"></div> -->
<div class="modal" id="mdlinfo">Mohon tunggu<br/> pengecekan data</div>

<script>

    $('document').ready(function(){
        if($("#tahun_keluar").val()==""){
            $("#sinBtn").attr("disabled", true);
        } else {
            $("#sinBtn").attr("disabled", false);
        }

        $("#tahun_keluar").change(function(){
            var tahun = $("#tahun_keluar").val();
            if(tahun!=""){
                $("#sinBtn").attr("disabled", false);
            } else {
                $("#sinBtn").attr("disabled", true);
            }
        })
    });

    function sinkron(sid){
            var tahun = $("#tahun_keluar").val();
            var waktos = new easytimer.Timer();
            if (tahun!="") {
                var thn = tahun-1;
                $.ajax({
                    url: $BASE_URL + "lulusan/sinkron/",
                    method: "POST",
                    dataType: "json",
                    data : {thn:thn,sid:sid},
                    beforeSend: function(){
                        jQuery.blockUI({
                            css: { 
                                border: 'none', 
                                padding: '15px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: 2, 
                                color: '#fff'
                            },
                            message : 'Mohon menunggu, proses Sinkronisasi sedang berjalan ! <span id="time"></span>.'
                        });
                        waktos.start();

                        waktos.addEventListener('secondsUpdated', function (e) {
                            $('#time').html(waktos.getTimeValues().toString());
                        });
                    },
                    success: function(data){
                        jQuery.unblockUI();
                        var data = $.parseJSON(JSON.stringify(data));
                        alert(data.pesan);
                        table.ajax.reload(null,false); 
                    }
                });
            } else {
                alert("Tahun kelulusan harus dipilih");
            }
    }
</script>
