<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lulusan_model extends CI_Model {
    var $table         = 'ref.peserta_didik a';
  //  var $table = 'view_lulusan_baru';
    var $column_order  = array(null,'nisn','nama','nama_jurusan_sp',"tanggal_keluar",null);
    var $column_search = array('nisn','nama','nama_jurusan_sp',"tanggal_keluar");
    var $order         = array('nama' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    private function _get_datatables_query(){
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->select("a.peserta_didik_id, a.nama, a.nisn,  left(a.tanggal_keluar,4) as tahun, b.nama_jurusan_sp");
        $this->db->from($this->table);
        $this->db->join("ref.jurusan_sp b","a.jurusan_id=b.jurusan_id","left");
        $this->db->where('a.sekolah_id',$sess_sekolah);
        $this->db->where('b.sekolah_id',$sess_sekolah);
        $this->db->where('a.nisn <>','');
        $bln = date('n');
        if($bln<7){
            $t1 = date('Y') - 3;
            $t2 = $t1+1;
            $t3 = $t1+2;
            $t4 = $t1+3;
        }else{
$t1 = date('Y') - 3;
            $t2 = $t1+1;
            $t3 = $t1+2;
            $t4 = $t1+3;
}
        //$this->db->where('a.jenis_keluar_id','1');
        if($this->input->post('tahun_keluar')!=""){
            $this->db->where("left(a.tanggal_keluar,4)",$this->input->post('tahun_keluar'));
        }
       
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        $this->db->where('sekolah_id',$sess_sekolah);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
}
