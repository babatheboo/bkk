<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/js/ticket-view-psmk.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<style>
@media (min-width: 1000px)/* The minimum width of the display area, such as a browser window*/
{
  .modal-dialog {
      width: 1200px;
  }
}
</style>
<style media="all" type="text/css">
  .alignCenter { text-align: center; }  
</style>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> 
			<div class="panel-heading-btn"> 
				<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload Data</button>
				</div> <h4 class="panel-title"><?php echo $halaman;?></h4>
                         </div> 
                         <div class="panel-body"> <div class="table-responsive"> 
	                         <table id="data-ticket" class="table table-striped table-bordered nowrap" width="100%"> 
	                         	<thead> 
	                         		<tr> 
	                         			<th style="text-align:center" width="1%">No.</th> 
	                         			<th style="text-align:center" width="20%">Id Ticket</th> 
	                         			<th style="text-align:center" width="20%">Tujuan Pengajuan</th> 
	                         			<th style="text-align:center" width="30%">Judul Pengajuan</th>
	                         			<th style="text-align:center" width="10%">Tanggal</th>
	                         			<th style="text-align:center" width="30%">Status</th> 
	                         			<th style="text-align:center" width="30%">Detail</th> 
	                         		</tr> 
	                         	</thead> 
	                         		<tbody> 
	                         		</tbody> 
	                         </table>
                          </div> 
                       </div>
                    </div> 
                </div>
</div>

<div id="modal-ticket" class="modal fade">
    <div class="modal-dialog">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Buat Ticket</h4>
            	<div class="alert alert-warning fade in m-b-15">
								<strong>Warning!</strong>
								informasi baca terlebih dahulu sebelum melakukan pengajuan ticket	 <br/> 
								- untuk pengajuan tentang seputar web bisa di ajukan ke technical support <br/> 
								- untuk pengajuan tentang seputar kebijakan di ajukan ke bagian psmk <br/> 
								- untuk pengajuan tentang seputar ketenaga kerjaaan di ajukan ke bagian depnaker <br/> 
							</div>
            </div>
            <div class="form-body">
            <form action="#" id="form-ticket" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Judul Ticket</label>
                                    <div class="col-md-10">
                                        <input id="judul-ticket" name="judul-ticket" placeholder="masukan judul ticket " class="form-control input-sm" type="text" >
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Deskripsi Ticket</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="desc" name="desc" placeholder="Textarea" rows="5"></textarea>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Ditujukan</label>
                                    <div class="col-md-10">
                                        <select name="tujuan" id="tujuan" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Tujuan --</option>
                                            <option value="4">PSMK</option>
                                            <option value="5">KEMENAKER</option>
                                            <option value="99">SUPPORT</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>    
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('data_ticket','ticket','#form-ticket','#modal-ticket')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>