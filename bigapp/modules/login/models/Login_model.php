<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}

	function login($username,$password){
		$ceklogin = $this->db->get_where('app.username',array('username'=>$username,'password'=>$password,'login'=>'1'));
		if(count($ceklogin->result())>0){
			$rows 		= $ceklogin->row();
			$level 		= $rows->level;
			$userid 	= $rows->user_id;
			$haspass 	= trim($rows->password);

			if($level=='0'){
				$userid = $rows->user_id;
				$ckdata = $this->db->query("SELECT * FROM app.user_psmk WHERE user_id = '$userid'");
				$row = $ckdata->row();
				$sekolah = "SUPERADMIN";
				$nama_sekolah = "SUPERADMIN";
				$nama = $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'nama_'     =>$nama_sekolah,
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);

			}elseif($level=='1'){
				$ckdata 		= $this->db->query("SELECT * FROM app.user_sekolah WHERE user_id = '$userid'");
				$row 			= $ckdata->row();
				$sekolah 		= $row->sekolah_id;
				$cekskl 		= $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id = '$sekolah'");
				$x            	= $cekskl->row();
				$nama_sekolah 	= $x->nama;
				$nama         	= $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'nama_'     =>$nama_sekolah, 
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}elseif ($level=='2') {
				$userid = $rows->user_id;
				$ckdata = $this->db->query("SELECT * FROM app.user_industri WHERE user_id = '$userid'");
				$row = $ckdata->row();
				$industri_id = $row->industri_id;
				$nama = $row->nama;
				$cekindustri = $this->db->query("SELECT * FROM ref.industri WHERE industri_id = '$industri_id'");
				$x = $cekindustri->row();
				$nama_industri = $x->nama;
				$newdata = array('level'=>$level,
					'login'=>TRUE,
					'user_id'=>$userid,
					'username'=>$username,
					'nama_'=>$nama_industri, 
					'role_'=>$industri_id,
					'nama_user'=>$nama);
				$this->session->set_userdata($newdata);
			}elseif ($level=='3') {
				$userid_ 	= $rows->username;
				$ckdata 	= $this->db->query("SELECT a.nama as nama_siswa,a.peserta_didik_id,a.sekolah_id,b.nama as nama_sekolah, a.email  FROM ref.peserta_didik a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.nisn = '$userid_' LIMIT 1");
				$row 		= $ckdata->row();
				$nama 		= $row->nama_siswa;
				$userid 	= $row->peserta_didik_id;
				$nama_ 		= $row->nama_sekolah;
				$sekolah_id = $row->sekolah_id;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$userid_,
					'nama_'     =>$nama_, 
					'role_'     =>$sekolah_id,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}elseif ($level=='4') {
				$userid 		= $rows->user_id;
				$ckdata 		= $this->db->query("SELECT * FROM app.user_psmk WHERE user_id = '$userid'");
				$row 			= $ckdata->row();
				$sekolah 		= $row->user_id;
				$nama_sekolah 	= "ADMIN PSMK";
				$nama 			= $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'nama_'     =>$nama_sekolah, 
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}elseif ($level=='5') {
				$userid 		= $rows->user_id;
				$ckdata 		= $this->db->query("SELECT * FROM app.user_dinas WHERE user_id = '$userid'");
				$row 			= $ckdata->row();
				$sekolah 		= $row->user_id;
				$nama_sekolah 	= $row->nama;
				$nama 			= $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'nama_'     =>$nama_sekolah, 
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}elseif($level=='6'){
				$userid 		= $rows->user_id;
				$ckdata 		= $this->db->query("SELECT * FROM app.user_dinas WHERE user_id = '$userid'");
				$row 			= $ckdata->row();
				$sekolah 		= $row->user_id;
				$prov 			= $row->kode_prov;
				$nama 			= $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'provinsi' 	=>$prov, 
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}elseif($level=='7'){
				$userid 		= $rows->user_id;
				$ckdata 		= $this->db->query("SELECT * FROM app.user_psmk WHERE user_id = '$userid'");
				$row 			= $ckdata->row();
				$sekolah 		= $row->user_id;
				$nama 			= $row->nama;
				$newdata = array(
					'level'     =>$level,
					'login'     =>TRUE,
					'user_id'   =>$userid,
					'username'  =>$username,
					'role_'     =>$sekolah,
					'nama_user' =>$nama);
				$this->session->set_userdata($newdata);
			}
			return true;
		}else{
			return false;
		}
	}
}
