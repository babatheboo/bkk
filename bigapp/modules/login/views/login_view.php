<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">    
<meta name="author" content="BKK">
<title>Bursa Kerja Khusus</title>    
<link rel="shortcut icon" href="<?php echo base_url();?>assets/frontpage/img/favicon.ico">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/bootstrap.min.css" type="text/css">    
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/jasny-bootstrap.min.css" type="text/css">  
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/bootstrap-select.min.css" type="text/css">  
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/material-kit.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/fonts/font-awesome.min.css" type="text/css"> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/fonts/themify-icons.css"> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/animate.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/owl.theme.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/settings.css" type="text/css"> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/slicknav.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/main.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/responsive.css" type="text/css">
<link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontpage/css/colors/blue.css" media="screen" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
</head>

<body> 

<div class="container" style="margin-top:30px">
<div class="row">
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-6 cd-user-modal">
<div class="info">
Silahkan login dengan menggunakan username dan password yang anda miliki.<br/>
Jika belum mendapatkan username dan password silahkan lakukan pendaftaran dengan mengklik link Daftar.<br/>
</div>
</div>
</div>
<br/>

<div class="row">
<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-6 cd-user-modal">
<div class="my-account-form">
<ul class="cd-switcher">
<li><a class="selected" href="#0">LOGIN</a></li>
<li><a href="#0">DAFTAR</a></li>
</ul>
<div id="cd-login" class="is-selected">
<div class="page-login-form">

<form  id="flogin" method="post" action="javascript:login()">
<div class="form-group is-empty">
<div class="input-icon">
<i class="ti-home" style="margin-top: -5px;"></i>
<input type="text" autofocus="" id="username" class="form-control" name="username" placeholder="NPSN">
</div>
<span class="material-input"></span></div>
<div class="form-group is-empty">
<div class="input-icon">
<i onclick="toong()" class="ti-eye" style="margin-top: -4px;"></i>
<input type="password" class="form-control" id="password" name="password" placeholder="Password">
</div>
<span class="material-input"></span></div>
<!-- <button class="btn btn-common log-btn" onclick="login()" type="button">Login</button> -->
<input type="submit" name="simpan" value="Login" class="btn btn-common log-btn">
<div class="checkbox-item">
<div class="checkbox">
<label for="rememberme" class="rememberme">
<input name="rememberme" id="rememberme" value="forever" type="checkbox">
Ingat saya</label>
</div>
<p class="cd-form-bottom-message"><a href="#0">Lupa username / password ?</a></p>
</div>
</form>
<script>
function toong() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>

</div>
</div>

<div id="cd-signup">
<div class="page-login-form register">

<form id="fregister" method="post" class="form-ad" action='javascript:daftar()'>

<div class="form-group is-empty">
<label class="control-label">Nama Lengkap</label>
	<input type="text" id="nama" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
<span class="material-input"></span>
</div>

<div class="form-group is-empty">
<label class="control-label">NISN</label>

<input type="text" id="nisn" class="form-control" id="nisn" name="nisn" placeholder="NISN">
<span class="material-input"></span></div>

<div class="form-group is-empty">
<label class="control-label">Tanggal Lahir</label>
<input type="date" data-date-format="dd-mm-yyyy" onclick="(this.type='date')" placeholder="Tanggal Lahir" class="form-control" id="tgllahir" name="tgllahir">
<span class="material-input"></span>
</div>

<div class="form-group is-empty">
<label class="control-label">Email</label>
<input type="email" class="form-control" name="email" id="email" placeholder="Email">
<span class="material-input"></span>
</div>

<div class="form-group">
<label class="control-label">Provinsi</label>
<select name="prov" id="prov" class="dropdown-product selectpicker" data-show-subtext="true" data-live-search="true" tabindex="-9">
<option value="">Pilih provinsi</option>
<?php
$sk = $this->db->query("select kode_prov, prov from ref.view_wilayah where kode_prov!='350000' group by kode_prov, prov order by prov asc ")->result();
foreach($sk as $key){
   echo "<option  value='" . $key->kode_prov . "'>" . $key->prov . "</option>";
}
?>
</select>
<span class="material-input"></span>
</div>


<div class="form-group">
<label  class="control-label">Kab/Kota</label>
<select name="kotax" id="kotax"  class="dropdown-product selectpicker" data-show-subtext="true" data-live-search="true">
	<option value="">Pilih Kab/Kota</option>
</select>
<span class="material-input"></span>
</div>



<div class="form-group is-empty">
<label  class="control-label">Pilih asal sekolah</label>
<select name="sekolah" id="sekolah"  class="dropdown-product selectpicker" data-show-subtext="true" data-live-search="true">
<option>Pilih asal sekolah</option>

</select>
<span class="material-input"></span>
</div>


<button class="btn btn-common log-btn" type="submit">Register</button>
</form>


</div>
</div>
<div class="page-login-form" id="cd-reset-password"> 
<p class="cd-form-message">Kesulitan masuk? masukan email yang anda daftarkan</p>
<form action='javascript:doKonfir()' autocomplete='off' method="POST" class="cd-form">
	<select name="jns" id="jns" class="dropdown-product selectpicker"> 
		<option value="">Jenis Pengguna</option>
		<option value="1">Admin BKK</option>
		<option value="2">Alumni SMK</option>
	</select>
<div class="form-group is-empty">
<div class="input-icon">
<i class="ti-user"></i>
<input type="text" id="unameKonfir" class="form-control" name="unameKonfir" placeholder="NPSN/NISN">
</div>
<div class="input-icon">
<i class="ti-email"></i>
<input type="email" id="emailKonfir" class="form-control" name="emailKonfir" placeholder="Email">
</div>
<span class="material-input"></span></div>
<p class="fieldset">
<button class="btn btn-common log-btn" type="submit">Reset password</button>
</p>
</form>
<p class="cd-form-bottom-message"><a href="#0">Kembali</a></p>
</div> 
</div>

</div>
</div>
</div>
<div class="modal" id="mdlinfo">Mohon tunggu<br/> pengecekan data</div>

<!-- Main JS  -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-3.3.1.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/material-kit.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.parallax.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.slicknav.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/main.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/contact-form-script.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/infinite-scroll.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/frontpage/js/logReg.js"></script>

<script>
	
</script>


</body>
</html>
