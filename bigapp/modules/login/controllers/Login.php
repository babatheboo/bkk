<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->library('email');
    $this->load->model('login_model');
  }
  
  

  public function index(){
  	if($this->session->userdata('login')==false){
  		$this->load->view('login_view');
  	} else {
      if($this->session->userdata('level')==3){
        redirect('frontpage','refresh');
      } else {
        redirect('dashboard','refresh');
      }
    }
  }


  public function doLogin(){
    $username = $this->asn->anti($this->input->post('username'));
    $password = md5(md5($this->asn->anti($this->input->post('password'))));
    $result  = $this->login_model->login($username,$password); 
    if($result==TRUE){
      $data['response'] = 'true';
    }else{
      $data['response'] = 'false';
    }
        // if ('IS_AJAX') {
    echo json_encode($data); 
        // }
  }


  public function doLogout(){
    $this->session->sess_destroy();
    redirect("frontpage", "refresh");
  }


 public function cek_kota($kode=Null){
    $ckdata = $this->db->query("select * from ref.view_kota where kode_prov='".$kode."' ORDER BY kota ASC")->result();
    $data = '<div class="form-group is-empty"><select id="id_kota" class="dropdown-product selectpicker" onchange="cek_sekolah(\'id_kota\',\'daftar_sekolah\',\'cek_sekolah\');"><option value="">Pilih Kota</option>';
    
    foreach($ckdata as $key){
      $data.='<option value="'.$key->kode_kota.'">'.$key->kota.'</option>';
    }
    $data.='</select></div>';

    if('IS_AJAX'){
        echo $data; //echo json string if ajax request
      }  
    }


    public function getkota(){
      $id = $this->input->post('idpr');
      $dt = $this->db->query("select kode_kota, kota from ref.view_wilayah where kode_prov!='350000' and kode_prov='$id' group by kode_kota, kota order by kota  asc  ")->result();
      $html = "<option>Pilih Kab/Kota</option>";
      foreach ($dt as $key) {
        $html .= "<option value='" . $key->kode_kota . "'>". $key->kota . "</option>";
      }
      echo $html;
    }

    public function getsekolah(){
      $id = $this->input->post('idpr');
      $dt = $this->db->query("select sekolah_id, nsekolah From ref.view_sebaran where kode_kota='$id' group by sekolah_id, nsekolah order by nsekolah")->result();
        $html = "<option>Pilih Asal Sekolah</option>";
      foreach ($dt as $key) {
        $html .= "<option value='" . $key->sekolah_id . "'>". $key->nsekolah . "</option>";
      }

      echo $html;
    }

    public function register(){
      $nama      = $this->input->post('nama');
      $nisn      = $this->input->post('nisn');
      $tgl       = $this->input->post('tgllahir');
      $email     = $this->input->post('email');
      $tgl_lahir = date("Y/m/d", strtotime($tgl));
      $sekolahid = $this->input->post('sekolah');
      $data = array();

    $status = 0;
    $newDate = date("Y/m/d", strtotime($tgl));
    $dt = $this->db->query("select * From ref.peserta_didik where nisn='$nisn'")->result();
    if(count($dt)>0){
      foreach ($dt as $key) {
        $uid = $key->peserta_didik_id;
        $ttl = date("Y/m/d", strtotime($key->tanggal_lahir));
        if($newDate==$ttl){
          if(strtoupper($nama)==strtoupper($key->nama)){
             if($key->sekolah_id==$sekolahid){
                $status = 1;
// cek di username
                $cekuname = $this->db->query("SELECT * FROM app.username WHERE username='$nisn'")->result();
                if(count($cekuname)>0){
                  $rp = ($this->rndmPass(8,1,"lower_case,upper_case,numbers,special_symbols")); // generate password
                  $pastomail = $rp[0];
                  $pastostore = md5(md5($pastomail));
                  $this->db->where('username', $nisn);
                  $dup = array('password'=> $pastostore);
                  if($this->db->update('app.username', $dup)){
                    $this->kirimEmail($nisn, $pastomail, $email);
                    $status = 1;
                  } else {
                    $status = 0;
                  }
                  
                } else {
                  $rp = ($this->rndmPass(8,1,"lower_case,upper_case,numbers,special_symbols")); // generate password
                  $pastomail = $rp[0];
                  $pastostore = md5(md5($pastomail));
                  $dup = array('user_id'=>$uid,'password'=> $pastostore,'level'=>'3','login'=>'1', 'status'=>'1','username'=>$nisn);
                  if($this->db->insert('app.username', $dup)){
                     $this->kirimEmail($nisn, $pastomail, $email);
                     $status = 1;
                  } else {
                    $status = 0;
                  }
                }

              } else {
                $status = 0;
              }
            } else {
              $status = 0;
            }
          } else {
            $status = 0;
         }
      }
    } else {

        $data = $this->getUser($nisn);
        if(!empty($data))
        {
          foreach ($data as $key) 
          {

            $id = trim($key['peserta_didik_id']);
            $b = $key['nama'];
            $c = $key['jenis_kelamin'];
            $d = $key['nisn'];
          //  $e = $key['nik'];
            $f = $key['tempat_lahir'];
            $g = $key['tanggal_lahir'];
            $h = $key['agama_id'];
            $i = $key['kebutuhan_khusus_id'];
            $j = $key['sekolah_id'];
            $k = $key['jenis_keluar_id'];
            $l = $key['tanggal_masuk_sekolah'];
            $m = $key['tanggal_keluar'];
            $n = $key['jurusan_id'];

            $b = trim($b);
            $c = trim($c);
            $d = trim($d);
         //   $e = trim($e);
            $f = trim($f);
            $g = trim($g);
            $h = trim($h);
            $i = trim($i);
            $j = trim($j);
            $k = trim($k);
            $l = trim($l);
            $m = trim($m);
            $n = trim($n);
            if($j==$sekolahid ){
              $dataInsert = array(
                      'peserta_didik_id' =>$id,
                      'nama' =>$b,
                      'jenis_kelamin' =>$c,
                      'nisn' =>$d,
                 //     'nik' =>$e,
                      'tempat_lahir' =>$f,
                      'tanggal_lahir' =>$g,
                      'agama_id' =>$h,
                      'kebutuhan_khusus_id' =>$i,
                      'sekolah_id'=>$j,
                      'jenis_keluar_id'=>$k,
                      'tanggal_masuk_sekolah' =>$l,
                      'tanggal_keluar'=>$m,
                      'jurusan_id'=>$n,
                      'email'=>$email,
                );
             $this->db->insert('ref.peserta_didik',$dataInsert);
             $rp = ($this->rndmPass(8,1,"lower_case,upper_case,numbers,special_symbols")); // generate password
             $pastomail = $rp[0];
             $pastostore = md5(md5($pastomail));
             $dup = array('user_id'=>$uid,'password'=> $pastostore,'level'=>'3','login'=>'1', 'status'=>'1','username'=>$nisn);
             if($this->db->insert('app.username', $dup)){
               $this->kirimEmail($nisn, $pastomail, $email);
               $status = 1;
             } else {
               $status = 0;
             }
            } else {
              $status = 0;
            }
            
          }
        } else {
          $status = 0;
        }
   }

    if($status==0){
        $data['response'] = "FALSE";
        $data['pesan'] = "Informasi yang anda masukan tidak ditemukan, cek kembali data yang anda masukan !";
    } else {
        $data['response'] = "TRUE";
        $data['pesan'] = "Silahkan cek email anda untuk informasi login di website ini, Terima Kasih";
    }
    
    echo json_encode($data);
}
    

    function getUser($nisn){
      $url = "http://172.18.4.11/dapodik/webapi.php?uid=bkk&upa=1234&utab=siswa_bkk&nisn=$nisn";
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $cexecute=curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($httpcode==200)
        {
          $data = json_decode($cexecute,true);
        }
        return $data;
    }
    function kirimEmail($username, $password, $email){
      $config = array(
        'protocol' => 'sendmail',
        'smtp_host' => 'smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => 'azharnauffal@gmail.com',
        'smtp_pass' => '[babayagatheboogeyman]',
        'smtp_timeout' => '4',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1'
      );
      $this->email->initialize($config);
      $this->email->from('no-reply@bkk.ditpsmk.net', 'Link ganti password');
      $this->email->to($email);
      $this->email->subject('Bursa Kerja Khusus | Informasi Login Bursa Kerja Khusus');
      $message = '<html>
      Silahkan login menggunakan informasi sebagai berikut :<br/>
      Username :' . $username . '<br/>
      Password :' . $password . '<br/>
      <p>Setalah berhasil login, silahkan ganti password untuk keamanan user anda, terima kasih</p>
      </html>';
      $this->email->message($message);
      $this->email->send();
    }


    function rndmPass($length,$count,$characters){
      $symbol = array();
      $passwords = array();
      $used_symbols = '';
      $pass = '';
      $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
      $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $symbols["numbers"] = '1234567890';
      $symbols["special_symbols"] = '!?~@#-_+<>[]{}';

    $characters = explode(",",$characters); // get characters types to be used for the passsword
    foreach ($characters as $key=>$value) {
        $used_symbols .= $symbols[$value]; // build a string with all characters
      }
    $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

    for ($p = 0; $p < $count; $p++) {
      $pass = '';
      for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $symbols_length); // get a random character from the string with all characters
            $pass .= $used_symbols[$n]; // add the character to the password string
          }
          $passwords[] = $pass;
        }
    return $passwords; // return the generated password
  }
}
?>
