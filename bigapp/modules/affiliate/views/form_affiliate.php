<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/affiliate.js"></script>
<div class="row">
<div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"><button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload Data</button> </div> <h4 class="panel-title">List Sekolah Terdaftar</h4>
                        </div> <div class="panel-body"> <div class="table-responsive"> <table id="data-sekolah" class="table table-striped table-bordered nowrap" width="10%"> <thead> <tr> <th width="1%" style="text-align:center">No</th> <th style="text-align:center" width="15%">NPSN</th><th style="text-align:center" width="50%">Nama Sekolah</th><th style="text-align:center" width="40%">Alamat</th><th style="text-align:center" width="10%">Action</th></tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>
<div class="modal modal-message fade" id="detil_mitra">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">List Mitra Industri <?php echo $this->session->userdata('sess_nama_sekolah');?></h4>
			</div>
			<div class="modal-body">
				<div id="hasil"></div>
			</div>
			<!-- <div class="modal-footer">
				<a href="javascript:;" onclick="app('<?php echo $this->session->userdata("sek");?>')" class="btn btn-sm btn-primary">Approve</a>
			</div> -->
		</div>
	</div>
</div>