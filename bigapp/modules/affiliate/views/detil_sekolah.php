<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/mitra_aliansi.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"> <button class="btn btn-danger btn-xs m-r-5" onclick="filter_data()"><i class="fa fa-search"></i> Filter Pencarian</button>&nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload Data</button> </div> <h4 class="panel-title"><?php echo $halaman;?></h4>
<div id="filter_pencarian">
                        <br/>
                        <?php echo form_dropdown('bidangx',$option_bidangx,isset($default['bidangx']) ? $default['bidangx'] : '','class="default-select2 form-control" style="width:25%" id="bidangx" name="bidangx" data-live-search="true" data-style="btn-white"');?>
                        </div>
                         </div> <div class="panel-body"> <div class="table-responsive"> <table id="data-mitra" class="table table-striped table-bordered nowrap" width="100%"> <thead> <tr> <th style="text-align:center" width="1%">No.</th> <th style="text-align:center" width="20%">Nama Industri</th> <th style="text-align:center" width="20%">E-mail</th> <th style="text-align:center" width="10%">No Telepon</th><th style="text-align:center" width="30%">Bidang Usaha</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>