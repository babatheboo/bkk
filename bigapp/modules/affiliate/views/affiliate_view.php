<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script> -->
<script src="<?php echo base_url();?>assets/backend/js/aliansi.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/aliansi_a.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/aliansi_b.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
  $(document).ready(function() {
    FormPlugins.init();
  });
</script>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <div class="input-group">
       <span class="input-group-addon"><i class="fa fa-search"></i> Pencarian</span>
       <input type="text" name="search_text" id="search_text" placeholder="Masukan Nama Sekolah / NPSN" class="form-control" />
     </div>
   </div>
   <br />
   <div style="display: none;" id="result"></div>
 </div>
</div>

<!-- n -->
<ul class="nav nav-tabs">
  <li class="nav-items">
    <a href="#aff" data-toggle="tab" class="nav-link active">
      <span class="d-sm-none">Affiliasi BKK</span>
      <span class="d-sm-block d-none">Affiliasi BKK</span>
    </a>
  </li>
  <li class="nav-items">
    <a href="#mohon" data-toggle="tab" class="nav-link">
      <span class="d-sm-none">Permohonan Affiliasi</span>
      <span class="d-sm-block d-none">Permohonan Affiliasi</span>
    </a>
  </li>
  <li class="nav-items">
    <a href="#minta" data-toggle="tab" class="nav-link">
      <span class="d-sm-none">Permintaan Affiliasi</span>
      <span class="d-sm-block d-none">Permintaan Affiliasi</span>
    </a>
  </li>
</ul>
<div class="tab-content">
  <!-- begin tab-pane -->
  <div class="tab-pane fade active show" id="aff">
    <div class="col-md-12">
      <h4 class="text-center"><i class="fas fa-handshake"></i> Daftar Affiliasi BKK</h4>
      <div class="panel-body">
        <div id="load_aff"></div>
        <blockquote style="display: none;" id="eep">Tidak Ada Affiliasi Lain</blockquote>
        <span><center><img style="display: none;" id="loaderAff" src="<?php echo base_url();?>assets/img/gifload.gif"></center></span>
      </div>
    </div>
  </div>
  <!-- end tab-pane -->
  <!-- begin tab-pane -->
  <div class="tab-pane fade" id="mohon">
    <div class="col-md-12">
      <h4 class="text-center"><i class="fas fa-building"></i> Daftar Permohonan Affiliasi</h4>
      <div class="panel-body">
        <div id="load_mohon"></div>
        <blockquote style="display: none;" id="seep">Tidak Ada Permohonan Affiliasi Lain</blockquote>
        <span><center><img style="display: none;" id="loaderMohon" src="<?php echo base_url();?>assets/img/gifload.gif"></center></span>
      </div>
    </div>
  </div>
  <!-- end tab-pane -->
  <!-- begin tab-pane -->
  <div class="tab-pane fade" id="minta">
    <div class="col-md-12">
      <h4 class="text-center"><i class="fas fa-tasks"></i> Daftar Permintaan Affiliasi</h4>
      <div class="panel-body">
        <div id="load_minta"></div>
        <blockquote style="display: none;" id="abis">Tidak Ada Permintaan Affiliasi Lain</blockquote>
        <span><center><img style="display: none;" id="loaderMinta" src="<?php echo base_url();?>assets/img/gifload.gif"></center></span>
      </div>
    </div>
</div>
<!-- end tab-pane -->
</div>
<!-- / -->

<!-- <div class="row"> 


</div> -->
<div class="col-md-12">
  <div class="panel-body">
    <div class="recommend-list">
      <span class="label label-primary f-s-10"><strong style="font-size: 13px;"> Rekomendasi Affiliasi</strong></span>
      <div class="loader__ m-t-15"></div>
    </div>
    <span><center><img style="display: none; width: 100px;" id="loader__" src="<?php echo base_url();?>assets/img/gifload.gif"></center></span>
  </div>
</div>
<?php
if($tot_recom>10){
  ?>
  <button id="reload_recom_" data-val = "0" style="text-align: center;width: 100%" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Lainnya</button>
  <?php
}
?>

<div class="modal modal-message fade" id="detil_mitra">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">List Mitra Industri <?php echo $this->session->userdata('sess_nama_sekolah');?></h4>
      </div>
      <div class="modal-body">
        <div id="hasil"></div>
      </div>
            <!-- <div class="modal-footer">
                <a href="javascript:;" onclick="approve_('<?php echo $this->session->userdata("sek");?>')" class="btn btn-sm btn-primary">Approve</a>
              </div> -->
            </div>
          </div>
        </div>