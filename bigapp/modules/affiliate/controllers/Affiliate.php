<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Affiliate extends CI_Controller {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("1");
  		$this->db1 = $this->load->database('default', TRUE);
  		$this->load->model('affiliate_model');
  		$this->load->model('affiliate_a_model');
      $this->load->model('affiliate_b_model');
  		$this->load->model('list_affiliate');
  		$this->load->model('mitra_aff_model');
  		// $this->load->model('RecommendAff_model');
  		$this->load->helper('form');
  	}
  	public function index(){
  		$this->_content();
  	}
  	public function _content(){
  		$lvl                           = $this->session->userdata('level');
      $sess                          = $this->session->userdata('role_');
      $isi['kelas']                  = "affiliate";
      $isi['namamenu']               = "Data Affiliate";
      $isi['page']                   = "mitra";
      $isi['link']                   = 'affiliate';
      $isi['option_kota_filter'][''] = "Pilih Kab / Kota";
      $isi['tot_recom']			   = $this->db->query("SELECT * FROM view_aliansi WHERE status = '0'")->num_rows();
      $ckpropinsi                    = $this->db1->query("SELECT * FROM ref.view_provinsi")->result();
      if(count($ckpropinsi)>0){
       foreach ($ckpropinsi as $row) {
        $isi['option_provinsi_filter'][trim($row->kode_prov)] = $row->prov;
      }
    }else{
     $isi['option_provinsi_filter'][''] = "Data Propinsi Belum Tersedia";
   }
   $isi['halaman'] = "Data Affiliate";
   $isi['judul'] = "Halaman Data Affiliate";
//   $isi['content'] = "affiliate_view";
 $sid = $this->session->userdata('role_');
        $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
        $valid = "";
        foreach ($b as $key) {
          $valid = $key->valid;
        }
        if($valid=="1"){
          $isi['content']              = "affiliate_view";
        } else {
          $isi['content']              = "admin_bkk";
        }


   $this->load->view("dashboard/dashboard_view",$isi);
 }
 public function getData(){
  if($this->input->is_ajax_request()){
    $sess = $this->session->userdata('role_');
    $npsn = $this->session->userdata('username');
    $output = '';
    $data = $this->affiliate_model->fetch_data($this->input->post('limit'), $this->input->post('start'));
    $output .='
    <ul class="media-list media-list-with-divider">
    <div class="row">';
    if($data->num_rows() > 0)
    {
     foreach($data->result() as $row)
     {
      $output .= '
      <div class="col-md-6">
      <blockquote>
      <li class="media media-sm">
      <a class="media-left" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" href="#detil_mitra">
      <img src="../assets/foto/bkk/'.$row->logo.'" alt="" class="media-object">
      </a>
      <div class="media-body">
      <h5 class="media-heading">'.$row->nama.'</h5>
      <ul class="list-unstyled">
      <li><small class="semi-bold">'.$row->npsn.'</small></li>
      <li><small><a style="color:inherit;text-decoration:none;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></small></li>
      <a href="#detil_mitra" style="color: #fff" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" class="btn btn-sm btn-warning pull-right" title="Lihat Mitra Industri '.$row->nama.'"><i class="fas fa-eye"></i></a>
      <a href="javascript:void(0)" style="color: #fff" onclick="hapus_data(\'Data Aliansi\',\'affiliate\',\'hapus_data\','."'".$row->id."'".')" data-toggle="modal" title="Berhenti bermitra dengan '.$row->nama.'?" class="btn btn-sm m-r-5 btn-danger pull-right m-r-5"><i class="fas fa-trash"></i></a>
      </ul>
      </div>
      </li>
      </blockquote>
      </div>
      ';
    }
    $output .= '</div>
    </ul>';
  }
  echo $output;
}else{
  redirect("_404","refresh");
}
}
public function getDataTunggu(){
  if($this->input->is_ajax_request()){
   $output = '';
   $data = $this->affiliate_a_model->fetch_data($this->input->post('limit'), $this->input->post('start'));
   $output .='
   <ul class="media-list media-list-with-divider">
   <div class="row">';
   if($data->num_rows() > 0)
   {
     foreach($data->result() as $row)
     {
      $output .= '
      <div class="col-md-6">
      <blockquote>
      <li class="media media-sm">
      <a class="media-left" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" href="#detil_mitra">
      <img src="../assets/foto/bkk/'.$row->logo.'" alt="" class="media-object">
      </a>
      <div class="media-body">
      <h5 class="media-heading">'.$row->nama.'</h5>
      <ul class="list-unstyled">
      <li><small class="semi-bold">'.$row->npsn.'</small></li>
      <li><small><a style="color:inherit;text-decoration:none;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></small></li>
      <a href="#detil_mitra" style="color: #fff" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" class="btn btn-sm btn-warning pull-right" title="Lihat Mitra Industri '.$row->nama.'"><i class="fas fa-eye"></i></a>
      <a href="javascript:void(0)" style="color: #fff" onclick="hapus_data(\'Data Aliansi\',\'affiliate\',\'hapus_data\','."'".$row->id."'".')" data-toggle="modal" title="Berhenti bermitra dengan '.$row->nama.'?" class="btn btn-sm m-r-5 btn-danger pull-right m-r-5"><i class="fas fa-trash"></i></a>
      </ul>
      </div>
      </li>
      </blockquote>
      </div>
      ';
    }
    $output .= '</div>
    </ul>';
  }
  echo $output;
}else{
  redirect("_404","refresh");
}
}
public function getDataReq(){
  if($this->input->is_ajax_request()){
   $output = '';
   $data = $this->affiliate_b_model->fetch_data($this->input->post('limit'), $this->input->post('start'));
   $output .='
   <ul class="media-list media-list-with-divider">
   <div class="row">';
   if($data->num_rows() > 0)
   {
     foreach($data->result() as $row)
     {
      $output .= '
      <div class="col-md-6">
      <blockquote>
      <li class="media media-sm">
      <a class="media-left" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" href="#detil_mitra">
      <img src="../assets/foto/bkk/'.$row->logo.'" alt="" class="media-object">
      </a>
      <div class="media-body">
      <h5 class="media-heading">'.$row->nama.'</h5>
      <ul class="list-unstyled">
      <li><small class="semi-bold">'.$row->npsn.'</small></li>
      <li><small><a style="color:inherit;text-decoration:none;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></small></li>
      <a href="#detil_mitra" style="color: #fff" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" class="btn btn-sm btn-warning pull-right" title="Lihat Mitra Industri '.$row->nama.'"><i class="fas fa-eye"></i></a>
      <a href="javascript:void(0)" style="color: #fff" onclick="hapus_data(\'Data Aliansi\',\'affiliate\',\'hapus_data\','."'".$row->id."'".')" data-toggle="modal" title="Berhenti bermitra dengan '.$row->nama.'?" class="btn btn-sm m-r-5 btn-danger pull-right m-r-5"><i class="fas fa-trash"></i></a>
      </ul>
      </div>
      </li>
      </blockquote>
      </div>
      ';
    }
    $output .= '</div>
    </ul>';
  }
  echo $output;
}else{
  redirect("_404","refresh");
}
}
public function detil_sekolah($kode){
  if($this->input->is_ajax_request()){
   $this->session->set_userdata('sek',$kode);
   $isi['kelas'] = "affiliate";
   $isi['namamenu'] = "Data Affiliate";
   $isi['page'] = "mitra";
   $isi['link'] = 'affiliate';
   $isi['option_bidangx'][''] = "Pilih Bidang Usaha";
   $ckbidang = $this->db->get_where('ref.bidang_usaha')->result();
   if(count($ckbidang)>0){
    foreach ($ckbidang as $row) {
     $isi['option_bidangx'][trim($row->bidang_usaha_id)] = $row->nama_bidang_usaha;
   }
 }else{
  $isi['option_bidangx'][''] = "Bidang Usaha Tidak Tersedia";
}
$isi['link'] = 'mitra';
$isi['halaman'] = "Data Mitra Industri";
$isi['judul'] = "Halaman Data Mitra Industri";
$this->load->view('affiliate/detil_sekolah',$isi);
}else{
 redirect("_404","refresh");
}
}
public function add(){
  $isi['kelas'] = "affiliate";
  $isi['namamenu'] = "Data Affiliate";
  $isi['page'] = "mitra";
  $isi['link'] = 'affiliate';
  $isi['halaman'] = "Data Affiliate";
  $isi['judul'] = "Halaman Data Affiliate";
  $isi['content'] = "form_affiliate";
  $this->load->view("dashboard/dashboard_view",$isi);
}
public function req(){
  $isi['kelas'] = "affiliate";
  $isi['namamenu'] = "Data Affiliate";
  $isi['page'] = "mitra";
  $isi['link'] = 'affiliate';
  $isi['halaman'] = "Data Affiliate";
  $isi['judul'] = "Halaman Data Affiliate";
  $isi['content'] = "form_affiliate_req";
  $this->load->view("dashboard/dashboard_view",$isi);
}
public function getSekolah(){
  if($this->input->is_ajax_request()){
   $list = $this->list_affiliate->get_datatables();
   $data = array();
   $no = $_POST['start'];
   foreach ($list as $rowx) {
    $no++;
    $row = array();
    $row[] = '<center>' . $no . "." . '</center>';
    $row[] = $rowx->npsn;
    $row[] = $rowx->nama;
    $row[] = $rowx->alamat_jalan;
    $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Approve Aliansi" onclick="app_('."'".$rowx->sekolah_id."'".')"><i class="fa fa-reply"></i></a><a class="btn btn-xs m-r-5 btn-warning" href="#detil_mitra" class="btn btn-sm btn-primary" data-toggle="modal" title="View Mitra Industri" onclick="mitra_detil('."'".$rowx->sekolah_id."'".')"><i class="icon-search icon-white"></i></a></center>';
    $data[] = $row;
  }
  $output = array(
    "draw" => $_POST['draw'],
    "recordsTotal" => $this->list_affiliate->count_all(),
    "recordsFiltered" => $this->list_affiliate->count_filtered(),
    "data" => $data,
  );
  echo json_encode($output);
}else{
 redirect("_404","refresh");
}
}
public function getReq(){
  if($this->input->is_ajax_request()){
   $this->load->model('list_affiliate_req');
   $list = $this->list_affiliate_req->get_datatables();
   $data = array();
   $no = $_POST['start'];
   foreach ($list as $rowx) {
    $no++;
    $row = array();
    $row[] = '<center>' . $no . "." . '</center>';
    $row[] = $rowx->npsn;
    $row[] = $rowx->nama;
    $row[] = $rowx->alamat_jalan;
    $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Approve Aliansi" onclick="app_req('."'".$rowx->sekolah_id."'".')"><i class="fa fa-reply"></i></a><a class="btn btn-xs m-r-5 btn-warning" href="#detil_mitra" class="btn btn-sm btn-primary" data-toggle="modal" title="View Mitra Industri" onclick="mitra_detil('."'".$rowx->sekolah_id."'".')"><i class="icon-search icon-white"></i></a></center>';
    $data[] = $row;
  }
  $output = array(
    "draw" => $_POST['draw'],
    "recordsTotal" => $this->list_affiliate_req->count_all(),
    "recordsFiltered" => $this->list_affiliate_req->count_filtered(),
    "data" => $data,
  );
  echo json_encode($output);
}else{
 redirect("_404","refresh");
}
}
public function getIndustri(){
  if($this->input->is_ajax_request()){
   $list = $this->mitra_aff_model->get_datatables();
   $data = array();
   $no = $_POST['start'];
   foreach ($list as $rowx) {
    $no++;
    $row = array();
    $row[] = '<center>' . $no . "." . '</center>';
    $row[] = $rowx->nama;
    $row[] = $rowx->email;
    $row[] = $rowx->no_tlp;
    $row[] = $rowx->nama_bidang_usaha;
    $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'users_bkk\',\'edit_data\','."'".$rowx->id."'".')"><i class="icon-pencil"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data User\',\'users_bkk\',\'hapus_data\','."'".$rowx->id."'".')"><i class="icon-remove icon-white"></i></a></center>';
    $data[] = $row;
  }
  $output = array(
    "draw" => $_POST['draw'],
    "recordsTotal" => $this->mitra_aff_model->count_all(),
    "recordsFiltered" => $this->mitra_aff_model->count_filtered(),
    "data" => $data,
  );
  echo json_encode($output);
}else{
 redirect("_404","refresh");
}		
}
public function approve($id){
  if($this->input->is_ajax_request()){
   $simpan = array('bkk_induk'=>$this->session->userdata('role_'),
    'bkk_anak'=>$id,
    'status'=>'0');
   $this->db->insert('aliansi_bkk',$simpan);
   echo json_encode(array("status" => TRUE));
 }else{
   redirect("_404","refresh");
 }
}
public function approve_req($id){
  if($this->input->is_ajax_request()){
   $simpan = array('status'=>'1');
   $this->db->where('id',$id);
   $this->db->update('aliansi_bkk',$simpan);
   echo json_encode(array("status" => TRUE));
 }else{
   redirect("_404","refresh");
 }
}
public function app_req($id){
  if($this->input->is_ajax_request()){
   $simpan = array('bkk_induk'=>$id,
    'bkk_anak'=>$this->session->userdata('role_'),
    'status'=>'0');
   $this->db->insert('aliansi_bkk',$simpan);
   echo json_encode(array("status" => TRUE));
 }else{
   redirect("_404","refresh");
 }
}
public function hapus_data($id){
  if($this->input->is_ajax_request()){
   $this->db->delete('aliansi_bkk',array('id'=>$id));
   echo json_encode(array("status" => TRUE));
 }else{
   redirect("_404","refresh");
 }
}
public function getAff(){
  if($this->input->is_ajax_request()){
   $sess = $this->session->userdata('role_');
   $page =  $_GET['page'];
   $offset = 10*$page;
   $limit = 10;
   $ckdata = $this->db->query("SELECT * FROM view_aliansi WHERE (bkk_induk != '$sess') AND status = '0' LIMIT  $limit OFFSET $offset")->result();
   foreach ($ckdata as $row) {
    echo '<ul class="list-group list-group-lg no-radius list-email">
    <li class="list-group-item primary">
    <div class="email-info">
    <span class="email-time" title="NSS">
    '.$row->nss.'
    </span>
    <h5 class="email-title" title="Nama Sekolah">
    '.$row->nama.'</br>
    <span class="label label-primary f-s-10" title="Website Sekolah"><a style="list-style: none; color: white;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></span>
    </h5>
    <p class="email-desc">'.$row->email.'</p>
    <p class="email-desc"><small><b>NPSN : '.$row->npsn.'</b></small></p>
    </div>
    <div style="text-align:right">
    <a href="javascript:void(0)" onclick="app_('."'".$row->bkk_anak."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-success" title="Tambahkan"><i class="fa fa-plus icon-white"></i></a>
    <a class="btn btn-xs m-r-5 btn-warning" href="#detil_mitra" class="btn btn-sm btn-primary" data-toggle="modal" title="View Mitra Industri" onclick="mitra_detil('."'".$row->bkk_anak."'".')"><i class="icon-search icon-white"></i></a>
    </div>
    </li>
    </ul>';
  }
  exit;
}else{
 redirect("_404","refresh");
}	
}

public function recommendAff(){
  if($this->input->is_ajax_request()){
   $sess = $this->session->userdata('role_');
   $npsn = $this->session->userdata('username');
   $page =  $_GET['page'];
   $offset = 10*$page;
   $limit = 10;
   $ckdata = $this->db->query("SELECT DISTINCT * FROM view_aliansi_anak WHERE npsn!='$npsn' AND status='0' LIMIT $limit OFFSET $offset")->result();
   echo '
   <ul class="media-list media-list-with-divider">
   <div class="row">';
   if (count($ckdata)>0) {

     foreach ($ckdata as $row) {
      echo '
      <div class="col-md-6">
      <blockquote>
      <li class="media media-sm">
      <a class="media-left" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" href="#detil_mitra">
      <img src="assets/foto/bkk/'.$row->logo.'" alt="" class="media-object">
      </a>
      <div class="media-body">
      <h5 class="media-heading">'.$row->nama.'</h5>
      <ul class="list-unstyled">
      <li><small class="semi-bold">'.$row->npsn.'</small></li>
      <li><small><a style="color:inherit;text-decoration:none;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></small></li>
      <a href="#detil_mitra" style="color: #d9e0e7" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" title="Lihat Mitra Industri '.$row->nama.'" class="btn btn-sm btn-success pull-right"><i class="fas fa-eye"></i></a>
      <a "javascript:void(0)" style="color: #d9e0e7" onclick="app_('."'".$row->bkk_anak."'".')" data-toggle="tooltip" class="btn btn-sm btn-primary pull-right m-r-5" title="Ajukan Affiliasi Ke '.$row->nama.'"><i class="fas fa-plus"></i></a>
      </ul>
      </div>
      </li>
      </blockquote>
      </div>
      ';
    }
  }else{
    echo '<blockquote>
    <strong>Tidak ada rekomendasi affiliasi</strong>
    </blockquote>';
  }
  echo '</div>
  </ul>';
}else{
 redirect("_404","refresh");
}
}
public function fetch(){
  $output = '';
  $query = '';
  if($this->input->post('query')!='')
  {
   $query = $this->input->post('query');
 }
 $data = $this->affiliate_model->fetch_qry($query);
 $output .= '<div class="panel-body">
 <ul class="media-list media-list-with-divider">
 <div class="row">';
 if($data->num_rows() > 0)
 {
   foreach($data->result() as $row)
   {
    $output .= '
    <div class="col-md-6">
    <blockquote>
    <li class="media media-sm">
    <a class="media-left" href="javascript:;">
    <img src="assets/foto/bkk/'.$row->logo.'" alt="" class="media-object" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" href="#detil_mitra">
    </a>
    <div class="media-body">
    <h5 class="media-heading">'.$row->nama.'</h5>
    <ul class="list-unstyled">
    <li><small class="semi-bold">'.$row->npsn.'</small>
    <a style="color: #d9e0e7" href="#detil_mitra" onclick="mitra_detil('."'".$row->bkk_anak."'".')" data-toggle="modal" title="Lihat Mitra Industri '.$row->nama.'" class="btn btn-sm btn-success pull-right"><i class="fas fa-eye"></i></a>

    <a "javascript:void(0)" style="color: #d9e0e7" onclick="app_('."'".$row->bkk_anak."'".')" data-toggle="tooltip" class="btn btn-sm btn-primary pull-right m-r-5" title="Ajukan Affiliasi Ke '.$row->nama.'"><i class="fas fa-plus"></i></a>
    </li>
    <li><small><a style="color:inherit;text-decoration:none;" href="'.$row->website.'" target="_blank">'.$row->website.'</a></small></li>
    </ul>
    </div>
    </li>
    </blockquote>
    </div>';
  }
}
else
{
 $output .= '<span class="label label-primary f-s-10"><strong> Data tidak ditemukan</strong></span>';
}
$output .= '</div>
</ul>
</div>';
echo $output;
}
}

