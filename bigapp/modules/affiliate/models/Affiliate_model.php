<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Affiliate_model extends CI_Model {

    function fetch_data($limit, $start){
    $sess = $this->session->userdata('role_');
      $this->db->select("*");
      $this->db->from("view_aliansi_anak");
      $this->db->where('bkk_induk',$sess);
      $this->db->where('status=1');
      $this->db->limit($limit, $start);
      $query = $this->db->get();
      return $query;
  }

  function fetch_qry($query)
    {
    $sess_sekolah  = $this->session->userdata('role_');
    $npsn = $this->session->userdata('username');
      $this->db->select("*");
      $this->db->distinct('nama');
      $this->db->from("view_aliansi_anak");
      $this->db->limit('2');
      $this->db->where('status=0');
      $this->db->where('bkk_induk!=',$sess_sekolah);
      // $this->db->where('npsn!=',$npsn);
      if($query != '')
      {
         $this->db->like('nama', $query);
         $this->db->or_like('npsn', $query);
         $this->db->or_like('email', $query);
         $this->db->or_like('website', $query);
         
     }
     $this->db->order_by('nama', 'DESC');
     return $this->db->get();
 }
}