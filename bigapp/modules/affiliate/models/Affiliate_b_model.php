<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Affiliate_b_model extends CI_Model {

    function fetch_data($limit, $start){
    $skl = $this->session->userdata('role_');
      $this->db->select("*");
      $this->db->from("view_aliansi");
      $this->db->where('bkk_anak',$skl);
      $this->db->where('status=0');
      $this->db->limit($limit, $start);
      $query = $this->db->get();
      return $query;
  }

}