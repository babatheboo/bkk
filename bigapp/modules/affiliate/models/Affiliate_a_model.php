<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Affiliate_a_model extends CI_Model {

    function fetch_data($limit, $start){
    $sess = $this->session->userdata('role_');
      $this->db->select("*");
      $this->db->from("view_aliansi_anak");
      $this->db->where('bkk_induk',$sess);
      $this->db->where('status=0');
      $this->db->limit($limit, $start);
      $query = $this->db->get();
      return $query;
  }

}