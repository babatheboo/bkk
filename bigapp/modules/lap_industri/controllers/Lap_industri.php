<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lap_industri extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('mitra_model');
	}
	public function index(){
		if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="4"){
			$isi['namamenu'] = "BKK";
			$isi['page']     = "lap";
			$isi['kelas']    = "registrasi";
			$isi['link']     = 'reg_bkk';
			$isi['halaman']  = "Laporan";
			$isi['judul']    = "Mitra Industri";
			$isi['content']  = "lap_view";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect('login','refresh');
		}
	}

	public function getMitra(){
		if($this->input->is_ajax_request()){
			$list = $this->mitra_model->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[]  = $rowx->nama;
				$row[]  = '<center>' . number_format($rowx->total) . '</center>';
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->mitra_model->count_all(),
				"recordsFiltered" => $this->mitra_model->count_filtered(),
				"data"            => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}

	function expMtr(){
		// $sid = $this->session->userdata('role_');
		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSettings = array( ' memoryCacheSize ' => '128MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator('NAA')
		->setLastModifiedBy('NAA')
		->setTitle("Data Mitra Industri")
		->setSubject("Mitra Industri")
		->setDescription("Laporan Mitra Industri")
		->setKeywords("Data Mitra Industri");
		$style_col = array('font' => array('bold' => true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$style_num = array('font' => array('bold' => false),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA MITRA");
		$excel->getActiveSheet()->mergeCells('A1:F1');
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NPSN");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "SMK");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Mitra");
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$bkk = $this->db->query("SELECT c.npsn, c.nama AS nama_sekolah, count(a.id) AS total_loker FROM lowongan a JOIN app.user_sekolah b ON a.dibuat_oleh::uuid = b.user_id JOIN ref.sekolah c ON b.sekolah_id = c.sekolah_id GROUP BY b.sekolah_id, c.nama, c.npsn ORDER BY (count(a.id)) DESC")->result();
		$no = 1;
		$numrow = 4;
		foreach($bkk as $data){
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->npsn);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama_sekolah);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->total_loker);
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_num);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$no++;
			$numrow++;
		}
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$excel->getActiveSheet(0)->setTitle("Laporan Mitra Industri");
		$excel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Data Mitra Industri"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$writer = PHPExcel_IOFactory::createWriter($excel, 'HTML');
		$writer->save('php://output');
		exit;
	}
}