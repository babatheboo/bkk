<!doctype html>
<html lang="en" dir="ltr">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta http-equiv="Content-Language" content="en" />
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="theme-color" content="#4188c9">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<link rel="icon" href="<?php echo base_url();?>assets/nebeng/images/favicon.ico" type="image/x-icon"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/nebeng/images/favicon.ico" />
	<title>Form Kerjasama Lasjurin</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/nebeng/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/nebeng/fonts.googleapis.com/css2608.css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
	<style type="text/css">
		table{
			margin: 0 auto;
			width: 100%;
			clear: both;
			border-collapse: collapse;
			table-layout: fixed;
			word-wrap:break-word;
		}
	</style>
	<!-- Dashboard Core -->
	<link href="<?php echo base_url();?>assets/nebeng/css/dashboard.css" rel="stylesheet" />
	<!-- <script src="<?php echo base_url();?>assets/nebeng/js/dashboard.js"></script> -->
	<!-- c3.js Charts Plugin -->
	<link href="<?php echo base_url();?>assets/nebeng/plugins/charts-c3/plugin.css" rel="stylesheet" />
	<!-- <script src="<?php echo base_url();?>assets/nebeng/plugins/charts-c3/plugin.js"></script> -->
	<!-- Google Maps Plugin -->
	<link href="<?php echo base_url();?>assets/nebeng/plugins/maps-google/plugin.css" rel="stylesheet" />
	<!-- <script src="assets/plugins/maps-google/plugin.js"></script> -->
	<!-- Input Mask Plugin -->
	<!-- <script src="assets/plugins/input-mask/plugin.js"></script> -->
	<!-- Datatables Plugin -->
	<link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css">
	<!-- <script src="<?php echo base_url();?>assets/nebeng/plugins/datatables/plugin.js"></script> -->
</head>
<body class="">
	<div class="page">
		<div class="flex-fill">
			<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
				<div class="container">
					<div class="row align-items-center">
						<!-- <div class="col-lg-3 ml-auto">
							<form class="input-icon my-3 my-lg-0">
								<input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
								<div class="input-icon-addon">
									<i class="fe fe-search"></i>
								</div>
							</form>
						</div> -->
						<div class="col-lg order-lg-first">
							<ul class="nav nav-tabs border-0 flex-column flex-lg-row">
								<li id="1st" class="nav-item">
									<a href="javascript:void(0)" class="nav-link"><i class="fe fe-list"></i> Data Kerjasama</a>
								</li>
								<li id="2nd" class="nav-item">
									<a href="javascript:void(0)" class="nav-link"><i class="fe fe-plus-square"></i> Tambah Data Kerjasama</a>
								</li>
								<li id="3rd" class="nav-item" style="display: none;">
									<a href="javascript:void(0)" class="nav-link active"><i class="fe fe-edit"></i> Edit Data Kerjasama</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="content1st" class="my-3 my-md-5">
				<div class="container">
					<div class="page-header">
						<h1 class="page-title">
							Data Kerjasama
						</h1>
					</div>
					<div class="row row-cards row-deck">
						<div class="col-12">
							<div class="card">
								<div class="table-responsive">
									<table id="data-kerjasama" class="table table-hover table-outline table-vcenter card-table">
										<thead>
											<tr>
												<th style="width: 65px;">No</th>
												<th style="width: 150px;">Kelompok</th>
												<th style="width: 200px;">Lead Kerja Sama</th>
												<th style="width: 75px;">Jenis</th>
												<th style="width: 100px;">Tahun</th>
												<th style="width: 200px;">Pihak I</th>
												<th style="width: 200px;">Pihak II</th>
												<th style="width: 200px;">Pihak III</th>
												<th style="width: 200px;">Pihak IV</th>
												<th style="width: 200px;">Pihak V</th>
												<th style="width: 300px;">Tentang</th>
												<th style="width: 200px;">Ruang Lingkup</th>
												<th style="width: 200px;">Bidang Keahlian</th>
												<th style="width: 200px;">SMK Pelaksana</th>
												<th style="width: 200px;">Keterangan</th>
												<th style="width: 150px;">Opsi</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="content2nd" style="display: none;">
				<div class="container">
					<div class="page-header">
						<h1 class="page-title">
							Form Input Kerjasama
						</h1>
					</div>
					<div class="row row-cards row-deck">
						<div class="col-12">
							<div class="card">
								<fieldset class="form-fieldset">
									<form action="javascript:void(0)" method="post" id="frmAdd">
										<div class="form-group">
											<label class="form-label">Kelompok<span class="form-required">*</span></label>
											<input name="kelompok" id="kelompok" type="text" class="form-control">
										</div>
										<div class="row">
											<div class="form-group col-md-9">
												<label class="form-label">Lead Kerja Sama<span class="form-required">*</span></label>
												<input id="lead" name="lead" type="text" class="form-control">
											</div>
											<div class="form-group col-md-1">
												<label class="form-label">Jenis<span class="form-required">*</span></label>
												<select class="form-control" id="jenis" name="jenis">
													<option value="" selected="">Pilih Jenis Kerjasama</option>
													<option value="nk">NK</option>
													<option value="pks">PKS</option>
												</select>
											</div>
											<div class="form-group col-md-2">
												<label class="form-label">Tahun<span class="form-required">*</span></label>
												<input id="tahun" name="tahun" type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">Pihak I<span class="form-required">*</span></label>
											<input name="pihak1" id="pihak1" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak II<span class="form-required">*</span></label>
											<input id="pihak2" name="pihak2" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak III<span class="form-required">*</span></label>
											<input id="pihak3" name="pihak3" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak IV<span class="form-required">*</span></label>
											<input name="pihak4" id="pihak4" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak V<span class="form-required">*</span></label>
											<input id="pihak5" name="pihak5" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Tentang<span class="form-required">*</span></label>
											<textarea name="tentang" id="tentang" style="height: 135px;" class="form-control"></textarea>
										</div>
										<div class="form-group">
											<label class="form-label">Ruang Lingkup<span class="form-required">*</span></label>
											<div class="field_lingkup">
												<input class="form-control" type="text" name="lingkup[]" value=""/>
												<a href="javascript:void(0);" class="add_lingkup" title="Tambah"><i class="fa fa-plus"></i></a>
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">Bidang Keahlian<span class="form-required">*</span></label>
											<div class="field_bidang">
												<input class="form-control" type="text" name="bidang[]" value=""/>
												<a href="javascript:void(0);" class="add_bidang" title="Tambah"><i class="fa fa-plus"></i></a>
											</div>
										</div>
										<!-- <div class="row"> -->
											<div class="form-group">
											<label class="form-label">SMK Pelaksana<span class="form-required">*</span></label>
											<div class="field_smk">
												<input class="form-control" type="text" name="pelaksana[]" id="pelaksana" value=""/>
												<a href="javascript:void(0);" class="add_smk" title="Tambah"><i class="fa fa-plus"></i></a>
											</div>
										</div>
										<!-- </div> -->
										<div class="form-group">
											<label class="form-label">Keterangan<span class="form-required">*</span></label>
											<input id="ket" name="ket" type="text" class="form-control">
										</div>
										<button type="submit" onclick="add()" class="btn btn-primary">Simpan</button>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="editContent" class="my-3 my-md-5" style="display: none;">
				<div class="container">
					<div class="page-header">
						<h1 class="page-title">
							Edit Data Kerjasama
						</h1>
					</div>
					<div class="row row-cards row-deck">
						<div class="col-12">
							<div class="card">
								<fieldset class="form-fieldset">
									<form action="javascript:void(0)" method="post" id="frmEdit">
										<div class="form-group">
											<label class="form-label">Kelompok<span class="form-required">*</span></label>
											<input name="kelompok-edit" id="kelompok-edit" type="text" class="form-control">
											<input type="hidden" name="idna" id="idna">
										</div>
										<div class="row">
											<div class="form-group col-md-9">
												<label class="form-label">Lead Kerja Sama<span class="form-required">*</span></label>
												<input id="lead-edit" name="lead-edit" type="text" class="form-control">
											</div>
											<div class="form-group col-md-1">
												<label class="form-label">Jenis<span class="form-required">*</span></label>
												<select class="form-control" id="jenis-edit" name="jenis-edit">
													<option value="" selected="">Pilih Jenis Kerjasama</option>
													<option value="nk">NK</option>
													<option value="pks">PKS</option>
												</select>
											</div>
											<div class="form-group col-md-2">
												<label class="form-label">Tahun<span class="form-required">*</span></label>
												<input id="tahun-edit" name="tahun-edit" type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="form-label">Pihak I<span class="form-required">*</span></label>
											<input name="pihak1-edit" id="pihak1-edit" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak II<span class="form-required">*</span></label>
											<input id="pihak2-edit" name="pihak2-edit" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak III<span class="form-required">*</span></label>
											<input id="pihak3-edit" name="pihak3-edit" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak IV<span class="form-required">*</span></label>
											<input name="pihak4-edit" id="pihak4-edit" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Pihak V<span class="form-required">*</span></label>
											<input id="pihak5-edit" name="pihak5-edit" type="text" class="form-control">
										</div>
										<div class="form-group">
											<label class="form-label">Tentang<span class="form-required">*</span></label>
											<textarea name="tentang-edit" id="tentang-edit" style="height: 135px;" class="form-control"></textarea>
										</div>
										<div class="form-group">
											<label class="form-label">Ruang Lingkup<span class="form-required">*</span></label>
											<a class="btn btn-primary" href='#mRuang' rel='modal:open'>Daftar Ruang Lingkup</a>
										</div>
										<div class="form-group">
											<label class="form-label">Bidang Keahlian<span class="form-required">*</span></label>
											<a class="btn btn-primary" href='#mBidang' rel='modal:open'>Daftar Bidang Keahlian</a>
										</div>
										<div class="form-group">
											<label class="form-label">SMK Pelaksana<span class="form-required">*</span></label>
											<a class="btn btn-primary" href='#mSmk' rel='modal:open'>Daftar SMK Pelaksana</a>
										</div>
										<div class="form-group">
											<label class="form-label">Keterangan<span class="form-required">*</span></label>
											<input id="ket-edit" name="ket-edit" type="text" class="form-control">
										</div>
										<button type="submit" onclick="edit()" class="btn btn-primary">Simpan</button>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="mRuang" class="modal">
				<strong>Data Ruang Lingkup</strong>
				<ul id="dataRuang"></ul>
			</div>
			<div id="mBidang" class="modal">
				<strong>Data Bidang Keahlian</strong>
				<ul id="dataBidang"></ul>
			</div>
			<div id="mSmk" class="modal">
				<strong>Data SMK Pelaksana</strong>
				<ul id="dataSmk"></ul>
			</div>
			<div id="meRuang" class="modal">
				<input class="form-control" type="text" name="lingkup-edit" id="lingkup-edit">
				<input type="hidden" name="idno" id="idno">
				<a href="#mRuang" type="button" class="btn btn-success" onclick="eRuang()" style="margin-top: 5px;" rel="modal:open">Simpan</a>
			</div>
			<div id="meBidang" class="modal">
				<input class="form-control" type="text" name="bidang-edit" id="bidang-edit">
				<input type="hidden" name="idnp" id="idnp">
				<a href="#mBidang" type="button" class="btn btn-success" onclick="eBidang()" style="margin-top: 5px;" rel="modal:open">Simpan</a>
			</div>
			<div id="meSmk" class="modal">
				<input class="form-control" type="text" name="smk-edit" id="smk-edit">
				<input type="hidden" name="idnq" id="idnq">
				<a href="#mSmk" type="button" class="btn btn-success" onclick="eSmk()" style="margin-top: 5px;" rel="modal:open">Simpan</a>
			</div>
			<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
			<script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
			<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
			<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
			<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					var maxField = 10;
					var addSMK = $('.add_smk');
					var addBidang = $('.add_bidang');
					var addLingkup = $('.add_lingkup');
					var wrapsmk = $('.field_smk');
					var wraplingkup = $('.field_lingkup');
					var wrapbdg = $('.field_bidang');
					var fieldSMK = '<div><input class="form-control" type="text" name="pelaksana[]" id="pelaksana" value=""/><a href="javascript:void(0);" class="remove_smk"><i class="fa fa-minus"></i></a></div>';
					var fieldBidang = '<div><input class="form-control" type="text" name="bidang[]" value=""/><a href="javascript:void(0);" class="remove_bidang"><i class="fa fa-minus"></i></a></div>';
					var fieldLingkup = '<div><input class="form-control" type="text" name="lingkup[]" value=""/><a href="javascript:void(0);" class="remove_lingkup"><i class="fa fa-minus"></i></a></div>';
					var x = 1;
					$(addSMK).click(function(){$(wrapsmk).append(fieldSMK);});
					$(addBidang).click(function(){$(wrapbdg).append(fieldBidang);});
					$(addLingkup).click(function(){$(wraplingkup).append(fieldLingkup);});
					$(wrapsmk).on('click', '.remove_smk', function(e){
						e.preventDefault();
						$(this).parent('div').remove();
						x--;
					});
					$(wrapbdg).on('click', '.remove_bidang', function(e){
						e.preventDefault();
						$(this).parent('div').remove();
						x--;
					});
					$(wraplingkup).on('click', '.remove_lingkup', function(e){
						e.preventDefault();
						$(this).parent('div').remove();
						x--;
					});
					$("#2nd").on('click', function(){
						$("#editContent").hide();
						$("#content1st").hide();
						$("#content2nd").show();
						$("#3rd").hide();
					});
					$("#1st").on('click', function(){
						$("#editContent").hide();
						$("#content1st").show();
						$("#content2nd").hide();
						$("#3rd").hide();
					});
					$(function(){
						table = $("#data-kerjasama").DataTable({
							autoWidth: false,
							processing: true,
							serverSide: true,
							pageLength: 10,
							ajax: {
								url: "<?php echo base_url();?>form_kerjasama/getData",
								type: "POST"
							},
						});
						$(".dataTables_filter input[type=search]").attr(
							"placeholder",
							"Filter Pencarian"
							);
						$("input").change(function() {
							$(this)
							.parent()
							.parent()
							.removeClass("has-error");
							$(this)
							.next()
							.empty();
						});
					});
				});
				function add(){
					var kel = $("#kelompok").val();
					var lead = $("#lead").val();
					var jns = $("#jenis").val();
					var thn = $("#tahun").val();
					var p1 = $("#pihak1").val();
					var p2 = $("#pihak2").val();
					var p3 = $("#pihak3").val();
					var p4 = $("#pihak4").val();
					var p5 = $("#pihak5").val();
					var ttg = $("#tentang").val();
					var lkp = $("#lingkup").val();
					var bdg = $("#bidang").val();
					var smk = $("#pelaksana").val();
					var ket = $("#keterangan").val();
					if (kel == '') {
						var scrl = document.getElementById("kelompok");scrl.scrollIntoView();
						jQuery("#kelompok").effect('shake', '1500').attr('placeholder', 'Masukan kelompok');
					}else if (lead == '') {
						var scrl = document.getElementById("lead");scrl.scrollIntoView();
						jQuery("#lead").effect('shake', '1500').attr('placeholder', 'Masukan lead kerja sama');
					}else if (jns == '') {
						var scrl = document.getElementById("jenis");scrl.scrollIntoView();
						jQuery("#jenis").effect('shake', '1500').attr('placeholder', 'Masukan jenis kerjasama');
					}else if (thn == '') {
						var scrl = document.getElementById("tahun");scrl.scrollIntoView();
						jQuery("#tahun").effect('shake', '1500').attr('placeholder', 'Masukan tahun kerja sama');
					}else if (p1 == '') {
						var scrl = document.getElementById("pihak1");scrl.scrollIntoView();
						jQuery("#pihak1").effect('shake', '1500').attr('placeholder', 'Masukan pihak kesatu');
					}else if (p2 == '') {
						var scrl = document.getElementById("pihak2");scrl.scrollIntoView();
						jQuery("#pihak2").effect('shake', '1500').attr('placeholder', 'Masukan pihak kedua');
					}else{
						$.ajax({
							url : '<?php echo base_url();?>form_kerjasama/addData',
							type: "POST",
							data : $("#frmAdd").serialize(),
							dataType: 'json',
							success: function(data) {
								var data = $.parseJSON(JSON.stringify(data));
								if(data.response == 'true'){
									reload_table();
									$.notify("Data berhasil ditambahkan", "success");
									window.location.reload();
								}else if (data.response == 'false') {
									$.notify("Gagal menyimpan data", "error");
								}else {
									$.notify("Terjadi kesalahan. Ulangi proses", "error");
								}
							}
						});
					}
				}

				function cRuang(id){
					$.ajax({
						url: '<?php echo base_url();?>form_kerjasama/cekRuang/'+id,
						type: 'POST',
						dataType: 'json',
						success: function(data){
							var html = '';
							for(i=0; i<data.length; i++){
								html +='<li style="margin-left: -25px;">'+data[i].lingkup+'</li>';
							}
							$("#dataRuang").html(html);
						},
						error: function(){
							alert('Could not load the data');
						}
					});
				}
				function cBidang(id){
					$.ajax({
						url: '<?php echo base_url();?>form_kerjasama/cekBidang/'+id,
						type: 'POST',
						dataType: 'json',
						success: function(data){
							var html = '';
							for(i=0; i<data.length; i++){
								html +='<li style="margin-left: -25px;">'+data[i].bidang+'</li>';
							}
							$("#dataBidang").html(html);
						},
						error: function(){
							alert('Could not load the data');
						}
					});
				}
				function cSmk(id){
					$.ajax({
						url: '<?php echo base_url();?>form_kerjasama/cekSmk/'+id,
						type: 'POST',
						dataType: 'json',
						success: function(data){
							var html = '';
							for(i=0; i<data.length; i++){
								html +='<li style="margin-left: -25px;">'+data[i].smk+'</li>';
							}
							$("#dataSmk").html(html);
						},
						error: function(){
							alert('Could not load the data');
						}
					});
				}

				function cek(id){
					$("#3rd").show();
					$("#editContent").show();
					$("#content2nd").hide();
					$("#content1st").hide();
					$.ajax({
						url : '<?php echo base_url();?>'+'form_kerjasama/cekData/'+id,
						'type':'post',
						beforeSend:function(data){
							$.ajax({
								url: '<?php echo base_url();?>form_kerjasama/cekRuang/'+id,
								type: 'POST',
								dataType: 'json',
								success: function(data){
									var html = '';
									for(i=0; i<data.length; i++){
										html +='<li style="margin-left: -25px;" id="lkp" name="lkp">'+data[i].lingkup+'<a href="#meRuang" rel="modal:open" onclick=cekRe('+data[i].id+')> Ubah</a> | <a href="#" onclick=del('+data[i].id+',"rl")>Hapus</a></li>';
									}
									$("#dataRuang").html(html);
								},
								error: function(){
									alert('Could not load the data');
								}
							});
							$.ajax({
								url: '<?php echo base_url();?>form_kerjasama/cekBidang/'+id,
								type: 'POST',
								dataType: 'json',
								success: function(data){
									var html = '';
									for(i=0; i<data.length; i++){
										html +='<li style="margin-left: -25px;" id="bd" name="bd">'+data[i].bidang+'<a href="#meBidang" rel="modal:open" onclick=cekBe('+data[i].id+')> Ubah</a> | <a href="#" onclick=del('+data[i].id+',"bk")>Hapus</a></li>';
									}
									$("#dataBidang").html(html);
								},
								error: function(){
									alert('Could not load the data');
								}
							});
							$.ajax({
								url: '<?php echo base_url();?>form_kerjasama/cekSmk/'+id,
								type: 'POST',
								dataType: 'json',
								success: function(data){
									var html = '';
									for(i=0; i<data.length; i++){
										html +='<li style="margin-left: -25px;" id="smk" name="smk">'+data[i].smk+'<a href="#meSmk" rel="modal:open" onclick=cekSe('+data[i].id+')> Ubah</a> | <a href="#" onclick=del('+data[i].id+',"sp")>Hapus</a></li>';
									}
									$("#dataSmk").html(html);
								},
								error: function(){
									alert('Could not load the data');
								}
							});
						},
						success:function(data){
							var data = $.parseJSON(data);
							$("#idna").val(data.idna);
							$('#kelompok-edit').val(data.kelompok);
							$('#lead-edit').val(data.lead);
							$('#jenis-edit').val(data.jenis);
							$('#tahun-edit').val(data.tahun);
							$('#pihak1-edit').val(data.pihak1);
							$('#pihak2-edit').val(data.pihak2);
							$('#pihak3-edit').val(data.pihak3);
							$('#pihak4-edit').val(data.pihak4);
							$('#pihak5-edit').val(data.pihak5);
							$('#tentang-edit').val(data.tentang);
							$('#ket-edit').val(data.keterangan);
						},
						error: function() {
							alert("TERJADI KESALAHAN");
						}
					});
				}

				function edit(){
					var idna = $("#idna").val();
					var kel = $("#kelompok-edit").val();
					var lead = $("#lead-edit").val();
					var jns = $("#jenis-edit").val();
					var thn = $("#tahun-edit").val();
					var p1 = $("#pihak1-edit").val();
					var p2 = $("#pihak2-edit").val();
					var p3 = $("#pihak3-edit").val();
					var p4 = $("#pihak4-edit").val();
					var p5 = $("#pihak5-edit").val();
					var ttg = $("#tentang-edit").val();
					var ket = $("#ket-edit").val();
					if (kel == '') {
						var scrl = document.getElementById("kelompok-edit");scrl.scrollIntoView();
						jQuery("#kelompok-edit").effect('shake', '1500').attr('placeholder', 'Masukan kelompok');
					}else if (lead == '') {
						var scrl = document.getElementById("lead-edit");scrl.scrollIntoView();
						jQuery("#lead-edit").effect('shake', '1500').attr('placeholder', 'Masukan lead kerjasama');
					}else if (jns == '') {
						var scrl = document.getElementById("jenis-edit");scrl.scrollIntoView();
						jQuery("#jenis-edit").effect('shake', '1500').attr('placeholder', 'Masukan jenis kerjasama');
					}else if (thn == '') {
						var scrl = document.getElementById("tahun-edit");scrl.scrollIntoView();
						jQuery("#tahun-edit").effect('shake', '1500').attr('placeholder', 'Masukan tahun kerjasama');
					}else if (p1 == '') {
						var scrl = document.getElementById("pihak1-edit");scrl.scrollIntoView();
						jQuery("#pihak1-edit").effect('shake', '1500').attr('placeholder', 'Masukan pihak pertama');
					}else if (p2 == '') {
						var scrl = document.getElementById("pihak2-edit");scrl.scrollIntoView();
						jQuery("#pihak2-edit").effect('shake', '1500').attr('placeholder', 'Masukan pihak kedua');
					}else if (p3 == '') {
						var scrl = document.getElementById("pihak3-edit");scrl.scrollIntoView();
						jQuery("#pihak3-edit").effect('shake', '1500').attr('placeholder', 'Masukan pihak ketiga');
					}else if (p4 == '') {
						var scrl = document.getElementById("pihak4-edit");scrl.scrollIntoView();
						jQuery("#pihak4-edit").effect('shake', '1500').attr('placeholder', 'Masukan pihak keempat');
					}else if (p5 == '') {
						var scrl = document.getElementById("pihak5-edit");scrl.scrollIntoView();
						jQuery("#pihak5-edit").effect('shake', '1500').attr('placeholder', 'Masukan pihak kelima');
					}else if (ttg == '') {
						var scrl = document.getElementById("tentang-edit");scrl.scrollIntoView();
						jQuery("#tentang-edit").effect('shake', '1500').attr('placeholder', 'Masukan tentang kerjasama');
					}else{
						$.ajax({
							url : '<?php echo base_url();?>form_kerjasama/edit',
							type: "POST",
							data : $("#frmEdit").serialize(),
							dataType: 'json',
							success: function(data) {
								var data = $.parseJSON(JSON.stringify(data));
								if(data.response == 'true'){
									reload_table();
									$.notify("Data berhasil diubah", "success");
									window.location.reload();
								}else if (data.response == 'false') {
									$.notify("Gagal menyimpan data", "error");
								}else {
									$.notify("Terjadi kesalahan. Ulangi proses", "error");
								}
							},
							error:function(){
								$.notify("Terjadi kesalahan. Ulangi proses", "error");
							}
						});
					}
				}

				function cekRe(id){
					$.ajax({
						url : '<?php echo base_url();?>'+'form_kerjasama/cekRe/'+id,
						'type':'post',
						success:function(data){
							var data = $.parseJSON(data);
							$("#idno").val(data.idno);
							$('#lingkup-edit').val(data.lingkup);
						},
						error: function() {
							alert("TERJADI KESALAHAN");
						}
					});
				}
				function cekBe(id){
					$.ajax({
						url : '<?php echo base_url();?>'+'form_kerjasama/cekBe/'+id,
						'type':'post',
						success:function(data){
							var data = $.parseJSON(data);
							$("#idnp").val(data.idno);
							$('#bidang-edit').val(data.bidang);
						},
						error: function() {
							alert("TERJADI KESALAHAN");
						}
					});
				}
				function cekSe(id){
					$.ajax({
						url : '<?php echo base_url();?>'+'form_kerjasama/cekSe/'+id,
						'type':'post',
						success:function(data){
							var data = $.parseJSON(data);
							$("#idnq").val(data.idno);
							$('#smk-edit').val(data.smk);
						},
						error: function() {
							alert("TERJADI KESALAHAN");
						}
					});
				}
				function eRuang(){
					var id = $("#idno").val();
					var er = $("#lingkup-edit").val();
					$.ajax({
						url : '<?php echo base_url();?>form_kerjasama/eRuang',
						type: "POST",
						data : {er:er, id:id},
						dataType: 'json',
						success: function(data) {
							var data = $.parseJSON(JSON.stringify(data));
							if(data.response == 'true'){
								reload_table();
								$.notify("Data berhasil diubah", "success");
							}else if (data.response == 'false') {
								$.notify("Gagal menyimpan data", "error");
							}else {
								$.notify("Terjadi kesalahan. Ulangi proses", "error");
							}
						},
						error:function(){
							$.notify("Terjadi kesalahan. Ulangi proses", "error");
						}
					});
				}

				function eBidang(){
					var id = $("#idnp").val();
					var eb = $("#bidang-edit").val();
					$.ajax({
						url : '<?php echo base_url();?>form_kerjasama/eBidang',
						type: "POST",
						data : {eb:eb, id:id},
						dataType: 'json',
						success: function(data) {
							var data = $.parseJSON(JSON.stringify(data));
							if(data.response == 'true'){
								reload_table();
								$.notify("Data berhasil diubah", "success");
							}else if (data.response == 'false') {
								$.notify("Gagal menyimpan data", "error");
							}else {
								$.notify("Terjadi kesalahan. Ulangi proses", "error");
							}
						},
						error:function(){
							$.notify("Terjadi kesalahan. Ulangi proses", "error");
						}
					});
				}
				function eSmk(){
					var id = $("#idnq").val();
					var es = $("#smk-edit").val();
					$.ajax({
						url : '<?php echo base_url();?>form_kerjasama/eSmk',
						type: "POST",
						data : {es:es, id:id},
						dataType: 'json',
						success: function(data) {
							var data = $.parseJSON(JSON.stringify(data));
							if(data.response == 'true'){
								reload_table();
								$.notify("Data berhasil diubah", "success");
							}else if (data.response == 'false') {
								$.notify("Gagal menyimpan data", "error");
							}else {
								$.notify("Terjadi kesalahan. Ulangi proses", "error");
							}
						},
						error:function(){
							$.notify("Terjadi kesalahan. Ulangi proses", "error");
						}
					});
				}

				function del(id, kod){
					if (confirm("Hapus data berikut?")) {
						$.ajax({
							method: "POST",
							url: '<?php echo base_url();?>' + "form_kerjasama/del/"+id+"/"+kod,
							success: function(data){
								var data = $.parseJSON(data);
								if(data.response=='true'){
									reload_table();
									$.notify("Data berhasil dihapus", "success");
								}else if (data.response=='false') {
									reload_table();
									$.notify("Gagal, coba lagi!", "error");
								}else{
									reload_table();
									$.notify("TERJADI KESALAHAN, Ulangi proses", "error");
								}
							},
							error: function(data){
								$.notify("TERJADI KESALAHAN");
							}
						});
					}
				}
				function reload_table(){
					table.ajax.reload(null,false);
				}
			</script>
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
							Copyright © <?php echo date('Y');?> <a href="http://psmk.kemdikbud.go.id/">Direktorat Pembinaan SMK </a>- Subdit Penyelarasan Kejuruan dan Kerjasama Industri.
						</div>
					</div>
				</div>
			</footer>
		</body>

		<!-- Created by @nauffalaazhar, hope helpful :) -->
		</html>