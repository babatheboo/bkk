<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Form_kerjasama extends CI_Controller {
	public function __construct(){		
		parent::__construct();
		$this->load->model('kerjasama_model');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->load->view('form');
	}

	public function getData(){
		$level = $this->session->userdata('level');
		// if($this->input->is_ajax_request() && $this->session->userdata('login')==TRUE && $level=='0'){
		$list = $this->kerjasama_model->get_datatables();
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $rowx) {
			$crl = $this->db->query("SELECT count(id) FROM ref.ruang_lingkup WHERE id_kerjasama='$rowx->id_kerjasama'")->row();
			$cbk = $this->db->query("SELECT count(id) FROM ref.bidang_keahlian WHERE id_kerjasama=$rowx->id_kerjasama")->row();
			$csp = $this->db->query("SELECT count(id) FROM ref.smk_pelaksana WHERE id_kerjasama='$rowx->id_kerjasama'")->row();
			$no++;
			$row    = array();
			$row[]  = $no . ".";
			$row[]  = $rowx->kelompok;
			$row[]  = $rowx->lead_kerja_sama;
			$row[]  = $rowx->jenis_berkas;
			$row[]  = $rowx->tahun;
			$row[]  = $rowx->pihak1;
			$row[]  = $rowx->pihak2;
			$row[]  = $rowx->pihak3;
			$row[]  = $rowx->pihak4;
			$row[]  = $rowx->pihak5;
			$row[]  = $rowx->tentang;
			$row[]  = "<a href='#mRuang' rel='modal:open' onclick='cRuang(".'"'.$rowx->id_kerjasama.'"'.")'>".$crl->count."</a>";
			$row[]  = "<a href='#mBidang' rel='modal:open' onclick='cBidang(".'"'.$rowx->id_kerjasama.'"'.")'>".$cbk->count."</a>";
			$row[]  = "<a href='#mSmk' rel='modal:open' onclick='cSmk(".'"'.$rowx->id_kerjasama.'"'.")'>".$csp->count."</a>";
			$row[]  = $rowx->keterangan;
			$row[]  = "<a href='#' onclick='cek(".'"'.$rowx->id_kerjasama.'"'.")'>Edit</a> | <a href='#' onclick='del(".'"'.$rowx->id_kerjasama.'"'.",".'"main"'.")'>Hapus</a>";
			$data[] = $row;
		}
		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->kerjasama_model->count_all(),
			"recordsFiltered" => $this->kerjasama_model->count_filtered(),
			"data"            => $data,
		);
		echo json_encode($output);
	}

	function addData(){
		$lingkup = $_POST['lingkup'];
		$bidang = $_POST['bidang'];
		$pelaksana = $_POST['pelaksana'];
		$ket = $this->input->post('ket');
		$kel = $this->input->post('kelompok');
		$lead = $this->input->post('lead');
		$jns = $this->input->post('jenis');
		$thn = $this->input->post('tahun');
		$p1 = $this->input->post('pihak1');
		$p2 = $this->input->post('pihak2');
		$p3 = $this->input->post('pihak3');
		$p4 = $this->input->post('pihak4');
		$p5 = $this->input->post('pihak5');
		$tentang = $this->input->post('tentang');
		$d2 = array();
		$d3 = array();
		$d4 = array();
		$d1 = array('kelompok'=>$kel, 'lead_kerja_sama'=>$lead, 'jenis_berkas'=>$jns, 'tahun'=>$thn, 'pihak1'=>$p1, 'pihak2'=>$p2, 'pihak3'=>$p3, 'pihak4'=>$p4, 'pihak5'=>$p5, 'tentang'=>$tentang, 'keterangan'=>$ket);
		if ($this->db->insert('ref.form_kerjasama',$d1)) {
			end($d1);
			$idk = $this->db->query("SELECT id_kerjasama FROM ref.form_kerjasama ORDER BY id_kerjasama DESC LIMIT 1")->row();
			foreach ($lingkup as $lingkups) {
				array_push($d2, array('id_kerjasama'=>$idk->id_kerjasama, 'lingkup'=>trim($lingkups)));
			}
			$this->db->insert_batch('ref.ruang_lingkup',$d2);
			foreach ($bidang as $bidangs) {
				array_push($d3, array('id_kerjasama'=>$idk->id_kerjasama, 'bidang'=>trim($bidangs)));
			}
			$this->db->insert_batch('ref.bidang_keahlian',$d3);
			foreach ($pelaksana as $pelaksanas) {
				array_push($d4, array('id_kerjasama'=>$idk->id_kerjasama, 'smk'=>trim($pelaksanas)));
			}
			$this->db->insert_batch('ref.smk_pelaksana',$d4);
			$data['response'] = 'true';
		}else{
			$data['response'] = 'false';
		}
		echo json_encode($data);
	}

	function cekRuang($id){
		$data = $this->db->query("SELECT * FROM ref.ruang_lingkup WHERE id_kerjasama='$id'")->result();
		echo trim(json_encode($data));
	}
	function cekBidang($id){
		$data = $this->db->query("SELECT * FROM ref.bidang_keahlian WHERE id_kerjasama='$id'")->result();
		echo json_encode($data);
	}
	function cekSmk($id){
		$data = $this->db->query("SELECT * FROM ref.smk_pelaksana WHERE id_kerjasama='$id'")->result();
		echo json_encode($data);
	}

	public function cekData($id){
		// if($this->input->is_ajax_request()){
		$ckdata = $this->db->get_where('ref.form_kerjasama',array('id_kerjasama'=>$id))->result();
		$data['idna']		='';
		$data['kelompok']	='';
		$data['lead']		='';
		$data['jenis'] 		='';
		$data['tahun']		='';
		$data['pihak1'] 	='';
		$data['pihak2'] 	='';
		$data['pihak3'] 	='';
		$data['pihak4'] 	='';
		$data['pihak5'] 	='';
		$data['tentang'] 	='';
		$data['keterangan'] ='';
		foreach ($ckdata as $key) {
			$data['idna']	  	= $key->id_kerjasama;
			$data['kelompok'] 	= trim($key->kelompok);
			$data['lead'] 	  	= trim($key->lead_kerja_sama);
			$data['jenis'] 	  	= trim($key->jenis_berkas);
			$data['tahun']	  	= $key->tahun;
			$data['pihak1']   	= trim($key->pihak1);
			$data['pihak2']   	= trim($key->pihak2);
			$data['pihak3']   	= trim($key->pihak3);
			$data['pihak4']   	= trim($key->pihak4);
			$data['pihak5']   	= trim($key->pihak5);
			$data['tentang'] 	= trim($key->tentang);
			$data['keterangan'] = trim($key->keterangan);
		}
		echo json_encode($data);
		// }else{
			// redirect("_404","refresh");
		// }
	}

	public function edit(){
		$id = $this->input->post('idna');
		$kel = $this->input->post('kelompok-edit');
		$lead = $this->input->post('lead-edit');
		$jns = $this->input->post('jenis-edit');
		$thn = $this->input->post('tahun-edit');
		$p1 = $this->input->post('pihak1-edit');
		$p2 = $this->input->post('pihak2-edit');
		$p3 = $this->input->post('pihak3-edit');
		$p4 = $this->input->post('pihak4-edit');
		$p5 = $this->input->post('pihak5-edit');
		$ttg = $this->input->post('tentang-edit');
		$ket = $this->input->post('ket-edit');
		if ($this->db->query("UPDATE ref.form_kerjasama SET kelompok='$kel', lead_kerja_sama='$lead', jenis_berkas='$jns', tahun='$thn', pihak1='$p1', pihak2='$p2', pihak3='$p3', pihak4='$p4', pihak5='$p5', tentang='$ttg', keterangan='$ket' WHERE id_kerjasama='$id'")) {
			$data['response'] = 'true';
		}else{
			$data['response'] = 'false';
		}
		echo json_encode($data);
	}

	function cekRe($id){
		if($this->input->is_ajax_request()){
			$ckdata = $this->db->get_where('ref.ruang_lingkup',array('id'=>$id))->result();
			$data['idno']		='';
			$data['lingkup']	='';
			foreach ($ckdata as $key) {
				$data['idno']	  = $key->id;
				$data['lingkup']	  = trim($key->lingkup);
			}
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}

	function cekBe($id){
		if($this->input->is_ajax_request()){
			$ckdata = $this->db->get_where('ref.bidang_keahlian',array('id'=>$id))->result();
			$data['idno']		='';
			$data['bidang']	='';
			foreach ($ckdata as $key) {
				$data['idno']	  = $key->id;
				$data['bidang']	  = trim($key->bidang);
			}
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}

	function cekSe($id){
		if($this->input->is_ajax_request()){
			$ckdata = $this->db->get_where('ref.smk_pelaksana',array('id'=>$id))->result();
			$data['idno']		='';
			$data['smk']	='';
			foreach ($ckdata as $key) {
				$data['idno']	  = $key->id;
				$data['smk']	  = trim($key->smk);
			}
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}

	function eRuang(){
		$id = $this->input->post('id');
		$er = $this->input->post('er');
		if ($this->db->query("UPDATE ref.ruang_lingkup SET lingkup='$er' WHERE id='$id'")) {
			$data['response'] = 'true';
		}else{
			$data['response'] = 'false';
		}
		echo json_encode($data);
	}

	function eBidang(){
		$id = $this->input->post('id');
		$eb = $this->input->post('eb');
		if ($this->db->query("UPDATE ref.bidang_keahlian SET bidang='$eb' WHERE id='$id'")) {
			$data['response'] = 'true';
		}else{
			$data['response'] = 'false';
		}
		echo json_encode($data);
	}

	function eSmk(){
		$id = $this->input->post('id');
		$es = $this->input->post('es');
		if ($this->db->query("UPDATE ref.smk_pelaksana SET smk='$es' WHERE id='$id'")) {
			$data['response'] = 'true';
		}else{
			$data['response'] = 'false';
		}
		echo json_encode($data);
	}

	function del($id, $kod){
		switch ($kod) {
			case 'rl':
			if ($this->db->query("DELETE FROM ref.ruang_lingkup WHERE id='$id'")) {
				$data['response'] = 'true';
			}else{
				$data['response'] = 'false';
			}
			break;
			case 'bk':
			if ($this->db->query("DELETE FROM ref.bidang_keahlian WHERE id='$id'")) {
				$data['response'] = 'true';
			}else{
				$data['response'] = 'false';
			}
			break;
			case 'sp':
			if ($this->db->query("DELETE FROM ref.smk_pelaksana WHERE id='$id'")) {
				$data['response'] = 'true';
			}else{
				$data['response'] = 'false';
			}
			break;
			case 'main':
			if ($this->db->query("DELETE FROM ref.form_kerjasama WHERE id_kerjasama='$id'")) {
				$data['response'] = 'true';
			}else{
				$data['response'] = 'false';
			}
			break;
		}
		echo json_encode($data);
	}

	function findNpsn(){
		if($this->input->is_ajax_request()){
			$npsn = $this->input->post('pelaksana');
			$ckdata = $this->db->get_where('ref.sekolah',array('npsn'=>$npsn))->result();
			$data['nama']		='';
			foreach ($ckdata as $key) {
				$data['nama']	  = $key->nama;
			}
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}
}