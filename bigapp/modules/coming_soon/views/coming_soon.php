<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <meta charset="utf-8" />
    <title>DIREKTORAT PSMK</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="Alvaro Setia Nusa" />
    <link href="data:image/x-icon;base64,AAABAAEAEBAAAAAAAABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAA/QEGAP/r6gDVBgQAx6urAKh6GwC8R0gA9fv/AOcBBwDqAgEA//r8AM6TkQDtBQEAwAYFALUTEQDhYGUAfHRtAKIGBgCoAgMAuri4AOmPjgB+fHwAzhQUAP/19wDWqaYA+/39AP/9/QDsAwUA7gICAOKmpgCdAQEAsIuNAKFyFwD6AgIAtJGNAPwABQDs0dUA8c/PAKiKZwDQDQkArAcKAPb4+ACSbm4A+Pj4APb8+wDp4eEA//byAIZ9gAD/+PgAszAzAOYFAwCtEhkA///7AMV5cwDADAcA9nR1APsBAAD8AQAA/gEAAMYQCgD6BgYAw4WFAF02CQDcwL8AhAUGAIslJgC+kzIA3wQBAPX6/wDaBwoA//f2AJABAwD6/PwA/vj5APo9PgD5/f8A//r/AFAAAgDPLSsAmIeEAOG5tAC3SEYAvW9pAN7IvQDFt7gAw7u7AOfo5gCZAAEAvKGkALYPEgDvAgUA7wQCAPMBAgDFBQYA9QECAPHIxgD16uwA4jpAAPkEAgBPBAYArZucANcIBgC/bGoAmY6RAONxeAD/hIMA+Tw9APv//gC2pKUA5wUGAO8AAADwu8QA6QUGAPQAAAClICIA9QAAAI+DgwDrf34A+gAAAJsJCwD5AwAA+QIGAPwDAADJCQoAqwACAPI2NQCuJyUA0svIAL2ijgClZGYA/f//AJlRSQCvGBQAwTAuAPYAAQD4AAEA9gMBAMQJCwB1BgQA+AMBAPsAAQCZHBgAtQEGANLQzwDAursA9v79AOICCAD4/v0Aj318AO4AAgDizskA+uHdAN1iYAD/3doA7nh9APMDAgDEeYEA5dLVAPR6egDPAQYA/AACAP0DAgD+AwIArAIBALw/QQCDAAUA2icmAMk3OwDgAgAA3gcGAD8BAQDnBgMAaAYGAP/+/gD43t4Aw5+ZAPICAADHAAQA2o+NAPcCAAD4AgAAeXR1AOEbFQD7AgAA/+TnAOqGhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKk2lHhJ8o1eKgQAAAAAABAOzO14BPCE2h5sLAAAAAECGoYU/VDBYFLN6SgAAAAAbZZZElJG1rwcae7C4AACqCSJMfxo+IJN0gi1DmAAAR0WCKWc0BUIrVRmtDmoAALGkEyxrroAmJGxIVpkIAh9bUJxGKQ9PU3dLCjVJbrJjIZ8ctzJtYC8ncDhxiaKerHaIOrM5KI8QUYy3iyOMN1+oklpzoIMXFS5ijHFhtiUAPU51m3iXkGSdtAxpjW8AAABBclmmqQ0zq45oMR0AAAAAAABdFlIRBqeEmgAAAAAAAAAAAGaVeVx9GAAAAAAAAAAAAAAAAH65AAAAAAAAAOAHAADAAwAAwAMAAMABAACAAQAAgAEAAIAAAAAAAAAAAAAAAAAAAAAAAAAAgAEAAMADAADwDwAA+B8AAP5/AAA=" rel="icon" type="image/x-icon" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/funcy/jquery.fancybox.css" media="screen" />
    <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.min.css"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style-responsive.min.css"> 
	<script src="<?php echo base_url();?>assets/plugins/pace/pace.min.js"></script>
</head>
<body class="pace-top">
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<div id="page-container" class="fade">
        <div class="error">
            <div class="error-code m-b-10"> <i class="fa fa-warning"></i></div>
            <div class="error-content">
                <div class="error-message">COMING SOON</div>
                <br/>
                <div>
                    <a href="<?php echo base_url();?>" class="btn btn-success">Halaman Utama</a>&nbsp;
					<button type="button" onclick="history.go(-1)" class="btn btn-success">Kembali </button>
                </div>
            </div>
        </div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	</div>
	<script src="<?php echo base_url();?>assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>
</html>
