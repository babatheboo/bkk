<style type="text/css">

ul {
    margin-left: 10px;
}
ul li {list-style-type: disc;}

</style>
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
        <ol style="list-style-type: decimal;">
                                <li>Pengertian
                                    <p style="text-align:justify">Bursa Kerja Khusus (BKK) adalah sebuah lembaga yang dibentuk di Sekolah Menengah Kejuruan Negeri dan Swasta, sebagai unit pelaksana yang memberikan pelayanan dan informasi lowongan kerja, pelaksana pemasaran, penyaluran dan penempatan tenaga kerja, merupakan mitra Dinas Tenaga Kerja dan Transmigrasi.</p>
                                </li>
                                <li>Latar Belakang
                                    <p style="text-align:justify">Sekolah Menengah Kejuruan (SMK) sebagai sub sistem pendidikan nasional yang bertanggungjawab dalam penyiapan SDM tingkat menengah yang handal, berorientasi kepada kebutuhan pasar harus mampu mengembangkan inovasi untuk mempengaruhi perubahan kebutuhan pasar sehingga dapat mewujudkan kepuasan pencari kerja. BKK SMK merupakan salah satu komponen penting dalam mengukur keberhasilan pendidikan di SMK, karena BKK menjadi lembaga yang berperan mengoptimalkan penyaluran tamatan SMK dan sumber informasi untuk pencari kerja. Pemberdayaan BKK SMK merupakan salah satu fungsi dalam manajemen sekolah yaitu sebagai bagian pembinaan terhadap proses pelaksanaan kegiatan BKK SMK yang telah direncanakan dalam upaya mencapai tujuan pendidikan SMK. BKK SMK merupakan salah satu komponen pelaksanaan pendidikan sistem ganda, karena tidak mungkin bisa dilaksanakan proses pembelajaran yang mengarah kepada kompetensi jika tidak ada pasangan industri/usaha kerja, sebagai lingkungan kerja dimana siswa belajar keahlian dan profesional serta etos kerja sesuai dengan tuntutan dunia kerja.</p>
                                </li>
                                <li>Tujuan
                                   
                                        <ul>
                                            <li>Sebagai wadah dalam mempertemukan tamatan dengan pencari kerja.</li>
                                            <li>Memberikan layanan kepada tamatan sesuai dengan tugas dan fungsi masing-masing seksi yang ada dalam BKK.</li>
                                            <li>Sebagai wadah dalam pelatihan tamatan yang sesuai dengan permintaan pencari kerja</li>
                                            <li>Sebagai wadah untuk menanamkan jiwa wirausaha bagi tamatan melalui pelatihan.</li>
                                        </ul>
                                </li>
                                <li>Ruang Lingkup Kegiatan
                                   
                                        <ul>
                                            <li>Penyusunan database siswa lulusan SMK pencari kerja dan perusahaan pencari tenaga kerja dan penelusuran tamatan siswa SMK.</li>
                                            <li>Menjaring informasi tentang pasar kerja melalui iklan di media massa, internet, kunjunagn ke dunia usaha (industri) maupun kerjasama dengan lembaga penyalur tenaga kerja dan Depnakertrans.</li>
                                            <li>Membuat leaflet informasi dan pemasaran lulusan SMK yang dikirim kedunia usaha/industri yang terkait Depnakertrans. </li>
                                            <li>Penyaluran calon tenaga kerja lulusan SMK ke dunia usaha dan industri.</li>
                                            <li>Melakukan proses tindak lanjut hasil pengiriman dan penempatan tenaga kerja melalui kegiatan penjajakan dan verifikasi.</li>
                                            <li>Mengadakan program pelatihan ketrampilan tambahan/khusus bagi siswa dan lulusan SMK disesuaikan dengan bidang keahlian yang diperlukan.</li>
                                            <li>Mengadakan program bimbingan menghadapi tahapan proses penerimaan siswa dalam suatu pekerjaan (wawancara, psikotest).</li>
                                            <li>Memberikan informasi kepada para ALUMNI ataupun para lulusan SMK lain yang membutuhkan informasi tentang lowongan kerja.</li>
                                        </ul>
                           
                                </li>
                                <li>Penyaluran Dan Penempatan Tamatan
                                    <p style="text-align:justify">
                                        Adapun pelaksanaan penyaluran dan penempatan tamatan yang dapat dilakukan BKK SMK adalah sebagai berikut :
                                        <ul>
                                            <li>Menindaklanjuti kerjasama dengan industri pasangan yang telah menjadi mitra kerja dengan BKK sekolah. </li>
                                            <li>Melakukan penelusuran alumni dan dimasukkan ke dalam database sekolah.</li>
                                            <li>Merangkul pengurus Majelis Sekolah yang peduli dengan penempatan tenaga kerja dari alumni.</li>
                                            <li>Membuat website khusus BKK yang selalu up to date yang dapat di link dengan situs-situs JOB CARRIER.</li>
                                            <li>Menanamkan jiwa enterpreunership kepada siswa melalui pelatihan ketrampilan untuk menjadi seorang wirausaha (enterpreuneur).</li>
                                        </ul>
                                    </p>
                                </li>
                                <li>Kegiatan Bursa Kerja Khusus
                                    <p style="text-align:justify">
                                        <ul>
                                            <li>Merencanakan program kerja hubungan industri setiap program studi.
                                                <ol>
                                                    <li>Mengadakan pertemuan dengan Kajur tentang penempatan siswa-siswi prakerin.</li>
                                                    <li>Mengadakan koordinasi dengan panitia PSG tentang penempatan siswa-siswi prakerin.</li>
                                                    <li>Mengadakan koordinasi dengan panitia PSG tentang guru monitoring.</li>
                                                </ol>
                                            </li>
                                            <li>Melakukan proses negosiasi dengan DU/DI dan pemerintah sebagai mitra dalam penempatan siswa-siswi prakerin.</li>
                                            <li>Menjalin kerjasama (MOU) dengan DU/DI dalam : 
                                                <ol>
                                                    <li>Sinkronisasi Kurikulum</li>
                                                    <li>Pelatihan</li>
                                                    <li>Penempatan tamatan</li>
                                                </ol>
                                            </li>
                                            <li>Pemetaan DU/DI</li>
                                            <li>Menjalin kerjasama dengan Depnakertrans tentang pelatihan (Magang) dan penempatan tamatan.</li>
                                            <li>Membentuk Majelis Sekolah.</li>
                                            <li>Membuat database penelusuran tamatan baik yang sudah bekerja maupun belum bekerja.</li>
                                            <li>Membentuk Ikatan alumni.</li>
                                            <li>Membuat mading informasi lowongan kerja.</li>
                                            <li>Membuat website khusus BKK </li>
                                            <li>Membuat Laporan Kegiatan </li>
                                            <li>Monitoring dan Evaluasi</li>
                                        </ul>
                                    </p>
                                </li>
                            </ol>
        </div>
    </div>
</div>