<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About extends CI_Controller {
	public function __construct(){
  		parent::__construct();
 	}

	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['menu'] = "about";
		$isi['content'] = 'about_view';
		$this->load->view("frontpage/front_view",$isi);
	}
	
}
