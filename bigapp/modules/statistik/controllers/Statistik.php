<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Statistik extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
  		//$this->load->model('dashboard/dashboard_model');
		$this->db1 = $this->load->database('default', TRUE);
  		//$this->load->library('bcrypt');
		//$this->db1 = $this->load->database('default', TRUE);
		//$this->load->model('statistik_model');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['page'] = "statistik";
		$isi['link'] = 'statistik';
		$isi['halaman'] = "Tentang BKK";
		$isi['judul'] = "Tentang BKK";
		$isi['content'] = "statistik_view";

		$isi['tombolsimpan'] = "Daftar";
		$isi['tombolbatal'] = "Batal";
		$isi['action'] = "statistik";
		$isi['content'] = "statistik_view";

		
		$this->load->view("frontpage/home_view",$isi);
	}
	
}
