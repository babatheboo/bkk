<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class His_loker_model extends CI_Model {
    var $table  = 'view_lowongan';
    var $table2 = 'bidding_lulusan';
    var $table3 = 'lowongan_bookmark';

    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
     
    public function get_lowongan_siswa(){
        $psid =  $this->session->userdata('user_id');
        $this->db->select("*,bidding_lulusan.id as id_bidding,bidding_lulusan.tgl_bid,bidding_lulusan.status");
        $this->db->from($this->table);
        $this->db->join($this->table2,'view_lowongan.id = bidding_lulusan.id_loker');
        $this->db->where('bidding_lulusan.id_siswa',$psid);
        $query = $this->db->get();
        return $query;
    }

    public function get_bookmark_lokerswa(){
        $psid =  $this->session->userdata('user_id');
        $this->db->select("*,lowongan_bookmark.id as id_bookmark,lowongan_bookmark.tgl");
        $this->db->from($this->table);
        $this->db->join($this->table3,'view_lowongan.id = lowongan_bookmark.id_loker');
        $this->db->where('lowongan_bookmark.peserta_didik_id',$psid);
        $query = $this->db->get();
        return $query;
    }
}