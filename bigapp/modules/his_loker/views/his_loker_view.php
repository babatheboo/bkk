<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<style type="text/css">
    .modal-footer button {
      float:right;
      margin-left: 10px;
    }
</style>
<script>
$(document).ready(function() {
    FormPlugins.init();
});
</script>
<div class="row">
    <div class="col-md-6">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Historly Apply Lowongan</h4>
                </div>
                <div class="panel-body">
                    <p style="text-align:center">
                        History Lowongan
                    </p>
                    <div>
                        <div data-scrollbar="true" data-height="250px">
                            <div id="result-rekomend"></div>
                            </br>
                            <?php
                            if(!empty($databid)){
                                foreach ($databid as $key) {
                                    $judul      = $key->judul;
                                    $deskripsi  = $key->deskripsi;

                                    $tgl_buka   = date('d-m-Y',strtotime($key->tgl_buka));
                                    $tgl_tutup  = date('d-m-Y',strtotime($key->tgl_tutup));
                                    $alamat     = $key->alamat;
                                    $id_bidding = $key->id_bidding;
                                    $tgl_bid    = date('d-m-Y',strtotime($key->tgl_bid));
                                    $status_bid = $key->status;
                                    $nama       = $key->nama;
                                    $nama_bidang = $key->nama_bidang_usaha;
                                    
                                    switch ($status_bid) {
                                        case "0":
                                            $status_loker = '<span class="label label-primary"> Tolak </span>';
                                            break;
                                        case "1":
                                            $status_loker = '<span class="label label-primary"> Baru Bid </span>';
                                            break;
                                        case "2":
                                             $status_loker = '<span class="label label-primary"> Panggilan </span>';
                                            break;
                                        case "3":
                                            $status_loker = '<span class="label label-primary"> Tes </span>';
                                            break;
                                        case "4":
                                            $status_loker = '<span class="label label-primary"> Terima </span>';
                                            break;    
                                        default:
                                            $status_loker = "belum bid";
                                    }    

                                    echo '<ul class="list-group list-group-lg no-radius list-email">
                                        <li class="list-group-item primary">
                                            <div class="email-info">
                                                <span class="email-time" title="Batas Lowongan">
                                                Tanggal tutup : '.date("d-m-Y",strtotime($key->tgl_tutup)).'
                                                </span>
                                                <h5 class="email-title" title="Nama Perusahaan">
                                                    Nama perusahaan : '.$key->nama.'</br>
                                                   
                                                    <span class="label label-primary f-s-10" title="Bidang Usaha Yang Di Butuhkan">Jenis bidang usaha : '.$key->nama_bidang_usaha.'</span>
                                                </h5>
                                                <p class="email-desc">Nama Lowongan : '.$key->judul.'</p>
                                                <p class="email-desc">Status loker : '.$status_loker.'</p>
                                                <p class="email-desc"><small><b>Range Gaji : '.$key->range.'</b></small></p>
                                            </div>
                                            <div style="text-align:right">
                                                <a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($key->id_loker).'" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="'.$key->nama.'">More</a>
                                            </div>                        
                                        </li>
                                    </ul>';
                                }
                            }
                            ?>  
                        </div>
                    </div>
                </div>
        </div>
        </div>
    <div class="col-md-6">
        <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">History Bookmark</h4>
                </div>
                <div class="panel-body">
                    <p style="text-align:center">
                        Bookmark Loker
                    </p>
                    <div>
                        <div data-scrollbar="true" data-height="250px">
                            <div id="result-rekomend"></div>
                            </br>
                            <?php
                            if(!empty($databookmark)){
                                foreach ($databookmark as $mark) {
                                    $judul      = $mark->judul;
                                    $deskripsi  = $mark->deskripsi;
                                    $tgl_buka   = date('d-m-Y',strtotime($mark->tgl_buka));
                                    $tgl_tutup  = date('d-m-Y',strtotime($mark->tgl_tutup));
                                    $alamat     = $mark->alamat;
                                    $id_bookmark = $mark->id_bookmark;
                                    $tgl_bookmark    = date('d-m-Y',strtotime($mark->tgl));
                                    $nama       = $mark->nama;
                                    $nama_bidang = $mark->nama_bidang_usaha;   

                                    echo '<ul class="list-group list-group-lg no-radius list-email">
                                        <li class="list-group-item primary">
                                            <div class="email-info">
                                                <span class="email-time" title="Batas Lowongan">
                                                Tanggal tutup : '.date("d-m-Y",strtotime($mark->tgl_tutup)).'
                                                </span>
                                                <h5 class="email-title" title="Nama Perusahaan">
                                                    Nama perusahaan : '.$mark->nama.'</br>
                                                   
                                                    <span class="label label-primary f-s-10" title="Bidang Usaha Yang Di Butuhkan">Jenis bidang usaha : '.$mark->nama_bidang_usaha.'</span>
                                                </h5>
                                                <p class="email-desc">Nama Lowongan : '.$mark->judul.'</p>
                                                <p class="email-desc"><small><b>Range Gaji : '.$mark->range.'</b></small></p>
                                            </div>
                                            <div style="text-align:right">
                                                <a href="'.base_url().'/loker/detil_loker/'.$this->asn->encode($key->id_loker).'" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="'.$key->nama.'">More</a>
                                                <a onclick="unbookmark(\''.$key->judul.'\',\''.$this->asn->encode($key->id_loker).'\')" href="javascript:void(0)" class="btn btn-danger btn-xs m-r-5">Unbookmark</a>
                                            </div>

                                        </li>
                                    </ul>';
                                }
                            }
                            ?>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>