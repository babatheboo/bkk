<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class His_loker extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("3");
		$this->db = $this->load->database('default', TRUE);
		$this->load->model('His_loker_model','hs_loker_model');
  		$this->load->library('bcrypt');
  		$this->load->helper('form');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$lvl = $this->session->userdata('level');
		$isi['kelas'] = "lok_siswa";
		$isi['namamenu'] = "Lowongan Kerja Siswa";
		$isi['page'] = "lok_siswa";
		$isi['link'] = 'His_loker';
		$isi['actionhapus'] = 'hapus';
		$isi['actionedit'] = 'edit';
		$isi['halaman'] = "Data profile Siswa";
		$psid =  $this->session->userdata('user_id');    
		$isi['databid'] = $this->hs_loker_model->get_lowongan_siswa()->result();	
		$isi['databookmark'] = $this->hs_loker_model->get_bookmark_lokerswa()->result();	
		$isi['judul'] = "";
		$isi['content'] = "his_loker_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}

	// public function getLoker_bid(){
	// 	if($this->input->is_ajax_request()){
	// 		$sess = $this->session->userdata('role_');
	// 		$page =  $_GET['page'];
	// 		$offset = 10*$page;
	//         $limit = 10;
	//         // /$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE sekolah_id = '$sess' AND status = '1' LIMIT  $limit OFFSET $offset")->result();
	// 		$ckdata = $this->db->query("select * from view_lowongan join bidding_lulusan on view_lowongan.id = bidding_lulusan.id_loker")->result();
	// 		foreach ($ckdata as $row) {
	// 			echo '<ul class="list-group list-group-lg no-radius list-email">
 //                        <li class="list-group-item primary">
 //                            <div class="email-info">
 //                                <span class="email-time" title="Batas Lowongan">
 //                                Tanggal tutup : '.date("d-m-Y",strtotime($row->tgl_tutup)).'
 //                                </span>
 //                                <h5 class="email-title" title="Nama Perusahaan">
 //                                    Nama perusahaan : '.$row->nama.'</br>
                                   
 //                                    <span class="label label-primary f-s-10" title="Bidang Usaha Yang Di Butuhkan">Jenis bidang usaha : '.$row->nama_bidang_usaha.'</span>
 //                                </h5>
 //                                <p class="email-desc">Nama Lowongan : '.$row->judul.'</p>
 //                                <p class="email-desc">Status loker : '.$status_loker.'</p>
 //                                <p class="email-desc"><small><b>Range Gaji : '.$row->range.'</b></small></p>
 //                            </div>
 //                            <div style="text-align:right">
 //                                <a href="javascript:void(0)" onclick="(\'aktif\',\'Data Lowongan\',\'loker\','."'".$row->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>
 //                            </div>            
 //                        </li>
 //                    </ul>';
	// 		}
 //        	exit;
	// 	}else{
	// 		redirect("_404","refresh");
	// 	}	
	// }
}
