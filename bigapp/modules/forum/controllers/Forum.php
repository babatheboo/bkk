<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forum extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->load->model('forum_model');
 	}
	public function index(){
		$this->_content();
		
	}
	public function _content(){
		redirect('dashboard','refresh');
	}

	public function kategori($id){
		$config = array();
		$idnya = $this->asn->dec_enc("decrypt", $id);
		$dt = $this->db->query("SELECT nama_kategori FROM kategori_forum WHERE id='$idnya'")->row();
		if($dt->nama_kategori!=""){
				$config['base_url'] = base_url() . "forum/kategori/" . $id;
				$config['total_rows'] = $this->forum_model->countPost($idnya);
				$config['per_page'] = 5;
				$config['uri_segment'] = 5;
				$choice = $config['total_rows'] / $config['per_page'];
				$config['query_string_segment'] = 'page';
				$config['num_links'] = floor($choice);
				$config['reuse_query_string'] = TRUE;
				$config['use_page_numbers'] = TRUE;
				$config['page_query_string'] = TRUE;
				$config['first_link'] = "First";
				$config['last_link'] = "Last";
				$config['pref_link'] = "Prev";
				$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		        $config['full_tag_close']   = '</ul></nav></div>';
		        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		        $config['num_tag_close']    = '</span></li>';
		        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['prev_tagl_close']  = '</span>Next</li>';
		        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		        $config['first_tagl_close'] = '</span></li>';
		        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['last_tagl_close']  = '</span></li>';
		 
				 if (!isset($_GET[$config['query_string_segment']]) || $_GET[$config['query_string_segment']] < 1) {
		        	$_GET[$config['query_string_segment']] = 1;
		   		 }
			    $limit = $config['per_page'];
			    $offset = ($this->input->get($config['query_string_segment']) - 1) * $limit;


		        $this->pagination->initialize($config);
		        $isi['page'] ="test";
				$isi['link'] 	= 'forum';
				$isi['halaman'] = "Forum";
				$isi['judul'] 	= $dt->nama_kategori . "<small> (BETA)</small>";
				$isi['idkategori'] = $idnya;
		 		$isi['content'] = "pengumuman";
		 		$isi['data'] = $this->forum_model->get_posting($idnya,$config['per_page'], $offset);
		 		$isi['pagination'] = $this->pagination->create_links();
				$this->load->view("dashboard/dashboard_view",$isi);
		} else {
			redirect("_404");
		}
	 
	}


	public function detil_forum($id){
		$idnya = $this->asn->dec_enc("decrypt", $id);
		if($idnya!=""){
				$config['base_url'] = base_url() . "forum/detil_forum/" . $id . "/";
			//	$config['total_rows'] = $this->db->count_all('post_komen', array('id_post'=>$idnya));
				$config['total_rows'] = $this->forum_model->count_Post($idnya);
				$config['per_page'] = 5;
				$config['uri_segment'] = 5;
				$choice = $config['total_rows'] / $config['per_page'];
				$config['query_string_segment'] = 'page';
				$config['num_links'] = floor($choice);
				$config['reuse_query_string'] = TRUE;
				$config['use_page_numbers'] = TRUE;
				$config['page_query_string'] = TRUE;
				$config['num_links'] = round($choice);
				$config['first_link'] = "First";
				$config['last_link'] = "Last";
				$config['pref_link'] = "Prev";
				$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		        $config['full_tag_close']   = '</ul></nav></div>';
		        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		        $config['num_tag_close']    = '</span></li>';
		        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['prev_tagl_close']  = '</span>Next</li>';
		        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		        $config['first_tagl_close'] = '</span></li>';
		        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		        $config['last_tagl_close']  = '</span></li>';
		 
		       if (!isset($_GET[$config['query_string_segment']]) || $_GET[$config['query_string_segment']] < 1) {
		        	$_GET[$config['query_string_segment']] = 1;
		   		 }
			    $limit = $config['per_page'];
			    $offset = ($this->input->get($config['query_string_segment']) - 1) * $limit;
			$isi['page'] = "OH";
			$isi['link'] 	= 'forum';
			$isi['halaman'] = "Forum";
			$isi['judul'] 	= "Diskusi" . "<small> (BETA)</small>";
			$isi['idkategori'] = $idnya;
			$isi['data'] = $this->db->query("SELECT * FROM post_forum WHERE id='$idnya'")->result();
			$isi['komen'] = $this->forum_model->get_post($idnya,$config['per_page'], $offset);
	 		$isi['content'] = "detil";
	 		$isi['pagination'] = $this->pagination->create_links();
			$this->load->view("dashboard/dashboard_view",$isi);
		} else {
			redirect("_404");
		}
		
	}


	public function tambah($id){
		$idnya = $this->asn->dec_enc("decrypt", $id);
		if($idnya!=""){
		$isi['page'] = "forum";
		$isi['link'] 	= 'forum';
		$isi['halaman'] = "Forum";
		$isi['judul'] 	= "Buat Diskusi Baru" . "<small> (BETA)</small>";
		$isi['idkategori'] = $idnya;
 		$isi['content'] = "forum_baru";
		$this->load->view("dashboard/dashboard_view",$isi);
		} else {
			redirect("_404");
		}
	}




public function postKomen(){
		if ($this->session->userdata('level')==4) {
			$sess = 'c12720c7-12bc-4f44-bada-17abfe07f88a';
		}else{
			$sess = $this->session->userdata('role_');
		}
		$komentar = $this->input->post('ket');
		$idkat 	  = $this->input->post('idkat');
		$idpost   = $this->input->post('idpos');
		$tgl 	  = date("Y-m-d h:i:s");
		$resp 	  = "";
		if ($komentar!="") {
			$sip = array('id_post'=>$idpost, 'isi'=>$komentar, 'tgl_dibuat'=>$tgl, 'status'=>'1', 'id_kategori'=>$idkat, 'dibuat_oleh'=>$sess);
			$this->db->insert("post_komen",$sip);
			$resp = "ok";
		}else{
			$resp = "no";
		}
		echo json_encode($resp);
	}

	public function hpsKomen($id){
		$this->db->where('id', $id);
		if($this->db->delete('post_komen')){
			$resp = "ok";
		} else {
			$resp = "not";
		}
		echo json_encode($resp);
	}

	public function cekKomen($id){
		if($this->input->is_ajax_request()){
			$ckdata = $this->db->get_where('post_komen',array('id'=>$id))->result();
			$data['id']='';
			$data['isi']='';
			foreach ($ckdata as $key) {
				$data['id']=$id;
				$data['isi']=$key->isi;
			}
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}

	public function edtKomen($id){
		$sess = $this->session->userdata('role_');
		$komE = $this->input->post('isi');
		$idkat = $this->input->post('idkat');
		$tgl 	  = date("Y-m-d h:i:s");
		if ($this->db->query("UPDATE post_komen SET isi='$komE', tgl_dibuat='$tgl' WHERE dibuat_oleh='$sess' AND id_post='$idkat'")) {
			$resp = "ok";
		}else{
			$resp = "no";
		}
		echo json_encode($resp);
	}


	public function simpanbaru(){
		$id = $this->input->post('id');
		$idnya = $this->asn->dec_enc("decrypt", $id);
		$jdl = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$tgl = date('Y-m-d H:i:s');
		if ($this->session->userdata('level')==4) {
			$pem = 'c12720c7-12bc-4f44-bada-17abfe07f88a';
		}else{
		$pem = $this->session->userdata('role_');
		}
		$data = array('id_kategori'=>$idnya, 'judul'=>$jdl,'desc'=>$isi,'dibuat_oleh'=>$pem,'status'=>"0", 'tgl_dibuat'=>$tgl);
		if($this->db->insert('post_forum',$data)){
			$resp = "ok";
		} else {
			$resp = "not";
		}
		echo json_encode($resp);
	}

  
}
