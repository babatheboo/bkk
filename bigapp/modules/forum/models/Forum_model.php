<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}

	function get_posting($idkategori, $limit, $start){
		$this->db->limit($limit,$start);
		$this->db->where('id_kategori', $idkategori);
		$this->db->order_by('tgl_dibuat', 'desc');
		$query = $this->db->get('post_forum');
		return $query;
	}

	function countPost($idkategori){
		$this->db->select('id');
		$this->db->from('post_forum');
		$this->db->where('id_kategori',$idkategori);
		$query = $this->db->get()->num_rows();
		return $query;
	}


	function get_post($idkategori, $limit, $start){
		$this->db->limit($limit, $start);
		$this->db->where('id_post',$idkategori);
		$this->db->where('dibuat_oleh is NOT NULL');
		$query = $this->db->get("post_komen");
		return $query;
	}


	function count_Post($idkategori){
		$this->db->select('id');
		$this->db->from('post_komen');
		$this->db->where('id_post',$idkategori);
		$query = $this->db->get()->num_rows();
		return $query;
	}


}
