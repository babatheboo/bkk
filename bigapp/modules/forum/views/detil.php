<script src="<?php echo base_url();?>assets/backend/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>

<div class="col-md-12">
    <!-- begin pagination -->
    <!-- end pagination -->
    <?php
    if (count($data)>0) {
        foreach ($data as $key) {
            $dibat = $key->dibuat_oleh;
            $id = $key->id;
            $desc =  $key->desc;
            $idkat = $key->id_kategori;
            $status = $key->status;
            $post = $key->tgl_dibuat;
            $tglPost = new DateTime($post);
            $now = new DateTime(date("Y-m-d h:i:s"));
            $intv = $tglPost->diff($now);
        }
    }else{
        redirect('_404');
    } ?>                
    <!-- topik -->
    <ul class="forum-list forum-detail-list">
        <li>
            <div class="media">
                <?php
                $sess = $this->session->userdata('role_');
                if ($this->session->userdata('level')=='4') { ?>
                    <img src="<?php echo base_url();?>assets/foto/tutwuri.png" alt=""/>
                <?php }else{
                    $ck =  $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id = '$key->dibuat_oleh'")->row(); ?>
                    <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ck->logo;?>" alt=""/>
                <?php } ?>
            </div>
            <!-- end media -->
            <!-- begin info-container -->
            <div class="info-container">
                <div class="post-user"><?php
                if ($key->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') {
                    echo "Administrator";
                }else{
                    $nm = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='".$key->dibuat_oleh."'")->row(); echo $nm->nama;   
                } ?><small>Topik</small></div>
                <div class="post-content">
                    <?php echo $desc;?>
                </div>
                <div class="post-time"><?php echo $intv->format('%i menit lalu');?></div>
            </div>
            <!-- end info-container -->
        </li>
    </ul>
    <!-- /topik -->

    <!-- balasan -->
    <ul class="forum-list forum-detail-list">
        <?php
        foreach ($komen->result() as $kek) {
            $post = $kek->tgl_dibuat;
            $tglPost = new DateTime($post);
            $now = new DateTime(date("Y-m-d h:i:s"));
            $intv = $tglPost->diff($now);
            ?>
            <?php
            $sess=$this->session->userdata('role_');
            if ($kek->dibuat_oleh==$sess) {
                ?>
                <!-- edit -->
                <li>
                    <div class="media">
                        <?php
                        $sess = $this->session->userdata('role_');
                        if ($kek->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') { ?>
                            <img src="<?php echo base_url();?>assets/foto/tutwuri.png" alt="" />
                        <?php }else{
                            $ck =  $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id = '$sess'")->row();?>
                            <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ck->logo;?>" alt="" />
                        <?php }?>
                    </div>
                    <div class="info-container">
                        <div class="pull-right">
                            <a href="javascript:void(0)" onclick="cekKomen(<?php echo $kek->id;?>)" class="text-success m-r-5"><i class="fas fa-pencil-alt fa-lg"></i></a>
                            <a href="javascript:void(0)" onclick="hpsKomen(<?php echo $kek->id;?>)" class="text-danger"><i class="fas fa-trash fa-lg"></i></a>
                        </div>
                        <div class="post-user"><?php $nm = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='".$kek->dibuat_oleh."'")->row(); echo $nm->nama;?><small>Balasan</small></div>
                        <div class="post-content">
                            <?php echo $kek->isi;?>
                        </div>
                    </li>
                    <!-- /edit -->
                <?php }else{ ?>
                    <!-- unedit -->
                    <li>
                        <div class="media">
                            <?php
                            if ($kek->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') { ?>
                                <img src="<?php echo base_url();?>assets/foto/tutwuri.png" alt=""/>
                            <?php }else{
                                $ck =  $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id = '".$kek->dibuat_oleh."'")->row();
                                    if ($ck->logo=='') {
                                        echo '<img src="<?php echo base_url();?>assets/foto/bkk/no.jpg" alt=""/>';
                                    }else{
                                ?>
                                <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ck->logo;?>" alt=""/>
                            <?php } } ?>
                        </div>
                        <div class="info-container">
                            <div class="post-user"><?php
                            if ($kek->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') {
                                echo "Administrator";
                            }else{
                                $nm = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='".$kek->dibuat_oleh."'")->row(); echo $nm->nama; } ?><small>Balasan</small></div>
                                <div class="post-content">
                                    <?php echo $kek->isi;?>
                                </div>

                            </li>
                            <!-- /unedit -->
                        <?php } }?>
                    </ul>
                    <!-- /balasan -->

                    <!-- begin pagination -->
                    <div class="pull-right">
                        <?php echo $pagination; ?>
                    </div>
                    <!-- end pagination -->

                    <!-- begin media -->
                    <?php 
                    if($status=="0"){
                        ?>
                        <ul class="forum-list forum-detail-list">
                            <li>
                                <!-- begin media -->
                                <div class="media">
                                    <?php
                                    $sess = $this->session->userdata('role_');
                                    if ($this->session->userdata('level')==4) { ?>
                                     <img  src="<?php echo base_url();?>assets/foto/tutwuri.png" alt="" />
                                 <?php }else{
                                    $ck =  $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id = '$sess'")->row();?>
                                    <img  src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ck->logo;?>" alt="" />
                                <?php }?>
                            </div>
                            <!-- end media -->
                            <!-- begin info-container -->
                            <div class="info-container">
                                <div class="post-user">
                                    <small>Tulis Komentar</small>
                                </div>
                                <form class="form" id="formKomen" action="" method="POST">
                                    <input type="hidden" name="idkat" id="idkat" value="<?php echo $idkat;?>">
                                    <input type="hidden" name="idpost" id="idpost" value="<?php echo $key->id;?>">
                                    <div class="form-group">
                                        <div>
                                            <textarea class="ckeditor" id="komentar" name="komentar" rows="20"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row m-b-8">
                                        <div class="col-md-8">
                                            <button type="button" onclick="postKomen();" class="btn btn-sm btn-primary m-r-5">Kirim</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- end info-container -->
                        </li>
                    </ul>
                <?php } ?>
            </div>
            <!-- edit komentar -->
            <div id="modalEdit" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Komentar</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form" id="edtForm" action="" method="POST">
                                <input type="hidden" name="edtPost" id="edtPost" value="<?php echo $idkategori;?>">
                                <div class="form-group">
                                    <div>
                                        <textarea class="ckeditor" id="komenE" name="komenE" rows="20"></textarea>
                                    </div>
                                </div>
                            </form>                
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSave" onclick="edtKomen('<?php echo $kek->id;?>')" class="btn btn-primary">Simpan</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /edit komentar -->
            <script src="<?php echo base_url();?>assets/backend/js/forum.js"></script>
