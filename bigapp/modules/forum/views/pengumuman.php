
<div class="row">
                <!-- begin col-9 -->
                <div class="col-md-9">
                    <!-- begin panel-forum -->
                    <div class="panel panel-forum">
                        <!-- begin forum-list -->
                        <ul class="forum-list forum-topic-list">
                         <?php
                            foreach ($data->result() as $key) {
                                    $post = $key->tgl_dibuat;
                                    $tglPost = new DateTime($post);
                                    $now = new DateTime(date("Y-m-d h:i:s"));
                                    $intv = $tglPost->diff($now);
                                    $idnya = $this->asn->dec_enc("encrypt", $key->id);
                            ?>
                            <li>
                                <div class="media">
                                <?php
                                if($idkategori=="1"){ ?>
                                        <img src="<?php echo base_url();?>assets/foto/tutwuri.png" alt="" />
               <?php                 } else {
                                 $ck =  $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id = '".$key->dibuat_oleh."'")->row();
?>
                                    <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $ck->logo;?>" alt="" />
<?php } ?>
                                </div>
                                <!-- end media -->
                                <!-- begin info-container -->
                                <div class="info-container">
                                    <div class="info">
                                        <h4 class="title"><a href="<?php echo base_url();?>forum/detil_forum/<?php echo $idnya;?>"><?php echo $key->judul;?></a></h4>
                                        <ul class="info-start-end">
                                            <li>dibuat oleh 
                                            <?php
                                            if ($key->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') {
                                                echo "Administrator";
                                            }else{
                                            $nm = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='".$key->dibuat_oleh."'")->row(); 
                                            echo $nm->nama;
                                            }
                                            ?>
                                            </li>
                                            <li>
                                            <?php 
                                                $reply = $this->db->query("SELECT * from post_komen a left join ref.sekolah b on a.dibuat_oleh=b.sekolah_id where a.id_post='".$key->id."' order by a.tgl_dibuat desc limit 1")->row(); 
                                                if(isset($reply)){
                                                        $id = $reply->id_post;
                                                        $idnya = $this->asn->dec_enc('encrypt', $id);
                                                        echo 'balasan terakhir <a href="' . base_url() . 'forum/detil_forum/' . $idnya  . '">' . $reply->nama; 
                                                }
                                                ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="date-replies">
                                        <div class="time">
                                            <?php echo $intv->format('%d menit lalu');?>
                                        </div>
                                        <div class="replies">
                                            <div class="total"><?php echo $this->db->query("SELECT * FROM post_komen WHERE id_post='".$key->id."'")->num_rows();?></div>
                                            <div class="text">BALASAN</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end info-container -->
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="text-right">
                    <?php echo $pagination; ?>
                    </div>
                    <!-- end pagination -->
                    <?php 
                    if($this->session->userdata('level')=="4" && $idkategori=="1"){
                         $idnya = $this->asn->dec_enc("encrypt", $idkategori);
                    ?>
                        <input type="button" value="Buat posting baru" class="btn btn-default m-r-5 m-b-5" onclick="baru('<?php echo $idnya;?>')">
                    <?php
                    } else if ($idkategori!="1") {
                        $idnya = $this->asn->dec_enc("encrypt", $idkategori);
?>
<input type="button" value="Buat posting baru" class="btn btn-default m-r-5 m-b-5" onclick="baru('<?php echo $idnya;?>')">
<?php                     }
                    ?>
                    
                </div>
                <!-- end col-9 -->
                <!-- begin col-3 -->
                <div class="col-md-3">
                    <!-- begin panel-forum -->
                    <div class="panel panel-forum">
                        <div class="panel-heading">
                            <h4 class="panel-title">Diskusi Aktif</h4>
                        </div>
                        <!-- begin threads-list -->
                        <ul class="threads-list">
<?php
    $dt = $this->db->query("SELECT count(id_post) as total, id_post FROM post_komen  WHERE id_kategori='$idkategori' GROUP BY id_post  LIMIT 5")->result();
    if(count($dt)>0){
        foreach ($dt as $key) {
            $idnya = $this->asn->dec_enc("encrypt", $key->id_post);
            $da = $this->db->query("SELECT id,judul FROM post_forum WHERE id='$key->id_post'")->result();
            foreach ($da as $kes) {
        ?>
                    <li>
                                <h4 class="title"><a href="<?php echo base_url();?>forum/detil_forum/<?php echo $idnya;?>"><?php echo $kes->judul;?></a></h4>
                                <?php
                                    $km = $this->db->query("SELECT b.nama, a.dibuat_oleh FROM post_komen a LEFT join ref.sekolah b ON a.dibuat_oleh=b.sekolah_id WHERE 
a.id_post='$key->id_post' ORDER BY id DESC")->row();
                                    if ($km->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a') {
                                        echo "Komentar terakhir oleh Administrator";
                                    }else{
                                        echo "komentar terakhir oleh ".$km->nama."";
                                    }
                                ?>
                    </li>


        <?php 
            }
        }
    }
?>
                        </ul>
                        <!-- end threads-list -->

                    </div>
                    <!-- end panel-forum -->

                </div>
                <!-- end col-3 -->
            </div>



<script >
  function baru(idkategori){
    window.location.href = "<?php echo base_url();?>forum/tambah/" + idkategori;
  }
</script>
