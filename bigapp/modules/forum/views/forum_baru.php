<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
<!-- begin panel-heading -->
<div class="panel-heading">
    <h4 class="panel-title">Form diskusi baru</h4>
</div>
<!-- end panel-heading -->
<!-- begin panel-body -->
<div class="panel-body">
    <form class="form" id="formdiskusi" action="" method="POST">
        <div class="form-group row m-b-15">
            <label class="col-form-label col-md-3">Judul diskusi</label>
            <div class="col-md-9">
                <input type="text" name="judul" id="judul" class="form-control m-b-5" placeholder="masukan judul diskusi">
            </div>
        </div>
        
        <div class="form-group row m-b-15">
            <label class="col-form-label col-md-3">Isi diskusi</label>
            <div class="col-md-9">
            <input type="hidden" name="idkategori" id="idkategori" value="<?php echo $this->asn->dec_enc("encrypt", $idkategori);?>">
                <textarea class="ckeditor" id="editor1" name="editor1" rows="20"></textarea>
            </div>
        </div>
        <div class="form-group row m-b-15">
        <label class="col-form-label col-md-3"></label>
            <div class="col-md-9">
                <button type="button" onclick="simpanbaru()" class="btn btn-sm btn-primary m-r-5">Kirim</button>
                <button type="button" onclick="batal()" class="btn btn-sm btn-default">Batal</button>
            </div>
        </div>
    </form>
</div>
<script src="<?php echo base_url();?>assets/backend/plugins/ckeditor/ckeditor.js"></script>
<script >
    function batal(){
        window.location.href = "<?php echo base_url();?>";
    }

    function simpanbaru(){
        var url = "<?php echo base_url();?>";
        var jdl = $("#judul").val();
        var ket = CKEDITOR.instances.editor1.getData();
        var idkategori = $("#idkategori").val();
        if(jdl==""){
             jQuery("#judul").effect('shake', '1500').attr('placeholder', 'Masukan judul diskusi');
        } else if(ket=="") {
            jQuery("#editor1").effect('shake', '1500').attr('placeholder', 'Masukan judul diskusi');
        } else {
            $.ajax({
                url : url + "forum/simpanbaru",
                method : "POST",
                data:{judul:jdl, isi:ket, id:idkategori},
                cache: false,
                success:function(data)
                {
                    resp = $.parseJSON(data);
                    if(resp=="ok"){
                      alert("Posting berhasil disimpan");
                      window.location.href = url + "forum/kategori/" + idkategori; 
                    } else {
                      alert("Posting gagal disimpan");
                      location.reload();
                    }
                }
            });
        } 
    }
</script>

                    