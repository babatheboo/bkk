<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/js/ticket-view-kmkr.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<style>
@media (min-width: 1000px)/* The minimum width of the display area, such as a browser window*/
{
  .modal-dialog {
      width: 1200px;
  }
}
</style>
<style media="all" type="text/css">
  .alignCenter { text-align: center; }  
</style>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> 
			<div class="panel-heading-btn"> 
				<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload Data</button>
				</div> <h4 class="panel-title"><?php echo $halaman;?></h4>
                         </div> 
                         <div class="panel-body"> <div class="table-responsive"> 
	                         <table id="data-ticket" class="table table-striped table-bordered nowrap" width="100%"> 
	                         	<thead> 
	                         		<tr> 
	                         			<th style="text-align:center" width="1%">No.</th> 
	                         			<th style="text-align:center" width="20%">Id Ticket</th> 
	                         			<th style="text-align:center" width="20%">Tujuan Pengajuan</th> 
	                         			<th style="text-align:center" width="30%">Judul Pengajuan</th>
	                         			<th style="text-align:center" width="10%">Tanggal</th>
	                         			<th style="text-align:center" width="30%">Status</th> 
	                         			<th style="text-align:center" width="30%">Detail</th> 
	                         		</tr> 
	                         	</thead> 
	                         		<tbody> 
	                         		</tbody> 
	                         </table>
                          </div> 
                       </div>
                    </div> 
                </div>
</div>

