<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Data_ticket_kmkr extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("5");
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('ticket_model');
  		$this->load->library('bcrypt');
  		$this->load->helper('form');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$lvl = $this->session->userdata('level');
		$isi['kelas'] = "ticket_kmkr";
		$isi['namamenu'] = "Ticket KEMENAKER";
		$isi['page'] = "data_ticket_kmkr";
		$isi['link'] = 'data_ticket_kmkr';
		$isi['halaman'] = "Ticket Pengajuan";
		$isi['judul'] = "Halaman Pengajuan Ticket";
		$isi['content'] = "ticket_view_kmkr";
		$this->load->view("dashboard/dashboard_view",$isi);
	}

	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->ticket_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = '<center>' . $no . "." . '</center>';
				$row[] = $rowx->id_ticket;
				$tujuan = $rowx->lvl_tujuan;
				if ($tujuan==9) {
					$row[] = '<span class="btn btn-primary btn-xs m-r-5">Technical Support</span>';
				}elseif ($tujuan==4) {
					$row[] = '<span class="btn btn-primary btn-xs m-r-5">PSMK</span>';
				}elseif($tujuan==5){
					 $row[] = '<span class="btn btn-primary btn-xs m-r-5">KEMENAKER</span>';
				}	
				$row[] = $rowx->judul;
				$row[] = date('d-m-Y',strtotime($rowx->tanggal));
				$status = $rowx->status;
				if ($status==1) {
					$row[] = '<span class="btn btn-info btn-xs m-r-5">Open</span>';
				}elseif ($status==2) {
					$row[] = '<span class="btn btn-success btn-xs m-r-5">Process</span>';
				}elseif($status==3){
					 $row[] = '<span class="btn btn-btn-danger btn-xs m-r-5">Close</span>';
				}
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="'.base_url().'data_ticket_kmkr/detil/'.$rowx->id_ticket.'" title="detail ticket" ><i class="fa fa-mail-forward"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->ticket_model->count_all(),
				"recordsFiltered" => $this->ticket_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}	

	function detil($id){
		$lvl = $this->session->userdata('level');
		$user_id = $this->session->userdata('user_id');
		$id = $this->asn->anti($id);
		$cekdata = $this->db->get_where('ticket_detail', array('id_ticket' => $id),1)->result();
		if (!empty($cekdata)){
			$cklvl = $this->db->where('id_ticket', $id)->get('ticket')->row()->lvl_tujuan;
			if ($cklvl==5) {
				$this->session->set_userdata('ticket_sess_id', $id);
				$isi['title_ticket'] = $this->db->where('id_ticket', $id)->get('ticket')->row()->judul;
				$isi['status_ticket'] = $this->db->where('id_ticket', $id)->get('ticket')->row()->status;
				$isi['data_pesan'] = $this->db->get_where('ticket_detail', array('id_ticket' => $id))->result();
				$isi['action'] = "proses_add";
				$isi['action2'] = "close_ticket";
				$isi['kelas'] = "data_ticket_psmk";
				$isi['namamenu'] = "Data Ticket";
				$isi['page'] = "data_ticket_psmk";
				$isi['link'] = 'data_ticket_psmk';
				$isi['halaman'] = "Detail halaman";
				$isi['judul'] = "pengajuan ticket";
				$isi['content'] = "ticket_detail_view";
				$this->load->view("dashboard/dashboard_view",$isi);
			}else{
				redirect("_404","refresh");		
			}
		}else{
			redirect("_404","refresh");		
		}
	}

	public function proses_add(){
		$this->form_validation->set_rules('message', 'pesan harus diisi', 'htmlspecialchars|trim|required|min_length[1]|max_length[500]');
		if ($this->form_validation->run() == TRUE){
			$session_ticket = $this->session->userdata('ticket_sess_id');
			$message = $this->asn->anti($this->input->post('message'));
			$date = new datetime();
			$tglchat = $date->format('Y-m-d H:i:s');
				$simpanpesan = array('id_ticket'=>$session_ticket,
					'user_id'=>$this->session->userdata('user_id'),
					'desc'=>$message,
					'level'=>$this->session->userdata('level'),
					'tgl_pesan'=>$tglchat);
				$this->db->insert('ticket_detail',$simpanpesan);
			redirect('data_ticket_kmkr/detil/'.$session_ticket, 'location',301);
	    }else{
	        redirect('data_ticket_kmkr/detil/'.$session_ticket, 'location',301);;
	    }
	}

	public function close_ticket(){
		$this->form_validation->set_rules('status_ticket', 'Status Ticket Harus diisi', 'htmlspecialchars|trim|required|min_length[1]|max_length[1]');
		if ($this->form_validation->run() == TRUE){
			$session_ticket = $this->session->userdata('ticket_sess_id');
			$status = $this->asn->anti($this->input->post('status_ticket'));
				$close_ticket = array('status'=>$status);
				$this->db->where('id_ticket', $session_ticket);
				$this->db->update('ticket', $close_ticket); 
				redirect('data_ticket_kmkr/detil/'.$session_ticket, 'location',301);
	    }else{
	        redirect('data_ticket_kmkr/detil/'.$session_ticket, 'location',301);
	    }
	}
}	