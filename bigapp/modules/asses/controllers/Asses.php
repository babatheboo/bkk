<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Asses extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
 	}
	
	public function _hasil()
	{
		echo "<input type='hidden' id='fin' value='OK' />";
	}

	public function cek($nisn){
		//$kode = "";
		$dv = $this->db->query("SELECT * FROM ref.peserta_didik WHERE nisn='$nisn'")->result();
		foreach ($dv as $key) {
			$kode = $key->peserta_didik_id;
			$this->session->set_userdata("user_id",$kode);
		}

		$cektest = $this->db->get_where('app.mbti_result',array('user_id'=>$kode))->result();
	
		if(count($cektest)==0)
		{
			$this->load->view("asses/quiz_view");
		}
		
	}

	function proses(){

		$ext=0;
		$int=0;
		$sen=0;
		$tui=0;
		$thi=0;
		$fee=0;
		$jud=0;
		$per=0;
		for($a=1;$a<39;$a++){
			$h= $this->input->post('pil'.$a);
			$hasil = substr($h, -1);
			switch ($hasil) {
				case 'E':
					$ext++;
					break;
				case 'I':
					$int++;
					break;
				case 'S':
					$sen++;
					break;
				case 'N':
					$tui++;
					break;
				case 'T':
					$thi++;
					break;
				case 'F':
					$fee++;
					break;
				case 'J':
					$jud++;
					break;
				case 'P':
					$per++;
					break;
			}
		}
		$result="";
		if($ext>$int){
			$result .= "E";
		}else{
			$result .= "I";
		}
		if($sen>$tui){
			$result .= "S";
		}else{
			$result .= "N";
		}
		if($thi>$fee){
			$result .= "T";
		}else{
			$result .= "F";
		}
		if($jud>$per){
			$result .= "J";
		}else{
			$result .= "P";
		}
		$tgl = date("Y-m-d H:i:s");
		$nisn= $this->session->userdata('user_id');
		$data = array('user_id'=>$nisn,'assesment_date'=>$tgl,'assesment_result'=>$result);
		$this->db->insert('app.mbti_result',$data);
		echo "OK";
	}
}
