<!-- <h3>Pertanyaan yang sering diajukan</h3> -->
<!-- <div id="accordion" class="accordion">
	<div class="card text-dark">
		<div class="card-header bg-dark-darker pointer-cursor d-flex align-items-center" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
			<i class="fa fa-circle fa-fw text-blue mr-2 f-s-8"></i> <span style="color: #000;">Kenapa akun saya belum divalidasi?</span>
		</div>
		<div id="collapseOne" class="collapse" data-parent="#accordion" style="">
			<div class="card-body">
				Sistem terbaru kami mengharuskan anda melakukan validasi akun untuk mencatat BKK anda.<br>
				Validasi akun dilakukan apabila anda sudah berhasil mengisi dokumen berikut:
				<ol style="padding-left: 3%;">
					<li>Nomor registrasi BKK</li>
					<li>Surat Keputusan Pendirian BKK</li>
					<li>Scan dokumen izin BKK</li>
				</ol>
				Untuk melihat akun anda sudah aktif/belum anda klik menu <b><a href="<?php echo base_url();?>loker">Bursa Kerja</a></b>, jika anda diarahkan ke laman validasi maka anda harus mengisi dokumen-dokumen di atas. 
				Jika akun anda belum divalidasi anda tidak dapat mengakses menu <b><a href="<?php echo base_url();?>loker">Bursa Kerja</a></b> hingga validasi akun anda berhasil.
			</div>
		</div>
	</div>
	<div class="card text-black">
		<div class="card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed" data-toggle="collapse" data-target="#collapseFour">
			<i class="fa fa-circle fa-fw text-info mr-2 f-s-8"></i> Maaf saat ini anda belum mempunyai hak untuk membuat lowongan pekerjaan. Untuk bisa membuat lowongan pekerjaan pastikan anda sudah memiliki mitra industri.
		</div>
		<div id="collapseFour" class="collapse" data-parent="#accordion">
			<div class="card-body">
				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			</div>
		</div>
	</div>
	<div class="card bg-dark text-white">
		<div class="card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed" data-toggle="collapse" data-target="#collapseFive">
			<i class="fa fa-circle fa-fw text-warning mr-2 f-s-8"></i> Collapsible Group Item #5
		</div>
		<div id="collapseFive" class="collapse" data-parent="#accordion">
			<div class="card-body">
				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			</div>
		</div>
	</div>
	<div class="card bg-dark text-white">
		<div class="card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed" data-toggle="collapse" data-target="#collapseSix">
			<i class="fa fa-circle fa-fw text-danger mr-2 f-s-8"></i> Collapsible Group Item #6
		</div>
		<div id="collapseSix" class="collapse" data-parent="#accordion">
			<div class="card-body">
				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			</div>
		</div>
	</div>
	<div class="card bg-dark text-white">
		<div class="card-header bg-dark-darker pointer-cursor d-flex align-items-center collapsed" data-toggle="collapse" data-target="#collapseSeven">
			<i class="fa fa-circle fa-fw text-muted mr-2 f-s-8"></i> Collapsible Group Item #7
		</div>
		<div id="collapseSeven" class="collapse" data-parent="#accordion">
			<div class="card-body">
				Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			</div>
		</div>
	</div>
</div> -->
<!-- begin panel -->
<div class="panel panel-inverse">
	<!-- <div class="panel-heading">
				<h4 class="panel-title">Panel Title here</h4>
				<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
	</div> -->
	<div class="panel-body">
		<h3>Hubungi Kami <small>Kami akan menjawab pertanyaan anda melalui email yang anda tulis di bawah.</small></h3><br>
		<form action="/" method="POST">
			<fieldset>
				<div class="col-md-12">
					<div class="row row-space-10">
						<div class="col-md-4">
							<label>Nama</label>
							<div class="input-group">
								<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
								<input id="skl" name="skl" value="<?php echo $this->session->userdata('nama_user');?>" type="text" readonly="" class="form-control">
							</div>
						</div>
						<?php
						$sid = $this->session->userdata('role_');
						$em = $this->db->query("SELECT email FROM app.user_sekolah WHERE sekolah_id='$sid'")->row();
						$dn = $this->db->query("SELECT email FROM app.user_dinas WHERE user_id='$sid'")->row();
						if ($this->session->userdata('level')=='1') {
							echo '<div class="col-md-4">
									<label>Email</label>
									<div class="input-group">
											<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>
											<input id="email" name="email" value="'.$em->email.'" type="email" readonly="" class="form-control">
									</div>
									<span class="pull-right"><a style="text-decoration: none;" href="javascript:cEmail();">Ubah email</a></span>
							</div>';
						}else{
							echo '<div class="col-md-4">
									<label>Email</label>
									<div class="input-group">
											<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>
											<input id="email" name="email" value="'.$dn->email.'" type="email" readonly="" class="form-control">
									</div>
									<span class="pull-right"><a style="text-decoration: none;" href="javascript:cEmail();">Ubah email</a></span>
							</div>';
						}
						?>
						<div class="col-md-4">
							<div class="input-group">
							</div>
						</div>
						<div class="col-md-5" style="height: 95px;">
							<div class="input-group">
								<!-- <label>Email</label> -->
								<div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-pencil-alt"></i></span></div>
								<textarea id="ask" name="ask" style="color: #9296a7;" class="form-control" rows="3" placeholder="Masukan Pertanyaan..."></textarea>
							</div>
						</div>
						<div class="col-md-4">
							<span style="display:inline-block;padding-left:15px">
								<i class="fas fa-map-pin"></i> <p>Komplek Kementerian Pendidikan dan <br> Kebudayaan,
									Gedung E, Lantai 13<br>
									Jl. Jend Sudirman, Senayan, Jakarta 10270<br>
									<i class="fas fa-phone"></i> 021-5725477 (hunting)<br><i class="fab fa-lg fa-fw fa-whatsapp"></i><a style="text-decoration: none;" href="https://wa.me/+628119252424/?text=5A" target="_blank">08119252424</a> / <a style="text-decoration: none;" href="https://wa.me/+6285721549004" target="_blank">085721549004</a></p>
								</span>
							</div>
							<!-- <div class="col-md-3"> -->
							<div class="mapouter" id="map"><div class="gmap_canvas"><iframe width="300" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=kementerian%20pendidikan%20dan%20kebudayaan%20lobby%20gedung%20e&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:250px;width:300px;margin-left: -80px; margin-top: -50px;}.gmap_canvas {overflow:hidden;background:none!important;height:250px;width:300px;}</style></div>
							<!-- </div> -->
						</div>
						<button onclick="okSend()" style="margin-top: -185px; position: relative;" class="btn btn-primary" type="button">Kirim</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
	<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.js"></script>
	<script type="text/javascript">
		function cEmail(){
			document.getElementById('email').removeAttribute('readonly');
		}
		function okSend(){
			var email = $("#email").val();
			var skl = $("#skl").val();
			var tanya = $("#ask").val();
			if (email == '') {
				jQuery("#email").effect('shake', '1500');
			}else if (tanya == '') {
				jQuery("#ask").effect('shake', '1500');
			}else{
				jQuery.blockUI({
					css: {
						border: 'none',
						padding: '15px',
						backgroundColor: '#000',
						'-webkit-border-radius': '10px',
						'-moz-border-radius': '10px',
						opacity: 2,
						color: '#fff'
					},
					message : 'Mengirim pertanyaan ... '
				});
				$.ajax({
	url:"<?php echo base_url();?>help/kirim",
	method:"POST",
	data:{email:email, skl:skl, tanya:tanya},
	success:function(data){
	var resp = $.parseJSON(data);
	if (resp == "true") {
	jQuery.unblockUI();
	$.notify({icon:'fa fa-info',message:"Pertanyaan berhasil dikirim. Kami akan segera membalas ke email yang anda cantumkan."},{type:"success",offset:{x:3,y:2}});
	}else if (resp == "false") {
	jQuery.unblockUI();
	$.notify({icon:'fa fa-info',message:"Gagal mengirim pertanyaan"},{type:"danger",offset:{x:3,y:2}});
	}else{
	jQuery.unblockUI();
	$.notify({icon:'fa fa-info',message:"Terjadi kesalahan"},{type:"warning",offset:{x:3,y:2}});
	}
	}, error:function(data){
	jQuery.unblockUI();
	$.notify({icon:'fa fa-info',message:"Terjadi kesalahan"},{type:"warning",offset:{x:3,y:2}});
	}
	});
	}
	}
	</script>