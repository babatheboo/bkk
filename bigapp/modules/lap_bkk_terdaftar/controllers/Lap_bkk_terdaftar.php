<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lap_bkk_terdaftar extends CI_Controller {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("4");
  		$this->db1 = $this->load->database('default', TRUE);
  		$this->load->model('bkk_terdaftar_model');
      $this->load->model('bkk_terdaftar_kokab');
      $this->load->model('bkk_terdaftar_prov');
      $this->load->library('bcrypt');
      $this->load->helper('form');
    }
    public function index(){
      $this->_content();
    }
    public function _content(){
      $lvl = $this->session->userdata('level');
      $isi['kelas'] = "mitra";
      $isi['namamenu'] = "Data BKK Terdaftar";
      $isi['page'] = "lap_bkk_terdaftar";
      $isi['option_bidangx'][''] = "Pilih Wilayah";
      $ckwilayah = $this->db->get_where('ref.view_wilayah')->result();
      if(count($ckwilayah)>0){
       foreach ($ckwilayah as $row) {
        $isi['option_wilayahx'][trim($row->prov)] = $row->prov;
      }
    }else{
     $isi['option_wilayahx'][''] = "Wilayah Tidak Tersedia";
   }
   $isi['link'] = 'lap_bkk_terdaftar';
   $isi['halaman'] = "Data BKK Terdaftar";
   $isi['judul'] = "Halaman Data BKK Terdaftar";
   $isi['content'] = "lap_bkk_terdaftar_view";
   $this->load->view("dashboard/dashboard_view",$isi);
 }
 public function getData(){
  if($this->input->is_ajax_request()){
   $list = $this->bkk_terdaftar_model->get_datatables();
   $data = array();
   $no = $_POST['start'];
   foreach ($list as $rowx) {
    $no++;
    $row = array();
    $row[] = '<center>' . $no . "." . '</center>';
    $row[] = $rowx->npsn;
    $row[] = $rowx->nama;
    $row[] = $rowx->no_izin_bkk;
    $row[] = $rowx->no_sk_pendirian;
    if ($rowx->scan != '') {
     $row[] = "<center><a href='".base_url()."assets/foto/scan/$rowx->scan' target='_blank'><img src='".base_url()."assets/foto/scan/$rowx->scan' width='15px'></img></a></center>";
   }else{
     $row[] = "<center><a href='".base_url()."assets/foto/scan/blm.jpg' target='_blank'><img src='".base_url()."assets/foto/scan/blm.jpg' width='15px'></img></a></center>";
   }
   $data[] = $row;
 }
 $output = array(
  "draw" => $_POST['draw'],
  "recordsTotal" => $this->bkk_terdaftar_model->count_all(),
  "recordsFiltered" => $this->bkk_terdaftar_model->count_filtered(),
  "data" => $data,
);
 echo json_encode($output);
}else{
 redirect("_404","refresh");
}		
}

public function getProv(){
  if($this->input->is_ajax_request()){
    $list = $this->bkk_terdaftar_kokab->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = '<center>' . $no . "." . '</center>';
      $row[] = $rowx->prov;
      $row[] = $rowx->kota;
      $row[] = $rowx->jumlah;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bkk_terdaftar_kokab->count_all(),
      "recordsFiltered" => $this->bkk_terdaftar_kokab->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }   
}

public function getProv2(){
  if($this->input->is_ajax_request()){
    $list = $this->bkk_terdaftar_prov->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = '<center>' . $no . "." . '</center>';
      $row[] = $rowx->prov;
      $row[] = $rowx->jml;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bkk_terdaftar_prov->count_all(),
      "recordsFiltered" => $this->bkk_terdaftar_prov->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }   
}

function expValid(){
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data BKK Terdaftar")
  ->setSubject("BKK Terdaftar")
  ->setDescription("Laporan BKK Terdaftar")
  ->setKeywords("Data BKK Terdaftar");
  $style_col = array('font' => array('bold' => true),
   'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA BKK TERDAFTAR");
  $excel->getActiveSheet()->mergeCells('A1:G1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "NPSN");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "SMK");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Provinsi");
  $excel->setActiveSheetIndex(0)->setCellValue('E3', "Kab/Kota");
  $excel->setActiveSheetIndex(0)->setCellValue('F3', "No Izin");
  $excel->setActiveSheetIndex(0)->setCellValue('G3', "No SK");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
  $bkk = $this->db->query("SELECT * FROM view_bkk_terdaftar ORDER BY prov ASC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
   $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
   $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->npsn);
   $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama);
   $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->prov);
   $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->kota);
   $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->no_izin_bkk);
   $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->no_sk_pendirian);
   $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
   $no++;
   $numrow++;
 }
 $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('B')->setWidth(9);
 $excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
 $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
 $excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
 $excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
 $excel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
 $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 $excel->getActiveSheet(0)->setTitle("Laporan BKK Terdaftar");
 $excel->setActiveSheetIndex(0);
 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment; filename="Laporan BKK Terdaftar"');
 header('Cache-Control: max-age=0');
 header('Cache-Control: max-age=1');
 header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
 header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
 header ('Cache-Control: cache, must-revalidate');
 header ('Pragma: public');
 $writer = PHPExcel_IOFactory::createWriter($excel, 'HTML');
 $writer->save('php://output');
 exit;
}

function expValidProv(){
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data Sebaran BKK Terdaftar")
  ->setSubject("BKK Sebaran Terdaftar")
  ->setDescription("Laporan Sebaran BKK Terdaftar")
  ->setKeywords("Data Sebaran BKK Terdaftar");
  $style_col = array('font' => array('bold' => true),
   'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SEBARAN BKK TERDAFTAR Per-PROVINSI");
  $excel->getActiveSheet()->mergeCells('A1:C1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "Provinsi");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Total");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $bkk = $this->db->query("SELECT * FROM mv_tot_prov2 ORDER BY jml DESC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
   $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
   $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->prov);
   $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->jml);
   $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
   $no++;
   $numrow++;
 }
 $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
 $excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
 $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 $excel->getActiveSheet(0)->setTitle("Laporan Sebaran BKK Terdaftar");
 $excel->setActiveSheetIndex(0);
 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment; filename="Laporan Sebaran BKK Terdaftar"');
 header('Cache-Control: max-age=0');
 header('Cache-Control: max-age=1');
 header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
 header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
 header ('Cache-Control: cache, must-revalidate');
 header ('Pragma: public');
 $writer = PHPExcel_IOFactory::createWriter($excel, 'HTML');
 $writer->save('php://output');
 exit;
}

function expValidKokab(){
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data Sebaran BKK Terdaftar")
  ->setSubject("BKK Sebaran Terdaftar")
  ->setDescription("Laporan Sebaran BKK Terdaftar")
  ->setKeywords("Data Sebaran BKK Terdaftar");
  $style_col = array('font' => array('bold' => true),
   'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
   'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SEBARAN BKK TERDAFTAR Per-KOTA/KABUPATEN");
  $excel->getActiveSheet()->mergeCells('A1:D1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "Provinsi");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Kota/Kab");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Total");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $bkk = $this->db->query("SELECT * FROM mv_tot_prov ORDER BY prov ASC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
   $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
   $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->prov);
   $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->kota);
   $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->jumlah);
   $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
   $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
   $no++;
   $numrow++;
 }
 $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
 $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
 $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
 $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
 $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
 $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
 $excel->getActiveSheet(0)->setTitle("Laporan Sebaran BKK Terdaftar");
 $excel->setActiveSheetIndex(0);
 header('Content-Type: application/vnd.ms-excel');
 header('Content-Disposition: attachment; filename="Laporan Sebaran BKK Terdaftar"');
 header('Cache-Control: max-age=0');
 header('Cache-Control: max-age=1');
 header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
 header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
 header ('Cache-Control: cache, must-revalidate');
 header ('Pragma: public');
 $writer = PHPExcel_IOFactory::createWriter($excel, 'HTML');
 $writer->save('php://output');
 exit;
}
}
