<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/bkk_terdaftar.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row">
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <a href="<?php echo base_url();?>lap_bkk_terdaftar/expValidProv" class="btn btn-success btn-xs m-r-5">
                        <i class="fas fa-file-excel"></i> Download
                    </a>
                    <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                        <i class="fas fa-sync-alt"></i> Reload Data
                    </button>
                </div> 
                <h4 class="panel-title">Sebaran BKK Terdaftar per-Provinsi</h4>
            </div>
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="data-bkk-p2" class="table table-striped table-bordered nowrap" width="100%"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="30%">Provinsi</th>
                                <th style="text-align:center" width="1%">Total</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>

    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <a href="<?php echo base_url();?>lap_bkk_terdaftar/expValidKokab" class="btn btn-success btn-xs m-r-5">
                        <i class="fas fa-file-excel"></i> Download
                    </a>
                    <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                        <i class="fas fa-sync-alt"></i> Reload Data
                    </button>
                </div> 
                <h4 class="panel-title">Sebaran BKK Terdaftar Per-Kabupaten/Kota</h4>
            </div>
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="data-bkk-p" class="table table-striped table-bordered nowrap" width="100%"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="30%">Provinsi</th>
                                <th style="text-align:center" width="30%">Kota/Kab</th>
                                <th style="text-align:center" width="1%">Total</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>

    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <a href="<?php echo base_url();?>lap_bkk_terdaftar/expValid" class="btn btn-success btn-xs m-r-5">
                        <i class="fas fa-file-excel"></i> Download
                    </a>
                    <button class="btn btn-primary btn-xs m-r-5" onclick="filter_data()"><i class="fa fa-search"></i> Filter Pencarian</button>
                    <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                        <i class="fas fa-sync-alt"></i> Reload Data
                    </button>
                </div> 
                <h4 class="panel-title">BKK yang sudah memiliki izin/sk pendirian</h4> 
                <div id="filter_pencarian">
                    <br/>
                    <?php echo form_dropdown('wilayahx',$option_wilayahx,isset($default['wilayahx']) ? $default['wilayahx'] : '','class="default-select2 form-control" style="width:25%" id="wilayahx" name="wilayahx" data-live-search="true" data-style="btn-white"');?>
                </div>
            </div>
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="data-bkk-d" class="table table-striped table-bordered nowrap" width="100%"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="1%">NPSN</th>
                                <th style="text-align:center" width="30%">Nama Sekolah</th>
                                <th style="text-align:center" width="10%">No Izin BKK</th>
                                <th style="text-align:center" width="10%">No SK Pendirian</th>
                                <th style="text-align:center" width="10%">Scan</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>
</div>

<div id="modal_img" hidden="" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="myImg"></div>
        </div>
    </div>
</div>
