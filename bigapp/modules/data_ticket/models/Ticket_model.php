<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ticket_model extends CI_Model {
    var $table = 'view_ticket';
    var $column_order = array(null,'id_bkk','lvl_tujuan','judul',"tanggal","nama_sekolah","status",null);
    var $column_search = array('id_ticket','lvl_tujuan','judul',"tanggal","nama_sekolah","status"); 
    var $order = array('tanggal' => 'desc');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    private function _get_datatables_query(){   
        // if($this->input->post('bidang')){
        //     $this->db->where("bidang_usaha_id",$this->input->post('bidang'));
        // }
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        $this->db->where('sekolah_id',$sess_sekolah);
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        $this->db->where('sekolah_id',$sess_sekolah);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $this->db->where('sekolah_id',$sess_sekolah);
        $query = $this->db->get();
        return $query->row();
    }

    public function save($data,$data2)
    {
        $this->db->trans_start();    
        $this->db->insert('ticket', $data);
        $this->db->insert('ticket_detail', $data2);
        $this->db->trans_complete();
    }
    
    // public function cari_industri($keyword){
    //     $this->db->select('ref.industri.nama as nama_industri,ref.industri.industri_id,ref.industri.alamat,
    //         ref.industri.no_tlp,
    //         ref.industri.kode_prov,
    //         ref.industri.bidang_usaha_id,
    //         ref.mst_wilayah.nama as nama_provinsi,
    //         ref.industri.email')->from('ref.industri');
    //     $this->db->like('ref.industri.nama',$keyword,'after');
    //     $this->db->join('ref.mst_wilayah','ref.industri.kode_prov=ref.mst_wilayah.kode_wilayah');
    //     // $this->db->join('mitra','ref.industri.industri_id=mitra.industri_id');
    //     $query = $this->db->get();    
    //     return $query->result();
    // }

}