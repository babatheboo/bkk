<script src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
  selector: 'textarea',
  height: 100,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code responsivefilemanager'
  ],
  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager',

    external_filemanager_path:"<?php echo base_url();?>filemanager/",
   filemanager_title:"Image Gallery" ,
   external_plugins: { "filemanager" : "<?php echo base_url();?>filemanager/plugin.min.js"}

});
var imageFilePicker = function (callback, value, meta) {               
    tinymce.activeEditor.windowManager.open({
        title: 'File and Image Picker',
        url: '/myapp/getfilesandimages',
        width: 700,
        height: 600,
        buttons: [{
            text: 'Insert',
            onclick: function () {
                //do some work to select an item and insert it into TinyMCE
                tinymce.activeEditor.windowManager.close();
            }
        }, 
        {
            text: 'Close',
            onclick: 'close'
        }],
    }, 
        {
            oninsert: function (url) {
            callback(url); 
        }
    });
};
</script>
<div class="row">
    <!-- begin col-4 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-default" data-sortable-id="index-2">
            <div class="panel-heading">
                <h4 class="panel-title">Judul Ticket : <?php echo $title_ticket; ?> <span class="label label-success pull-right"></span></h4>
            </div>
            <div class="panel-body bg-silver">
                <div class="col-md-6">
                    <form class="form-inline" action="<?php echo base_url();?>/data_ticket/<?php echo $action2;?>" method="post" >
                        <div class="checkbox m-r-10">
                            <label>
                                 Status Ticket
                            </label>
                        </div>
                        <?php   if($status_ticket==1) {
                                    ?>
                                    <div class="form-group m-r-10">
                                            <select name="status_ticket" id="status_ticket" class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white">
                                                <option value="1" selected>Status Open</option>
                                                <option value="3">Closed</option>
                                            </select>
                                        </div>
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Submit</button>
                                    <?php 
                                }elseif($status_ticket==2){
                                    ?>
                                    <div class="form-group m-r-10">
                                            <select name="status_ticket" id="status_ticket" class="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white">
                                                <option value="2" selected>Status Process</option>
                                                <option value="3">Closed</option>
                                            </select>
                                        </div>
                                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Submit</button>
                                    <?php
                                }else{
                                    ?>
                                     <span class="label label-danger">Closed</span>
                                    <?php 
                                }
                        ?>
                    </form>
                </div>
                <div class="col-md-6">
                    <?php  if ($status_ticket== 1) { 
                            ?>
                            <div class="alert alert-danger fade in m-b-15">
                                <strong>Info !</strong>
                                Status Ticket Open
                            </div>
                            <?php 
                    }else if($status_ticket== 2){
                        ?>
                            <div class="alert alert-info fade in m-b-15">
                                <strong>Info !</strong>
                                Status Ticket Process
                            </div>
                        <?php 
                    }else if($status_ticket== 3){
                        ?>
                            <div class="alert alert-success fade in m-b-15">
                                <strong>Info !</strong>
                                Status Ticket Selesai
                            </div>
                        <?php 
                    }
                ?>        
                </div>
                <div class="col-md-12">
                    <div data-scrollbar="true" data-height="600px">
                        <ul class="chats">
                            <?php 
                                if (!empty($data_pesan)) {
                                    $i = 0;
                                    foreach ($data_pesan as $dtpes ) {
                                         $level  = $dtpes->level;
                                        ?>
                                            <?php if($level==1) { ?> 
                                                <li class="left"> <?php }else{ ?> <li class="right"> <?php } ?>
                                                    <span class="date-time"><?php echo date('d-m-Y H:i:s',strtotime($dtpes->tgl_pesan));?></span>
                                                    <?php 
                                                        $userid = $dtpes->user_id;           
                                                        if($level==1) {
                                                           $nama = $this->db->get_where('app.user_sekolah', array('app.user_sekolah.user_id' => $userid), 1)->row()->nama;
                                                            ?>
                                                                <a href="javascript:;" class="name"><?php echo $nama; ?></a>
                                                                <a href="javascript:;" class="image"><img src="http://bkk.com/assets/foto/administrator.png"  alt=""/></a>
                                                                <div class="message">
                                                                    <?php  echo html_entity_decode($dtpes->desc); ?>
                                                                </div>
                                                            <?php 
                                                        }elseif ($level==4) {
                                                            $nama = $this->db->get_where('app.user_psmk', array('app.user_psmk.user_id' => $userid), 1)->row()->nama;
                                                            ?>
                                                                <a href="javascript:;" class="name"><?php echo $nama; ?></a>
                                                                <a href="javascript:;" class="image"><img src="http://bkk.com/assets/foto/administrator.png"  alt=""/></a>
                                                                <div class="message" style="text-align:right;">
                                                                    <?php  echo html_entity_decode($dtpes->desc); ?>
                                                                </div>
                                                            <?php 
                                                        }elseif ($level==5) {
                                                            $nama = $this->db->get_where('app.user_kemenaker', array('app.user_kemenaker.user_id' => $userid), 1)->row()->nama;
                                                              ?>
                                                                <a href="javascript:;" class="name"><?php echo $nama; ?></a>
                                                                <a href="javascript:;" class="image"><img src="http://bkk.com/assets/foto/administrator.png"  alt=""/></a>
                                                                <div class="message" style="text-align:right;">
                                                                    <?php  echo html_entity_decode($dtpes->desc); ?>
                                                                </div>
                                                            <?php                                                 
                                                        }     
                                                    ?>
                                                </li>
                                        <?php            
                                    }
                                }
                            ?>    
                        </ul>
                    </div>
                </div>    
            </div>
            <div class="panel-footer">
            <?php  if ($status_ticket== 1 || $status_ticket==2) {
                    ?>
                        <form name="send_message_form" data-id="message-form" action="<?php echo base_url();?>/data_ticket/<?php echo $action;?>" method="post">
                            <div class="input-group">
                                <textarea name="message" placeholder="masukan pesan anda disini" class="form-control input-sm"> </textarea>
                            </div>
                            <br />
                            <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="submit">Send</button>
                            </span>
                        </form> <?php 
                    }else{
                        ?>
                            <h4 class="panel-title" style="text-align:center;"> Ticket ini telah selesai, terima kasih :) </h4>
                        <?php 
                    }
                    ?>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-4 -->
</div>    