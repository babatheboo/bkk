<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller {

  public function __construct(){
      parent::__construct();
     $this->load->model('profile_model','ps_model');
  }
  
  public function index(){
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $isi['content'] = "profile_view";
      $isi['menu'] = "profile";
      $isi['mn'] = 'profile';
      $isi['dtsiswa'] = $this->ps_model->get_data_siswa();   
      $this->load->view('frontpage/front_view', $isi);
    } else {
      redirect('frontpage','refresh');
    }
  }

  public function rubahStatus(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $id = $this->input->post('id');
      $st = $this->input->post('status');
      $resp = "";
        if($this->db->query("UPDATE kerja_siswa set status='$st', akhir_kerja=null WHERE id='$id'")){
            $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
        } else {
          redirect('frontpage','refresh');
      }
  }

  public function cekKerja($id){
    if($this->input->is_ajax_request()){
      $ckdata = $this->db->get_where('kerja_siswa',array('id'=>$id))->result();
      $data['id']='';
      $data['posisi']='';
      $data['mulai']='';
      $data['akhir']='';
      $data['status']='';
      $data['range']='';
      foreach ($ckdata as $key) {
        $data['id']=$id;
        $data['posisi']=$key->posisi;
        $data['mulai']=$key->mulai_kerja;
        $data['akhir']=$key->akhir_kerja;
        $data['status']=$key->status;
        $data['range']=$key->range_gaji;
      }
      echo json_encode($data);
    }else{
      redirect("_404","refresh");
    }
  }

  public function cekWira($id){
    if($this->input->is_ajax_request()){
      $ckdata = $this->db->get_where('wira_siswa',array('id'=>$id))->result();
      $data['idW']='';
      $data['posisiW']='';
      $data['mulaiW']='';
      $data['akhirW']='';
      $data['statusW']='';
      $data['rangeW']='';
      foreach ($ckdata as $key) {
        $data['idW']=$id;
        $data['posisiW']=$key->jenis_usaha;
        $data['mulaiW']=$key->mulai_usaha;
        $data['akhirW']=$key->akhir_usaha;
        $data['statusW']=$key->status;
        $data['rangeW']=$key->pendapatan;
      }
      echo json_encode($data);
    }else{
      redirect("_404","refresh");
    }
  }

  public function editKerja($id){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $posisi = $this->input->post('kePosisi');
      $mulai = $this->input->post('keMulai');
      $akhir = $this->input->post('keAkhir');
      $status = $this->input->post('keStatus');
      $range = $this->input->post('keRange');
      $resp = "";
        if($this->db->query("UPDATE kerja_siswa set posisi='$posisi', mulai_kerja='$mulai', status='$status', akhir_kerja='$akhir', range_gaji='$range' WHERE id='$id'")){
            $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
        } else {
          redirect('frontpage','refresh');
      }
  }

  public function editWira($id){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      // $id = $this->input->post('id');
      $posisi = $this->input->post('wiPosisi');
      $mulai = $this->input->post('wiMulai');
      $akhir = $this->input->post('wiAkhir');
      $status = $this->input->post('wiStatus');
      $range = $this->input->post('wiRange');
      $resp = "";
        if($this->db->query("UPDATE wira_siswa set jenis_usaha='$posisi', mulai_usaha='$mulai', status='$status', akhir_usaha='$akhir', pendapatan='$range' WHERE id='$id'")){
            $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
        } else {
          redirect('frontpage','refresh');
      }
  }


   public function rubahStatusW(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $id = $this->input->post('id');
      $resp = "";
      $st = $this->input->post('status');
        if($this->db->query("UPDATE wira_siswa set status='$st', akhir_usaha=null WHERE id='$id'")){
           $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
        } else {
        redirect('frontpage','refresh');
      }
  }

 public function rubahStatusK(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $id = $this->input->post('id');
      $st = $this->input->post('status');
      $resp = "";
        if($this->db->query("UPDATE kuliah_siswa set status='$st', akhir_kuliah=null WHERE id='$id'")){
           $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
        } else {
        redirect('frontpage','refresh');
      }
  }

  public function rbhStatus(){
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $id = $this->input->post('kdjob');
      $resp = "";
      $st = $this->input->post('stjob');
      $tgl = date('Y-m-d', strtotime($this->input->post('akhir')));
      if($this->db->query("UPDATE kerja_siswa set status='$st', akhir_kerja='$tgl' WHERE id='$id'")){
         $resp = "ok";
            }   else {
              $resp = "not";
            }
            echo json_encode($resp);
    } else {
      redirect('frontpage','refresh');
    }
  }

  public function rbhStatusW(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
    $id = $this->input->post('kdwir');
    $st = $this->input->post('stjob');
    $resp = "";
    $tgl = date('Y-m-d', strtotime($this->input->post('akhir')));
    if($this->db->query("UPDATE wira_siswa set status='$st', akhir_usaha='$tgl' WHERE id='$id'")){
       $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);

  } else {
    redirect('frontpage','refresh');
  }
  }

  public function rbhStatusK(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
    $id = $this->input->post('kdkul');
    $st = $this->input->post('stjob');
    $resp = "";
    $tgl = date('Y-m-d', strtotime($this->input->post('akhir')));
    if($this->db->query("UPDATE kuliah_siswa set status='$st', akhir_kuliah='$tgl' WHERE id='$id'")){
       $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);

  } else {
    redirect('frontpage','refresh');
  }
  }



  public function addKerja(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $perusahaan = $this->input->post('perusahaan');
      $pos = $this->input->post('posisi');
      $range = $this->input->post('range');
      $mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
      $nisn = $this->session->userdata('username');
      $pd = $this->session->userdata('user_id');
      $resp = "";
      $data = array('nisn'=>$nisn,'perusahaan'=>$perusahaan,'posisi'=>$pos,'mulai_kerja'=>$mulai,'status'=>'1','peserta_didik_id'=>$pd,'ver_status'=>'0','range_gaji'=>$range);
      if($this->db->insert('kerja_siswa',$data)){
         $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);

    } else {
      redirect('frontpage','refresh');
    }
  }


   public function addKul(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
        $perusahaan = $this->input->post('perusahaan');
        $pos = $this->input->post('posisi');
        $mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
        $nisn = $this->session->userdata('username');
        $pd = $this->session->userdata('user_id');
        $resp = "";
        $data = array('nisn'=>$nisn,'nama_pt'=>$perusahaan,'jurusan'=>$pos,'mulai_kuliah'=>$mulai,'status'=>'1','peserta_didik_id'=>$pd,'ver_status'=>'0');
        if($this->db->insert('kuliah_siswa',$data)){
           $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
 
    } else {
    redirect('frontpage','refresh');
    }
  }


  public function addWira(){
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $perusahaan = $this->input->post('perusahaan');
      $pos = $this->input->post('jenis');
      $penghasilan = $this->input->post('penghasilan');
      $mulai = date('Y-m-d', strtotime($this->input->post('mulai')));
      $nisn = $this->session->userdata('username');
      $pd = $this->session->userdata('user_id');
      $resp = "";
      $data = array('nisn'=>$nisn,'perusahaan'=>$perusahaan,'jenis_usaha'=>$pos,'mulai_usaha'=>$mulai,'status'=>'1','peserta_didik_id'=>$pd,'ver_status'=>'0', 'pendapatan'=>$penghasilan);
      if($this->db->insert('wira_siswa',$data)){
         $resp = "ok";
          }   else {
            $resp = "not";
          }
          echo json_encode($resp);
  
  } else {
    redirect('frontpage','refresh');
  }
  }


  public function updateFoto(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $acakx=rand(00,99);
      $bersihx = $_FILES['foto']['name'];
      $nmx = str_replace(" ","_","$bersihx");
      $pisahx = explode(".",$nmx);
      $nama_murnix = $pisahx[0];
      $ubahx = $acakx.$nama_murnix;
      $nama_flx = $acakx.$nmx;
      $tmpNamex = str_replace(" ", "_", $_FILES['foto']['name']);
      $nmxfilex = "file_".time();
        if($tmpNamex!=''){
            $config['file_name'] = $nmxfilex;
            $config['upload_path'] = './assets/foto/siswa';
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['max_size'] = '1024';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('foto')){
                      $gbr = $this->upload->data(); 
                      $simpandata = array('photo'=>$gbr['file_name']);
                      $this->db->where('peserta_didik_id',$this->session->userdata('user_id'));
                      $this->db->update('ref.peserta_didik',$simpandata);
                      redirect('profile','refresh');
            }else{
              ?>
              <script type="text/javascript">
                        alert("Pastikan photo yang anda upload bertype jpg dan ukuran file maksimal 1mb");
                        window.location.href="<?php echo base_url();?>profile";
              </script>
              <?php
            }
      }
      } else {
    redirect('frontpage','refresh');
    }
  }

  public function lamaran(){
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
        $isi['content'] = "lamaran_view";
        $isi['menu'] = "profile";
        $isi['mn'] = 'lamaran';
        $isi['dtsiswa'] = $this->ps_model->get_data_siswa();   
        $this->load->view('frontpage/front_view', $isi);
    } else {
        redirect('frontpage','refresh');
    }
  }


  public function notifikasi(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
        $isi['content'] = "inbox_view";
        $sid = $this->session->userdata('user_id');
        $isi['d'] =  $this->db->query("SELECT * FROM bidding_lulusan WHERE id_siswa='$sid'")->result();
        $isi['disi'] =  $this->db->query("SELECT * FROM inbox WHERE peserta_didik_id='$sid' order by read")->result();
        $isi['menu'] = "profile";
        $isi['mn'] = 'notif';
        $isi['dtsiswa'] = $this->ps_model->get_data_siswa();   
        $this->load->view('frontpage/front_view', $isi);
    } else {
        redirect('frontpage','refresh');
    }
  }

  public function read($idj, $idin){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
        $sid = $this->session->userdata('user_id');
        $dup = array('read'=>'1');
        $this->db->where('id',$idin);
        $this->db->update('inbox', $dup);
        $isi['content'] = "read_view";
        $isi['disi'] =  $this->db->query("SELECT id, alamat, tgl, jenis_tes, tgl_mulai, tgl_selesai, judul, keterangan, narahubung FROM jadwal_tes WHERE id='$idj'")->result();
        $isi['menu'] = "profile";
        $isi['mn'] = 'notif';
        $isi['dtsiswa'] = $this->ps_model->get_data_siswa();   
        $this->load->view('frontpage/front_view', $isi);
    } else {
        redirect('frontpage','refresh');
    }
  }

  public function hpsKerja($id){
      $this->db->where('id', $id);
      if($this->db->delete('kerja_siswa')){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }


   public function hpsWira($id){
      $this->db->where('id', $id);
      if($this->db->delete('wira_siswa')){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }


 public function hpsKuliah($id){
      $this->db->where('id', $id);
      if($this->db->delete('kuliah_siswa')){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }

  public function hpsLamaran($id){
      $sid = $this->session->userdata('user_id');
      $dlt = array('id_loker'=>$id, 'id_siswa'=>$sid);
      if($this->db->delete('bidding_lulusan',$dlt)){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }

  public function hpsInbox($id){
      $sid = $this->session->userdata('user_id');
      $dlt = array('id'=>$id, 'peserta_didik_id'=>$sid);
      if($this->db->delete('inbox',$dlt)){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }

  public function updateEmail(){
     $sid = $this->session->userdata('user_id');
    $email = $this->input->post('email');
    $this->db->where('peserta_didik_id', $sid);
    if($this->db->update('ref.peserta_didik',array('email'=>$email))){
        $resp = "ok";
      } else {
            $resp = "not";
      }
      echo json_encode($resp);
  }


  public function gtPass(){
     if($this->session->userdata('login')==TRUE && $this->session->userdata('level')==3){
      $isi['content'] = "pass_view";
      $isi['menu'] = "profile";
      $isi['mn'] = 'pass';
      $this->load->view('frontpage/front_view', $isi);
    } else {
      redirect('frontpage','refresh');
    }
  }


 public function rbhPass(){
    $resp = "";
    $uname = $this->session->userdata("username");
    $plama = md5(md5($this->input->post('opass')));
    $pbaru = md5(md5($this->input->post('npass')));
    if($plama!="" && $pbaru!=""){
        $cek = $this->db->query("SELECT * FROM app.username WHERE username='$uname' AND password='$plama'")->result();
        if(count($cek)>0){
            $this->db->where('username', $uname);
            if($this->db->update('app.username',array('password'=>$pbaru))) {
              $resp = "ok";
            } else {
              $resp = 'nok';
            }

        } else {
           $resp = "not";
        }
    } else{
      $resp = "non";
    }

    echo json_encode($resp);
 }
}
