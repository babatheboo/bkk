<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile_model extends CI_Model {
    var $table  = 'ref.peserta_didik';
    var $table2 = 'view_sekolah_terdaftar';
    var $table3 = 'foto_siswa';
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    
    public function get_data_siswa(){
        $psid =  $this->session->userdata('user_id');
        // $this->db->select("ref.peserta_didik.nama,view_sekolah_terdaftar.nama as nama_sekolah");
        // $this->db->select('ref.jurusan_sp.nama_jurusan_sp');
        // $this->db->from($this->table);
        // $this->db->join($this->table2,'ref.peserta_didik.sekolah_id = view_sekolah_terdaftar.sekolah_id');
        // $this->db->join('ref.jurusan_sp','peserta_didik.jurusan_id=ref.jurusan_sp.jurusan_id');
        // $this->db->where('ref.peserta_didik.peserta_didik_id',$psid);
        // $this->db->group_by('ref.peserta_didik.peserta_didik_id');
        //  $this->db->group_by('ref.peserta_didik.nama');
        //   $this->db->group_by('view_sekolah_terdaftar.nama');
        //    $this->db->group_by('ref.jurusan_sp.nama_jurusan_sp');


        $query = $this->db->query("SELECT a.nama,a.email,a.photo, b.nama as nama_sekolah, c.nama_jurusan
FROM ref.peserta_didik a
LEFT JOIN ref.sekolah b ON a.sekolah_id=b.sekolah_id
LEFT JOIN ref.jurusan c ON a.jurusan_id=c.jurusan_id WHERE a.peserta_didik_id='$psid'");
        return $query;
    }
    public function get_photo()
    {
        $psid =  $this->session->userdata('user_id');
        $this->db->select('foto_siswa.foto');
        $this->db->from($this->table3);
        $this->db->where('foto_siswa.peserta_didik_id',$psid);
        $query = $this->db->get();
        return $query;   
    }

    public function save($data)
    {
        $this->db->insert($this->table3, $data);
    }

    public function get_photo_id($id)
    {
        $this->db->from($this->table3);
        $this->db->where('foto_siswa.peserta_didik_id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function update_photo($where, $data)
    {
        $this->db->update($this->table3, $data, $where);
        return $this->db->affected_rows();
    }

    public function save_kerja($data)
    {
        $this->db->insert('kerja_siswa', $data);
    }

    public function update_kerja($psid,$kode, $data)
    {
        $this->db->where('kerja_siswa.id',$kode);
        $this->db->where('kerja_siswa.peserta_didik_id',$psid);
        $this->db->update('kerja_siswa', $data);
        return $this->db->affected_rows();
    }

   public function delete_kerja($psid,$id)
    {
        $this->db->where('kerja_siswa.peserta_didik_id', $psid);
        $this->db->where('kerja_siswa.id', $id);
        $this->db->delete('kerja_siswa');
    }

    public function get_kerja(){
        $psid =  $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('kerja_siswa');
        $this->db->where('kerja_siswa.peserta_didik_id',$psid);
        $this->db->order_by('kerja_siswa.mulai_kerja', 'desc');
        $query = $this->db->get();
        return $query;   
    }

    public function get_kerja_id($id)
    {
        $psid =  $this->session->userdata('user_id');    
        $this->db->from('kerja_siswa');
        $this->db->where('kerja_siswa.peserta_didik_id',$psid);
        $this->db->where('kerja_siswa.id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_data_competency($psid){
        $this->db->select("kompetensi_siswa.id, kompetensi_siswa.user_id, kompetensi_siswa.kompetensi_inti,
                            kompetensi_siswa.judul_kompetensi, kompetensi_siswa.penyelenggara_id, kompetensi_siswa.tgl_sertifikat,kompetensi_siswa.ver_status,
                            kompetensi_siswa.no_sertifikat, ref.penyelenggara.nama");
        $this->db->from('kompetensi_siswa');
        $this->db->join('ref.penyelenggara','kompetensi_siswa.penyelenggara_id = ref.penyelenggara.id');
        $this->db->where('kompetensi_siswa.user_id',$psid);
        $this->db->order_by("kompetensi_siswa.tgl_sertifikat", "desc"); 
        $query = $this->db->get();
        return $query;   
    }
    public function save_kompetensi($data)
    {
        $this->db->insert('kompetensi_siswa', $data);
    }
    public function update_kompetensi($psid,$kodeid, $data)
    {
        $this->db->where('kompetensi_siswa.user_id',$psid);
        $this->db->where('kompetensi_siswa.id',$kodeid);
        $this->db->update('kompetensi_siswa', $data);
        return $this->db->affected_rows();
    }
    public function delete_kompetensi($psid,$id)
    {
        $this->db->where('kompetensi_siswa.id', $id);    
        $this->db->where('kompetensi_siswa.user_id', $psid);
        $this->db->delete('kompetensi_siswa');
    }
    public function get_kompetensi_id($id)
    {
        $this->db->select("kompetensi_siswa.id, kompetensi_siswa.user_id, kompetensi_siswa.kompetensi_inti,
                            kompetensi_siswa.judul_kompetensi, kompetensi_siswa.penyelenggara_id, kompetensi_siswa.tgl_sertifikat,
                            kompetensi_siswa.no_sertifikat,kompetensi_siswa.ver_status, ref.penyelenggara.nama");
        $this->db->from('kompetensi_siswa');
        $this->db->join('ref.penyelenggara','kompetensi_siswa.penyelenggara_id = ref.penyelenggara.id');
        $this->db->where('kompetensi_siswa.id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_data_penyelenggara($nama){
        $this->db->select("*");
        $this->db->from('ref.penyelenggara');
        $this->db->like('ref.penyelenggara.nama', $nama);
        $query = $this->db->get();
        return $query;
    }
    public function get_kuliah(){
        $psid = $this->session->userdata('user_id');
        $this->db->select("*");
        $this->db->from('kuliah_siswa');
        $this->db->where('kuliah_siswa.peserta_didik_id', $psid);
        $query = $this->db->get();
        return $query;
    }
    public function get_kuliah_id($id)
    {
        $this->db->select("*");
        $this->db->from('kuliah_siswa');
        $this->db->where('kuliah_siswa.id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function save_kuliah($data)
    {
        $this->db->insert('kuliah_siswa', $data);
    }
    public function update_kuliah($psid,$kodeid, $data)
    {
        $this->db->where('kuliah_siswa.peserta_didik_id',$psid);
        $this->db->where('kuliah_siswa.id',$kodeid);
        $this->db->update('kuliah_siswa', $data);
        return $this->db->affected_rows();
    }
    public function delete_kuliah($psid,$id)
    {
        $this->db->where('kuliah_siswa.peserta_didik_id', $psid);
        $this->db->where('kuliah_siswa.id', $id);
        $this->db->delete('kuliah_siswa');
    }
    public function get_wira(){
        $psid = $this->session->userdata('user_id');
        $this->db->select("*");
        $this->db->from('wira_siswa');
        $this->db->where('wira_siswa.peserta_didik_id', $psid);
        $query = $this->db->get();
        return $query;
    }
    public function get_wira_id($id)
    {
        $this->db->select("*");
        $this->db->from('wira_siswa');
        $this->db->where('wira_siswa.id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function save_wira($data)
    {
        $this->db->insert('wira_siswa', $data);
    }
    public function update_wira($psid,$kodeid, $data)
    {
        $this->db->where('wira_siswa.peserta_didik_id',$psid);
        $this->db->where('wira_siswa.id',$kodeid);
        $this->db->update('wira_siswa', $data);
        return $this->db->affected_rows();
    }
    public function delete_wira($psid,$id)
    {
        $this->db->where('wira_siswa.peserta_didik_id', $psid);
        $this->db->where('wira_siswa.id', $id);
        $this->db->delete('wira_siswa');
    }
    public function get_data_bidang($nama){
        $this->db->select("*");
        $this->db->from('ref.bidang_usaha');
        $this->db->like('ref.bidang_usaha.nama_bidang_usaha', $nama);
        $query = $this->db->get();
        return $query;
    }
    public function get_pribadi(){
        $psid = $this->session->userdata('user_id');
        $this->db->select("*");
        $this->db->from('siswa_pribadi');
        $this->db->where('siswa_pribadi.peserta_didik_id', $psid);
        $query = $this->db->get();
        return $query;
    }
    public function get_pribadi_id($id)
    {
        $psid = $this->session->userdata('user_id');
        $this->db->select("*");
        $this->db->from('siswa_pribadi');
        $this->db->where('siswa_pribadi.peserta_didik_id',$psid);
        $query = $this->db->get();
        return $query->row();
    }
    public function save_pribadi($data)
    {
        $this->db->insert('siswa_pribadi', $data);
    }
    public function update_pribadi($psid,$data)
    {
        $this->db->where('siswa_pribadi.peserta_didik_id',$psid);
        $this->db->update('siswa_pribadi', $data);
        return $this->db->affected_rows();
    }
    public function get_prestasi($psid){
        $this->db->select("*");
        $this->db->from('prestasi_siswa');
        $this->db->where('prestasi_siswa.peserta_didik_id', $psid);
        $query = $this->db->get();
        return $query;
    }
    public function save_prestasi($data)
    {
        $this->db->insert('prestasi_siswa', $data);
    }
    public function update_prestasi($id,$psid,$data)
    {
        $this->db->where('prestasi_siswa.id',$id);
        $this->db->where('prestasi_siswa.peserta_didik_id',$psid);
        $this->db->update('prestasi_siswa', $data);
        return $this->db->affected_rows();
    }
    public function delete_prestasi($id,$psid)
    {
        $this->db->where('prestasi_siswa.id',$id);
        $this->db->where('prestasi_siswa.peserta_didik_id', $psid);
        $this->db->delete('prestasi_siswa');
    }
    public function get_prestasi_id($id,$psid)
    {
        $nisn = $this->session->userdata('user_id');
        $this->db->select("*");
        $this->db->from('prestasi_siswa');
        $this->db->where('prestasi_siswa.id',$id);
        $this->db->where('prestasi_siswa.peserta_didik_id',$psid);
        $query = $this->db->get();
        return $query->row();
    }
}