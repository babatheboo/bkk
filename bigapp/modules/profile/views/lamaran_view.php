<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<?php
				$this->load->view('menu_profile');
				?>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="inner-box my-resume">
					<h3>Lowongan yang anda lamar</h3>
					
					<?php
					$sid = $this->session->userdata('user_id');
//$this->db->query("REFRESH MATERIALIZED VIEW view_bidding");
					$d =  $this->db->query("SELECT * FROM bidding_lulusan WHERE id_siswa='$sid'")->result();
					if(count($d)>0){
						?>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center">Kode</th>
									<th class="text-center">Judul Lowongan</th>
									<th class="text-center">Tgl Lamaran</th>
									<th class="text-center">Status</th>
									<th class="text-center">Aksi</th>
								</tr>
							</thead>
							<tbody>		
								<?php	
								foreach ($d as $key) {
									echo "<tr>";
									echo "<td>" . $key->id_loker . "</td>";
									$lw = $this->db->query("SELECT judul FROM mview_lowongan_frontpage WHERE id='$key->id_loker'")->row();
									echo "<td>" . $lw->judul . "</td>";	
									echo "<td>" . $key->tgl_bid . "</td>";
									if($key->status=="0"){
										echo "<td>Diproses</td>";
									} else {
										echo "<td>Testing</td>";
									}
									echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"hpsLamaran('" . $key->id_loker . "')\"><i class='ti-trash'></i></a></td>";
								}
								echo "</tbody></table>";
							} else {
								echo "<p>Anda belum melamar pekerjaan</p>";
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>