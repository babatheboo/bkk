<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
			<?php
			$this->load->view('menu_profile');
			?>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="inner-box my-resume">
				<h3>Ganti Password</h3>	
				<form class="form" method="post" action="" id="fpass">
						<div class="form-group is-empty">
						<label class="control-label" for="textarea">Password Lama*</label>
						<input class="form-control" type="text" name="opass" id="opass">
						<span class="material-input"></span>
						<span class="material-input"></span></div>
						<div class="form-group is-empty">
						<label class="control-label" for="textarea">Password baru*</label>
						<input class="form-control" type="text" name="npass" id="npass">
						<span class="material-input"></span>
						<span class="material-input"></span></div>
						<a href="javascript:void(0)" onclick="rbhpass()" id="submit" class="btn btn-common">Simpan</a>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>