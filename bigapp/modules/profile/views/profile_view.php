<div id="content">
<div class="container">
<div class="row">

<div id="mAddKerja" title="Update pekerjaan">
<form action="" method="post" class="form" id="fmAddKerja"  >
 <div class="form-group is-empty">
   	<label class="control-label">Nama perusahaan</label> <input type="text" name="perusahaan" id="perusahaan" class="form-control">
 </div>
  <div class="form-group is-empty">
  	<label class="control-label">Posisi</label> <input type="text" name="posisi" id="posisi" class="form-control">
 </div>
  <div class="form-group is-empty">
  	<label class="control-label">Mulai Kerja</label><input type="date" name="mulai" id="mulai" class="form-control">
 </div>
 <div class="form-group is-empty">
  	<label class="control-label">Gaji rata-rata</label>
  	<select class="form-control" class="range" name="range" id="range">
  		<option value="<1.000.000">< 1.000.000</option>
  		<option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
  		<option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
  		<option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
  		<option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
  		<option value=">5.000.000">> 5.000.000</option>
  	</select>
 </div>
  <div class="form-group is-empty">
   	<button type="button" onclick="addKerja()" class="btn btn-common">Simpan</button></p>
  </fieldset>
  </div>
</form>
</div>

<div id="mAddWira" title="Update Wirausaha">
<form action="" method="post" class="form" id="fmAddWira"  class="form" >
  <div class="form-group is-empty">
  <label>Nama perusahaan</label><input type="text" name="perusahaan" id="perusahaan" class="form-control">
  </div>
  <div class="form-group is-empty">
  <label>Jenis usaha</label> <input type="text" name="jenis" id="jenis" class="form-control"></div>
  <div class="form-group is-empty">
  <label>Mulai usaha</label>
  <input type="date" name="mulai" id="mulai" class="form-control">
  </div>
  <div class="form-group is-empty">
  	<label class="control-label">Penghasilan</label>
  	<select class="form-control" name="penghasilan" id="penghasilan">
  		<option value="<1.000.000">< 1.000.000</option>
  		<option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
  		<option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
  		<option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
  		<option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
  		<option value=">5.000.000">> 5.000.000</option>
  	</select>
 </div>
  <button type="button" onclick="addWira()" class="btn btn-common">Simpan</button>
</form>
</div>

<div id="mAddKul" title="Update Kuliah">
<form action="" method="post" class="form"  id="fmAddKul">
  <div class="form-group is-empty">
  <label>Nama perguruan tinggi </label> <input type="text" name="perusahaan" id="perusahaan" class="form-control">
</div>
  <div class="form-group is-empty">
  <label>Jurusan</label> <input type="text" name="posisi" id="posisi" class="form-control">
</div>
  <div class="form-group is-empty">
  <label>Mulai kuliah</label><input type="date" name="mulai" id="mulai" class="form-control">
</div>
  <button type="button" onclick="addKul()" class="btn btn-common">Simpan</button>

</form>
</div>

<div id="mRbhKerja" title="Rubah status kerja">
<form action="" method="post" class="form" id="fmRbhKerja" >
  <div class="form-group is-empty">
  <label>Berakhir pada</label> <input type="date" name="akhir" id="akhir" class="form-control">
	<input type="hidden" value="" id="kdjob" name="kdjob" ><input type="hidden" value="0" id="stjob" name="stjob" ></div>
  <p><button type="button" onclick="simpanRbh()" class="btn btn-common">Simpan</button></p>
</form>
</div>

<div id="mRbhWira" title="Rubah status wirausaha">
<form action="" method="post" class="form" id="fmRbhWira">
  <div class="form-group is-empty"><label>Berakhir pada</label> <input type="date" name="akhir" id="akhir">
<input type="hidden" value="" id="kdwir" name="kdwir" ><input type="hidden" value="0" id="stjob" name="stjob" ></div>
  <button type="button" onclick="simpanRbhW()" class="btn btn-common">Simpan</button>
</form>
</div>

<div id="mRbhKul" title="Rubah status kuliah">
<form action="" method="post" class="form" id="fmRbhKul" >
  <div class="form-group is-empty">
  <label>Berakhir pada</label> <input type="date" name="akhir" id="akhir">
<input type="hidden" value="" id="kdkul" name="kdkul" ><input type="hidden" value="0" id="stjob" name="stjob" ></div>
  <button type="button" onclick="simpanRbhK()" class="btn btn-common">Simpan</button>
</form>
</div>


<div id="mImg" title="Update Foto">
<form action="<?php echo base_url();?>profile/updateFoto" enctype="multipart/form-data" method="post" id="gImage">
  <div>
  <label>Pilih foto </label><input name="MAX_FILE_SIZE" value="9999999999" type="hidden">
  <input type="file" name="foto" id="foto"></div>
  <button type="submit" class="btn btn-common">Simpan</button>
</form>
</div>

<div id="mEmail" title="Update Email">
<form action="" method="post" class="form" id="fmEmail" >
  <div class="form-group is-empty">
  <label>Email</label> <input type="text" name="email" id="email" class="form-control"></div>
  <button type="button" onclick="simpanEmail()" class="btn btn-common">Simpan</button>
</form>
</div>

<div id="mEditKer" title="Edit Kerja">
<form action="" method="post" class="form" id="fmEditKer" >
<div class="form-group is-empty">
  <label>Posisi</label>
  <input type="text" name="kePosisi" id="kePosisi" class="form-control">
  <input type="hidden" name="idK" id="idK">
</div>
<div class="form-group is-empty">
  <label>Mulai Kerja</label>
  <input type="date" name="keMulai" id="keMulai" class="form-control">
</div>
<div class="form-group is-empty">
  <label>Sampai Dengan</label>
  <input type="date" name="keAkhir" id="keAkhir" class="form-control">
</div>
<div class="form-group is-empty">
  <label>Status</label>
  <select class="form-control" name="keStatus" id="keStatus">
  	<option value="1">Aktif</option>
  	<option value="0">Tidak Aktif</option>
  </select>
</div>
<div class="form-group is-empty">
  <label>Penghasilan Perbulan</label>
  <select class="form-control" name="keRange" id="keRange">
  		<option value="<1.000.000">< 1.000.000</option>
  		<option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
  		<option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
  		<option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
  		<option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
  		<option value=">5.000.000">> 5.000.000</option>
  	</select>
</div>
  <button type="button" onclick="editKerja()" class="btn btn-common">Simpan</button>
</form>
</div>

<div id="mEditWir" title="Edit Wira">
<form action="" method="post" class="form" id="fmEditWir" >
<div class="form-group is-empty">
  <label>Posisi</label>
  <input type="text" name="wiPosisi" id="wiPosisi" class="form-control">
  <input type="hidden" name="idW" id="idW">
</div>
<div class="form-group is-empty">
  <label>Mulai Wira</label>
  <input type="date" name="wiMulai" id="wiMulai" class="form-control">
</div>
<div class="form-group is-empty">
  <label>Sampai Dengan</label>
  <input type="date" name="wiAkhir" id="wiAkhir" class="form-control">
</div>
<div class="form-group is-empty">
  <label>Status</label>
  <select class="form-control" name="wiStatus" id="wiStatus">
  	<option value="1">Aktif</option>
  	<option value="0">Tidak Aktif</option>
  </select>
</div>
<div class="form-group is-empty">
  <label>Penghasilan Perbulan</label>
  <select class="form-control" name="wiRange" id="wiRange">
  		<option value="<1.000.000">< 1.000.000</option>
  		<option value="1.000.000-2.000.000"> 1.000.000 - 2.000.000</option>
  		<option value="2.000.000-3.000.000"> 2.000.000 - 3.000.000</option>
  		<option value="3.000.000-4.000.000"> 3.000.000 - 4.000.000</option>
  		<option value="4.000.000-5.000.000"> 4.000.000 - 5.000.000</option>
  		<option value=">5.000.000">> 5.000.000</option>
  	</select>
</div>
  <button type="button" onclick="editWira()" class="btn btn-common">Simpan</button>
</form>
</div>



<div class="col-md-4 col-sm-4 col-xs-12">
<?php
$this->load->view('menu_profile');
?>
</div>


<div class="col-md-8 col-sm-8 col-xs-12">
<div class="inner-box my-resume">
<div class="author-resume">
<div class="thumb">
<?php
$pd = $this->session->userdata('user_id');
$ft = $this->db->query("SELECT photo FROM ref.peserta_didik WHERE peserta_didik_id='$pd' and photo is not null")->result();
if(count($ft)>0){
	foreach ($ft as $key) {
?>
		<img src="<?php echo base_url();?>assets/foto/siswa/<?php echo $key->photo;?>" style="width:128px;height:128px">
<?php
	}
} else {
?>
<img src="<?php echo base_url();?>assets/foto/siswa/no.jpg" style="width:128px;height:128px">
<?php	
}
?>

<p style="text-align:center"><a href="javascript:void(0)" onclick="gimage()"><i class="ti-image"></i> Ganti Photo</a></p>
</div>
<div class="author-info">
<h3><?php echo $this->session->userdata('nama_');?></h3><br/>
<h3><?php echo $this->session->userdata('nama_user');?></h3>
<p class="sub-title">NISN : <?php echo $this->session->userdata('username');?></p>
<?php if(!empty($dtsiswa->result())){ $key = $dtsiswa->row();?>
<p><span class="address">Email : <?php echo $key->email;?> &nbsp; &nbsp; &nbsp;<a href="javascript:void(0)" onclick="eEmail()"><i class="ti-email"></i> Rubah Email</a></span></p>
<p><span class="address">Jurusan : <?php echo $key->nama_jurusan;?></span></p>
<?php
}
?>

</div>
</div>

<div class="about-me item">
<h3>Riwayat Pekerjaan <a href="javascript:void(0)" onclick="edKerja()"><i class="ti-plus"></i></a></h3>

<?php 
$nisn = $this->session->userdata('username');
$kerja = $this->db->query("SELECT * FROM kerja_siswa WHERE nisn='$nisn'")->result();
if (count($kerja)>0){
?>
<p>
<table class="table table-responsive">
<thead>
	<tr>
	<th class="text-center">No</th>
	<th class="text-center">Perusahaan/Institusi</th>
	<th class="text-center">Posisi</th>
	<th class="text-center">Mulai Kerja</th>
	<th class="text-center">Sampai Dengan</th>
	<th class="text-center">Status</th>
	<th class="text-center">Verifikasi</th>
	<th class="text-center">Penghasilan Perbulan</th>
	<th class="text-center" colspan="2">Aksi</th>
	</tr>
</thead>
<tbody>
<?php
	$n = 0;
	foreach ($kerja as $key) {
		$n++;
		echo "<tr>";
		echo "<td>" . $n . "</td>";
		echo "<td>" . $key->perusahaan . "</td>";
		echo "<td>" . $key->posisi . "</td>";
		echo "<td>" . date("d-m-Y",strtotime($key->mulai_kerja)) . "</td>";
		if($key->akhir_kerja!=null){
			echo "<td>" . date("d-m-Y",strtotime($key->akhir_kerja)) . "</td>";
		} else {
			echo "<td>Sekarang</td>";
		}
		
		if($key->status==0){
			echo "<td>Non Aktif</td>";
		} else {
			echo "<td>Aktif</td>";
		}
		if($key->ver_status==2){
			echo "<td>Tervirifikasi</td>";
		} else {
			echo "<td>Belum diverifikasi</td>";
		}
		echo "<td>" . $key->range_gaji . "</td>";
		echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"hpsKerja('" . $key->id . "')\"><i class='ti-trash'></i></a></td>";
		echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"cek_kerja('" . $key->id . "')\"><i class='ti-pencil'></i></a></td>";

		echo "</tr>";
	}
	echo "</tbody></table></p>";
} else {
	echo "<p>Anda belum mengisi riwayat pekerjaan, silahkan tambahkan pekerjaan anda !</p>";
}
?>
</div>

<div class="about-me item">
<h3>Riwayat Wirausaha <a href="javascript:void(0)" onclick="edWira()" ><i class="ti-plus"></i></a></h3>
<?php 
$nisn = $this->session->userdata('username');
$kerja = $this->db->query("SELECT * FROM wira_siswa WHERE nisn='$nisn'")->result();
if (count($kerja)>0){
?>
<p>
<table class="table">
<thead>
	<tr>
	<th class="text-center">No</th>
	<th class="text-center">Perusahaan/Institusi</th>
	<th class="text-center">Posisi</th>
	<th class="text-center">Mulai Wira</th>
	<th class="text-center">Sampai Dengan</th>
	<th class="text-center">Status</th>
	<th class="text-center">Verifikasi</th>
	<th class="text-center">Penghasilan Perbulan</th>
	<th class="text-center" colspan="2">Aksi</th>
	</tr>
</thead>
<tbody>
<?php
	$n=0;
	foreach ($kerja as $key) {
		$n++;
		echo "<tr>";
		echo "<td>" . $n . "</td>";
		echo "<td>" . $key->perusahaan . "</td>";
		echo "<td>" . $key->jenis_usaha . "</td>";	
		echo "<td>" . date("d-m-Y",strtotime($key->mulai_usaha)) . "</td>";
		if($key->akhir_usaha!=null){
			echo "<td>" . date("d-m-Y",strtotime($key->akhir_usaha)) . "</td>";
		} else {
			echo "<td>Sekarang</td>";
		}
		if($key->status==0){
			echo "<td><a href='javascript:void(0)'>Non Aktif</a></td>";
		} else {
			echo "<td><a href='javascript:void(0)'>Aktif</a></td>";
		}
		if($key->ver_status==2){
			echo "<td>Tervirifikasi</td>";
		} else {
			echo "<td>Belum diverifikasi</td>";
		}
		echo "<td>" . $key->pendapatan . "</td>";	
		echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"hpsWira('" . $key->id . "')\"><i class='ti-trash'></i></a></td>";
		echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"cek_wira('" . $key->id . "')\"><i class='ti-pencil'></i></a></td>";

		echo "</tr>";
	}
	echo "</tbody></table></p>";
} else {
	echo "<p>Anda belum mengisi riwayat wira usaha, silahkan tambahkan wira usaha anda !</p>";
}
?>

</div>

<div class="about-me item">
<h3>Riwayat Perkuliahan <a href="javascript:void(0)" onclick="edKuliah()" ><i class="ti-plus"></i></a></h3>


<?php 
$nisn = $this->session->userdata('username');
$kerja = $this->db->query("SELECT * FROM kuliah_siswa WHERE nisn='$nisn'")->result();
if (count($kerja)>0){
?>
<p>
<table class="table">
<thead>
	<tr>
	<th class="text-center">No</th>
	<th class="text-center">Perguruan tinggi</th>
	<th class="text-center">Jurusan</th>
	<th class="text-center">Mulai Kuliah</th>
	<th class="text-center">Sampai Dengan</th>
	<th class="text-center">Status</th>
	<th class="text-center">Verifikasi</th>
	<th class="text-center">Aksi</th>
	</tr>
</thead>
<tbody>
<?php
	$n=0;
	foreach ($kerja as $key) {
		$n++;
		echo "<tr>";
		echo "<td>" . $n . "</td>";
		echo "<td>" . $key->nama_pt . "</td>";
		echo "<td>" . $key->jurusan . "</td>";	
		echo "<td>" . date("d-m-Y",strtotime($key->mulai_kuliah)) . "</td>";
		if($key->akhir_kuliah!=null){
			echo "<td>" . date("d-m-Y",strtotime($key->akhir_kuliah)) . "</td>";
		} else {
			echo "<td>Sekarang</td>";
		}
		if($key->status==0){
			echo "<td><a href='javascript:void(0)' onclick=\"rubahk('" .$key->id. "','1')\" >Non Aktif</a></td>";
		} else {
			echo "<td><a href='javascript:void(0)' onclick=\"rubahk('" .$key->id. "','0')\" >Aktif</a></td>";
		}
		if($key->ver_status==2){
			echo "<td>Tervirifikasi</td>";
		} else {
			echo "<td>Belum diverifikasi</td>";
		}
				echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"hpsKuliah('" . $key->id . "')\"><i class='ti-trash'></i></a></td>";

		echo "</tr>";
	}
	echo "</tbody></table></p>";
} else {
	echo "<p>Anda belum mengisi riwayat kuliah, silahkan tambahkan jika anda sedang berkuliah !</p>";
}
?>
</div>
</div>
</div>
</div>
</div>
</div>

