<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
			<?php
			$this->load->view('menu_profile');
			?>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="inner-box my-resume">
					<h3>INBOX</h3>			
<?php
if(count($disi)>0){
?>
<table class="table table-inbox table-hover">
	<thead>
		<tr>
		<th class="text-center">Dari</th>
		<th class="text-center">Judul</th>
		<th class="text-center">Tanggal</th>
		<th class="text-center" style="width: 1%;">Aksi</th>
		</tr>
	</thead>
	<tbody>		
<?php	
	foreach ($disi as $key) {
		if($key->read=='0'){
			echo "<tr class='unread'>";
		} else {
			echo "<tr>";
		}
		
		$iss = $this->db->query("SELECT id, id_loker, alamat, tgl, jenis_tes, tgl_mulai, tgl_selesai, judul, keterangan FROM jadwal_tes where id='$key->jadwal_id'")->result();
		foreach ($iss as $kv) {
			$lid = $kv->id;
			$lw = $this->db->query("SELECT * FROM lowongan WHERE id='$kv->id_loker'")->result();
			foreach ($lw as $keys) {
				$sk = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='$keys->sekolah_pembuat'")->result();
				foreach ($sk as $k) {
					echo "<td><a href='" . base_url() . "profile/read/" . $lid . "/" . $key->id  . "'>" . $k->nama . "</a></td>";	
				}
			}
			echo "<td><a href='" . base_url() . "profile/read/" . $lid . "/" . $key->id  . "'>" . $kv->judul . "</a></td>";	
			echo "<td><a href='" . base_url() . "profile/read/" . $lid . "/" . $key->id  . "'>"  . date("d-m-Y", strtotime($kv->tgl_mulai )). "</a></td>";
			echo "<td class='text-center'><a href='javascript:void(0)' onclick=\"hpsInbox('" . $key->id . "')\"><i class='ti-trash'></i></a></td>";
		}
	}
	echo "</tbody></table>";
} else {
	echo "<p>Tidak ada pesan untuk Anda</p>";
}
?>
				</div>
				</div>
		</div>
	</div>
</div>