<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Master_revitalisasi extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login('4');
  		$this->load->model('master_rev_model');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
 	}
 	public function index(){
 		$isi['namamenu'] = "Revitalisasi";
 		$isi['page']     = "master_revitalisasi";
 		$isi['kelas']    = "master_revitalisasi";
 		$isi['link']     = 'master_revitalisasi';
 		$isi['halaman']  = "Master Revitalisasi";
 		$isi['judul']    = "Master Revitalisasi";
 		$isi['content']  = "master_rev_view";
       	$this->load->view("dashboard/dashboard_view",$isi);
 	}

 	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->master_rev_model->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[]  = $rowx->nama;
				$row[]  = '<center>' . $rowx->npsn . '</center>';
				// $row[]  = '<center>' . number_format($rowx->total_bekerja) . '</center>';
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->master_rev_model->count_all(),
				"recordsFiltered" => $this->master_rev_model->count_filtered(),
				"data"            => $data,
				);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}

	public function cari()
 {
  $output = '';
  $query = '';
  // $this->load->model('ajaxsearch_model');
  if($this->input->post('query')!='')
  {
   $query = $this->input->post('query');
  }
  $data = $this->master_rev_model->fetch_data($query);
  $output .= '<table class="table table-striped table-bordered nowrap" width="100">
  ';
  if($data->num_rows() > 0)
  {
   foreach($data->result() as $row)
   {
    $output .= '
    <tr>
    <td>'.$row->nama.'</td>
    <td>'.$row->npsn.'</td>
    <td>
    <center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\',\'Master Revitalisasi\',\'master_revitalisasi\','."'".$row->sekolah_id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="Tambahkan?"><i class="fa fa-plus-square icon-white"></i></a>
    </td>
    </tr>';
   }
  }
  else
  {
   $output .= '<span class="label label-primary f-s-10"><strong> Data tidak ditemukan</strong></span>';
  }
  $output .= '</table>';
  echo $output;
 }

 public function ubah_status($jns=Null,$id=Null){
    if($this->input->is_ajax_request()){
      if($jns=="inaktif"){
          $data = array('rvt'=>'1');
      }
      $this->db->where('sekolah_id',$this->asn->anti($id));
      $this->db->update('sekolah_terdaftar',$data);
    }else{
      redirect("_404",'refresh');
    }
  }
}