<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile_swa extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("3");
		$this->db = $this->load->database('default', TRUE);
		$this->load->model('profile_swa_model','ps_model');
  		$this->load->library('bcrypt');
  		$this->load->helper('form');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$lvl = $this->session->userdata('level');
		$isi['kelas'] = "inf_swa";
		$isi['namamenu'] = "Data Profile Siswa";
		$isi['page'] = "inf_swa";
		$isi['link'] = 'profile_swa';
		$isi['actionhapus'] = 'hapus';
		$isi['actionedit'] = 'edit';
		$isi['halaman'] = "Data profile Siswa";
        // $isi['dtsiswa'] = $this->ps_model->get_data_siswa()->result();		
        $isi['dtsiswa'] = $this->ps_model->get_data_siswa();		

		$isi['dtfoto']	= $this->ps_model->get_photo()->result();
		$isi['dtkerja']	= $this->ps_model->get_kerja()->result();
		$isi['dtkuliah'] = $this->ps_model->get_kuliah()->result();
		$isi['dtwira'] = $this->ps_model->get_wira()->result();
		$isi['dtpribadi'] = $this->ps_model->get_pribadi()->result();
		$psid =  $this->session->userdata('user_id');    
		$isi['dtprestasi'] = $this->ps_model->get_prestasi($psid)->result();
		$isi['dtkompetensi'] = $this->ps_model->get_data_competency($psid)->result();
		$isi['judul'] = "";
		$isi['content'] = "profile_swa_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function save_foto(){
		if ($this->input->is_ajax_request()) {
			$ckfoto = $this->ps_model->get_photo()->result();
			if (empty($ckfoto)) {
				$this->_validate_photo();
				$data = array(
						'peserta_didik_id' =>$this->session->userdata('user_id')
				);
				if(!empty($_FILES['photo']['name']))
				{
					$upload = $this->_do_upload();
					$data['foto'] = $upload;
				}
				$insert = $this->ps_model->save($data);
				echo json_encode(array("status" => TRUE));
			}else{
				echo json_encode(array("status" => 'avl'));
			}
		}else{
			redirect("_404","refresh");
		}	
	}	
	public function update_foto()
	{
		$this->_validate_photo();
		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/foto/siswa/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink($_SERVER['DOCUMENT_ROOT'].'/assets/foto/siswa/'.$this->input->post('remove_photo'));
			$data['foto'] = '';
		}
		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			//delete file
			$psid = $this->session->userdata('user_id');
			$photo = $this->ps_model->get_photo_id($psid);
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/assets/foto/siswa/'.$photo->foto) && $photo->foto)
				unlink($_SERVER['DOCUMENT_ROOT'].'/assets/foto/siswa/'.$photo->foto);
			$data['foto'] = $upload;
		}
		$this->ps_model->update_photo(array('peserta_didik_id' => $psid), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function photo_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_photo_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	private function _do_upload()
	{
		$pathfile = $_SERVER['DOCUMENT_ROOT'];
		$config['upload_path']          = $pathfile.'/assets/foto/siswa';
        $config['allowed_types']        = 'jpg|jpeg|png';
        $config['max_size']             = 2000; //set max size allowed in Kilobyte
        $config['max_width']            = 2000; // set max width image allowed
        $config['max_height']           = 2000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('photo')) 
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); 
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}
	private function _validate_photo()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if (empty($_FILES['photo']['name']))
		{
			$data['inputerror'][] = 'photo';
			$data['error_string'][] = 'photo dibutuhkan';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_kerja(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_kerja();
				$this->db->trans_start();
				$peserta_didik_id = $this->session->userdata('user_id');
				$dtsiswa = $this->ps_model->get_data_siswa()->result();				
				foreach ($dtsiswa as $key ) {
					$nisn = $key->nisn;
				}
				$tglker = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglker'))));
				$data = array(
							'nisn' => $nisn,
							'perusahaan' => $this->asn->anti($this->input->post('namaper')),
							'posisi' => $this->asn->anti($this->input->post('posker')),
							'mulai_kerja' => $tglker,
							'ver_status' => 0,
							'status' => $this->asn->anti($this->input->post('statusker')),
							'peserta_didik_id' => $peserta_didik_id
						);
				$insert = $this->ps_model->save_kerja($data);
				
				$this->db->select('*');
		        $this->db->from('kerja_siswa');
		        $this->db->where('peserta_didik_id',$peserta_didik_id);
		        $this->db->where('nisn',$nisn);
		        $this->db->where('perusahaan',$this->asn->anti($this->input->post('namaper')));
		        $this->db->where('posisi',$this->asn->anti($this->input->post('posker')));
		        $this->db->where('mulai_kerja',$tglker);
		        $this->db->where('ver_status',0);
		        $this->db->where('status',$this->asn->anti($this->input->post('statusker')));
		        $this->db->limit(1);
		        $query = $this->db->get()->result();  
		        if (!empty($query)) {
		        	foreach ($query as $key ) {
		        		$uuid = $key->id;
		        	}

		        	$tgl_history = date("Y-m-d H:i:s");
									$data2             = array('id_history' => $uuid,
									'peserta_didik_id' =>$peserta_didik_id,
									'tgl_history'      =>$tgl_history,
									'sekolah_id'       =>$this->session->userdata('role_'),
									'tipe_history'     =>4,
									'ver_status'       =>0);
		        	$this->db->insert('siswa_history_all', $data2);	
		        }	

		        $this->db->trans_complete();
		        $this->db1->query("refresh materialized view siswa_history_group_view");
				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function kerja_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_kerja_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_kerja()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_kerja();
			$data = array(
					'perusahaan' =>$this->asn->anti($this->input->post('namaper')),
					'posisi' =>$this->asn->anti($this->input->post('posker')),
					'mulai_kerja' => date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglker')))),
					'ver_status' =>0,
					'status' =>$this->asn->anti($this->input->post('statusker'))
				);
			$psid = $this->session->userdata('user_id');
			$kode = $this->asn->anti($this->input->post('kode'));
			$this->ps_model->update_kerja($psid,$kode,$data);

			$tgl_history = date("Y-m-d H:i:s");
        	$data2 = array('tgl_history'=>$tgl_history,'ver_status' =>0);
        	$this->db->where('id_history',$kode);
        	$this->db->update('siswa_history_all', $data2);	
        	$this->db->trans_complete();
			echo json_encode(array("status" => TRUE));
			$this->db1->query("refresh materialized view siswa_history_group_view");
		}else{
			redirect("_404","refresh");	
		}	
	}
	public function hapus_kerja($id)
    {
        if ($this->input->is_ajax_request()) {
        	$psid = $this->session->userdata('user_id');
	        $this->ps_model->delete_kerja($psid,$id);
	        $this->db1->query("refresh materialized view siswa_history_group_view");
	        echo json_encode(array("status" => TRUE));
    	}else{
    		redirect("_404","refresh");	
    	}
    }
	private function _validate_kerja()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('namaper')) == '')
		{
			$data['inputerror'][] = 'namaper';
			$data['error_string'][] = 'nama perusahaan diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('posker')) == '')
		{
			$data['inputerror'][] = 'posker';
			$data['error_string'][] = 'posisi kerja diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tglker')) == '')
		{
			$data['inputerror'][] = 'tglker';
			$data['error_string'][] = 'tanggal mulai kerja diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tglker')) != '')
		{
			$tglker = $this->input->post('tglker');
			list($dd,$mm,$yyyy) = explode('-',$tglker);
			if (!checkdate($mm,$dd,$yyyy)) {
			    $data['inputerror'][] = 'tglker';
				$data['error_string'][] = 'format tanggal mulai kerja tidak valid';
				$data['status'] = FALSE;
			}	
		}
		if (!is_numeric($this->asn->anti($this->input->post('statusker'))))
		{
			$data['inputerror'][] = 'statusker';
			$data['error_string'][] = 'input status kerja tidak valid';
			$data['status'] = FALSE;    
		}
		if ($this->asn->anti($this->input->post('statusker')) == '')
		{
			$data['inputerror'][] = 'statusker';
			$data['error_string'][] = 'status kerja harap diisi';
			$data['status'] = FALSE;    
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_kompetensi(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_kompetensi();
				$psid = $this->session->userdata('user_id');
				$tglser = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglser'))));
				$this->db->trans_start();
				$data = array(
							'user_id' => $psid,
							'kompetensi_inti' => $this->asn->anti($this->input->post('ki')),
							'judul_kompetensi' => $this->asn->anti($this->input->post('jukom')),
							'penyelenggara_id' => $this->asn->anti($this->input->post('pylkode')),
							'tgl_sertifikat' => $tglser,
							'ver_status' => 0,
							'no_sertifikat' => $this->asn->anti($this->input->post('noser'))
						);
				$insert = $this->ps_model->save_kompetensi($data);

		        $this->db->select('*');
		        $this->db->from('kompetensi_siswa');
		        $this->db->where('user_id',$psid);
		        $this->db->where('kompetensi_inti',$this->asn->anti($this->input->post('ki')));
		        $this->db->where('judul_kompetensi',$this->asn->anti($this->input->post('jukom')));
		        $this->db->where('judul_kompetensi' ,$this->asn->anti($this->input->post('jukom')));
		        $this->db->where('penyelenggara_id',$this->asn->anti($this->input->post('pylkode')));
		        $this->db->where('tgl_sertifikat',$tglser);
		        $this->db->where('no_sertifikat',$this->asn->anti($this->input->post('noser')));
		        $this->db->limit(1);
		        $query = $this->db->get()->result();  

		        if (!empty($query)) {
		        	foreach ($query as $key ) {
		        		$uuid = $key->id;
		        	}

		        	$tgl_history = date("Y-m-d H:i:s");
		        	$data2 = array('id_history' => $uuid,
		        					'peserta_didik_id'=>$psid,
		        					'tgl_history'=>$tgl_history,
		        					'sekolah_id'=>$this->session->userdata('role_'),
		        					'tipe_history'=>5,'ver_status'=>0);
		        	$this->db->insert('siswa_history_all', $data2);	
		        }

		        $this->db->trans_complete();
		        $this->db1->query("refresh materialized view siswa_history_group_view");
				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function kompetensi_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_kompetensi_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_kompetensi()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_kompetensi();
			$this->db->trans_start();
			$data = array(
					'kompetensi_inti' =>$this->asn->anti($this->input->post('ki')),
					'judul_kompetensi' =>$this->asn->anti($this->input->post('jukom')),
					'tgl_sertifikat' => date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglser')))),
					'no_sertifikat' =>$this->asn->anti($this->input->post('noser')),
					'ver_status' => 0,
					'penyelenggara_id' =>$this->asn->anti($this->input->post('pylkode'))
				);
			$psid = $this->session->userdata('user_id');
			$kodeid = $this->asn->anti($this->input->post('kodeid'));
			$this->ps_model->update_kompetensi($psid,$kodeid,$data);
			

			$tgl_history = date("Y-m-d H:i:s");
        	$data2 = array('tgl_history'=>$tgl_history,'ver_status'=>0);
        	$this->db->where('id_history',$kodeid);
        	$this->db->update('siswa_history_all', $data2);	
        	$this->db->trans_complete();
        	$this->db1->query("refresh materialized view siswa_history_group_view");
			echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");	
		}	
	}
	public function hapus_kompetensi($id)
    {
        if ($this->input->is_ajax_request()) {
        	$psid = $this->session->userdata('user_id');
	        $this->ps_model->delete_kompetensi($psid,$id);
	        $this->db1->query("refresh materialized view siswa_history_group_view");
	        echo json_encode(array("status" => TRUE));
    	}else{
    		redirect("_404","refresh");	
    	}
    }
	public function search_operator(){
		if ($this->input->is_ajax_request()) {
			$keyword = $this->asn->anti($this->input->post('pyl'));
			$data['response'] = 'false';
			$query = $this->ps_model->get_data_penyelenggara($keyword)->result();
			if(!empty($query) ){
			    $data['response'] = 'true';
			    $data['message'] = array();
			    foreach( $query as $row ){
					$data['message'][] = array('id'=>$row->id,
						'value'=>$row->nama,
	                    'pyl' => $row->nama
	                );
	            }
			}
			if('IS_AJAX'){
			    echo json_encode($data); //echo json string if ajax request
			}
		}else{
			redirect("_404","refresh");
		}	    
	}	
	private function _validate_kompetensi()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('ki')) == '')
		{
			$data['inputerror'][] = 'ki';
			$data['error_string'][] = 'kompetensi inti diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('jukom')) == '')
		{
			$data['inputerror'][] = 'jukom';
			$data['error_string'][] = 'judul kompetensi diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('pyl')) == '')
		{
			$data['inputerror'][] = 'pyl';
			$data['error_string'][] = 'penyelenggara diisi';
			$data['status'] = FALSE;
		}
		// if($this->asn->anti($this->input->post('pyl')) != '')
		// {
		// 	$nama =	$this->asn->anti($this->input->post('pyl'));
		// 	$dtop = $this->ps_model->get_penyelenggara_id($nama)->result();
		// 	if (empty($dtop)) {
		// 			$data['inputerror'][] = 'pyl';
		// 			$data['error_string'][] = 'penyelenggara tidak tersedia';
		// 			$data['status'] = FALSE;	
		// 		}
		// }
		if($this->asn->anti($this->input->post('pylkode')) == '')
		{
			$data['inputerror'][] = 'pylkode';
			$data['error_string'][] = '';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tglser')) != '')
		{
			$tglser = $this->input->post('tglser');
			list($dd,$mm,$yyyy) = explode('-',$tglser);
			if (!checkdate($mm,$dd,$yyyy)) {
			    $data['inputerror'][] = 'tglser';
				$data['error_string'][] = 'format tanggal sertifikat tidak valid';
				$data['status'] = FALSE;
			}	
		}
		if($this->asn->anti($this->input->post('tglser')) == '')
		{
			$data['inputerror'][] = 'tglser';
			$data['error_string'][] = 'tanggal sertifikat diisi';
			$data['status'] = FALSE;
		}
		if ($this->asn->anti($this->input->post('noser')) == '')
		{
			$data['inputerror'][] = 'noser';
			$data['error_string'][] = 'nomor sertifikat harap diisi';
			$data['status'] = FALSE;    
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_kuliah(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_kuliah();
				$psid = $this->session->userdata('user_id');
				$dtsiswa = $this->ps_model->get_data_siswa()->result();				
				foreach ($dtsiswa as $key ) {
					$nisn = $key->nisn;
				}
				$tglkul = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglkul'))));

				$this->db->trans_start();
				$data = array(
							'nisn' => $nisn,
							'nama_pt' => $this->asn->anti($this->input->post('namapt')),
							'jurusan' => $this->asn->anti($this->input->post('jukul')),
							'mulai_kuliah' => $tglkul,
							'status' => $this->asn->anti($this->input->post('statuskul')),
							'peserta_didik_id' => $psid,
							'ver_status' => 0
						);
				$insert = $this->ps_model->save_kuliah($data);
				$this->db->select('*');
		        $this->db->from('kuliah_siswa');
		        $this->db->where('peserta_didik_id',$psid);
		        $this->db->where('nama_pt',$this->asn->anti($this->input->post('namapt')));
		        $this->db->where('jurusan',$this->asn->anti($this->input->post('jukul')));
		        $this->db->where('mulai_kuliah',$tglkul);
		        $this->db->where('status',$this->asn->anti($this->input->post('statuskul')));
		        $this->db->limit(1);
		        $query = $this->db->get()->result();  
		        if (!empty($query)) {
		        	foreach ($query as $key ) {
		        		$uuid = $key->id;
		        	}

		        	$tgl_history = date("Y-m-d H:i:s");
		        	$data2 = array('id_history' => $uuid,
		        					'peserta_didik_id'=>$psid,
		        					'tgl_history'=>$tgl_history,
		        					'sekolah_id'=>$this->session->userdata('role_'),
		        					'tipe_history'=>2,'ver_status'=>0);
		        	$this->db->insert('siswa_history_all', $data2);	
		        }
		        $this->db->trans_complete();
		        $this->db1->query("refresh materialized view siswa_history_group_view");

				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function kuliah_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_kuliah_id($id);
			$this->db1->query("refresh materialized view siswa_history_group_view");
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_kuliah()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_kuliah();
			$tglkul = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tglkul'))));
			
			$this->db->trans_complete();
			$data = array(
						'nama_pt' => $this->asn->anti($this->input->post('namapt')),
						'jurusan' => $this->asn->anti($this->input->post('jukul')),
						'mulai_kuliah' => $tglkul,
						'ver_status' => 0,
						'status' => $this->asn->anti($this->input->post('statuskul'))
					);
			$psid = $this->session->userdata('user_id');
			$kodeid = $this->asn->anti($this->input->post('kodept'));
			$this->ps_model->update_kuliah($psid,$kodeid,$data);

			$tgl_history = date("Y-m-d H:i:s");
        	$data2 = array('tgl_history'=>$tgl_history,'ver_status'=>0);
        	$this->db->where('id_history',$kodeid);
        	$this->db->update('siswa_history_all', $data2);	
        	$this->db->trans_complete();
        	$this->db1->query("refresh materialized view siswa_history_group_view");
			echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");	
		}	
	}
	public function hapus_kuliah($id)
    {
        if ($this->input->is_ajax_request()) {
        	$nisn = $this->session->userdata('user_id');
	        $this->ps_model->delete_kuliah($nisn,$id);
	        $this->db1->query("refresh materialized view siswa_history_group_view");
	        echo json_encode(array("status" => TRUE));
    	}else{
    		redirect("_404","refresh");	
    	}
    }
	private function _validate_kuliah()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('namapt')) == '')
		{
			$data['inputerror'][] = 'namapt';
			$data['error_string'][] = 'nama perguruan diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('jukul')) == '')
		{
			$data['inputerror'][] = 'jukul';
			$data['error_string'][] = 'jurusan kuliah diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tglkul')) != '')
		{
			$tglkul = $this->input->post('tglkul');
			list($dd,$mm,$yyyy) = explode('-',$tglkul);
			if (!checkdate($mm,$dd,$yyyy)) {
			    $data['inputerror'][] = 'tglkul';
				$data['error_string'][] = 'format tanggal mulai kuliah tidak valid';
				$data['status'] = FALSE;
			}	
		}
		if($this->asn->anti($this->input->post('tglkul')) == '')
		{
			$data['inputerror'][] = 'tglkul';
			$data['error_string'][] = 'tanggal mulai kuliah diisi';
			$data['status'] = FALSE;
		}
		if ($this->asn->anti($this->input->post('statuskul')) == '')
		{
			$data['inputerror'][] = 'statuskul';
			$data['error_string'][] = 'status kuliah diisi';
			$data['status'] = FALSE;    
		}
		if($this->asn->anti($this->input->post('statuskul')) != 0 &&  $this->asn->anti($this->input->post('statuskul')) != 1 ) {
			$data['inputerror'][] = 'statuskul';
			$data['error_string'][] = 'format status kuliah tidak valid';
			$data['status'] = FALSE;    
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_wira(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_wira();
				$this->db->trans_start();
				$peserta_didik_id = $this->session->userdata('user_id');
				$dtsiswa = $this->ps_model->get_data_siswa()->result();				
				foreach ($dtsiswa as $key ) {
					$nisn = $key->nisn;
				}
				$tgl_usaha = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tgl_usaha'))));
				$data = array(
							'nisn' => $nisn,
							'perusahaan' => $this->asn->anti($this->input->post('namawir')),
							'jenis_usaha' => $this->asn->anti($this->input->post('jwira')),
							'mulai_usaha' => $tgl_usaha,
							'ver_status' => 0,
							'status' => $this->asn->anti($this->input->post('status_wira')),
							'peserta_didik_id' => $peserta_didik_id
						);
				$insert = $this->ps_model->save_wira($data);

				$this->db->select('*');
		        $this->db->from('wira_siswa');
		        $this->db->where('peserta_didik_id',$peserta_didik_id);
		        $this->db->where('perusahaan',$this->asn->anti($this->input->post('namawir')));
		        $this->db->where('jenis_usaha',$this->asn->anti($this->input->post('jwira')));
		        $this->db->where('mulai_usaha',$tgl_usaha);
		        $this->db->where('status',$this->asn->anti($this->input->post('status_wira')));
		        $this->db->limit(1);
		        $query = $this->db->get()->result();  
		        if (!empty($query)) {
		        	foreach ($query as $key ) {
		        		$uuid = $key->id;
		        	}

		        	$tgl_history = date("Y-m-d H:i:s");
		        	$data2 = array('id_history' => $uuid,
		        					'peserta_didik_id'=>$peserta_didik_id,
		        					'tgl_history'=>$tgl_history,
		        					'sekolah_id'=>$this->session->userdata('role_'),
		        					'tipe_history'=>3,
		        					'ver_status'=>0);
		        	$this->db->insert('siswa_history_all', $data2);	
		        }

		        $this->db->trans_complete();
		        $this->db1->query("refresh materialized view siswa_history_group_view");
				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function wira_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_wira_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_wira()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_wira();
			$this->db->trans_start();
			$psid = $this->session->userdata('user_id');
			$tgl_usaha = date('Y-m-d',strtotime($this->asn->anti($this->input->post('tgl_usaha'))));
				$data = array(
							'perusahaan' => $this->asn->anti($this->input->post('namawir')),
							'jenis_usaha' => $this->asn->anti($this->input->post('jwira')),
							'mulai_usaha' => $tgl_usaha,
							'ver_status' => 0,
							'status' => $this->asn->anti($this->input->post('status_wira'))
			);
			$kodewir = $this->asn->anti($this->input->post('kodewir'));
			$this->ps_model->update_wira($psid,$kodewir,$data);

			$tgl_history = date("Y-m-d H:i:s");
        	$data2 = array('tgl_history'=>$tgl_history,'ver_status'=>0);
        	$this->db->where('id_history',$kodewir);
        	$this->db->update('siswa_history_all', $data2);	
        	$this->db->trans_complete();
        	$this->db1->query("refresh materialized view siswa_history_group_view");
			echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");	
		}	
	}
	public function hapus_wira($id)
    {
        if ($this->input->is_ajax_request()) {
        	$psid = $this->session->userdata('user_id');
	        $this->ps_model->delete_wira($psid,$id);
	        $this->db1->query("refresh materialized view siswa_history_group_view");
	        echo json_encode(array("status" => TRUE));
    	}else{
    		redirect("_404","refresh");	
    	}
    }
	public function search_jenis_wira(){
		if ($this->input->is_ajax_request()) {
			$keyword = $this->asn->anti($this->input->post('jwira'));
			$data['response'] = 'false';
			$query = $this->ps_model->get_data_bidang($keyword)->result();
			if(!empty($query) ){
			    $data['response'] = 'true';
			    $data['message'] = array();
			    foreach( $query as $row ){
					$data['message'][] = array('id'=>$row->nama_bidang_usaha,
						'value'=>$row->nama_bidang_usaha,
	                    'kode' => $row->bidang_usaha_id
	                );
	            }
			}
			if('IS_AJAX'){
			    echo json_encode($data); //echo json string if ajax request
			}
		}else{
			redirect("_404","refresh");
		}	    
	}
	private function _validate_wira()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('namawir')) == '')
		{
			$data['inputerror'][] = 'namawir';
			$data['error_string'][] = 'nama wirausaha diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('jwira')) == '')
		{
			$data['inputerror'][] = 'jwira';
			$data['error_string'][] = 'jenis wirausaha diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tgl_usaha')) != '')
		{
			$tgl_usaha = $this->input->post('tgl_usaha');
			list($dd,$mm,$yyyy) = explode('-',$tgl_usaha);
			if (!checkdate($mm,$dd,$yyyy)) {
			    $data['inputerror'][] = 'tgl_usaha';
				$data['error_string'][] = 'format tanggal mulai usaha tidak valid';
				$data['status'] = FALSE;
			}	
		}
		if($this->asn->anti($this->input->post('tgl_usaha')) == '')
		{
			$data['inputerror'][] = 'tgl_usaha';
			$data['error_string'][] = 'tanggal mulai usaha diisi';
			$data['status'] = FALSE;
		}
		if ($this->asn->anti($this->input->post('status_wira')) == '')
		{
			$data['inputerror'][] = 'status_wira';
			$data['error_string'][] = 'status wirausaha diisi';
			$data['status'] = FALSE;    
		}
		if($this->asn->anti($this->input->post('status_wira')) != 0 &&  $this->asn->anti($this->input->post('status_wira')) != 1 ) {
			$data['inputerror'][] = 'status_wira';
			$data['error_string'][] = 'format status wirausaha tidak valid';
			$data['status'] = FALSE;    
		}
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_pribadi(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_pribadi();
				$psid = $this->session->userdata('user_id');
				$dtsiswa = $this->ps_model->get_data_siswa()->result();				
				foreach ($dtsiswa as $key ) {
					$nisn = $key->nisn;
				}
				$data = array(
							'nis' => $nisn,
							'tinggi_badan' => $this->asn->anti($this->input->post('tinggi-badan')),
							'berat_badan' => $this->asn->anti($this->input->post('berat-badan')),
							'cacat_fisik' => $this->asn->anti($this->input->post('cacat-fisik')),
							'meroko' => $this->asn->anti($this->input->post('meroko')),
							'buta_warna' => $this->asn->anti($this->input->post('buta-warna')),
							'peserta_didik_id' => $psid
						);
				$insert = $this->ps_model->save_pribadi($data);
				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function pribadi_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$data = $this->ps_model->get_pribadi_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_pribadi()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_pribadi();
			$psid = $this->session->userdata('user_id');
				$data = array(
							'tinggi_badan' => $this->asn->anti($this->input->post('tinggi-badan')),
							'berat_badan' => $this->asn->anti($this->input->post('berat-badan')),
							'cacat_fisik' => $this->asn->anti($this->input->post('cacat-fisik')),
							'meroko' => $this->asn->anti($this->input->post('meroko')),
							'buta_warna' => $this->asn->anti($this->input->post('buta-warna'))
						);
			$this->ps_model->update_pribadi($psid,$data);
			echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");	
		}	
	}
	private function _validate_pribadi()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('tinggi-badan')) == '')
		{
			$data['inputerror'][] = 'tinggi-badan';
			$data['error_string'][] = 'tinggi badan diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('berat-badan')) == '')
		{
			$data['inputerror'][] = 'berat-badan';
			$data['error_string'][] = 'berat badan diisi';
			$data['status'] = FALSE;
		}
		if ($this->asn->anti($this->input->post('cacat-fisik')) == '')
		{
			$data['inputerror'][] = 'cacat-fisik';
			$data['error_string'][] = 'pilih opsi ya atau tidak';
			$data['status'] = FALSE;    
		}
		if ($this->asn->anti($this->input->post('meroko')) == '')
		{
			$data['inputerror'][] = 'meroko';
			$data['error_string'][] = 'meroko diisi';
			$data['status'] = FALSE;    
		}
		if ($this->asn->anti($this->input->post('meroko')) == '')
		{
			$data['inputerror'][] = 'buta-warna';
			$data['error_string'][] = 'buta-warna diisi';
			$data['status'] = FALSE;    
		}			
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	public function save_prestasi(){
		if ($this->input->is_ajax_request()) {
				$this->_validate_prestasi();
				$this->db->trans_start();
				$psid = $this->session->userdata('user_id');
				$data = array(
							'peserta_didik_id' => $psid,
							'jenis_kegiatan' => $this->asn->anti($this->input->post('jns-kegiatan')),
							'tingkat' => $this->asn->anti($this->input->post('tingkat')),
							'jenis' => $this->asn->anti($this->input->post('jenis')),
							'ver_status' => 0,
							'tahun' => $this->asn->anti($this->input->post('tahun'))
						);
				$insert = $this->ps_model->save_prestasi($data);

		        $this->db->select('*');
		        $this->db->from('prestasi_siswa');
		        $this->db->where('peserta_didik_id',$psid);
		        $this->db->where('jenis_kegiatan',$this->asn->anti($this->input->post('jns-kegiatan')));
		        $this->db->where('tingkat',$this->asn->anti($this->input->post('tingkat')));
		        $this->db->where('jenis' ,$this->asn->anti($this->input->post('jenis')));
		        $this->db->where('tahun',$this->asn->anti($this->input->post('tahun')));
		        $this->db->limit(1);
		        $query = $this->db->get()->result();  

		        if (!empty($query)) {
		        	foreach ($query as $key ) {
		        		$uuid = $key->id;
		        	}

		        	$tgl_history = date("Y-m-d H:i:s");
		        	$data2 = array('id_history' => $uuid,
		        					'peserta_didik_id'=>$psid,
		        					'tgl_history'=>$tgl_history,
		        					'sekolah_id'=>$this->session->userdata('role_'),
		        					'tipe_history'=>1,'ver_status'=>0);
		        	$this->db->insert('siswa_history_all', $data2);	
		        }

		        $this->db->trans_complete();
		        $this->db1->query("refresh materialized view siswa_history_group_view");
				echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}	
	}
	public function prestasi_edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$id = $this->asn->anti($id);    
	        $psid = $this->session->userdata('user_id');
			$data = $this->ps_model->get_prestasi_id($id,$psid);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");	
		}
	}
	public function update_prestasi()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate_prestasi();
			$psid = $this->session->userdata('user_id');	
			$id = $this->asn->anti($this->input->post('kodepres'));
				$data = array(
							'jenis_kegiatan' => $this->asn->anti($this->input->post('jns-kegiatan')),
							'tingkat' => $this->asn->anti($this->input->post('tingkat')),
							'jenis' => $this->asn->anti($this->input->post('jenis')),
							'ver_status' => 0,
							'tahun' => $this->asn->anti($this->input->post('tahun'))
						);
			$this->ps_model->update_prestasi($id,$psid,$data);
			echo json_encode(array("status" => TRUE));
		
			$tgl_history = date("Y-m-d H:i:s");
        	$data2 = array('tgl_history'=>$tgl_history,'ver_status'=>0);
        	$this->db->where('id_history',$id);
        	$this->db->update('siswa_history_all', $data2);	
        	$this->db->trans_complete();
        	$this->db1->query("refresh materialized view siswa_history_group_view");
		}else{
			redirect("_404","refresh");	
		}	
	}
	public function hapus_prestasi($id)
    {
        if ($this->input->is_ajax_request()) {
        	$id = $this->asn->anti($id);
        	$get_psid  = $this->ps_model->get_data_siswa()->result();
	        $psid = $this->session->userdata('user_id');
	        $this->ps_model->delete_prestasi($id,$psid);
	        echo json_encode(array("status" => TRUE));
    	}else{
    		redirect("_404","refresh");	
    	}
    }
	private function _validate_prestasi()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;		
		if($this->asn->anti($this->input->post('jns-kegiatan')) == '')
			{
			$data['inputerror'][] = 'jns-kegiatan';
			$data['error_string'][] = 'jenis kegiatan diisi';
			$data['status'] = FALSE;
		}
		if($this->asn->anti($this->input->post('tingkat')) == '')
		{
			$data['inputerror'][] = 'tingkat';
			$data['error_string'][] = 'tingkat diisi';
			$data['status'] = FALSE;
		}
		if ($this->asn->anti($this->input->post('jenis')) == '')
		{
			$data['inputerror'][] = 'jenis';
			$data['error_string'][] = 'pilih jenis dahulu';
			$data['status'] = FALSE;    
		}    			
		if ($this->asn->anti($this->input->post('tahun')) == '')
		{
		     	$data['inputerror'][] = 'tahun';
				$data['error_string'][] = 'tahun prestasi diisi';
				$data['status'] = FALSE;    
		}
		if ($this->asn->anti($this->input->post('tahun')) != '')
		{
			$tahun = (int)$this->input->post('tahun');
		    if($tahun<1945 && $tahun>2100)
		    {
		     	$data['inputerror'][] = 'tahun';
				$data['error_string'][] = 'format tahun tidak valid';
				$data['status'] = FALSE;    
		    }
		}
		// if (empty($_FILES['sertifikat']['name']))
		// {
		// 	$data['inputerror'][] = 'sertifikat';
		// 	$data['error_string'][] = 'sertifikat dibutuhkan';
		// 	$data['status'] = FALSE;
		// }
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}
