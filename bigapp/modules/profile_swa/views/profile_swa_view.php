<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/js/siswa/prof-siswa.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
 <script> -->
    $(document).ready(function() {
        FormPlugins.init();
         $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $(".datepicker").datepicker({
            todayHighlight: !0
        });
    });
 </script>
<div class="row"> 
    <div class="col-md-12"> 
            <div class="profile-container">
                <div class="profile-section">
                    <div class="profile-left">
                        <div class="profile-image">
                            <?php 
                                if(!empty($dtfoto)) { foreach ($dtfoto as $df ) { $foto = $df->foto; } }else{
                                        $foto = 'no.jpg';
                                }
                            ?>
                            <img src="<?php echo base_url();?>assets/foto/siswa/<?php echo $foto;?>" />
                            <i class="fa fa-user hide"></i>
                        </div>
                        <div class="m-b-10">
                            <button class="btn btn-warning btn-block btn-sm"  
                                    <?php if (!empty($dtfoto)): ?>
                                            <?php $nisn = $this->session->userdata('user_id'); ?>
                                            onclick="edit_photo('<?php echo $nisn; ?>')"><i class="glyphicon glyphicon-plus"></i> update Photo 
                                    <?php else: ?>
                                           onclick="add('#form-photo','#modal-photo','ganti foto')"><i class="glyphicon glyphicon-plus"></i> Tambah Photo 
                                    <?php endif ?>
                            </button>
                        </div>
                    </div>
                    <?php if(!empty($dtsiswa->result())){ $key = $dtsiswa->row();
                    ?>
                    <div class="profile-right">
                        <div class="profile-info">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-profile">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>
                                                    <h4>Informasi Sekolah <small></small></h4>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="highlight">
                                                <td class="field">Nama Sekolah</td>
                                                <td><?php echo $key->nama_sekolah;?></td>
                                            </tr>
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td class="field">NISN</td>
                                                <td><?php echo $key->nisn;?></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Jurusan</td>
                                                <td><a href="#"><?php echo $key->nama_jurusan_sp;?></a></td>
                                            </tr>
                                            <tr>
                                                <td class="field">Tanggal Keluar</td>
                                                <td><?php echo date('d-m-Y',strtotime($key->tanggal_keluar));?></td>
                                            </tr>
                                            <tr class="divider">
                                                <td colspan="2"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-profile">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>
                                                    <h4>Informasi Pribadi 
                                                        <?php if (empty($dtpribadi)) {
                                                            ?>
                                                            <button class="btn btn-primary btn-sm" onclick="add('#form-pribadi','#modal-pribadi','tambah informasi pribadi')"><i class="glyphicon glyphicon-plus"></i> tambah </button>    
                                                            <?php 
                                                        } else {
                                                            ?>
                                                            <button class="btn btn-primary btn-sm" onclick="edit_pribadi('profile_swa','pribadi_edit','<?php echo $this->session->userdata('user_id')?>','tambah informasi pribadi')"><i class="glyphicon glyphicon-plus"></i> edit </button>                                                                  
                                                            <?php 
                                                        }
                                                        ?>
                                                     <small></small>
                                                     </h4>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (!empty($dtpribadi)) {
                                                foreach ($dtpribadi as $dtp_siswa) {
                                                    $tinggi_badan = $dtp_siswa->tinggi_badan;
                                                    $berat_badan  = $dtp_siswa->berat_badan;
                                                    $cacat_fisik  = $dtp_siswa->cacat_fisik;
                                                    $meroko       = $dtp_siswa->meroko;
                                                    $buta_warna   = $dtp_siswa->buta_warna;
                                                    if($cacat_fisik!=0) {
                                                        $cacat_fisik = 'Ya';
                                                    }else{
                                                        $cacat_fisik = 'Tidak';
                                                    }
                                                    if($meroko!=0) {
                                                        $meroko = 'Ya';
                                                    }else{
                                                        $meroko = 'Tidak';
                                                    }
                                                    if($buta_warna!=0) {
                                                        $buta_warna = 'Ya';
                                                    }else{
                                                        $buta_warna = 'Tidak';
                                                    }
                                                }    
                                                ?>
                                                    <tr class="highlight">
                                                        <td class="field">Nama</td>
                                                        <td><?php echo $key->nama;?></td>
                                                    </tr>
                                                    <tr class="divider">
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Tgl lahir</td>
                                                        <td><?php echo date('d-m-Y',strtotime($key->tanggal_lahir)); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Jenis Kelamin</td>
                                                        <td><?php echo $key->jenis_kelamin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Tinggi Badan</td>
                                                        <td> <?php echo $tinggi_badan; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Berat Badan</td>
                                                        <td> <?php echo $berat_badan; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Cacat Fisik</td>
                                                        <td> <?php echo $cacat_fisik; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Buta Warna</td>
                                                        <td> <?php echo $buta_warna; ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Merokok</td>
                                                        <td> <?php echo $meroko; ?> </td>
                                                    </tr>
                                                <?php
                                            } else {
                                                ?>
                                                    <tr class="highlight">
                                                        <td class="field">Nama</td>
                                                        <td><?php echo $key->nama;?></td>
                                                    </tr>
                                                    <tr class="divider">
                                                        <td colspan="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Tgl lahir</td>
                                                        <td><?php echo date('d-m-Y',strtotime($key->tanggal_lahir)); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Jenis Kelamin</td>
                                                        <td><?php echo $key->jenis_kelamin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Tinggi Badan</td>
                                                        <td> - </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Berat Badan</td>
                                                        <td> - </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Cacat Fisik</td>
                                                        <td> - </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Buta Warna</td>
                                                        <td> - </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="field">Merokok</td>
                                                        <td> - </td>
                                                    </tr>
                                                <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                      <?php   } ?>
                    </div>
                </div>
                <div class="profile-section">
                    <div class="row">   
                        <div class="col-md-6">
                            <h4 class="title">Prestasi Siswa <button class="btn btn-primary btn-sm m-r-5" onclick="add('#form-prestasi','#modal-prestasi','Tambah Data Prestasi')"><i class="glyphicon glyphicon-plus"></i> Tambah Prestasi Siswa </button> </h4> 
                            <div data-scrollbar="true" data-height="280px" class="bg-silver">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kegiatan</th>
                                            <th>Tingkat</th>
                                            <th>Jenis</th>
                                            <th>Tahun</th>
                                            <th>verifikasi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(!empty($dtprestasi)) {
                                               $i=0;
                                               foreach ($dtprestasi as $lsprestasi) {
                                                $i++;    
                                                     ?>
                                                        <tr>
                                                            <td><?php echo $i;?>.</td>
                                                            <td><?php echo $lsprestasi->jenis_kegiatan;?></td>
                                                            <td><?php echo $lsprestasi->tingkat;?></td>
                                                            <td><?php echo $lsprestasi->jenis;?></td>
                                                            <td><?php echo $lsprestasi->tahun;?></td>
                                                            <td>
                                                                <?php if ($lsprestasi->ver_status == 0){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5" title="menunggu verifikasi admin bkk">menunggu ver</a> <?php
                                                                }else if($lsprestasi->ver_status == 1){ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="sedang melakukan proses verfikasi">proses ver</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="verifikasi data kerja sudah valid">valid ver</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_prestasi('profile_swa','prestasi_edit','<?php echo $lsprestasi->id;?>')"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('<?php echo $lsprestasi->id;?>','<?php echo $lsprestasi->jenis_kegiatan;?>','profile_swa','hapus_prestasi')"><i class="glyphicon glyphicon-trash"></i></a>
                                                            </td>    
                                                        </tr>
                                                     <?php
                                                 }  
                                             } 
                                        ?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="title">Kuliah Siswa  <button class="btn btn-primary btn-sm m-r-5" onclick="add('#form-kuliah','#modal-kuliah','tambah riwayat kuliah')"><i class="glyphicon glyphicon-plus"></i> Tambah Riwayat Kuliah </button> </h4> 
                            <div data-scrollbar="true" data-height="280px" class="bg-silver">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama PT</th>
                                            <th>Jurusan</th>
                                            <th>Tgl Kuliah</th>
                                            <th>Status</th>
                                            <th>Verifikasi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(!empty($dtkuliah)) {
                                               $i=0;
                                               foreach ($dtkuliah as $listkul) {
                                                $i++;    
                                                     ?>
                                                        <tr>
                                                            <td><?php echo $i;?>.</td>
                                                            <td><?php echo $listkul->nama_pt;?></td>
                                                            <td><?php echo $listkul->jurusan;?></td>
                                                            <td><?php echo date('d-m-Y',strtotime($listkul->mulai_kuliah));?></td>
                                                            <td>
                                                                <?php if ($listkul->status == 1){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5">aktif</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-danger  btn-xs m-r-5">inaktif</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($listkul->ver_status == 0){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5" title="menunggu verifikasi admin bkk">menunggu ver</a> <?php
                                                                }else if($listkul->ver_status == 1){ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="sedang melakukan proses verfikasi">proses ver</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="verifikasi data kerja sudah valid">valid ver</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_kuliah('profile_swa','kuliah_edit','<?php echo $listkul->id;?>')"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('<?php echo $listkul->id;?>','<?php echo $listkul->nama_pt;?>','profile_swa','hapus_kuliah')"><i class="glyphicon glyphicon-trash"></i></a>
                                                            </td>    
                                                        </tr>
                                                     <?php
                                                 }  
                                             } 
                                        ?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-section">
                    <div class="row">   
                        <div class="col-md-6">
                            <h4 class="title">Wira Usaha 
                                <button class="btn btn-primary btn-sm m-r-5" onclick="add('#form-wira','#modal-wira','tambah riwayat wira')"><i class="glyphicon glyphicon-plus"></i> Tambah  Wira Usaha </button> </h4> 
                            <div data-scrollbar="true" data-height="280px" class="bg-silver">
                                <table class="table table-condensed">                        
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Perusahaan</th>
                                            <th>Jenis Usaha</th>
                                            <th>Mulai Usaha</th>
                                            <th>Status</th>
                                            <th>Verifikasi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(!empty($dtwira)) {
                                               $i=0;
                                               foreach ($dtwira as $dtw) {
                                                $i++;    
                                                     ?>
                                                        <tr>
                                                            <td><?php echo $i;?>.</td>
                                                            <td><?php echo $dtw->perusahaan;?></td>
                                                            <td><?php echo $dtw->jenis_usaha;?></td>
                                                            <td><?php echo date('d-m-Y',strtotime($dtw->mulai_usaha));?></td>
                                                            <td>
                                                                <?php if ($dtw->status == 1){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5">aktif</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-danger  btn-xs m-r-5">inaktif</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($dtw->ver_status == 0){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5" title="menunggu verifikasi admin bkk">menunggu ver</a> <?php
                                                                }else if($dtw->ver_status == 1){ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="sedang melakukan proses verfikasi">proses ver</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="verifikasi data kerja sudah valid">valid ver</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_wira('profile_swa','wira_edit','<?php echo $dtw->id;?>')"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('<?php echo $dtw->id;?>','<?php echo $dtw->perusahaan;?>','profile_swa','hapus_wira')"><i class="glyphicon glyphicon-trash"></i></a>
                                                            </td>    
                                                        </tr>
                                                     <?php
                                                 }  
                                             } 
                                       ?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="title">Riwayat Kerja 
                                <button class="btn btn-primary btn-sm m-r-5" onclick="add('#form-kerja','#modal-kerja','tambah riwayat kerja')"><i class="glyphicon glyphicon-plus"></i> Tambah Riwayat Kerja </button> </h4> 
                            <div data-scrollbar="true" data-height="280px" class="bg-silver">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Perusahaan</th>
                                            <th>Posisi</th>
                                            <th>Tgl Kerja</th>
                                            <th>Status</th>
                                            <th>Verifikasi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(!empty($dtkerja)) {
                                               $i=0;
                                               foreach ($dtkerja as $dtk) {
                                                $i++;    
                                                     ?>
                                                        <tr>
                                                            <td><?php echo $i;?>.</td>
                                                            <td><?php echo $dtk->perusahaan;?></td>
                                                            <td><?php echo $dtk->posisi;?></td>
                                                            <td><?php echo date('d-m-Y',strtotime($dtk->mulai_kerja));?></td>
                                                            <td>
                                                                <?php if ($dtk->status == 1){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5">aktif</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-danger  btn-xs m-r-5">inaktif</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($dtk->ver_status == 0){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5" title="menunggu verifikasi admin bkk">menunggu ver</a> <?php
                                                                }else if($dtk->ver_status == 1){ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="sedang melakukan proses verfikasi">proses ver</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="verifikasi data kerja sudah valid">valid ver</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_kerja('profile_swa','kerja_edit','<?php echo $dtk->id;?>')"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('<?php echo $dtk->id;?>','<?php echo $dtk->perusahaan;?>','profile_swa','hapus_kerja')"><i class="glyphicon glyphicon-trash"></i></a>
                                                            </td>    
                                                        </tr>
                                                     <?php
                                                 }  
                                             } 
                                       ?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-section">
                <div class="row">        
                        <div class="col-md-12">
                            <h4 class="title">Kompetensi Siswa <button class="btn btn-primary btn-sm m-r-5" onclick="add('#form-kompetensi','#modal-kompetensi','Tambah Kompetensi Siswa')"><i class="glyphicon glyphicon-plus"></i> Tambah Kompetensi Siswa </button> </h4>
                            <div data-scrollbar="true" data-height="280px" class="bg-silver">
                                <table class="table table-condensed">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kompetensi Inti</th>
                                            <th>Nama Kompetensi</th>
                                            <th>Penyelenggara</th>
                                            <th>Tgl Sertifikat</th>
                                            <th>No Sertifikat</th>
                                            <th>Verifikasi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if (!empty($dtkompetensi)) {
                                               $ii = 0;
                                               foreach ($dtkompetensi as $kom) {
                                                   $ii++; 
                                                ?>
                                                    <tr>
                                                        <td class="col-md-1 p-r-5"><?php echo $ii; ?></td>
                                                        <td><?php echo $kom->kompetensi_inti;?></td>
                                                        <td><?php echo $kom->judul_kompetensi;?></td>
                                                        <td><?php echo $kom->nama;?></td>
                                                        <td><?php echo date('d-m-Y',strtotime($kom->tgl_sertifikat));?></td>
                                                        <td><?php echo $kom->no_sertifikat; ?></td>
                                                        <td>
                                                                <?php if ($kom->ver_status == 0){ ?> 
                                                                    <a href="javascript:;" class="btn btn-primary btn-xs m-r-5" title="menunggu verifikasi admin bkk">menunggu ver</a> <?php
                                                                }else if($kom->ver_status == 1){ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="sedang melakukan proses verfikasi">proses ver</a> <?php
                                                                }else{ ?>
                                                                    <a href="javascript:;" class="btn btn-primary  btn-xs m-r-5" title="verifikasi data kerja sudah valid">valid ver</a> <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        <td>
                                                        <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_komptensi('profile_swa','kompetensi_edit','<?php echo $kom->id;?>')"><i class="glyphicon glyphicon-pencil"></i></a>
                                                                <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('<?php echo $kom->id;?>','<?php echo $kom->judul_kompetensi;?>','profile_swa','hapus_kompetensi')"><i class="glyphicon glyphicon-trash"></i></a>
                                                        </td>
                                                    </tr>        
                                                <?php    
                                               }
                                            }
                                        ?>     
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<div id="modal-photo" class="modal fade" role="dialog" >
    <div class="modal-dialog"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Ganti Foto</h4>
            </div>
            <div class="modal-body">
                <form action="#" id="form-photo" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-body">
                                <div class="form-group" id="photo-preview">
                                    <label class="control-label col-md-6">Photo</label>
                                    <div class="col-md-6">
                                        (No photo)
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-6" id="label-photo">Upload Photo </label>
                                    <div class="col-md-6">
                                        <input name="photo" type="file">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" id="btnsavepic" onclick="save('profile_swa','foto','#form-photo','#modal-photo')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-kerja" class="modal fade" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah Riwayat Kerja</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-kerja" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Nama Perusahaan</label>
                                    <div class="col-md-8">
                                        <input type="hidden" value="" name="kode"/>
                                        <input name="namaper" placeholder="nama perusahaan" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Posisi Kerja</label>
                                    <div class="col-md-8">
                                        <input name="posker" placeholder="masukan posisi kerja" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>    
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">mulai kerja</label>
                                    <div class="col-md-8">
                                        <input id="tglker" name="tglker" placeholder="tgl mulai kerja" class="form-control input-sm datepicker" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Status kerja * :</label>
                                    <div class="col-md-8">
                                        <select name="statusker" id="statusker" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Status Kerja --</option>
                                            <option value="0">Non Aktif</option>
                                            <option value="1">Aktif</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" id="btnSave" onclick="save('profile_swa','kerja','#form-kerja','#modal-kerja')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-kompetensi" class="modal fade" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah kompetensi</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-kompetensi" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Kompetensi inti</label>
                                    <div class="col-md-8">
                                        <input name="kodeid" type="hidden">
                                        <input name="ki" placeholder="kompetensi inti" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Judul Kompetensi</label>
                                    <div class="col-md-8">
                                        <input name="jukom" placeholder="masukan judul kompetensi" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Penyelenggara</label>
                                    <div class="col-md-8">
                                        <input id="pylkode" name="pylkode" type="hidden">
                                        <input id="pyl" name="pyl" placeholder="masukan penyelenggara" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>    
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Tgl Sertifikat</label>
                                    <div class="col-md-8">
                                        <input id="tglser" name="tglser" placeholder="tanggal sertifikat" class="form-control input-sm datepicker" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Nomer Sertifikat</label>
                                    <div class="col-md-8">
                                        <input id="noser" name="noser" placeholder="masukan no sertifikat" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('profile_swa','kompetensi','#form-kompetensi','#modal-kompetensi')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-kuliah" class="modal container fade"  tabindex="-1" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah kuliah</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-kuliah" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Nama Perguruan Tinggi</label>
                                    <div class="col-md-8">
                                        <input name="kodept" type="hidden">
                                        <input name="namapt" placeholder="masukan nama perguruan tinggi " class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jurusan Kuliah</label>
                                    <div class="col-md-8">
                                        <input name="jukul" placeholder="masukan jurusan kuliah" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Mulai Kuliah</label>
                                <div class="col-md-8">
                                    <input id="tglkul" name="tglkul" placeholder="Mulai Kuliah" class="form-control input-sm datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Status Kuliah</label>
                                <div class="col-md-8">
                                    <select name="statuskul" id="statuskul" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status Kuliah --</option>
                                        <option value="0">Non Aktif</option>
                                        <option value="1">Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>    
                        </div>    
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('profile_swa','kuliah','#form-kuliah','#modal-kuliah')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-wira" class="modal container fade"  tabindex="-1" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah wirausaha</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-wira" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Nama Perusahaan</label>
                                    <div class="col-md-8">
                                        <input name="kodewir" type="hidden">
                                        <input name="namawir" placeholder="masukan nama wira usaha " class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Masukan Jenis Usaha</label>
                                    <div class="col-md-8">
                                        <input id="jwirakode" name="jwirakode" type="hidden">
                                        <input id="jwira" name="jwira" placeholder="masukan jenis usaha" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Mulai usaha</label>
                                <div class="col-md-8">
                                    <input id="tgl_usaha" name="tgl_usaha" placeholder="Mulai wirausaha" class="form-control input-sm datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Status wira</label>
                                <div class="col-md-8">
                                    <select name="status_wira" id="status_wira" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status wira --</option>
                                        <option value="0">Non Aktif</option>
                                        <option value="1">Aktif</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>    
                        </div>    
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('profile_swa','wira','#form-wira','#modal-wira')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-pribadi" class="modal container fade"  tabindex="-1" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah Data Pribadi</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-pribadi" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Tinggi badan</label>
                                    <div class="col-md-8">
                                        <input id="tinggi-badan" name="tinggi-badan" placeholder="masukan angka tinggi badan " class="form-control input-sm" type="text" maxlength="5">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Masukan berat badan</label>
                                    <div class="col-md-8">
                                        <input id="berat-badan" name="berat-badan" placeholder="masukan angka berat badan" class="form-control input-sm" type="text" maxlength="5" >
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Cacat Fisik</label>
                                    <div class="col-md-8">
                                        <select name="cacat-fisik" id="cacat-fisik" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Status --</option>
                                            <option value="0">Tidak</option>
                                            <option value="1">Ya</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label class="control-label col-md-4">Meroko</label>
                                    <div class="col-md-8">
                                        <select name="meroko" id="meroko" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Status --</option>
                                            <option value="0">Tidak</option>
                                            <option value="1">Ya</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-4">Buta Warna</label>
                                <div class="col-md-8">
                                    <select name="buta-warna" id="buta-warna" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                        <option value="" selected="selected">-- Pilih Status --</option>
                                        <option value="0">Tidak</option>
                                        <option value="1">Ya</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>      
                        </div>    
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('profile_swa','pribadi','#form-pribadi','#modal-pribadi')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div id="modal-prestasi" class="modal container fade"  tabindex="-1" >
    <div class="modal-dialog modal-lg">     
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Tambah Data prestasi</h4>
            </div>
            <div class="form-body">
            <form action="#" id="form-prestasi" class="form-horizontal" enctype="multipart/form-data">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jenis Kegiatan</label>
                                    <div class="col-md-8">
                                        <input id="kodepres" name="kodepres" type="hidden" >
                                        <input id="jns-kegiatan" name="jns-kegiatan" placeholder="masukan jenis kegiatan " class="form-control input-sm" type="text" >
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Tingkat Prestasi</label>
                                    <div class="col-md-8">
                                        <input id="tingkat" name="tingkat" placeholder="masukan tingkat prestasi" class="form-control input-sm" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jenis</label>
                                    <div class="col-md-8">
                                        <select name="jenis" id="jenis" class="form-control input-sm" data-size="20" data-live-search="true" data-style="btn-white">
                                            <option value="" selected="selected">-- Pilih Jenis --</option>
                                            <option value="Perorangan">Perorangan</option>
                                            <option value="Kelompok">Kelompok</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Tahun</label>
                                    <div class="col-md-8">
                                        <input id="tahun" name="tahun" placeholder="masukan tahun prestasi" class="form-control input-sm" maxlength="4" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                        </div>    
                    </div>
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="row">    
                    <div class="col-md-12">
                        <button type="button" onclick="save('profile_swa','prestasi','#form-prestasi','#modal-prestasi')" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .ui-autocomplete
    {
            z-index: 99999;
    }
</style>
<!-- "option", "appendTo", "#form-kompetensi" -->
