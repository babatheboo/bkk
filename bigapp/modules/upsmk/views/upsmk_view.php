<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/upsmk.js"></script>
<div class="row"> <div class="col-md-12"> 
<div class="panel panel-inverse"> 
<div class="panel-heading"> 
<div class="panel-heading-btn">
 <button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()"><i class="fa fa-plus-circle"></i> Tambah Data</button>&nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fas fa-sync-alt"></i> Reload Data</button> </div> <h4 class="panel-title"><?php echo $halaman;?></h4> </div> <div class="panel-body"> <div class="table-responsive"> 
 <table id="data-upsmk" class="table table-striped table-bordered nowrap" width="100%"> 
 <thead>
  <tr> <th style="text-align:center" width="1%">No.</th> 
 <th style="text-align:center" width="10%">NIP</th> 
 <th style="text-align:center" width="50%">Nama</th>
 <th style="text-align:center" width="30%">E-mail</th>
 <th style="text-align:center" width="20%">Telepon</th> 
  <!-- <th style="text-align:center" width="20%">Level</th> -->
 <th style="text-align:center" width="10%">Action</th> 
 </tr> 
 </thead> 
 <tbody> </tbody> 
 </table> 
 </div> 
 </div> 
 </div> 
 </div>
</div>
<div id="modal_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="forminp" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP</label>
                            <div class="col-md-6">
                                <input name="nip" id="nip" placeholder="Masukan NIP" class="form-control" type="text">
                                <span class="help-block"></span>
                                <small>NIP diguanakan untuk username dan password</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama</label>
                            <div class="col-md-6">
                                <input name="nama" id="nama" placeholder="Masukan Nama" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">E-mail</label>
                            <div class="col-md-6">
                                <input name="mail" id="mail" placeholder="Masukan E-mail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Telepon</label>
                            <div class="col-md-6">
                                <input name="tlp" id="tlp" placeholder="Masukan No Telepon" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Level</label>
                            <div class="col-md-6">
                                <input name="level" id="level" placeholder="Operator" readonly="" value="4" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div id="modal_form_pass" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="formch" class="form-horizontal">
                    <input type="hidden" value="" name="idna" id="idna" /> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-6">
                                <input name="psatu" id="psatu" placeholder="Masukan Password" class="form-control" type="password">
                                <span class="help-block"></span>
                                <small>Password Baru</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ulangi Password</label>
                            <div class="col-md-6">
                                <input name="pdua" id="pdua" placeholder="Ulangi Masukan Password" class="form-control" type="password">
                                <span class="help-block"></span>
                                <small>Password Baru</small>
                            </div>
                        </div>
                    </div>
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_pass('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
