<link href="<?php echo base_url();?>assets/css/xls/lapbkk.css" rel="stylesheet" />

<script type="text/javascript">
    $(document).ready(function(){
        $("#pro").hide();
        $("#tombol").hide();
        $("#jenis").change(function(){
            var n = $("#jenis").val();
            if(n==0)
            {
                $("#pro").hide();
                $("#tombol").hide();
                $("#table_wrapper").html("");
            }
            else if(n==1)
            {
             $("#pro").hide(); 
              $("#tombol").show();
            }
            else
            {
              
               $("#pro").show(); 
              $("#tombol").show();
            }
            
        });

        $( "#tombol" ).click(function() {
          $("#table_wrapper").empty();
            var n = $("#jenis").val();
            if(n=="0")
            {
                alert("Pilih jenis laporan !");
                return true;
            }
            else if(n=="1")
            {
                $("#table_wrapper").html("");
                $("#table_wrapper").load("<?php echo base_url();?>lbkk/repot/1");
                return true;
            }
            else 
            {
                var p = $("#prov").val();
                if(p=="0")
                {
                    alert("Pilih Provinsi terlebih dahulu");
                }
                else
                {
                    $("#table_w").html("");
                    $("#table_w").load("<?php echo base_url();?>lbkk/repot/"+p);
                }
                return true;
            }
        });

    });

        function repot()
        {
            
        }

</script>
<div class="row">
<div class="col-md-12 ui-sortable">
        <div class="panel panel-primary" data-sortable-id="ui-widget-15">
        <div class="panel-heading">
            <h4 class="panel-title">Setup Laporan</h4>
        </div>
        <div class="panel-body">
        <div class="col-md-6">
        <label>Jenis Laporan</label>
        
        <select class="form-control" id="jenis">
            <option value="0">Pilih Jenis Laporan</option>
            <option value="1">Semua BKK</option>
            <option value="2">Per Provinsi</option>
        </select>
       <br><br>
            <button type="button" class="btn btn-primary m-r-5 m-b-5" id="tombol">Proses</button>
        </div>
       
        <div id="pro" class="form-group">
        <div class="col-md-6">
        <label>Pilih Provinsi</label>
            <select class="form-control" id="prov" name="prov">
                <option value="0">Pilih Provinsi</option>
                <?php
                    $prov = $this->db1->get('ref.view_provinsi')->result();
                    foreach ($prov as $key) {
                        echo "<option value='" . $key->kode_prov . "'>" . $key->prov . "</option>";
                    }
                ?>
            </select>
        </div>
        </div>    

        </div>
    </div>
    </div>


    <div class="col-md-12 ui-sortable" id="table_wrapper">
    </div>
    <div class="col-md-12 ui-sortable" id="table_w">
    </div>
</div>