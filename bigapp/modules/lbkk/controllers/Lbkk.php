<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lbkk extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
 	}

 	public function index()
 	{
 		$isi['namamenu'] = "BKK";
		$isi['page'] = "lap";
		$isi['kelas'] = "registrasi";
		$isi['link'] = 'reg_bkk';
		$isi['halaman'] = "Laporan";
		$isi['judul'] = "BKK Terdaftar";
		$isi['content'] = "lbkk_view";
       	$this->load->view("dashboard/dashboard_view",$isi);
 	}

 	function repot($kode)
 	{
 		if($kode=="1")
		 {
		 	$dt = $this->db1->get('view_reg_bkk')->result();
		 	echo  "<table border=0 cellpadding=0 cellspacing=0 width=1604 style='border-collapse:
				 collapse;table-layout:fixed;width:1204pt'>
				 <col width=35 style='mso-width-source:userset;mso-width-alt:1280;width:26pt'>
				 <col width=186 span=3 style='mso-width-source:userset;mso-width-alt:6802;
				 width:140pt'>
				 <col width=100 span=5 style='mso-width-source:userset;mso-width-alt:3657;
				 width:75pt'>
				 <col width=92 style='mso-width-source:userset;mso-width-alt:3364;width:69pt'>
				 <col width=240 style='mso-width-source:userset;mso-width-alt:8777;width:180pt'>
				 <col width=64 style='width:48pt'>
				 <col width=115 style='mso-width-source:userset;mso-width-alt:4205;width:86pt'>
				 <tr height=20 style='height:15.0pt'>
				  <td colspan=9 height=20 class=xl65 width=1093 style='height:15.0pt;
				  width:821pt'>LAPORAN BKK TERDAFTAR</td>
				  <td class=xl65 width=92 style='width:69pt'></td>
				  <td class=xl65 width=240 style='width:180pt'></td>
				  <td class=xl65 width=64 style='width:48pt'></td>
				  <td class=xl65 width=115 style='width:86pt'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td colspan=9 height=20 class=xl65 style='height:15.0pt'>Sampai dengan
				  tanggal :<span style='mso-spacerun:yes'> " .date('d-m-Y') ." </span></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				 </tr>
				 <tr height=40 style='height:30.0pt;mso-xlrowspan:2'>
				  <td height=40 colspan=13 style='height:30.0pt;mso-ignore:colspan'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td rowspan=2 height=40 class=xl66 style='height:30.0pt'>No</td>
				  <td rowspan=2 class=xl66>Nama BKK</td>
				  <td rowspan=2 class=xl69>Alamat</td>
				  <td rowspan=2 class=xl69>Provinsi</td>
				  <td colspan=5 class=xl66 style='border-left:none'>Jumlah</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td height=20 class=xl66 style='height:15.0pt;border-top:none;border-left:
				  none'>Lulusan (3 thn )</td>
				  <td class=xl66 style='border-top:none;border-left:none'>Kls Akhir</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Bekerja</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Kuliah</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Wira Usaha</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>";
				 
				 $n=0;
				 foreach ($dt as $key) {
				 	$n++;
				 	$id = $key->sekolah_id;
				 	$th = date('Y');
				 	$ta = $th - 3;
				 	$th = $th . "-05" . "-01";
				 	$ta = $ta . "-04" . "-30";
				 	$x = "SELECT * FROM view_siswa_terdaftar WHERE sekolah_id='$id' AND jenis_keluar_id='1' AND tanggal_keluar BETWEEN '$ta' AND '$th'";
				 	$xx = "SELECT * FROM view_siswa_terdaftar WHERE sekolah_id='$id' AND jenis_keluar_id is null";
				 	$ks = "SELECT * FROM view_kerja_siswa WHERE sekolah_id='$id'";
				 	$us = "SELECT * FROM view_kuliah_siswa WHERE sekolah_id='$id'";
				 	$ss = "SELECT * FROM view_wira_siswa WHERE sekolah_id='$id'";
				 	$l = count($this->db1->query($x)->result());
					$k = count($this->db1->query($xx)->result());
					$ks = count($this->db1->query($ks)->result());
					$ksa = count($this->db1->query($us)->result());
					$ksb = count($this->db1->query($ss)->result());

				 	echo "<tr height=20 style='height:15.0pt'>
					  <td height=20 class=xl68 style='height:15.0pt;border-top:none'>". $n . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". $key->nama ."</td>
					  <td class=xl68 style='border-left:none'>". $key->alamat_jalan ."</td>
					  <td class=xl68 style='border-left:none'>". $key->nama_prov ."</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($l) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($k) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ks) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ksa) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ksb) . "</td>
					  <td colspan=4 style='mso-ignore:colspan'></td>
					 </tr>";
				 }
				 

				 echo "<tr height=20 style='height:15.0pt'>
				  <td colspan=2 height=20 class=xl67 style='height:15.0pt'>JUMLAH</td>
				  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>
				 
				</table>";
		 }
		 else
		 {
		 	$q = "SELECT * FROM view_reg_bkk WHERE kode_wilayah='$kode'";
		 	$dt = $this->db1->query($q)->result();
		 	if(empty($dt))
		 	{
		 		echo "Tidak ada data";
		 	}
		 	else
		 	{
		 		echo  "<table border=0 cellpadding=0 cellspacing=0 width=1604 style='border-collapse:
				 collapse;table-layout:fixed;width:1204pt'>
				 <col width=35 style='mso-width-source:userset;mso-width-alt:1280;width:26pt'>
				 <col width=186 span=3 style='mso-width-source:userset;mso-width-alt:6802;
				 width:140pt'>
				 <col width=100 span=5 style='mso-width-source:userset;mso-width-alt:3657;
				 width:75pt'>
				 <col width=92 style='mso-width-source:userset;mso-width-alt:3364;width:69pt'>
				 <col width=240 style='mso-width-source:userset;mso-width-alt:8777;width:180pt'>
				 <col width=64 style='width:48pt'>
				 <col width=115 style='mso-width-source:userset;mso-width-alt:4205;width:86pt'>
				 <tr height=20 style='height:15.0pt'>
				  <td colspan=9 height=20 class=xl65 width=1093 style='height:15.0pt;
				  width:821pt'>LAPORAN BKK TERDAFTAR</td>
				  <td class=xl65 width=92 style='width:69pt'></td>
				  <td class=xl65 width=240 style='width:180pt'></td>
				  <td class=xl65 width=64 style='width:48pt'></td>
				  <td class=xl65 width=115 style='width:86pt'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td colspan=9 height=20 class=xl65 style='height:15.0pt'>Sampai dengan
				  tanggal :<span style='mso-spacerun:yes'> " .date('d-m-Y') ." </span></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				  <td class=xl65></td>
				 </tr>
				 <tr height=40 style='height:30.0pt;mso-xlrowspan:2'>
				  <td height=40 colspan=13 style='height:30.0pt;mso-ignore:colspan'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td rowspan=2 height=40 class=xl66 style='height:30.0pt'>No</td>
				  <td rowspan=2 class=xl66>Nama BKK</td>
				  <td rowspan=2 class=xl69>Alamat</td>
				  <td rowspan=2 class=xl69>Provinsi</td>
				  <td colspan=5 class=xl66 style='border-left:none'>Jumlah</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>
				 <tr height=20 style='height:15.0pt'>
				  <td height=20 class=xl66 style='height:15.0pt;border-top:none;border-left:
				  none'>Lulusan (3 thn )</td>
				  <td class=xl66 style='border-top:none;border-left:none'>Kls Akhir</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Bekerja</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Kuliah</td>
				  <td class=xl67 style='border-top:none;border-left:none'>Wira Usaha</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>";
				 
				 $n=0;
				 foreach ($dt as $key) {
				 	$n++;
				 	$id = $key->sekolah_id;
				 	$th = date('Y');
				 	$ta = $th - 3;
				 	$th = $th . "-05" . "-01";
				 	$ta = $ta . "-04" . "-30";
				 	$x = "SELECT * FROM view_siswa_terdaftar WHERE sekolah_id='$id' AND jenis_keluar_id='1' AND tanggal_keluar BETWEEN '$ta' AND '$th'";
				 	$xx = "SELECT * FROM view_siswa_terdaftar WHERE sekolah_id='$id' AND jenis_keluar_id is null";
				 	$ks = "SELECT * FROM view_kerja_siswa WHERE sekolah_id='$id'";
				 	$us = "SELECT * FROM view_kuliah_siswa WHERE sekolah_id='$id'";
				 	$ss = "SELECT * FROM view_wira_siswa WHERE sekolah_id='$id'";
				 	$l = count($this->db1->query($x)->result());
					$k = count($this->db1->query($xx)->result());
					$ks = count($this->db1->query($ks)->result());
					$ksa = count($this->db1->query($us)->result());
					$ksb = count($this->db1->query($ss)->result());

				 	echo "<tr height=20 style='height:15.0pt'>
					  <td height=20 class=xl68 style='height:15.0pt;border-top:none'>". $n . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". $key->nama ."</td>
					  <td class=xl68 style='border-left:none'>". $key->alamat_jalan ."</td>
					  <td class=xl68 style='border-left:none'>". $key->nama_prov ."</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($l) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($k) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ks) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ksa) . "</td>
					  <td class=xl68 style='border-top:none;border-left:none'>". number_format($ksb) . "</td>
					  <td colspan=4 style='mso-ignore:colspan'></td>
					 </tr>";
				 }
				 echo "<tr height=20 style='height:15.0pt'>
				  <td colspan=2 height=20 class=xl67 style='height:15.0pt'>JUMLAH</td>
				  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td class=xl68 style='border-top:none;border-left:none'>&nbsp;</td>
				  <td colspan=4 style='mso-ignore:colspan'></td>
				 </tr>
				 
				</table>";
		 	}
		 }

 		
 	}
}