<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lapbkk_kuliah_model extends CI_Model {
    var $table = 'view_lap_kuliah a';
    var $column_order = array(null,'a.nama','a.tahun_keluar','b.nama_jurusan_sp', 'a.nisn',null);
    var $column_search = array('LOWER(nama)','nama_jurusan_sp');
    var $order = array('nama' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    private function _get_datatables_query(){
      $sess_sekolah = $this->session->userdata('role_');

      $tahun = $this->input->post('tahun');
      $jurusan = $this->input->post('jurusan');
      if($tahun == 0 and $jurusan == 0){
          $this->db->select("a.nama,b.nama_jurusan as nama_jurusan_sp, a.tahun_keluar, a.nama_pt, a.sekolah_id, a.nisn");
          $this->db->from($this->table);
          $this->db->join("ref.jurusan b","a.jurusan_id=b.jurusan_id");
          $this->db->where('sekolah_id', $sess_sekolah);
      } else if ($tahun != 0 and $jurusan == 0 ) {
          $this->db->select("a.nama,b.nama_jurusan as nama_jurusan_sp, a.tahun_keluar, a.nama_pt, a.sekolah_id, a.nisn");
          $this->db->from($this->table);
          $this->db->join("ref.jurusan b","a.jurusan_id=b.jurusan_id");
          $this->db->where(array('sekolah_id'=>$sess_sekolah,'a.tahun_keluar'=>$tahun));
      } else if ($tahun == 0 and $jurusan != 0 ) {
          $this->db->select("a.nama,b.nama_jurusan as nama_jurusan_sp, a.tahun_keluar, a.nama_pt, a.sekolah_id, a.nisn");
          $this->db->from($this->table);
          $this->db->join("ref.jurusan b","a.jurusan_id=b.jurusan_id");
          $this->db->where(array('sekolah_id'=>$sess_sekolah,'a.jurusan_id'=>$jurusan));
      } else if($tahun != 0 and $jurusan != 0){
          $this->db->select("a.nama,b.nama_jurusan as nama_jurusan_sp, a.tahun_keluar, a.nama_pt, a.sekolah_id, a.nisn");
          $this->db->from($this->table);
          $this->db->join("ref.jurusan b","a.jurusan_id=b.jurusan_id");
          $this->db->where(array('sekolah_id'=>$sess_sekolah,'a.jurusan_id'=>$jurusan,'a.tahun_keluar'=>$tahun));
      }
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
     function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
      $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
      // $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        // $this->db->where('sekolah_id',$sess_sekolah);
        return $this->db->count_all_results();
    }
  }