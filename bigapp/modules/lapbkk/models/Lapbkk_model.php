<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lapbkk_model extends CI_Model {
    var $table = 'view_kerja_siswa';
    var $table2 = 'view_kuliah_siswa';
    var $table3 = 'view_wira_siswa';
    var $column_order = array(null,'nama','tanggal_keluar','nama_jurusan_sp',null);
    var $column_search = array('nama',"left(tanggal_keluar,4)",'nama_jurusan_sp'); 
    var $order = array('nama' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    private function _get_datatables_query($tabel){
        $tahun = $this->input->post('tahun');
        $jurusan = $this->input->post('jurusan');
        $sess_sekolah = $this->session->userdata('role_');
        if($tabel=='ker')
        {
            $this->db->select("nama,left(tanggal_keluar,4) as tanggal_keluar,nama_jurusan_sp,perusahaan");
            $this->db->from($this->table);
        }
        else if($tabel=='kul')
        {
            $this->db->select("nama,left(tanggal_keluar,4) as tanggal_keluar,nama_jurusan_sp,nama_pt");
            $this->db->from($this->table2);
        }
        else
        {
            $this->db->select("nama,left(tanggal_keluar,4) as tanggal_keluar,nama_jurusan_sp,perusahaan");
            $this->db->from($this->table3);
        }
        $this->db->where('sekolah_id',$sess_sekolah);
        if($tahun!='0')
        {
            $this->db->where('left(tanggal_keluar,4)',$tahun);
        }
        if($jurusan!='0')
        {
            $this->db->where('jurusan_id',$jurusan);
        }
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables($tabel){
        $this->_get_datatables_query($tabel);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered($tabel){
        $this->_get_datatables_query($tabel);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all($tabel){
        $sess_sekolah = $this->session->userdata('role_');
        if($tabel=='ker')
        {
            $this->db->from($this->table);
        }
        else if($tabel=='kul')
        {
            $this->db->from($this->table2);
        }
        else
        {
            $this->db->from($this->table3);
        }
        $this->db->where('sekolah_id',$sess_sekolah);
        return $this->db->count_all_results();
    }
    public function get_by_id($id,$tabel){

        if($tabel=='ker')
        {
            $this->db->from($this->table);
        }
        else if($tabel=='kul')
        {
            $this->db->from($this->table2);
        }
        else
        {
            $this->db->from($this->table3);
        }
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
}
