<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lapbkk_mitra_model extends CI_Model {
    var $table = 'view_mitra a';
    var $column_order = array(null, 'a.nama','a.alamat','a.nama_bidang_usaha', 'b.industri_id', null);
    var $column_search = array('UPPER(a.nama)','a.nama_bidang_usaha');
    var $order = array('a.nama' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    private function _get_datatables_query(){
      $sess_sekolah = $this->session->userdata('role_');
      $this->db->select("a.nama,b.industri_id, a.alamat, a.nama_bidang_usaha");
      $this->db->from($this->table);
      $this->db->join("mitra b","a.industri_id=b.industri_id");
      $this->db->where('a.sekolah_id', $sess_sekolah);
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                }else{
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
      $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
      // $sess_sekolah = $this->session->userdata('role_');
        $this->db->from($this->table);
        // $this->db->where('sekolah_id',$sess_sekolah);
        return $this->db->count_all_results();
    }
  }