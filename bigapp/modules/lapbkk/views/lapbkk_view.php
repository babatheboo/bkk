<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<!-- <script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script> -->
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/lap_bkk.js"></script>
<div id="modal_info" hidden="" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Saran Penggunaan</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger fade in m-b-15">
                    <strong>Informasi!</strong><br>
                    <p>Tampilkan semua data sebelum melakukan <i>export</i></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Mengerti!</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ui-sortable">
        <div class="panel panel-primary" data-sortable-id="ui-widget-15">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <button class="btn btn-danger btn-xs m-r-5" onclick="info()"><i class="fa fa-info"></i> Info!</button>&nbsp;
                </div>
                <!-- <div class="panel-heading-btn">
                    <button onclick="exp()" class="btn btn-success btn-xs m-r-5"><i class="fa fa-file-excel"></i> Download</button>&nbsp;
                </div> -->
                <h4 class="panel-title">Setup Laporan</h4>
            </div>
            <div class="panel-body">
                <div class="pull-right">
                    <a href="javascript:bukaNaker()" class="btn btn-success"><i class="fas fa-users"></i> EKSPORT DATA DISNAKER <i class="fas fa-users"></i></a>
                    <a href="<?php echo base_url();?>assets/dokumen/FORMAT LAPORAN BKK.xlsx" data-toggle="tooltip" title="Download format laporan" class="btn btn-inverse btn-icon btn-circle btn-lg m-r-10 m-t-10 m-b-10" href="">
                        <i class="fas fa-download"></i>
                    </a>
                </div>
                <div class="col-md-3">
                    <label>Jenis Laporan </label style="border-bottom: none;">
                    <select class="form-control" id="jenis" name="jenis">
                        <option value="0">Pilih Jenis Laporan</option>
                        <option value="1">Keterserapan Lulusan</option>
                        <option value="2">Mitra Industri</option>
                    </select>
                </div>
            </div>
            <div class="panel-body" id="boxstatus">
                <div class="row">
                    <div class="col-md-2">
                        <label>Status Lulusan</label>
                        <select class="form-control" id="status" name="status">
                            <option value="1">Bekerja</option>
                            <option value="2">Kuliah</option>
                            <option value="3">Wirausaha</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Tahun Lulusan</label>
                        <select class="form-control" id="tahun" name="tahun">
                            <option value="0">Semua Tahun</option>
                            <?php
                            $ts = date("Y");
                            $ta = $ts-2;
                            for ($i=$ta; $i <$ts ; $i++) {
                                ?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                            }
                            ?>
                            <option value="<?php echo date('Y')?>"><?php echo date('Y')?></option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Jurusan</label>
                        <select class="form-control" id="jurusan" name="jurusan">
                            <option value="0">Semua Jurusan</option>
                            <?php
                            $id = $this->session->userdata('role_');
                            $dj = $this->db->query("SELECT * FROM ref.jurusan_sp WHERE sekolah_id='$id'")->result();
                            foreach ($dj as $key) {

                                ?>
                                <option value="<?php echo $key->jurusan_id;?>"><?php echo $key->nama_jurusan_sp;?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <!-- kerja -->
            <div class="panel-body" id="divk">
                <div class="table-responsive">
                    <center><h4>Data Bekerja Alumni</h4></center>
                    <table id="data-kerja" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="40%">Tahun</th>
                                <th style="text-align:center" width="40%">NISN</th>
                                <th style="text-align:center" width="40%">Nama</th>
                                <!-- <th style="text-align:center" width="20%">Jurusan</th> -->
                                <!-- <th style="text-align:center" width="10%">Tahun Lulus</th> -->
                                <th style="text-align:center" width="10%">Perusahaan</th>
                                <th style="text-align:center" width="40%">Posisi</th>
                                <th style="text-align:center" width="40%">Mulai</th>
                                <th style="text-align:center" width="40%">Range Gaji</th>
                                <th style="text-align:center" width="40%">Bidang</th>
                                <th style="text-align:center" width="40%">Provinsi</th>
                                <th style="text-align:center" width="40%">Linearitas</th>
                                <th style="text-align:center" width="40%">Perjanjian</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- kuliah -->
            <div class="panel-body" id="divm">
                <div class="table-responsive">
                    <center><h4>Data Kuliah Alumni</h4></center>
                    <table id="data-kuliah" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">Tahun Lulus</th>
                                <th style="text-align:center" width="10%">NISN</th>
                                <th style="text-align:center" width="40%">Nama</th>
                                <th style="text-align:center" width="10%">Perguruan Tinggi</th>
                                <th style="text-align:center" width="10%">Bidang Studi</th>
                                <th style="text-align:center" width="20%">Jurusan</th>
                                <th style="text-align:center" width="10%">Tanggal Masuk</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- wira -->
            <div class="panel-body" id="divw">
                <div class="table-responsive">
                    <center><h4>Data Wirausaha Alumni</h4></center>
                    <table id="data-wira" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="10%">Tahun Lulus</th>
                                <th style="text-align:center" width="10%">NISN</th>
                                <th style="text-align:center" width="40%">Nama</th>
                                <th style="text-align:center" width="20%">Jurusan</th>
                                <th style="text-align:center" width="10%">Perusahaan</th>
                                <th style="text-align:center" width="10%">Jenis Usaha</th>
                                <th style="text-align:center" width="10%">Tanggal Usaha</th>
                                <th style="text-align:center" width="10%">Pendapatan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- mitra -->
            <div class="panel-body" id="div-mitra">
                <div class="table-responsive">
                    <center><h4>Data Mitra DUDI</h4></center>
                    <table id="data-mitra" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="20%">Nama Perusahaan</th>
                                <th style="text-align:center" width="25%">Alamat</th>
                                <th style="text-align:center" width="25%">Bidang Usaha</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- / -->
            <div class="panel-body" id="tombol">
<!-- <div class="col-md-4">
<button type="button" class="btn btn-primary m-r-5 m-b-5" id="tombol"><i class="fa fa-file-excel-o"> Export ke Excel</i></button>
</div> -->
</div>
</div>
</div>
</div>
<?php
$bln = date('n');
if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
}else{
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
}

?>
<div id="modal_naker" class="modal fade">
    <form action="<?php echo base_url();?>lapbkk/dataDisnaker" method="post">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Unduh Data Disnaker</h4>
            </div>
            <div class="modal-body">
                <label for="bulan">Bulan</label>
                <select required="" class="form-control" name="bulan" id="bulan">
                    <option value="" selected="">Pilih bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">Nopember</option>
                    <option value="12">Desember</option>
                </select>
                <label for="tahunNaker">Tahun</label>
                <select required="" class="form-control" name="tahunNaker" id="tahunNaker">
                    <option value="" selected="">Pilih tahun</option>
                    <option value="<?php echo $t1;?>"><?php echo $t1;?></option>
                    <option value="<?php echo $t2;?>"><?php echo $t2;?></option>
                    <option value="<?php echo $t3;?>"><?php echo $t3;?></option>
                    <option value="<?php echo $t4;?>"><?php echo $t4;?></option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Unduh</button>
            </div>
        </div>
    </div>
    </form>
</div>