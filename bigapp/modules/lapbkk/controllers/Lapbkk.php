<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lapbkk extends CI_Controller {
  public function __construct(){
    parent::__construct();
$this->asn->login(); // cek session login
if($this->session->userdata('level')==1){
  $this->asn->role_akses("1");
}elseif($this->session->userdata('level')==2){
  $this->asn->role_akses("2");
}
$this->db = $this->load->database('default', TRUE);
$this->load->model('lapbkk_kerja_model');
$this->load->model('lapbkk_kuliah_model');
$this->load->model('lapbkk_wira_model');
$this->load->model('lapbkk_mitra_model');
date_default_timezone_set('Asia/Jakarta');
}
public function index()
{
  $isi['namamenu'] = "BKK";
  $isi['page'] = "lap";
  $isi['kelas'] = "registrasi";
  $isi['link'] = 'reg_bkk';
  $isi['halaman'] = "Laporan";
  $isi['judul'] = "pembuatan laporan";
//  $isi['content'] = "lapbkk_view";
  $sid = $this->session->userdata('role_');
  $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
  $valid = "";
  foreach ($b as $key) {
    $valid = $key->valid;
  }
  if($valid=="1"){
    $isi['content']              = "lapbkk_view";
  } else {
    $isi['content']              = "admin_bkk";
  }

  $this->load->view("dashboard/dashboard_view",$isi);
}
public function getData($tabel)
{
// if($this->input->is_ajax_request()){
  if ($tabel == 'k') {
// $this->db->query("REFRESH MATERIALIZED VIEW CONCURRENTLY siswa_lastkerja_repot");
    $list = $this->lapbkk_kerja_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $krj = $this->db->query("SELECT a.posisi, a.mulai_kerja, a.range_gaji, a.linear, a.perjanjian_kerja, b.nama, c.nama_bidang_usaha, d.prov from kerja_siswa a left join ref.peserta_didik b on a.nisn=b.nisn left join ref.bidang_usaha c on a.kode_bidang=c.bidang_usaha_id left join ref.view_provinsi d on a.kode_prov = d.kode_prov WHERE a.nisn='$rowx->nisn'")->row();
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->tahun_keluar;
      $row[] = $rowx->nisn;
      $row[] = $krj->nama;
      $row[] = $rowx->perusahaan;
      $row[] = $krj->posisi;
      $row[] = $krj->mulai_kerja;
      $row[] = $krj->range_gaji;
      $row[] = $krj->nama_bidang_usaha;
      $row[] = $krj->prov;
      switch ($krj->linear) {
        case '1':
        $row[] = 'Linear';
        break;
        case '0':
        $row[] = 'Tidak';
        break;
        default:
        $row[] = '-';
        break;
      }
      switch ($krj->perjanjian_kerja) {
        case '1':
        $row[] = 'Kontrak';
        break;
        case '0':
        $row[] = 'Tetap';
        break;
        default:
        $row[] = '-';
        break;
      }
      // $row[] = $rowx->nama_jurusan_sp;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->lapbkk_kerja_model->count_all(),
      "recordsFiltered" => $this->lapbkk_kerja_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }
  if ($tabel == 'ku') {
// $this->db->query("REFRESH MATERIALIZED VIEW CONCURRENTLY siswa_lastkul_repot");
    $list = $this->lapbkk_kuliah_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $kl = $this->db->query("SELECT * FROM kuliah_siswa WHERE nisn='$rowx->nisn'")->row();
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->tahun_keluar;
      $row[] = $rowx->nisn;
      $row[] = $rowx->nama;
      $row[] = $rowx->nama_pt;
      $row[] = $kl->jurusan;
      $row[] = $rowx->nama_jurusan_sp;
      $row[] = $kl->mulai_kuliah;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->lapbkk_kuliah_model->count_all(),
      "recordsFiltered" => $this->lapbkk_kuliah_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }if ($tabel == 'w') {
    $list = $this->lapbkk_wira_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $wr = $this->db->query("SELECT * FROM wira_siswa WHERE nisn='$rowx->nisn'")->row();
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->tahun_keluar;
      $row[] = $wr->nisn;
      $row[] = $rowx->nama;
      $row[] = $rowx->nama_jurusan_sp;
      $row[] = $rowx->perusahaan;
      $row[] = $wr->jenis_usaha;
      $row[] = $wr->mulai_usaha;
      $row[] = $wr->pendapatan;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->lapbkk_wira_model->count_all(),
      "recordsFiltered" => $this->lapbkk_wira_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }if ($tabel == 'mitra') {
    $list = $this->lapbkk_mitra_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->nama;
      $row[] = $rowx->alamat;
      $row[] = $rowx->nama_bidang_usaha;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->lapbkk_mitra_model->count_all(),
      "recordsFiltered" => $this->lapbkk_mitra_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }
}

function expLap(){
  $st = $this->input->post('stt');
  if ($st=='1') {
    $data['response'] = 'kerja';
  }else if ($st=='2') {
    $data['response'] = 'kulyah';
  }else if ($st=='3') {
    $data['response'] = 'wiras';
  }else{
    $data['response'] = 'ups';
  }
}

function dataDisnaker(){
  $bulan = $this->input->post('bulan');
  $tahun = $this->input->post('tahunNaker');
  $this->load->library('pdf');
  $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetTitle('Laporan Disnaker');
            $pdf->SetHeaderMargin(30);
            $pdf->SetTopMargin(20);
            $pdf->setFooterMargin(20);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('NAA');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->AddPage();
            $i=0;
            $sid = $this->session->userdata('role_');
            $nm = $this->session->userdata('nama_');
            $bll = substr($bulan, 6,2);
            $blal = $bll-1;
            $skl = $this->db->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$sid'")->row();
            $bl1 = $this->db->query("SELECT count(peserta_didik_id) as bulan_ini FROM siswa_history_all WHERE left(tgl_history::text, 4) = '$tahun' AND substring(tgl_history::text, 6,2) = '$bulan' AND tipe_history='4'")->row();
            $lk1 = $this->db->query("SELECT count(b.peserta_didik_id) as laki FROM siswa_history_all a left join ref.peserta_didik b on a.peserta_didik_id = b.peserta_didik_id WHERE b.jenis_kelamin = 'L' AND left(a.tgl_history::text, 4) = '$tahun' AND substring(a.tgl_history::text, 6,2) = '$bulan' AND a.tipe_history='4'")->row();
            $pr1 = $this->db->query("SELECT count(b.peserta_didik_id) as cewe FROM siswa_history_all a left join ref.peserta_didik b on a.peserta_didik_id = b.peserta_didik_id WHERE b.jenis_kelamin = 'P' AND left(a.tgl_history::text, 4) = '$tahun' AND substring(a.tgl_history::text, 6,2) = '$bulan' AND a.tipe_history='4'")->row();
            switch ($bulan) {
            case '01':
            $bulan = "Januari";
            break;
            case '02':
            $bulan = "Februari";
            break;
            case '03':
            $bulan = "Maret";
            break;
            case '04':
            $bulan = "April";
            break;
            case '05':
            $bulan = "Mei";
            break;
            case '06':
            $bulan = "Juni";
            break;
            case '07':
            $bulan = "Juli";
            break;
            case '08':
            $bulan = "Agustus";
            break;
            case '09':
            $bulan = "September";
            break;
            case '10':
            $bulan = "Oktober";
            break;
            case '11':
            $bulan = "Nopember";
            break;
            case '12':
            $bulan = "Desember";
            break;
            default:
            "Belum dipilih";
            break;
            }
            $html='<h3 style="text-align: center;">LAPORAN PENEMPATAN TENAGA KERJA</h3><br>';
                  $html.='<table>
                  <tr>
                  <th style="width: 115px; text-align: left;">Nama Bursa Kerja</th>
                  <th style="width: 5px; text-align: right;">:</th>
                  <th style="text-align: left;"> ';
                  $html.= $nm;
                  $html.='</th>
                  </tr>
                  <tr>
                  <th style="width: 115px; text-align: left;">Alamat</th>
                  <th style="width: 5px; text-align: right;">:</th>
                  <th style="text-align: left;"> ';
                  $html .= $skl->alamat_jalan;
                  $html.='</th>
                  </tr>
                  <tr>
                  <th style="width: 115px; text-align: left;">Untuk Bulan</th>
                  <th style="width: 5px; text-align: right;">:</th>
                  <th style="text-align: left;"> ';
                  $html .=$bulan.', '.$tahun;
                  $html .='</th>
                  </tr>
                  </table>
                    <table cellspacing="1" bgcolor="#666666" cellpadding="2" style="width: 100%; font-size: 8px;">
                        <tr bgcolor="#ffffff">
                            <th width="5%" align="center" rowspan="3">No</th>
                            <th width="20%" align="center" rowspan="3">PENCARI KERJA (ALUMNI/LULUSAN), LOWONGAN DAN PENEMPATAN</th>
                            <th align="center" rowspan="2" colspan="2">SATUAN PENDIDIKAN</th>
                            <th align="center" colspan="2">PERGURUAN TINGGI</th>
                            <th width="10%" align="center" rowspan="3">LEMBAGA PELATIHAN</th>
                            <th width="5%" align="center" rowspan="3">L</th>
                            <th width="5%" align="center" rowspan="3">P</th>
                            <th width="5%" align="center" rowspan="3">JML</th>
                            <th align="center" rowspan="3">KET</th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center" colspan="2">FAKULTAS</th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">SMU</th>
                            <th align="center">SMK</th>
                            <th align="center">Eksak</th>
                            <th align="center">Non Eksak</th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">I.</th>
                            <th align="center">PENCARI KERJA</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">1.<br><br>2.</th>
                            <th>Pencari Kerja yang belum ditempatkan akhir bulan lalu<br>Pencaker yang terdaftar bulan ini</th>
                            <th align="center"></th>
                            <th align="center">';
                            $html.= "a";
                            $html.='<br><br>';
                            $html.=$bl1->bulan_ini;
                            $html.='</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center">a<br><br>';
                            $html.=$lk1->laki;
                            $html.='</th>
                            <th align="center">a<br><br>';
                            $html.=$pr1->cewe;
                            $html.='</th>
                            <th align="center">a<br><br>';
                            $html.=$pr1->cewe+$lk1->laki;
                            $html.='</th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">A.</th>
                            <th align="center">JUMLAH (1+2)</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">3.<br><br>4.<br><br>5.<br><br>6.</th>
                            <th>Pencaker yang dikirim ke pengguna<br>Pencaker yang ditempatkan bulan ini<br>Pencaker yang berusaha mandiri<br>Pencaker yang dihapuskan bulan ini</th>
                            <th align="center"></th>
                            <th align="center">a<br><br>b<br><br>c<br><br>d</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center">a<br><br>b<br><br>c<br><br>d</th>
                            <th align="center">a<br><br>b<br><br>c<br><br>d</th>
                            <th align="center">a<br><br>b<br><br>c<br><br>d</th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">B.</th>
                            <th align="center">JUMLAH (3+4+5)</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">7.</th>
                            <th>Pencaker yang belum ditempatkan pada akhir bulan ini (A-B)</th>
                            <th align="center"></th>
                            <th align="center">a</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center">a</th>
                            <th align="center">a</th>
                            <th align="center">a</th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">II.</th>
                            <th align="center">LOWONGAN KERJA</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center" colspan="5">SEKTOR LAPANGAN USAHA*</th>
                            <th width="6%" align="center">L</th>
                            <th width="6%" align="center">P</th>
                            <th width="6%" align="center">JML</th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center" rowspan="2">1.<br><br><br>2.</th>
                            <th rowspan="2">Lowongan yang belum ditempatkan pada akhir bulan lalu<br>Lowongan yang terdaftar bulan ini</th>
                            <th width="4.5%" align="center">1</th>
                            <th width="4.5%" align="center">2</th>
                            <th width="4.5%" align="center">3</th>
                            <th width="4.5%" align="center">4</th>
                            <th width="4.5%" align="center">5</th>
                            <th width="4.5%" align="center">6</th>
                            <th width="4.5%" align="center">7</th>
                            <th width="4.5%" align="center">8</th>
                            <th width="4.5%" align="center">9</th>
                            <th width="4.9%" align="center">10</th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.9%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">A.</th>
                            <th align="center">JUMLAH (1+2)</th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.9%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">3.<br><br>4.</th>
                            <th>Lowongan yang dipenuhi bulan ini<br>Lowongan yang dihapuskan dalam bulan ini</th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.9%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">B.</th>
                            <th align="center">JUMLAH (3+4)</th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.9%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <th align="center">5.</th>
                            <th>Lowongan yang belum dipenuhi akhir bulan ini (A-B)</th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.5%" align="center"></th>
                            <th width="4.9%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6%" align="center"></th>
                            <th width="6.1%" align="center"></th>
                        </tr>';
            $html.='</table>';
            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->Output('laporan.pdf', 'I');
      //       if () {
      //         $resp = "ok";
      // }   else {
      //   $resp = "no";
      // }
      //       echo json_encode($resp);
}
}