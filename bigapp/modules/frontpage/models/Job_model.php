<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Job_model extends CI_Model {

    function fetch_data($limit, $start){
      $this->db->select("*");
      $this->db->from("mview_lowongan_frontpage");
      $this->db->where('tgl_tutup<',date('Y-m-d'));
      $this->db->where('tujuan','9');
      $this->db->limit($limit, $start);
      $query = $this->db->get();
      return $query;
  }
}