<section class="footer-Content">
	<h6></h6>
	<div class="container" style="border-bottom:1px solid rgba(255, 255, 255, 0.1);padding-bottom:25px">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<span style="vertical-align:top;"><img src="<?php echo base_url();?>assets/frontpage/img/tut-wuri.png" alt="logo" style="height:64px"></span>
				<span style="display:inline-block;padding-left:15px">
					DIREKTORAT PEMBINAAN<br/>SEKOLAH MENENGAH KEJURUAN<br/>
					<i class="fa fa-map-marker"></i> &nbsp;&nbsp;<span style="font-size:12px;line-height:1.5em;font-weight:normal">Komplek Kementerian Pendidikan dan Kebudayaan,</span><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12px;line-height:1.5em;font-weight:normal">Gedung E, Lantai 13</span><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12px;line-height:1.5em;font-weight:normal">Jl. Jend Sudirman, Senayan, Jakarta 10270</span><br/><i class="fa fa-phone"></i> <span style="font-size:12px;line-height:1.5em;font-weight:normal"> 021-5725477 (hunting)</span>
				</span>

			</div>	
			<div class="col-md-3">
				<ul>
					<li><i class="fa fa-facebook-square"></i>&nbsp;<a style="color:white" href="https://www.facebook.com/DirektoratPSMK" rel="nofollow">DITPSMK</a></li>
					<li><i class="fa fa-twitter"></i>&nbsp;<a style="color:white" href="https://twitter.com/ditpsmk" rel="nofollow">@ditpsmk</a></li>
					<li><i class="fa fa-instagram" aria-hidden="true"></i>&nbsp;<a  style="color:white" href="http://instagram.com/ditpsmk">@ditpsmk</a></li>
					<li><i class="fa fa-youtube-play"></i>&nbsp;<a style="color:white" href="https://www.youtube.com/user/ditpsmk" rel="nofollow">ditpsmk</a></li>
					<li><i class="fa fa-envelope"></i>&nbsp;<a style="color:white" href="mailto:subdit.lasjurin@kemdikbud.go.id" 
rel="nofollow">subdit.lasjurin@kemdikbud.go.id</a></li>
				</ul>
			</div>

			<div class="col-md-3">
				<img class="img-responsive" src="<?php echo base_url();?>assets/frontpage/img/smk-bisa-smk-hebat.png" alt="http://psmk.kemdikbud.go.id/img/smk-bisa-smk-hebat.png" style="height:163px;width:263px">
			</div>
		</div> 
	</div>



	<!-- Copyright Start  -->
	<div id="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<p style="font-size:9px;line-height:1.5em">&copy; <?php echo date('Y');?> Direktorat Pembinaan Sekolah Menengah Kejuruan
							<br/>Kementerian Pendidikan dan Kebudayaan Republik Indonesia </p>
						</div>  
						<?php 
						$count_my_page = ("hitcounter.txt"); 
						$hits[0] = "";
						$hits = file($count_my_page); 
						if(!$this->session->userdata('current_user')){         
							$hits[0] ++; 
							$fp = fopen($count_my_page , "w"); 
							fputs($fp , "$hits[0]"); 
							fclose($fp); 

						} 
						?>

						<div class="col-md-6">
							<p style="font-size:9px;text-align:right;line-height:1.5em">Page rendered in <strong>{elapsed_time}</strong> seconds.<br/>
								Total pengunjung : 
								<?php
								echo number_format($hits[0]);
								?>
							</p>
						</div>  

					</div>
				</div>
			</div>
		</div>
		<!-- Copyright End -->

	</section>
