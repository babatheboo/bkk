
<div id="isi">
	<?php
		$t1 = date('Y')-2;
		$t2 = date('Y')-1;
		$t3 = date('Y');

	if($this->session->userdata('login')==FALSE) {
		?>

		<div style="margin-top:25px">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<h5>Penyalur kerja terbanyak</h5>
						<div> 
							<table>
								<thead>
									<tr>
										<th >No</th>
										<th >Nama</th>
										<th ><?php echo $t1;?> </th>
										<th ><?php echo $t1+1;?></th>
										<th ><?php echo $t1+2;?></th> 
										<th >Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$rank = $this->db->query("select nama_sekolah,sekolah_id, sum(total_bekerja) as total from mview_rank_tahun where tahun >='$t1' and tahun <='$t3' group by nama_sekolah, sekolah_id order by total desc limit 5")->result();
									$n = 0;
									if(count($rank)>0){
										foreach ($rank as $key ) {
											$n = $n+1;
											$total = 0;
											echo "<tr ". 'style="height:5px"' ."><td>" . $n . "</td><td>". $key->nama_sekolah . "</td>" ;
											$th1 = $this->db->query("select total_bekerja from mview_rank_tahun where sekolah_id='$key->sekolah_id' AND tahun='$t1' LIMIT 1")->result();
											if(count($th1)>0){
												foreach ($th1 as $key1) {
													echo "<td>" . $key1->total_bekerja . "</td>";
													$total = $total + $key1->total_bekerja;
												}
											} else {
												echo "<td>0</td>";
											}

											$th2 = $this->db->query("select total_bekerja from mview_rank_tahun where sekolah_id='$key->sekolah_id' AND tahun='$t2' LIMIT 1")->result();
											if(count($th2)>0){
												foreach ($th2 as $key2) {
													echo "<td>" . $key2->total_bekerja . "</td>";
													$total = $total + $key2->total_bekerja;
												}
											} else {
												echo "<td>0</td>";
											}

											$th3 = $this->db->query("SELECT total_bekerja FROM mview_rank_tahun WHERE sekolah_id='$key->sekolah_id' AND tahun='$t3' ORDER BY total_bekerja DESC LIMIT 1")->result();
											if(count($th3)>0){
												foreach ($th3 as $key3) {
													echo "<td>" . $key3->total_bekerja . "</td>";
													$total = $total + $key3->total_bekerja;
												}
											} else {
												echo "<td>0</td>";
											}
											echo "<td style='text-align:right'>" . number_format($total) . "</td>";
											echo "</tr>";
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-md-4">
						<h5>Mitra Industri terbanyak</h5>
						<div>
							<table >
								<thead>
									<tr>
										<th >No</th>
										<th >Nama</th>
										<th ><?php echo $t1;?> </th>
										<th ><?php echo $t1+1;?></th>
										<th><?php echo $t1+2;?></th> 
										<th>Total</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$sq = "SELECT nama, sekolah_id, sum(total) as total from mview_mitra_tahun  where tahun >='$t1' and tahun<='$t3' group by nama, sekolah_id order by total desc limit 5";
									$ranka = $this->db->query($sq)->result();
									$n = 0;
									if(count($ranka)>0){
										foreach ($ranka as $key ) {
											$n = $n+1;
											$total = 0;
											echo "<tr><td>" . $n . "</td><td>". $key->nama . "</td>" ;
											$th1a = $this->db->query("SELECT sum(total) as total from mview_mitra_tahun where tahun='$t2' and sekolah_id='$key->sekolah_id' ORDER BY total DESC")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}
													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}

											$th1a = $this->db->query("SELECT sum(total) as total from mview_mitra_tahun where tahun='$t3' and sekolah_id='$key->sekolah_id' ORDER BY total DESC")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}
													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}

											$th1a = $this->db->query("SELECT sum(total) as total from mview_mitra_tahun where tahun='$t3' and sekolah_id='$key->sekolah_id' ORDER BY total DESC")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}

													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}
											echo "<td style='text-align:right'>" . number_format($total) . "</td>";
											echo "</tr>";
										}
									}
									?>            
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-md-4">
						<h5>Lowongan terbanyak</h5>
						<div class="table-responsive">
							<table class="blueTable">
								<thead><tr>
									<th >No</th>
									<th >Nama</th>
									<th ><?php echo $t1;?> </th>
									<th ><?php echo $t1+1;?></th>
									<th><?php echo $t1+2;?></th> 
									<th>Total</th></tr>
								</thead>
								<tbody>
									<?php
									$sq = "SELECT nama_sekolah as nama, sekolah_id, sum(total_loker) as total from mview_loker_tahun group by nama_sekolah, sekolah_id order by total desc limit 5";

									$ranka = $this->db->query($sq)->result();
									$n = 0;
									if(count($ranka)>0){
										foreach ($ranka as $key ) {
											$n = $n+1;
											$total = 0;
											echo "<tr><td>" . $n . "</td><td>". $key->nama . "</td>" ;
											$th1a = $this->db->query("select sum(total_loker) as total from mview_loker_tahun where tahun='$t1' and sekolah_id='$key->sekolah_id'")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}
													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}

											$th1a = $this->db->query("select sum(total_loker) as total from mview_loker_tahun where tahun='$t2' and sekolah_id='$key->sekolah_id'")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}
													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}

											$th1a = $this->db->query("select sum(total_loker) as total from mview_loker_tahun where tahun='$t3' and sekolah_id='$key->sekolah_id'")->result();
											if(count($th1a)>0){
												foreach ($th1a as $key1) {
													if(!empty($key1->total)){
														echo "<td>" . $key1->total . "</td>";
													} else {
														echo "<td>0</td>";
													}

													$total = $total + $key1->total;
												}
											} else {
												echo "<td>0</td>";
											}
											echo "<td style='text-align:right'>" . number_format($total) . "</td>";
											echo "</tr>";
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php
	}?>

	<br/>

	<?php
	$lpsmk = $this->db->query("SELECT * FROM lowongan_psmk WHERE  tgl_tutup>=now() AND status='1' order BY tgl_tutup ")->result();
	if(count($lpsmk)>0){
		?>
		<section class="section">
			<div class="container">
				<h2 class="section-title">Informasi dari PSMK</h2>
				<div class="row">
					<?php

					foreach ($lpsmk as $key) {
  # code...
						?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="featured-item">
								<div class="featured-wrap">
									<div class="featured-inner">
										<figure class="item-thumb">
											<!-- <a href="<?php echo base_url();?>assets/foto/iklan/<?php echo $key->gambar;?>"> -->
												<img class="thumbnail" src="<?php echo base_url();?>assets/foto/iklan/<?php echo $key->gambar;?>" alt="" style="width:200px;200px" onclick='bukakeun("<?php echo $key->gambar;?>");'>
												<!-- </a> -->
											</figure>
											<div class="item-body">
												<h3 class="job-title"><a href="javascript:bukakeun('<?php echo $key->gambar;?>')"><?php echo $key->judul;?></a></h3>
												<div class="adderess" onclick='bukakeun("<?php echo $key->gambar;?>");'><?php echo $key->deskripsi;?></div>
											</div>
										</div>
									</div>
									<div class="item-foot">
										<span><i class="ti-briefcase"></i> Perusahaan : <?php echo $key->perusahaan;?></span><br/>
										<span><i class="ti-time"></i> Berakhir pada : <?php echo date('d-m-Y', strtotime($key->tgl_tutup));?></span>

									</div>
								</div>
							</div>
							<?php
						}
						?>


					</div>
				</div>
			</section>
			<?php 
		}
		?>


		<?php
		$sid = $this->session->userdata('role_');
		$low = $this->db->query("SELECT id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup, logo, nama_bidang_usaha, nama FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() and tujuan='$sid' order BY tgl_tutup")->result();
		if(count($low)>0) {
   // $newDate = "";
			foreach ($low as $key) {
				?>
				<section class="find-job section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h5 class="section-title">Lowongan khusus alumni <?php echo $this->session->userdata('nama_');?></h5>      

								<div class="job-list col-md-12">
									<div class="thumb">
										<a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $key->logo;?>" alt="" style="width:100px;height:100px"></a>
									</div>
									<div class="job-list-content">
										<h4><a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>"><?php echo $key->judul;?></a></h4>
										<p><?php echo substr($key->deskripsi, 0, 50) . " ....." ; ?></p>
										<div class="job-tag">
											<div class="pull-left">
												<div class="meta-tag">
													<span>Bidang Usaha : <a href="<?php echo base_url();?>frombkk/bidang/<?php echo $key->bidang_usaha_id;?>"><?php echo $key->nama_bidang_usaha;?></a></span><br/>
													<span>Diposting BKK : <a href="<?php echo base_url();?>frombkk/bidang/<?php echo $key->sekolah_pembuat;?>"><?php echo $key->nama;?></a></span><br/>
													<span><i class="ti-time"></i>Lamaran ditutup pada : <?php echo date("d-m-Y", strtotime($key->tgl_tutup));?></span>
												</div>
											</div>
											<div class="pull-right">
												<a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>" class="btn btn-common btn-rm">Lihat</a>
												<a href="javascript:void(0)" onclick="lamar('<?php echo $key->id;?>')" class="btn btn-common">Lamar</a>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>
				<?php 
			}
		}
		?>

		<section class="find-job section">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2 class="section-title">Lowongan dari BKK</h2>   
						<div id="load_lowbkk"></div>
						<div id="load_data_message"></div>
					</div>

					<div class="col-md-6">
						<!-- 
						<h2 class="section-title">Lowongan dari TOP Karir</h2>
						<?php
						$curl = curl_init();
						curl_setopt_array($curl, array(
							CURLOPT_HEADER => false,
							CURLOPT_URL => "https://smk:smkxtkid@www.topkarir.com/api/jobs/kerjasmk",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "GET",
							CURLOPT_HTTPHEADER => array(
								"X-API-KEY: tkidxsmk",
								"Accept: application/json"
							),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						$hasil = json_decode($response,true);
						if($hasil){


							foreach ($hasil as $key => $value) {
								?>
								<div class="job-list">
									<div class="thumb">
										<img src="<?php echo $value['url_logo'];?>" alt="" style="width:100px;height:100px">
									</div>
									<div class="job-list-content">
										<h4><a href="https://smk.topkarir.com/lowongan/detil/<?php echo $value['permalink'];?>" target="_blank"><?php echo $value['job_title'];?></a></h4>
										<p><?php echo $value['job_post_intro'];?></p>
										<div class="job-tag">
											<div class="pull-left">
												<div class="meta-tag">
													<span>Perusahaan :<i class="ti-location-pin"></i><?php echo $value['company_name'];?></span><br>
													<span>Lokasi : <?php echo $value['kota'];?></span><br>
													<span><i class="ti-time"></i>Lamaran ditutup pada : <?php echo date("d-m-Y", strtotime($value['end']));?></span> 
												</div>
											</div>
											<div class="pull-right"><a href="https://smk.topkarir.com/lowongan/detil/<?php echo $value['permalink'];?>"  class="btn btn-common btn-rm" target="_blank">Lihat</a>
											</div>
										</div>
									</div>
								</div>
								<?php
							}
						}
						?> -->
					</div>
				</div>
			</div>
		</section>
		<!-- Find Job Section End -->
	</div>
	<div class="modal fade" id="modalna" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <img id="fotona" style="width: 100%;">
        </div>
      </div>
      
    </div>
  </div>
	<script>
		function bukakeun(fotona){
			$("#fotona").attr("src", "assets/foto/iklan/" + fotona);
			$("#modalna").modal('show');
		}
	</script>