<!DOCTYPE html>
<html lang="id">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141826192-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-141826192-1');
</script>

  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">    
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="author" content="bkk">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Expires" content="0">
  <title>Bursa Kerja Khusus</title>    
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontpage/img/favicon.ico">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/front.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/fonts/font.css" type="text/css"> 
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/animate.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/main.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/imgzoom.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/jquery-ui.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/colors/blue.css" media="screen" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/custom.css" media="screen" />
  <script  src="<?php echo base_url();?>assets/frontpage/js/jquery-3.3.1.min.js"></script>
</head>
<body>  
  <div class="header">   
    <?php $this->load->view('header');?>
  </div>    
  <?php $this->load->view($content);?>
  <footer>
   <?php $this->load->view('footer');?>
 </footer>
 <div id="cso" style="position:fixed;bottom:0;right:0;margin-top: -2.5em;text-align:center">
  <a href="javascript:void(0)" onclick="helpdesk()"><img src="<?php echo base_url();?>assets/img/help_desk.png" alt="helpdesk" 
style="width:80px;height:80px"></a> 
</div>
<div id="loading">
  <div id="loading-center">
    <div id="loading-center-absolute">
      <div class="object" id="object_one"></div>
      <div class="object" id="object_two"></div>
      <div class="object" id="object_three"></div>
      <div class="object" id="object_four"></div>
      <div class="object" id="object_five"></div>
      <div class="object" id="object_six"></div>
      <div class="object" id="object_seven"></div>
      <div class="object" id="object_eight"></div>
    </div>
  </div>
</div>
    
<script  src="<?php echo base_url();?>assets/frontpage/js/jquery-ui.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/jquery.parallax.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/jquery.slicknav.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.revolution.min.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.tools.min.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/bootstrap.min.js"></script>    
<script  src="<?php echo base_url();?>assets/frontpage/js/bootbox.min.js"></script>    
<script  src="<?php echo base_url();?>assets/frontpage/js/bootstrap-notify.min.js"></script>    
<script  src="<?php echo base_url();?>assets/frontpage/js/material.min.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/material-kit.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/main.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/jasny-bootstrap.min.js"></script>
<script  src="<?php echo base_url();?>assets/frontpage/js/bootstrap-select.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery.blockUI.js"></script> -->
<?php
if($menu=="frontpage"){
  ?>
  <script>
   $(document).ready(function(){
    var limit = 5;
    var start = 0;
    var action = 'inactive';
    function loadlow(limit) {
      var output = "";
      for(var count=0; count<limit; count++){
        output += '<div class="post_data">';
        output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
        output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
        output += '</div>';
      }
      $('#load_data_message').html(output);
    }
    loadlow(limit);
    function loadMoreData(limit, start){
	$("#nomore").remove();
     $.ajax({
      url:"<?php echo base_url(); ?>frontpage/loadMore",
      method:"POST",
      data:{limit:limit, start:start},
      cache: false,
      success:function(data){
        if(data == ''){
          $('#load_data_message').html('<h3>Tidak ada data lagi</h3>');
          action = 'active';
        }else{
          $('#load_lowbkk').append(data);
          $('#load_data_message').html("");
          action = 'inactive';
        }
      }
    })
   }
   if(action == 'inactive'){
    action = 'active';
    loadMoreData(limit, start);
  }
  $(window).scroll(function(){
    if($(window).scrollTop() + $(window).height() > $("#load_lowbkk").height() && action == 'inactive')
    {
      loadlow(limit);
      action = 'active';
      start = start + limit;
      setTimeout(function(){
        loadMoreData(limit, start);
      }, 1000);
    }
  });
});
</script>
<?php 
}
?>
<script>
  function lamar(id){
    var r = confirm("Yakin melamar lowongan ini ?");
    if(r == true ){
      $.ajax({
       url:"<?php echo base_url(); ?>frombkk/lamar",
       method:"POST",
       data:{id:id},
       cache: false,
       success:function(data){
        resp = $.parseJSON(data);
        if(resp=="ok"){
          $.notify({icon:'fa fa-check',message:"Lamaran berhasil terkirim! <a href='<?php echo base_url();?>profile/lamaran'> Klik untuk melihat daftar lamaran</a>"},{type:"success",offset:{x:3,y:66}});
        }else if (resp=='dobel') {
          $.notify({icon:'fa fa-info',message:"Anda sudah melamar pekerjaan ini! <a href='<?php echo base_url();?>profile/lamaran'> Klik untuk melihat daftar lamaran</a>"},{type:"danger",offset:{x:3,y:66}});
        }else{
          alert("Lowongan gagal dimasukan !");
          location.reload();
        }
      }
    });
    }
  }
</script>
<?php
if($menu=="sebaran"){
  ?>
  <script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
  <script>
    function cek_prov(id,link,cek_action){
      $.ajax({
        url :'<?php echo base_url();?>sebaran/cek_prov/'+$('#'+id).val(),
        dataType : 'json',
        type : 'post',
        success : function(json) {
          if(json.say != '') {
            $('#kota').empty();
            $('#kota').append(json.say);
            $('#form_kota').show();
            $('#tombol').hide();
            return true;
          }else{
            return false;
          }

        }
      });  
    }
    var table;
    $(function() {
      table = $('#data-sebaran').DataTable({ 
        "processing": true,
        "serverSide": true,
        "pageLength": 20,
        "pagingType": "full_numbers",
        "searching": false,
'language': {
            'loadingRecords': '&nbsp;',
            'processing': '<div class="spinner"></div>'
        }   ,
        "ajax": {
          "url": "<?php echo base_url();?>sebaran/getData/",
          "type": "POST",
          "data": function ( data ) {
            data.jurusan = $('#jurusan').val();
            data.provinsi = $('#provinsi').val();
            data.id_kotax = $('#id_kotax').val();
          }
        },
        'columnDefs': [{
          'targets': 0,
          'searchable': false,
          'orderable': false,
          'width':'1%',
          'className': 'text-center'
        }],
        'order': [[1, 'asc']]
      });
      $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
      });
      jQuery("#id_kotax").change(function(){
        table.ajax.reload(null,false); 
      });
      jQuery("#jurusan").change(function(){
        table.ajax.reload(null,false); 
      });
      jQuery("#provinsi").change(function(){
        table.ajax.reload(null,false); 
      });
    });
    function reload_table(){
      table.ajax.reload(null,false);
    }
  </script>
  <?php
} 
if($menu=="profile"){
  ?>
  <script  src="<?php echo base_url();?>assets/frontpage/js/profile.js"></script>    
  <?php   
}
?>

<script>
      dialog = $( "#dialog" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true
    });


    function play(){
       dialog.dialog( "open" );
    }
</script>
</body>
</html>
