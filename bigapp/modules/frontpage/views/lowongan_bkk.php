<?php
$this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
$low = $this->db->query("SELECT * FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() order BY tgl_tutup  limit 5")->result();
if(count($low)>0){
  $n=0;
  foreach ($low as $key) {
    $n = $n+1;
    ?>
    <div class="job-list" id="<?php echo $n;?>">
      <div class="thumb">
        <a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $key->logo;?>" alt="" style="width:100px;height:100px"></a>
      </div>
      <div class="job-list-content">
        <h4><a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>"><?php echo $key->judul;?></a></h4>
        <p><?php echo substr($key->deskripsi, 0, 50) . "....." ;?></p>
        <div class="job-tag">
          <div class="pull-left">
            <div class="meta-tag">
              <span>Posisi : </span><br/>
              <span>Bidang Usaha : <a href="<?php echo base_url();?>frombkk/bidang/<?php echo $key->bidang_usaha_id;?>"><?php echo $key->nama_bidang_usaha;?></a></span><br/>
              <span>Diposting BKK :<i class="ti-location-pin"></i><a href="<?php echo base_url();?>frombkk/detil/<?php echo $key->sekolah_pembuat;?>"><?php echo $key->nama;?></a></span><br/>
              <?php
              $newDate = date("d-m-Y", strtotime($key->tgl_tutup));
              ?>
              <span><i class="ti-time"></i>Lamaran ditutup pada : <?php echo $newDate ;?></span>
            </div>
          </div>
          <div class="pull-right">
            <a href="<?php echo base_url();?>frombkk/ldetil/<?php echo $key->id;?>" class="btn btn-common btn-rm">Lihat</a>
            <a href="<?php echo base_url();?>login" class="btn btn-common btn-rm">Lamar</a>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
}
?>
<script type="text/javascript"></script>
