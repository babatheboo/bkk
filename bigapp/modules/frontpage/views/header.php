        <!-- Start intro section -->
        <section id="intro" class="section-intro">
          <div class="logo-menu">
            <?php 
            if($this->session->userdata('login')==FALSE) {
              ?>
              <nav class="navbar navbar-default" data-spy="affix" data-offset-top="50">
                <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  <!-- <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/frontpage/img/tut-wuri.png" alt="logo" style="margin:15px;height:64px;width:64px;vertical-align:top">
<span style="color:white;line-height:1em;display:inline-block;margin-top:25px">DIREKTORAT PEMBINAAN SMK<br/>DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH<br/>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</span>
</a> -->
</div>

<div class="collapse navbar-collapse" id="navbar">
  <!-- Start Navigation List -->
    <div class="col-md-6">
  <a href="<?php echo base_url();?>"><img style="margin-top: 15px;"
    src="<?php echo base_url();?>assets/frontpage/img/tut-wuri.png" alt="logo"></a>
    <p style="margin-left: 75px; margin-top: -65px;">
      DIREKTORAT PEMBINAAN SMK DIREKTORAT JENDERAL PENDIDIKAN DASAR DAN MENENGAH KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN
    </p>
    </div>
    <ul class="nav navbar-nav">
      <li>
        <a <?php if($menu=="frontpage"){ echo 'class="active"';}?>
        href="<?php echo base_url();?>">Beranda</a>
      </li>
      <li>
        <a <?php if($menu=="sebaran"){ echo 'class="active"';}?>
        href="<?php echo base_url();?>sebaran">Sebaran</a>
      </li>
      <li>
        <a href="#">
          Manual <i class="fa fa-angle-down"></i>
        </a>
        <ul class="dropdown">
          <li>
            <a href="<?php echo base_url();?>assets/dokumen/Panduan Penggunaan Admin SMK.pdf" target="_blank">Aplikasi WEB Admin BKK</a>
          </li>
          <li>
            <a href="<?php echo base_url();?>dokumen/manualwebalumni.pdf">Aplikasi WEB Alumni</a>
          </li>
          <li>
            <a href="<?php echo base_url();?>dokumen/manualandroid.pdf">Aplikasi Android</a>
          </li>
        </ul>
      </li>
      <li>
        <a <?php if($menu=="about"){ echo 'class="active"';}?>
        href="<?php echo base_url();?>about">Tentang BKK</a>
      </li>
      <li class="right"><a href="<?php echo base_url();?>login"><i class="ti-lock"></i> Log In</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right float-right">
    </ul>
  </div>
</div>
<!-- Mobile Menu Start -->
<ul class="wpb-mobile-menu">
  <span><a href="<?php echo base_url();?>"><img class="pull-right"
    style="margin-right: 0px; opacity: 0.5; filter: alpha(opacity=50);"
    src="<?php echo base_url();?>assets/frontpage/img/tut-wuri.png" alt="logo"></a></span>
    <li>
      <a class="active" href="<?php echo base_url();?>">Beranda</a>
    </li>
    <li>
      <a href="<?php echo base_url();?>sebaran">Sebaran</a>
    </li>
    <li>
      <a href="#">
        Manual <i class="fa fa-angle-down"></i>
      </a>
      <ul class="dropdown">
        <li>
          <a href="<?php echo base_url();?>dokumen/manualwebadmin.pdf">Alikasi WEB Admin BKK</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>dokumen/manualwebalumni.pdf">Alikasi WEB Alumni</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>dokumen/manualandroid.pdf">Aplikasi Android</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="<?php echo base_url();?>about">Tentang BKK</a>
    </li>
    <li class="right"><a href="<?php echo base_url();?>login"><i class="ti-lock"></i> Log In</a></li>
  </ul>
  <!-- Mobile Menu End -->
</nav>
</div>
<?php if($menu=='frontpage') {
  ?>
  <!-- <div class="row"> -->
<!-- <div class="container">
<div class="col-md-12" >
<div class="content">
    <div class="col-md-2" style="text-align:center; border: 2px solid #00A65A;border-radius: 5px;margin-top:10px;background:#00A65A;margin-right:50px;">
        <p style="text-align:center">BKK Terdaftar<br/>
            <?php
              $jm = $this->db->query("select count(sekolah_id) as jumlah from sekolah_terdaftar where no_izin_bkk<>'' or no_sk_pendirian<>''")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              }
            ?>
        </p>
    </div>
    <div class="col-md-2" style="text-align:center; border: 2px solid #00A65A;border-radius: 5px;margin-top:10px;background:#00A65A;margin-right:50px">
        <p style="text-align:center">Industri Terdaftar<br/>
            <?php
              $jm = $this->db->query("select count(industri_id) as jumlah from ref.industri")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              }
            ?>
        </p>
    </div>
     <div class="col-md-2" style="text-align:center; border: 2px solid #00A65A;border-radius: 5px;margin-top:10px;background:#00A65A;margin-right:50px">
        <p style="text-align:center">Lowongan Tersedia<br/>
            <?php
              $jm = $this->db->query("select count(id) as jumlah from lowongan where sekolah_pembuat is not null")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              }
            ?>
        </p>
    </div>
    <div class="col-md-4 pull-right" style="font-size: 15px; text-align:center; border: 2px solid #00A65A;border-radius: 5px;margin-top:10px;background:#00A65A;">
        <?php
        $bln = date('n');
  if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
  }
        ?>
        <p>Jumlah Lulusan</p>
        <ul>
          <li><?php echo $t1;?> : <?php
              $jm = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t1'")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              } ?></li>
          <li><?php echo $t2;?> : <?php
              $jm = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t2'")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              }
            ?></li>
          <li><?php echo $t3;?> : <?php
              $jm = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t3'")->result();
              foreach ($jm as $key) {
                 echo number_format($key->jumlah);
              }
            ?></li>
        </ul>
    </div>
</div>
</div>
</div> -->

<div class="navmenu navmenu-default navmenu-fixed-left offcanvas"> 
  <!--- Off Canvas Side Menu -->
  <div class="close" data-toggle="offcanvas" data-target=".navmenu">
    <i class="ti-close"></i>
  </div>
  <h3 class="title-menu">Data BKK</h3>
  <ul style="color: black;" class="nav navmenu-nav">
    <li><a href="javascript:void(0)"> BKK Tervalidasi <span class="pull-right"><?php
    $jm = $this->db->query("SELECT count(sekolah_id) as jumlah from sekolah_terdaftar where valid='1'")->result();
    foreach ($jm as $key) {
     echo number_format($key->jumlah);
   }
   ?></span></a>
 </li>
 <li><a href="javascript:void(0)">Industri Terdaftar <span class="pull-right"><?php
 $jm = $this->db->query("select count(industri_id) as jumlah from ref.industri")->result();
 foreach ($jm as $key) {
   echo number_format($key->jumlah);
 }
 ?></span></a></li>
 <li><a href="javascript:void(0)">Lowongan <span class="pull-right"><?php
 $jm = $this->db->query("select count(id) as jumlah from lowongan where sekolah_pembuat is not null")->result();
 foreach ($jm as $key) {
   echo number_format($key->jumlah);
 }
 ?></span></a></li>
 <?php
 $bln = date('n');
 if($bln<7){
  $t1 = date('Y') - 3;
  $t2 = $t1+1;
  $t3 = $t1+2;
}else{
 $t1 = date('Y') - 3;
 $t2 = $t1+1;
 $t3 = $t1+2;
}
?>
<li><a href="javascript:void(0)">Lulusan SMK : <br>
  <?php echo $t1;?> <span class="pull-right"><?php
  $th1 = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t1'")->row();
   echo number_format($th1->jumlah);
   ?></span><br>
 <?php echo $t2;?> <span class="pull-right"><?php
 $th2 = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t2'")->row();
   echo number_format($th2->jumlah);
 ?></span><br>
 <?php echo $t3;?> <span class="pull-right"><?php
 $th3 = $this->db->query("select count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)='$t3'")->row();
   echo "(sementara) " . number_format($th3->jumlah);
 ?></span>
</a></li>
</ul><!--- End Menu -->
</div>
<div class="tbtn wow pulse" id="menu" data-wow-iteration="infinite" data-wow-duration="500ms" data-toggle="offcanvas" data-target=".navmenu">
  <p><i class="ti-info-alt"></i> Informasi</p>
</div>
<!-- </div> -->

<?php
}
} else {
  ?>
  <nav class="navbar navbar-default" data-spy="affix" data-offset-top="50">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php
        $sid = $this->session->userdata('role_');
        $ft = $this->db->query("SELECT logo FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
        $logo = "";
        if(count($ft)>0){
          foreach ($ft as $key) {
            $logo = $key->logo;
            ?>
            <a href="<?php echo base_url();?>">
              <img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $logo;?>" alt="logo"
              style="margin-top:10px;height:50px;width:52px;vertical-align:middle">
              <span
              style="line-height:50px;margin-left:10px;color:white"><?php echo $this->session->userdata('nama_');?></span>
            </a>
            <?php
          } 
        }else {
          ?>
          <a class="navbar-brand logo" href="<?php echo base_url();?>"><img
            src="<?php echo base_url();?>assets/frontpage/img/tut-wuri.png" alt="logo"
            style="height:50px;width:52px"></a>
            <?php
          }
          ?>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
          <!-- Start Navigation List -->
          <ul class="nav navbar-nav">
            <li>
              <a <?php if($menu=="frontpage"){ echo 'class="active"';}?>
              href="<?php echo base_url();?>">Beranda</a>
            </li>
            <li>
              <a <?php if($menu=="profile"){ echo 'class="active"';}?>
              href="<?php echo base_url();?>profile">Profile</a>
            </li>

          </ul>
          <ul class="nav navbar-nav navbar-right float-right">
            <li class="right"><a href="<?php echo base_url();?>login/doLogout"><i class="ti-unlock"></i> Log
            Out</a></li>
          </ul>
        </div>
      </div>
      <!-- Mobile Menu Start -->
      <ul class="wpb-mobile-menu">
        <li>
          <a class="active" href="<?php echo base_url();?>">Beranda</a>
        </li>

        <li class="right"><a href="<?php echo base_url();?>login/doLogout"><i class="ti-unlock"></i> Log Out</a>
        </li>
      </ul>
      <!-- Mobile Menu End -->
    </nav>
  </div>
  <?php
}
?>
<?php if($menu=='frontpage') {
  ?>
  <div class="search-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Temukan pekerjaan yang tepat</h1><br>
          <h2>Jika anda lulusan SMK</h2>
          <div class="content">
            <form action="javascript:cari()" method="post" id="fsearch">
              <div class="row">
                <div class="col-md-11 col-sm-12">
                  <div class="form-group">
                    <input class="form-control" type="text" id="keyword" name="keyword"
                    placeholder="jenis lowongan / keyword / perusahaan">
                    <i class="ti-time"></i>
                  </div>
                </div>
                        <!-- <div class="col-md-4 col-sm-6">
            <div class="form-group">
              <input class="form-control" type="text" id="bkk" name="bkk" placeholder="sekolah / bkk">
              <i class="ti-location-pin"></i>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="search-category-container">
              <label class="styled-select">
                <select class="dropdown-product selectpicker" id="bidang" name="bidang">
                  <option value="">Pilih Bidang Pekerjaan</option>
                  <?php
                    $bid = $this->db->query("SELECT * FROM ref.bidang_usaha")->result();
                    foreach ($bid as $key ) {
                      echo "<option value='". $key->bidang_usaha_id ."'>" . $key->nama_bidang_usaha . "</option>" ;
                    }
                  ?>
                </select>
              </label>
            </div>
          </div> -->
          <div class="col-md-1 col-sm-6">
            <button type="button" class="btn btn-search-icon" onclick="cari()"><i
              class="ti-search"></i></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<?php } ?>
</section>
