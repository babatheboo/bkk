<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Frontpage extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('visitormodel', 'vm');
		$this->load->model('job_model');
	}
	public function index(){
		$this->db->query("REFRESH MATERialized view mview_lowongan_frontpage");
		$this->db->query("REFRESH MATERialized view mview_rank_tahun");
		$this->db->query("REFRESH MATERialized view mview_mitra_tahun");
		if($this->session->userdata('login')==TRUE){
			if($this->session->userdata('level')==3){
				$isi['content'] = "content";
				$isi['menu'] = "frontpage";
				$this->load->view('frontpage/front_view', $isi);
			} else {
				redirect("dashboard","refresh");
			}
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
			if($ip!='127.0.0.1'){
					$ses = session_id();
				$agent = $this->input->user_agent();
				$currentURL = current_url();
				$mod = $this->router->fetch_class();
				$data = array('no_of_visits'=>'1', 'site_sess'=>$ses,'user_agent'=>$agent,'ip_address'=>$ip,'requested_url'=>$currentURL,'referer_page'=>$mod);
				$cek = $this->db->query("SELECT * FROM site_log WHERE site_sess='$ses'")->num_rows();
				if($cek==0){
					$this->db->insert('site_log', $data);
				}
			}
			$site_statics_today = $this->vm->get_site_data_for_today();
			$site_statics_last_week = $this->vm->get_site_data_for_last_week();
			$isi['visits_today'] = isset($site_statics_today['visits']) ? $site_statics_today['visits'] : 0;
			$isi['visits_last_week'] = isset($site_statics_last_week['visits']) ? $site_statics_last_week['visits'] : 0;
			$isi['content'] = "content";
			$isi['menu'] = "frontpage";
			$this->load->view('frontpage/front_view', $isi);
		}
	}
	function get_chart_data() {
		if (isset($_POST)) {
			if (isset($_POST['month']) && strlen($_POST['month']) && isset($_POST['year']) && strlen($_POST['year'])) {
				$month = $_POST['month'];
				$year = $_POST['year'];
				$data = $this->vm->get_chart_data_for_month_year($month, $year);
				if ($data !== NULL) {
					foreach ($data as $value) {
						echo $value->day . "t" . $value->visits . "n";
					}
				} else {
					$timestamp = mktime(0, 0, 0, $month);
					$label = date("F", $timestamp);
					echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' .
					$label . '-' . $year . '"</div>';
				}
			} else if (isset($_POST['month']) && strlen($_POST['month'])) {
				$month = $_POST['month'];
				$data = $this->vm->get_chart_data_for_month_year($month);
				if ($data !== NULL) {
					foreach ($data as $value) {
						echo $value->day . "t" . $value->visits . "n";
					}
				} else {
					$timestamp = mktime(0, 0, 0, $month);
					$label = date("F", $timestamp);
					echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' .
					$label . '"</div>';
				}
			} else if (isset($_POST['year']) && strlen($_POST['year'])) {
				$year = $_POST['year'];
				$data = $this->vm->get_chart_data_for_month_year(0, $year);
				if ($data !== NULL) {
					foreach ($data as $value) {
						echo $value->day . "t" . $value->visits . "n";
					}
				} else {
					echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' .
					$year . '"</div>';
				}
			} else {
				$data = $this->vm->get_chart_data_for_month_year();
				if ($data !== NULL) {
					foreach ($data as $value) {
						echo $value->day . "t" . $value->visits . "n";
					}
				} else {
					echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found!</div>';
				}
			}
		}
	}
	function search(){
		$keyword = $this->input->post('keyword');
		$bidang = $this->input->post("bidang");
		$bkk = strtoupper($this->input->post('bkk'));
	//	 if($keyword!=""){
		if($keyword!=""){
			$hs =  $this->db->query("SELECT DISTINCT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama FROM
				mview_lowongan_frontpage WHERE deskripsi like '%$keyword%' or judul like '%$keyword%' order BY tgl_tutup")->result();
			if (count($hs)>0) {
				$html = '<div id="isi">';
					$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12">';
							foreach ($hs as $key ) {
							$html .= '<div class="job-list" style="width: 100%;"><div class="thumb">';
								$html .= '<a href="';
									$html .= base_url();
									$html .= 'frombkk/detil/';
									$html .= $key->sekolah_pembuat;
									$html .= '"><img style="width: 100px;" src="';
									$html .= base_url();
									$html .= 'assets/foto/bkk/';
									$html .= $key->logo;
								$html .= '" alt="" ></a></div><div class="job-list-content"><h4><a href="';
								$html .= base_url();
								$html .= 'frombkk/ldetil/';
								$html .= $key->id;
								$html .= '">';
								$html .= $key->judul;
							$html .= '</a></h4><p>';
							$html .= substr($key->deskripsi, 0, 50) ;
						$html .= '.....</p><div class="job-tag"><div class="pull-left"><div class="meta-tag"><span>Bidang Usaha : <a href="';
						$html .= base_url();
						$html .= 'frombkk/bidang/';
						$html .= $key->bidang_usaha_id;
						$html .= '">';
						$html .= $key->nama_bidang_usaha;
					$html .= '</a></span><br/><span>Diposting BKK :<i class="ti-location-pin"></i><a href="';
					$html .= base_url();
					$html .= 'frombkk/detil/';
					$html .= $key->sekolah_pembuat;
					$html .= '">';
					$html .= $key->nama;
					$html .= '</a></span><br/>';
					$newDate = date("d-m-Y", strtotime($key->tgl_tutup));
					$html .= '<span><i class="ti-time"></i>Lamaran ditutup pada : ';
					$html .= $newDate ;
					$html .= '</span></div></div><div class="pull-right"><a href="';
					$html .= base_url();
					$html .= 'frombkk/ldetil/';
					$html .=  $key->id;
					$html .= '" class="btn btn-common btn-rm">Lihat</a>&nbsp;<a href="';
					if($this->session->userdata('login')==false){
						$html .= base_url();
						$html .= 'login"';
					} else {
						$html .= 'javascript:void()" onclick="lamar(';
						$html .= "'";
						$html .= $key->id;
						$html .= "')";
						$html .= '"';
					}
					$html .= ' class="btn btn-common btn-rm">Lamar</a></div></div></div></div>';
				}
				$html .= '</div>';
				echo $html;
			} else {
				$html = '<div id="isi">';
				$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12">';
				$html .= '<h2 class="section-title">Hasil tidak ditemukan dengan keyword ';
				$html .= $keyword;
				$html .= '</h2></section></div></div></div>';
				echo $html;
				}
		}
/*
		if($bkk!=""){
			$hs =  $this->db->query("SELECT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama FROM mview_lowongan_frontpage WHERE nama like '%$bkk%' and tgl_tutup>=now() order BY id DESC")->result();
			if (count($hs)>0) {
				$html = '<div id="isi">';
					foreach ($hs as $key ) {
					$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12"><div class="job-list col-md-12"><div class="thumb">';
					$html .= '<a href="';
					$html .= base_url();
					$html .= 'frombkk/detil/';
					$html .= $key->sekolah_pembuat;
					$html .= '"><img src="';
					$html .= base_url();
					$html .= 'assets/foto/bkk/';
					$html .= $key->logo;
					$html .= '" alt="" style="width: 100px; height: 100px;"></a></div><div class="job-list-content"><h4><a href="';
					$html .= base_url();
					$html .= 'frombkk/ldetil/';
					$html .= $key->id;
					$html .= '">';
					$html .= $key->judul;
					$html .= '</a></h4><p>';
					$html .= substr($key->deskripsi, 0, 50) ;
					$html .= '.....</p><div class="job-tag"><div class="pull-left"><div class="meta-tag"><span>Bidang Usaha : <a href="';
					$html .= base_url();
					$html .= 'frombkk/bidang/';
					$html .= $key->bidang_usaha_id;
					$html .= '">';
					$html .= $key->nama_bidang_usaha;
					$html .= '</a></span><br/><span>Diposting BKK :<i class="ti-location-pin"></i><a href="';
					$html .= base_url();
					$html .= 'frombkk/detil/';
					$html .= $key->sekolah_pembuat;
					$html .= '">';
					$html .= $key->nama;
					$html .= '</a></span><br/>';
					$newDate = date("d-m-Y", strtotime($key->tgl_tutup));
					$html .= '<span><i class="ti-time"></i>Lamaran ditutup pada : ';
					$html .= $newDate ;
					$html .= '</span></div></div><div class="pull-right"><a href="';
					$html .= base_url();
					$html .= 'frombkk/ldetil/';
					$html .=  $key->id;
					$html .= '" class="btn btn-common btn-rm">Lihat</a>&nbsp;<a href="';
	if($this->session->userdata('login')==false){
		$html .= base_url();
		$html .= 'login"';
	} else {
		$html .= 'javascript:void()" onclick="lamar(';
		$html .= "'";
		$html .= $key->id;
		$html .= "')";
		$html .= '"';
	}
	$html .= ' class="btn btn-common btn-rm">Lamar</a></div></div></div></div>';
				}
				$html .= '</div>';
				echo $html;
			} else {
				$html = '<div id="isi">';
				$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12">';
				$html .= '<h2 class="section-title">Data tidak ditemukan hasil dengan keyword ';
				$html .= $bkk;
				$html .= '</h2></section></div></div></div></div>';
				echo $html;
				}
		}
			if($bidang!=""){
			$hs =  $this->db->query("SELECT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama FROM mview_lowongan_frontpage WHERE bidang_usaha_id ='$bidang' and tgl_tutup>=now() order BY tgl_tutup")->result();
			if (count($hs)>0) {
				$html = '<div id="isi">';
					foreach ($hs as $key ) {
					$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12"><div class="job-list col-md-12"><div class="thumb">';
					$html .= '<a href="';
					$html .= base_url();
					$html .= 'frombkk/detil/';
					$html .= $key->sekolah_pembuat;
					$html .= '"><img src="';
					$html .= base_url();
					$html .= 'assets/foto/bkk/';
					$html .= $key->logo;
					$html .= '" alt="" style="width:100px;height:100px"></a></div><div class="job-list-content"><h4><a href="';
					$html .= base_url();
					$html .= 'frombkk/ldetil/';
					$html .= $key->id;
					$html .= '">';
					$html .= $key->judul;
					$html .= '</a></h4><p>';
					$html .= substr($key->deskripsi, 0, 50) ;
					$html .= '.....</p><div class="job-tag"><div class="pull-left"><div class="meta-tag"><span>Bidang Usaha : <a href="';
					$html .= base_url();
					$html .= 'frombkk/bidang/';
					$html .= $key->bidang_usaha_id;
					$html .= '">';
					$html .= $key->nama_bidang_usaha;
					$html .= '</a></span><br/><span>Diposting BKK :<i class="ti-location-pin"></i><a href="';
					$html .= base_url();
					$html .= 'frombkk/detil/';
					$html .= $key->sekolah_pembuat;
					$html .= '">';
					$html .= $key->nama;
					$html .= '</a></span><br/>';
					$newDate = date("d-m-Y", strtotime($key->tgl_tutup));
					$html .= '<span><i class="ti-time"></i>Lamaran ditutup pada : ';
					$html .= $newDate ;
					$html .= '</span></div></div><div class="pull-right"><a href="';
					$html .= base_url();
					$html .= 'frombkk/ldetil/';
					$html .=  $key->id;
					$html .= '" class="btn btn-common btn-rm">Lihat</a>&nbsp;<a href="';
	if($this->session->userdata('login')==false){
		$html .= base_url();
		$html .= 'login"';
	} else {
		$html .= 'javascript:void()" onclick="lamar(';
		$html .= "'";
		$html .= $key->id;
		$html .= "')";
		$html .= '"';
	}
	$html .= ' class="btn btn-common btn-rm">Lamar</a></div></div></div></div>';
				}
				$html .= '</div>';
				echo $html;
			} else {
				$html = '<div id="isi">';
				$html .= '<section class="find-job section"><div class="container"><div class="row"><div class="col-md-12">';
				$html .= '<h2 class="section-title">Data tidak ditemukan hasil dengan keyword ';
				$html .= $keyword;
				$html .= '</h2></section></div></div></div></div>';
				echo $html;
				}
		} */
	//  	 }
	}
	function loadMore(){
		$limit  = $this->input->post('limit');
		$start = $this->input->post('start');
		$html = '';
		$low = $this->db->query("SELECT DISTINCT logo,id,judul,deskripsi,bidang_usaha_id,sekolah_pembuat,tgl_tutup,nama_bidang_usaha,nama,posisi FROM mview_lowongan_frontpage WHERE tgl_tutup>=now() order BY tgl_tutup limit $limit offset $start")->result();
		if(count($low)>0){
			foreach ($low as $key) {
				$html .='<div class="job-list"><div class="thumb"><a href="';
				$html .= base_url();
				$html .= 'frombkk/detil/';
				$html .= $key->sekolah_pembuat;
				$html .= '"><img src="';
				$html .= base_url();
				$html .= 'assets/foto/bkk/';
				$html .= $key->logo;
				$html .= '" alt="" style="width:100px;height:100px"></a></div><div class="job-list-content"><h4><a href="';
				$html .= base_url();
				$html .= 'frombkk/ldetil/';
				$html .= $key->id;
				$html .= '">';
				$html .= $key->judul;
				$html .= '</a></h4><p>';
				$html .= substr($key->deskripsi, 0, 50) . " ....." ;
				$html .= '</p><div class="job-tag"><div class="pull-left"><div class="meta-tag"><span>Posisi : '.$key->posisi.'</span><br><span>Bidang Usaha : <a
				href="';
				$html .= base_url();
				$html .= 'frombkk/bidang/';
				$html .= $key->bidang_usaha_id;
				$html .= '">';
				$html .= $key->nama_bidang_usaha;
				$html .= '</a></span><br/><span>Diposting BKK :<i class="ti-location-pin"></i><a href="';
				$html .= base_url();
				$html .= 'frombkk/detil/';
				$html .= $key->sekolah_pembuat;
				$html .= '">';
				$html .= $key->nama;
				$html .= '</a></span><br/>';
				$newDate = date("d-m-Y", strtotime($key->tgl_tutup));
				$html .='<span><i class="ti-time"></i>Lamaran ditutup pada : ';
				$html .= $newDate ;
				$html .= '</span></div></div><div class="pull-right"><a href="';
				$html .= base_url();
				$html .= 'frombkk/ldetil/';
				$html .= $key->id;
				$html .= '" class="btn btn-common btn-rm">Lihat</a>&nbsp;<a href="';
				if($this->session->userdata('login')==false){
					$html .= base_url();
					$html .= 'login"';
				} else {
					$html .= 'javascript:void(0)" onclick="lamar(';
					$html .= "'";
					$html .= $key->id;
					$html .= "')";
					$html .= '"';
				}
				$html .= 'class="btn btn-common btn-rm">Lamar</a></div></div></div></div>';
			}
		}else{
			$html .= '<div id="nomore" class="job-list">Belum ada lowongan baru</div>';
		}
		echo $html;
	}
	}