<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgot extends CI_Controller {
	public function __construct(){		
		parent::__construct();
		$this->load->library('email');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$isi['content'] = 'forgot_view';
		$this->load->view('login/login_view',$isi);
	}


	// function generateRandomString($length = 10) {
 //    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 //    $charactersLength = strlen($characters);
 //    $randomString = '';
 //    for ($i = 0; $i < $length; $i++) {
 //        $randomString .= $characters[rand(0, $charactersLength - 1)];
 //    }
 //    return $randomString;
	// }

	public function do_konfir() {
			$username = $this->input->post('txtunamekon');
			$email = $this->input->post('txtemailkon');
			$jns = $this->input->post('txtjenis');
			if ($jns == '2') {
				$hasil = $this->db->query("SELECT * FROM app.username JOIN ref.peserta_didik ON username.user_id = peserta_didik.peserta_didik_id WHERE username='$username' AND email='$email'")->result();
			$iduser = "";
			if (count($hasil)>0) {
				foreach ($hasil as $key) {
					$iduser = $key->user_id;
					$qr = $this->db1->query("SELECT a.email FROM ref.peserta_didik a LEFT JOIN app.username b ON a.peserta_didik_id=b.user_id WHERE b.username='$username'")->result();
					if (count($qr)>0) {
						foreach ($qr as $kon) {
							if ($email == $kon->email) {
								$x = uniqid();
								$tglenkrip = date("Ymd");
								$tgl =  date("Y-m-d", time()+86400);
								$kode = md5(md5($tglenkrip));
								$dodol = array('pass'=>$kode,'tgl_berakhir'=>$tgl,'status'=>"0",'user_id'=>$iduser, 'kode_x'=>$x);
								if ($this->db->insert('app.tbl_gantipass',$dodol)) {
									$config = array(
										'protocol' => 'sendmail',
										'smtp_host' => 'smtp.gmail.com',
										'smtp_port' => 465,
										'smtp_user' => 'azharnauffal@gmail.com',
										'smtp_pass' => '[babayagatheboogeyman]',
										'smtp_timeout' => '4',
										'mailtype' => 'html',
										'charset' => 'iso-8859-1'
									);
									$this->email->initialize($config);
									$this->email->from('no-reply@bkk.ditpsmk.net', 'Bursa Kerja Khusus | Link ganti password');
									$this->email->to($email);
									$this->email->subject('Link Ganti Password');
									$message = '<html>
									Silahkan klik link berikut ini untuk mengganti password <br/>
									<a href="https://bkk.ditpsmk.net/forgot/gantiPass/' .$kode. '/' .$iduser.'/'.$x.'">Ganti Password</a> <br/>
									Link berlaku sampai dengan tanggal ' .$tgl. ' Terima kasih
									</html>';
									$this->email->message($message);
									$this->email->send();
									$data['response'] = "sip";
								}else{
									$data['response'] = "ups";
								}
								// if('IS_AJAX'){
								// 	echo json_encode($data);
								// }
							}else{
								redirect('login','refresh');
							}
						}
					}else{
						$data['response'] = "ups";
					}
				}
			}else{
				$data['response'] = "ups";
			}
			echo json_encode($data);
			}elseif ($jns == '1') {
				$hasil = $this->db->query("SELECT * FROM app.username JOIN app.user_sekolah ON username.user_id = user_sekolah.user_id WHERE username='$username' AND email='$email'")->result();
			$iduser = "";
			if (count($hasil)>0) {
				foreach ($hasil as $keyz) {
					$iduser = $keyz->user_id;
					$qr = $this->db1->query("SELECT a.email FROM app.user_sekolah a LEFT JOIN app.username b ON a.user_id=b.user_id WHERE b.username='$username'")->result();
					if (count($qr)>0) {
						foreach ($qr as $kon) {
							if ($email == $kon->email) {
								$x = uniqid();
								$tglenkrip = date("Ymd");
								$tgl =  date("Y-m-d", time()+86400);
								$kode = md5(md5($tglenkrip));
								$dodol = array('pass'=>$kode,'tgl_berakhir'=>$tgl,'status'=>"0",'user_id'=>$iduser, 'kode_x'=>$x);
								if ($this->db->insert('app.tbl_gantipass',$dodol)) {
									$config = array(
										'protocol' => 'sendmail',
										'smtp_host' => 'smtp.gmail.com',
										'smtp_port' => 465,
										'smtp_user' => 'azharnauffal@gmail.com',
										'smtp_pass' => '[babayagatheboogeyman]',
										'smtp_timeout' => '4',
										'mailtype' => 'html',
										'charset' => 'iso-8859-1'
									);
									$this->email->initialize($config);
									$this->email->from('no-reply@bkk.ditpsmk.net', 'Bursa Kerja Khusus | Link ganti password');
									$this->email->to($email);
									$this->email->subject('Link Ganti Password');
									$message = '<html>
									Silahkan klik link berikut ini untuk mengganti password <br/>
									<a href="https://bkk.ditpsmk.net/forgot/gantiPass/' .$kode. '/' .$iduser.'/'.$x.'">Ganti Password</a> <br/>
									Link berlaku sampai dengan tanggal ' .$tgl. ' Terima kasih
									</html>';
									$this->email->message($message);
									$this->email->send();
									$data['response'] = "sip";
								}else{
									$data['response'] = "ups";
								}
								// if('IS_AJAX'){
								// 	echo json_encode($data);
								// }
							}else{
								redirect('login','refresh');
							}
						}
					}else{
						$data['response'] = "ups";
					}
				}
			}else{
				$data['response'] = "ups";
			}
			echo json_encode($data);
			}else{
				$data['response'] = "no";
			}
		}

	public function gantiPass($date,$iduser,$kode_x){
		$query = $this->db->query("SELECT * FROM app.tbl_gantipass WHERE user_id='$iduser' AND pass='$date' AND status='0' AND kode_x='$kode_x'")->result();
		$date = date('Y-m-d');
		if (count($query)>0) {
			foreach ($query as $key) {
				if ($date > $key->tgl_berakhir) {
					$gagal['konten'] = 'Link Tidak Berlaku';
					$this->load->view('invalid_view',$gagal);
				}elseif ($key->status=='1') {
					$gagal['konten'] = 'Password Anda sudah diganti';
					$this->load->view('invalid_view',$gagal);
				}else{
					$isi['iduser'] = $iduser;
					$this->load->view('gantipass_view',$isi);
				}
			}
		}else{
			$gagal['konten'] = 'Link Bermasalah';
			$this->load->view('invalid_view',$gagal);
		}
		// echo json_encode($data);
	}
	public function proses_gantipass(){
		// if($this->input->is_ajax_request()){
			$iduser = $this->input->post('iduser');
			$passnew = md5(md5($this->input->post('txtpass2')));
			$pw = array('password'=>$passnew);
			$this->db->where('user_id',$iduser);
			if ($this->db->update('app.username',$pw)){
				$this->db->where('user_id',$iduser);
				$this->db->update('app.tbl_gantipass',array('status'=>'1'));
				$data['response'] = "ok";
			}else{
				$data['response'] = "ko";
			}
			echo json_encode($data);
		// }
	}
}