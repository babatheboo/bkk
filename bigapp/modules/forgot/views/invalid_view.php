<html lang="en"><!--<![endif]--><head>
    <meta charset="utf-8">
    <title>Bursa Kerja Khusus | <?php echo $konten;?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta content="" name="description">
    <meta content="" name="DIREKTORAT PSMK">
    
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/default/style.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/default/style-responsive.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/default/theme/default.css" rel="stylesheet" id="theme">
    <!-- ================== END BASE CSS STYLE ================== -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script async="" src="https://www.google-analytics.com/analytics.js"></script><script src="<?php echo base_url();?>assets/backend/plugins/pace/pace.min.js"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top  pace-done"><div class="pace  pace-inactive"><div class="pace-progress" style="width: 100%;" data-progress-text="100%" data-progress="99">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade show d-none"><span class="spinner"></span></div>
    <!-- end #page-loader -->
    
    <!-- begin #page-container -->
    <div id="page-container" class="fade show">
        <!-- begin error -->
        <div class="error">
            <div class="error-code m-b-10"><i class="fas fa-bug"></i></div>
            <div class="error-content">
                <div class="error-message"><?php echo $konten;?></div>
                <div class="error-desc m-b-30">
                    Silahkan Ulangi Reset.
                </div>
                <div>
                    <a href="<?php echo base_url();?>login" class="btn btn-success p-l-20 p-r-20">Reset Ulang</a>
                    <a href="<?php echo base_url();?>" class="btn btn-success p-l-20 p-r-20 m-l-5">Halaman Utama</a>
                </div>
            </div>
        </div>
        <!-- end error -->
        
        
        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <!--[if lt IE 9]>
        <script src="../assets/crossbrowserjs/html5shiv.js"></script>
        <script src="../assets/crossbrowserjs/respond.min.js"></script>
        <script src="../assets/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/js-cookie/js.cookie.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/theme/default.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
    <!-- ================== END BASE JS ================== -->
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-53034621-1', 'auto');
      ga('send', 'pageview');

    </script>

</body></html>