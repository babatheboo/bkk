<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="author" content="BKK">
    <title>Bursa Kerja Khusus</title>    
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontpage/img/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/bootstrap.min.css" type="text/css">    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/jasny-bootstrap.min.css" type="text/css">  
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/bootstrap-select.min.css" type="text/css">  
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/material-kit.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/fonts/font-awesome.min.css" type="text/css"> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/fonts/themify-icons.css"> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/animate.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/extras/settings.css" type="text/css"> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/slicknav.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/main.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontpage/css/responsive.css" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontpage/css/colors/blue.css" media="screen" />
    <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
</head>

<body> 

    <div class="container" style="margin-top:100px">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-6 cd-user-modal">
                <div class="my-account-form">

                    <div class="page-login-form"> 
                        <p class="cd-form-message">Masukan password baru Anda.</p>
                        <form autocomplete='off' method="POST" class="cd-form">
                            <div class="form-group is-empty">
                                <input type="hidden" name="iduser" value="<?php echo $iduser; ?>" id="iduser">
                                <div class="input-icon">
                                    <i class="ti-key"></i>
                                    <input type="password" id="pw1" class="form-control" name="pw1" placeholder="Password">
                                </div>
                                <div class="input-icon">
                                    <i class="ti-pin"></i>
                                    <input type="password" id="pw2" class="form-control" name="pw2" placeholder="Ulangi Password">
                                </div>
                                <span class="material-input"></span></div>
                                <p class="fieldset">
                                    <button onclick="cekPass()" class="btn btn-common log-btn" type="button">Reset Password</button>
                                </p>
                            </form>
                            <!-- <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p> -->
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <!-- Main JS  -->
        <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-3.3.1.min.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery-ui.js"></script>
        <script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery.blockUI.js"></script>
        <script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/material.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/material-kit.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.parallax.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.slicknav.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/main.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.counterup.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jasny-bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/form-validator.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/contact-form-script.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/frontpage/js/infinite-scroll.pkgd.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>assets/frontpage/js/gantipass.js"></script>

    </body>
    </html>