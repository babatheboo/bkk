<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Syncdata extends CI_Controller {
  public function __construct(){
      parent::__construct();
      $this->asn->login();
      $this->asn->role_akses('0');
      $this->db1 = $this->load->database('default', TRUE);
      $this->db2 = $this->load->database('mssql', TRUE);
      $this->load->model('syncdata/syncdata_model');
  }


  public function index()
  {
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=='0')
    {
      $isi['namamenu'] = "Sinkronisasi Data";
      $isi['page'] = "sync";
      $isi['kelas'] = "sinkron";
      $isi['link'] = 'syncdata';
      $isi['halaman'] = "Sinkronisasi";
      $isi['judul'] = "Data sinkron ke DAPODIK";
      $isi['content'] = "sync_view";
            $this->load->view("dashboard/dashboard_view",$isi);
    }
    else
    {
      redirect('login','refresh');
    }
  }

  function doSync()
  {

    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=='0')
    {
      $isi['namamenu'] = "Sinkronisasi Data";
      $isi['page'] = "sync";
      $isi['kelas'] = "sinkron";
      $isi['link'] = 'syncdata';
      $isi['halaman'] = "Sinkronisasi";
      $isi['judul'] = "Data sinkron ke DAPODIK";
      $isi['content'] = "p_sync_view";
      $this->load->view("dashboard/dashboard_view",$isi);
    }
    else
    {
      redirect('login','refresh');
    }
  }

   function sinkron()
   {
        $isi['halaman'] = "Sinkronisasi";
        $isi['judul'] = "Data sinkron ke DAPODIK";
        $isi['page'] = "syncdata";
        $isi['content'] = "l_sync_view";
        $this->load->view("dashboard/dashboard_view",$isi);
   }


  
  function alasan() // sinkron anggota rombel
  {
    $x = $this->input->post('term');
    if($x=='alasan')
    {
      $jAgama = $this->db2->get('ref.alasan_layak_pip')->result();
      $lAgama = $this->db1->get('ref.alasan_layak_pip')->result();
      if(count($lAgama)==0)
      {
        foreach ($jAgama as $key) {
          $data['response'] = "true";
          $dAgama = array(
            'id_layak_pip'=>$key->id_layak_pip,
            'alasan_layak_pip'=>$key->alasan_layak_pip
            );
          $this->db1->insert('ref.alasan_layak_pip',$dAgama);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data layak PIP</li>';
      }
      else
      {
        $this->db1->empty_table('ref.anggota_rombel');
        foreach ($jAgama as $key) {
          $data['response'] = "true";
          $dAgama = array(
            'id_layak_pip'=>$key->id_layak_pip,
            'alasan_layak_pip'=>$key->alasan_layak_pip
            );
          $this->db1->insert('ref.alasan_layak_pip',$dAgama);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data layak PIP</li>';
      }
      
    }
    else
    {
      $data['response'] = 'false';
      $data['stat'] = '- Error sinkron data layak PIP</li>';
    }
    echo json_encode($data);
  }


  function anggota_rombel() // sinkron anggota rombel
  {
    $x = $this->input->post('term');
    if($x=='arombel')
    {
      $jAgama = $this->db2->get_where('dbo.anggota_rombel',array('soft_delete'=>'0'))->result();
      $lAgama = $this->db1->get('ref.anggota_rombel')->result();
      if(count($lAgama)==0)
      {
        foreach ($jAgama as $key) {
          $data['response'] = "true";
          $dAgama = array(
            'anggota_rombel_id'=>$key->anggota_rombel_id,
            'rombongan_belajar_id'=>$key->rombongan_belajar_id,
            'peserta_didik_id'=>$key->peserta_didik_id,
            'jenis_pendaftaran_id'=>$key->jenis_pendaftaran_id
            );
          $this->db1->insert('ref.anggota_rombel',$dAgama);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data anggota rombel</li>';
      }
      else
      {
        $this->db1->empty_table('ref.anggota_rombel');
        foreach ($jAgama as $key) {
          $data['response'] = "true";
          $dAgama = array(
            'anggota_rombel_id'=>$key->anggota_rombel_id,
            'rombongan_belajar_id'=>$key->rombongan_belajar_id,
            'peserta_didik_id'=>$key->peserta_didik_id,
            'jenis_pendaftaran_id'=>$key->jenis_pendaftaran_id
            );
          $this->db1->insert('ref.anggota_rombel',$dAgama);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data anggota rombel</li>';
      }
      
    }
    else
    {
      $data['response'] = 'false';
      $data['stat'] = '- Error sinkron data anggota rombel</li>';
    }
    echo json_encode($data);
  }

  function jurusan() // sinkron jurusan
  {
     $x = $this->input->post('term');
     if($x=='jurusan')
     {
      $jJurusan = $this->db2->get_where('ref.jurusan',array('untuk_smk'=>'1'))->result();
      $lJurusan = $this->db1->get('ref.jurusan')->result();
      if(count($jJurusan)==0)
      {
        foreach ($jJurusan as $key) {
          $dJurusan = array(
            'jurusan_id'=>$key->jurusan_id, 
            'nama_jurusan'=>$key->nama_jurusan, 
            'jenjang_pendidikan_id'=>$key->jenjang_pendidikan_id,
            'jurusan_induk'=>$key->jurusan_induk,
            'level_bidang_id'=>$key->level_bidang_id
            );
          $this->db1->insert('ref.jurusan',$dJurusan);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data jurusan </li>';
      }
      else
      {
        $this->db1->empty_table('ref.jurusan');
        foreach ($jJurusan as $key) {
          $dJurusan = array(
            'jurusan_id'=>$key->jurusan_id, 
            'nama_jurusan'=>$key->nama_jurusan, 
            'jenjang_pendidikan_id'=>$key->jenjang_pendidikan_id,
            'jurusan_induk'=>$key->jurusan_induk,
            'level_bidang_id'=>$key->level_bidang_id
            );
          $this->db1->insert('ref.jurusan',$dJurusan);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data jurusan</li>';
      }
    }
     else
     {
       $data['response'] = 'false';
       $data['stat'] = '- Error sinkron data jurusan</li>';
     }
   echo json_encode($data);  
  }

  function jurusansp() // sinkron jurusan
  {
    $x = $this->input->post('term');
    if($x=='jurusansp')
    {
      $jJurusan = $this->db2->get('jurusan_sp')->result();
      $lJurusan = $this->db1->get('ref.jurusan_sp')->result();
      if(count($jJurusan)==0)
      {
        foreach ($jJurusan as $key) {
          $dJurusan = array(
            'jurusan_sp_id'=>$key->jurusan_sp_id, 
            'sekolah_id'=>$key->sekolah_id, 
            'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id, 
            'jurusan_id'=>$key->jurusan_id,
            'nama_jurusan_sp'=>$key->nama_jurusan_sp
            );
          $this->db1->insert('ref.jurusan_sp',$dJurusan);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data jurusan sekolah</li>';
      }
      else
      {
        $this->db1->empty_table('ref.jurusan_sp');
        foreach ($jJurusan as $key) {
          $dJurusan = array(
            'jurusan_sp_id'=>$key->jurusan_sp_id, 
            'sekolah_id'=>$key->sekolah_id, 
            'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id, 
            'jurusan_id'=>$key->jurusan_id,
            'nama_jurusan_sp'=>$key->nama_jurusan_sp
            );
          $this->db1->insert('ref.jurusan_sp',$dJurusan);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data jurusan sekolah</li>';
      }
    }
    else
    {
      $data['response'] = 'false';
      $data['stat'] = '- Error sinkron data jurusan</li>';
    }
    echo json_encode($data);  
  }

  function bidang() // sinkron kelompok bidang
  {
    $x = $this->input->post('term');
    if($x=='bidang')
    {
      $jBidang = $this->db2->get_where('ref.kelompok_bidang',array('untuk_smk'=>'1'))->result();
      $lBidang = $this->db1->get('ref.kelompok_bidang')->result();
      if(count($jBidang)==0)
      {
        print_r($jBidang);
        foreach ($jBidang as $key) {
          $dBidang = array(
              'level_bidang_id'=>$key->level_bidang_id,
                'nama_level_bidang'=>$key->nama_level_bidang,
                'level_bidang_induk'=>$key->level_bidang_induk
            );
          $this->db1->insert('ref.kelompok_bidang',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data kelompok bidang</li>';
      }
      else
      {
        $this->db1->empty_table('ref.kelompok_bidang');
        foreach ($jBidang as $key) {
          $dBidang = array(
              'level_bidang_id'=>$key->level_bidang_id,
                'nama_level_bidang'=>$key->nama_level_bidang,
                'level_bidang_induk'=>$key->level_bidang_induk
            );
          $this->db1->insert('ref.kelompok_bidang',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data kelompok bidang</li>';
      }
    }
    else
    {
      $data['response'] = 'false';
    }
    echo json_encode($data);
  }

  function pekerjaan() // sinkron kelompok bidang
  {
    $x = $this->input->post('term');
    if($x=='kerja')
    {
      $jBidang = $this->db2->get('ref.pekerjaan')->result();
      $lBidang = $this->db1->get('ref.pekerjaan')->result();
      if(count($jBidang)==0)
      {
        print_r($jBidang);
        foreach ($jBidang as $key) {
          $dBidang = array(
              'pekerjaan_id'=>$key->pekerjaan_id,
                'nama_pekerjaan'=>$key->nama
            );
          $this->db1->insert('ref.pekerjaan',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data pekerjaan</li>';
      }
      else
      {
        $this->db1->empty_table('ref.pekerjaan');
        foreach ($jBidang as $key) {
          $dBidang = array(
                'pekerjaan_id'=>$key->pekerjaan_id,
                'nama_pekerjaan'=>$key->nama
            );
          $this->db1->insert('ref.pekerjaan',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data pekerjaan</li>';
      }
    }
    else
    {
      $data['response'] = 'false';
    }
    echo json_encode($data);
  }

 function penghasilan() // sinkron kelompok bidang
  {
    $x = $this->input->post('term');
    if($x=='penghasilan')
    {
      $jBidang = $this->db2->get('ref.penghasilan_orangtua_wali')->result();
      $lBidang = $this->db1->get('ref.penghasilan_orangtua_wali')->result();
      if(count($jBidang)==0)
      {
        print_r($jBidang);
        foreach ($jBidang as $key) {
          $dBidang = array(
              'penghasilan_orangtua_wali_id'=>$key->penghasilan_orangtua_wali_id,
                'nama_penghasilan'=>$key->nama,
                'batas_bawah'=>$key->batas_bawah,
                'batas_atas'=>$key->batas_atas
            );
          $this->db1->insert('ref.penghasilan_orangtua_wali',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses import data penghasilan</li>';
      }
      else
      {
        $this->db1->empty_table('ref.penghasilan_orangtua_wali');
        foreach ($jBidang as $key) {
          $dBidang = array(
                'pekerjaan_id'=>$key->pekerjaan_id,
                'nama_pekerjaan'=>$key->nama
            );
          $this->db1->insert('ref.penghasilan_orangtua_wali',$dBidang);
        }
        $data['response'] = 'true'; 
        $data['stat'] = '<li>Sukses update data penghasilan</li>';
      }
    }
    else
    {
      $data['response'] = 'false';
    }
    echo json_encode($data);
  }

  function peserta() // peserta didik
  {
      $x = $this->input->post('term');
      if($x=='peserta')
      {
          $jPeng = $this->db2->get_where('dbo.peserta_didik',array('soft_delete'=>"0"))->result();
          $lPeng = $this->db1->get('ref.peserta_didik')->result();
          if(count($jPeng)==0)
          {
              foreach ($jPeng as $key) {
                  $dPeng = array(
                        'peserta_didik_id'=>$key->peserta_didik_id,
                        'nama'=>$key->nama,
                        'jenis_kelamin'=>$key->jenis_kelamin,
                        'nisn'=>$key->nisn,
                        'nik'=>$key->nik,
                        'tempat_lahir'=>$key->tempat_lahir,
                        'tanggal_lahir'=>$key->tanggal_lahir,
                        'agama_id'=>$key->agama_id,
                        'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id,
                        'alamat_jalan'=>$key->alamat_jalan,
                        'rt'=>$key->rt,
                        'rw'=>$key->rw,
                        'nama_dusun'=>$key->nama_dusun,
                        'desa_kelurahan'=>$key->desa_kelurahan,
                        'kode_wilayah'=>$key->kode_wilayah,
                        'kode_pos'=>$key->kode_pos,
                        'nomor_telepon_rumah'=>$key->nomor_telepon_rumah,
                        'nomor_telepon_seluler'=>$key->nomor_telepon_seluler,
                        'email'=>$key->email,
                        'nik_ayah'=>$key->nik_ayah,
                        'nik_ibu'=>$key->nik_ibu,
                        'penerima_KPS'=>$key->penerima_KPS,
                        'no_KPS'=>$key->no_KPS,
                        'layak_PIP'=>$key->layak_PIP,
                        'penerima_KIP'=>$key->penerima_KIP,
                        'no_KIP'=>$key->no_KIP,
                        'nm_KIP'=>$key->nm_KIP,
                        'no_KKS'=>$key->no_KKS,
                        'id_layak_pip'=>$key->id_layak_pip,
                        'nama_ayah'=>$key->nama_ayah,
                        'tahun_lahir_ayah'=>$key->tahun_lahir_ayah,
                        'jenjang_pendidikan_ayah'=>$key->jenjang_pendidikan_ayah,
                        'pekerjaan_id_ayah'=>$key->pekerjaan_id_ayah,
                        'penghasilan_id_ayah'=>$key->penghasilan_id_ayah,
                        'nama_ibu_kandung'=>$key->nama_ibu_kandung,
                        'tahun_lahir_ibu'=>$key->tahun_lahir_ibu,
                        'jenjang_pendidikan_ibu'=>$key->jenjang_pendidikan_ibu,
                        'penghasilan_id_ibu'=>$key->penghasilan_id_ibu,
                        'pekerjaan_id_ibu'=>$key->pekerjaan_id_ibu
                      );
                  $this->db1->insert('ref.peserta_didik',$dPeng);
              }
               $data['response'] = 'true'; 
                $data['stat'] = '<li>Sukses import data peserta didik</li>'; 
          }
          else
          {
              $this->db1->empty_table('ref.peserta_didik');
               foreach ($jPeng as $key) {
                  $dPeng = array(
                         'peserta_didik_id'=>$key->peserta_didik_id,
                        'nama'=>$key->nama,
                        'jenis_kelamin'=>$key->jenis_kelamin,
                        'nisn'=>$key->nisn,
                        'nik'=>$key->nik,
                        'tempat_lahir'=>$key->tempat_lahir,
                        'tanggal_lahir'=>$key->tanggal_lahir,
                        'agama_id'=>$key->agama_id,
                        'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id,
                        'alamat_jalan'=>$key->alamat_jalan,
                        'rt'=>$key->rt,
                        'rw'=>$key->rw,
                        'nama_dusun'=>$key->nama_dusun,
                        'desa_kelurahan'=>$key->desa_kelurahan,
                        'kode_wilayah'=>$key->kode_wilayah,
                        'kode_pos'=>$key->kode_pos,
                        'nomor_telepon_rumah'=>$key->nomor_telepon_rumah,
                        'nomor_telepon_seluler'=>$key->nomor_telepon_seluler,
                        'email'=>$key->email,
                        'nik_ayah'=>$key->nik_ayah,
                        'nik_ibu'=>$key->nik_ibu,
                        'penerima_KPS'=>$key->penerima_KPS,
                        'no_KPS'=>$key->no_KPS,
                        'layak_PIP'=>$key->layak_PIP,
                        'penerima_KIP'=>$key->penerima_KIP,
                        'no_KIP'=>$key->no_KIP,
                        'nm_KIP'=>$key->nm_KIP,
                        'no_KKS'=>$key->no_KKS,
                        'id_layak_pip'=>$key->id_layak_pip,
                        'nama_ayah'=>$key->nama_ayah,
                        'tahun_lahir_ayah'=>$key->tahun_lahir_ayah,
                        'jenjang_pendidikan_ayah'=>$key->jenjang_pendidikan_ayah,
                        'pekerjaan_id_ayah'=>$key->pekerjaan_id_ayah,
                        'penghasilan_id_ayah'=>$key->penghasilan_id_ayah,
                        'nama_ibu_kandung'=>$key->nama_ibu_kandung,
                        'tahun_lahir_ibu'=>$key->tahun_lahir_ibu,
                        'jenjang_pendidikan_ibu'=>$key->jenjang_pendidikan_ibu,
                        'penghasilan_id_ibu'=>$key->penghasilan_id_ibu,
                        'pekerjaan_id_ibu'=>$key->pekerjaan_id_ibu
                      );
                  $this->db1->insert('ref.peserta_didik',$dPeng);
              }
              $data['response'] = 'true'; 
              $data['stat'] = '<li>Sukses update data peserta didik</li>'; 
            }
      }
      else
      {
          $data['response'] = 'false';
      }
      echo json_encode($data);
  }

  function reg_peserta() // registrasi peserta didik
  {
    $x = $this->input->post('term');
      if($x=='regpeserta')
      {
          $jPeng = $this->db2->get_where('dbo.registrasi_peserta_didik',array('soft_delete'=>"0"))->result();
          $lPeng = $this->db1->get('ref.registrasi_peserta_didik')->result();
          if(count($jPeng)==0)
          {
              foreach ($jPeng as $key) {
                  $dPeng = array(
                        'registrasi_id'=>$key->registrasi_id,
                        'jurusan_sp_id'=>$key->jurusan_sp_id,
                        'peserta_didik_id'=>$key->peserta_didik_id,
                        'sekolah_id'=>$key->sekolah_id,
                        'jenis_pendaftaran_id'=>$key->jenis_pendaftaran_id,
                        'nipd'=>$key->nipd,
                        'tanggal_masuk_sekolah'=>$key->tanggal_masuk_sekolah,
                        'jenis_keluar_id'=>$key->jenis_keluar_id,
                        'tanggal_keluar'=>$key->tanggal_keluar,
                        'keterangan'=>$key->keterangan
                      );
                  $this->db1->insert('ref.registrasi_peserta_didik',$dPeng);
              }
               $data['response'] = 'true'; 
      $data['stat'] = '<li>Sukses update data registrasi peserta didik</li>'; 
          }
          else
          {
              $this->db1->empty_table('ref.registrasi_peserta_didik');
               foreach ($jPeng as $key) {
                  $dPeng = array(
                      'registrasi_id'=>$key->registrasi_id,
                        'jurusan_sp_id'=>$key->jurusan_sp_id,
                        'peserta_didik_id'=>$key->peserta_didik_id,
                        'sekolah_id'=>$key->sekolah_id,
                        'jenis_pendaftaran_id'=>$key->jenis_pendaftaran_id,
                        'nipd'=>$key->nipd,
                        'tanggal_masuk_sekolah'=>$key->tanggal_masuk_sekolah,
                        'jenis_keluar_id'=>$key->jenis_keluar_id,
                        'tanggal_keluar'=>$key->tanggal_keluar,
                        'keterangan'=>$key->keterangan
                      );
                  $this->db1->insert('ref.registrasi_peserta_didik',$dPeng);
              }
              $data['response'] = 'true'; 
      $data['stat'] = '<li>Sukses update data registrasi peserta didik</li>'; 
          }
      }
      else
      {
          $data['response'] = 'false';
      }
      echo json_encode($data);
  }


  function rombel() // rombongan belajar
  {
    $x = $this->input->post('term');
      if($x=='rombel')
      {
          $jPeng = $this->db2->get_where('dbo.rombongan_belajar',array('soft_delete'=>"0"))->result();
          $lPeng = $this->db1->get('ref.rombongan_belajar')->result();
          if(count($jPeng)==0)
          {
              foreach ($jPeng as $key) {
                  $dPeng = array(
                        'rombongan_belajar_id'=>$key->rombongan_belajar_id,
                        'semester_id'=>$key->semester_id,
                        'sekolah_id'=>$key->sekolah_id,
                        'tingkat_pendidikan_id'=>$key->tingkat_pendidikan_id,
                        'jurusan_sp_id'=>$key->jurusan_sp_id,
                        'nama'=>$key->nama,
                        'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id
                      );
                  $this->db1->insert('ref.rombongan_belajar',$dPeng);
              }
               $data['response'] = 'true'; 
      $data['stat'] = '<li>Sukses update data registrasi peserta didik</li>'; 
          }
          else
          {
              $this->db1->empty_table('ref.rombongan_belajar');
               foreach ($jPeng as $key) {
                  $dPeng = array(
                        'rombongan_belajar_id'=>$key->rombongan_belajar_id,
                        'semester_id'=>$key->semester_id,
                        'sekolah_id'=>$key->sekolah_id,
                        'tingkat_pendidikan_id'=>$key->tingkat_pendidikan_id,
                        'jurusan_sp_id'=>$key->jurusan_sp_id,
                        'nama'=>$key->nama,
                        'kebutuhan_khusus_id'=>$key->kebutuhan_khusus_id
                      );
                  $this->db1->insert('ref.rombongan_belajar',$dPeng);
              }
              $data['response'] = 'true'; 
      $data['stat'] = '<li>Sukses update data registrasi peserta didik</li>'; 
          }
      }
      else
      {
          $data['response'] = 'false';
      }
      echo json_encode($data);
  }


  function sekolah() // sekolahs
  {
    $iSkl = $this->db1->get('ref.sekolah')->result();
    $query = $this->db2->get_where('dbo.sekolah',array('bentuk_pendidikan_id'=>'15','soft_delete'=>"0"));
    if(count($iSkl)==0)
    {
      foreach ($query->result() as $key) {
            $dSkolah = array (
              "sekolah_id"=>$key->sekolah_id,
              "nama"=>$key->nama,
              "nama_nomenklatur"=>$key->nama_nomenklatur,
              "nss"=>$key->nss,
              "npsn"=>$key->npsn,
              "bentuk_pendidikan_id"=>$key->bentuk_pendidikan_id,
              "alamat_jalan"=>$key->alamat_jalan,
              "rt"=>$key->rt,
              "rw"=>$key->rw,
              "nama_dusun"=>$key->nama_dusun,
              "desa_kelurahan"=>$key->desa_kelurahan,
              "kode_wilayah"=>$key->kode_wilayah,
              "kode_pos"=>$key->kode_pos,
              "lintang"=>$key->lintang,
              "bujur"=>$key->bujur,
              "nomor_telepon"=>$key->nomor_telepon,
              "nomor_fax"=>$key->nomor_fax,
              "email"=>$key->email,
              "website"=>$key->website,
              "kebutuhan_khusus_id"=>$key->kebutuhan_khusus_id,
              "status_sekolah"=>$key->status_sekolah
              );
            $this->db1->insert('ref.sekolah',$dSkolah);
      }
      
        $data['response'] = 'true'; 
        $data['status'] = '<li>Sukses import data sekolah</li>'; 
    }
    else
    {
      $this->db1->empty_table('ref.sekolah');
    foreach ($query->result() as $key) {
            $dSkolah = array (
              "sekolah_id"=>$key->sekolah_id,
              "nama"=>$key->nama,
              "nama_nomenklatur"=>$key->nama_nomenklatur,
              "nss"=>$key->nss,
              "npsn"=>$key->npsn,
              "bentuk_pendidikan_id"=>$key->bentuk_pendidikan_id,
              "alamat_jalan"=>$key->alamat_jalan,
              "rt"=>$key->rt,
              "rw"=>$key->rw,
              "nama_dusun"=>$key->nama_dusun,
              "desa_kelurahan"=>$key->desa_kelurahan,
              "kode_wilayah"=>$key->kode_wilayah,
              "kode_pos"=>$key->kode_pos,
              "lintang"=>$key->lintang,
              "bujur"=>$key->bujur,
              "nomor_telepon"=>$key->nomor_telepon,
              "nomor_fax"=>$key->nomor_fax,
              "email"=>$key->email,
              "website"=>$key->website,
              "kebutuhan_khusus_id"=>$key->kebutuhan_khusus_id,
              "status_sekolah"=>$key->status_sekolah
              );
      $this->db1->insert('ref.sekolah',$dSkolah);
      }
      
      $data['response'] = 'true'; 
    $data['status'] = '<li>Sukses update data sekolah</li>'; 
    }
   echo json_encode($data);
  }




  function log($jawal)
  {
    $tgl = date('Y-m-d');
   // $jawal = $this->input->post('xawal');
    $jakhir = date('H:i:s');
    $oleh = $this->session->userdata('user_id');
    $status = "Sukes Sinkronisasi dengan DAPODIK";
    $data = array('tgl' => $tgl,'start'=>$jawal, 'stop'=>$jakhir,'status'=>$status,'oleh'=>$oleh );
    $this->db1->insert('app.sync_log',$data);
    $data['response'] = 'true'; 
    $data['status'] = 'Sinkronisasi selesai'; 
    echo json_encode($data);
  }
  

  function get_log()
    {
      $list = $this->syncdata_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
       foreach ($list as $key) {
       
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->tgl;
            $row[] = $key->start;
            $row[] = $key->stop;
            $row[] = $key->nama;
            $row[] = $key->status;     
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->syncdata_model->count_all(),
                        "recordsFiltered" => $this->syncdata_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }


}