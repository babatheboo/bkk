<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Syncdata extends CI_Controller {
  var $host = "http://202.92.203.37/";
  public function __construct(){
      parent::__construct();
      $this->asn->login();
      $this->asn->role_akses('0');
      $this->db1 = $this->load->database('default', TRUE);
  //    $this->db2 = $this->load->database('mssql', TRUE);
      $this->load->model('syncdata/syncdata_model');
  }


  public function index()
  {
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=='0')
    {
      $isi['namamenu'] = "Sinkronisasi Data";
      $isi['page'] = "sync";
      $isi['kelas'] = "sinkron";
      $isi['link'] = 'syncdata';
      $isi['halaman'] = "Sinkronisasi";
      $isi['judul'] = "Data sinkron ke DAPODIK";
      $isi['content'] = "sync_view";
            $this->load->view("dashboard/dashboard_view",$isi);
    }
    else
    {
      redirect('login','refresh');
    }
  }

  function doSync()
  {

    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=='0')
    {
      $isi['namamenu'] = "Sinkronisasi Data";
      $isi['page'] = "sync";
      $isi['kelas'] = "sinkron";
      $isi['link'] = 'syncdata';
      $isi['halaman'] = "Sinkronisasi";
      $isi['judul'] = "Data sinkron ke DAPODIK";
      $isi['content'] = "p_sync_view";
      $this->load->view("dashboard/dashboard_view",$isi);
    }
    else
    {
      redirect('login','refresh');
    }
  }

   function sinkron()
   {
        $isi['halaman'] = "Sinkronisasi";
        $isi['judul'] = "Data sinkron ke DAPODIK";
        $isi['page'] = "syncdata";
        $isi['content'] = "l_sync_view";
        $this->load->view("dashboard/dashboard_view",$isi);
   }


  
 //  function alasan() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='alasan')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=nalasa');
 //        $dataS = json_decode($jsondata, true);
 //        $al = 0;
 //        $lCek = $this->db1->get('ref.alasan_layak_pip')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.alasan_layak_pip');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'id_layak_pip'=> $dataS[$a]['id_layak_pip'],
 //              'alasan_layak_pip'=> $dataS[$a]['alasan_layak_pip']
 //            );
 //          if($this->db1->insert('ref.alasan_layak_pip',$dataInsert))
 //          {
 //            $al = $al+0;
 //          }
 //          else
 //          {
 //            $al = $al+1;
 //          }
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data layak PIP</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //  function anggota_rombel() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='arombel')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.anggota_rombel')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.anggota_rombel');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'anggota_rombel_id'=>$dataS[$a]['anggota_rombel_id'],
 //              'rombongan_belajar_id'=>$dataS[$a]['rombongan_belajar_id'],
 //              'peserta_didik_id'=>$dataS[$a]['peserta_didik_id'],
 //              'jenis_pendaftaran_id'=>$dataS[$a]['jenis_pendaftaran_id']
 //            );
 //          $this->db1->insert('ref.anggota_rombel',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data anggota rombel</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //  function jurusan() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='jurusan')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=nasuruj');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.jurusan')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.jurusan');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'jurusan_id' =>$dataS[$a]['jurusan_id'],
 //              'nama_jurusan' =>$dataS[$a]['nama_jurusan'],
 //              'jenjang_pendidikan_id' =>$dataS[$a]['jenjang_pendidikan_id'],
 //              'jurusan_induk'=>$dataS[$a]['jurusan_induk'],
 //              'level_bidang_id'=>$dataS[$a]['level_bidang_id']
 //            );
 //          $this->db1->insert('ref.jurusan',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data jurusan</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //  function jurusansp() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='jurusansp')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.jurusan_sp')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.jurusan_sp');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'jurusan_sp_id' =>$dataS[$a]['jurusan_sp_id'],
 //              'sekolah_id' =>$dataS[$a]['sekolah_id'],
 //              'kebutuhan_khusus_id' =>$dataS[$a]['kebutuhan_khusus_id'],
 //              'jurusan_id'=>$dataS[$a]['jurusan_id'],
 //              'nama_jurusan_sp'=>$dataS[$a]['nama_jurusan_sp']
 //            );
 //          $this->db1->insert('ref.jurusan_sp',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data jurusan sp</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //  function bidang() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='bidang')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=gnadib');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.kelompok_bidang')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.kelompok_bidang');
 //        }
 //        $id ="";
 //        $idn = "";
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'level_bidang_id' =>$dataS[$a]['level_bidang_id'],
 //              'nama_level_bidang' =>$dataS[$a]['nama_level_bidang'],
 //              'level_bidang_induk' =>$dataS[$a]['level_bidang_induk']
 //            );
 //          $this->db1->insert('ref.kelompok_bidang',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data kelompok bidang</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }
 

 // function pekerjaan() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='kerja')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=naajrekep');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.pekerjaan')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.pekerjaan');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'pekerjaan_id' =>$dataS[$a]['pekerjaan_id'],
 //              'nama_pekerjaan' =>$dataS[$a]['nama']
 //            );
 //          $this->db1->insert('ref.pekerjaan',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data kelompok bidang</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }


 //  function penghasilan() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='penghasilan')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=nalisahgnep');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.penghasilan_orangtua_wali')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.penghasilan_orangtua_wali');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'penghasilan_orangtua_wali_id' =>$dataS[$a]['penghasilan_orangtua_wali_id'],
 //              'nama' =>$dataS[$a]['nama'],
 //              'batas_bawah' =>$dataS[$a]['batas_bawah'],
 //              'batas_atas' =>$dataS[$a]['batas_atas']
 //            );
 //          $this->db1->insert('ref.penghasilan_orangtua_wali',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data penghasilan orang tua wali</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //  function peserta() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='peserta')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresep');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.peserta_didik')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.peserta_didik');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'peserta_didik_id' =>$dataS[$a]['peserta_didik_id'],
 //              'nama' =>$dataS[$a]['nama'],
 //              'jenis_kelamin' =>$dataS[$a]['jenis_kelamin'],
 //              'nisn' =>$dataS[$a]['nisn'],
 //              'nik' =>$dataS[$a]['nik'],
 //              'tempat_lahir' =>$dataS[$a]['tempat_lahir'],
 //              'tanggal_lahir' =>$dataS[$a]['tanggal_lahir'],
 //              'agama_id' =>$dataS[$a]['agama_id'],
 //              'kebutuhan_khusus_id' =>$dataS[$a]['kebutuhan_khusus_id'],
 //              'alamat_jalan' =>$dataS[$a]['alamat_jalan'],
 //              'rt' =>$dataS[$a]['rt'],
 //              'rw' =>$dataS[$a]['rw'],
 //              'nama_dusun' =>$dataS[$a]['nama_dusun'],
 //              'desa_kelurahan' =>$dataS[$a]['desa_kelurahan'],
 //              'kode_wilayah' =>$dataS[$a]['kode_wilayah'],
 //              'kode_pos' =>$dataS[$a]['kode_pos'],
 //              'nomor_telepon_rumah' =>$dataS[$a]['nomor_telepon_rumah'],
 //              'nomor_telepon_seluler' =>$dataS[$a]['nomor_telepon_seluler'],
 //              'email' =>$dataS[$a]['email'],
 //              'nik_ayah' =>$dataS[$a]['nik_ayah'],
 //              'nik_ibu' =>$dataS[$a]['nik_ibu'],
 //              'penerima_KPS' =>$dataS[$a]['penerima_KPS'],
 //              'no_KPS' =>$dataS[$a]['no_KPS'],
 //              'layak_PIP' =>$dataS[$a]['layak_PIP'],
 //              'penerima_KIP' =>$dataS[$a]['penerima_KIP'],
 //              'no_KIP'=>$dataS[$a]['no_KIP'],
 //              'nm_KIP' =>$dataS[$a]['nm_KIP'],
 //              'no_KKS' =>$dataS[$a]['no_KKS'],
 //              'id_layak_pip' =>$dataS[$a]['id_layak_pip'],
 //              'nama_ayah' =>$dataS[$a]['nama_ayah'],
 //              'tahun_lahir_ayah' =>$dataS[$a]['tahun_lahir_ayah'],
 //              'jenjang_pendidikan_ayah' =>$dataS[$a]['jenjang_pendidikan_ayah'],
 //              'pekerjaan_id_ayah' =>$dataS[$a]['pekerjaan_id_ayah'],
 //              'penghasilan_id_ayah' =>$dataS[$a]['penghasilan_id_ayah'],
 //              'nama_ibu_kandung' =>$dataS[$a]['nama_ibu_kandung'],
 //              'tahun_lahir_ibu' =>$dataS[$a]['tahun_lahir_ibu'],
 //              'jenjang_pendidikan_ibu' =>$dataS[$a]['jenjang_pendidikan_ibu'],
 //              'penghasilan_id_ibu' =>$dataS[$a]['penghasilan_id_ibu'],
 //              'pekerjaan_id_ibu' =>$dataS[$a]['pekerjaan_id_ibu']
 //            );
 //          $this->db1->insert('ref.peserta_didik',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data peserta didik</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }

 //   function reg_peserta() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='regpeserta')
 //    {
 //        $tgl;
 //        $tgla;
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.registrasi_peserta_didik')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.registrasi_peserta_didik');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          if($dataS[$a]['jenis_keluar_id']!="")
 //          {
 //            $tgl =  $dataS[$a]['tanggal_keluar']['date'];
 //          }
 //          else
 //          {
 //            $tgl =  "1900-01-01";
 //          }
 //          // print_r($dataS[$a]['tanggal_masuk_sekolah']);
 //          if(count($dataS[$a]['tanggal_masuk_sekolah'])>0)
 //          {
 //            $tgla = $dataS[$a]['tanggal_masuk_sekolah']['date'];
 //          }
 //          else
 //          {
 //            $tgla = "1900-01-01";
 //          }
 //          // echo $tgla . "<br/>";
 //          $dataInsert = array(
 //              'registrasi_id' =>$dataS[$a]['registrasi_id'],
 //              'jurusan_sp_id' =>$dataS[$a]['jurusan_sp_id'],
 //              'peserta_didik_id' =>$dataS[$a]['peserta_didik_id'],
 //              'sekolah_id' =>$dataS[$a]['sekolah_id'],
 //              'jenis_pendaftaran_id' =>$dataS[$a]['jenis_pendaftaran_id'],
 //              'nipd' =>$dataS[$a]['nipd'],
 //              'tanggal_masuk_sekolah' =>$tgla,
 //              'jenis_keluar_id' =>$dataS[$a]['jenis_keluar_id'],
 //              'tanggal_keluar' =>$tgl,
 //              'keterangan'=>$dataS[$a]['keterangan']
 //            );
 //         $this->db1->insert('ref.registrasi_peserta_didik',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data registrasi peserta didik</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }


 // function rombel() // sinkron anggota rombel
 //  {
 //    $x = $this->input->post('term');
 //    if($x=='rombel')
 //    {
 //        $jsondata = file_get_contents($this->host . 'dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor');
 //        $dataS = json_decode($jsondata, true);
 //        $lCek = $this->db1->get('ref.rombongan_belajar')->result();
 //        if(count($lCek)>0)
 //        {
 //          $this->db1->empty_table('ref.rombongan_belajar');
 //        }
 //        for($a=0;$a<count($dataS);$a++)
 //        {
 //          $dataInsert = array(
 //              'rombongan_belajar_id' =>$dataS[$a]['rombongan_belajar_id'],
 //              'semester_id' =>$dataS[$a]['semester_id'],
 //              'sekolah_id' =>$dataS[$a]['sekolah_id'],
 //              'tingkat_pendidikan_id' =>$dataS[$a]['tingkat_pendidikan_id'],
 //              'jurusan_sp_id' =>$dataS[$a]['jurusan_sp_id'],
 //              'nama' =>$dataS[$a]['nama'],
 //              'kebutuhan_khusus_id' =>$dataS[$a]['kebutuhan_khusus_id']
 //            );
 //          $this->db1->insert('ref.rombongan_belajar',$dataInsert);
 //        }
 //        $data['response'] = 'true'; 
 //        $data['stat'] = '<li>Sukses sinkron data rombongan belajar</li>';
 //    }
 //    else
 //    {
 //      $data['response'] = 'false';
 //      $data['stat'] = '- Error sinkron data layak PIP</li>';
 //    }
 //    echo json_encode($data);
 //  }


  function sekolah() // sinkron anggota rombel
  {
    // $x = $this->input->post('term');
    // if($x=='sekolah')
    // {
          $prop = $this->db1->get('view_provinsi')->result();
      // foreach ($prop as $pr) {
          $url = 'http://202.92.203.37/dapodik/webapi.php?uid=evaluasi&upa=emonev&utab=sekolah&id_prop=010000&id_kab=016000';
           $jsondata = \Httpful\Request::get($url)->send();
          $dataS = json_decode($jsondata, true);
          print_r($dataS);
          // $lCek = $this->db1->get('ref.sekolah')->result();
          // if(count($lCek)>0)
          // {
          //   $this->db1->empty_table('ref.sekolah');
          // }
          // for($a=0;$a<count($dataS);$a++)
          // {
          //     $dataInsert = array(
          //     'sekolah_id' =>$dataS[$a]['sekolah_id'],
          //     'nama' =>$dataS[$a]['nama'],
          //     'nama_nomenklatur' =>$dataS[$a]['nama_nomenklatur'],
          //     'nss' =>$dataS[$a]['nss'],
          //     'npsn' =>$dataS[$a]['npsn'],
          //     'bentuk_pendidikan_id' =>$dataS[$a]['bentuk_pendidikan_id'],
          //     'alamat_jalan' =>$dataS[$a]['alamat_jalan'],
          //     'rt' =>$dataS[$a]['rt'],
          //     'rw' =>$dataS[$a]['rw'],
          //     'nama_dusun' =>$dataS[$a]['nama_dusun'],
          //     'desa_kelurahan' =>$dataS[$a]['desa_kelurahan'],
          //     'kode_wilayah' =>$dataS[$a]['kode_wilayah'],
          //     'kode_pos' =>$dataS[$a]['kode_pos'],
          //     'lintang' =>$dataS[$a]['lintang'],
          //     'bujur' =>$dataS[$a]['bujur'],
          //     'nomor_telepon' =>$dataS[$a]['nomor_telepon'],
          //     'nomor_fax' =>$dataS[$a]['nomor_fax'],
          //     'email' =>$dataS[$a]['email'],
          //     'website' =>$dataS[$a]['website'],
          //     'kebutuhan_khusus_id' =>$dataS[$a]['kebutuhan_khusus_id'],
          //     'status_sekolah' =>$dataS[$a]['status_sekolah']
          //   );
          // $this->db1->insert('ref.sekolah',$dataInsert);
          // }
          // $data['response'] = 'true'; 
          // $data['stat'] = '<li>Sukses sinkron data kelompok bidang</li>';
      // }
    
    // }
    // else
    // {
    //   $data['response'] = 'false';
    //   $data['stat'] = '- Error sinkron data layak PIP</li>';
    // }
    echo json_encode($data);
  }




  function log($jawal)
  {
    $tgl = date('Y-m-d');
   // $jawal = $this->input->post('xawal');
    $jakhir = date('H:i:s');
    $oleh = $this->session->userdata('user_id');
    $status = "Sukes Sinkronisasi dengan DAPODIK";
    $data = array('tgl' => $tgl,'start'=>$jawal, 'stop'=>$jakhir,'status'=>$status,'oleh'=>$oleh );
    $this->db1->insert('app.sync_log',$data);
    $data['response'] = 'true'; 
    $data['status'] = 'Sinkronisasi selesai'; 
    echo json_encode($data);
  }
  

  function get_log()
    {
      $list = $this->syncdata_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
       foreach ($list as $key) {
       
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->tgl;
            $row[] = $key->start;
            $row[] = $key->stop;
            $row[] = $key->nama;
            $row[] = $key->status;     
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->syncdata_model->count_all(),
                        "recordsFiltered" => $this->syncdata_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }


}