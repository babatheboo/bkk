<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Syncdata_model extends CI_Model {

	var $table = 'app.view_sync_log';
    var $column_order = array(null, 'tgl','start','stop','status','nama'); //set column field database for datatable orderable
    var $column_search = array('tgl','start','stop','status','nama'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 

public function __construct(){
  	parent::__construct();
  		$this->db1 = $this->load->database('default', TRUE);
  		$this->db2 = $this->load->database('mssql', TRUE);
	}


	function get_matpel_jur()
	{
			$q= "SELECT ref.mata_pelajaran.mata_pelajaran_id, ref.jurusan.jurusan_id
				FROM ref.kurikulum INNER JOIN
				     ref.mata_pelajaran_kurikulum ON ref.kurikulum.kurikulum_id = ref.mata_pelajaran_kurikulum.kurikulum_id INNER JOIN
				     ref.mata_pelajaran ON ref.mata_pelajaran_kurikulum.mata_pelajaran_id = ref.mata_pelajaran.mata_pelajaran_id inner join 
					ref.jurusan on ref.jurusan.jurusan_id = ref.kurikulum.jurusan_id
				WHERE ref.jurusan.untuk_smk = '1'
				GROUP BY ref.mata_pelajaran.mata_pelajaran_id,  jurusan.jurusan_id 
				ORDER BY ref.mata_pelajaran.mata_pelajaran_id ASC, jurusan.jurusan_id ASC";
			$qry = $this->db2->query($q)->result();
			return $qry;
	}
	

	function data_log(){
 		$this->db1->from($this->table); 
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db1->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db1->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db1->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db1->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db1->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db1->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->data_log();
        if($_POST['length'] != -1)
        $this->db1->limit($_POST['length'], $_POST['start']);
        $query = $this->db1->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->data_log();
        $query = $this->db1->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db1->from($this->table);
        return $this->db1->count_all_results();
    }

}