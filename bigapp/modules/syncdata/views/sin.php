<script type="text/javascript">
$(document).ready(function(){
        $("#agama").hide();
        $("#jurusan").hide();
        $("#bidang").hide();
        $("#kurikulum").hide();
        $("#matpel").hide();
        $("#matkul").hide();

});



function timer(){
setTimeout('timer()',1000);
}

function agama()
{
    $("#agama").show();
    a=0;
        setTimeout(function(){
            document.getElementById("da").style.width =a+"%";
            document.getElementById("da").innerHTML =a+"% Completed";
            a++;
            if(a<101)
            {
                agama();
            }
        },500); 

}

function jurusan()
{
    $("#jurusan").show()
    a=0;
        setTimeout(function(){
            document.getElementById("db").style.width =a+"%";
            document.getElementById("db").innerHTML =a+"% Completed";
            a++;
            if(a<101)
            {
                jurusan();
            }
        },500); 
    
}
function doProses()
{
if (confirm('Yakin memulai proses Sinkronisasi ?')) 
{
  // window.location = $BASE_URL+'syncdata/doSync';
$("#tabel").hide();
setTimeout(function(){    
    $.ajax({
        url: "<?php echo base_url(); ?>syncdata/agama",
        dataType: 'json',
        type: 'POST',
        data: {term:'agama'},
        success:    
        function(data){
            if(data.response =="true"){
                agama();
            }
        },
    });
},4000);

setTimeout(function(){ 
$.ajax({
    url: "<?php echo base_url(); ?>syncdata/jurusan",
    dataType: 'json',
    type: 'POST',
    data: {term:'jurusan'},
    success:    
    function(data){
        if(data.response =="true"){
          jurusan();
        }
    },
});
},4000);


} else {
    alert("Proses dibatalkan !");
    return true;
}
}
</script>
<div class="row">
			    <!-- begin col-6 -->
			    <div class="col-md-12 ui-sortable" id="tabel">
			        <!-- begin panel -->                   <button type="button" onclick="doProses()" class="btn btn-success"><i class="fa fa-exchange"></i> Proses Sinkronisasi</button>

                    <div class="panel panel-inverse">
                   <div class="panel-body">
                            <table class="table" id="log_table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Start</th>
                                        <th>Stop</th>
                                        <th>Oleh</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
			    </div>
			    <!-- end col-6 -->
                <div id="agama">
            <h2>Data Agama</h2>
            <div class="progress">
              <div id="da" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>

            <div id="jurusan">
            <h2>Data Jurusan</h2>
            <div class="progress">
              <div id="db" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>

            <div id="bidang">
            <h2>Data Kelompok Bidang</h2>
            <div class="progress">
              <div id="dc" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>

            <div id="kurikulum">
            <h2>Data Kurikulum</h2>
            <div class="progress">
              <div id="dd" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>


            <div id="matpel">
            <h2>Data Mata Pelajaran</h2>
            <div class="progress">
              <div id="de" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>

            <div id="matkul">
            <h2>Mata Pelajaran</h2>
            <div class="progress">
              <div id="df" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            </div>

</div>
