<script>
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';  
var save_method;
var table;
$(function() {
    table = $('#log_table').DataTable({ 
        autoWidth: false,
        "processing": true,
        "serverSide": true,
        "responsive": true, 
        "order": [],
        "pageLength": 20,
        "ajax": {
            "url": $BASE_URL+ "syncdata/get_log",
            "type": "POST"
        },
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});

        $.blockUI.defaults.css = { 
            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            cursor: 'wait'
        };

function doProses()
{
 window.location.href=("<?php echo base_url();?>syncdata/sinkron");
}



</script> 
<div  id="domMessage"  style="display:none;" class="alert alert-danger alert-dismissible fade in" role="alert">
<strong>Proses Sinkronisasi</strong> Silahkan tunggu !
</div>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"> &nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="doProses()"><i class="fa fa-refresh"></i> Proses Sinkronisasi</button> </div> <h4 class="panel-title"><?php echo $halaman;?></h4> </div> <div class="panel-body"> <div class="table-responsive"> <table id="log_table" class="table table-striped table-bordered nowrap" width="100%"> <thead> <tr> <th style="text-align:center" width="1%">No.</th> <th style="text-align:center" width="10%">Tanggal</th> <th style="text-align:center" width="50%">Start</th><th style="text-align:center" width="30%">Stop</th><th style="text-align:center" width="20%">Oleh</th> <th style="text-align:center" width="10%">Detail</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>
<div  id="domMessage"  style="display:none;" class="alert alert-danger alert-dismissible fade in" role="alert">
<strong>Proses Sinkronisasi</strong> Silahkan tunggu !
</div>