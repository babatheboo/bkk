<link href="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<script >
	$(document).ready(function(){
		$("#agama").hide();
		$("#jurusan").hide();
		$("#bidang").hide();
		$("#kurikulum").hide();
		$("#matpel").hide();
		$("#matkul").hide();
	});
	
</script>
<div class="row">
    <div class="col-md-6">
        <div class="x_panel">
          	<div class="x_title">
            	<h2>Proses Sinkronisasi Data dengan DAPODIK !</h2>
            	<div class="clearfix"></div>
          	</div>
          	<div class="x_content">
<!-- content -->
			<div id="agama">
 			<h2>Data Agama</h2>
			<div class="progress">
			  <div id="da" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>

			<div id="jurusan">
 			<h2>Data Jurusan</h2>
			<div class="progress">
			  <div id="db" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>

			<div id="bidang">
 			<h2>Data Kelompok Bidang</h2>
			<div class="progress">
			  <div id="dc" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>

			<div id="kurikulum">
 			<h2>Data Kurikulum</h2>
			<div class="progress">
			  <div id="dd" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>


			<div id="matpel">
 			<h2>Data Mata Pelajaran</h2>
			<div class="progress">
			  <div id="de" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>

			<div id="matkul">
 			<h2>Mata Pelajaran</h2>
			<div class="progress">
			  <div id="df" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
			</div>

<!-- end content -->
			</div>
		</div>
	</div>
</div>
