<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('user_mode');
    }
	public function pisah(){
		$dba = $this->db->query("SELECT peserta_didik_id, nisn FROM ref.peserta_didik l LEFT JOIN app.username r ON r.user_id = l.peserta_didik_id WHERE r.user_id IS NULL AND l.nisn!='0' and l.nisn is not null and length(nisn)>5")->result();
		foreach($dba as $key){
			$data = array('user_id'=>$key->peserta_didik_id,'username'=>$key->nisn,'password'=>'23f86cc2c670b465198a4da285e5e890','level'=>'3','login'=>'1','status'=>NULL);
			$this->db->insert('app.username',$data);
		}
	}
	public function getuname(){
		$db = $this->db->query("SELECT peserta_didik_id,nisn FROM ref.peserta_didik WHERE nisn!=''")->result();
		foreach($db as $key)
		{
			$p = $key->peserta_didik_id;
			$cek = $this->db->query("SELECT * FROM app.username WHERE user_id='$p'")->result();
			if(count($cek)==0){
				$data = array('user_id'=>$key->peserta_didik_id,'username'=>$key->nisn,'password'=>'23f86cc2c670b465198a4da285e5e890','level'=>'3','login'=>'1','status'=>NULL);
				$this->db->insert('app.username',$data);
			}
		}
	}
	public function chpass(){
		$nisn = $this->input->post('nisn');
		$plama = md5(md5($this->input->post('plama')));
		$pbaru = md5(md5($this->input->post('pbaru')));
		$cek = $this->db->query("SELECT * FROM app.username WHERE username='$nisn' AND password='$plama'")->result();
		if(count($cek)==1){
			if($this->db->query("UPDATE app.username SET password='$pbaru' WHERE username='$nisn'")){
				$response["error"] = FALSE;
				echo json_encode($response);
				}else{
				  $response["error"] = TRUE;
                        	  $response["error_msg"] = "Password berhasil dirubah, silahkan logout !";
                        	  echo json_encode($response);
				}
			}else {
			$response["error"] = TRUE;
			$response["error_msg"] = "Password lama salah, silahkan ulangi";
			echo json_encode($response);
		}
	}
	public function slider($sid){
		$gb = $this->db->get_where('slide',array('sekolah_id'=>$sid,'status'=>'1'))->result();
		if(count($gb>0)){
		  foreach($gb as $key){
			$judul = $key->judul;
			$file = $key->gambar;
			$link = base_url() . "assets/img/slider/". $file;
			$data[] = array('judul'=>$judul,'link'=>$link);
		  }
	   	}else{
			$judul = "Belum ada kegiatan";
			$file = "no.jpg";
			$link = base_url() . "assets/img/slider/" . $file;
			$data[] = array('judul'=>$judul,'link'=>$link);
		}
		echo json_encode($data,JSON_UNESCAPED_SLASHES);
	}
	public function user_get()
	{
		//$nisn = trim($this->input->post('nisn'));
		//$pass = trim($this->input->post('jejak'));
//		$nisn = trim($this->input->get('nisn'));
//		$pass = trim($this->input->get('jejak'));
		$nisn = $this->asn->anti($this->input->post('nisn'));
		$pass = $this->asn->anti($this->input->post('jejak'));
		$res = $this->user_mode->get_user($nisn,$pass);
		if(count($res)!=0)
		{
			foreach ($res as $key) {
				$response['error'] = FALSE;
				$id = trim($key->user_id);
				$response['uid'] = trim($key->user_id);
				$response["user"]["username"] = $key->username;
			}
			$ns = $this->db->query("select * from ref.peserta_didik where peserta_didik_id='$id'")->result();
			foreach ($ns as $key) {
				$response["user"]["nama"] = $key->nama;
				$sid = $key->sekolah_id;
				$response["user"]["sekolah_id"] = $key->sekolah_id;
			}
			$sk = $this->db->query("select * from ref.sekolah where sekolah_id='$sid'")->result();
			foreach ($sk as $key) {
				$response['user']['nama_sekolah'] = $key->nama;
			}
			$lg = $this->db->query("SELECT * FROM sekolah_terdaftar where sekolah_id='$sid'")->result();
			foreach ($lg as $key) {
				$response['user']['logo'] = $key->logo;
			}
			$cektest = $this->db->get_where('app.mbti_result',array('user_id'=>$id))->result();
			if(count($cektest)==0)
			{
				$response["user"]["assesment"] = "0";
			}
			else
			{
				$response["user"]["assesment"] = "1";
			}
			echo json_encode($response);
		}
		else{
			$response["error"] = TRUE;
			$response["error_msg"] = "Username dan Password Salah. Silahkan Coba Lagi!";
			echo json_encode($response);
		}
	}
	public function get_loker(){
		$tgl    = date("Y-m-d");
		$ckdata = $this->db->query("SELECT id,kode_perusahaan,judul,tgl_tutup FROM view_lowongan_tujuan WHERE sekolah_id = '9' AND status = '1' AND tgl_tutup>='$tgl'")->result();
		foreach ($ckdata as $key) {
			$kd     = $key->kode_perusahaan;
			$psh    = $this->db->query("SELECT nama FROM ref.industri WHERE industri_id='$kd'");
			$keys   = $psh->row();
			$nama   = $keys->nama;
			$data[] = array('id'=>$key->id,'judul'=>$key->judul,'tgl_tutup'=>$key->tgl_tutup,'perusahaan'=>$nama);
		}
		echo json_encode($data);
	}
	public function get_loker_sendiri($sess){
		$tgl    = date("Y-m-d");
		$ckdata = $this->db->query("SELECT id,kode_perusahaan,judul,tgl_tutup FROM view_lowongan_tujuan WHERE sekolah_id = '$sess' AND status = '1' AND tgl_tutup>='$tgl'")->result();
		foreach ($ckdata as $key) {
			$kd     = $key->kode_perusahaan;
			$psh    = $this->db->query("SELECT nama FROM ref.industri WHERE industri_id='$kd'");
			$keys   = $psh->row();
			$nama   = $keys->nama;
			$data[] = array('id'=>$key->id,'judul'=>$key->judul,'tgl_tutup'=>$key->tgl_tutup,'perusahaan'=>$nama);
		}
		echo json_encode($data);
	}
	public function get_dloker($id,$nisn){
		$siswa = $this->db->query("SELECT peserta_didik_id FROM ref.peserta_didik where nisn='$nisn'")->result();
		foreach ($siswa as $key) {
			$pdi = $key->peserta_didik_id;
		}
		$cek = $this->db->query("SELECT * FROM bidding_lulusan WHERE id_siswa='$pdi' AND id_loker='$id'")->result();
		if(count($cek)>0){
			$sbid = "1";	
		}else{
			$sbid = "0";
		}
		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE id='$id'")->result();
		foreach ($ckdata as $key) {
			$kd = $key->kode_perusahaan;
			$psh = $this->db->query("SELECT nama FROM ref.industri WHERE industri_id='$kd'")->result();
			foreach ($psh as $keys) {
				$nama = $keys->nama;
			}
			$dl = $this->db->query("SELECT * FROM view_lowongan_detil WHERE id='$id'")->result();
			if(count($dl)>0)
			{
							foreach ($dl as $keys) {
				$jml = $keys->dibutuhkan;
			}
			}else{
				$jml = "";
			}
			$data[] = array('cek'=>$sbid ,'id'=>$key->id,'judul'=>$key->judul,'tgl_tutup'=>$key->tgl_tutup,'perusahaan'=>$nama,'tgl_tutup'=>$key->tgl_tutup,
				'deskripsi'=>$key->deskripsi,'dibutuhkan'=>$jml);
		}
		echo json_encode($data);
		//echo json_encode($ckdata);
	}
	public function get_inbox($nisn){
		$siswa = $this->db->query("SELECT peserta_didik_id FROM ref.peserta_didik where nisn='$nisn'");
		$key = $siswa->row();
		$pdi = $key->peserta_didik_id;
		$ckdata = $this->db->query("SELECT * FROM bidding_lulusan WHERE id_siswa = '$pdi'")->result();
		if(count($ckdata)>0){
			foreach ($ckdata as $key) {
				$idloker = $key->id_loker;
				$ckjadwal = $this->db->query("SELECT * FROM jadwal_tes WHERE id_loker = '$idloker'")->result();
				foreach ($ckjadwal as $row) {
					$user = $row->user_industri;
					$idn = $this->db->query("SELECT * FROM app.user_industri WHERE user_id='$user'")->result();
					if(count($idn)>0){
						foreach ($idn as $keys) {
							$id =  $keys->industri_id;
							$p = $this->db->query("SELECT * FROM ref.industri WHERE industri_id='$id'")->result();
							foreach ($p as $keya) {
								$prsh = $keya->nama;
							}
						}
					}else{
						$prsh = "";
					}
					$isi = "Mulai test : " . $row->tgl_mulai . " Selesai Test : " . $row->tgl_selesai;
					$data[] = array('judul'=>$row->judul,'isi'=>$row->keterangan,'tgl'=>$isi, 'alamat'=>$row->alamat,'perusahaan'=>$prsh);
				}
			}
		}
		echo json_encode($data);
	}
	public function save_bid($nisn,$idloker)
	{
		$siswa = $this->db->query("SELECT peserta_didik_id FROM ref.peserta_didik where nisn='$nisn'")->result();
		foreach ($siswa as $key) {
			$pdi = $key->peserta_didik_id;
		}
		$tgl = date("Y-m-d");
		$data  = array('id_siswa' =>$pdi , 'tgl_bid'=>$tgl, 'status'=>'1','id_loker'=>$idloker );
		$this->db->insert("bidding_lulusan",$data);
	}
	public function get_profile($nisn){
		$sis = $this->db->get_where('ref.peserta_didik',array("nisn"=>$nisn))->result();
		if(count($sis)>0){
			foreach ($sis as $key) {
				$nama = $key->nama;
			$jur = $this->db->get_where("ref.jurusan",array("jurusan_id"=>$key->jurusan_id))->result();
			if(count($jur)>0){
				foreach ($jur as $keg) {
					$jurusan = $keg->nama_jurusan;
				}
			}else{
				$jurusan = "";
			}
			}
		}else{
			$nama = "";
		}
		$pr = $this->db->query("SELECT * FROM kerja_siswa WHERE nisn='$nisn' AND status='1'")->result();
		$kl = $this->db->query("SELECT * FROM kuliah_siswa WHERE nisn='$nisn' AND status='1'")->result();
		$wl = $this->db->query("SELECT * FROM wira_siswa WHERE nisn='$nisn'  AND status='1'")->result();
		$na = count($pr);
		$nb = count($kl);
		$nc = count($wl);
		$stat = "";
		if($na==0 && $nb==0 && $nc==0) // Belum kerja
		{
			$stat = "Belum bekerja";
		}
		if($na==0 && $nb==0 && $nc>0) // wira usaha
		{
			foreach ($wl as $key) {
						$wi = $key->perusahaan;
						$jn = $key->jenis_usaha;
					}
					$stat = "Nama usaha " . $wi . " jenis usaha " . $jn;
		}
		if($na==0 && $nb>0 && $nc==0) // kuliah
		{
			foreach ($kl as $key) {
					$pt = $key->nama_pt;
					$ju = $key->jurusan;
				}
				$stat = "Kuliah di " . $pt . " jurusan " . $ju;
		}
		if($na==0 && $nb>0 && $nc>0) // kuliah
		{
			foreach ($kl as $key) {
					$pt = $key->nama_pt;
					$ju = $key->jurusan;
				}
				$stat = "Kuliah di " . $pt . " jurusan " . $ju;
		}
		if($na>0 && $nb==0 && $nc==0) // kerja
		{
			foreach ($pr as $key) {
				$perus = $key->perusahaan;
				$job = $key->posisi;
			}
			$stat = "Bekerja di " . $perus . " sebagai " . $job;
		}
		if($na>0 && $nb==0 && $nc>0) // kerja
		{
			foreach ($pr as $key) {
				$perus = $key->perusahaan;
				$job = $key->posisi;
			}
			$stat = "Bekerja di " . $perus . " sebagai " . $job;
		}
		if($na>0 && $nb>0 && $nc==0) // kerja
		{
			foreach ($pr as $key) {
				$perus = $key->perusahaan;
				$job = $key->posisi;
			}
			$stat = "Bekerja di " . $perus . " sebagai " . $job;
		}
		if($na>0 && $nb>0 && $nc>0) // kerja
		{
			foreach ($pr as $key) {
				$perus = $key->perusahaan;
				$job = $key->posisi;
			}
			$stat = "Bekerja di " . $perus . " sebagai " . $job;
		}
		$data[]  = array('status'=>$stat,'nama'=>$nama,'nis'=>$nisn,'jurusan'=>$jurusan);
		echo json_encode($data);
	}
	public function save_prof(){
		$kode = $this->input->post('kode');
		$nisn = $this->input->post('nisn');
		$a = $this->input->post('varA');
		$b = $this->input->post('varB');
		$c = $this->input->post('varC');
		// $kode = $this->input->get('kode');
		// $nisn = $this->input->get('nisn');
		// $a = $this->input->get('varA');
		// $b = $this->input->get('varB');
		// $c = $this->input->get('varC');
		$siswa = $this->db->query("SELECT peserta_didik_id FROM ref.peserta_didik where nisn='$nisn'")->result();
		foreach ($siswa as $key) {
			$pdi = $key->peserta_didik_id;
		}
		$tg = explode("/", $c);
		$tgl =  $tg[2] . "-" . $tg[1] . "-" . $tg[0];
		switch ($kode) {
			case '0':
				$this->db->where('nisn',$nisn);
				$this->db->update('kerja_siswa',array('status'=>"0"));
				$this->db->update('kuliah_siswa',array('status'=>"0"));
				$this->db->update('wira_siswa',array('status'=>"0"));
				break;
			case '1':
				$cek = $this->db->query("SELECT * FROM kerja_siswa WHERE nisn='$nisn' AND perusahaan='$a'")->result();
				if(count($cek)==0){
					$data = array('nisn'=>$nisn,'perusahaan'=>$a,'posisi'=>$b,'mulai_kerja'=>$tgl,'status'=>"1",'peserta_didik_id'=>$pdi,'ver_status'=>"0");
					$this->db->insert("kerja_siswa",$data);
				}
			case '2':
				$data = array('nisn'=>$nisn,'nama_pt'=>$a,'jurusan'=>$b,'mulai_kuliah'=>$tgl,'status'=>"1",'peserta_didik_id'=>$pdi,'ver_status'=>"0");
				$this->db->insert("kuliah_siswa",$data);
				break;
			case '3':
				$data = array('nisn'=>$nisn,'perusahaan'=>$a,'jenis_usaha'=>$b,'mulai_usaha'=>$tgl,'status'=>"1",'peserta_didik_id'=>$pdi,'ver_status'=>"0");
				$this->db->insert("wira_siswa",$data);
				break;
		}
	}
}
