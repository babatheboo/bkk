<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/js/banner.js"></script>
<div class="row"> <div class="col-md-12"> 
    <div class="panel panel-inverse"> <div class="panel-heading"> 
        <div class="panel-heading-btn">
            <button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
                <i class="fa fa-plus-circle"></i> Tambah Data
            </button>&nbsp;
            <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                <i class="fa fa-refresh"></i> Reload Data
            </button>
        </div>
        <h4 class="panel-title"><?php echo $halaman;?></h4>
    </div> 
    <div class="panel-body"> 
        <div class="table-responsive"> 
            <table id="data-banner" class="table table-striped table-bordered nowrap" width="100%"> 
                <thead> 
                    <tr> 
                        <th style="text-align:center" width="1%">No.</th> 
                        <th style="text-align:center" width="15%">Foto Banner</th>
                        <th style="text-align:center" width="20%">Deskripsi</th>
                        <th style="text-align:center" width="10%">Status</th>
                        <th style="text-align:center" width="10%">Action</th>
                    </tr> 
                </thead> 
                <tbody> 
                </tbody> 
            </table> 
        </div> 
    </div> 
</div> 
</div>
</div>
<div id="modal_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-group" id="photo-preview">
                        <label class="control-label col-md-3">Foto</label>
                        <div class="col-md-9">
                            (No photo)
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Deskripsi</label>
                            <div class="col-md-6">
                                <input name="nama" id="nama" placeholder="Masukan Deskripsi" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label id="label-photo" class="control-label col-md-3">Foto Banner</label>
                            <div class="col-md-6">
                                <input name="foto" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form> 

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_banner('<?php echo $link;?>','banner','form')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
