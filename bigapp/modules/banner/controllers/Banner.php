<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Banner extends CI_Controller {
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('banner_model');
      //  $this->asn->login();
    }
    public function index(){
    $this->_content();
  }
  public function _content(){
    $isi['kelas'] = "admin";
    $isi['namamenu'] = "Data Banner";
    $isi['page'] = "banner";
    $isi['cek'] = "edit";
    $isi['link'] = 'banner';
    $isi['actionhapus'] = 'hapus';
    $isi['actionedit'] = 'edit';
    $isi['halaman'] = "Data Banner";
    $isi['judul'] = "Halaman Data Banner";
    $isi['content'] = "banner_view";
    $this->load->view("dashboard/dashboard_view",$isi);
  }
  public function getData(){
    if($this->input->is_ajax_request()){
      $list = $this->banner_model->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $rowx) {
        $no++;
        $row = array();
        $row[] = $no . ".";
        $row[] = "<center><img src='".base_url()."img/banner/$rowx->gambar' width='100px'></img></center>";
        $row[] = $rowx->deskripsi;
        $row[] = $rowx->status ? '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\',\'Data Banner\',\'banner\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>' : '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\',\'Data Banner\',\'banner\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-danger" title="Status NonAktif"><i class="fa fa-lock icon-white"></i></a>';
        $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data Banner\',\'banner\',\'edit_data\','."'".$rowx->id."'".')"><i class="icon-pencil"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data Banner\',\'banner\',\'hapus_data\','."'".$rowx->id."'".')"><i class="icon-remove icon-white"></i></a></center>';
        $data[] = $row;
      }
      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->banner_model->count_all(),
        "recordsFiltered" => $this->banner_model->count_filtered(),
        "data" => $data,
        );
      echo json_encode($output);
    }else{
      redirect("_404","refresh");
    }
  }
  private function _validate($cek){
      $data = array();
      $data['error_string'] = array();
      $data['inputerror'] = array();
      $data['status'] = TRUE;
      if($this->input->post('nama') == ''){
        $data['inputerror'][] = 'nama';
        $data['error_string'][] = 'Masukan deskripsi untuk gambar ini.';
        $data['status'] = FALSE;
      }
      if($data['status'] === FALSE){
        echo json_encode($data);
        exit();
      }
  }
  public function save_banner(){
    if ($this->input->is_ajax_request()) {
          $method         = "save";
          $this->_validate($method);
          $data           = array('deskripsi'=>$this->input->post('nama'),
            'status'        =>'1');
          if(!empty($_FILES['foto']['name'])){
            $upload         = $this->_do_upload();
            $data['gambar'] = $upload;
          }
          $this->banner_model->simpan($data);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  public function update_banner(){
    if ($this->input->is_ajax_request()) {
        $method = "update";
        $this->_validate($method);
        $data   = array('deskripsi'=>$this->input->post('nama'));
        if(!empty($_FILES['foto']['name'])){
          $this->hapus_foto($this->input->post('id'));
          $upload         = $this->_do_upload();
          $data['gambar'] = $upload;
        }
        $this->banner_model->update(array('id' => trim($this->input->post('id'))), $data);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  private function _do_upload(){
      $config['upload_path']    = 'img/banner';
      $config['allowed_types']  = 'gif|jpeg|jpg|png|bmp';
      $config['max_size']		= '1048';
      $config['max_width'] 		= '0';
      $config['max_height'] 	= '0';
      $config['overwrite'] 		= TRUE;
      $config['file_name']      = round(microtime(true) * 1000);
      $this->upload->initialize($config);
      $this->load->library('upload', $config);
      if(!$this->upload->do_upload('foto')) {
        $data['inputerror'][] = 'foto';
        $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('','');
        $data['status'] = FALSE;
        echo json_encode($data);
        exit();
      }
      return $this->upload->data('file_name');
  }
  public function hapus_data($id){
    if($this->input->is_ajax_request()){
        $this->hapus_foto($id);
        $this->banner_model->hapus_by_id($id);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  public function hapus_foto($id){
      $ckfoto    = $this->db->get_where('banner',array('id'=>$id));
      $row       = $ckfoto->row();
      $fotona    = $row->gambar;
      if($fotona !="no.jpg"){
      @chmod(unlink('./img/banner/' . $fotona),777);
      }
  }
  public function edit_data($id){
    if($this->input->is_ajax_request()){
        $data = $this->banner_model->get_by_id($id);
        echo json_encode($data);
    }else{
        redirect("_404","refresh");
    }
  }
  public function ubah_status($jns=Null,$id=Null){
    if($this->input->is_ajax_request()){
      if($jns=="aktif"){
          $data = array('status'=>'0');
      }else{
          $data = array('status'=>'1');
      }
      $this->db->where('id',$this->asn->anti($id));
      $this->db->update('banner',$data);
    }else{
      redirect("_404",'refresh');
    }
  }
}
