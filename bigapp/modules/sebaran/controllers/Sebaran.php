<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sebaran extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->load->model('sebaran_model');
	}

	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['menu'] = "sebaran";
		$isi['content'] = "sebaran_view";
		$isi['option_jurusan'][''] = "Semua Jurusan";
		$ckjrs = $this->db->query("SELECT jurusan_id,nama_jurusan FROM ref.jurusan WHERE level_bidang_id = '12' order by nama_jurusan asc")->result();
		foreach ($ckjrs as $key ) {
			$isi['option_jurusan'][$key->jurusan_id] = $key->nama_jurusan;
		}

		$isi['option_prov'][''] = "Semua Provinsi";
		$ckprov = $this->db->query("SELECT kode_prov,prov FROM ref.view_provinsi order by prov asc")->result();
		foreach ($ckprov as $key ) {
			$isi['option_prov'][$key->kode_prov] = str_replace('Prop. ', '', $key->prov);
		}

		$this->load->view("frontpage/front_view",$isi);
	}

	public function getData(){
		if($this->input->is_ajax_request()){
			 $this->db->query("REFRESH MATERialized view ref.view_sebaran");
			$list = $this->sebaran_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->nsekolah;
				$row[] = $rowx->prov;
				$row[] = $rowx->kota;
				$row[] = $rowx->kec;
				$row[] = $rowx->jml_lulusan;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->sebaran_model->count_all(),
				"recordsFiltered" => $this->sebaran_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}

	public function cek_prov($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_kota where kode_prov='".$kode."' order by kota asc")->result();
		$data['say'] = '
			<select id="id_kota" class="form-control select" style="width:100%" data-live-search="true" data-style="btn-white">
				<option value="">Pilih Kota</option>
		';
		
		foreach($ckdata as $key){
			$data['say'].='<option value="'.$key->kode_kota.'">'.$key->kota.'</option>';
		}
		$data['say'].='</select>';

		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}
}
