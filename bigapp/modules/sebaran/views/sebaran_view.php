<style>
.spinner:before {
  content: 'Mohon tunggu';
  box-sizing: border-box;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 20px;
  height: 20px;
  margin-top: -10px;
  margin-left: -10px;
  border-radius: 50%;
  border: 2px solid #ccc;
  border-top-color: #333;
  animation: spinner .6s linear infinite;
}
</style>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<div id="content">
    <div class="container">
        <div class="row">
        <h4>Data sebaran lulusan</h4>
            <div class="col-md-12 col-sm-12 col-xs-12">
                    <div > 
                        <div class="col-md-4">
                        <span><?php echo form_dropdown('jurusan',$option_jurusan,isset($default['jurusan']) ? $default['jurusan'] : '','class="form-control" style="width:100%" id="jurusan" name="jurusan" data-live-search="true" data-style="btn-white"');?></span>
                        </div>
                        <div class="col-md-4">
                        <span><?php echo form_dropdown('provinsi',$option_prov,isset($default['provinsi']) ? $default['provinsi'] : '','class="form-control" style="width:100%" id="provinsi" name="provinsi" onchange="cek_prov(\'provinsi\',\'sebaran\',\'cek_prov\');" data-live-search="true" data-style="btn-white"');?></span> 
                        </div>
                        <div class="col-md-4">
                        <span id="kota"></span>
                        </div>
                    </div>
                </div>
                <div id="filter_pencarian">
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    
                        <table id="data-sebaran" class="display" width="100%"> 
                            <thead> 
                                <tr>
                                    <th style="text-align:center" width="1%">ID</th> 
                                    <th style="text-align:center" width="39%">Sekolah</th>
                                    <th style="text-align:center" width="15%">Provinsi</th>
                                    <th style="text-align:center" width="15%">Kota/Kab</th>
                                    <th style="text-align:center" width="15%">Kecamatan</th>
                                    <th style="text-align:center" width="15%">Jumlah Lulusan</th> 
                                </tr> 
                            </thead> 
                            <tbody> </tbody> 
                        </table> 
                </div> 

            </div> 
        </div>
    </div>
