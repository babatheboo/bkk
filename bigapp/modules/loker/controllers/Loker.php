<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Loker extends CI_Controller {
	public function __construct(){
		parent::__construct();
$this->asn->login(); // cek session login
if($this->session->userdata('level')==1){
	$this->asn->role_akses("1");
}elseif($this->session->userdata('level')==2){
	$this->asn->role_akses("2");
}
$this->load->library('encryption'); //in controller
$this->load->model('loker_model');
$this->load->model('loker_x_model');
$this->load->model('lokerku_model');
date_default_timezone_set('Asia/Jakarta');
$this->db1 = $this->load->database('default', TRUE);
}
public function index(){
	$this->_content();
}
public function _content(){
	$sess = $this->session->userdata('role_');
	$isi['option_kat_loker'][''] = "Semua Kategori";
	$this->db1->query("refresh materialized view view_lowongan_tujuan");
	$this->db1->query("refresh materialized view view_lowongan");
	$this->db1->query("refresh materialized view view_lowongan_detil");
	$ckkat = $this->db1->query("SELECT a.kode_bidang,b.nama_bidang_usaha FROM view_lowongan_detil a JOIN ref.bidang_usaha b ON a.kode_bidang = b.bidang_usaha_id GROUP BY a.kode_bidang,b.nama_bidang_usaha")->result();
	if(count($ckkat)>0){
		foreach ($ckkat as $row) {
			$isi['option_kat_loker'][trim($row->kode_bidang)] = $row->nama_bidang_usaha;
		}
	}else{
		$isi['option_kat_loker'][''] = "Kategori Lowongan Belum Tersedia";
	}
	$ckmitra = $this->db->query("SELECT a.kode_perusahaan,b.nama FROM view_lowongan_detil a JOIN ref.industri b ON a.kode_perusahaan = b.industri_id::varchar GROUP BY a.kode_perusahaan,b.nama")->result();
	if(count($ckmitra)>0){
		$isi['option_industri'][''] = "Semua Industri";
		foreach ($ckmitra as $key) {
			$isi['option_industri'][$key->kode_perusahaan] = $key->nama;
		}
	}else{
		$isi['option_industri'][''] = "Anda Belum Memiliki Mitra Industri";
	}
	$this->asn->cekSekolah($sess);
	$isi['tot_loker'] = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (status = '1' OR status = '9') AND sekolah_pembuat = '$sess'")->num_rows();
	$isi['namamenu'] = "Data Lowongan Pekerjaan";
	$isi['halaman'] = "Lowongan Pekerjaan";
	$isi['judul'] = "Halaman Daftar Lowongan Pekerjaan";
	$isi['page'] = "bursa";
	$isi['link'] = 'loker';
//	$isi['content'] = "_content";
$sid = $this->session->userdata('role_');
        $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
        $valid = "";
        foreach ($b as $key) {
          $valid = $key->valid;
        }
        if($valid=="1"){
          $isi['content']              = "_content";
        } else {
          $isi['content']              = "admin_bkk";
        }

	$this->load->view('dashboard/dashboard_view',$isi);
}
public function add_loker(){
	$ckmitra = $this->db->get_where('mitra',array('sekolah_id'=>$this->session->userdata('role_')))->result();
	if(count($ckmitra)>0){
		$isi['namamenu'] = "Input Loker";
		$isi['page'] = "bursa";
		$isi['link'] = 'loker';
		$isi['option_industri'][''] = "Pilih Mitra Industri";
		$isi['option_kota_filter'][''] = "Pilih Kab / Kota";
		$ckmitra = $this->db->get_where('view_mitra',array('sekolah_id'=>$this->session->userdata('role_')))->result();
		if(count($ckmitra)>0){
			foreach ($ckmitra as $row) {
				$isi['option_industri'][$row->industri_id] = $row->nama;
			}
		}else{
			$isi['option_industri'][''] = "Anda Belum Memiliki Mitra Industri";
		}
		$ckpropinsi = $this->db1->query("SELECT * FROM ref.view_provinsi")->result();
		if(count($ckpropinsi)>0){
			foreach ($ckpropinsi as $row) {
				$isi['option_provinsi'][trim($row->kode_prov)] = $row->prov;
			}
		}else{
			$isi['option_provinsi'][''] = "Data Propinsi Belum Tersedia";
		}
		$isi['option_provinsi_filter'][''] = "Pilih Propinsi";
		$ckpropinsi = $this->db1->query("SELECT * FROM ref.view_provinsi")->result();
		if(count($ckpropinsi)>0){
			foreach ($ckpropinsi as $row) {
				$isi['option_provinsi_filter'][trim($row->kode_prov)] = $row->prov;
			}
		}else{
			$isi['option_provinsi_filter'][''] = "Data Propinsi Belum Tersedia";
		}
		$isi['option_kota_filter'][''] = "Pilih Kabuaten / Kota";
		$isi['option_gaji'][''] = "Pilih Range Gaji";
		$d_gaji = $this->db->query("SELECT * FROM ref.range_gaji ORDER BY id ASC")->result();
		foreach ($d_gaji as $key) {
			$isi['option_gaji'][$key->id] = $key->range;
		}
		$isi['actionhapus'] = 'hapus';
		$isi['actionedit'] = 'edit';
		$isi['halaman'] = "Input Loker";
		$isi['judul'] = "Halaman Input Lowongan Pekerjaan";
//		$isi['content'] = "form_loker";
$sid = $this->session->userdata('role_');
        $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
        $valid = "";
        foreach ($b as $key) {
          $valid = $key->valid;
        }
        if($valid=="1"){
          $isi['content']              = "form_loker";
        } else {
          $isi['content']              = "admin_bkk";
        }
		$this->load->view("dashboard/dashboard_view",$isi);
	}else{
		$isi['tombolsimpan'] = 'Simpan';
		$isi['namamenu'] = "Input Loker";
		$isi['page'] = "bursa";
		$isi['link'] = 'loker';
		$isi['tombolbatal'] = 'Batal';
		$isi['halaman'] = "Input Loker";
		$isi['judul'] = "BKK Anda belum bermitra";
		$isi['content'] = "error_loker";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
}
public function getSakola(){
	if($this->input->is_ajax_request()){
		$list = $this->loker_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rowx) {
			$no++;
			$row = array();
			$row[] = $rowx->bkk_anak;
			$row[] = $rowx->npsn;
			$row[] = $rowx->npsn;
			$row[] = $rowx->nama;
			$row[] = '<a href="'.$rowx->website.'" target="_blank">Link</a>';
			$row[] = '<a href="'.$rowx->website.'" target="_blank">Link</a>';
			$data[] = $row;
		}
		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->loker_model->count_all(),
			"recordsFiltered" => $this->loker_model->count_filtered(),
			"data"            => $data,
		);
		echo json_encode($output);
	}else{
		redirect("_404","refresh");
	}
}
public function getPelamar(){
	if($this->input->is_ajax_request()){
		$this->load->model('pelamar_model');
		$list = $this->pelamar_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rowx) {
			$no++;
			$row   = array();
			$row[] = "<center>".$no.".</center>";
			$foto  = $this->db->query("SELECT photo FROM ref.peserta_didik WHERE peserta_didik_id='$rowx->peserta_didik_id'")->row();
			if($foto->photo!=Null){
			$row[] = '<center><img style="height:100px" src="'.base_url().'assets/foto/siswa/'.$foto->photo.'"></center>';
			}else{
				$row[] = '<center><img style="height:100px" src="'.base_url().'assets/foto/siswa/no.jpg"></center>';
			}
			if($this->session->userdata('level')!="3"){
				$row[] = '<a href="'.base_url().'loker/detil_pelamar/'.$rowx->peserta_didik_id.'" title="Info Pelamar">'.$rowx->nisn.'</a>';
			}else{
				$row[] = $rowx->nisn;
			}
			$row[]  = $rowx->nama;
			$row[]  = $rowx->nama_sekolah;
			$row[]  = '<center>'.$rowx->umur.'</center>';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->pelamar_model->count_all(),
			"recordsFiltered" => $this->pelamar_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}else{
		redirect("_404","refresh");
	}
}
public function getData(){
	if($this->input->is_ajax_request()){
		$list = $this->loker_x_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rowx) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $rowx->nama;
			$row[] = $rowx->judul;
			$row[] = $rowx->nama_bidang_usaha;
			$row[] = $rowx->range;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->loker_x_model->count_all(),
			"recordsFiltered" => $this->loker_x_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}else{
		redirect("_404","refresh");
	}
}
public function ajax_get_industri(){
	if($this->input->is_ajax_request()){
		$sess = $this->session->userdata('role_');
		$q = $this->input->post('q');
		$data_array = array();
		if($q != ""){
			$this->db1->select("industri_id");
			$this->db1->select("nama");
			$this->db1->from('view_mitra');
			$this->db1->where('sekolah_id',$sess);
			$this->db1->group_start();
			$this->db1->like('nama', $q['term']);
			$this->db1->group_end();
			$this->db1->limit(20);
			$query = $this->db1->get();
			foreach ($query->result() as $row){
				$data['id'] = $row->industri_id;
				$data['text']= $row->nama;
				$data_array[] = $data;
			}
		}
		echo json_encode($data_array);
	}else{
		redirect("_404","refresh");
	}
}
public function ajax_get_tujuan(){
	if($this->input->is_ajax_request()){
		$q = $this->input->post('q');
		$data_array = array();
		if($q != ""){
			$sess = $this->session->userdata('role_');
			$this->db1->select("sekolah_id");
			$this->db1->select("nama");
			$this->db1->from('"public"."sekolah"');
			$this->db1->group_start();
			$this->db1->where_not_in('sekolah_id', $sess);
			$this->db1->like('nama', $q['term']);
			$this->db1->group_end();
			$this->db1->limit(20);
			$query = $this->db1->get();
			foreach ($query->result() as $row){
				$data['id'] = $row->sekolah_id;
				$data['text']= $row->nama;
				$data_array[] = $data;
			}
			$data['id'] = "ALL";
			$data['text']= "ALL";
			$data_array[] = $data;
		}
		echo json_encode($data_array);
	}else{
		redirect("_404","refresh");
	}
}
public function ajax_get_spesial(){
	if($this->input->is_ajax_request()){
		$q = $this->input->post('q');
		$data_array = array();
		if($q != ""){
			$this->db->select('kode_kat, spesialisasi');
			$this->db->from('sub_kat_lowongan');
			$this->db->group_start();
			$this->db->like('spesialisasi', $q['term']);
			$this->db->group_end();
			$this->db->limit(20);
			$query = $this->db->get();
			foreach ($query->result() as $row){
				$data['id'] = $row->kode_kat;
				$data['text']= $row->spesialisasi;
				$data_array[] = $data;
			}
		}
		echo json_encode($data_array);
	}else{
		redirect("_404","refresh");
	}
}
public function getKabkot(){
	if($this->input->is_ajax_request()){
		$kode = $_POST['depart']; 
		$kodex = $this->asn->anti($kode);
		$list_kota = array();
		$data = $this->db1->query("SELECT * FROM ref.view_kota WHERE kode_prov = '$kodex'")->result();
		foreach ($data as $key) {
			$list_kota[] = array("id" => $key->kode_kota, "kota" => $key->kota);
		}
		echo json_encode($list_kota);
	}else{
		redirect("_404","refresh");
	}
}
public function getKabkot_(){
	if($this->input->is_ajax_request()){
		$kode = $_POST['depart']; 
		$kodex = $this->asn->anti($kode);
		$list_kota = array();
		$data = $this->db1->query("SELECT * FROM ref.view_kota WHERE kode_prov = '$kodex'")->result();
		foreach ($data as $key) {
			$list_kota[] = array("id" => $key->kode_kota, "kota" => $key->kota);
		}
		echo json_encode($list_kota);
	}else{
		redirect("_404","refresh");
	}
}
public function getKategori($kode){
	if($this->input->is_ajax_request()){
		$return = "";
		$kodex = $kode;
		$data = $this->db->query("SELECT * FROM ref.sub_kat_lowongan WHERE kode_kat_lowongan = '$kodex' AND status = '1'")->result();
		if(count($data)>0){
			$return = "<option value='' class=\"form-control\" data-size=\"100\" id=\"spesial_loker\" data-parsley-required=\"true\" data-live-search=\"true\" data-style=\"btn-white\"> Pilih Spesialisasi Lowongan </option>";
			foreach ($data as $key) {
				$return .= '<option class="form-control selectpicker" data-size="100" id="spesial_loker" data-parsley-required="true" data-live-search="true" data-style="btn-white" value="' .$key->kode_kat.'">' . $key->spesialisasi . '</option>';
			}
		}else{
			$return .= '<option class="form-control selectpicker" data-size="100" id="kota" data-parsley-required="true" data-live-search="true" data-style="btn-white" value="">Data Spesialisasi Lowongan Tidak Ditemukan</option>';
		}
		print $return;
	}else{
		redirect("_404","refresh");
	}
}
public function proses_add(){
	$this->form_validation->set_rules('industri', 'industri', 'htmlspecialchars|trim|required|min_length[1]|max_length[50]');
	$this->form_validation->set_rules('judul', 'judul lowongan', 'htmlspecialchars|trim|required|min_length[10]');
	// $this->form_validation->set_rules('posisi', 'posisi lowongan', 'htmlspecialchars|trim|required|min_length[10]');
	$this->form_validation->set_rules('deskripsi', 'deskripsi lowongan', 'htmlspecialchars|trim|required|min_length[10]');
	$this->form_validation->set_rules('gaji', 'range gaji', 'htmlspecialchars|trim|required|min_length[1]');
	$this->form_validation->set_rules('tgl_buka', 'tgl dibuka lowongan', 'htmlspecialchars|trim|required|min_length[10]|max_length[10]');
	$this->form_validation->set_rules('tgl_tutup', 'tgl ditutup lowongan', 'htmlspecialchars|trim|required|min_length[10]|max_length[10]');
	$this->form_validation->set_message('is_unique', '%s sudah ada sebelumnya');
	$this->form_validation->set_message('required', '%s tidak boleh kosong');
	$this->form_validation->set_message('min_length', '%s minimal %s karakter');
	$this->form_validation->set_message('max_length', '%s maximal %s karakter');
	if ($this->form_validation->run() == TRUE){
		$industri = htmlspecialchars($this->input->post('industri'));
		$judul = htmlspecialchars($this->input->post('judul'));
		$posisi = htmlspecialchars($this->input->post('posisi'));
		$deskripsi = htmlspecialchars($this->input->post('deskripsi'));
		$gaji = str_replace(".", "", $this->input->post('gaji'));
		$tgl_buka = date("Y-m-d",strtotime($this->input->post('tgl_buka')));
		$tgl_tutup = date("Y-m-d",strtotime($this->input->post('tgl_tutup')));
		$prov = $this->input->post('provinsi');
		$simpanloker = array('kode_perusahaan'=>$industri,
			'gaji'=>$gaji,
			'judul'=>$judul,
			'posisi'=>$posisi,
			'deskripsi'=>$deskripsi,
			'tgl_buka'=>$tgl_buka,
			'tgl_tutup'=>$tgl_tutup,
			'tgl_buat'=>date("Y-m-d"),
			'jam_buat'=>date("H:i:s"),
			'dibuat_oleh'=>$this->session->userdata('user_id'),
			'sekolah_pembuat'=>$this->session->userdata("role_"));
		$this->db1->insert('lowongan',$simpanloker);
		$ckid = $this->db->query("SELECT * FROM lowongan ORDER BY id DESC LIMIT 1");
		$x = $ckid->row();
		foreach ($_POST['rows'] as $key => $count ){
			$jns_kel = $_POST['jns_kel_'.$count];
			$jml     = $_POST['jml_'.$count];
			$tinggi  = $_POST['tinggi_'.$count];
			$berat   = $_POST['berat_'.$count];
			$umur    = $_POST['umur_'.$count];
			$simpan_syarat = array('id_loker'=>$x->id,
				'dibutuhkan'   =>$jml,
				'jns_kel'      =>$jns_kel,
				'tinggi_badan' =>$tinggi,
				'berat_badan'  =>$berat,
				'umur'         =>$umur);
			$this->db1->insert('lowongan_syarat',$simpan_syarat);
		}
		$id_x = $x->id;
		$jrs = $this->input->post('jurusan');
		for ($i=0; $i < count($jrs) ; $i++) { 
			if($jrs==99){
				$ckjrs = $this->db->query("SELECT * FROM ref.jurusan WHERE level_bidang_id = '12'")->result();
				foreach ($ckjrs as $key) {
					$simpan_detil = array('id_loker'=>$id_x,
						'kode_jurusan'=>$key->jurusan_id);
					$this->db->insert('lowongan_detil',$simpan_detil);	
				}
			}else{
				$simpan_detil = array('id_loker'=>$id_x,
					'kode_jurusan'=>$jrs[$i]);
				$this->db->insert('lowongan_detil',$simpan_detil);
			}
		}
		$kategori = $this->input->post('kat_loker');
		for ($ii=0; $ii < count($kategori) ; $ii++) { 
			$simpan_kategori = array('id_loker'=>$id_x,
				'kode_bidang'=>$kategori[$ii]);
			$this->db->insert('lowongan_kategori',$simpan_kategori);
		}
		for ($n=0; $n < count($prov) ; $n++) { 
			$simpan_lokasi = array('id_loker'=>$id_x,
				'kode_prov'=>$prov[$n]);
			$this->db->insert('lowongan_lokasi',$simpan_lokasi);
		}       
		if($this->input->post('tujuan')=='1'){
			$tujuan = $this->input->post('tujuan_sekolah');
			for ($i=0; $i < count($tujuan); $i++) { 
				$tujuanx = $tujuan[$i];
				$simpan_tujuan = array('id_loker'=>$x->id,
					'sekolah_id'=>$tujuanx,
					'status'=>'0');
				$this->db1->insert('lowongan_tujuan',$simpan_tujuan);
			}
			$simpanxx = array('id_loker'=>$x->id,
				'sekolah_id'=>$this->session->userdata('role_'),
				'status'=>'1');
			$this->db1->insert('lowongan_tujuan',$simpanxx);
		}else if($this->input->post('tujuan')=='9'){
			$simpanxx = array('id_loker'=>$x->id,
				'sekolah_id'=>'9',
				'status'=>'1');
			$this->db1->insert('lowongan_tujuan',$simpanxx);
		}elseif($this->input->post('tujuan')=='99'){
			$simpanxx = array('id_loker'=>$x->id,
				'sekolah_id'=>$this->session->userdata('role_'),
				'status'=>'1');
			$this->db1->insert('lowongan_tujuan',$simpanxx);
		}else{
			$sess = $this->session->userdata('role_');
			$aliansi = $this->db->get_where('view_aliansi_anak',array('bkk_induk'=>$sess))->result();
			foreach ($aliansi as $row) {
				$simpanxx = array('id_loker'=>$x->id,
					'sekolah_id'=>$row->bkk_anak,
					'status'=>'0');
				$this->db1->insert('lowongan_tujuan',$simpanxx);
			}
			$simpanxx = array('id_loker'=>$x->id,
				'sekolah_id'=>$this->session->userdata('role_'),
				'status'=>'1');
			$this->db1->insert('lowongan_tujuan',$simpanxx);
		}
		redirect('loker','refresh');
	}else{
		$this->add_loker();
	}
}
public function ubah_status($jns=Null,$id=Null){
	if($this->input->is_ajax_request()){
		if($jns=="aktif"){
			$data = array('status'=>'0',
				'user_approve'=>$this->session->userdata('user_id'),
				'tgl_approve'=>date("Y-m-d H:i:s"));
		}else{
			$data = array('status'=>'1',
				'user_approve'=>$this->session->userdata('user_id'),
				'tgl_approve'=>date("Y-m-d H:i:s"));
		}
		$this->db->where('id',$id);
		$this->db->update('lowongan_tujuan',$data);
		$this->db1->query("refresh materialized view view_lowongan_tujuan");
		$this->db1->query("refresh materialized view view_lowongan");
		$this->db1->query("refresh materialized view view_lowongan_detil");
	}else{
		redirect("_404",'refresh');
	}
}
public function getLoker(){
	if($this->input->is_ajax_request()){
		$this->db1->query("refresh materialized view view_lowongan_tujuan");
		$this->db1->query("refresh materialized view view_lowongan");
		$this->db1->query("refresh materialized view view_lowongan_detil");
		$sess = $this->session->userdata('role_');
		$this->asn->cekSekolah($sess);
		$page =  $_GET['page'];
		$kategori = $_GET['kat'];
		$industri = $_GET['industri'];
		$cari = $_GET['cari'];
		$offset = 10*$page;
		$limit = 10;
		if($this->session->userdata('cek_terdaftar_')=="Y"){
			if($kategori!="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND (kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri' AND (status = '1' OR status = '9') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_perusahaan = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_perusahaan = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (judul LIKE '%$cari%') OR (deskripsi LIKE '%$cari%') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori==="" && $industri==="" && $cari===""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}
		}else{
			if($kategori!="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND (kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri') AND (status = '1') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri' AND (status = '1') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_perusahaan = '$industri' AND (status = '1') AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND kode_perusahaan = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') AND (status = '1') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri=="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (sekolah_id = '$sess' OR sekolah_id ='9') AND (status = '1') LIMIT $limit OFFSET $offset")->result();
			}
		}
		if(count($ckdata)>0){
			foreach ($ckdata as $row) {
				$ckper = $this->db->get_where('ref.industri',array('industri_id'=>$row->kode_perusahaan));
				if(count($ckper->result())>0){
					$x = $ckper->row();
					$nama_perusahaan = $x->nama;
				}else{
					$nama_perusahaan = 'NotFound';
				}
				$cksekolah = $this->db->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$row->dibuat_oleh'");
				if(count($cksekolah->result())>0){
					$u = $cksekolah->row();
					$sk = $u->nama_sekolah;
				}else{
					$sk = "NotFound";
				}
				echo '<li>
				<div class="result-info">
				<h4 class="title"><a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'">'.$row->judul.'</a></h4>
				<p class="location" title="Nama Perusahaan | Sekolah Pembuat Lowongan | Tanggal Dibuat Lowongan">'.$nama_perusahaan.' | '.$sk.' |'.date("d-m-Y",strtotime($row->tgl_buat)).'</p>
				<p class="desc">
				';
				if(strlen($row->deskripsi) > 179){
					echo ''.substr($row->deskripsi, 0,179).' ... <a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'"><b>more</b></a>';
				}else{
					$tot = 200 - strlen($row->deskripsi);
					$hasil = $tot + strlen($row->deskripsi);
					echo $row->deskripsi;
					for ($i=0; $i < $hasil ; $i++) { 
						echo '&nbsp;';
					}
				}
				echo '</p>
				<div class="btn-row">
				<a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fas fa-fw fa-calendar" title="Tanggal Mulai : '.date("d-m-Y",strtotime($row->tgl_buka)).'"></i></a>
				<a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fas fa-fw fa-calendar-o" title="Tanggal Selesai : '.date("d-m-Y",strtotime($row->tgl_tutup)).'"></i></a>
				<a href="javascript:;" title="Total Pelamar : '.$this->db->get_where('bidding_lulusan',array('id_loker'=>$row->id))->num_rows().'" data-toggle="tooltip" data-container="body" data-title="Users"><i class="fas fa-fw fa-user"></i></a>
				</div>
				</div>
				<div class="result-price">';
				echo            '<small>Range Gaji : '.$row->range.'</small>';
				if(date("Y-m-d") > $row->tgl_tutup){
					echo '<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
				}else{
					echo '<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
				}
				if(date("Y-m-d") > $row->tgl_tutup){
					echo  '<p><a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
					</p></div>
					</li>';
				}else{
					if($this->session->userdata('level')=='3'){
						$ckbookmark 		= $this->db->get_where('view_bookmark',array('peserta_didik_id'=>$this->session->userdata('user_id'),'id_loker'=>$row->id));
						$ckbid 				= $this->db->get_where('view_bidding',array('peserta_didik_id'=>$this->session->userdata('user_id'),'id_loker'=>$row->id));
						if(!empty($ckbid->result()) && !empty($ckbookmark->result()) ){
							echo  '<p><a onclick="unbid_jobsis(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\',\''.$this->session->userdata('user_id').'\')" href="javascript:void(0)" class="btn btn-danger btn-xs m-r-5">Unbid Job</a>
							<a onclick="unbookmark(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\')" href="javascript:void(0)" class="btn btn-danger btn-xs m-r-5">Unbookmark</a>
							<a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
							</p></div>
							</li>';	
						}else if(!empty($ckbid->result()) && empty($ckbookmark->result()) ){
							echo  '<p><a onclick="unbid_jobsis(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\',\''.$this->session->userdata('user_id').'\')" href="javascript:void(0)" class="btn btn-danger btn-xs m-r-5">Unbid Job</a>
							<a onclick="bookmark(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\')" href="javascript:void(0)" class="btn btn-inverse btn-xs m-r-5">bookmark</a>
							<a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
							</p></div>
							</li>';	
						}else if(empty($ckbid->result()) && !empty($ckbookmark->result()) ){
							echo  '<p><a onclick="apply_jobsis(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\',\''.$this->session->userdata('user_id').'\')" href="javascript:void(0)" class="btn btn-inverse btn-xs m-r-5">Apply</a>
							<a onclick="unbookmark(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\')" href="javascript:void(0)" class="btn btn-danger btn-xs m-r-5">Unbookmark</a>
							<a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
							</p></div>
							</li>';	
						}else{
							echo  '<p><a onclick="apply_jobsis(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\')" href="javascript:void(0)" class="btn btn-inverse btn-xs m-r-5">Apply Job</a>
							<a onclick="bookmark(\''.$row->judul.'\',\''.$this->asn->encode($row->id).'\')" href="javascript:void(0)" class="btn btn-inverse btn-xs m-r-5">Bookmark</a>
							<a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
							</p></div>
							</li>';	
						}	
					}else{
						echo  '<p><a href="'.base_url().'loker/detil_loker/'.$this->asn->encode($row->id).'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
						</p></div>
						</li>';	
					}
				}
			}
		}else{
			echo '<div class="alert alert-danger m-b-15">
			Maaf, Lowongan Pekerjaan Tidak Tersedia.
			</div>';
		}
		exit;
	}else{
		redirect("_404","refresh");
	}	
}
public function detil_loker($kode=Null){
	$this->db1->query("refresh materialized view view_lowongan_tujuan");
	$this->db1->query("refresh materialized view view_lowongan");
	$this->db1->query("refresh materialized view view_lowongan_detil");
	// $kodex = $this->asn->decode($kode);
	$this->session->set_userdata('idx',$kode);
	$isi['id_loker'] = $kode;
	$ses             = $this->session->userdata('role_');
	$ckdata          = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE status = '1' AND id = '$kodex'");
	if(count($ckdata->result())>0){
		$this->session->set_userdata('id_loker_',$kode);
		$key = $ckdata->row();
		if(date("Y-m-d") > $key->tgl_tutup){
			$isi['status'] ='<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
		}else{
			$isi['status'] ='<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
		}
		$cksekolah = $this->db->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$key->dibuat_oleh'");
		if(count($cksekolah->result())>0){
			$u  = $cksekolah->row();
			$sk = $u->nama_sekolah;
		}else{
			$sk = "NotFound";
		}
		$isi['dibuat_oleh']  = $sk;
		$isi['kode_loker']   = $kode;
		$isi['nama']         = "";
		$isi['judul_loker']  = $key->judul;
		$isi['fotox']        = "no.jpg";
		$isi['kategori']     = "";
		$isi['spesialisasi'] = "";
		$isi['tglbuka']      = date("d-m-Y",strtotime($key->tgl_buka));
		$isi['tgltutup']     = date("d-m-Y",strtotime($key->tgl_tutup));
		$isi['deskripsi']    = $key->deskripsi;
		$isi['gaji']         = $key->range;
		$isi['namamenu']     = "Input Loker";
		$isi['page']         = "bursa";
		$isi['link']         = 'loker';
		$isi['halaman']      = "Detil Lowongan Pekerjaan";
		$isi['judul']        = "Halaman Detil Lowongan Pekerjaan";
		$isi['content']      = "detil_loker";
		$cksub               = $this->db1->query("SELECT * FROM ref.bidang_usaha")->result();
		if(count($cksub)>0){
			foreach ($cksub as $xx) {
				$isi['option_kategori'][trim($xx->bidang_usaha_id)] = $xx->nama_bidang_usaha;
			}
		}else{
			$isi['option_kategori'][''] = 'Bidang Usaha Belum Tersedia';
		}
		$cksubx = $this->db1->query("SELECT * FROM ref.jurusan WHERE level_bidang_id = '12' ORDER BY nama_jurusan ASC")->result();
		if(count($cksubx)>0){
			foreach ($cksubx as $xx) {
				$isi['option_jurusan'][trim($xx->jurusan_id)] = $xx->nama_jurusan;
			}
		}else{
			$isi['option_jurusan'][''] = 'Jurusan Untuk SMK Belum Tersedia';
		}
		$provinsi = $this->db->get('ref.view_provinsi')->result();
		foreach ($provinsi as $row) {
			$isi['option_lokasi'][trim($row->kode_prov)] = $row->prov;
		}
		$this->load->view("dashboard/dashboard_view",$isi);
	}else{
		redirect('_404','refresh');
	}
}
public function detil_lokerku($kode=Null){
	$this->db1->query("refresh materialized view view_lowongan_tujuan");
	$this->db1->query("refresh materialized view view_lowongan");
	$this->db1->query("refresh materialized view view_lowongan_detil");
	// $kodex = $this->asn->decode($kode);
// $this->session->set_userdata('id_lokerku',$kodex);
	$this->session->set_userdata('idx',$kode);
	$isi['id_loker'] = $kode;
	$ses = $this->session->userdata('role_');
	$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE status = '1' AND id = '$kode' AND sekolah_pembuat = '$ses' ");
	if(count($ckdata->result())>0){
		$this->session->set_userdata('id_loker_',$kode);
		$key = $ckdata->row();
		if(date("Y-m-d") > $key->tgl_tutup){
			$isi['status'] ='<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
		}else{
			$isi['status'] ='<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
		}
		$cksekolah = $this->db->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$key->dibuat_oleh'");
		if(count($cksekolah->result())>0){
			$u = $cksekolah->row();
			$sk = $u->nama_sekolah;
		}else{
			$sk = "NotFound";
		}
		$isi['dibuat_oleh'] = $sk;
		$isi['kode_loker'] = $kode;
		$isi['nama'] = "";
		$isi['judul_loker'] = $key->judul;
		$isi['fotox'] = "no.jpg";
		$isi['kategori'] = "";
		$isi['spesialisasi'] = "";
		$isi['tglbuka'] = date("d-m-Y",strtotime($key->tgl_buka));
		$isi['tgltutup'] = date("d-m-Y",strtotime($key->tgl_tutup));
		$isi['deskripsi'] = $key->deskripsi;
		$isi['gaji'] = $key->range;
		$isi['namamenu'] = "Input Loker";
		$isi['page'] = "bursa";
		$isi['link'] = 'loker';
		$isi['halaman'] = "Detil Lowongan Pekerjaan";
		$isi['judul'] = "Halaman Detil Lowongan Pekerjaan";
		$isi['content'] = "detil_lokerku";
		$cksub = $this->db1->query("SELECT * FROM ref.bidang_usaha")->result();
		if(count($cksub)>0){
			foreach ($cksub as $xx) {
				$isi['option_kategori'][trim($xx->bidang_usaha_id)] = $xx->nama_bidang_usaha;
			}
		}else{
			$isi['option_kategori'][''] = 'Bidang Usaha Belum Tersedia';
		}
		$cksubx = $this->db1->query("SELECT * FROM ref.jurusan WHERE level_bidang_id = '12' ORDER BY nama_jurusan ASC")->result();
		if(count($cksubx)>0){
			foreach ($cksubx as $xx) {
				$isi['option_jurusan'][trim($xx->jurusan_id)] = $xx->nama_jurusan;
			}
		}else{
			$isi['option_jurusan'][''] = 'Jurusan Untuk SMK Belum Tersedia';
		}
		$provinsi = $this->db->get('ref.view_provinsi')->result();
		foreach ($provinsi as $row) {
			$isi['option_lokasi'][trim($row->kode_prov)] = $row->prov;
		}
		$this->load->view("dashboard/dashboard_view",$isi);
	}else{
		redirect('_404','refresh');
	}
}
public function edit_loker($kode=Null){
	$this->db1->query("refresh materialized view view_lowongan_tujuan");
	$this->db1->query("refresh materialized view view_lowongan");
	$this->db1->query("refresh materialized view view_lowongan_detil");
	$this->session->set_userdata('kodeE',$kode);
	// $kodex = $this->asn->decode($kode);
	$ckuser = $this->db->get_where('app.user_sekolah',array('sekolah_id'=>$this->session->userdata('role_')))->result();
	if(count($ckuser)>0){
		$this->session->set_userdata('idna',$kode);
		foreach ($ckuser as $xx) {
			$user_id = $xx->user_id;
			$ckdata = $this->db->query("SELECT * FROM lowongan WHERE id = '$kode' AND dibuat_oleh = '$user_id' LIMIT 1")->result();
			foreach ($ckdata as $uy) {
				$ckmitra = $this->db->get_where('view_mitra',array('sekolah_id'=>$this->session->userdata('role_')))->result();
				if(count($ckmitra)>0){
					foreach ($ckmitra as $row) {
						$isi['option_industri'][$row->industri_id] = $row->nama;
					}
				}else{
					$isi['option_industri'][''] = "Anda Belum Memiliki Mitra Industri";
				}
				$isi['default']['judul'] = $uy->judul;
				$isi['deskripsi'] = $uy->deskripsi;
				$gaji = $uy->gaji;
				$ckdatagaji = $this->db->get_where('ref.range_gaji',array('id'=>$gaji));
				$xa = $ckdatagaji->row();
				$isi['option_gaji'][$gaji] = $xa->range;
				$d_gaji = $this->db->query("SELECT * FROM ref.range_gaji ORDER BY id ASC")->result();
				foreach ($d_gaji as $key) {
					$isi['option_gaji'][$key->id] = $key->range;
				}
				$isi['default']['tgl_buka'] = date("d-m-Y",strtotime($uy->tgl_buka));
				$isi['default']['tgl_tutup'] = date("d-m-Y",strtotime($uy->tgl_tutup));
				$isi['tombolsimpan'] = 'Simpan';
				$isi['namamenu'] = "Edit Data Loker";
				$isi['page'] = "bursa";
				$isi['link'] = 'loker';
				$isi['tombolbatal'] = 'Batal';
				$isi['halaman'] = "Edit Data Lowongan";
				$isi['judul'] = "Halaman Data Lowongan";
				$isi['content'] = "form_edit_loker";
				$this->load->view("dashboard/dashboard_view",$isi);
			}
		}
	}else{
		redirect('_404',"refres");
	}
}
public function proses_edit_loker(){
	$industri = htmlspecialchars($this->input->post('industri'));
	$judul = htmlspecialchars($this->input->post('judul'));
	$deskripsi = htmlspecialchars($this->input->post('deskripsi'));
	$gaji = str_replace(".", "", $this->input->post('gaji'));
	$tgl_buka = date("Y-m-d",strtotime($this->input->post('tgl_buka')));
	$tgl_tutup = date("Y-m-d",strtotime($this->input->post('tgl_tutup')));
	$prov = $this->input->post('provinsi');
	$kota = $this->input->post('kabupaten');
	$alamat = $this->input->post('alamat');
	$lat = $this->input->post('lintangO');
	$lon = $this->input->post('bujurO');
	$alamat = $this->input->post('alamatbaru');
	$kategori = $this->input->post('kat_loker');
	$simpanloker = array('kode_perusahaan'=>$industri,
		'gaji'=>$gaji,
		'judul'=>$judul,
		'deskripsi'=>$deskripsi,
		'tgl_buka'=>$tgl_buka,
		'tgl_tutup'=>$tgl_tutup);
	$this->db->where('id',$this->session->userdata('idna'));
	if($this->db->update('lowongan',$simpanloker)){
		$this->session->unset_userdata('idna');
		redirect("loker/detil_lokerku/".$this->session->userdata('kodeE') ,"refresh");
	}
}
public function detil_pelamar($id){
	if($this->session->userdata('level')!=""){
		$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$id));
		if(count($cksiswa->result())>0){
			$isi['tombolsimpan']   = 'Simpan';
			$isi['namamenu']       = "Input Loker";
			$isi['page']           = "bursa";
			$isi['kode']           = $id;
			$isi['jml_kompetensi'] = $this->db->get_where('kompetensi_siswa',array('user_id'=>$id))->num_rows();
			$this->session->set_userdata('id_pelamar',$id);
			$isi['link']        = 'loker';
			$isi['tombolbatal'] = 'Batal';
			$isi['halaman']     = "Detil Pelamar";
			$isi['judul']       = "Halaman Detil Pelamar";
			$isi['content']     = "cv";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect("_404","refresh");
		}
	}else{
		redirect("_404","refresh");
	}
}
public function apply_jobsis($idloker){
	if($this->input->is_ajax_request()){
		$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$this->session->userdata('user_id')));
		if(!empty($cksiswa->result())){
			$id_loker = $this->asn->decode($idloker);
			$tgl = date("Y-m-d");
			$ckloker = $this->db->get_where('view_lowongan_tujuan',array('id'=>$id_loker,'tgl_tutup > '=>$tgl));
			if(!empty($ckloker->result())){
				$simpanbid = array('id_siswa'=>$this->session->userdata('user_id'),
					'tgl_bid'=>$tgl,
					'status'=>1,
					'id_loker'=>$id_loker
				);
				$this->db->insert('bidding_lulusan',$simpanbid);
				echo json_encode(array("status" => TRUE));	
			}	
		}else{
			echo json_encode(array("status" => FALSE));	
		}
	}else{
		redirect("_404","refresh");
	}	
}
public function unbid_jobsis($idloker){
	if($this->input->is_ajax_request()){
		$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$this->session->userdata('user_id')));
		if(!empty($cksiswa->result())){
			$id_loker = $this->asn->decode($idloker);
			$psid = $this->asn->anti($this->session->userdata('user_id'));
			$tgl = date("Y-m-d");
			$ckbid = $this->db->get_where('view_bidding',array('id_loker'=>$id_loker,'peserta_didik_id'=>$psid,'status'=>'1'));
			if(!empty($ckbid->result())){
				$this->db->where('id_loker',$id_loker);
				$this->db->where('id_siswa',$psid);
				$this->db->delete('bidding_lulusan');
				echo json_encode(array("status" => TRUE));	
			}	
		}else{
			echo json_encode(array("status" => FALSE));	
		}
	}else{
		redirect("_404","refresh");
	}	
}
public function bookmark_jobsis($idloker){
	if($this->input->is_ajax_request()){
		$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$this->session->userdata('user_id')));
		if(!empty($cksiswa->result())){
			$id_loker = $this->asn->decode($idloker);
			$tgl = date("Y-m-d");
			$ckbookmark = $this->db->get_where('lowongan_bookmark',array('id_loker'=>$id_loker,'peserta_didik_id'=>$this->session->userdata('user_id')));
			if(empty($ckbookmark->result())){
				$simpanbook = array('peserta_didik_id'=>$this->session->userdata('user_id'),
					'tgl'=>$tgl,
					'id_loker'=>$id_loker
				);
				$this->db->insert('lowongan_bookmark',$simpanbook);
				echo json_encode(array("status" => TRUE));	
			}else{
				echo json_encode(array("status" => FALSE));	
			}	
		}else{
			echo json_encode(array("status" => FALSE));	
		}
	}else{
		redirect("_404","refresh");
	}	
}
public function unbookmark_jobsis($idloker){
	if($this->input->is_ajax_request()){
		$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$this->session->userdata('user_id')));
		if(!empty($cksiswa->result())){
			$id_loker = $this->asn->decode($idloker);
			$psid = $this->asn->anti($this->session->userdata('user_id'));
			$tgl = date("Y-m-d");
			$ckbookmark = $this->db->get_where('lowongan_bookmark',array('id_loker'=>$id_loker,'peserta_didik_id'=>$psid));
			if(!empty($ckbookmark->result())){
				$this->db->where('id_loker',$id_loker);
				$this->db->where('peserta_didik_id',$psid);
				$this->db->delete('lowongan_bookmark');
				echo json_encode(array("status" => TRUE));	
			}else{
				echo json_encode(array("status" => TRUE));	
			}	
		}else{
			echo json_encode(array("status" => FALSE));	
		}
	}else{
		redirect("_404","refresh");
	}	
}
public function getKompetensi(){
	if($this->input->is_ajax_request()){
		$sess = $this->session->userdata('id_pelamar');
		$page =  $_GET['page'];
		$offset = 10*$page;
		$limit = 10;
		$ckdata = $this->db->query("SELECT * FROM kompetensi_siswa WHERE user_id = '$sess' ORDER BY tgl_sertifikat DESC LIMIT $limit OFFSET $offset")->result();
		foreach ($ckdata as $row) {
			echo 	'<ul class="list-group list-group-lg no-radius list-email">
			<li class="list-group-item primary">
			<div class="email-info">
			<span class="email-time" title="Tanggal Sertifikat">
			'.date("d-m-Y",strtotime($row->tgl_sertifikat)).'
			</span>
			<h5 class="email-title" title="Nama Penyelenggara">
			'.$row->judul_kompetensi.'</br>
			<span class="label label-primary f-s-10" title="No Sertifikat">No Sertifikat : '.$row->no_sertifikat.'</span>
			<br/>
			<span class="label label-primary f-s-10" title="Kompetensi Inti">Kompetensi Inti : '.$row->kompetensi_inti.'</span>
			</h5>
			</div>
			</li>
			</ul>';
		}
		exit;
	}else{
		redirect("_404","refresh");
	}	
}
public function lokerku(){

		if($this->input->is_ajax_request()){
			$this->db1->query("refresh materialized view view_lowongan_tujuan");
			$this->db1->query("refresh materialized view view_lowongan");
			$this->db1->query("refresh materialized view view_lowongan_detil");
			$sess = $this->session->userdata('role_');
			$this->asn->cekSekolah($sess);
			$page =  $_GET['page'];
			$kategori = $_GET['kat'];
			$industri = $_GET['industri'];
			$cari = $_GET['cari'];
			$offset = 10*$page;
	        $limit = 10;
        	if($kategori!="" && $industri!="" && $cari!=""){
		        $ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND (kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
        	}elseif($kategori!="" && $industri!="" && $cari==""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND kode_perusahaan = '$industri' AND (status = '1' OR status = '9') LIMIT  $limit OFFSET $offset")->result();
        	}elseif($kategori!="" && $industri=="" && $cari!=""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT  $limit OFFSET $offset")->result();
        	}elseif($kategori=="" && $industri!="" && $cari!=""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_perusahaan = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
        	}elseif($kategori!="" && $industri=="" && $cari==""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
    		}elseif($kategori=="" && $industri!="" && $cari==""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_perusahaan = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
        	}elseif($kategori=="" && $industri=="" && $cari!=""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (judul LIKE '%$cari%') OR (deskripsi LIKE '%$cari%') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
        	}elseif($kategori==="" && $industri==="" && $cari===""){
        		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
        	}
        	$ckuser = $this->db1->get_where('app.user_sekolah',array('sekolah_id'=>$this->session->userdata('role_')))->result();
	        if(count($ckdata)>0){
	        	foreach ($ckdata as $row) {
					$ckper = $this->db1->get_where('ref.industri',array('industri_id'=>$row->kode_perusahaan));
					if(count($ckper->result())>0){
						$x = $ckper->row();
						$nama_perusahaan = $x->nama;
					}else{
						$nama_perusahaan = 'NotFound';
					}
					$cksekolah = $this->db1->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$row->dibuat_oleh'");
					if(count($cksekolah->result())>0){
						$u = $cksekolah->row();
						$sk = $u->nama_sekolah;
					}else{
						$sk = "NotFound";
					}
					echo '<li>
                        <div class="result-info">
                            <h4 class="title"><a href="'.base_url().'loker/detil_lokerku/'.$row->id.'">'.$row->judul.'</a></h4>
                            <p class="location" title="Nama Perusahaan | Sekolah Pembuat Lowongan | Tanggal Dibuat Lowongan">'.$nama_perusahaan.' | '.$sk.' |'.date("d-m-Y",strtotime($row->tgl_buat)).'</p>
                            <p class="desc">
                                ';
                                if(strlen($row->deskripsi) > 179){
					                echo ''.substr($row->deskripsi, 0,179).' ... <a href="'.base_url().'loker/detil_lokerku/'.$row->id.'"><b>more</b></a>';
				                }else{
				                	$tot = 200 - strlen($row->deskripsi);
				                	$hasil = $tot + strlen($row->deskripsi);
					                echo $row->deskripsi;
					                for ($i=0; $i < $hasil ; $i++) { 
					                	echo '&nbsp;';
					                }
				                }
        			echo'</p>
                            <div class="btn-row">
                                <a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fas fa-calendar" title="Tanggal Mulai : '.date("d-m-Y",strtotime($row->tgl_buka)).'"></i></a>
                                <a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fas fa-calendar-alt" title="Tanggal Selesai : '.date("d-m-Y",strtotime($row->tgl_tutup)).'"></i></a>
                                <a href="javascript:;" title="Total Pelamar : '.$this->db->get_where('bidding_lulusan',array('id_loker'=>$row->id))->num_rows().'" data-toggle="tooltip" data-container="body" data-title="Users"><i class="fa fa-fw fa-user"></i></a>
                            </div>
                        </div>
                        <div class="result-price">';
	                echo            '<small>Range Gaji : '.$row->range.'</small>';
	                if(date("Y-m-d") > $row->tgl_tutup){
		                echo '<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
	                }else{
		                echo '<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
	                }
	                if(date("Y-m-d") > $row->tgl_tutup){
	                	echo  '<p><a href="'.base_url().'loker/detil_lokerku/'.$row->id.'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
	                               </p></div>
	                            </li>';
	                }else{
	            		echo  '<p><a href="'.base_url().'loker/detil_lokerku/'.$row->id.'" class="btn btn-inverse btn-xs m-r-5">View Details</a><a href="javascript:void(0)" onclick="hapus_loker(\'Data Lowongan Pekerjaan\',\'loker\',\'hapus_data\','."'".$row->id."'".')" class="btn btn-danger btn-xs m-r-5">Hapus Loker</a>
			                               </p></div>
		                            </li>';	
	                }
				}
	        }else{
	        	echo '<div class="alert alert-danger m-b-15 m-t-15">
					Maaf, Lowongan Tidak Tersedia.
				</div>';
	        }
        	exit;
		}else{
			redirect("_404","refresh");
		}	
}

public function rbLowongan($id){
	$ch = $this->input->post('rbS');
	// $id = $this->input->post('idL');
	if ($this->db->query("UPDATE lowongan set status='$ch' WHERE id=".$id."")) {
		$data['response'] = 'sip';
	}else{
		$data['response'] = 'no';
	}
	echo json_encode($data);
}

public function hapus_data($kode=NULL){
	if($this->input->is_ajax_request()){
		$kodex = $kode;
		$ckbid = $this->db->get_where('bidding_lulusan',array('id_loker'=>$kodex))->result();
		if(count($ckbid)=="0"){
			$ckuserid = $this->db->query("SELECT id FROM lowongan WHERE id = '$kodex'");
			if(count($ckuserid->result())>0){
				$this->hapus_loker($kodex);
				$this->hapus_tujuan($kodex);
				$this->hapus_syarat($kodex);
				$this->hapus_detil($kodex);
				$this->hapus_lok($kodex);
				echo json_encode(array("status" => TRUE));
			}else{
				echo json_encode(array("status" => FALSE));
			}
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}else{
		redirect("_404","refresh");
	}
}
public function hapus_loker($kodex){
	$this->db->where('id',$kodex);
	$this->db->delete('lowongan');
}
public function hapus_tujuan($kodex){
	$this->db->where('id_loker',$kodex);
	$this->db->delete('lowongan_tujuan');
}
public function hapus_syarat($kodex){
	$this->db->where('id_loker',$kodex);
	$this->db->delete('lowongan_syarat');
}
public function hapus_detil($kodex){
	$this->db->where('id_loker',$kodex);
	$this->db->delete('lowongan_detil');
}
public function hapus_lok($kodex){
	$this->db->where('id_loker',$kodex);
	$this->db->delete('lowongan_lokasi');
}
public function edit_kriteria($kode=NULL){
	$this->session->set_userdata('xx',$kode);
	$kodex = $this->asn->decode($kode);
	$this->session->set_userdata('kodex',$kodex);
	$isi['id_loker_x'] = $kode;
	$ckuser = $this->db->get_where('app.user_sekolah',array('sekolah_id'=>$this->session->userdata('role_')))->result();
	if(count($ckuser)>0){
		$this->session->set_userdata('idna',$kodex);
		foreach ($ckuser as $xx) {
			$user_id = $xx->user_id;
			$ckdata = $this->db->query("SELECT * FROM lowongan WHERE id = '$kodex' AND dibuat_oleh = '$user_id' LIMIT 1")->result();
			foreach ($ckdata as $uy) {
				$isi['kode_loker'] = $kodex;
				$isi['default']['lintangO'] = $uy->lat;
				$isi['default']['bujurO'] = $uy->lon;
				$isi['tombolsimpan'] = 'Simpan';
				$isi['namamenu'] = "Edit Data Loker";
				$isi['page'] = "bursa";
				$isi['link'] = 'loker';
				$isi['tombolbatal'] = 'Batal';
				$isi['halaman'] = "Edit Data Lowongan";
				$isi['judul'] = "Halaman Data Lowongan";
				$isi['content'] = "form_edit_kriteria";
				$this->load->view("dashboard/dashboard_view",$isi);
			}
		}
	}else{
		redirect('_404',"refres");
	}
}
public function edit_syarat($id=NULL){
	$this->session->set_userdata('idnax',$id);
	$ckloker = $this->db->get_where('lowongan_syarat',array('id'=>$id));
	if(count($ckloker->result())>0){
		$row = $ckloker->row();
		$isi['default']['butuh'] = number_format($row->dibutuhkan);
		$isi['default']['tinggi_badan'] = number_format($row->tinggi_badan);
		$isi['default']['berat_badan'] = number_format($row->berat_badan);
		$isi['default']['umur'] = $row->umur;
		$isi['jk'] = $row->jns_kel;
		$isi['tombolsimpan'] = 'Simpan';
		$isi['namamenu'] = "Input Loker";
		$isi['page'] = "bursa";
		$isi['kode'] = $this->asn->decode($id);
		$isi['link'] = 'loker';
		$isi['tombolbatal'] = 'Batal';
		$isi['halaman'] = "Edit Syarat Pelamar";
		$isi['judul'] = "Halaman Edit Syarat Pelamar";
		$isi['content'] = "form_edit_syarat";
		$this->load->view("dashboard/dashboard_view",$isi);
	}else{
		redirect("_404","refresh");
	}
}
public function add_syarat($id=NULL){
	$kode_loker = $this->session->userdata('kodex');
	$cksyarat = $this->db->get_where('lowongan_syarat',array('id_loker'=>$kode_loker))->result();
	if(count($cksyarat)<2){
		$this->session->set_userdata('idnax',$id);
		$isi['tombolsimpan'] = 'Simpan';
		$isi['namamenu'] = "Input Loker";
		$isi['page'] = "bursa";
		$isi['jk'] = "";
		$isi['kode'] = $this->asn->decode($id);
		$isi['link'] = 'loker';
		$isi['tombolbatal'] = 'Batal';
		$isi['halaman'] = "Edit Syarat Pelamar";
		$isi['judul'] = "Halaman Edit Syarat Pelamar";
		$isi['content'] = "form_add_syarat";
		$this->load->view("dashboard/dashboard_view",$isi);
	}else{
		redirect("_404","refresh");
	}
}
public function proses_edit_syarat(){
	$jns_kel = $_POST['jk'];
	$jml = $_POST['butuh'];
	$tinggi = $_POST['tinggi_badan'];
	$berat = $_POST['berat_badan'];
	$umur = $_POST['umur'];
	$simpan_syarat = array('dibutuhkan'=>$jml,
		'jns_kel'=>$jns_kel,
		'tinggi_badan'=>$tinggi,
		'berat_badan'=>$berat,
		'umur'=>$umur);
	$this->db1->where('id',$this->session->userdata('idnax'));
	$this->db1->update('lowongan_syarat',$simpan_syarat);
	redirect('loker/edit_kriteria/'.$this->session->userdata('xx'));
}
public function hapus_syaratx($id=NULL){
	$this->db->where('id',$id);
	$this->db->delete('lowongan_syarat');
	redirect($_SERVER["HTTP_REFERER"]);
}
public function proses_add_syarat(){
	$jns_kel = $_POST['jk'];
	$jml = $_POST['butuh'];
	$tinggi = $_POST['tinggi_badan'];
	$berat = $_POST['berat_badan'];
	$umur = $_POST['umur'];
	$simpan_syarat = array('dibutuhkan'=>$jml,
		'jns_kel'=>$jns_kel,
		'tinggi_badan'=>$tinggi,
		'berat_badan'=>$berat,
		'umur'=>$umur,
		'id_loker'=>$this->asn->decode($this->session->userdata('idnax')));
	$this->db1->insert('lowongan_syarat',$simpan_syarat);
	redirect('loker/edit_kriteria/'.$this->session->userdata('xx'));
}
public function proses_edit(){
	$kat_loker = $this->input->post('kat_loker');
	$jurusan = $this->input->post('jurusan');
	$simpanloker = array('kode_bidang'=>$kat_loker,
		'kode_spesialisasi'=>$jurusan);
	$this->db->where('id_loker',$this->input->post('id_loker'));
	$this->db->update('lowongan_detil',$simpanloker);
	$prov = $this->input->post('provinsi');
	$kota = $this->input->post('kabupaten');
	$alamat = $this->input->post('alamat');
	$lat = $this->input->post('lintangO');
	$lon = $this->input->post('bujurO');
	$alamat = $this->input->post('alamatbaru');
	$kategori = $this->input->post('kat_loker');
	$simpanloker = array('kode_prov'=>$prov,
		'kode_kota'=>$kota,
		'lat'=>$lat,
		'lon'=>$lon,
		'alamat'=>$alamat);
	$this->db->where('id',$this->input->post('id_loker'));
	$this->db->update('lowongan',$simpanloker);
	redirect('loker/edit_kriteria/'.$this->session->userdata('xx'));
}
public function hapus_detilna($id){
	if($this->input->is_ajax_request()){
		$this->db->where('id',$id);
		$this->db->delete('lowongan_detil');
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
public function hapus_kategori($id){
	if($this->input->is_ajax_request()){
		$this->db->where('id',$id);
		$this->db->delete('lowongan_kategori');
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
public function hapus_lokasi($id){
	if($this->input->is_ajax_request()){
		$this->db->where('id',$id);
		$this->db->delete('lowongan_lokasi');
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
public function edit_kat($id){
	if($this->input->is_ajax_request()){
		$data = $this->loker_model->get_by_id_kat($id);
		echo json_encode($data);
	}else{
		redirect("_404","refresh");
	}
}
public function edit_det($id){
	if($this->input->is_ajax_request()){
		$data = $this->loker_model->get_by_id_det($id);
		echo json_encode($data);
	}else{
		redirect("_404","refresh");
	}
}
public function edit_lokasi($id){
	if($this->input->is_ajax_request()){
		$data = $this->loker_model->get_by_id_lokasi($id);
		echo json_encode($data);
	}else{
		redirect("_404","refresh");
	}
}
public function proses_edit_kat(){
	$data = array('kode_bidang' => $this->asn->anti(htmlspecialchars(strtoupper($this->input->post('kategori')))));
	$this->loker_model->update_kategori(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	echo json_encode(array("status" => TRUE));
}
public function proses_edit_det(){
	$data = array('kode_jurusan' => $this->asn->anti(htmlspecialchars(strtoupper($this->input->post('jurusan')))));
	$this->loker_model->update_detil(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	echo json_encode(array("status" => TRUE));
}
public function proses_edit_lokasi(){
	$data = array('kode_prov' => $this->asn->anti(htmlspecialchars(strtoupper($this->input->post('lokasi')))));
	$this->loker_model->update_lokasi(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	echo json_encode(array("status" => TRUE));
}
public function proses_add_kat(){
	if($this->input->is_ajax_request()){
		$this->_validasi();
		$id_loker = $this->session->userdata('id_loker_');
		$data = array('kode_bidang' => $this->input->post('kategori'),
			'id_loker'=>$id_loker);
		$insert = $this->loker_model->simpan_kategori($data);
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
public function proses_add_det(){
	if($this->input->is_ajax_request()){
		$id_loker = $this->session->userdata('id_loker_');
		$data = array('kode_jurusan' => $this->input->post('jurusan'),
			'id_loker'=>$id_loker);
		$insert = $this->loker_model->simpan_det($data);
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
private function _validasi(){
	$data = array();
	$data['error_string'] = array();
	$data['inputerror'] = array();
	$data['status'] = TRUE;
	if($this->input->post('kategori') == ''){
		$data['inputerror'][] = 'kategori';
		$data['error_string'][] = 'kategori harus di isi.';
		$data['status'] = FALSE;
	}
	if($data['status'] === FALSE){
		echo json_encode($data);
		exit();
	}
}
public function proses_add_lokasi(){
	if($this->input->is_ajax_request()){
		$id_loker = $this->session->userdata('id_loker_');
		$data = array('kode_prov' => $this->input->post('lokasi'),
			'id_loker'=>$id_loker);
		$insert = $this->loker_model->simpan_lokasi($data);
		echo json_encode(array("status" => TRUE));
	}else{
		redirect("_404","refresh");
	}
}
}
