<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/filter_skl.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnyR-SfN_ycHfdpi6oEQF-VqkhLYjqwQ4"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
        TableManageResponsive.init();
        table = $('#data-news').DataTable({});
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-validation-2">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                <?php
                $cksyarat = $this->db->get_where('lowongan_syarat',array('id_loker'=>$kode_loker))->result();
                if(count($cksyarat)<2){
                    ?>                    
                    <a href="<?php echo base_url();?>loker/add_syarat/<?php echo $id_loker_x;?>" id="add_btn" title="Tambah Kriteria Calon Pencari Kerja" class="btn btn-primary btn-xs m-r-5">Tambah Kriteria Calon Pencari Kerja</a>
                    <?php
                }
                ?>
                </div>
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body panel-form">
                <form name="form" id="form" action="<?php echo base_url();?>loker/proses_edit" class="form-horizontal form-bordered" method="post">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="data-news" class="table table-striped table-bordered nowrap" width="200%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center" width="1%">No.</th>
                                                <th style="text-align:center" width="20%">Jns Kelamin</th>
                                                <th style="text-align:center" width="20%">Jumlah Penerimaan</th>
                                                <th style="text-align:center" width="20%">Tinggi Badan</th>
                                                <th style="text-align:center" width="20%">Berat Badan</th>
                                                <th style="text-align:center" width="20%">Umur</th>
                                                <th style="text-align:center" width="5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ii = 0;
                                        $cksyarat = $this->db->get_where('lowongan_syarat',array('id_loker'=>$kode_loker))->result();
                                        foreach ($cksyarat as $xxx) {
                                            $id = $xxx->id;
                                            if($xxx->jns_kel=='1'){
                                                $jns = "Laki-Laki";
                                            }elseif($xxx->jns_kel=='2'){
                                                $jns = "Perempuan";
                                            }else{
                                                $jns = "ALL";
                                            }
                                            $ii++;
                                            ?> 
                                            <tr class="odd gradeX">
                                                <td width="1%" style="text-align:center"><?php echo $ii . "." ;?></td>
                                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->dibutuhkan)." Orang" ;?></td>
                                                <td width="20%" style="text-align:center"><?php echo $jns ;?></td>
                                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->tinggi_badan)." cm" ;?></td>
                                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->berat_badan)." Kg" ;?></td>
                                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->umur)." Tahun" ;?></td>
                                                <td style="text-align:center" width="20%">
                                                    <a href="<?php echo base_url();?>loker/edit_syarat/<?php echo $id;?>" data-toggle="tooltip" class="btn btn-warning btn-xs m-r-5" title='Edit Data'><i class="icon-pencil icon-white"></i></a>
                                                    <a href="<?php echo base_url();?>loker/hapus_syaratx/<?php echo $id;?>" data-toggle="tooltip" class="btn btn-danger btn-xs m-r-5" title='Hapus Data'><i class="icon-remove icon-white"></i></a>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
