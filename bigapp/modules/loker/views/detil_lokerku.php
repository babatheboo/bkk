<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/detil_loker.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
        
    });
    
</script>
<div class="profile-container">
    <div class="profile-section">
        <div class="profile-left">
            <div></div>
        </div>
        <div class="profile-center">
            <div class="profile-info" style="background-color: #fff">
                <div class="table-responsive">
                    <table class="table table-profile">
                        <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h4><?php echo $nama;?></h4>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="field">Judul</td>
                                <td class="bg-gradient-indigo "><?php echo "<b>" . $judul_loker . "</b>";?></td>
                            </tr>
                            <tr>
                                <td class="field">Kategori</td>
                                <td>
                                    <?php
                                    $ckkategori = $this->db->query("SELECT a.id,a.kode_bidang,b.nama_bidang_usaha FROM lowongan_kategori a JOIN ref.bidang_usaha b ON a.kode_bidang = b.bidang_usaha_id WHERE a.id_loker = '$kode_loker' GROUP BY a.kode_bidang,a.id,b.nama_bidang_usaha")->result();
                                    foreach ($ckkategori as $key) {
                                        echo '<small><button title="Kategori Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->nama_bidang_usaha . '</button><button href="javascript:void(0)" onclick="hapus_kat(\'Data Kategori Lowongan Pekerjaan\',\'loker\',\'hapus_kategori\','."'".$key->id."'".')" title="Hapus Kategori Lowongan Pekerjaan" class="btn btn-danger btn-xs m-r-5"><i class="fas fa-trash"></i></button><a class="btn btn-xs m-r-5 btn-warning" href="javascript:void(0)" title="Edit Data" onclick="edit_kat(\'Data Kategori Lowongan Pekerjaan\',\'loker\',\'edit_kat\','."'".$key->id."'".')"><i class="fas fa-pencil-alt"></i></a></small>' . "<br/><br/>";
                                    }
                                    ?>
                                    <button class="btn btn-info btn-xs m-r-5" onclick="tambah_data()"><i class="fa fa-plus-circle"></i> Tambah Data Kategori Lowongan</button>
                                </td>
                            </tr>
                            <tr>
                                <td class="field">Spesifikasi</td>
                                <td>
                                    <?php
                                    $ckkategori = $this->db->query("SELECT a.id,a.kode_jurusan,b.nama_jurusan FROM lowongan_detil a JOIN ref.jurusan b ON a.kode_jurusan = b.jurusan_id WHERE a.id_loker = '$kode_loker'")->result();
                                    foreach ($ckkategori as $key) {
                                        echo '<small><button title="Spesifikasi Jurusan Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->nama_jurusan . '</button><button href="javascript:void(0)" onclick="hapus_kat(\'Data Spesifikasi Jurusan\',\'loker\',\'hapus_detilna\','."'".$key->id."'".')" title="Hapus Spesifikasi Jurusan Lowongan Pekerjaan" class="btn btn-danger btn-xs m-r-5"><i class="fas fa-trash"></i></button><a class="btn btn-xs m-r-5 btn-warning" href="javascript:void(0)" title="Edit Data" onclick="edit_det(\'Data Spesifikasi Jurusan Lowongan Pekerjaan\',\'loker\',\'edit_det\','."'".$key->id."'".')"><i class="fas fa-pencil-alt"></i></a></small>' . "<br/><br/>";
                                    }
                                    ?>
                                    <button class="btn btn-info btn-xs m-r-5" onclick="tambah_data_det()"><i class="fa fa-plus-circle"></i> Tambah Data Spesifikasi Lowongan</button>
                                </td>
                            </tr>
                            <tr>
                                <td class="field">Gaji</td>
                                <td><b><?php echo $gaji;?></b></td>
                            </tr>
                            <tr>
                                <td class="field">Tgl dibuka</td>
                                <td><?php echo $tglbuka;?></td>
                            </tr>
                            <tr>
                                <td class="field">Tgl ditutup</td>
                                <td><?php echo $tgltutup;?></td>
                            </tr>
                            <tr>
                                <td class="field">Pelamar</td>
                                <td><?php echo $this->db->get_where('bidding_lulusan',array('id_loker'=>$kode_loker))->num_rows();?> Orang</td>
                            </tr>
                            <tr>
                                <td class="field">Status</td>
                                <td><?php echo $status;?></td>
                            </tr>
                            <tr>
                                <td class="field">Dibuat</td>
                                <td><?php echo $dibuat_oleh;?></td>
                            </tr>
                            <tr>
                                <td class="field">Status</td>
                            <?php
                            $ckjadwal = $this->db->get_where('jadwal_tes',array('id_loker'=>$kode_loker))->result();
                            if(count($ckjadwal)>0){
                                ?>
                                    <td>Sudah Ada Jadwal Tes</td>
                                <?php
                            }else{
                                ?>
                                    <td>Belum Ada Jadwal Tes</td>
                                <?php
                            }
                            ?>
                            </tr>
                            <tr>
                                <td class="field">Provinsi</td>
                                <td>
                                    <?php
                                    $ckrpo = $this->db->query("SELECT a.id,a.kode_prov,b.prov FROM lowongan_lokasi a JOIN ref.view_provinsi b ON a.kode_prov = b.kode_prov WHERE a.id_loker = '$kode_loker' GROUP BY a.id,a.kode_prov,b.prov")->result();
                                    foreach ($ckrpo as $key) {
                                        echo '<small><button title="Lokasi Penempatan Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->prov . '</button><button href="javascript:void(0)" onclick="hapus_kat(\'Data Lokasi Penempatan Lowongan\',\'loker\',\'hapus_lokasi\','."'".$key->id."'".')" title="Hapus Lokasi Penempatan Jurusan Lowongan Pekerjaan" class="btn btn-danger btn-xs m-r-5"><i class="fas fa-trash"></i></button><a class="btn btn-xs m-r-5 btn-warning" href="javascript:void(0)" title="Edit Data" onclick="edit_lokasi(\'Data Lokasi Penempatan Lowongan Pekerjaan\',\'loker\',\'edit_lokasi\','."'".$key->id."'".')"><i class="fas fa-pencil-alt"></i></a></small>' . "<br/><br/>";
                                    }
                                    ?>
                                    <button class="btn btn-info btn-xs m-r-5" onclick="tambah_data_lokasi()"><i class="fa fa-plus-circle"></i> Tambah Provinsi Penempatan Lowongan</button>
                                </td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                            <div class="panel-heading"> <div class="panel-heading-btn">
<a href = '<?php echo base_url();?>loker/edit_loker/<?php echo $id_loker;?>' class="btn btn-primary btn-xs m-r-5">
<i class="icon-pencil"></i> Edit Data Lowongan</a>&nbsp;
</div> 
<br/></div>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <h4 class="panel-title">Deskripsi Lowongan Kerja</h4>
            </div>
            <div class="panel-body">
                <div data-scrollbar="true" data-height="280px">
                        <?php echo $deskripsi;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <div class="panel-heading-btn"> <a href="<?php echo base_url();?>loker/edit_kriteria/<?php echo $this->session->userdata('idx');?>" class="btn btn-primary btn-xs m-r-5">Edit Data Kriteria</a> </div>
                </div>
                <?php
                $butuhtot = $this->db->query("SELECT SUM(dibutuhkan::int) as total FROM lowongan_syarat WHERE id_loker = '$kode_loker'");
                $x = $butuhtot->row();
                ?>
                <h4 class="panel-title">Deskripsi Total Yang Dibutuhkan : <?php echo number_format($x->total) . " Orang";?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-syarat" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="20%">Dibutuhkan</th>
                                <th style="text-align:center" width="20%">Jenis Kelamin</th>
                                <th style="text-align:center" width="20%">Tinggi Badan</th>
                                <th style="text-align:center" width="20%">Berat Badan</th>
                                <th style="text-align:center" width="20%">Umur</th>
                            </tr>
                        </thead>
                        <tbody> 
                        <?php
                        $ii = 0;
                        $cksyarat = $this->db->get_where('lowongan_syarat',array('id_loker'=>$kode_loker))->result();
                        foreach ($cksyarat as $xxx) {
                            if($xxx->jns_kel=='1'){
                                $jns = "Laki-Laki";
                            }elseif($xxx->jns_kel=='2'){
                                $jns = "Perempuan";
                            }else{
                                $jns = "ALL";
                            }
                            $ii++;
                            ?> 
                            <tr class="odd gradeX">
                                <td width="1%" style="text-align:center"><?php echo $ii . "." ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->dibutuhkan)." Orang" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo $jns ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->tinggi_badan)." cm" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->berat_badan)." Kg" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->umur)." Tahun" ;?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Informasi Data Pelamar : <?php echo number_format($this->db->query("SELECT * FROM bidding_lulusan WHERE id_loker = '$kode_loker'")->num_rows()) . " Orang";?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-pelamar" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="15%">Foto Siswa</th>
                                <th style="text-align:center" width="10%">NISN</th>
                                <th style="text-align:center" width="40%">Nama Siswa</th>
                                <th style="text-align:center" width="30%">Asal Sekolah</th>
                                <th style="text-align:center" width="9%">Umur</th>
                            </tr>
                        </thead>
                        <tbody> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Pilih Kategori</label>
                            <div class="col-md-6">
                                <?php echo form_dropdown('kategori',$option_kategori,isset($default['kategori']) ? $default['kategori'] : '','id="kategori" style="width:100%" name="kategori" data-live-search="true" data-style="btn-white" class="select"');?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSimpan" onclick="save('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_det" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form_det" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Spesifikasi Jurusan </label>
                            <div class="col-md-8">
                                <?php echo form_dropdown('jurusan',$option_jurusan,isset($default['jurusan']) ? $default['jurusan'] : '','id="jurusan" style="width:100%" name="jurusan" data-live-search="true" data-style="btn-white" class="select"');?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSimpan_det" onclick="update_det('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_lokasi" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form_lokasi" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Provinsi </label>
                            <div class="col-md-8">
                                <?php echo form_dropdown('lokasi',$option_lokasi,isset($default['lokasi']) ? $default['lokasi'] : '','id="lokasi" style="width:100%" name="lokasi" data-live-search="true" data-style="btn-white" class="select"');?>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSimpan_lokasi" onclick="update_lokasi('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>