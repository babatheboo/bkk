<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-inverse" data-sortable-id="ui-general-1">
	        <div class="panel-heading">
	            <h4 class="panel-title">Informasi</h4>
	        </div>
	        <div class="panel-body">
				<div class="alert alert-danger m-b-15">
					<strong>Error!</strong><br>
					Maaf saat ini anda belum mempunyai hak untuk membuat lowongan pekerjaan.<br/>
					Untuk bisa membuat lowongan pekerjaan pastikan anda sudah memiliki mitra industri.
				</div>
	        </div>
	    </div>
	</div>
</div>
