<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/detil_loker.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="profile-container">
    <div class="profile-section">
        <div class="profile-left">
            <div></div>
        </div>
        <div class="profile-center">
            <div class="profile-info">
                <div class="table-responsive">
                    <table class="table table-profile">
                        <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h4><?php echo $nama;?></h4>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="highlight">
                                <td class="field">Judul</td>
                                <td><?php echo "<b>" . $judul_loker . "</b>";?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Kategori</td>
                                <td>
                                    <?php
                                    $ckkategori = $this->db->query("SELECT a.id,a.kode_bidang,b.nama_bidang_usaha FROM lowongan_kategori a JOIN ref.bidang_usaha b ON a.kode_bidang = b.bidang_usaha_id WHERE a.id_loker = '$kode_loker' GROUP BY a.kode_bidang,a.id,b.nama_bidang_usaha")->result();
                                    foreach ($ckkategori as $key) {
                                        echo '<small><button title="Kategori Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->nama_bidang_usaha . '</button></small>' . "<br/><br/>";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Spesifikasi</td>
                                <td>
                                    <?php
                                    $ckkategori = $this->db->query("SELECT a.id,a.kode_jurusan,b.nama_jurusan FROM lowongan_detil a JOIN ref.jurusan b ON a.kode_jurusan = b.jurusan_id WHERE a.id_loker = '$kode_loker'")->result();
                                    foreach ($ckkategori as $key) {
                                        echo '<small><button title="Spesifikasi Jurusan Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->nama_jurusan . '</button></small>' . "<br/><br/>";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Gaji</td>
                                <td><b><?php echo $gaji;?></b></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Tgl dibuka</td>
                                <td><?php echo $tglbuka;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Tgl ditutup</td>
                                <td><?php echo $tgltutup;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Pelamar</td>
                                <td><?php echo $this->db->get_where('bidding_lulusan',array('id_loker'=>$kode_loker))->num_rows();?> Orang</td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Status</td>
                                <td><?php echo $status;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Dibuat</td>
                                <td><?php echo $dibuat_oleh;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Status</td>
                            <?php
                            $ckjadwal = $this->db->get_where('jadwal_tes',array('id_loker'=>$kode_loker))->result();
                            if(count($ckjadwal)>0){
                                ?>
                                    <td>Sudah Ada Jadwal Tes</td>
                                <?php
                            }else{
                                ?>
                                    <td>Belum Ada Jadwal Tes</td>
                                <?php
                            }
                            ?>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Provinsi</td>
                                <td>
                                    <?php
                                    $ckrpo = $this->db->query("SELECT a.id,a.kode_prov,b.prov FROM lowongan_lokasi a JOIN ref.view_provinsi b ON a.kode_prov = b.kode_prov WHERE a.id_loker = '$kode_loker' GROUP BY a.id,a.kode_prov,b.prov")->result();
                                    foreach ($ckrpo as $key) {
                                        echo '<small><button title="Lokasi Penempatan Lowongan Pekerjaan" class="btn btn-primary btn-xs m-r-5">' . $key->prov . '</button></small>' . "<br/><br/>";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <h4 class="panel-title">Deskripsi Lowongan Kerja</h4>
            </div>
            <div class="panel-body">
                <div data-scrollbar="true" data-height="280px">
                        <?php echo $deskripsi;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <?php
                $butuhtot = $this->db->query("SELECT SUM(dibutuhkan::int) as total FROM lowongan_syarat WHERE id_loker = '$kode_loker'");
                $x = $butuhtot->row();
                ?>
                <h4 class="panel-title">Deskripsi Total Yang Dibutuhkan : <?php echo number_format($x->total) . " Orang";?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-syarat" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="20%">Dibutuhkan</th>
                                <th style="text-align:center" width="20%">Jenis Kelamin</th>
                                <th style="text-align:center" width="20%">Tinggi Badan</th>
                                <th style="text-align:center" width="20%">Berat Badan</th>
                                <th style="text-align:center" width="20%">Umur</th>
                            </tr>
                        </thead>
                        <tbody> 
                        <?php
                        $ii = 0;
                        $cksyarat = $this->db->get_where('lowongan_syarat',array('id_loker'=>$kode_loker))->result();
                        foreach ($cksyarat as $xxx) {
                            if($xxx->jns_kel=='1'){
                                $jns = "Laki-Laki";
                            }elseif($xxx->jns_kel=='2'){
                                $jns = "Perempuan";
                            }else{
                                $jns = "ALL";
                            }
                            $ii++;
                            ?> 
                            <tr class="odd gradeX">
                                <td width="1%" style="text-align:center"><?php echo $ii . "." ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->dibutuhkan)." Orang" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo $jns ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->tinggi_badan)." cm" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->berat_badan)." Kg" ;?></td>
                                <td width="20%" style="text-align:center"><?php echo number_format($xxx->umur)." Tahun" ;?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Informasi Data Pelamar : <?php echo number_format($this->db->query("SELECT * FROM bidding_lulusan WHERE id_loker = '$kode_loker'")->num_rows()) . " Orang";?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-pelamar" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th style="text-align:center" width="1%">No.</th>
                                <th style="text-align:center" width="15%">Foto Siswa</th>
                                <th style="text-align:center" width="10%">NISN</th>
                                <th style="text-align:center" width="40%">Nama Siswa</th>
                                <th style="text-align:center" width="30%">Asal Sekolah</th>
                                <th style="text-align:center" width="9%">Umur</th>
                            </tr>
                        </thead>
                        <tbody> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
