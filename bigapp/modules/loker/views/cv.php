<script src="<?php echo base_url();?>assets/backend/plugins/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        getKompetensi(0);
        $("#reload_kompetensi").click(function(e){
            e.preventDefault();
            var page = $(this).data('val');
            getKompetensi(page);
        });
    });
    var getKompetensi = function(page){
        $("#loader").show();
        $.ajax({
            url:$BASE_URL+"loker/getKompetensi",
            type:'GET',
            data: {page:page}
        }).done(function(response){
            $("#result-kompetensi").append(response);
            $("#loader").hide();
            $('#reload_kompetensi').data('val', ($('#reload_kompetensi').data('val')+1));
        });
    };
</script>
<?php
$cksiswa      = $this->db->query("SELECT * FROM ref.peserta_didik a JOIN ref.jurusan_sp b ON a.jurusan_id = b.jurusan_id WHERE a.peserta_didik_id = '$kode'");
$row          = $cksiswa->row();
$ckjrs        = $this->db->get_where('ref.jurusan_sp',array('jurusan_id'=>$row->jurusan_id));
$a            = $ckjrs->row();
$ckdetilsiswa = $this->db->query("SELECT * FROM ref.peserta_didik_detil WHERE peserta_didik_id = '$kode'");
$xs           = $ckdetilsiswa->result();
$xx           = $ckdetilsiswa->row();
$ckfoto       = $this->db->get_where('foto_siswa',array('peserta_didik_id'=>$kode));
if(count($cksiswa->result())>0){
    $foto = $row->photo;
    if($foto!=NULL){
        $fotox = $foto;
    }else{
        $fotox = "no.jpg";
    }
}else{
    $fotox = "no.jpg";
}
$nama_siswa = $row->nama;
$umur       = $this->db->query("SELECT date_part('year'::text, age((b.tanggal_lahir)::timestamp with time zone)) AS umur FROM ref.peserta_didik b WHERE peserta_didik_id='$kode'");
$xu         = $umur->row();
?>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                 <!--  <a href="<?php echo base_url();?><?php echo $link;?>/downloadcv" title="Download CV" class="btn btn-primary btn-xs m-r-5">Download CV</a>  -->
             </div> 
             <h4 class="panel-title">Curicullum Vitae</h4> 
         </div> 
     </div>
     <div class="profile-container">
        <div class="profile-section">
            <div class="profile-left">
                <div class="profile-image">
                    <img style="width: 100%; height: 100%;" src="<?php echo base_url();?>assets/foto/siswa/<?php echo $fotox;?>" />
                    <i class="fa fa-user hide"></i>
                </div>
            </div>
            <div class="profile-center">
                <div class="profile-info">
                    <div class="table-responsive">
                        <table class="table table-profile">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>
                                        <h4><?php echo $row->nama;?></h4>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="highlight">
                                    <td class="field">Tempat Lahir</td>
                                    <td><?php echo $row->tempat_lahir;?></td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Tanggal Lahir</td>
                                    <td><?php echo date('d-m-Y',strtotime($row->tanggal_lahir));?></td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Umur</td>
                                    <td><?php echo $xu->umur . " Tahun";?></td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Jenis Kelamin</td>
                                    <td><?php echo $row->jenis_kelamin;?></td>
                                </tr>
                                <?php
                                if (count($xs)>0) {
                                    ?>
                                    <tr class="highlight">
                                        <td class="field">Alamat</td>
                                        <td><?php echo $xx->alamat_jalan . ' RT.' . $xx->rt . " RW." . $xx->rw;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Nama Dusun</td>
                                        <td><?php echo $xx->nama_dusun;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Kelurahan</td>
                                        <td><?php echo $xx->desa_kelurahan;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Kode POS</td>
                                        <td><?php echo $xx->kode_pos;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">No Tlp Rumah</td>
                                        <td><?php echo $xx->nomor_telepon_rumah;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">No Tlp Seluler</td>
                                        <td><?php echo $xx->nomor_telepon_seluler;?></td>
                                    </tr>
                                    <?php
                                }else{
                                 ?>
                                 <tr class="highlight">
                                    <td class="field">Alamat</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Nama Dusun</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Kelurahan</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Kode POS</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">No Tlp Rumah</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">No Tlp Seluler</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                            <?php }
                            ?>
                            <tr class="highlight">
                                <td class="field">E-mail</td>
                                <?php
                                if ($row->email == '') {
                                    echo "<td class='text-danger'>TIDAK TERCANTUM</td>";
                                }else{
                                    echo '<td>'.$row->email.'</td>';
                                }
                                ?>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Jurusan</td>
                                <td><?php echo $a->nama_jurusan_sp;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Status Siswa</td>
                                <td>
                                    <?php
                                    if($row->jenis_keluar_id=='1'){
                                        echo "Lulus";
                                    }else{
                                        echo "Pelajar";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<br/>
<?php
$hasil =  $this->db->get_where('app.mbti_result',array('user_id'=>$kode));
if(count($hasil->result())>0){
  $row = $hasil->row();
  $mbti = $row->assesment_result;
  $mbtiresult = $this->db->get_where('app.mbti_kode',array('mbti_kode'=>$mbti));
  if(count($mbtiresult->result())>0){
     $row = $mbtiresult->row();
     $mbtialias = $row->mbti_alias;
     $mbtiname = $row->mbti_name;
     $mbtir = $row->mbti_result;
     $disc = $row->disc_model;
 }
 $psalias = explode("-", $mbtialias);
 $pisah = explode("|", $mbtir);
 $pdisc = explode("|", $disc);
 $D=0;
 $I=0;
 $S=0;
 $C=0;
 for ($f=0; $f < count($pdisc) ; $f++) { 
    if($pdisc[$f]=="DOMINANT"){
        $D++;
    }
    if($pdisc[$f]=="INSPIRING"){
        $I++;
    }
    if($pdisc[$f]=="STEADYNESS"){
        $S++;
    }
    if($pdisc[$f]=="COMPLIANCE"){
        $C++;
    }
}
$D = $D*10;
$I = $I*10;
$S = $S*10;
$C = $C*10;
if($D==0){
    $D = 11;
    $I = $I-3;
    $S = $S-3;
    $C = $C-3;
}
if($I==0){
    $D = $D-3;
    $I = 11;
    $S = $S-3;
    $C = $C-3;
}
if($S==0){
    $D = $C-3;
    $I = $I-3;
    $S = 11;
    $C = $C-3;
}
if($C==0){
    $D = $C-3;
    $I = $I-3;
    $S = $S-3;
    $C = 11;
}
$mtk = 0;
$ing = 0;
$ind = 0;
$prd = 0;
$cknilai = $this->db->get_where('nilai_lulusan',array('peserta_didik_id'=>$kode));
if(count($cknilai->result())>0){
    $a = $cknilai->row();
    $mtk = $a->mat;
    $ing = $a->ing;
    $ind = $a->ind;
    $prd = $a->prd;
}else{
    $mtk = 0;
    $ing = 0;
    $ind = 0;
    $prd = 0;
}
?>
<div class="row"> 
    <div class="col-md-6"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <h4 class="panel-title"><center>MBTI</center></h4>
                <?php echo '<center><small>'.$psalias[0] . "-" . $psalias[1] . "-" . $psalias[2] . "-" . $psalias[3].'</small></center>';?>
            </div>
            <div class="panel-body">
                <h5 style="text-align: center;"><?php echo $mbtiname;?></h5>    
                <div id="mbti-chart" class="height-sm"></div>
            </div> 
        </div>
    </div>
    <div class="col-md-6"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <h4 class="panel-title"><center>DISC</center></h4>
                <center><small>Dominant-Inspiring-Steadyness-Compliance</small></center>
            </div> 
            <div class="panel-body">
                <h5 style="text-align: center;">DISC</h5>    
                <div id="disc-chart" class="height-sm"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <h4 class="panel-title"><center>Kompetensi Inti</center></h4>
            </div> 
            <div class="panel-body">
                <div id="disc-nilai" class="height-sm"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Kompetensi Siswa</h4>
            </div>
            <div class="panel-body">
                <p style="text-align:center">
                    Informasi Kompetensi <?php echo $nama_siswa;?>
                </p>
                <div>
                    <div data-scrollbar="true" data-height="273px">
                        <div id="result-kompetensi"></div>
                        <?php
                        if(count($jml_kompetensi)>3){
                            ?>
                        </br>
                        <button id="reload_loker_" data-val = "0" style="text-align: center;width: 100%" class="btn btn-xs m-r-5 btn-info"><i class="fa fa-refresh"></i> Lainnya <img style="display: none" id="loader" src="<?php echo base_url();?>assets/img/loader.gif"></button>
                        <?php    
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #mbti-chart{
        left: 120px;
        width:250px;
        height:250px;
    }
    #disc-chart{
        left: 120px;
        width:250px;
        height:250px;
    }
    #disc-nilai{
        left: 120px;
        width:250px;
        height:250px;
    }
</style>
<script type="text/javascript">
    var datambti = [
    { label: "<?php echo $psalias[1];?>", data: 1, color: "#005CDE" },
    { label: "<?php echo $psalias[2];?>", data: 1, color: "#00A36A" },
    { label: "<?php echo $psalias[3];?>", data: 1, color: "#7D0096" },
    { label: "<?php echo $psalias[0];?>", data: 1, color: "#2dcb06" },
    ];
    var datadisc = [
    { label: "I", data: <?php echo $I;?>, color: "#f3fa24" },
    { label: "S", data: <?php echo $S;?>, color: "#110bd5" },
    { label: "C", data: <?php echo $C;?>, color: "#39a5fa" },
    { label: "D", data: <?php echo $D;?>, color: "#2dcb06" },
    ];
    var datanilai = [
    { label: "MTK", data: <?php echo $mtk;?>, color: "#f3fa24" },
    { label: "ING", data: <?php echo $ing;?>, color: "#110bd5" },
    { label: "IND", data: <?php echo $ind;?>, color: "#39a5fa" },
    { label: "PRD", data: <?php echo $prd;?>, color: "#2dcb06" },
    ];
    $.plot('#mbti-chart', datambti, {
        series: {
            pie: {
                innerRadius: 0.5,
                radius:1,
                show: true,
                label: {
                    show: true, 
                    radius:0.85,             
                    formatter: function (label, series) {
                        return '<div style="font-size:10pt;text-align:center;padding:2px;color:black;"><strong>' + label +'</strong></div>';
                    },
                    threshold: 0.1
                },     
                combine:{color:"#999",threshold:.1}
            }
        },
        grid:{borderWidth:2,hoverable:!0,clickable:!0},legend:{show:!1}
    });
    $.plot('#disc-chart', datadisc, {
        series: {
            pie: {
                innerRadius: 0.5,
                show: true,
                radius:1,
                label: {
                    show: true, 
                    radius:0.7,
                    formatter: function (label, series) {
                        return '<div style="font-size:12pt;text-align:center;padding:12px;color:black;"><strong>' + label + " : " + Math.round(series.percent) +'% </strong></div>';
                    },
                    threshold: 0.1              
                },     
                combine:{color:"#999",threshold:.1}
            }
        },
        grid:{borderWidth:2,hoverable:!0,clickable:!0},legend:{show:!1}
    });
    $.plot('#disc-nilai', datanilai, {
        series: {
            pie: {
                innerRadius: 0.5,
                show: true,
                radius:1,
                label: {
                    show: true, 
                    radius:0.7,
                    formatter: function (label, series) {
                        return '<div style="font-size:10pt;text-align:center;padding:10px;color:black;"><strong>' + label + " : " + series.data[0][1] +'</strong></div>';
                    },
                    threshold: 0.1              
                },     
                combine:{color:"#999",threshold:.1}
            }
        },
        legend: {
            show: true
        },
        grid:{borderWidth:2,hoverable:!0,clickable:!0},legend:{show:!1}
    });
</script>

<?php
}else{
    ?>
    <div class="row">
        <div class="col-md-12"> 
            <div class="panel panel-inverse"> 
                <div class="panel-heading"> 
                    <h4 class="panel-title"><center>Informasi</center></h4>
                </div> 
                <div class="panel-body">
                    <div class="alert alert-danger m-b-15">
                        Maaf siswa bersangkutan belum melakukan tes kepribadian.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
