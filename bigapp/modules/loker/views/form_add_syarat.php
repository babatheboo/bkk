<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/parsley/dist/parsley.js"></script>
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    <div class="panel-body panel-form">
		        <form class="form-horizontal form-bordered" action="<?php echo base_url();?>loker/proses_add_syarat" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Dibutuhkan * :</label>
						<div class="col-md-1 col-sm-1">
							<input class="form-control" type="text" id="butuh" minlength="1" maxlength='50' name="butuh" value="<?php echo set_value('butuh',isset($default['butuh']) ? $default['butuh'] : ''); ?>" data-type="butuh" data-parsley-required="true" data-parsley-minlength="1" data-parsley-maxlength="50"/>
                            <span style="color:red;"><?php echo form_error('butuh');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Tinggi Badan * :</label>
						<div class="col-md-1 col-sm-1">
							<input class="form-control" type="text" id="tinggi_badan" minlength="1" maxlength="20" name="tinggi_badan" value="<?php echo set_value('tinggi_badan',isset($default['tinggi_badan']) ? $default['tinggi_badan'] : ''); ?>" data-type="tinggi_badan" data-parsley-required="true" data-parsley-minlength="1" data-parsley-maxlength="20"/>
                            <span style="color:red;"><?php echo form_error('tinggi_badan');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Berat Badan * :</label>
						<div class="col-md-1 col-sm-1">
							<input class="form-control" type="text" id="berat_badan" minlength="1" maxlength="20" name="berat_badan" value="<?php echo set_value('berat_badan',isset($default['berat_badan']) ? $default['berat_badan'] : ''); ?>" data-type="berat_badan" data-parsley-required="true" data-parsley-minlength="1" data-parsley-maxlength="20"/>
                            <span style="color:red;"><?php echo form_error('berat_badan');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Umur * :</label>
						<div class="col-md-1 col-sm-1">
							<input class="form-control" type="text" id="umur" minlength="1" maxlength="20" name="umur" value="<?php echo set_value('umur',isset($default['umur']) ? $default['umur'] : ''); ?>" data-type="umur" data-parsley-required="true" data-parsley-minlength="1" data-parsley-maxlength="20"/>
                            <span style="color:red;"><?php echo form_error('umur');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Jenis Kelamin * :</label>
						<div class="col-md-15 col-sm-5">
							<input type="radio" data-parsley-required="true" name="jk" value="1" <?php if($jk=="1"){echo 'checked="checked"';}?>/> Laki - Laki &nbsp; &nbsp;
                            <input type="radio" data-parsley-required="true" name="jk" value="2"  <?php if($jk=="2"){echo 'checked="checked"';}?> /> Perempuan &nbsp; &nbsp;
                            <input type="radio" data-parsley-required="true" name="jk" value="0"  <?php if($jk=="0"){echo 'checked="checked"';}?> /> Semua Jenis Kelamin &nbsp; &nbsp;
                            <span style="color:red;"><?php echo form_error('jk');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3"></label>
						<div class="col-md-3 col-sm-3">
							<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
						</div>
					</div>
		        </form>
		    </div>
		</div>
	</div>
</div>
