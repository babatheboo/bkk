<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        $('#tgl_buka').datepicker();
        $('#tgl_tutup').datepicker();
        FormPlugins.init();
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-validation-2">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body panel-form">
                <form name="form" id="form" action="<?php echo base_url();?>loker/proses_edit_loker" class="form-horizontal form-bordered" method="post">
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Nama Industri * :</label>
                        <div class="col-md-5 col-sm-5">
                            <?php echo form_dropdown('industri',$option_industri,isset($default['industri']) ? $default['industri'] : '','id="industri" name="industri" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
                            <span style="color:red;"><?php echo form_error('industri');?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Judul Lowongan * :</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="judul" value="<?php echo set_value('judul',isset($default['judul']) ? $default['judul'] : ''); ?>" minlength="1" name="judul" value="" data-type="judul" data-parsley-required="true" data-parsley-minlength="1" />
                            <span style="color:red;"><?php echo form_error('judul');?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Deskripsi Lowongan * :</label>
                        <div class="col-md-5 col-sm-5">
                            <textarea class="form-control" minlength="1" data-parsley-required="true" id="deskripsi" name="deskripsi" rows="3"><?php echo $deskripsi;?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Gaji * :</label>
                        <div class="col-md-3 col-sm-3">
                            <?php echo form_dropdown('gaji',$option_gaji,isset($default['gaji']) ? $default['gaji'] : '','id="gaji" data-size="10" data-parsley-required="true" data-live-search="true" data-style="btn-white" class="form-control selectpicker"');?>
                            <span style="color:red;"><?php echo form_error('gaji');?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Tanggal Dibuka * :</label>
                        <div class="col-md-2 col-sm-2"> 
                            <div class="input-group date" id="tgl_buka" data-date-format="dd-mm-yyyy">
                                <input type="text" class="form-control" name="tgl_buka" value="<?php echo set_value('tgl_buka',isset($default['tgl_buka']) ? $default['tgl_buka'] : ''); ?>" data-type="tgl_buka" data-parsley-required="true"/>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <span style="color:red;"><?php echo form_error('tgl_buka');?></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right">Tanggal Ditutup * :</label>
                        <div class="col-md-2 col-sm-2"> 
                            <div class="input-group date" id="tgl_tutup" data-date-format="dd-mm-yyyy">
                                <input type="text" class="form-control" name="tgl_tutup" value="<?php echo set_value('tgl_tutup',isset($default['tgl_tutup']) ? $default['tgl_tutup'] : ''); ?>" data-type="tgl_tutup" data-parsley-required="true"/>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <span style="color:red;"><?php echo form_error('tgl_tutup');?></span>
                            </div>
                        </div>
                    </div>
                    <div id="tombol_">
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 text-right"></label>
                            <div class="col-md-3 col-sm-3">
                                <button type="submit" class="btn btn-success btn-sm">SIMPAN</button>
                                <button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">BATAL</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>