<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/parsley/dist/parsley.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/filter_skl.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
$(document).ready(function() {
FormPlugins.init();
});
</script>
<div class="row">
<div class="col-md-12">
<div class="panel panel-inverse" data-sortable-id="form-validation-2">
<div class="panel-heading">
<h4 class="panel-title"><?php echo $halaman;?></h4>
</div>
<div class="panel-body panel-form">
<form name="form" id="form" action="<?php echo base_url();?>loker/proses_add" class="form-horizontal form-bordered" method="post" data-parsley-validate="true">
<?php if($this->session->userdata('level')!=2){?>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Nama Industri * :</label>
<div class="col-md-5 col-sm-5">
<?php echo form_dropdown('industri',$option_industri,isset($default['industri']) ? $default['industri'] : '','id="industri" data-parsley-required="true" name="industri" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
<span style="color:red;"><?php echo form_error('industri');?></span>
</div>
</div>
<?php }else{
$uid = $this->session->userdata('user_id');
$dt = $this->db1->get_where('app.user_industri',array('user_id'=>$uid))->result();
foreach ($dt as $key) {
$id_industri = $key->industri_id;
}
?>
<input type="hidden" name="industri" value="<?php echo $id_industri;?>" />
<?php
}
?>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Judul Lowongan * :</label>
<div class="col-md-4 col-sm-4">
<input class="form-control" type="text" id="judul" minlength="1" name="judul" value="" data-type="judul" data-parsley-required="true" data-parsley-minlength="1" />
<span style="color:red;"><?php echo form_error('judul');?></span>
</div>
</div>
<div class="form-group row">
                        <label class="text-right control-label col-md-3 col-sm-3">Posisi Lowongan * :</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="posisi" minlength="1" name="posisi" value=""
                                data-type="posisi" data-parsley-required="true" data-parsley-minlength="1" />
                            <span style="color:red;"><?php echo form_error('posisi');?></span>
                        </div>
                    </div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Deskripsi Lowongan * :</label>
<div class="col-md-5 col-sm-5">
<textarea class="form-control" minlength="1" data-parsley-required="true" id="deskripsi" name="deskripsi" rows="3"></textarea>
<span style="color:red;"><?php echo form_error('deskripsi');?></span>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Gaji * :</label>
<div class="col-md-3 col-sm-3">
<?php echo form_dropdown('gaji',$option_gaji,isset($default['gaji']) ? $default['gaji'] : '','id="gaji" data-size="10" data-parsley-required="true" data-live-search="true" data-style="btn-white" class="form-control"');?>
<span style="color:red;"><?php echo form_error('gaji');?></span>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Tanggal Dibuka * :</label>
<div class="col-md-2 col-sm-2"> 
<div class="input-group date" id="tgl_buka" data-date-format="dd-mm-yyyy">
<input type="text" class="form-control" name="tgl_buka" value="<?php echo set_value('tgl_buka',isset($default['tgl_buka']) ? $default['tgl_buka'] : ''); ?>" data-type="tgl_buka" data-parsley-required="true"/>
<span class="input-group-addon m-l-10"><i class="fas fa-calendar-alt"></i></span>
<span style="color:red;"><?php echo form_error('tgl_buka');?></span>
</div>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Tanggal Ditutup * :</label>
<div class="col-md-2 col-sm-2"> 
<div class="input-group date" id="tgl_tutup" data-date-format="dd-mm-yyyy">
<input type="text" class="form-control" name="tgl_tutup" value="<?php echo set_value('tgl_tutup',isset($default['tgl_tutup']) ? $default['tgl_tutup'] : ''); ?>" data-type="tgl_tutup" data-parsley-required="true"/>
<span class="input-group-addon m-l-10"><i class="fas fa-calendar-alt"></i></span>
<span style="color:red;"><?php echo form_error('tgl_tutup');?></span>
</div>
</div>
</div>
<?php
$ckdata =$this->db->get_where('view_aliansi',array('bkk_induk'=>$this->session->userdata('role_')));
if(count($ckdata->result())>0){
?>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Lowongan Tujuan * :</label>
<div class="col-md-3 col-sm-3">
<select id="tujuan" name="tujuan" data-live-search="true" data-parsley-required="true" data-style="btn-white" class="form-control">
<option value="" selected="selected">Pilih Tujuan</option>
<option value="9">Umum</option>
<option value="99">BKK Sendiri</option>
<option value="0">Semua Aliansi</option>
<option value="1">Filter Aliansi</option>
</select>
</div>
</div>
<div id="filter_sekolah">
<div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"> <button class="btn btn-danger btn-xs m-r-5" onclick="filter_data()"><i class="fa fa-search"></i> Filter Pencarian</button>&nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fas fa-sync-alt"></i> Reload Data</button> </div> <h4 class="panel-title">List Aliansi BKK</h4>
<div id="filter_pencarian">
<br/>
<?php echo form_dropdown('provinsi_filter',$option_provinsi_filter,isset($default['provinsi_filter']) ? $default['provinsi_filter'] : '','class="form-control" style="width:25%" id="provinsi_filter" data-live-search="true" data-style="btn-white"');?>
<?php echo form_dropdown('kota_filter',$option_kota_filter,isset($default['kota_filter']) ? $default['kota_filter'] : '','class="form-control" style="width:25%" id="kota_filter" data-live-search="true" data-style="btn-white"');?>
</div> </div> 
<div class="panel-body"> 
<div class="table-responsive">
<table id="data-sekolah" class="table table-striped table-bordered nowrap" width="100%">
<thead>
<tr>
<th width="1%" style="text-align:center">
<input type="checkbox" name="select_all" value="1" style="text-align:center" id="select-all">
</th> 
<th style="text-align:center" width="15%">NPSN</th>
<th style="text-align:center" width="50%">Nama Sekolah</th>
<th style="text-align:center" width="40%">Website</th>
</tr>
</thead>
<tbody> 
</tbody>
</table>
</div> 
</div> 
</div> </div>
</div>
<?php
}else{
?>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Lowongan Tujuan * :</label>
<div class="col-md-3 col-sm-3">
<select id="tujuan" name="tujuan" data-live-search="true" data-parsley-required="true" data-style="btn-white" class="default-select2 form-control">
<option value="" selected="selected">Pilih Tujuan</option>
<option value="9">Umum</option>
<option value="99">BKK Sendiri</option>
</select>
<span style="color:red;"><?php echo form_error('tujuan');?></span>
</div>
</div>
<?php
}
?>
<div class="form-group row">
<label class="text-right control-label col-md-3">Provinsi * :</label>
<div class="col-md-6">
<select name="provinsi[]" id="provinsi" name="provinsi" data-live-search="true" data-parsley-required="true" data-style="btn-white" class="multiple-provinsi form-control" multiple="multiple">
<?php 
$provinsi = $this->db->get('ref.view_provinsi')->result();
foreach ($provinsi as $row) {
echo '<option value="'.trim($row->kode_prov).'">'.$row->prov.'</option>';
}
?>
</select>
</div>
</div>
<div class="col-md-12">
<div class="panel panel-inverse">
<div class="panel-heading">
<div class="panel-heading-btn">
<a href="JavaScript:void(0)" id="add_btn" title="Tambah Kriteria Calon Pencari Kerja" class="btn btn-primary btn-xs m-r-5">Tambah Kriteria Calon Pencari Kerja</a>
</div>
<h4 class="panel-title">Kriteria Calon Pencari Kerja</h4>
</div>
<div class="panel-body">
<div class="table-responsive">
<table id="data-news" class="table table-striped table-bordered nowrap" width="100%">
<thead>
<tr>
<th style="text-align:center" width="1%">No.</th>
<th style="text-align:center" width="20%">Jenis Kelamin</th>
<th style="text-align:center" width="10%">Jumlah Kebutuhan</th>
<th style="text-align:center" width="10%">Tinggi Badan</th>
<th style="text-align:center" width="10%">Berat Badan</th>
<th style="text-align:center" width="10%">Umur</th>
<th style="text-align:center" width="5%">Aksi</th>
</tr>
</thead>
<tbody id="container">
</tbody>
</table>
</div>
</div>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Kategori Lowongan * :</label>
<div class="col-md-6 col-sm-6">
<select name="kat_loker[]" class="multiple-kategori form-control" multiple="multiple" data-parsley-required="true" id="kat_loker" data-size="10" data-parsley-required="true" data-live-search="true" data-style="btn-white">
<?php
$cksub = $this->db1->query("SELECT * FROM ref.bidang_usaha")->result();
if(count($cksub)>0){
foreach ($cksub as $xx) {
echo '<option value="'.trim($xx->bidang_usaha_id).'">'.$xx->nama_bidang_usaha.'</option>';
}
}else{
echo '<option value="">Bidang Usaha Belum Tersedia</option>';
}
?>
</select>
<span style="color:red;"><?php echo form_error('kat_loker');?></span>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3">Spesifikasi Jurusan * :</label>
<div class="col-md-6 col-sm-6">
<select name="jurusan[]" class="multiple-jurusan form-control" multiple="multiple" id="jurusan" data-parsley-required="true" data-size="10" data-parsley-required="true" data-live-search="true" data-style="btn-white">
<option value="99">Semua Jurusan</option>
<?php
$ckjrs = $this->db1->query("SELECT * FROM ref.jurusan WHERE level_bidang_id = '12' ORDER BY nama_jurusan ASC")->result();
if(count($ckjrs)>0){
foreach ($ckjrs as $xx) {
echo '<option value="'.trim($xx->jurusan_id).'">'.$xx->nama_jurusan.'</option>';
}
}else{
echo '<option value="">Jurusan Untuk SMK Belum Tersedia</option>';
}
?>
</select>
<span style="color:red;"><?php echo form_error('jurusan');?></span>
</div>
</div>
<div class="form-group row">
<label class="text-right control-label col-md-3 col-sm-3"></label>
<div class="col-md-3 col-sm-3">
<button type="submit" class="btn btn-success btn-sm m-r-10">SIMPAN</button>
<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">BATAL</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
