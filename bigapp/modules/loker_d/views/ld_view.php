<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extension/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/ld.js"></script>
<div class="row"> <div class="col-md-12"> 
    <div class="panel panel-inverse"> <div class="panel-heading"> 
        <div class="panel-heading-btn">
            <button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
                <i class="fa fa-plus-circle"></i> Tambah Data
            </button>&nbsp;
            <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                <i class="fas fa-sync-alt"></i> Reload Data
            </button>
        </div>
        <h4 class="panel-title"><?php echo $halaman;?></h4>
    </div>
    <div class="panel-body"> 
        <div class="table-responsive"> 
            <table id="data-iklan" class="table table-striped table-bordered nowrap" width="100%"> 
                <thead> 
                    <tr> 
                        <th style="text-align:center" width="1%">No.</th> 
                        <th style="text-align:center" width="15%">Gambar</th>
                        <th style="text-align:center" width="15%">Judul</th>
                        <th style="text-align:center" width="20%">Perusahaan</th>
                        <th style="text-align:center" width="10%">Deskripsi</th>
                        <th style="text-align:center" width="10%">Tgl Buka</th>
                        <th style="text-align:center" width="10%">Tgl Tutup</th>
                        <th style="text-align:center" width="10%">Status</th>
                        <th style="text-align:center" width="10%">Aksi</th>
                    </tr> 
                </thead> 
                <tbody> 
                </tbody> 
            </table> 
        </div> 
    </div> 
</div> 
</div>
</div>
<div id="modal_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-group" id="photo-preview">
                        <label class="control-label col-md-3">Foto</label>
                        <div class="col-md-9">
                            (No photo)
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Judul</label>
                            <div class="col-md-6">
                                <input name="judul" id="judul" placeholder="Masukan Judul" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Perusahaan</label>
                            <div class="col-md-6">
                                <input name="perusahaan" id="perusahaan" placeholder="Masukan Nama Perusahaan" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Deskripsi</label>
                            <div class="col-md-6">
                                <input name="deskripsi" id="deskripsi" placeholder="Masukan Deskripsi" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Tutup</label>
                            <div class="col-md-6">
                                <input name="tgl_tutup" id="tgl_tutup" class="form-control" type="date">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label id="label-photo" class="control-label col-md-3">Foto Iklan</label>
                            <div class="col-md-6">
                                <input name="foto" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form> 

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_iklan('<?php echo $link;?>','iklan','form')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
