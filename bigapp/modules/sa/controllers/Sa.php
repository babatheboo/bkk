<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sa extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
 	}

 	public function detil_pendaftar($id)
 	{
 		$dt = $this->db1->get_where('ref.sekolah',array('sekolah_id'=>$id))->result();

 		echo "<table width='100%'>";
 		foreach ($dt as $key) {
 			echo "<tr>" ;
 			echo "<td>Nama Sekolah</td><td>:</td>";
 			echo "<td>" . $key->nama . "</td></tr>";
 			echo "<tr>" ;
 			echo "<td>NPSN</td><td>:</td>";
 			echo "<td>" . $key->npsn . "</td></tr>";
 			echo "<tr>" ;
 			echo "<td>Alamat</td><td>:</td>";
 			echo "<td>" . $key->alamat_jalan . "</td></tr>";
 		}

 		$ds = $this->db1->get_where('app.view_registrasi',array('sekolah_id'=>$id))->result();
 		foreach ($ds as $key) {
 			$idn = $id;
 			echo "<tr>" ;
 			echo "<td>Nama Pendaftar</td><td>:</td>";
 			echo "<td>" . $key->nama_pendaftar . "</td></tr>";
 			echo "<tr>" ;
 			echo "<td>No Telepon</td><td>:</td>";
 			echo "<td>" . $key->tlp . "</td></tr>";
 			echo "<tr>" ;
 			echo "<td>Email</td><td>:</td>";
 			echo "<td>" . $key->email . "</td></tr>";
 		}
 		echo "</table><br/><br/>";
 		echo "<input type='hidden' name='idna' id='idna' value='$idn'>";
 		//echo '<button type="button" class="btn btn-success" onclik="approve(\'' . $idn . '\')"><i class="fa fa-check"></i> Approve</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
 		//echo '<button type="button" class="btn btn-danger"><i class="fa fa-check"></i> Decline</button>';
 	}

 	public function approve($id)
 	{
 		$x = $this->input->post('term');
 		if($x=='approve')
 		{	
 			$this->db1->where('registrasi_id',$id);
 			$data=array('status'=>'1','approved_by'=>$this->session->userdata('user_id'));
 			$this->db1->update('app.registrasi_sekolah',$data);
 			$dt = $this->db1->get_where('app.registrasi_sekolah',array('registrasi_id'=>$id))->result();
 			foreach ($dt as $key ) {
 				$sek_id = $key->sekolah_id;
 			}
 			$this->session->set_userdata('sakolana',$sek_id);
 			$this->asn->proses_register();
 		}
 	}

}


