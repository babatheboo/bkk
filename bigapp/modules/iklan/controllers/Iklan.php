<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Iklan extends CI_Controller {
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('iklan_model');
    }
    public function index(){
    $this->_content();
  }
  public function _content(){
    $isi['kelas'] = "admin";
    $isi['namamenu'] = "Data Iklan";
    $isi['page'] = "iklan";
    $isi['cek'] = "edit";
    $isi['link'] = 'iklan';
    $isi['actionhapus'] = 'hapus';
    $isi['actionedit'] = 'edit';
    $isi['halaman'] = "Data Iklan";
    $isi['judul'] = "Halaman Data Iklan";
    $isi['content'] = "iklan_view";
    $this->load->view("dashboard/dashboard_view",$isi);
  }
  public function getData(){
    if($this->input->is_ajax_request()){
      $list = $this->iklan_model->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $rowx) {
        $no++;
        $row = array();
        $row[] = $no . ".";
        $row[] = "<center><img src='".base_url()."./assets/foto/iklan/$rowx->gambar' width='100px'></img></center>";
        $row[] = $rowx->judul;
        $row[] = $rowx->perusahaan;
        $row[] = $rowx->deskripsi;
        $row[] = $rowx->tgl_post;
        $row[] = $rowx->tgl_tutup;
        $row[] = $rowx->status ? '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\',\'Data Iklan\',\'iklan\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>' : '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\',\'Data Iklan\',\'iklan\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-danger" title="Status NonAktif"><i class="fa fa-lock icon-white"></i></a>';
        $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data Iklan\',\'iklan\',\'edit_data\','."'".$rowx->id."'".')"><i class="fas fa-pencil-alt"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data Iklan\',\'iklan\',\'hapus_data\','."'".$rowx->id."'".')"><i class="fas fa-trash"></i></a></center>';
        $data[] = $row;
      }
      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->iklan_model->count_all(),
        "recordsFiltered" => $this->iklan_model->count_filtered(),
        "data" => $data,
        );
      echo json_encode($output);
    }else{
      redirect("_404","refresh");
    }
  }
  private function _validate($cek){
      $data = array();
      $data['error_string'] = array();
      $data['inputerror'] = array();
      $data['status'] = TRUE;
      if($this->input->post('deskripsi') == '' && $this->input->post('judul') == '' && $this->input->post('perusahaan') == ''
          && $this->input->post('tgl_tutup') == ''){
        $data['inputerror'][] = 'deskripsi';
        $data['inputerror'][] = 'judul';
        $data['inputerror'][] = 'perusahaan';
        $data['inputerror'][] = 'tgl_tutup';
      $data['error_string'][] = 'Pastikan data sudah benar!';
      $data['status'] = FALSE;
      }
      if($data['status'] === FALSE){
        echo json_encode($data);
        exit();
      }
  }
  public function save_iklan(){
    if ($this->input->is_ajax_request()) {
          $method         = "save";
          $this->_validate($method);
          $tgl_post = date("Y-m-d");
          $data           = array('deskripsi'=>$this->input->post('deskripsi'), 'judul'=>$this->input->post('judul'),
                                  'perusahaan'=>$this->input->post('perusahaan'), 'tgl_tutup'=>$this->input->post('tgl_tutup'), 'status'=>'1', 'tgl_post'=>$tgl_post);
          if(!empty($_FILES['foto']['name'])){
            $upload         = $this->_do_upload();
            $data['gambar'] = $upload;
          }else{
            $data['gambar'] = 'tutwuri.png';
          }
          $this->iklan_model->simpan($data);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  public function update_iklan(){
    if ($this->input->is_ajax_request()) {
        $method = "update";
        $this->_validate($method);
        $tgl_post = date("Y-m-d");
        $data   = array('deskripsi'=>$this->input->post('deskripsi'), 'judul'=>$this->input->post('judul'),
                        'perusahaan'=>$this->input->post('perusahaan'), 'tgl_tutup'=>$this->input->post('tgl_tutup'), 'tgl_post'=>$tgl_post);
        if(!empty($_FILES['foto']['name'])){
          $this->hapus_foto($this->input->post('id'));
          $upload         = $this->_do_upload();
          $data['gambar'] = $upload;
        }
        $this->iklan_model->update(array('id' => trim($this->input->post('id'))), $data);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  private function _do_upload(){
      $config['upload_path']    = './assets/foto/iklan';
      $config['allowed_types']  = 'gif|jpeg|jpg|png|bmp';
      $config['max_size']		= '1048';
      $config['max_width'] 		= '0';
      $config['max_height'] 	= '0';
      $config['overwrite'] 		= TRUE;
      $config['file_name']      = round(microtime(true) * 1000);
      $this->upload->initialize($config);
      $this->load->library('upload', $config);
      if(!$this->upload->do_upload('foto')) {
        $data['inputerror'][] = 'foto';
        $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('','');
        $data['status'] = FALSE;
        echo json_encode($data);
        exit();
      }
      return $this->upload->data('file_name');
  }
  public function hapus_data($id){
    if($this->input->is_ajax_request()){
        $this->hapus_foto($id);
        $this->iklan_model->hapus_by_id($id);
        echo json_encode(array("status" => TRUE));
    }else{
      redirect("_404","refresh");
    }
  }
  public function hapus_foto($id){
      $ckfoto    = $this->db->get_where('lowongan_psmk',array('id'=>$id));
      $row       = $ckfoto->row();
      $fotona    = $row->gambar;
      if($fotona !="no.jpg"){
      @chmod(unlink('./assets/foto/iklan' . $fotona),777);
      }
  }
  public function edit_data($id){
    if($this->input->is_ajax_request()){
        $data = $this->iklan_model->get_by_id($id);
        echo json_encode($data);
    }else{
        redirect("_404","refresh");
    }
  }
  public function ubah_status($jns=Null,$id=Null){
    if($this->input->is_ajax_request()){
      if($jns=="aktif"){
          $data = array('status'=>'0');
      }else{
          $data = array('status'=>'1');
      }
      $this->db->where('id', $id);
      $this->db->update('lowongan_psmk',$data);
    }else{
      redirect("_404",'refresh');
    }
  }
}
