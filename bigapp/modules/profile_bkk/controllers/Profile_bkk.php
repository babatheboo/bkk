<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile_bkk extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login();
		$this->asn->role_akses('1');
	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$sess = $this->session->userdata('role_');
		$ckdata = $this->db->query("SELECT * FROM sekolah_terdaftar a WHERE a.sekolah_id ='$sess'");
		if(count($ckdata->result()) > 0){
			$row                            = $ckdata->row();
			$sess                           = $this->session->userdata('role_');
			$ckizin                         = $this->db->query("SELECT no_izin_bkk,no_sk_pendirian FROM sekolah_terdaftar WHERE sekolah_id = '$sess'");
			$x                              = $ckizin->row();
			$isi['default']['no_izin']      = $x->no_izin_bkk;
			$isi['default']['sk_pendirian'] = $x->no_sk_pendirian;
			$isi['foto']                    = $row->logo;
			$isi['scan']                    = $row->scan;
			$isi['page']                    = "setup";
			$isi['cek']                     = "edit";							
			$isi['tombolsimpan']            = 'Simpan';
			$isi['tombolbatal']             = 'Batal';
			$isi['halaman']                 = "Ganti Password Akun";
			$isi['judul']                   = "Halaman Edit Profile BKK";
			$isi['content']                 = "form_sekolah";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect("_404","refresh");
		}
	}
	public function proses_edit(){
		$acak=rand(00,99);
		$bersih = $_FILES['foto']['name'];
		$nm = str_replace(" ","_","$bersih");
		$pisah = explode(".",$nm);
		$nama_murni = $pisah[0];
		$ubah = $acak.$nama_murni;
		$nama_fl = $acak.$nm;
		$tmpName = str_replace(" ", "_", $_FILES['foto']['name']);
		$nmfile = "file_".time();
		if($tmpName!=''){
			$config['file_name'] = $nmfile;
			$config['upload_path'] = './assets/foto/bkk';
			$config['allowed_types'] = 'gif|jpg|png|bmp';
			$config['max_size'] = '1024';
			$config['max_width'] = '0';
			$config['max_height'] = '0';
			$config['overwrite'] = TRUE;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('foto')){
				$gbr = $this->upload->data();	
				$this->hapusfoto($this->session->userdata('role_'));
				$simpandata = array('logo'=>$gbr['file_name']);
				$this->db->where('sekolah_id',$this->session->userdata('role_'));
				$this->db->update('sekolah_terdaftar',$simpandata);
				redirect('profile_bkk','refresh');
			}else{
				?>
				<script type="text/javascript">
					alert("Pastikan Type File jpg dan ukuran file maksimal 1mb");
					window.location.href="<?php echo base_url();?>profile_bkk";
				</script>
				<?php
			}
		}
	}

	public function change_pass(){
		$this->form_validation->set_rules('password', 'Password', 'htmlspecialchars|trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('password_konfirmasi', 'Password Konfirmasi', 'htmlspecialchars|trim|required|min_length[5]|max_length[20]|matches[password]');
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		$this->form_validation->set_message('min_length', '%s minimal %s karakter');
		$this->form_validation->set_message('matches', '%s tidak sesuai');
		$this->form_validation->set_message('max_length', '%s maximal %s karakter');
		if ($this->form_validation->run() == TRUE){
			$password = $this->input->post('password_konfirmasi');
			$hashpasssword = md5(md5($password));
			$simpan = array('password'=>$hashpasssword);
			$this->db->where("user_id",$this->session->userdata('user_id'));
			if($this->db->update('app.username',$simpan)){
				?>
				<script type="text/javascript">
					alert("Pergantian Password Berhasil, Silahkan Login Kembali!");
					window.location.href = '<?php echo base_url();?>profile_bkk';
				</script>
				<?php
			}
		}else{
			$this->index();
		}
	}
	function hapusfoto($x){
		$ckdata = $this->db->query("SELECT * FROM sekolah_logo WHERE sekolah_id = '$x'");
		if(count($ckdata->result())>0){
			$hehe = $ckdata->row();
			$fotona = $hehe->logo;
			if($fotona!='no.jpg'){
				unlink('./assets/foto/bkk/' . $fotona);
			}
		}
	}
	function hapusfoto_scan($x){
		$ckdata = $this->db->query("SELECT * FROM sekolah_logo WHERE sekolah_id = '$x'");
		if(count($ckdata->result())>0){
			$hehe = $ckdata->row();
			$fotona = $hehe->scan;
			if($fotona!='no.jpg'){
				unlink('./assets/foto/scan/' . $fotona);
			}
		}
	}

	public function addketua(){
		$nama = $this->input->post('nama');
		$tlp = $this->input->post('tlp');
		$email = $this->input->post('email');
		$sess = $this->session->userdata('role_');
		$dtk = array('sekolah_id'=>$sess, 'nama'=>$nama, 'tlp'=>$tlp, 'email'=>$email);
		$hmm = $this->db->query("SELECT * FROM ketua_bkk WHERE sekolah_id='$sess'")->result();
		if (count($hmm)>0) {
			if ($this->db->query("UPDATE ketua_bkk SET nama='$nama', tlp='$tlp', email='$email' WHERE sekolah_id='$sess'")) {
				$data['response'] = 'updated';
			}else{
				$data['response'] = 'gglUpdate';
			}
		}else{
			if ($this->db->insert('ketua_bkk',$dtk)) {
				$data['response'] = 'inserted';
			}else{
				$data['response'] = 'gglInsert';
			}
		}
		echo json_encode($data);
	}
}
