<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
			<div class="panel-heading">
				<h4 class="panel-title">Edit Logo BKK</h4>
			</div>
			<div class="panel-body panel-form">
				<?php
				if($cek=='edit'){
					?>
					<div class="form-group">
						<div class="col-md-2 col-sm-2" style="margin-left: 145px; margin-top: 10px;">
							<?php
							if($foto==""){
								$fotox = "no.jpg";
							}else{
								$fotox = $foto;
							}
							?>
							<img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
						</div>
					</div>
					<?php	
				}
				?>

				<form  action="<?php echo base_url();?>profile_bkk/proses_edit" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					<div class="note note-yellow m-b-15">
						<div class="note-icon f-s-20">
							<i class="fa fa-lightbulb fa-2x"></i>
						</div>
						<div class="note-content">
							<h4 class="m-t-5 m-b-5 p-b-2">Catatan</h4>
							<ul class="m-b-5 p-l-25">
								<li>Besar file maximum adalah 1 MB</li>
								<li>File yang diizinkan adalah yang bertype JPG, GIF, PNG</li>
								<li>Gambar akan ditimpa dengan gambar terbaru </li>
							</ul>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4">Logo Sekolah :</label>
						<div class="col-md-4 col-sm-4">
							<input name="MAX_FILE_SIZE" value="9999999999" type="hidden">
							<span class="btn btn-primary fileinput-button m-r-3">
								<i class="fa fa-plus"></i> <span>Pilih logo ...</span>
								<input type="file" id="foto" name="foto"/>  
							</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4"></label>
						<div class="col-md-4 col-sm-4">
							<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<h4 class="panel-title">Ketua BKK</h4>
			</div>
			<div class="panel-body panel-form">
				<form class="form-horizontal form-bordered" action="javascript:doneKetua()" method="post">
					<div class="form-group row">
						<div class="col-md-5 col-sm-5">
							<label class="control-label">Nama</label>
							<input type="text" name="namaKetua" id="namaKetua" placeholder="Nama Lengkap Ketua BKK" class="form-control"/>
							<label class="control-label">No. Tlp</label>
							<input type="text" name="tlpKetua" id="tlpKetua" placeholder="No Tlp Ketua BKK" class="form-control"/>
							<label class="control-label">Email</label>
							<input type="text" name="emailKetua" id="emailKetua" placeholder="Email Ketua BKK" class="form-control"/>
							<button onclick="doneKetua()" type="button" class="btn btn-success btn-sm m-t-15">Update</button>
						</div>
						<div class="col-md-7 col-sm-7 m-t-15">
							<?php
							$sess = $this->session->userdata('role_');
							$ckk = $this->db->query("SELECT * FROM ketua_bkk WHERE sekolah_id='$sess'")->result();
							if (count($ckk)>0) {
								foreach ($ckk as $wkw) {
									echo "
									<label>Nama Ketua BKK:</label> <strong>$wkw->nama</strong><br>
									<label>No Tlp Ketua BKK:</label> <strong>$wkw->tlp</strong><br>
									<label>Email Ketua BKK:</label> <strong>$wkw->email</strong>";
								}
							}else{
								echo "
								<label>Nama Ketua BKK:</label> <strong>Belum Diisi</strong><br>
								<label>No Tlp Ketua BKK:</label> <strong>Belum Diisi</strong><br>
								<label>Email Ketua BKK:</label> <strong>Belum Diisi</strong>";
							}
							?>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
			<div class="panel-heading">
				<h4 class="panel-title"><?php echo $halaman;?></h4>
			</div>
			<div class="panel-body panel-form">
				<form class="form-horizontal form-bordered" action="<?php echo base_url();?>profile_bkk/change_pass" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					<div class="form-group row">
						<div class="col-md-5 col-sm-5">
							<label class="control-label">Password * :</label>
							<input type="password" name="password" value="<?php echo set_value('password',isset($default['password']) ? $default['password'] : ''); ?>" id="password" placeholder="Masukan Password Baru" class="form-control"/>
							<span style="color:red;"><?php echo form_error('password');?></span>
						</div>
						<!-- </div> -->
						<!-- <div class="form-group"> -->
							<div class="col-md-5 col-sm-5">
								<label class="control-label">Konfirmasi Password * :</label>
								<input type="password" name="password_konfirmasi" value="<?php echo set_value('password_konfirmasi',isset($default['password_konfirmasi']) ? $default['password_konfirmasi'] : ''); ?>" id="password_konfirmasi" placeholder="Masukan Password Baru" class="form-control"/>
								<span style="color:red;"><?php echo form_error('password_konfirmasi');?></span>
							</div>
							<!-- </div> -->
							<!-- <div class="form-group"> -->
								<div class="col-md-4 col-sm-4">
									<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			function doneKetua(){
				var nama = $("#namaKetua").val();
				var tlp = $("#tlpKetua").val();
				var email = $("#emailKetua").val();
				if (nama == '') {
					jQuery("#namaKetua").effect('shake', '1500').attr('placeholder', 'Masukan nama ketua BKK');
				}else if (tlp == '') {
					jQuery("#tlpKetua").effect('shake', '1500').attr('placeholder', 'Masukan no tlp BKK');
				}else if (email == '') {
					jQuery("#emailKetua").effect('shake', '1500').attr('placeholder', 'Masukan email ketua BKK');
				}else{
					jQuery.blockUI({
						css: {
							border: "none",
							padding: "15px",
							backgroundColor: "#000",
							"-webkit-border-radius": "10px",
							"-moz-border-radius": "10px",
							opacity: 2,
							color: "#fff"
						},
						message: "Sebentar ... "
					});
					$.ajax({
						url: $BASE_URL + "profile_bkk/addKetua/",
						method: "POST",
						data: { nama: nama, tlp:tlp, email:email },
						cache: false,
						success: function(data) {
							data = $.parseJSON(data);
							if (data.response=='inserted') {
								jQuery.unblockUI();
								$.notify({icon:'fas fa-thumbs-o-up',message:"Data berhasil disimpan ✔"},{type:"success",offset:{x:3,y:2}});
								$("#namaKetua").val('');
								$("#tlpKetua").val('');
								$("#emailKetua").val('');
								window.location.reload();
							}else if (data.response == 'gglInsert') {
								jQuery.unblockUI();
								$.notify({icon:'fas fa-info',message:"Gagal menyimpan data. Coba lagi ❌"},{type:"warning",offset:{x:3,y:2}});
								$("#namaKetua").val('');
								$("#tlpKetua").val('');
								$("#emailKetua").val('');
							}else if (data.response=='updated') {
								jQuery.unblockUI();
								$.notify({icon:'fas fa-thumbs-o-up',message:"Data berhasil diperbarui ✔"},{type:"success",offset:{x:3,y:2}});
								$("#namaKetua").val('');
								$("#tlpKetua").val('');
								$("#emailKetua").val('');
								window.location.reload();
							}else if (data.response=='gglupdate') {
								jQuery.unblockUI();
								$.notify({icon:'fas fa-info',message:"Gagal memperbarui data. Coba lagi ❌"},{type:"warning",offset:{x:3,y:2}});
								$("#namaKetua").val('');
								$("#tlpKetua").val('');
								$("#emailKetua").val('');
							}else{
								jQuery.unblockUI();
								$.notify(
									{ icon: "fa fa-info", message: "Terjadi kesalahan. Ulangi proses" },
									{ type: "danger", offset: { x: 3, y: 2 } }
									);
							}
						}
					});
				}
			}
		</script>