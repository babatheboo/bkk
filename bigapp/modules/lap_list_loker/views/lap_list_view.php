<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/lap_list_loker.js"></script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <!-- <a href="<?php echo base_url();?>lap_loker/expLkr" class="btn btn-success btn-xs m-r-5">
                        <i class="fas fa-file-excel"></i> Download
                    </a> -->
                    <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                        <i class="fas fa-sync-alt"></i> Reload Data
                    </button> 
                </div> 
                <h4 class="panel-title">Data Laporan Lowongan Pekerjaan</h4> 
            </div> 
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="list-loker" class="table table-striped table-bordered nowrap" width="100%"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center; width: 1%;">No.</th> 
                                <th style="text-align:center">Tgl Post</th>
                                <th style="text-align:center">NPSN</th>
                                <th style="text-align:center">SMK</th>  
                                <th style="text-align:center">Judul</th>
                                <th style="text-align:center">Status</th>
                                <th style="text-align:center">Jenis</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>
</div>