<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lap_list_loker extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('list_loker_model');
	}
	public function index(){
		if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="4"){
			$isi['namamenu'] = "BKK";
			$isi['page']     = "lap";
			$isi['kelas']    = "registrasi";
			$isi['link']     = 'lap_list_loker';
			$isi['halaman']  = "Laporan";
			$isi['judul']    = "Daftar Lowongan Kerja";
			$isi['content']  = "lap_list_view";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect('login','refresh');
		}
	}
	public function getList(){
		if($this->input->is_ajax_request()){
			$list = $this->list_loker_model->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			$now = date('Y-m-d');
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[]  = date('d-m-Y', strtotime($rowx->tgl_buat));
				// $row[]  = $rowx->tgl_tutup;
				$row[]  = $rowx->npsn;
				$row[]  = $rowx->nama;
				if (strlen($rowx->judul)>25) {
				$row[]  = substr($rowx->judul, 0, 25).'...';
				}else{
				$row[]  = $rowx->judul;
				}
				if ($rowx->tgl_tutup >= $now) {
				$row[] = '<p class="text-success">Aktif</p>';
				}else{
				$row[]  = '<p class="text-danger">Inaktif</p>';
				}
				switch ($rowx->jenis) {
					case '1':
				$row[]  = '<p class="text-success">Publik</p>';
						break;
					case '0':
				$row[]  = '<p class="text-warning">Privasi</p>';
						break;
					default:
				$row[]  = '<p class="text-danger">Tidak Terdeteksi</p>';
						break;
				}
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->list_loker_model->count_all(),
				"recordsFiltered" => $this->list_loker_model->count_filtered(),
				"data"            => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
}