<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/lap_keterserapan.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<?php
$bln = date('n');
if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
}else{
$t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
}
?>
<!-- filter -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-sortable-id="ui-widget-15">
            <div class="panel-heading">
                <h4 class="panel-title">Filter Keterserapan</h4>
            </div>
            <div class="panel-body" >
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="row" style="margin-top: 15px;">
                                <form method="post" action="">
                                    <div class="row col-md-12">
                                        <div class="col-md-4">
                                            <label class="control-label">Jenis Keterserapan :</label>
                                            <select name="jenis" id="jenis" class="selectpicker">
                                               <option value="1">Kerja</option>
                                               <option value="2">Wirausaha</option>
                                               <option value="3">Kuliah</option>
                                               <option value="4">Tidak Terdata</option>
                                               <option value="0">Semua</option>
                                           </select>
                                       </div>
                                       <div class="col-md-4">
                                           <label class="control-label">Tahun Keterserapan :</label>
                                           <select name="tahun" id="tahun" class="selectpicker">
                                               <option value="0">3 Tahun terakhir</option>
                                               <option value="<?php echo $t1;?>"><?php echo $t1;?></option>
                                               <option value="<?php echo $t2;?>"><?php echo $t2;?></option>
                                               <option value="<?php echo $t3;?>"><?php echo $t3;?></option>
                                           </select>
                                       </div>
                                       <button class="btn btn-primary" onclick="buatlap()" type="button">Proses</button>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
               </div> 
           </div>
       </div>
   </div>
</div>

<!-- table -->
<div id="tbl" style="display: none;" class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <button class="btn btn-danger btn-xs m-r-5" onclick="info()">
                        <i class="fas fa-bell"></i>
                    </button> 
                </div> 
                <h4 class="panel-title">Hasil Keterserapan</h4> 
            </div> 
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="data-hasil" class="table table-striped table-bordered nowrap"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th> 
                                <th style="text-align:center" width="30%">Nama Sekolah</th>
                                <th style="text-align:center" width="1%">Tahun</th>
                                <th style="text-align:center" width="1%">Total</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>
</div>

<!-- semua jurusan -->
<div id="tblS" style="display: none;" class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                    <button class="btn btn-danger btn-xs m-r-5" onclick="info()">
                        <i class="fa fa-bell"></i>
                    </button> 
                </div> 
                <h4 class="panel-title">Hasil Keterserapan</h4> 
            </div> 
            <div class="panel-body"> 
                <div class="table-responsive"> 
                    <table id="data-semua" class="table table-striped table-bordered nowrap"> 
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th> 
                                <th style="text-align:center" width="5%">NPSN</th>
                                <th style="text-align:center" width="30%">Nama Sekolah</th>
                                <th style="text-align:center" width="1%">Tahun</th>
                                <th style="text-align:center" width="1%">Total<br>Bekerja</th>
                                <th style="text-align:center" width="1%">Total<br>Wirausaha</th>
                                <th style="text-align:center" width="1%">Total<br>Kuliah</th>
                                <th style="text-align:center" width="1%">Tidak<br>Terdata</th>
                                <th style="text-align:center" width="1%">Total</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>
</div>
