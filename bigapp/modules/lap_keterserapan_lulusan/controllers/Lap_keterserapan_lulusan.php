<?php if ( ! defined('BASEPATH')) exit('No direct script access owed');
class Lap_keterserapan_lulusan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index(){
		if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="4"){
			$isi['namamenu'] = "BKK";
			$isi['page']     = "lap";
			$isi['kelas']    = "registrasi";
			$isi['link']     = 'reg_bkk';
			$isi['halaman']  = "Laporan";
			$isi['judul']    = "Keterserapan";
			$isi['content']  = "lap_view";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect('login','refresh');
		}
	}


	public function getHasil($jenis, $tahun){
		$bln = date('n');
		if($bln<7){
			$t1 = date('Y') - 3;
			$t2 = $t1+1;
			$t3 = $t1+2;
		}else{
			$t1 = date('Y') - 3;
			$t2 = $t1+1;
			$t3 = $t1+2;

		}

		/*kerja*/
		if ($jenis == "1") {
			if ($tahun == "0") {
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totker
					from ref.peserta_didik a
					left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3'
					group by c.nama
					order by totker desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$t1.'-'.$t3."</center>",
						"<center>".number_format($key->totker)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}else{
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totker
					from ref.peserta_didik a
					left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)='$tahun'
					group by c.nama
					order by totker desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$tahun."</center>",
						"<center>".number_format($key->totker)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}
			
			/*wira*/
		}elseif ($jenis == "2") {
			if ($tahun == "0") {
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totwir
					from ref.peserta_didik a
					left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3'
					group by c.nama
					order by totwir desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$t1.'-'.$t3."</center>",
						"<center>".number_format($key->totwir)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}else{
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totwir
					from ref.peserta_didik a
					left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)='$tahun'
					group by c.nama
					order by totwir desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$tahun."</center>",
						"<center>".number_format($key->totwir)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}

			/*kuliah*/
		}elseif ($jenis =="3") {
			if ($tahun == "0") {
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totkul
					from ref.peserta_didik a
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3'
					group by c.nama
					order by totkul desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$t1.'-'.$t3."</center>",
						"<center>".number_format($key->totkul)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}else{
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(e.peserta_didik_id) as totkul
					from ref.peserta_didik a
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					where left(a.tanggal_keluar,4)='$tahun'
					group by c.nama
					order by totkul desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$tahun."</center>",
						"<center>".number_format($key->totkul)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}

			/*tidak terdata*/
		}elseif ($jenis =="4") {
			if ($tahun == "0") {
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(b.peserta_didik_id) as totker,
					count(d.peserta_didik_id) as totwir,
					count(e.peserta_didik_id) as totkul,
					count(a.peserta_didik_id) as totsiw
					from ref.peserta_didik a
					left join kerja_siswa b on a.peserta_didik_id=b.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					left join wira_siswa d on a.peserta_didik_id=d.peserta_didik_id
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3'
					group by c.nama
					order by count(a.peserta_didik_id) - count(d.peserta_didik_id) - count(e.peserta_didik_id) - count(b.peserta_didik_id) desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					// foreach ($qry as $key2) {
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$t1.'-'.$t3."</center>",
						"<center>".number_format($key->totsiw-$key->totker-$key->totwir-$key->totkul)."</center>");
					array_push($d['data'], $dt);
					// }
				}
				echo json_encode($d);
			}else{
				$data = $this->db->query("SELECT c.nama as nama_sekolah, count(b.peserta_didik_id) as totker,
					count(d.peserta_didik_id) as totwir,
					count(e.peserta_didik_id) as totkul,
					count(a.peserta_didik_id) as totsiw
					from ref.peserta_didik a
					left join kerja_siswa b on a.peserta_didik_id=b.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					left join wira_siswa d on a.peserta_didik_id=d.peserta_didik_id
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					where left(a.tanggal_keluar,4)='$tahun'
					group by c.nama
					order by count(a.peserta_didik_id) - count(d.peserta_didik_id) - count(e.peserta_didik_id) - count(b.peserta_didik_id) desc")->result();
				$d['data'] = array();
				$no = "";
				foreach ($data as $key) {
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->nama_sekolah,
						"<center>".$tahun."</center>",
						"<center>".number_format($key->totsiw - $key->totker - $key->totwir - $key->totkul)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}

			/*semua*/
		}else{
			redirect("_404","refresh");
		}
	}
	public function getSemua($jenis, $tahun){
		$bln = date('n');
		if($bln<7){
			$t1 = date('Y') - 3;
			$t2 = $t1+1;
			$t3 = $t1+2;
		}else{
			$t1 = date('Y') - 3;
			$t2 = $t1+1;
			$t3 = $t1+2;
		}
		/*semua tahun*/
		if ($jenis =="0") {
			if ($tahun == "0") {
				$data = $this->db->query("SELECT c.nama as nama_sekolah, c.npsn, count(b.peserta_didik_id) as totker,
					count(d.peserta_didik_id) as totwir,
					count(e.peserta_didik_id) as totkul,
					count(a.peserta_didik_id) as totsiw
					from ref.peserta_didik a
					left join kerja_siswa b on a.peserta_didik_id=b.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					left join wira_siswa d on a.peserta_didik_id=d.peserta_didik_id
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3'
					group by c.nama, c.npsn
					order by count(a.peserta_didik_id) + count(d.peserta_didik_id) + count(e.peserta_didik_id) - count(b.peserta_didik_id) - count(d.peserta_didik_id)- count(e.peserta_didik_id)-count(b.peserta_didik_id) desc")->result();
				$d['data'] = array();
				$no = 0;
				foreach ($data as $key) {
					$tdk = $key->totsiw-$key->totker-$key->totwir-$key->totkul;
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->npsn,
						$key->nama_sekolah,
						"<center>".$t1.'-'.$t3."</center>",
						"<center>".number_format($key->totker)."</center>",
						"<center>".number_format($key->totwir)."</center>",
						"<center>".number_format($key->totkul)."</center>",
						"<center>".number_format($key->totsiw-$key->totker-$key->totwir-$key->totkul)."</center>",
						"<center>".number_format($key->totker+$key->totwir+$key->totkul+$tdk)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}else{
				$data = $this->db->query("SELECT c.nama as nama_sekolah, c.npsn, count(b.peserta_didik_id) as totker,
					count(d.peserta_didik_id) as totwir,
					count(e.peserta_didik_id) as totkul,
					count(a.peserta_didik_id) as totsiw
					from ref.peserta_didik a
					left join kerja_siswa b on a.peserta_didik_id=b.peserta_didik_id
					left join ref.sekolah c on a.sekolah_id=c.sekolah_id
					left join wira_siswa d on a.peserta_didik_id=d.peserta_didik_id
					left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id
					where left(a.tanggal_keluar,4)='$tahun'
					group by c.nama, c.npsn
					order by count(a.peserta_didik_id) + count(d.peserta_didik_id) + count(e.peserta_didik_id) - count(b.peserta_didik_id) - count(d.peserta_didik_id)- count(e.peserta_didik_id)-count(b.peserta_didik_id) desc")->result();
				$d['data'] = array();
				$no = 0;
				foreach ($data as $key) {
					$tdk = $key->totsiw-$key->totker-$key->totwir-$key->totkul;
					$no++;
					$dt = array("<center>".$no.".</center>",
						$key->npsn,
						$key->nama_sekolah,
						"<center>".$tahun."</center>",
						"<center>".number_format($key->totker)."</center>",
						"<center>".number_format($key->totwir)."</center>",
						"<center>".number_format($key->totkul)."</center>",
						"<center>".number_format($key->totsiw-$key->totker-$key->totwir-$key->totkul)."</center>",
						"<center>".number_format($key->totker+$key->totwir+$key->totkul+$tdk)."</center>");
					array_push($d['data'], $dt);
				}
				echo json_encode($d);
			}

		}else{
			redirect('_404', 'refresh');
		}
	}
}
