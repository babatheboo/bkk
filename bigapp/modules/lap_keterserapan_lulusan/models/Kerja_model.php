<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kerja_model extends CI_Model {
    var $table = 'mview_rank_tahun';
    var $column_order = array('nama_sekolah','sekolah_id','tahun','total_bekerja');
    var $column_search = array('nama_sekolah'); 
    var $order = array('total_bekerja' => 'desc');
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    private function _get_datatables_query(){
        // if($this->input->post('status')){
        //     $this->db->where("jenis_kelamin=",$this->input->post('jns_kel'));
        // }
        $this->db->from($this->table);
        // if($this->input->post('tahun')){
        //     $this->db->where('tahun', $this->input->post('tahun'));
        // }
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}