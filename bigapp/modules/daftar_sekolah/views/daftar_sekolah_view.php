<!DOCTYPE html>
<html lang="id">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daftar BKK - Bursa Kerja Khusus</title>
    <link href="data:image/x-icon;base64,AAABAAEAEBAAAAAAAABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAA/QEGAP/r6gDVBgQAx6urAKh6GwC8R0gA9fv/AOcBBwDqAgEA//r8AM6TkQDtBQEAwAYFALUTEQDhYGUAfHRtAKIGBgCoAgMAuri4AOmPjgB+fHwAzhQUAP/19wDWqaYA+/39AP/9/QDsAwUA7gICAOKmpgCdAQEAsIuNAKFyFwD6AgIAtJGNAPwABQDs0dUA8c/PAKiKZwDQDQkArAcKAPb4+ACSbm4A+Pj4APb8+wDp4eEA//byAIZ9gAD/+PgAszAzAOYFAwCtEhkA///7AMV5cwDADAcA9nR1APsBAAD8AQAA/gEAAMYQCgD6BgYAw4WFAF02CQDcwL8AhAUGAIslJgC+kzIA3wQBAPX6/wDaBwoA//f2AJABAwD6/PwA/vj5APo9PgD5/f8A//r/AFAAAgDPLSsAmIeEAOG5tAC3SEYAvW9pAN7IvQDFt7gAw7u7AOfo5gCZAAEAvKGkALYPEgDvAgUA7wQCAPMBAgDFBQYA9QECAPHIxgD16uwA4jpAAPkEAgBPBAYArZucANcIBgC/bGoAmY6RAONxeAD/hIMA+Tw9APv//gC2pKUA5wUGAO8AAADwu8QA6QUGAPQAAAClICIA9QAAAI+DgwDrf34A+gAAAJsJCwD5AwAA+QIGAPwDAADJCQoAqwACAPI2NQCuJyUA0svIAL2ijgClZGYA/f//AJlRSQCvGBQAwTAuAPYAAQD4AAEA9gMBAMQJCwB1BgQA+AMBAPsAAQCZHBgAtQEGANLQzwDAursA9v79AOICCAD4/v0Aj318AO4AAgDizskA+uHdAN1iYAD/3doA7nh9APMDAgDEeYEA5dLVAPR6egDPAQYA/AACAP0DAgD+AwIArAIBALw/QQCDAAUA2icmAMk3OwDgAgAA3gcGAD8BAQDnBgMAaAYGAP/+/gD43t4Aw5+ZAPICAADHAAQA2o+NAPcCAAD4AgAAeXR1AOEbFQD7AgAA/+TnAOqGhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKk2lHhJ8o1eKgQAAAAAABAOzO14BPCE2h5sLAAAAAECGoYU/VDBYFLN6SgAAAAAbZZZElJG1rwcae7C4AACqCSJMfxo+IJN0gi1DmAAAR0WCKWc0BUIrVRmtDmoAALGkEyxrroAmJGxIVpkIAh9bUJxGKQ9PU3dLCjVJbrJjIZ8ctzJtYC8ncDhxiaKerHaIOrM5KI8QUYy3iyOMN1+oklpzoIMXFS5ijHFhtiUAPU51m3iXkGSdtAxpjW8AAABBclmmqQ0zq45oMR0AAAAAAABdFlIRBqeEmgAAAAAAAAAAAGaVeVx9GAAAAAAAAAAAAAAAAH65AAAAAAAAAOAHAADAAwAAwAMAAMABAACAAQAAgAEAAIAAAAAAAAAAAAAAAAAAAAAAAAAAgAEAAMADAADwDwAA+B8AAP5/AAA=" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel='stylesheet' type="text/css" href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700,300'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/bootstrap-material-design/css/bootstrap-material-design.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/bootstrap-material-design/css/ripples.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/amcharts/plugins/export/export.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo base_url();?>font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/social-share-kit.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/icomoon/styles.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/animate/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/jquery-countdown/css/jquery.countdown.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/jquery-marquee/css/jquery.marquee.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/jquery-event-calendar/css/eventCalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/jquery-event-calendar/css/eventCalendar_theme_responsive.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/libs/jquery-event-calendar/css/eventCalendar_theme.min.css">
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css" rel="stylesheet" />
</head>
<body>
<nav class="navbar navbar-fixed-top navbar-custom navbar-info" role="navigation">
    <div class="container">
        <?php echo $this->load->view("menu_view");?>
    </div>
</nav>
<div class="top"></div>
<div class="container isi">
    <div class="info"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="panel panel-inverse" data-sortable-id="form-validation-2">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo $halaman;?></h4>
                    </div>
                    <div class="panel-body panel-form">
                        <form id="cek_nisn" class="form-horizontal form-bordered" action="<?php echo $action;?>/daftar" method="post" data-parsley-validate="true">
                           
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Provonsi :</label>
                                <div class="col-md-3 col-sm-3">
                                    <select id="prov" class="form-control selectpicker" data-live-search='true' onchange="cek_prov('prov','daftar_sekolah','cek_prov');">
                                        <?php 
                                            foreach ($option_prov as $key => $value) {
                                                ?>
                                                <option value="<?php echo $key ?>"> <?php echo $value ?> </option>
                                                <?php
                                            }
                                        ?>
                                    </select>                                    
                                </div>
                            </div>
                            <div class="form-group" id="form_kota" style="display: none">
                                <label class="control-label col-md-3 col-sm-3">Kota :</label>
                                <div class="col-md-3 col-sm-3" id="kota">
                                                                       
                                </div>
                            </div>
                            <div class="form-group" id="form_sekolah" style="display: none">
                                <label class="control-label col-md-3 col-sm-3">Sekolah :</label>
                                <div class="col-md-3 col-sm-3" id="sekolah">
                                                                       
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Nama Pendaftar:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input type="hidden" id="sekolah_id" name="sekolah_id"  value="" data-type="text"/>  
                                    <input class="form-control" type="text" id="nama_pendaftar" minlength="1" name="nama_pendaftar" value="" data-type="text"/>   
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">NIP:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input class="form-control" type="text" id="nip" minlength="1" name="nip" value="" data-type="text"/>   
                                </div>
                            </div>               
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Email:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input class="form-control" type="text" id="email" minlength="1" name="email" value="" data-type="text"/>   
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Telp:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input class="form-control" type="text" id="telp" minlength="1" name="telp" value="" data-type="text"/>   
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Jabatan:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input class="form-control" type="text" id="jabatan" minlength="1" name="jabatan" value="" data-type="text"/>   
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">No Izin:</label>
                                <div class="col-md-4 col-sm-4">
                                    <input class="form-control" type="text" id="no_izin" minlength="1" name="no_izin" value="" data-type="text"/>   
                                </div>
                            </div> 
                            
                            <div class="form-group" id="tombol" style=" display: none">
                                <label class="control-label col-md-3 col-sm-3"></label>
                                <div class="col-md-3 col-sm-3">
                                    <button type="submit" class="btn btn-success"><?php echo $tombolsimpan;?></button>
                                    <button type="button" onclick="history.go(-1)" class="btn btn-info"><?php echo $tombolbatal ; ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
<div class="copyright">
    <p class="text-center">Copyright © 2017 Direktorat Pembinaan SMK</p>
</div>
<div class="ssk-sticky ssk-left ssk-center ssk-md">
    <a href="#" class="ssk ssk-facebook"></a>
    <a href="#" class="ssk ssk-twitter"></a>
    <a href="#" class="ssk ssk-google-plus"></a>
    <a href="#" class="ssk ssk-linkedin"></a>
    <a href="#" class="ssk ssk-pinterest"></a>
    <a href="#" class="ssk ssk-tumblr"></a>
</div>
<div class="bs-component btn-group-sm" id="keAtas">
    <a href="javascript:void(0)" class="btn btn-info btn-fab" title="Kembali keatas"><i class="material-icons">expand_less</i></a>
</div>
<script src="<?php echo base_url();?>assets/libs/jquery/jquery-1.11.3.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/bootstrap-material-design/js/material.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/bootstrap-material-design/js/ripples.min.js"></script>
<script src="<?php echo base_url();?>assets/js/social-share-kit.js"></script>
<script src="<?php echo base_url();?>assets/js/functions.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/jquery-marquee/js/jquery.marquee.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libs/jquery-countdown/js/jquery.plugin.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/jquery-countdown/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/jquery-countdown/js/jquery.countdown-id.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/jquery-event-calendar/js/jquery.eventCalendar.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/underscore/underscore.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/libs/numeral/numeral.min.js"></script>
<script src="<?php echo base_url();?>modules/frontpage.min.js"></script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>

</body>
</html>