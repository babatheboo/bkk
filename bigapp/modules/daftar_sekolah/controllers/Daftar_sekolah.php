<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Daftar_Sekolah extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		//$this->db1 = $this->load->database('default', TRUE);
		//$this->load->model('Daftar_siswa_model');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['page'] = "daftar_siswa";
		$isi['link'] = 'daftar_siswa';
		$isi['halaman'] = "Formulir Pendaftaran (Sekolah)";
		$isi['judul'] = "Pendaftaran (Sekolah)";
		$isi['content'] = "daftar_sekolah_view";

		$isi['tombolsimpan'] = "Daftar";
		$isi['tombolbatal'] = "Batal";
		$isi['action'] = "daftar_sekolah";

		$isi['option_prov']['']  = "Pilih Provinsi";
		$this->db->order_by('prov');
		$ang = $this->db->get('ref.view_provinsi')->result();
		foreach($ang as $key)
		{
			$isi['option_prov'][$key->kode_prov]=str_replace("Prop. ", "", $key->prov);
		}
		

		$this->load->view("daftar_sekolah/daftar_sekolah_view",$isi);
	}
	public function getData(){
			
	}

	public function cek_npsn($kode=Null){
		$ckdata = $this->db->query("select * from ref.sekolah where npsn='".$kode."'")->result();
		if(count($ckdata)==1){
			$data['say'] = "ok";
			$sekolah_id="";
			foreach($ckdata as $key){
				$data['nama']=$key->nama;
				$sekolah_id=$key->sekolah_id;
			}
			$qr="select * from ref.sekolah join public.sekolah_terdaftar on ref.sekolah.sekolah_id=public.sekolah_terdaftar.sekolah_id where ref.sekolah.sekolah_id='".$sekolah_id."'";
			$ckdata2 = $this->db->query($qr)->result();
			if(count($ckdata2)==1){
				$data['sekolah_terdaftar']="Sudah Terdaftar";
			}else{
				$data['sekolah_terdaftar']="Belum Terdaftar";
			}
			
		}else{
			$data['say'] = "NotOk";
		}
		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}

	public function cek_prov($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_kota where kode_prov='".$kode."' ORDER BY kota ASC")->result();
		$data['say'] = '
			<select id="id_kota" class="form-control selectpicker" data-live-search="true" onchange="cek_sekolah(\'id_kota\',\'daftar_sekolah\',\'cek_sekolah\');">
				<option value="">Pilih Kota</option>
		';
		
		foreach($ckdata as $key){
			$data['say'].='<option value="'.$key->kode_kota.'">'.$key->kota.'</option>';
		}
		$data['say'].='</select>';

		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}

	public function cek_sekolah($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_sekolah_wilayah where kode_kota='".$kode."' ORDER BY nsekolah ASC")->result();
		$data['say'] = '
			<select id="id_sekolah" class="form-control selectpicker" data-live-search="true" onchange="if(this.value!=\'\'){$(\'#tombol\').show();$(\'#sekolah_id\').val(this.value);}else{$(\'#tombol\').hide();}">
				<option value="">Pilih Sekolah</option>
		';
		
		foreach($ckdata as $key){
			$data['say'].='<option value="'.$key->sekolah_id.'">'.$key->nsekolah.'</option>';
		}
		$data['say'].='</select>';

		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}

	public function daftar(){
		//$pass = $this->bcrypt->hash_password($this->input->post('update_password'));
		//$ckdata = $this->db->query("update app.username set password='".$pass."' where user_id='".$this->input->post('peserta_didik_id')."'");
		$ckdata = $this->db->query("select * from app.registrasi_sekolah where sekolah_id='".$this->input->post('sekolah_id')."'")->result();
		if(count($ckdata)==1){			
			$isi['keterangan'] = "Sekolah Anda Sudah Terdaftar!";
			
		}else{
			$this->db->query("insert into app.registrasi_sekolah(sekolah_id,nama_pendaftar,nip,email,tlp,jabatan,no_izin,status) values('".$this->input->post('sekolah_id')."','".$this->input->post('nama_pendaftar')."','".$this->input->post('nip')."','".$this->input->post('email')."','".$this->input->post('telp')."','".$this->input->post('jabatan')."','".$this->input->post('no_izin')."',0) ");
			$isi['keterangan'] = "Terima Kasih Sudah Mendaftar!";
		}
		$isi['page'] = "daftar_sekolah";
		$isi['link'] = 'daftar_sekolah';
		$isi['halaman'] = "Formulir Pendaftaran BKK";
		$isi['judul'] = "Pendaftaran BKK";
		$isi['content'] = "daftar_sekolah_view";

		$isi['tombolsimpan'] = "Daftar";
		$isi['tombolbatal'] = "Batal";
		$isi['action'] = "daftar_sekolah";
		$this->load->view("daftar_sekolah/daftar_sekolah_sukses_view",$isi);
	}
}
