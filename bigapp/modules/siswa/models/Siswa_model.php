<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Siswa_model extends CI_Model {
    var $table = 'ref.peserta_didik';
    var $column_order = array(null,'ref.peserta_didik.nisn','ref.peserta_didik.nama','ref.peserta_didik.jenis_kelamin','ref.jurusan.nama_jurusan',null);
    var $column_search = array('ref.peserta_didik.nisn','ref.peserta_didik.nama','ref.peserta_didik.jenis_kelamin','ref.jurusan.nama_jurusan');
    var $order = array('ref.peserta_didik.nama' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    private function _get_datatables_query(){
        if($this->input->post('jurusan')){
            $this->db->where('ref.peserta_didik.jurusan_id', $this->input->post('jurusan'));
        }
        if($this->input->post('jns_kel')){
            $this->db->where("jenis_kelamin=",$this->input->post('jns_kel'));
        }
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->select("*,ref.jurusan.nama_jurusan");
        $this->db->from($this->table);
        $this->db->join('ref.jurusan','ref.peserta_didik.jurusan_id = ref.jurusan.jurusan_id');
        $this->db->where('ref.peserta_didik.sekolah_id',$sess_sekolah);
        $this->db->where('ref.peserta_didik.jenis_keluar_id !=','1');
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
       if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->where(array('sekolah_id'=>$sess_sekolah,'jenis_keluar_id !='=>'1'));
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        $sess_sekolah = $this->session->userdata('role_');
        $this->db->where(array('sekolah_id'=>$sess_sekolah,'jenis_keluar_id !='=>'1'));
        return $this->db->count_all_results();
    }
}
