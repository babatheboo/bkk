<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/js/siswa.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"> <button class="btn btn-danger btn-xs m-r-5" onclick="filter()"><i class="fa fa-search"></i> Filter Pencarian</button>&nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload Data</button> </div> <h4 class="panel-title"><?php echo $halaman;?></h4>
<div id="filter_pencarian">
                            <br/>
                                <?php echo form_dropdown('jurusan',$option_jurusan,isset($default['jurusan']) ? $default['jurusan'] : '','class="default-select2 form-control" style="width:25%" id="jurusan" name="jurusan" data-live-search="true" data-style="btn-white"');?>
                                <select name="jns_kel" class="default-select2 form-control" style="width:25%" id="jns_kel" name="jns_kel" data-live-search="true" data-style="btn-white">
								<option value="" selected="selected">Semua Jenis Kelamin</option>
								<option value="L">Laki - Laki</option>
								<option value="P">Perempuan</option>
								</select>
                        </div>
                         </div> <div class="panel-body"> <div class="table-responsive"> <table id="data-siswa" class="table table-striped table-bordered nowrap" width="100%"> <thead> <tr> <th style="text-align:center" width="1%">No.</th> <th style="text-align:center" width="10%">NISN</th> <th style="text-align:center" width="40%">Nama Siswa</th> <th style="text-align:center" width="5%">Jns Kel</th> <th style="text-align:center" width="30%">Jurusan</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>