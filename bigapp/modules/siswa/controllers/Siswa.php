<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Siswa extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("1");
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('siswa_model');
  		$this->load->library('bcrypt');
  		$this->load->helper('form');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$lvl = $this->session->userdata('level');
		$isi['kelas'] = "manaj_siswa";
		$isi['namamenu'] = "Data Siswa";
		$isi['page'] = "manaj_siswa";
		$isi['link'] = 'siswa';
		$isi['actionhapus'] = 'hapus';
		$isi['actionedit'] = 'edit';
		$isi['halaman'] = "Data Siswa";
        $sess_sekolah = $this->session->userdata('role_');
		$isi['option_jurusan'][''] = "Semua Jurusan";
		$ckjrs = $this->db1->query("SELECT a.jurusan_id,b.nama_jurusan FROM ref.view_kelas_12 a JOIN ref.jurusan b ON a.jurusan_id = b.jurusan_id WHERE a.sekolah_id = '$sess_sekolah'")->result();
		foreach ($ckjrs as $key ) {
			$isi['option_jurusan'][$key->jurusan_id] = $key->nama_jurusan;
		}
		$isi['judul'] = "Halaman Data Seluruh Siswa";
		$isi['content'] = "siswa_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->siswa_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->nisn;
				$row[] = $rowx->nama;
				$row[] = $rowx->jenis_kelamin;
				$row[] = $rowx->nama_jurusan;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->siswa_model->count_all(),
				"recordsFiltered" => $this->siswa_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }		
	}
}
