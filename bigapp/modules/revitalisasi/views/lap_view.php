<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css"> -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css">
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.flash.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/revitalisasi.js"></script>
<script language="JavaScript">
function qry()
{
    $('#tahun').val();
    opener.window.location.href += "?update=done";
    opener.window.location.reload();
    self.close();
    return false;
}
</script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn">
                    <a href="<?php echo base_url();?>revitalisasi/expRev" class="btn btn-success btn-xs m-r-5">
                        <i class="fas fa-file-excel"></i> Download
                    </a>
                    <!-- <button class="btn btn-danger btn-xs m-r-5" onclick="filter_data()"><i class="fa fa-search"></i> Filter Pencarian</button> -->
                    <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                        <i class="fas fa-sync-alt"></i> Reload Data
                    </button> 
                </div> 
                <h4 class="panel-title">Laporan Revitalisasi</h4>
                <!-- <div id="filter_pencarian">
                    <form action="" onload="qry()">
                        <select class="form-control" name="tahun" style="width: 25%">
                            <option value="" selected="selected">Pilih Tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                        </select>
                        <button type="submit" class="btn btn-primary">Cari</button>
                    </form>
                </div> -->
            </div> 
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-total" class="table table-striped table-bordered nowrap" width="100%">                
                        <thead> 
                            <tr> 
                                <th style="text-align:center" width="1%">No.</th> 
                                <th style="text-align:center" width="30%">Nama Sekolah</th>
                                <th style="text-align:center" width="10%">Total Bekerja</th>
                                <th style="text-align:center" width="10%">Total Wirausaha</th>
                                <th style="text-align:center" width="10%">Total Kuliah</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        </tbody> 
                    </table> 
                </div> 
            </div> 
        </div> 
    </div>
</div>