<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Revitalisasi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login('4');
		$this->load->model('revitalisasi_model');
		$this->load->model('rev_kuliah_model');
		$this->load->model('rev_wira_model');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index(){
		$isi['namamenu'] = "BKK";
		$isi['page']     = "revitalisasi";
		$isi['kelas']    = "revitalisasi";
		$isi['link']     = 'revitalisasi';
		$isi['halaman']  = "Revitalisasi";
		$isi['judul']    = "Revitalisasi";
		$isi['content']  = "lap_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
 	// public function tahuna(){
  //       $thaun = $this->input->post('tahun');
  //   }
	public function getData(){
		if($this->input->is_ajax_request()){
			$this->db->query("REFRESH MATERIALIZED view m_view_rev");
			$list = $this->revitalisasi_model->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[]  = $rowx->nama;
				// $row[]  = $rowx->total_bekerja;
				$row[]  = '<center>' . number_format($rowx->total_bekerja) . '</center>';
				$row[]  = '<center>' . number_format($rowx->total_wira) . '</center>';
				$row[]  = '<center>' . number_format($rowx->total_kuliah) . '</center>';
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->revitalisasi_model->count_all(),
				"recordsFiltered" => $this->revitalisasi_model->count_filtered(),
				"data"            => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}

	function expRev(){
		// $sid = $this->session->userdata('role_');
		$this->load->library('excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
		$cacheSettings = array( ' memoryCacheSize ' => '128MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		$excel = new PHPExcel();
		$excel->getProperties()->setCreator('NAA')
		->setLastModifiedBy('NAA')
		->setTitle("Data Revitalisasi")
		->setSubject("Revitalisasi")
		->setDescription("Laporan Revitalisasi")
		->setKeywords("Data Revitalisasi");
		$style_col = array('font' => array('bold' => true),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$style_num = array('font' => array('bold' => false),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA REVITALISASI");
		$excel->getActiveSheet()->mergeCells('A1:F1');
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NPSN");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "SMK");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Bekerja");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Wira");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kuliah");
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$bkk = $this->db->query("SELECT a.npsn, a.nama, b.total_bekerja, c.total_wira, d.total_kuliah FROM master_rev_view a LEFT JOIN view_kerja b ON a.nama = b.nama_sekolah LEFT JOIN view_wira c ON b.nama_sekolah = c.nama_sekolah LEFT JOIN view_kuliah d ON c.nama_sekolah = d.nama_sekolah WHERE a.rvt = '1' AND b.total_bekerja is not NULL ORDER BY b.total_bekerja DESC")->result();
		$no = 1;
		$numrow = 4;
		foreach($bkk as $data){
			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->npsn);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->total_bekerja);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->total_wira);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->total_kuliah);
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_num);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
			$no++;
			$numrow++;
		}
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(45);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		$excel->getActiveSheet(0)->setTitle("Laporan Revitalisasi");
		$excel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="Data Revitalisasi"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$writer->save('php://output');
		exit;
	}
}