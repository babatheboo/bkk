<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ketua_bkk extends CI_Controller {
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('ketua_model');
    }
    public function index(){
    $this->_content();
  }
  public function _content(){
    $isi['kelas'] = "admin";
    $isi['namamenu'] = "Data Ketua BKK";
    $isi['page'] = "ketua_bkk";
    $isi['cek'] = "edit";
    $isi['link'] = 'ketua_bkk';
    $isi['actionhapus'] = 'hapus';
    $isi['actionedit'] = 'edit';
    $isi['halaman'] = "Data Ketua BKK";
    $isi['judul'] = "Halaman Data Ketua BKK";
    $isi['content'] = "ketua_view";
    $this->load->view("dashboard/dashboard_view",$isi);
  }
  public function getData(){
    if($this->input->is_ajax_request()){
      $list = $this->ketua_model->get_datatables();
      $data = array();
      $no = $_POST['start'];
      foreach ($list as $rowx) {
        $skl = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='$rowx->sekolah_id'")->row();
        $no++;
        $row = array();
        $row[] = $no . ".";
        $row[] = $skl->nama;
        $row[] = $rowx->nama;
        $row[] = '<a href="https://wa.me/+62'.$rowx->tlp.'"" target="_blank">'.'0'.$rowx->tlp.'</a>';
        $row[] = $rowx->email;
        $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="cek('."'".$rowx->id."'".')"><i class="fas fa-pencil-alt"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="befHapus('."'".$rowx->id."'".','."'".$skl->nama."'".')"><i class="fas fa-trash"></i></a></center>';
        $data[] = $row;
      }
      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->ketua_model->count_all(),
        "recordsFiltered" => $this->ketua_model->count_filtered(),
        "data" => $data,
        );
      echo json_encode($output);
    }else{
      redirect("_404","refresh");
    }
  }

  public function cek($id){
    if($this->input->is_ajax_request()){
    $ckdata = $this->db->get_where('ketua_bkk',array('id'=>$id))->result();
    $data['idna']='';
    $data['smk']='';
    $data['nama']='';
    $data['tlp']='';
    $data['email']='';
    foreach ($ckdata as $key) {
      $skl = $this->db->query("SELECT nama FROM ref.sekolah WHERE sekolah_id='$key->sekolah_id'")->row();
    $data['idna']=$id;
    $data['smk']=$skl->nama;
    $data['nama']=$key->nama;
    $data['tlp']=$key->tlp;
    $data['email']=$key->email;
    }
    echo json_encode($data);
    }else{
    redirect("_404","refresh");
    }
    }

    function edit(){
      $idna = $this->input->post('idna');
      $nama = $this->input->post('nama');
      $tlp = $this->input->post('tlp');
      $email = $this->input->post('email');
      $arr = array('nama'=>$nama, 'tlp'=>$tlp, 'email'=>$email);
      $this->db->where('id',$idna);
      if ($this->db->update('ketua_bkk',$arr)) {
        $data['response'] = 'fun';
      }else{
        $data['response'] = 'nuf';
      }
      echo json_encode($data);
    }

    function hapus(){
      $idh = $this->input->post('idh');
      $this->db->where('id',$idh);
      if ($this->db->delete('ketua_bkk')) {
        $data['response'] = 'fun';
      }else{
        $data['response'] = 'nuf';
      }
      echo json_encode($data);
    }
}