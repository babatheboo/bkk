<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extension/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/ketua.js"></script>
<div class="row"> <div class="col-md-12"> 
    <div class="panel panel-inverse"> <div class="panel-heading"> 
        <div class="panel-heading-btn">
            <!-- <button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
                <i class="fa fa-plus-circle"></i> Tambah Data
            </button>&nbsp; -->
            <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                <i class="fas fa-sync-alt"></i> Reload Data
            </button>
        </div>
        <h4 class="panel-title"><?php echo $halaman;?></h4>
    </div>
    <div class="panel-body"> 
        <div class="table-responsive"> 
            <table id="data-ketua" class="table table-striped table-bordered nowrap" width="100%"> 
                <thead> 
                    <tr> 
                        <th style="text-align:center" width="1%">No.</th> 
                        <th style="text-align:center" width="15%">SMK</th>
                        <th style="text-align:center" width="15%">Nama Ketua</th>
                        <th style="text-align:center" width="20%">No. Tlp</th>
                        <th style="text-align:center" width="10%">E-mail</th>
                        <th style="text-align:center" width="10%">Aksi</th>
                    </tr> 
                </thead> 
                <tbody> 
                </tbody> 
            </table> 
        </div> 
    </div> 
</div> 
</div>
</div>
<div id="modal-edit" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="javascript:edit()" class="form-horizontal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Data Ketua BKK</h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" id="idna" name="idna"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">SMK</label>
                            <div class="col-md-12">
                                <input name="smk" id="smk" placeholder="Nama SMK" readonly="" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Ketua</label>
                            <div class="col-md-12">
                                <input name="nama" id="nama" placeholder="Nama Ketua" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">No. Tlp</label>
                            <div class="col-md-12">
                                <input name="tlp" id="tlp" placeholder="Nomor Tlp" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-12">
                                <input name="email" id="email" placeholder="Alamat Surel" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
            </form> 
        </div>
    </div>
</div>

<div id="modal-hapus" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="javascript:hapus()" class="form-horizontal">
            <div class="modal-body">
                    <input type="hidden" id="idh" name="idh"/>
                    <div class="form-body">
                        <div class="form-group">
                            <center><h4>Hapus data ketua BKK <span id="nm"></span>?</h4></center>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
            </div>
            </form> 
        </div>
    </div>
</div>
