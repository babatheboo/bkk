<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ketua_model extends CI_Model {
    var $table = 'ketua_bkk';
    var $column_order = array('id','sekolah_id','nama','tlp','email');
    var $column_search = array('nama');
    var $order = array('id' => 'desc');
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    private function _get_datatables_query(){
        $this->db->from($this->table);
        if($_POST['search']['value']!=''){
            $word = explode(" ", $_POST['search']['value']);
            $formatted_word = array();
            foreach ($word as $keyword) {
                array_push($formatted_word, '('.$keyword.')');
       
            }
            $formulated_word='';
            for ($i=0; $i < count($formatted_word); $i++) { 
            if($i<count($formatted_word)-1){
                    $formulated_word.=$formatted_word[$i].'|';
                }else{
                    $formulated_word.=$formatted_word[$i];
                }
            }
            $where_str='';
            if($formulated_word!=''){
                foreach ($this->column_search as $item) {
                    if($where_str!=''){
                        $where_str.=" or lower(".$item.") similar to lower('%(".$formulated_word.")%')";
                    }else{
                        $where_str.=" lower(".$item.") similar to lower('%(".$formulated_word.")%')";
                    }
                }
                $this->db->where($where_str);
            }
    }
}
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function simpan($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
    public function hapus_by_id($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}