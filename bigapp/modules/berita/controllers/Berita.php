<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Berita extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('0|4');
  		$this->load->model('berita_model');
		$this->db1 = $this->load->database('default', TRUE);
  		$this->load->library('bcrypt');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['link'] = 'user_bkk';
		$isi['halaman'] = "Berita / Informasi";
		$isi['page'] = "berita";
		$isi['judul'] = "List Berita";
		$isi['content'] = "list_berita";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->berita_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = substr(html_entity_decode($rowx->judul),0,50). "......";
				$row[] = substr(html_entity_decode($rowx->isi),0,75). "......";
				$row[] = $rowx->nama;
				$row[] = $rowx->tgl_dibuat;
				$row[] = $rowx->publish;
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'user_bkk\',\'edit_data\','."'".$rowx->id."'".')"><i class="icon-pencil"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data User\',\'user_bkk\',\'hapus_data\','."'".$rowx->id."'".')"><i class="icon-remove icon-white"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->berita_model->count_all(),
				"recordsFiltered" => $this->berita_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}

	public function add_news()
	{
		$isi['link'] = 'user_bkk';
		$isi['halaman'] = "Berita / Informasi";
		$isi['page'] = "berita";
		$isi['judul'] = "Buat Berita";
		$isi['content'] = "form_berita";
		$this->load->view("dashboard/dashboard_view",$isi);
	}

	public function save_news()
	{
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$publish = $this->input->post('publish');
		if($publish=="on")
		{
			$publish="1";
		}
		else
		{
			$publish="0";
		}
 		$tgl = date("d-m-Y H:i:s");
		$data = array('judul'=>$judul,'isi'=>htmlentities($isi),'publish'=>$publish,'tgl_dibuat'=>$tgl,
			'dibuat_oleh'=>$this->session->userdata('user_id'));
		if($this->db1->insert('news',$data))
		{
			redirect('berita','refresh');
		}
		else
		{
			$this->add_news();
		}
	}

	public function proses_add(){
		if($this->input->is_ajax_request()){
			$method = "save";
	        $this->_validasi($method);
			$nip = $this->input->post('nip');
			$tlp = $this->input->post('tlp');
	        $nama = $this->input->post('nama');
	        $email = $this->input->post('mail');
	        $sess_skolah = $this->session->userdata('sekolah_sess');
	        $data = array('sekolah_id' => $sess_skolah,
	        	'nip'=>$nip,
	        	'nama'=>htmlspecialchars($nama),
	        	'email'=>$email,
	        	'tlp' =>$tlp,
	        );
	        $insert = $this->berita_model->simpan($data);
	        $q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
			$qry = $this->db1->query($q);
			$key = $qry->row();
			$user_id = $key->user_id;
			$pass = $this->bcrypt->hash_password($nip);
			$dd = array('user_id' => $user_id ,
				'username'=> $nip,
				'password'=>$pass,
				'level' => '1',
				'login' => '1' 
			);
			$this->db1->insert('app.username',$dd);
	        echo json_encode(array("status" => TRUE));
        }else{
	    	redirect("_404","refresh");
	    }
    }
  
	public function proses_edit(){
		if($this->input->is_ajax_request()){
			$method = "edit";
	        $this->_validasi($method);
	       	$nip = $this->input->post('nip');
			$tlp = $this->input->post('tlp');
	        $nama = $this->input->post('nama');
	        $email = $this->input->post('mail');
	        $sess_skolah = $this->session->userdata('sekolah_sess');
	        $data = array('sekolah_id' => $sess_skolah,
	        	'nip'=>$nip,
	        	'nama'=>htmlspecialchars($nama),
	        	'email'=>$email,
	        	'tlp' =>$tlp,
	        );
	        $this->berita_model->update(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
	public function hapus_data($id){
		if($this->input->is_ajax_request()){
			$ckuserid = $this->db->query("SELECT user_id FROM app.user_sekolah WHERE id = '$id'");
			$row = $ckuserid->row();
			$this->berita_model->hapus_user_id($row->user_id);
	        $this->berita_model->hapus_by_id($id);
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
    public function edit_data($id){
		if($this->input->is_ajax_request()){
			$data = $this->berita_model->get_by_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}
}
