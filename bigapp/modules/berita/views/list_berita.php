<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/berita.js"></script>

<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> 
<div class="panel-heading"> <div class="panel-heading-btn">
<button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
<i class="fa fa-plus-circle"></i> Tambah Berita</button>&nbsp;
<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
<i class="fa fa-refresh"></i> Reload Data</button> </div> 
<h4 class="panel-title"><?php echo $halaman;?></h4> </div> <div class="panel-body"> 
<div class="table-responsive"> 
<table id="data-user" class="table table-striped table-bordered nowrap" width="100%"> 
<thead> <tr>
 <th style="text-align:center" width="1%">ID</th> 
 <th style="text-align:center" width="10%">Judul</th>
  <th style="text-align:center" width="40%">Isi</th>
  <th style="text-align:center" width="10%">Dibuat Oleh</th>
  <th style="text-align:center" width="10%">Tgl dibuat</th>
  <th style="text-align:center" width="10%">Publish</th> 
  <th style="text-align:center" width="9%">Action</th> </tr> 
  </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>

