<script src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
  selector: 'textarea',
  height: 300,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code responsivefilemanager'
  ],

  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager',

    external_filemanager_path:"<?php echo base_url();?>filemanager/",
   filemanager_title:"Image Gallery" ,
   external_plugins: { "filemanager" : "<?php echo base_url();?>filemanager/plugin.min.js"}

});

var imageFilePicker = function (callback, value, meta) {               
    tinymce.activeEditor.windowManager.open({
        title: 'File and Image Picker',
        url: '/myapp/getfilesandimages',
        width: 700,
        height: 600,
        buttons: [{
            text: 'Insert',
            onclick: function () {
                //do some work to select an item and insert it into TinyMCE
                tinymce.activeEditor.windowManager.close();
            }
        }, 
        {
            text: 'Close',
            onclick: 'close'
        }],
    }, 
    {
        oninsert: function (url) {
            callback(url); 
        }
    });
};

</script>
<div class="row">
    <div class="col-md-12">
    <div class="panel panel-inverse" data-sortable-id="form-validation-2">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Buat berita baru</h4>
        </div>
        <div class="panel-body">
           <form action="save_news" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Judul Berita</label>
                    <input class="form-control" id="judul" name="judul" placeholder="Judul" type="text">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Isi Berita</label>
                    <textarea name="isi"></textarea>
                </div>
                <div class="checkbox">
                  <label>
                      <input type="checkbox" name="publish"> Publish
                  </label>
              </div>
              <button type="submit" class="btn btn-sm btn-primary m-r-5">Simpan</button>
          </form>
        </div>
    </div>
  </div>
</div>