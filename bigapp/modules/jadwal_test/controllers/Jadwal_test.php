<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jadwal_test extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->asn->login();
		if($this->session->userdata('level')==1){
			$this->asn->role_akses("1");
		}elseif($this->session->userdata('level')==2){
			$this->asn->role_akses("2");
		}
		$this->load->library('encryption'); 
		$this->load->model('jadwal_model');
		$this->load->model('jadwal_tes_model');
		$this->load->model('jadwal_x_model');
		date_default_timezone_set('Asia/Jakarta');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['kelas'] = "jadwal_test";
		$isi['namamenu'] = "Data Mitra Industri";
		$isi['page'] = "bursa";
		$isi['link'] = 'jadwal_test';
		$isi['halaman'] = "Data Jadwal Tes";
		$isi['judul'] = "Halaman Data Jadwal Tes";
//		$isi['content'] = "jadwal_content";
		$sid = $this->session->userdata('role_');
		$b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
		$valid = "";
		foreach ($b as $key) {
			$valid = $key->valid;
		}
		if($valid=="1"){
			$isi['content']              = "jadwal_content";
		} else {
			$isi['content']              = "admin_bkk";
		}



		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getJadwalTes(){
		if($this->input->is_ajax_request()){
			$list = $this->jadwal_tes_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = '<center>' . $no . "." . '</center>';
				$row[] = $rowx->judul_tes;
				$row[] = $rowx->judul_loker;
				$row[] = '<center>'.date('d-m-Y',strtotime($rowx->tgl_mulai)).'</center>';
				$row[] = '<center>'.date('d-m-Y',strtotime($rowx->tgl_selesai)).'</center>';
				$row[] = $rowx->status ? '<center><a href="javascript:void(0)" onclick="rbstatus(\'aktif\',\'Jadwal Tes\',\'jadwal_test\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-info" title="Status Aktif"><i class="fa fa-unlock icon-white"></i></a>' : '<center><a href="javascript:void(0)" onclick="rbstatus(\'inaktif\',\'Jadwal Tes\',\'jadwal_test\','."'".$rowx->id."'".')" data-toggle="tooltip" class="btn btn-xs m-r-5 btn-danger" title="Status NonAktif"><i class="fas fa-lock icon-white"></i></a>';
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="'.base_url().'jadwal_test/edit/'.$rowx->id.'" title="Edit Data"><i class="fas fa-pencil-alt"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data Jadwal Test\',\'jadwal_test\',\'hapus_data\','."'".$rowx->id."'".')"><i class="fas fa-trash"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->jadwal_tes_model->count_all(),
				"recordsFiltered" => $this->jadwal_tes_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}		
	}
	public function add_jadwal(){
		$sess = $this->session->userdata('role_');
		$isi['option_kat_loker'][''] = "Semua Kategori";
		$ckkat = $this->db1->query("SELECT a.kode_bidang,b.nama_bidang_usaha FROM view_lowongan_detil a JOIN ref.bidang_usaha b ON a.kode_bidang = b.bidang_usaha_id GROUP BY a.kode_bidang,b.nama_bidang_usaha")->result();
		if(count($ckkat)>0){
			foreach ($ckkat as $row) {
				$isi['option_kat_loker'][trim($row->kode_bidang)] = $row->nama_bidang_usaha;
			}
		}else{
			$isi['option_kat_loker'][''] = "Kategori Lowongan Belum Tersedia";
		}
		$ckmitra = $this->db->query("SELECT a.kode_perusahaan,b.nama FROM view_lowongan_detil a JOIN ref.industri b ON a.kode_perusahaan = b.industri_id::varchar GROUP BY a.kode_perusahaan,b.nama")->result();
		if(count($ckmitra)>0){
			$isi['option_industri'][''] = "Semua Industri";
			foreach ($ckmitra as $key) {
				$isi['option_industri'][$key->kode_perusahaan] = $key->nama;
			}
		}else{
			$isi['option_industri'][''] = "Anda Belum Memiliki Mitra Industri";
		}
		$this->asn->cekSekolah($sess);
		if($this->session->userdata('cek_terdaftar_')=="Y"){
			$isi['tot_loker'] = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (status = '1' OR status = '9')")->num_rows();
		}else{
			$isi['tot_loker'] = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE status = '1'")->num_rows();
		}
		$isi['namamenu'] = "Data Lowongan Pekerjaan";
		$isi['halaman'] = "Lowongan Pekerjaan";
		$isi['judul'] = "Halaman Lowongan Pekerjaan";
		$isi['page'] = "bursa";
		$isi['link'] = 'loker';
		$isi['content'] = "_content";
		$this->load->view('dashboard/dashboard_view',$isi);
	}
	public function getSakola(){
		if($this->input->is_ajax_request()){
			$list = $this->loker_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $rowx->npsn;
				$row[] = $rowx->nama;
				$row[] = '<a href="'.$rowx->website.'" target="_blank">Link</a>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->loker_model->count_all(),
				"recordsFiltered" => $this->loker_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function getPelamar(){
		if($this->input->is_ajax_request()){
			$this->load->model('pelamar_model');
			$list = $this->pelamar_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $rowx->id . "|" .$rowx->peserta_didik_id;
				$foto = $rowx->foto;
				if($foto!=Null){
					$foto = $rowx->foto;
				}else{
					$foto = "no.jpg";
				}
				$row[] = '<center><img style="height:100px" src="'.base_url().'assets/foto/siswa/'.$foto.'"></center>';
				$row[] = '<a href="'.base_url().'jadwal_test/detil_pelamar/'.$rowx->peserta_didik_id.'" title="Info Pelamar">'.$rowx->nisn.'</a>';
				$row[] = $rowx->nama;
				$row[] = $rowx->nama_sekolah;
				$row[] = '<center>'.$rowx->umur.'</center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->pelamar_model->count_all(),
				"recordsFiltered" => $this->pelamar_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->loker_x_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $rowx->nama;
				$row[] = $rowx->judul;
				$row[] = $rowx->nama_bidang_usaha;
				$row[] = $rowx->range;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->loker_x_model->count_all(),
				"recordsFiltered" => $this->loker_x_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function getLoker(){
		if($this->input->is_ajax_request()){
			$this->db1->query("refresh materialized view view_lowongan_tujuan");
			$this->db1->query("refresh materialized view view_lowongan");
			$this->db1->query("refresh materialized view view_lowongan_detil");
			$sess = $this->session->userdata('role_');
			$this->asn->cekSekolah($sess);
			$page =  $_GET['page'];
			$kategori = $_GET['kat'];
			$industri = $_GET['industri'];
			$cari = $_GET['cari'];
			$offset = 10*$page;
			$limit = 10;
			if($kategori!="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND (kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND industri_id = '$industri') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND industri_id = '$industri' AND (status = '1' OR status = '9') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT  $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND industri_id = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') AND (judul LIKE '%$cari%' OR deskripsi LIKE '%$cari%') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori!="" && $industri=="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND kode_kat_lowongan = '$kategori' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri!="" && $cari==""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND industri_id = '$industri' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori=="" && $industri=="" && $cari!=""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (judul LIKE '%$cari%') OR (deskripsi LIKE '%$cari%') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}elseif($kategori==="" && $industri==="" && $cari===""){
				$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE (sekolah_id = '$sess' OR sekolah_id ='9') AND sekolah_pembuat = '$sess' AND to_char(tgl_tutup,'YYYY-mm-dd') >= to_char(now(),'YYYY-mm-dd') AND (status = '1' OR status = '9') LIMIT $limit OFFSET $offset")->result();
			}
			$ckuser = $this->db1->get_where('app.user_sekolah',array('sekolah_id'=>$this->session->userdata('role_')))->result();
			if(count($ckdata)>0){
				foreach ($ckdata as $row) {
					$ckper = $this->db1->get_where('ref.industri',array('industri_id'=>$row->kode_perusahaan));
					if(count($ckper->result())>0){
						$x = $ckper->row();
						$nama_perusahaan = $x->nama;
					}else{
						$nama_perusahaan = 'NotFound';
					}
					$cksekolah = $this->db1->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$row->dibuat_oleh'");
					if(count($cksekolah->result())>0){
						$u = $cksekolah->row();
						$sk = $u->nama_sekolah;
					}else{
						$sk = "NotFound";
					}
					echo '<li>
					<div class="result-info">
					<h4 class="title"><a href="'.base_url().'loker/detil_lokerku/'.$row->id.'">'.$row->judul.'</a></h4>
					<p class="location" title="Kategori Lowongan | Nama Perusahaan | Sekolah Pembuat Lowongan | Tanggal Dibuat Lowongan">'.$nama_perusahaan.' | '.$sk.' |'.date("d-m-Y",strtotime($row->tgl_buat)).'</p>
					<p class="desc">
					';
					if(strlen($row->deskripsi) > 179){
						echo ''.substr($row->deskripsi, 0,179).' ... <a href="'.base_url().'loker/detil_lokerku/'.$row->id.'"><b>more</b></a>';
					}else{
						$tot = 200 - strlen($row->deskripsi);
						$hasil = $tot + strlen($row->deskripsi);
						echo $row->deskripsi;
						for ($i=0; $i < $hasil ; $i++) { 
							echo '&nbsp;';
						}
					}
					echo'</p>
					<div class="btn-row">
					<a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fa fa-fw fa-calendar" title="Tanggal Mulai : '.date("d-m-Y",strtotime($row->tgl_buka)).'"></i></a>
					<a href="javascript:;" data-toggle="tooltip" data-container="body" data-title="Analytics"><i class="fa fa-fw fa-calendar-o" title="Tanggal Selesai : '.date("d-m-Y",strtotime($row->tgl_tutup)).'"></i></a>
					<a href="javascript:;" title="Total Pelamar : '.$this->db->get_where('bidding_lulusan',array('id_loker'=>$row->id))->num_rows().'" data-toggle="tooltip" data-container="body" data-title="Users"><i class="fa fa-fw fa-user"></i></a>
					</div>
					</div>
					<div class="result-price">';
					echo            '<small>Range Gaji : '.$row->range.'</small>';
					if(date("Y-m-d") > $row->tgl_tutup){
						echo '<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
					}else{
						echo '<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
					}
					if(date("Y-m-d") > $row->tgl_tutup){
						echo  '<p><a href="'.base_url().'loker/detil_lokerku/'.$row->id.'" class="btn btn-inverse btn-xs m-r-5">View Details</a>
						</p></div>
						</li>';
					}else{
						echo  '<p><a href="'.base_url().'jadwal_test/detil_loker/'.$row->id.'" class="btn btn-inverse btn-xs m-r-5">Buat Jadwal Test</a>
						</p></div>
						</li>';	
					}
				}
			}else{
				echo '<div class="alert alert-danger fade show m-b-15">
				Maaf, Lowongan Pekerjaan Tidak Tersedia.
				</div>';
			}
			exit;
		}else{
			redirect("_404","refresh");
		}	
	}
	public function getKabkot(){
		if($this->input->is_ajax_request()){
			$kode = $_POST['depart']; 
			$kodex = $this->asn->anti($kode);
			$list_kota = array();
			$data = $this->db1->query("SELECT * FROM ref.view_kota WHERE kode_prov = '$kodex'")->result();
			foreach ($data as $key) {
				$list_kota[] = array("id" => $key->kode_kota, "kota" => $key->kota);
			}
			echo json_encode($list_kota);
		}else{
			redirect("_404","refresh");
		}
	}
	public function getKabkot_(){
		if($this->input->is_ajax_request()){
			$kode = $_POST['depart']; 
			$kodex = $this->asn->anti($kode);
			$list_kota = array();
			$data = $this->db1->query("SELECT * FROM ref.view_kota WHERE kode_prov = '$kodex'")->result();
			foreach ($data as $key) {
				$list_kota[] = array("id" => $key->kode_kota, "kota" => $key->kota);
			}
			echo json_encode($list_kota);
		}else{
			redirect("_404","refresh");
		}
	}
	public function detil_loker($kode=Null){
		$this->db1->query("refresh materialized view view_lowongan_tujuan");
		$this->db1->query("refresh materialized view view_lowongan");
		$this->db1->query("refresh materialized view view_lowongan_detil");
		// $kodex = $this->asn->decode($kode);
		$isi['option_sekolah_filter'][''] = "Pilih Asal Sekolah";
		$cksekolah = $this->db->get_where('view_bidding',array('id_loker'=>$kode))->result();
		if(count($cksekolah)>0){
			foreach ($cksekolah as $key) {
				$isi['option_sekolah_filter'][$key->sekolah_id] = $key->nama_sekolah;
			}
		}else{
			$isi['option_sekolah_filter'][''] = "Data Sekolah Tidak ditemukan";
		}
		$this->session->set_userdata('idx',$kode);
		$isi['id_loker'] = $kode;
		$ses = $this->session->userdata('role_');
		$ckdata = $this->db->query("SELECT * FROM view_lowongan_tujuan WHERE status = '1' AND id = '$kode'");
		if(count($ckdata->result())>0){
			$this->session->set_userdata('id_loker_',$kode);
			$key = $ckdata->row();
			if(date("Y-m-d") > $key->tgl_tutup){
				$isi['status'] ='<small><button title="Lowongan di Tutup" class="btn btn-danger btn-xs m-r-5">Closed</button></small>';
			}else{
				$isi['status'] ='<small><button title="Lowongan Masih di Buka" class="btn btn-primary btn-xs m-r-5">Open</button></small>';
			}
			$cksekolah = $this->db->query("SELECT b.nama as nama_sekolah FROM app.user_sekolah a JOIN ref.sekolah b ON a.sekolah_id = b.sekolah_id WHERE a.user_id = '$key->dibuat_oleh'");
			if(count($cksekolah->result())>0){
				$u = $cksekolah->row();
				$sk = $u->nama_sekolah;
			}else{
				$sk = "NotFound";
			}
			$isi['dibuat_oleh'] = $sk;
			$isi['kode_loker'] = $kode;
			$isi['judul_loker'] = $key->judul;
			$isi['fotox'] = "no.jpg";
			$isi['spesialisasi'] = "";
			$isi['tglbuka'] = date("d-m-Y",strtotime($key->tgl_buka));
			$isi['tgltutup'] = date("d-m-Y",strtotime($key->tgl_tutup));
			$isi['deskripsi'] = $key->deskripsi;
			$isi['gaji'] = $key->range;
			$isi['namamenu'] = "Input Loker";
			$isi['page'] = "bursa";
			$isi['link'] = 'jadwal_test';
			$isi['halaman'] = "Detil Lowongan Pekerjaan";
			$isi['judul'] = "Halaman Detil Lowongan Pekerjaan";
			$isi['content'] = "detil_loker";
			$isi['option_provinsi'][''] = "Pilih Provinsi";
			$isi['option_kabupaten'][''] = "Pilih Kota/Kabupaten";
			$provinsi = $this->db->get('ref.view_provinsi')->result();
			foreach ($provinsi as $row) {
				$isi['option_provinsi'][trim($row->kode_prov)] = $row->prov;
			}
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect('_404','refresh');
		}
	}
	public function detil_pelamar($id){
		$cksiswa = $this->db->get_where('view_siswa_terdaftar',array('peserta_didik_id'=>$id));
		if(count($cksiswa->result())>0){
			$isi['tombolsimpan'] = 'Simpan';
			$isi['namamenu'] = "Input Loker";
			$isi['page'] = "bursa";
			$isi['kode'] = $id;
			$isi['jml_kompetensi'] = $this->db->get_where('kompetensi_siswa',array('user_id'=>$id))->num_rows();
			$this->session->set_userdata('id_pelamar',$id);
			$isi['link'] = 'loker';
			$isi['tombolbatal'] = 'Batal';
			$isi['halaman'] = "Detil Pelamar";
			$isi['judul'] = "Halaman Detil Pelamar";
			$isi['content'] = "cv";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect("_404","refresh");
		}
	}
	public function getKompetensi(){
		if($this->input->is_ajax_request()){
			$sess = $this->session->userdata('id_pelamar');
			$page =  $_GET['page'];
			$offset = 10*$page;
			$limit = 10;
			$ckdata = $this->db->query("SELECT * FROM kompetensi_siswa WHERE user_id = '$sess' ORDER BY tgl_sertifikat DESC LIMIT $limit OFFSET $offset")->result();
			foreach ($ckdata as $row) {
				echo 	'<ul class="list-group list-group-lg no-radius list-email">
				<li class="list-group-item primary">
				<div class="email-info">
				<span class="email-time" title="Tanggal Sertifikat">
				'.date("d-m-Y",strtotime($row->tgl_sertifikat)).'
				</span>
				<h5 class="email-title" title="Nama Penyelenggara">
				'.$row->judul_kompetensi.'</br>
				<span class="label label-primary f-s-10" title="No Sertifikat">No Sertifikat : '.$row->no_sertifikat.'</span>
				<br/>
				<span class="label label-primary f-s-10" title="Kompetensi Inti">Kompetensi Inti : '.$row->kompetensi_inti.'</span>
				</h5>
				</div>
				</li>
				</ul>';
			}
			exit;
		}else{
			redirect("_404","refresh");
		}	
	}
	private function _validasi(){
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		if($this->input->post('judul') == ''){
			$data['inputerror'][] = 'judul';
			$data['error_string'][] = 'judul harus di isi.';
			$data['status'] = FALSE;
		}
		if($this->input->post('keterangan') == ''){
			$data['inputerror'][] = 'keterangan';
			$data['error_string'][] = 'keterangan jadwal tes harus di isi.';
			$data['status'] = FALSE;
		}
		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}
	public function buat_jadwal(){
		if($this->input->is_ajax_request()){
			$this->_validasi();
			$id_loker = $this->session->userdata('id_loker_');
			$judul = $this->asn->anti($this->input->post('judul'));
			$tgl_buka = date("Y-m-d",strtotime($this->input->post('tgl_buka')));
			$tgl_tutup = date("Y-m-d",strtotime($this->input->post('tgl_tutup')));
			$prov = $this->input->post('provinsi');
			$kota = $this->input->post('kabupaten');
			$alamat = $this->asn->anti($this->input->post('alamat'));
			$keterangan = $this->asn->anti($this->input->post('keterangan'));
			// $lat = $this->asn->anti($this->input->post('lintangO'));
			// $lon = $this->asn->anti($this->input->post('bujurO'));
			$alamat = $this->asn->anti($this->input->post('address'));
			$narahubung = $this->asn->anti($this->input->post('narahubung'));
			$simpanjadwal = array('id_loker'=>$id_loker,
				// 'lat'=>$lat,
				// 'lon'=>$lon,
				'alamat'=>$alamat,
				'tgl'=>date("Y-m-d"),
				'status'=>'1',
				'kode_prov'=>$prov,
				'kode_kota'=>$kota,
				'keterangan'=>$keterangan,
				'user_industri'=>$this->session->userdata('user_id'),
				'tgl_create'=>date("Y-m-d"),
				'jenis_tes'=>'3',
				'judul'=>$judul,
				'tgl_mulai'=>$tgl_buka,
				'tgl_selesai'=>$tgl_tutup,
				'narahubung'=>$narahubung);
			if($this->db->insert('jadwal_tes',$simpanjadwal)){
				$tujuan = $this->input->post('dat_siswa');
				for ($i=0; $i < count($tujuan); $i++) {
					$tujuanx = $tujuan[$i];
					$x = explode("|", $tujuanx);
					$id_biding = $x[0];
					$peserta_didik_id = $x[1];
					$ckdata = $this->db->get_where('bidding_lulusan',array('id'=>$id_biding,'id_siswa'=>$peserta_didik_id,'id_loker'=>$id_loker))->result();
					if(count($ckdata)>0){
						$updatestatus = array('status'=>'3');
						$this->db->where('id',$id_biding);
						$this->db->where('id_loker',$id_loker);
						$this->db->where('id_siswa',$peserta_didik_id);
						$this->db->update('bidding_lulusan',$updatestatus);
					}
					$simpanhistory = array('id_bidding'=>$id_biding,
						'tgl_update'=>date("Y-m-d"),
						'peserta_didik_id'=>$peserta_didik_id,
						'user_industri'=>$this->session->userdata('user_id'),
						'status'=>'3');
					$jdw = $this->db->query("SELECT id FROM jadwal_tes WHERE id_loker='$id_loker'")->row();
					$ibx = array('peserta_didik_id'=>$peserta_didik_id, 'jadwal_id'=>$jdw->id, 'read'=>'0');
					$this->db->insert('inbox',$ibx);
					$this->db->insert('bidding_history',$simpanhistory);
				}
				echo json_encode(array("status" => TRUE));
			}else{
				echo json_encode(array("status" => FALSE));
			}
		}else{
			redirect("_404","refresh");
		}
	}
	public function ubah_status($jns=Null,$id=Null){
		if($this->input->is_ajax_request()){
			if($jns=="aktif"){
				$data = array('status'=>'0');
			}else{
				$data = array('status'=>'1');
			}
			$this->db->where('id',$this->asn->anti($id));
			$this->db->update('jadwal_tes',$data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function hapus_data($id){
		if($this->input->is_ajax_request()){
			$this->db->where('id',$id);
			$this->db->delete('jadwal_tes');
			$this->db->where('jadwal_id',$id);
			$this->db->delete('inbox');
			echo json_encode(array("status" => TRUE));
		}else{
			redirect("_404","refresh");
		}
	}
	public function edit($id){
		$ckdata = $this->db->get_where('jadwal_tes',array('id'=>$id));
		if(count($ckdata->result())>0){
			$this->session->set_userdata('idna',$id);
			$row = $ckdata->row();
			$isi['judul'] = "Halaman Edit Jadwal Tes";
			$isi['judul_loker'] = $row->judul;
			$isi['default']['tgl_buka'] = date('d-m-Y',strtotime($row->tgl_mulai));
			$isi['default']['tgl_tutup'] = date('d-m-Y',strtotime($row->tgl_selesai));
			$isi['default']['lintangO'] = $row->lat;
			$isi['default']['bujurO'] = $row->lon;
			$isi['keterangan'] = $row->keterangan;
			$ckwil = $this->db->get_where('ref.view_provinsi',array('kode_prov'=>$row->kode_prov));
			$x = $ckwil->row();
			$isi['option_provinsi'][$row->kode_prov] = $x->prov;
			$ckkota= $this->db->get_where('ref.view_kota',array('kode_kota'=>$row->kode_kota));
			$xx = $ckkota->row();
			$kode_prov = $row->kode_prov;
			$isi['option_kabupaten'][$row->kode_kota] = $xx->kota;
			$isi['alamat'] = $row->alamat;
			$ckpropinsi = $this->db1->query("SELECT * FROM ref.view_provinsi")->result();
			if(count($ckpropinsi)>0){
				foreach ($ckpropinsi as $row) {
					$isi['option_provinsi'][trim($row->kode_prov)] = $row->prov;
				}
			}else{
				$isi['option_provinsi'][''] = "Data Propinsi Belum Tersedia";
			}
			$ckkota = $this->db->get_where('ref.view_kota',array('kode_prov'=>$kode_prov))->result();
			if(count($ckkota)>0){
				foreach ($ckkota as $xxx) {
					$isi['option_kabupaten'][$xxx->kode_kota] = $xxx->kota;
				}
			}else{
				$isi['option_kabupaten'][""] = "Data Kabupaten / Kota Tidak Tersedia";				
			}
			$isi['tombolsimpan'] = 'Simpan';
			$isi['namamenu'] = "Input Loker";
			$isi['page'] = "bursa";
			$isi['kode'] = $id;
			$isi['link'] = 'loker';
			$isi['tombolbatal'] = 'Batal';
			$isi['halaman'] = "Edit Jadwal Tes";
			$isi['content'] = "_form_edit";
			$this->load->view("dashboard/dashboard_view",$isi);
		}else{
			redirect("_404","refresh");
		}
	}
	public function proses_edit(){
		$this->form_validation->set_rules('judul_loker', 'judul tes', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_rules('tgl_buka', 'tanggal mulai', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_rules('tgl_tutup', 'tanggal tutup', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_rules('provinsi', 'provinsi', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_rules('kabupaten', 'kabupaten', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_rules('address', 'alamat', 'htmlspecialchars|trim|required|min_length[1]');
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		$this->form_validation->set_message('min_length', '%s minimal %s karakter');
		$this->form_validation->set_message('max_length', '%s maximal %s karakter');
		if ($this->form_validation->run() == TRUE){
			$judul = $this->asn->anti($this->input->post('judul_loker'));
			$tgl_buka = date("Y-m-d",strtotime($this->input->post('tgl_buka')));
			$tgl_tutup = date("Y-m-d",strtotime($this->input->post('tgl_tutup')));
			$prov = $this->input->post('provinsi');
			$kota = $this->input->post('kabupaten');
			$alamat = $this->asn->anti($this->input->post('alamat'));
			$lat = $this->asn->anti($this->input->post('lintangO'));
			$lon = $this->asn->anti($this->input->post('bujurO'));
			$alamat = $this->asn->anti($this->input->post('alamatbaru'));
			$simpanjadwal = array('lat'=>$lat,
				'lon'=>$lon,
				'alamat'=>$alamat,
				'tgl'=>date("Y-m-d"),
				// 'status'=>'1',
				'kode_prov'=>$prov,
				'kode_kota'=>$kota,
				'user_industri'=>$this->session->userdata('user_id'),
				'tgl_create'=>date("Y-m-d"),
				// 'jenis_tes'=>'3',
				'judul'=>$judul,
				'tgl_mulai'=>$tgl_buka,
				'tgl_selesai'=>$tgl_tutup);
			$this->db->where('id',$this->session->userdata('idna'));
			$this->db->update('jadwal_tes',$simpanjadwal);
			redirect('jadwal_test','refresh');
		}else{
			$this->edit($this->session->userdata('idna'));
		}
	}
}
