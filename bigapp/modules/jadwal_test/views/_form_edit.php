<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/parsley/dist/parsley.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnyR-SfN_ycHfdpi6oEQF-VqkhLYjqwQ4"></script> -->
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/detil_loker_jadwal.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <form name="form" id="form" action="<?php echo base_url();?>jadwal_test/proses_edit" class="form-horizontal form-bordered" method="post">
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Judul Tes * :</label>
                    <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="text" id="judul_loker" minlength="1" name="judul_loker" value="<?php echo $judul_loker;?>" data-type="judul_loker" data-parsley-required="true" data-parsley-minlength="1" />
                        <span style="color:red;"><?php echo form_error('judul_loker');?></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Keterangan * :</label>
                    <div class="col-md-5 col-sm-5">
                        <textarea name="keterangan" placeholder="masukan keterangan" class="form-control input-sm"> <?php echo $keterangan;?></textarea>
                        <span style="color:red;"><?php echo form_error('keterangan');?></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Tanggal Mulai Tes * :</label>
                    <div class="col-md-2 col-sm-2"> 
                        <div class="input-group date" id="tgl_buka" data-date-format="dd-mm-yyyy">
                            <input type="text" class="form-control" name="tgl_buka" value="<?php echo set_value('tgl_buka',isset($default['tgl_buka']) ? $default['tgl_buka'] : ''); ?>" data-type="tgl_buka" data-parsley-required="true"/>
                            <span class="input-group-addon m-l-5"><i class="fas fa-calendar-alt"></i></span>
                            <span style="color:red;"><?php echo form_error('tgl_buka');?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Tanggal Selesai * :</label>
                    <div class="col-md-2 col-sm-2"> 
                        <div class="input-group date" id="tgl_tutup" data-date-format="dd-mm-yyyy">
                            <input type="text" class="form-control" name="tgl_tutup" value="<?php echo set_value('tgl_tutup',isset($default['tgl_tutup']) ? $default['tgl_tutup'] : ''); ?>" data-type="tgl_tutup" data-parsley-required="true"/>
                            <span class="input-group-addon m-l-5"><i class="fas fa-calendar-alt"></i></span>
                            <span style="color:red;"><?php echo form_error('tgl_tutup');?></span>
                        </div>
                    </div>
                </div>
                <legend><h5 class="m-t-10 m-l-25"><b>* Lokasi Penempatan Tes</b></h5></legend>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3">Provinsi</label>
                    <div class="col-md-3">
                        <?php echo form_dropdown('provinsi',$option_provinsi,isset($default['provinsi']) ? $default['provinsi'] : '','id="provinsi" name="provinsi" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Kabupaten / Kota * :</label>
                        <div class="col-md-3">
                        <?php echo form_dropdown('kabupaten',$option_kabupaten,isset($default['kabupaten']) ? $default['kabupaten'] : '','class="default-select2 form-control" id="kabupaten" name="kabupaten" data-live-search="true" data-style="btn-white"');?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3">Alamat Jadwal Tes * :</label>
                    <div class="col-md-5 col-sm-5">
                        <input type="text" name="address" id="address" value="<?php echo $alamat;?>" class="form-control"  />
                    </div>
                </div>
                <!-- <div id="carialamat">
                    <div class="form-group row">
                        <label class="text-right control-label col-md-3 col-sm-3"></label>
                        <div class="col-md-5 col-sm-5">
                            <input type="button" class="btn btn-primary btn-xs m-r-5" value="Cari Lokasi" onClick="codeAlamat()"><br>
                        </div>
                    </div>
                </div> -->
<!--                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div id = "map_canvas"></div>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <!-- <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="lintangO" value="<?php echo set_value('lintangO',isset($default['lintangO']) ? $default['lintangO'] : ''); ?>" id="lintangO" class="form-control"  data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('lintangO');?></small></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="bujurO" id="bujurO" value="<?php echo set_value('bujurO',isset($default['bujurO']) ? $default['bujurO'] : ''); ?>" class="form-control" data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('bujurO');?></small></span>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="elevasi" id="elevasi" title="klik di area yang dicari untuk mendapatkan ketinggian tempat (di atas permukaan laut)" class="form-control"  />
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="alamatbaru" value="<?php echo set_value('alamatbaru',isset($default['alamatbaru']) ? $default['alamatbaru'] : ''); ?>" id="alamatbaru" class="form-control"  data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('alamatbaru');?></small></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="text-right control-label col-md-3 col-sm-3"></label>
                    <div class="col-md-3 col-sm-3">
                        <button type="submit" class="btn btn-success btn-sm">EDIT JADWAL</button>
                        <button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">BATAL</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
    initialize();
    var geocoder;
    var map;
    var elevator;
    var infowindow = new google.maps.InfoWindow();
    jQuery("#carialamat").hide();
    jQuery("#carilatlong").hide();
    function sendData(){
        var lintang = jQuery("#lintangO").val();
        var bujur = jQuery("#bujurO").val();
        var alamatbaru = jQuery("#alamatbaru").val();
        jQuery.ajax({
            url : $BASE_URL+'list_dokter/update_lokasi',
            data : {lintang:lintang,bujur:bujur,alamatbaru:alamatbaru},
            type : 'POST',
            dataType: 'json',
            success:function(data){
                if(data.response=='true'){
                    window.location = self.location;
                }else{
                    $.gritter.add({title:"Informasi !",text: " Data Gagal Di Update !"});return false;
                }
            }
        });
    }
    function initialize() {
        var lat = jQuery("#lintangO").val();
        var longitude = jQuery("#bujurO").val();
        geocoder = new google.maps.Geocoder();
        var LatLng = new google.maps.LatLng(lat, longitude);
        var myOptions = {
            zoom: 15,
            center: LatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        marker = new google.maps.Marker({
            position: LatLng,
            title: 'Lokasi USaha',
            map: map,
            draggable: true
        });
        updateMarkerPosition(LatLng);
        geocodePosition(LatLng);
        aksi();
        elevator = new google.maps.ElevationService();
        google.maps.event.addListener(map, 'click', getElevation);
    }
    function aksi(){
        google.maps.event.addListener(marker, 'dragstart', function(){
            updateMarkerAddress('Bergeser...');
        });
        google.maps.event.addListener(marker, 'drag', function(){
            updateMarkerPosition(marker.getPosition());
        });
        google.maps.event.addListener(marker, 'dragend', function(){
            geocodePosition(marker.getPosition());
        });
    }
    function getElevation(event){
        var locations = [];
        var clickedLocation = event.latLng;
        locations.push(clickedLocation);
        var positionalRequest = {
            'locations': locations
        }
        elevator.getElevationForLocations(positionalRequest, function(results, status){
            if(status == google.maps.ElevationStatus.OK){
                if(results[0]){
                    hasil = results[0].elevation;
                    hasil2 = Math.floor((hasil - Math.floor(hasil)) * 100);
                    if(hasil2 < 50){
                        hasil = Math.floor(hasil);
                    }else{
                        hasil = Math.floor(hasil) + 1;
                    }
                    infowindow.setContent('Ketinggian lokasi ini sekitar<br>' + hasil + ' meter  di atas permukaan laut.');
                    infowindow.setPosition(clickedLocation);
                    infowindow.open(map);
                    document.getElementById("elevasi").value = results[0].elevation;
                }else{
                    alert('Hasil tidak ditemukan');
                }
            }else{
                alert('Layanan elevasi gagal, karena: ' + status);
            }
        });
    }
    function codeAlamat(){
        var address = document.getElementById("address").value;
        geocoder.geocode({ 'address': address}, function(results, status){
            if(status == google.maps.GeocoderStatus.OK){
                map.setCenter(results[0].geometry.location);
                if(marker != undefined)
                    marker.setPosition(results[0].geometry.location);
                else
                    marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        position: results[0].geometry.location
                    });
                updateMarkerPosition(results[0].geometry.location);
                geocodePosition(results[0].geometry.location);
                aksi();
            }else{
                alert("Geocode tidak berhasil dengan alasan karena: " + status);
            }
        });
    }
    function GotoLatLng(){
        var lat = document.getElementById("lintang").value;
        var lang = document.getElementById("bujur").value;
        LatLng = new google.maps.LatLng(lat,lang);
        map.setCenter(LatLng);
        if(marker != undefined)
              marker.setPosition(LatLng);
         else
              marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                position: LatLng,
              });
        updateMarkerPosition(LatLng);
        geocodePosition(LatLng);
        aksi();
    }
    function geocodePosition(pos){
        geocoder.geocode({latLng: pos},
        function(responses){
            if(responses && responses.length > 0){
                updateMarkerAddress(responses[0].formatted_address);
            }else{
                updateMarkerAddress('Maaf, sistem tidak bisa mengenali alamat di lokasi ini.');
            }
        });
    }
    function updateMarkerStatus(str){
        document.getElementById('markerStatus').innerHTML = str;
    }
    function updateMarkerPosition(LatLng){
        document.getElementById("lintangO").value = [ LatLng.lat('') ];
        document.getElementById("bujurO").value = [ LatLng.lng('') ];
    }
    function updateMarkerAddress(str){
        document.getElementById("alamatbaru").value = str;
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style type="text/css">
    #map_canvas{
        margin: 0;
        width: 100%;
        height: 600px;
    }
</style> -->