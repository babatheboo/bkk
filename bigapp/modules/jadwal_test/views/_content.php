<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/plugins/pace/pace.min.js"></script> -->
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/loker_list_jadwal.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    App.init();
    FormPlugins.init();
});
</script>
<div class="row">
    <div class="col-md-12">
        <div class="result-container">
            <div class="input-group m-b-20">
                <input type="text" class="form-control input-white" name="cari" id="cari" placeholder="Filter Pencarian" />
                <div class="input-group-btn">
                    <button type="button" onclick="cari_loker()" class="btn btn-inverse"><i class="fa fa-search"></i> Pencarian</button>
                    <button type="button" class="btn btn-inverse dropdown-toggle" title="Filter Pencarian" onclick="filter_search()" data-toggle="dropdown">
                        <i class="fa fa-cog"></i>
                    </button>
                </div>
            </div>
            <div id="filter-kerja">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse" data-sortable-id="form-validation-2">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" title="Sembunyikan" class="btn btn-xs btn-icon btn-circle btn-warning" onclick="hide_()"><i class="fa fa-minus"></i></a>
                                </div>
                                <h4 class="panel-title"><?php echo $halaman;?></h4>
                            </div>
                            <div class="panel-body panel-form">
                                <form class="form-horizontal form-bordered" action="javascript;" method="post" data-parsley-validate="true">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">Industri :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <?php echo form_dropdown('industri',$option_industri,isset($default['industri']) ? $default['industri'] : '','class="default-select2 form-control" id="industri" data-size="10" style="width:100%" data-parsley-required="true" data-live-search="true" data-style="btn-white"');?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">Kategori Lowongan :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <?php echo form_dropdown('kat_loker',$option_kat_loker,isset($default['kat_loker']) ? $default['kat_loker'] : '','class="default-select2 form-control" id="kat_loker" data-size="10" style="width:100%" data-parsley-required="true" data-live-search="true" data-style="btn-white"');?>
                                            <span style="color:red;"><?php echo form_error('kat_loker');?></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="result-list">
                <div class="loader"></div>
            </ul>
        </div>
        <br/>
        <?php
        if($tot_loker>10){
            ?>
                <button id="reload_loker" data-val = "0" style="text-align: center;width: 100%" class="btn btn-sm btn-info"><i class="fa fa-refresh"></i> Lainnya <img style="display: none" id="loader" src="<?php echo base_url();?>assets/img/loader.gif"></button>
            <?php
        }
        ?>
    </div>
</div>
