<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/parsley/dist/parsley.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnyR-SfN_ycHfdpi6oEQF-VqkhLYjqwQ4"></script> -->
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/detil_loker_jadwal.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="ui-widget-7">
            <form name="form" id="form" action="javascript:void();" class="form-horizontal form-bordered" method="post">
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Judul Tes * :</label>
                    <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="text" id="judul" minlength="1" name="judul" value="" data-type="judul" data-parsley-required="true" data-parsley-minlength="1" />
                        <span style="color:red;"><?php echo form_error('judul');?></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Keterangan * :</label>
                    <div class="col-md-5 col-sm-5">
                        <textarea name="keterangan" placeholder="masukan keterangan" class="form-control input-sm"> </textarea>
                        <span style="color:red;"><?php echo form_error('keterangan');?></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Tanggal Mulai Tes * :</label>
                    <div class="col-md-2 col-sm-2"> 
                        <div class="input-group date" id="tgl_buka" data-date-format="dd-mm-yyyy">
                            <input type="text" class="form-control" name="tgl_buka" value="<?php echo set_value('tgl_buka',isset($default['tgl_buka']) ? $default['tgl_buka'] : ''); ?>" data-type="tgl_buka" data-parsley-required="true"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <span style="color:red;"><?php echo form_error('tgl_buka');?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Tanggal Selesai * :</label>
                    <div class="col-md-2 col-sm-2"> 
                        <div class="input-group date" id="tgl_tutup" data-date-format="dd-mm-yyyy">
                            <input type="text" class="form-control" name="tgl_tutup" value="<?php echo set_value('tgl_tutup',isset($default['tgl_tutup']) ? $default['tgl_tutup'] : ''); ?>" data-type="tgl_tutup" data-parsley-required="true"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <span style="color:red;"><?php echo form_error('tgl_tutup');?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Narahubung * :</label>
                    <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="text" id="narahubung" minlength="1" name="narahubung" value="" data-type="narahubung" data-parsley-required="true" data-parsley-minlength="1" />
                        <span style="color:red;"><?php echo form_error('narahubung');?></span>
                    </div>
                </div>
                <legend><h5>&nbsp;&nbsp;&nbsp;<b>* Data Pelamar Kerja</b></h5></legend>
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                       <div class="panel-heading-btn"> <button class="btn btn-danger btn-xs m-r-5" onclick="filter_data()"><i class="fa fa-search"></i> Filter Pencarian</button>&nbsp;<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()"><i class="fas fa-sync-alt"></i> Reload Data</button> </div> 
                    </div>
                    <h4 class="panel-title"><b>Informasi Data Pelamar : <?php echo number_format($this->db->query("SELECT * FROM bidding_lulusan WHERE id_loker = '$kode_loker'")->num_rows()) . " Orang";?></b></h4>

                    <div id="filter_pencarian">
                        <br/>
                            <?php echo form_dropdown('sekolah_filter',$option_sekolah_filter,isset($default['sekolah_filter']) ? $default['sekolah_filter'] : '','class="default-select2 form-control" style="width:25%" id="sekolah_filter" data-live-search="true" data-style="btn-white"');?>
                    </div> 
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-pelamar" class="table table-striped table-bordered nowrap" width="100%">
                            <thead>
                                <tr>
                                    <th width="1%" style="text-align:center"><input type="checkbox" name="select_all" value="1" style="text-align:center" id="select-all"></th>
                                    <th style="text-align:center" width="15%">Foto Siswa</th>
                                    <th style="text-align:center" width="10%">NISN</th>
                                    <th style="text-align:center" width="40%">Nama Siswa</th>
                                    <th style="text-align:center" width="30%">Asal Sekolah</th>
                                    <th style="text-align:center" width="9%">Umur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <legend><h5>&nbsp;&nbsp;&nbsp;<b>* Lokasi Penempatan Tes</b></h5></legend>
                <div class="form-group row">
                    <label class="control-label col-md-3 text-right">Provinsi :</label>
                    <div class="col-md-3">
                        <?php echo form_dropdown('provinsi',$option_provinsi,isset($default['provinsi']) ? $default['provinsi'] : '','id="provinsi" name="provinsi" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Kabupaten / Kota * :</label>
                        <div class="col-md-3">
                        <?php echo form_dropdown('kabupaten',$option_kabupaten,isset($default['kabupaten']) ? $default['kabupaten'] : '','class="default-select2 form-control" id="kabupaten" name="kabupaten" data-live-search="true" data-style="btn-white"');?>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 text-right">Alamat Jadwal Tes * :</label>
                    <div class="col-md-5 col-sm-5">
                        <input type="text" name="address" id="address" class="form-control"  />
                    </div>
                </div>
                <div id="tombol_">
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right"></label>
                        <div class="col-md-3 col-sm-3">
                            <button type="button" onclick="simpan_jadwal('<?php echo $link;?>');" class="btn btn-success btn-sm">BUAT JADWAL</button>
                            <button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">BATAL</button>
                        </div>
                    </div>
                </div>
                <!-- <div id="carialamat">
                    <div class="form-group row">
                        <label class="control-label col-md-3 col-sm-3 text-right"></label>
                        <div class="col-md-5 col-sm-5">
                            <input type="button" class="btn btn-primary btn-xs m-r-5" value="Cari Lokasi" onClick="codeAlamat()"><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div id = "map_canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="lintangO" value="<?php echo set_value('lintangO',isset($default['lintangO']) ? $default['lintangO'] : ''); ?>" id="lintangO" class="form-control"  data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('lintangO');?></small></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="bujurO" id="bujurO" value="<?php echo set_value('bujurO',isset($default['bujurO']) ? $default['bujurO'] : ''); ?>" class="form-control" data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('bujurO');?></small></span>
                        </div>
                    </div>
                    <?php
                    if($this->session->userdata('level')=='1'){
                        ?>
                        <input type="hidden" id="sakolana" name="sakolana" value="<?php echo $this->session->userdata('sekolah_sess');?>">
                        <?php
                    }
                    ?>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="elevasi" id="elevasi" title="klik di area yang dicari untuk mendapatkan ketinggian tempat (di atas permukaan laut)" class="form-control"  />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <input type="hidden" name="alamatbaru" value="<?php echo set_value('alamatbaru',isset($default['alamatbaru']) ? $default['alamatbaru'] : ''); ?>" id="alamatbaru" class="form-control"  data-parsley-required="true" data-parsley-minlength="1"/>
                            <span style="color:red;"><small><?php echo form_error('alamatbaru');?></small></span>
                        </div>
                    </div>
                </div> -->
            </form>
        </div>
    </div>
</div>
<!-- <script type="text/javascript">
initialize();
var geocoder;
var map;
var elevator;
var infowindow = new google.maps.InfoWindow();
jQuery("#carialamat").hide();
jQuery("#carilatlong").hide();
function sendData(){
    var lintang = jQuery("#lintangO").val();
    var bujur = jQuery("#bujurO").val();
    var alamatbaru = jQuery("#alamatbaru").val();
    jQuery.ajax({
        url : $BASE_URL+'perusahaan/update_lokasi',
        data : {lintang:lintang,bujur:bujur,alamatbaru:alamatbaru},
        type : 'POST',
        dataType: 'json',
        success:function(data){
            if(data.response=='true'){
                window.location = self.location;
            }else{
                $.gritter.add({title:"Informasi !",text: " Data Gagal Di Update !"});return false;
            }
        }
    });
}
function initialize() {
    geocoder = new google.maps.Geocoder();
    var LatLng = new google.maps.LatLng(-7.3246168188575655, 108.21276447721561);
    var myOptions = {
        zoom: 15,
        center: LatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
    marker = new google.maps.Marker({
        position: LatLng,
        title: 'Lokasi Perusahaan',
        map: map,
        draggable: true
    });
    updateMarkerPosition(LatLng);
    geocodePosition(LatLng);
    aksi();
    elevator = new google.maps.ElevationService();
    google.maps.event.addListener(map, 'click', getElevation);
}
function aksi(){
    google.maps.event.addListener(marker, 'dragstart', function(){
        updateMarkerAddress('Bergeser...');
    });
    google.maps.event.addListener(marker, 'drag', function(){
        updateMarkerPosition(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'dragend', function(){
        geocodePosition(marker.getPosition());
    });
}
function getElevation(event){
    var locations = [];
    var clickedLocation = event.latLng;
    locations.push(clickedLocation);
    var positionalRequest = {
        'locations': locations
    }
    elevator.getElevationForLocations(positionalRequest, function(results, status){
        if(status == google.maps.ElevationStatus.OK){
            if(results[0]){
                hasil = results[0].elevation;
                hasil2 = Math.floor((hasil - Math.floor(hasil)) * 100);
                if(hasil2 < 50){
                    hasil = Math.floor(hasil);
                }else{
                    hasil = Math.floor(hasil) + 1;
                }
                infowindow.setContent('Ketinggian lokasi ini sekitar<br>' + hasil + ' meter  di atas permukaan laut.');
                infowindow.setPosition(clickedLocation);
                infowindow.open(map);
                document.getElementById("elevasi").value = results[0].elevation;
            }else{
                alert('Hasil tidak ditemukan');
            }
        }else{
            alert('Layanan elevasi gagal, karena: ' + status);
        }
    });
}
function codeAlamat(){
    var address = document.getElementById("address").value;
    var propinsi = jQuery("#propinsi option:selected").text();
    var kabupaten = jQuery("#kabupaten option:selected").text();
    geocoder.geocode({ 'address': address + " " + propinsi + " " +kabupaten}, function(results, status){
        if(status == google.maps.GeocoderStatus.OK){
            map.setCenter(results[0].geometry.location);
            if(marker != undefined)
                marker.setPosition(results[0].geometry.location);
            else
                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: results[0].geometry.location
                });
            updateMarkerPosition(results[0].geometry.location);
            geocodePosition(results[0].geometry.location);
            aksi();
        }else{
            alert("Geocode tidak berhasil dengan alasan karena: " + status);
        }
    });
}
function GotoLatLng(){
    var lat = document.getElementById("lintang").value;
    var lang = document.getElementById("bujur").value;
    LatLng = new google.maps.LatLng(lat,lang);
    map.setCenter(LatLng);
    if(marker != undefined)
          marker.setPosition(LatLng);
     else
          marker = new google.maps.Marker({
                map: map,
                draggable: true,
            position: LatLng,
          });
    updateMarkerPosition(LatLng);
    geocodePosition(LatLng);
    aksi();
}
function geocodePosition(pos){
    geocoder.geocode({latLng: pos},
    function(responses){
        if(responses && responses.length > 0){
            updateMarkerAddress(responses[0].formatted_address);
        }else{
            updateMarkerAddress('Maaf, sistem tidak bisa mengenali alamat di lokasi ini.');
        }
    });
}
function updateMarkerStatus(str){
    document.getElementById('markerStatus').innerHTML = str;
}
function updateMarkerPosition(LatLng){
    document.getElementById("lintangO").value = [ LatLng.lat('') ];
    document.getElementById("bujurO").value = [ LatLng.lng('') ];
}
function updateMarkerAddress(str){
    document.getElementById("alamatbaru").value = str;
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<style type="text/css">
    #map_canvas{
        margin: 0;
        width: 100%;
        height: 600px;
    }
</style> -->