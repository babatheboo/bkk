<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-plugins.min.js"></script>
<script>
	$(document).ready(function() {
		FormPlugins.init();
	});
</script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				</div>
				<h4 class="panel-title"><?php echo $halaman;?></h4>
			</div>
			<div class="panel-body panel-form">
				<form class="form-horizontal form-bordered" action="<?php echo base_url();?>reset/eksekusi" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Level * :</label>
						<div class="col-md-3 col-sm-3">
							<select id="level" name="level" class="form-control selectpicker" data-size="10" data-parsley-required="true" data-live-search="true" data-style="btn-white">
								<option value="" />PILIH LEVEL
								<option value="1" />Admin BKK
								<option value="2" />Alumni
								<option value="3" />Industri
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Password * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="pass" minlength="1" name="pass" value="<?php echo set_value('pass',isset($default['pass']) ? $default['pass'] : ''); ?>" data-type="pass" data-parsley-required="true" data-parsley-minlength="1" data-parsley-minlength="1"/>
							<span style="color:red;"><?php echo form_error('pass');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3"></label>
						<div class="col-md-3 col-sm-3">
							<button type="submit" class="btn btn-success btn-sm">RESET</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				</div>
				<h4 class="panel-title">Manajemen Reset Password Per-Username</h4>
			</div>
			<div class="panel-body panel-form">
				<form class="form-horizontal form-bordered" action="<?php echo base_url();?>reset/eksekusix" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Username * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="username" minlength="1" name="username" value="<?php echo set_value('username',isset($default['username']) ? $default['username'] : ''); ?>" data-type="username" data-parsley-required="true" data-parsley-minlength="1" data-parsley-minlength="1"/>
							<span style="color:red;"><?php echo form_error('username');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Password * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="pass" minlength="1" name="pass" value="<?php echo set_value('pass',isset($default['pass']) ? $default['pass'] : ''); ?>" data-type="pass" data-parsley-required="true" data-parsley-minlength="1" data-parsley-minlength="1"/>
							<span style="color:red;"><?php echo form_error('pass');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3"></label>
						<div class="col-md-3 col-sm-3">
							<button type="submit" class="btn btn-success btn-sm">RESET</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="alert alert-danger fade in m-b-15">
	<strong>Informasi!</strong>
	Untuk orang-orang lemah yang suka lupa passwordnya<br/>
	1. Reset Password Per-Level<br/>
	2. Reset Password Per-Username<br/>
	Jadi jika ada orang lemah yang lupa sama passwordnya (biasanya admin BKK), fasilitas ini digunakan untuk mereset password Per-Username tinggal masukan username (NPSN Sekolah) pada Form Ke-2 Kemudian masukan Password Baru<br/>
	dan untuk para developer lemah yang lupa memasukan query (Tidak Menggunakan Wer TaKewerKewer) silahkan menggunakan Form Ke-1 (Reset Per-Level) Hahahaha
</div>