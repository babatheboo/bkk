<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reset extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('0');
		$this->db1 = $this->load->database('default', TRUE);
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['link'] = 'reset';
		$isi['halaman'] = "Manajemen Reset Password";
		$isi['page'] = "reset(array)";
		$isi['judul'] = "Halaman Reset Password";
		$isi['content'] = "reset_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function eksekusi(){
		$lv = $this->input->post('level');
		$pass = md5(md5($this->input->post('pass')));
		if($lv!="" && $pass!=""){
			$reset = array('password'=>$pass);
			$this->db->where('level',$lv);
			$this->db->update('app.username',$reset);
			redirect("reset",'refresh');
		}else{
			?>
			<script type="text/javascript">
				alert("Level dan Password tidak boleh kosong !!! ");
				window.location = '<?php echo base_url();?>reset';
			</script>
			<?php
		}
	}
	public function eksekusix(){
		$lv = $this->input->post('username');
		$pass = md5(md5($this->input->post('pass')));
		if($lv!="" && $pass!=""){
			$reset = array('password'=>$pass);
			$this->db->where('username',$lv);
			$this->db->update('app.username',$reset);
			redirect("reset",'refresh');
		}else{
			?>
			<script type="text/javascript">
				alert("Username dan Password tidak boleh kosong !!! ");
				window.location = '<?php echo base_url();?>reset';
			</script>
			<?php
		}
	}
}

