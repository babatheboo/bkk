<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_bkk extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('0');
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('u_bkka');
  		$this->load->library('bcrypt');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['link']    = 'user_bkk';
		$isi['halaman'] = "Manajemen User";
		$isi['page']    = "users";
		$isi['judul']   = "Halaman user BKK";
		$isi['content'] = "users_bkk_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->u_bkka->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row    = array();
				$row[]  = $no . ".";
				$row[]  = $rowx->nip;
				$row[]  = $rowx->nama;
				$row[]  = $rowx->nskolah;
				$row[]  = $rowx->email;
				$row[]  = $rowx->tlp;
				$row[]  = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'user_bkk\',\'edit_data\','."'".$rowx->id."'".')"><i class="icon-pencil"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data User\',\'user_bkk\',\'hapus_data\','."'".$rowx->id."'".')"><i class="icon-remove icon-white"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->u_bkka->count_all(),
				"recordsFiltered" => $this->u_bkka->count_filtered(),
				"data"            => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}
	public function proses_add(){
		if($this->input->is_ajax_request()){
			$method      = "save";
			$this->_validasi($method);
			$nip         = $this->input->post('nip');
			$tlp         = $this->input->post('tlp');
			$nama        = $this->input->post('nama');
			$email       = $this->input->post('mail');
			$sess_skolah = $this->session->userdata('sekolah_sess');
			$data        = array('sekolah_id' => $sess_skolah,
				'nip'        =>$nip,
				'nama'       =>htmlspecialchars($nama),
				'email'      =>$email,
				'tlp'        =>$tlp,
			);
			$insert  = $this->u_bkka->simpan($data);
			$q       = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
			$qry     = $this->db1->query($q);
			$key     = $qry->row();
			$user_id = $key->user_id;
			$pass    = $this->bcrypt->hash_password($nip);
			$dd      = array(
				'user_id'  => $user_id ,
				'username' => $nip,
				'password' => $pass,
				'level'    => '1',
				'login'    => '1' 
			);
			$this->db1->insert('app.username',$dd);
	        echo json_encode(array("status" => TRUE));
        }else{
	    	redirect("_404","refresh");
	    }
    }
    private function _validasi($method){
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		$nip                  = $this->input->post('nip');
        if($method=="save"){
			$sess_skolah = $this->session->userdata('sekolah_sess');
			$ckdata      = $this->db->query("SELECT * FROM app.user_sekolah WHERE sekolah_id = '$sess_skolah'")->result();
        	if(count($ckdata)>=4){
				$data['inputerror'][]   = 'nip';
				$data['error_string'][] = 'akses user sudah batas minimum.';
				$data['status']         = FALSE;
        	}else{
        		$ckdata = $this->db->query("SELECT * FROM app.user_sekolah WHERE nip = '$nip'")->result();
	        	if(count($ckdata)>0){
					$data['inputerror'][]   = 'nip';
					$data['error_string'][] = 'nip sudah terdaftar sebelumnya.';
					$data['status']         = FALSE;
		        }
        	}
    	}
    	if($this->input->post('nama') == ''){
			$data['inputerror'][]   = 'nama';
			$data['error_string'][] = 'nama harus di isi.';
			$data['status']         = FALSE;
        }
        if($this->input->post('mail') == ''){
			$data['inputerror'][]   = 'mail';
			$data['error_string'][] = 'email harus di isi.';
			$data['status']         = FALSE;
        }else{
        	if (!filter_var($this->input->post('mail'), FILTER_VALIDATE_EMAIL)) {
				$data['inputerror'][]   = 'mail';
				$data['error_string'][] = 'penulisan email tidak valid.';
				$data['status']         = FALSE;
			}
        }
        if($this->input->post('tlp') == ''){
			$data['inputerror'][]   = 'tlp';
			$data['error_string'][] = 'no tlp harus di isi.';
			$data['status']         = FALSE;
        }else{
        	if (!is_numeric($this->input->post('tlp'))){
				$data['inputerror'][]   = 'tlp';
				$data['error_string'][] = 'no tlp hanya karakter angka.';
				$data['status']         = FALSE;
		    }
        }
        if($nip == ''){
			$data['inputerror'][]   = 'nip';
			$data['error_string'][] = 'nip harus di isi.';
			$data['status']         = FALSE;
        }else{
        	if (!is_numeric($this->input->post('nip'))){
				$data['inputerror'][]   = 'nip';
				$data['error_string'][] = 'nip hanya karakter angka.';
				$data['status']         = FALSE;
		    }
        }
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }
	public function proses_edit(){
		if($this->input->is_ajax_request()){
			$method      = "edit";
			$this->_validasi($method);
			$nip         = $this->input->post('nip');
			$tlp         = $this->input->post('tlp');
			$nama        = $this->input->post('nama');
			$email       = $this->input->post('mail');
			$sess_skolah = $this->session->userdata('sekolah_sess');
			$data        = array(
				'sekolah_id' =>$sess_skolah,
				'nip'        =>$nip,
				'nama'       =>htmlspecialchars($nama),
				'email'      =>$email,
				'tlp'        =>$tlp,
	        );
	        $this->u_bkka->update(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
	public function hapus_data($id){
		if($this->input->is_ajax_request()){
			$ckuserid = $this->db->query("SELECT user_id FROM app.user_sekolah WHERE id = '$id'");
			$row      = $ckuserid->row();
			$this->u_bkka->hapus_user_id($row->user_id);
			$this->u_bkka->hapus_by_id($id);
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
    public function edit_data($id){
		if($this->input->is_ajax_request()){
			$data = $this->u_bkka->get_by_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}
}
