<link href="<?php echo base_url();?>assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/parsley/dist/parsley.js"></script>
<?php
if($cek!="edit"){
	?>
	<script type="text/javascript">
	$(document).ready(function() {
		jQuery("#sekolah").change(function(){
	        var sekolah = jQuery("#sekolah").val();
	        if(sekolah!=""){
	            jQuery.blockUI({
	                css: { 
	                    border: 'none', 
	                    padding: '15px', 
	                    backgroundColor: '#000', 
	                    '-webkit-border-radius': '10px', 
	                    '-moz-border-radius': '10px', 
	                    opacity: 0.5, 
	                    color: '#fff' 
	                },
	                message : 'Sedang Melakukan Pengecekan Data, Mohon menunggu ... '
	            });
	            setTimeout(function(){
		            $.ajax({
		                url : $BASE_URL+'u_sekolah/cek_user/'+sekolah,
		                dataType : 'json',
		                type : 'post',
		                success : function(json) {
		                    $.unblockUI();
		                    if (json.say == "ok") {
					            $.gritter.add({title:"Informasi !",text: " Silahkan Masukan Data dengan Benar !"});
		                    }else{
		                        $.gritter.add({title:"Informasi !",text: " Sekolah Ini Sudah Memiliki User Akun !"});
						        jQuery("#sekolah").select2("val", "");
		                    }
		                }
		            });             
	            },500);
	        }
	    });
	});
	</script>
	<?php
}
?>
<div class="row">
    <div class="col-md-12">
		<div class="panel panel-inverse" data-sortable-id="form-validation-2">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        </div>
		        <h4 class="panel-title"><?php echo $halaman;?></h4>
		    </div>
		    <div class="panel-body panel-form">
		        <form class="form-horizontal form-bordered" action="<?php echo $action;?>" method="post" enctype="multipart/form-data" data-parsley-validate="true">
					<?php
					if($cek=='edit'){
						?>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3">Foto User :</label>
							<div class="col-md-2 col-sm-2">
							<?php
								if($foto==""){
									$fotox = "no.jpg";
								}else{
									$fotox = $foto;
								}
							?>
								<img src="<?php echo base_url();?>assets/foto/sekolah/<?php echo $fotox;?>" style="width:150px;text-align:center;height:180px;">
							</div>
						</div>
						<?php	
					}
					?>
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Nama User * :</label>
						<div class="col-md-3 col-sm-3">
	                        <input class="form-control" type="text" id="nama" minlength="1" name="nama" value="<?php echo set_value('nama',isset($default['nama']) ? $default['nama'] : ''); ?>" data-type="nama" data-parsley-required="true" data-parsley-minlength="1" data-parsley-minlength="1"/>
							<span style="color:red;"><?php echo form_error('nama');?></span>
						</div>
                    </div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">No Handphone * :</label>
						<div class="col-md-2 col-sm-2">
							<input class="form-control" style="text-align:right" type="text" id="hp" minlength="1" name="hp" value="<?php echo set_value('hp',isset($default['hp']) ? $default['hp'] : ''); ?>" data-type="hp" data-parsley-required="true" data-parsley-type="number" data-parsley-minlength="1" data-parsley-minlength="15"/>
                            <span style="color:red;"><?php echo form_error('hp');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Email * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="mail" name="mail" value="<?php echo set_value('mail',isset($default['mail']) ? $default['mail'] : ''); ?>" data-type="mail" data-parsley-required="true" data-parsley-type="email"/>
                            <span style="color:red;"><?php echo form_error('email');?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Username * :</label>
						<div class="col-md-3 col-sm-3">
							<input class="form-control" type="text" id="username" name="username" value="<?php echo set_value('username',isset($default['username']) ? $default['username'] : ''); ?>" data-type="username" data-parsley-required="true"/>
                            <span style="color:red;"><?php echo form_error('username');?></span>
						</div>
					</div>
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Password * :</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="password" name="password" id="password" minlength="6" maxlength="20" <?php if($cek=="add"){ echo 'data-parsley-required="true"';} ;?>  class="form-control m-b-5" />
                            <span style="color:red;"><?php echo form_error('password');?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3">Ketikan Password * :</label>
                        <div class="col-md-3 col-sm-3">
                            <input type="password" name="konfirmasi" id="konfirmasi" minlength="6" maxlength="20" <?php if($cek=="add"){ echo 'data-parsley-required="true"';} ;?> class="form-control m-b-5" />
                            <span style="color:red;"><?php echo form_error('konfirmasi');?></span>
                        </div>
                    </div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3">Foto User :</label>
						<div class="col-md-3 col-sm-3">
                            <input name="MAX_FILE_SIZE" value="9999999999" type="hidden">
				            <input type="file" id="foto" name="foto" />  
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3"></label>
						<div class="col-md-3 col-sm-3">
							<button type="submit" class="btn btn-success btn-sm"><?php echo $tombolsimpan;?></button>
                      		<button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm"><?php echo $tombolbatal ; ?></button>
						</div>
					</div>
		        </form>
		    </div>
		</div>
	</div>
</div>
