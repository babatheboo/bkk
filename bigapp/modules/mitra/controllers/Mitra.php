<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mitra extends CI_Controller {
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
  		$this->asn->login(); // cek session login
  		$this->asn->role_akses("1");
  		$this->load->model('mitra_model');
  		$this->load->helper('form');
  	}
  	public function index(){
  		$this->_content();
  	}
  	public function _content(){
  		$lvl = $this->session->userdata('level');
  		$isi['kelas'] = "mitra";
  		$isi['namamenu'] = "Data Mitra Industri";
  		$isi['page'] = "mitra";
  		$isi['option_bidangx'][''] = "Pilih Bidang Usaha";
  		$ckbidang = $this->db->get_where('ref.bidang_usaha')->result();
  		if(count($ckbidang)>0){
  			foreach ($ckbidang as $row) {
  				$isi['option_bidangx'][trim($row->bidang_usaha_id)] = $row->nama_bidang_usaha;
  			}
  		}else{
  			$isi['option_bidangx'][''] = "Bidang Usaha Tidak Tersedia";
  		}
  		$isi['link'] = 'mitra';
  		$isi['halaman'] = "Data Mitra Industri";
  		$isi['judul'] = "Halaman Data Mitra Industri";
  		$sid = $this->session->userdata('role_');
  		$b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
  		$valid = "";
  		foreach ($b as $key) {
  			$valid = $key->valid;
  		}
  		if($valid=="1"){
  			$isi['content']              = "mitra_view";
  		} else {
  			$isi['content']              = "admin_bkk";
  		}


//		$isi['content'] = "mitra_view";
  		$this->load->view("dashboard/dashboard_view",$isi);
  	}

  	public function getData(){
  		if($this->input->is_ajax_request()){
  			$list = $this->mitra_model->get_datatables();
  			$data = array();
  			$no = $_POST['start'];
  			foreach ($list as $rowx) {
  				$no++;
  				$row = array();
  				$row[] = '<center>' . $no . "." . '</center>';
  				$row[] = $rowx->nama;
  				$row[] = $rowx->email;
  				$row[] = $rowx->no_tlp;
  				$row[] = $rowx->nama_bidang_usaha;
  				$row[] = '<center><a href="'.base_url().'mitra/edit/'.$rowx->industri_id.'" title="Edit Data" ><i class="far fa-lg fa-fw m-r-10 fa-edit"></i></a><a style="color:red"  href="javascript:void(0)" title="Hapus Data" onclick="hapus_mitra(\'Data Mitra Industri\',\'mitra\',\'hapus_data\','."'".$rowx->industri_id."'".')"><i class="fas fa-lg fa-fw m-r-10 fa-trash"></i></a></center>';
  				$data[] = $row;
  			}
  			$output = array(
  				"draw" => $_POST['draw'],
  				"recordsTotal" => $this->mitra_model->count_all(),
  				"recordsFiltered" => $this->mitra_model->count_filtered(),
  				"data" => $data,
  			);
  			echo json_encode($output);
  		}else{
  			redirect("_404","refresh");
  		}		
  	}
  	public function add(){
  		$isi['cek'] = "add";
  		$isi['kelas'] = "mitra";
  		$isi['namamenu'] = "Tambah Data Mitra Industri";
  		$isi['page'] = "mitra";
  		$isi['link'] = 'mitra';
  		$isi['option_provinsi'][''] = "Pilih Propinsi";
  		$isi['option_bidang'][''] = "Pilih Bidang Usaha";
  		$isi['option_kabupaten'][''] = "Pilih Kab / Kota";
  		$ckpropinsi = $this->db->query("SELECT * FROM ref.view_provinsi")->result();
  		if(count($ckpropinsi)>0){
  			foreach ($ckpropinsi as $row) {
  				$isi['option_provinsi'][trim($row->kode_prov)] = $row->prov;
  			}
  		}else{
  			$isi['option_provinsi'][''] = "Data Propinsi Belum Tersedia";
  		}
  		$ckbidang_usaha = $this->db->query("SELECT * FROM ref.bidang_usaha")->result();
  		if(count($ckbidang_usaha)>0){
  			foreach ($ckbidang_usaha as $row) {
  				$isi['option_bidang'][trim($row->bidang_usaha_id)] = $row->nama_bidang_usaha;
  			}
  		}else{
  			$isi['option_bidang'][''] = "Pilih bidang usaha";
  		}
  		$isi['action'] = "proses_add";
  		$isi['halaman'] = "Data Mitra Industri";
  		$isi['judul'] = "Halaman Tambah Data Mitra Industri";
  		$isi['content'] = "form_mitra";
  		$this->load->view("dashboard/dashboard_view",$isi);
  	}
  	public function getBidang(){
  		if($this->input->is_ajax_request()){
  			$list_bidang = array();
  			$data = $this->db->query("SELECT * FROM ref.bidang_usaha")->result();
  			foreach ($data as $key) {
  				$list_bidang[] = array("bidang_usaha_id" => trim($key->bidang_usaha_id), "nama_bidang_usaha" => $key->nama_bidang_usaha);
  			}
  			echo json_encode($list_bidang);
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function getBid(){
  		if($this->input->is_ajax_request()){
  			$return_arr = array();
  			$row_array = array();
  			$text = $this->input->get('text');
  			$prov = $this->db->select("*")
  			->from("ref.bidang_usaha")
  			->like("nama_bidang_usaha", $text)
  			->get();
  			if($prov->num_rows() > 0){
  				foreach($prov->result_array() as $row){
  					$row_array['id'] = trim($row['bidang_usaha_id']);
  					$row_array['text'] = utf8_encode("$row[nama_bidang_usaha]");
  					array_push($return_arr,$row_array);
  				}
  			}
  			echo json_encode(array("results" => $return_arr ));
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function getIndustri(){
  		if($this->input->is_ajax_request()){
  			$keyword = $this->input->post('term');
  			$data['response'] = 'false';
  			$query = $this->mitra_model->cari_industri($keyword);
  			if( ! empty($query) ){
  				$data['response'] = 'true';
  				$data['message'] = array();
  				foreach( $query as $row ){
  					$data['message'][] = array(
  						'label' => $row->nama_industri,
  						'nama'=>$row->nama_industri,
  						'industri_id'=>$row->industri_id,
  						'email'=>$row->email,
  						'alamat'=>$row->alamat,
  						'no_tlp'=>$row->no_tlp,
  						'province_id'=>trim($row->kode_prov),
  						'bidang_usaha_id'=>trim($row->bidang_usaha_id),
  						'nama_provinsi'=>$row->nama_provinsi
  					);
  				}
  			}
  			if('IS_AJAX'){
  				echo json_encode($data);
  			}     
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function getInfo(){
  		if($this->input->is_ajax_request()){
  			$id = $this->input->get('id');
  			$info = $this->db->select("ref.mst_wilayah.nama as nama_kota,ref.mst_wilayah.kode_wilayah,ref.industri.bidang_usaha_id,ref.industri.alamat")
  			->from("ref.industri")
  			->join("ref.mst_wilayah","ref.industri.kode_kota=ref.mst_wilayah.kode_wilayah")
  			->join("ref.bidang_usaha","ref.industri.bidang_usaha_id=ref.bidang_usaha.bidang_usaha_id")
  			->where("ref.industri.industri_id",$id)
  			->get()
  			->row();
  			echo json_encode($info);
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function getPro(){
  		if($this->input->is_ajax_request()){
  			$return_arr = array();
  			$row_array = array();
  			$text = $this->input->get('text');
  			$prov = $this->db->select("*")
  			->from("ref.view_provinsi")
  			->like("prov", $text)
  			->get();
  			if($prov->num_rows() > 0){
  				foreach($prov->result_array() as $row){
  					$row_array['id'] = $row['kode_prov'];
  					$row_array['text'] = utf8_encode("$row[prov]");
  					array_push($return_arr,$row_array);
  				}
  			}
  			echo json_encode(array("results" => $return_arr ));
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function getKab(){
  		if($this->input->is_ajax_request()){
  			$return_arr = array();
  			$row_array = array();
  			$text = $this->input->get('text');
  			$prov = $this->db->select("*")
  			->from("ref.view_kota")
								 // ->where("ref.view_kota.kode_prov",$pro)
  			->like("kota", $text)
  			->get();
  			if($prov->num_rows() > 0){
  				foreach($prov->result_array() as $row){
  					$row_array['id'] = $row['kode_kota'];
  					$row_array['text'] = utf8_encode("$row[kota]");
  					array_push($return_arr,$row_array);
  				}
  			}
  			echo json_encode(array("results" => $return_arr ));
  		}else{
  			redirect("_404","refresh");
  		}
  	}


  	public function getKabkot(){
  		if($this->input->is_ajax_request()){
  			$this->db->query("REFRESH MATERialized view ref.view_kota");
  			$kode = $_POST['depart']; 
  			$kodex = $this->asn->anti($kode);
  			$list_kota = array();
  			$data = $this->db->query("SELECT kode_kota,kota FROM ref.view_kota WHERE kode_prov = '$kodex'")->result();
  			foreach ($data as $key) {
  				$list_kota[] = array("id" => $key->kode_kota, "kota" => $key->kota);
  			}
  			echo json_encode($list_kota);
  		}else{
  			redirect("_404","refresh");
  		}
  	}

  	public function proses_add(){
  		$this->form_validation->set_rules('industri', 'industri', 'htmlspecialchars|trim|required|min_length[1]|max_length[100]');
  		$this->form_validation->set_rules('provinsi', 'provinsi', 'htmlspecialchars|trim|required|min_length[1]|max_length[50]');
  		$this->form_validation->set_rules('kabupaten', 'kabupaten', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_rules('address', 'alamat', 'htmlspecialchars|trim|required|min_length[1]|max_length[100]');
  		$this->form_validation->set_rules('no_tlp', 'no telepon', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_rules('bidang_industri', 'bidang industri', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_message('is_unique', '%s sudah ada sebelumnya');
  		$this->form_validation->set_message('required', '%s tidak boleh kosong');
  		$this->form_validation->set_message('min_length', '%s minimal %s karakter');
  		$this->form_validation->set_message('max_length', '%s maximal %s karakter');
  		$this->form_validation->set_message('is_natural', '%s hanya karakter angka');
  		if ($this->form_validation->run() == TRUE){
  			$industri = $this->asn->anti($this->input->post('industri'));
  			$provinsi = $this->asn->anti($this->input->post('provinsi'));
  			$kabupaten = $this->asn->anti($this->input->post('kabupaten'));
  			$alamat = $this->asn->anti($this->input->post('address'));
  			$no_tlp = $this->asn->anti($this->input->post('no_tlp'));
  			$bidang_industri = $this->asn->anti($this->input->post('bidang_industri'));
  			$email = $this->asn->anti($this->input->post('email'));

  			$simpanindustri = array('nama'=>$industri,
  				'alamat'=>$alamat,
  				'kode_kota'=>$kabupaten,
  				'kode_prov'=>$provinsi,
  				'no_tlp'=>$no_tlp,
  				'email'=>$email,
  				'bidang_usaha_id'=>$bidang_industri,
  				'registered'=>'1',
  				'tgl_add'=>date("Y-m-d"));
  			$this->db->insert('ref.industri',$simpanindustri);
  			$ckdata = $this->db->query("SELECT * FROM ref.industri ORDER BY id DESC");
  			$row = $ckdata->row();
  			$id = $row->industri_id;
  			$simpan = array('sekolah_id'=>$this->session->userdata('role_'),
  				'industri_id'=>$id,
  				'no'=>'',
  				'berkas'=>'',
  				'tgl_add'=>date("Y-m-d"),
  				'oleh'=>$this->session->userdata('kode'));
  			$this->db->insert('mitra',$simpan);
  			redirect('mitra','refresh');
  		}else{
  			$this->add();
  		}
  	}
  	public function edit($id){
  		$ckdata = $this->db->get_where('view_mitra',array('industri_id'=>$id));
  		if(count($ckdata->result())>0){
  			$cklintang = $this->db->get_where('ref.industri',array('industri_id'=>$id));
  			$ha = $cklintang->row();
  			$this->session->set_userdata('idna',$ha->id);
  			$isi['cek']	= "edit";  
  			$isi['kelas'] 	= "mitra";
  			$isi['namamenu'] = "Data Mitra Industri";
  			$isi['page'] = "mitra";
  			$isi['link'] = 'mitra';
  			$row = $ckdata->row();
  			$isi['default']['industri'] = $row->nama;
  			$isi['option_provinsi'][$row->kode_prov] = $row->kode_prov;
  			$pro = substr($row->kode_prov, 0,3);
  			$ckkota = $this->db->get_where('ref.view_kota',array('kode_kota'=>$row->kode_kota));
  			$xx = $ckkota->row();
  			$isi['option_kabupaten'][$row->kode_kota] = $xx->kota;
  			$isi['default']['address'] = $row->alamat;
  			$isi['default']['alamatbaru'] = $row->alamat;
  			$isi['default']['no_tlp'] = $row->no_tlp;
  			$isi['default']['email'] = $row->email;
  			$isi['option_bidang'][$row->bidang_usaha_id] = $row->nama_bidang_usaha;
  			$ckkota = $this->db->query("SELECT * FROM ref.view_kota WHERE LEFT(kode_prov,3) = '$pro'")->result();
  			foreach ($ckkota as $key) {
  				$isi['option_kabupaten'][$key->kode_kota] = $key->kota;
  			}
  			$ckbidang = $this->db->get('ref.bidang_usaha')->result();
  			foreach ($ckbidang as $key) {
  				$isi['option_bidang'][$key->bidang_usaha_id] = $key->nama_bidang_usaha;
  			}
  			$ckpropinsi = $this->db->query("SELECT * FROM ref.view_provinsi")->result();
  			if(count($ckpropinsi)>0){
  				foreach ($ckpropinsi as $row) {
  					$isi['option_provinsi'][trim($row->kode_prov)] = $row->prov;
  				}
  			}else{
  				$isi['option_provinsi'][''] = "Data Provinsi Belum Tersedia";
  			}
  			$isi['action'] = "../proses_edit";
  			$isi['halaman'] = "Data Mitra Industri";
  			$isi['judul'] = "Halaman Edit Data Mitra Industri";
  			$isi['content'] = "form_mitra";
  			$this->load->view("dashboard/dashboard_view",$isi);
  		}else{
  			redirect("_404","refresh");
  		}
  	}
  	public function proses_edit(){
  		$this->form_validation->set_rules('industri', 'industri', 'htmlspecialchars|trim|required|min_length[1]|max_length[100]');
  		$this->form_validation->set_rules('provinsi', 'provinsi', 'htmlspecialchars|trim|required|min_length[1]|max_length[50]');
  		$this->form_validation->set_rules('kabupaten', 'kabupaten', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_rules('email', 'e-mail', 'htmlspecialchars|trim|required|min_length[1]|max_length[50]|valid_email');
  		$this->form_validation->set_rules('address', 'alamat', 'htmlspecialchars|trim|required|min_length[1]|max_length[100]');
  		$this->form_validation->set_rules('no_tlp', 'no telepon', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_rules('bidang_industri', 'bidang industri', 'htmlspecialchars|trim|required|min_length[1]');
  		$this->form_validation->set_message('is_unique', '%s sudah ada sebelumnya');
  		$this->form_validation->set_message('required', '%s tidak boleh kosong');
  		$this->form_validation->set_message('min_length', '%s minimal %s karakter');
  		$this->form_validation->set_message('valid_email', 'penulisan e-mail tidak valid');
  		$this->form_validation->set_message('max_length', '%s maximal %s karakter');
  		$this->form_validation->set_message('is_natural', '%s hanya karakter angka');
  		if ($this->form_validation->run() == TRUE){
  			$industri = $this->asn->anti($this->input->post('industri'));
  			$provinsi = $this->asn->anti($this->input->post('provinsi'));
  			$kabupaten = $this->asn->anti($this->input->post('kabupaten'));
  			$alamat = $this->asn->anti($this->input->post('address'));
  			$no_tlp = $this->asn->anti($this->input->post('no_tlp'));
  			$bidang_industri = $this->asn->anti($this->input->post('bidang_industri'));
  			$email = $this->asn->anti($this->input->post('email'));
  			$simpanindustri = array('nama'=>$industri,
  				'alamat'=>$alamat,
  				'kode_kota'=>$kabupaten,
  				'kode_prov'=>$provinsi,
  				'no_tlp'=>$no_tlp,
  				'email'=>$email,
  				'bidang_usaha_id'=>$bidang_industri,
  				'registered'=>'1',
  				'tgl_add'=>date("Y-m-d"));
  			$this->db->where('id',$this->session->userdata('idna'));
  			$this->db->update('ref.industri',$simpanindustri);
  			redirect('mitra','refresh');
			// }
  			redirect('mitra','refresh');
  		}else{
  			$this->add();
  		}
  	}


  	public function hapus_data($id){
  		if($this->input->is_ajax_request()){		
  			$ckloker = $this->db->get_where('lowongan',array('kode_perusahaan'=>$id));
  			if(empty($ckloker->result())){
  				$this->mitra_model->hapus_by_id($id);
  				echo json_encode(array("status" => TRUE));
  			}else{
  				echo json_encode(array("status" => FALSE));
  			}
  		}else{
  			redirect("_404","refresh");
  		}
  	}

  	function expMitra(){
  		$sid = $this->session->userdata('role_');
  		$this->load->library('excel');
  		$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  		$cacheSettings = array( ' memoryCacheSize ' => '128MB');
  		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  		$excel = new PHPExcel();
  		$excel->getProperties()->setCreator('NAA')
  		->setLastModifiedBy('NAA')
  		->setTitle("Data Mitra Industri")
  		->setSubject("Mitra Industri")
  		->setDescription("Laporan Mitra Industri")
  		->setKeywords("Data Mitra Industri");
  		$style_col = array('font' => array('bold' => true),
  			'alignment' => array(
  				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
  				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
  			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  		$style_num = array('font' => array('bold' => false),
  			'alignment' => array(
  				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
  				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
  			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  		$style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
  			'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
  				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  		$excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA MITRA INDUSTRI");
  		$excel->getActiveSheet()->mergeCells('A1:F1');
  		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  		$excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Industri");
  		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Email");
  		$excel->setActiveSheetIndex(0)->setCellValue('D3', "No Tlp");
  		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Bidang Usaha");
  		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
  		$bkk = $this->db->query("SELECT * FROM view_mitra WHERE sekolah_id='$sid'")->result();
  		$no = 1;
  		$numrow = 4;
  		foreach($bkk as $data){
  			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
  			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama);
  			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->email);
  			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->no_tlp);
  			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->nama_bidang_usaha);
  			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_num);
  			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
  			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
  			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
  			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
  			$no++;
  			$numrow++;
  		}
  		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
  		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
  		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
  		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
  		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
  		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
  		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  		$excel->getActiveSheet(0)->setTitle("Laporan Mitra Industri");
  		$excel->setActiveSheetIndex(0);
  		header('Content-Type: application/vnd.ms-excel');
  		header('Content-Disposition: attachment; filename="Data Mitra Industri"');
  		header('Cache-Control: max-age=0');
  		header('Cache-Control: max-age=1');
  		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
  		header ('Cache-Control: cache, must-revalidate');
  		header ('Pragma: public');
  		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
  		$writer->save('php://output');
  		exit;
  	}
  }