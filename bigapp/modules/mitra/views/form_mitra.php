<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />

<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/add_mitra.js"></script>

<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse" data-sortable-id="form-validation-2">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $halaman;?></h4>
            </div>
            <div class="panel-body">
                <form name="form" id="form" action="<?php echo $action;?>" method="post">
                    
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Nama Industri * :</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control m-b-5" type="text" value="<?php echo set_value('industri',isset($default['industri']) ? $default['industri'] : ''); ?>" id="industri" minlength="1" name="industri" value="" data-parsley-required="true" data-parsley-minlength="1" />
                            <span style="color:red;"><?php echo form_error('industri');?></span>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Provinsi</label>
                        <div class="col-md-5 col-sm-5">
                            <?php echo form_dropdown('provinsi',$option_provinsi,isset($default['provinsi']) ? $default['provinsi'] : '','id="provinsi" name="provinsi" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
                            <span style="color:red;"><small><?php echo form_error('provinsi');?></small></span>
                        </div>
                    </div>

                    <div class="form-group row m-b-15">
                        <label class="control-label col-md-3">Kabupaten / Kota * :</label>
                            <div class="col-md-5 col-sm-5">
                            <?php echo form_dropdown('kabupaten',$option_kabupaten,isset($default['kabupaten']) ? $default['kabupaten'] : '','class="default-select2 form-control" id="kabupaten" name="kabupaten" data-live-search="true" data-style="btn-white"');?>
                            <span style="color:red;"><small><?php echo form_error('kabupaten');?></small></span>
                        </div>
                    </div>
                    
                    <div class="form-group row m-b-15">
                        <label class="control-label col-md-3">Alamat * :</label>
                        <div class="col-md-5 col-sm-5">
                            <input type="text" name="address" value="<?php echo set_value('address',isset($default['address']) ? $default['address'] : ''); ?>" id="address"class="form-control m-b-5"  />
                            <span style="color:red;"><?php echo form_error('address');?></span>
                        </div>
                    </div>
                    
                    <div class="form-group row m-b-15">
                        <label class="control-label col-md-3">No Telepon * :</label>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control m-b-5" type="text" id="no_tlp" value="<?php echo set_value('no_tlp',isset($default['no_tlp']) ? $default['no_tlp'] : ''); ?>" minlength="1" name="no_tlp" value="" data-parsley-required="true" data-parsley-minlength="1" />
                            <span style="color:red;"><?php echo form_error('no_tlp');?></span>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="control-label col-md-3">E-mail :</label>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control m-b-5" type="text" id="email" name="email" value=""/>
                            <!-- <span style="color:red;"><?php echo form_error('email');?></span> -->
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-form-label col-md-3">Bidang Usaha</label>
                        <div class="col-md-4">
                            <?php echo form_dropdown('bidang_industri',$option_bidang,isset($default['bidang_industri']) ? $default['bidang_industri'] : '','id="bidang_industri" name="bidang_industri" data-live-search="true" data-style="btn-white" class="default-select2 form-control"');?>
                            <span style="color:red;"><small><?php echo form_error('bidang_industri');?></small></span>
                            
                        </div>
                    </div>
                  
                    <div class="form-group row m-b-15">
                        <label class="control-label col-md-3 col-sm-3"></label>
                        <div class="col-md-3 col-sm-3">
                            <button type="submit" class="btn btn-success btn-sm">SIMPAN</button>
                            <button type="button" onclick="history.go(-1)" class="btn btn-info btn-sm">BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
