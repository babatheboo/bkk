<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Assesment extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->load->model('assesment/assesment_model');
		date_default_timezone_set('Asia/Jakarta');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		if($this->session->userdata('level')==3){
			$kode = $this->session->userdata('user_id');
			$cektest = $this->db->get_where('app.mbti_result',array('user_id'=>$kode));
			if(count($cektest->result())>0){
				echo "Kadieu";
				redirect('dashboard');
			}else{
				$isi['namamenu'] = "";
				$isi['page'] = "dashboard";
				$isi['kelas'] = "dashboard";
				$isi['link'] = 'dashboard';
				$isi['option_kategori'][''] = "Pilih Kategori Perusahaan";
				$isi['halaman'] = "Dashboard";
				$isi['judul'] = "Halaman Dashboard";
				$isi['content'] = "welcome";
				$this->load->view("assesment/quiz_view",$isi);
			}
		}else{
			$isi['namamenu'] = "";
			$isi['page'] = "dashboard";
			$isi['kelas'] = "dashboard";
			$isi['link'] = 'dashboard';
			$isi['option_kategori'][''] = "Pilih Kategori Perusahaan";
			$isi['halaman'] = "Dashboard";
			$isi['judul'] = "Halaman Dashboard";
			$isi['content'] = "welcome";
			$this->load->view("dashboard/dashboard_view",$isi);
		}
	}
	function proses(){
		$ext=0;
		$int=0;
		$sen=0;
		$tui=0;
		$thi=0;
		$fee=0;
		$jud=0;
		$per=0;
		for($a=1;$a<39;$a++){
			$h= $this->input->post('pil'.$a);
			$hasil = substr($h, -1);
			switch ($hasil) {
				case 'E':
					$ext++;
					break;
				case 'I':
					$int++;
					break;
				case 'S':
					$sen++;
					break;
				case 'N':
					$tui++;
					break;
				case 'T':
					$thi++;
					break;
				case 'F':
					$fee++;
					break;
				case 'J':
					$jud++;
					break;
				case 'P':
					$per++;
					break;
			}
		}
		$result="";
		if($ext>$int){
			$result .= "E";
		}else{
			$result .= "I";
		}
		if($sen>$tui){
			$result .= "S";
		}else{
			$result .= "N";
		}
		if($thi>$fee){
			$result .= "T";
		}else{
			$result .= "F";
		}
		if($jud>$per){
			$result .= "J";
		}else{
			$result .= "P";
		}
		$tgl = date("Y-m-d H:i:s");
		$nisn= $this->session->userdata('user_id');
		$data = array('user_id'=>$nisn,'assesment_date'=>$tgl,'assesment_result'=>$result);
		$this->db->insert('app.mbti_result',$data);
		redirect('dashboard/result');
	}
}
