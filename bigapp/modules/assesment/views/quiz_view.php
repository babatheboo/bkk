<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]-->
<head>

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>Personality Test</title>
<meta name="description" content="">
<meta name="author" content="Ansonika">

<!-- Favicons-->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url();?>assets/quiz/img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url();?>assets/quiz/img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url();?>assets/quiz/img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url();?>assets/quiz/img/apple-touch-icon-144x144-precomposed.png">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="<?php echo base_url();?>assets/quiz/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/quiz/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/quiz/font-awesome/css/font-awesome.css" rel="stylesheet" >
<link href="<?php echo base_url();?>assets/quiz/css/socialize-bookmarks.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/quiz/js/fancybox/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet">
<link href="<?php echo base_url();?>assets/quiz/check_radio/skins/square/aero.css" rel="stylesheet">

<!-- Toggle Switch -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/quiz/css/jquery.switch.css">

<!-- Owl Carousel Assets -->
<link href="<?php echo base_url();?>assets/quiz/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/quiz/css/owl.theme.css" rel="stylesheet">

<!-- Google web font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Jquery -->
<script src="<?php echo base_url();?>assets/quiz/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url();?>assets/quiz/js/jquery-ui-1.8.22.min.js"></script>

<!-- Wizard-->
<script src="<?php echo base_url();?>assets/quiz/js/jquery.wizard.js"></script>

<!-- Radio and checkbox styles -->
<script src="<?php echo base_url();?>assets/quiz/check_radio/jquery.icheck.js"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="<?php echo base_url();?>assets/quiz/js/modernizr.custom.17475.js"></script>

<!-- Support media queries for IE8 -->
<script src="<?php echo base_url();?>assets/quiz/js/respond.min.js"></script>

</head>

<body>           
            <div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>Personality Test</h1>
                <p>Sebagai Pra Syarat untuk melamar kerja</p>
                </div>
            </div>
            </div>

<section class="container" id="main">

<!-- Start Survey container -->
<div id="survey_container">
<div id="top-wizard">
        <strong>Progress <span id="location"></span></strong>
        <div id="progressbar"></div>
        <div class="shadow"></div>
    </div><!-- end top-wizard -->
    <form name="example-1" id="wrapped" action="<?php echo base_url();?>assesment/proses" method="POST" >
        <div id="middle-wizard">
            
                
             <?php
                $soal = $this->db->get("app.mbti_assesment");
                $x=0;
                foreach ($soal->result() as $row)            
                {
                    $x=$x+1;
            ?>
                <div class="step row">
                <div class="col-md-10">
                <h3><?php echo $row->soal;?></h3>
                <ul class="data-list-2">
                        <li><input name="pil<?php echo $x;?>" type="radio" class="required check_radio" value="<?php echo $row->nilai1;?>"><label><?php echo $row->jawaban1;?></label></li>
                        <li><input name="pil<?php echo $x;?>" type="radio" class="required check_radio" value="<?php echo $row->nilai2;?>"><label><?php echo $row->jawaban2;?></label></li>
                </ul>
                </div>
            </div><!-- end step -->
            
            <?php
                }
            ?>
            
            <div class="submit step" id="complete">
                        <i class="icon-check"></i>
                        <h3>Test Selesai</h3>
                        <button type="submit" name="process" class="submit">Simpan Hasil</button>
            </div><!-- end submit step -->
            
        </div><!-- end middle-wizard -->
        
        <div id="bottom-wizard">
            <button type="button" name="backward" class="backward">Backward</button>
            <button type="button" name="forward" class="forward">Forward </button>
        </div><!-- end bottom-wizard -->
    </form>
    
</div><!-- end Survey container -->


</section><!-- end section main container -->
       

<!-- OTHER JS --> 
<script src="<?php echo base_url();?>assets/quiz/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/quiz/js/jquery.placeholder.js"></script>
<script src="<?php echo base_url();?>assets/quiz/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/quiz/js/retina.js"></script>
<script src="<?php echo base_url();?>assets/quiz/js/functions.js"></script>


<!-- FANCYBOX -->
<script  src="<?php echo base_url();?>assets/quiz/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/quiz/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/quiz/js/fancy_func.js" type="text/javascript"></script> 

</body>
</html>