<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model {
    var $table = 'view_lulusan';
    var $column_order = array(null,'nama','nsekolah','jenis_kelamin'); //set column field database for datatable orderable
    var $column_search = array('nama','nsekolah','jenis_kelamin'); //set column field database for datatable searchable 
    var $order = array('peserta_didik_id' => 'asc'); // default order 
     public function __construct()
     {
        parent::__construct();
        $this->db1 = $this->load->database('default', TRUE);
     }
     public function get_menu($level)
     {
        $qry =  $this->db1->query("CALL get_menu('$level')");
        $hasil = $qry->result();
        mysqli_next_result( $this->db1->conn_id );
        $qry->free_result();
        return $hasil;
     }
     public function get_submenu($level)
     {
        $qry =  $this->db1->query("CALL get_submenu('$level')");
        $hasil = $qry->result();
        mysqli_next_result( $this->db1->conn_id );
        $qry->free_result();
        return $hasil;
     }
     public function get_submenux()
     {
        $qry =  $this->db1->query("CALL get_submenux()");
        $hasil = $qry->result();
        mysqli_next_result( $this->db1->conn_id );
        $qry->free_result();
        return $hasil;
     }
     public function get_jml_bkk(){
        $q = $this->db1->query("SELECT count(sekolah_id) as jml FROM sekolah_terdaftar");
        $x = $q->row();
        return $x->jml;
     }
    public function get_siswa(){
        $q = $this->db1->query("SELECT count(peserta_didik_id) as jml FROM ref.peserta_didik WHERE jenis_keluar_id IS NULL");
        $x = $q->row();
        return $x->jml;
    }
     public function get_lulusan(){
 $lvl = $this->session->userdata('role_');
        $lv = $this->session->userdata('level');
        switch ($lv) {
            case '0':
                $q = $this->db1->query("SELECT count(peserta_didik_id) as jml FROM ref.peserta_didik WHERE jenis_keluar_id = '1'");
                break;
            case '1':
                $q = $this->db1->query("SELECT count(peserta_didik_id) as jml FROM ref.peserta_didik WHERE jenis_keluar_id = '1' AND sekolah_id='$lvl'");
                break;
            case '4':
                $q = $this->db1->query("SELECT count(peserta_didik_id) as jml FROM ref.peserta_didik WHERE jenis_keluar_id = '1'");
                break;
           case '3':
                $q = $this->db1->query("SELECT count(peserta_didik_id) as jml FROM ref.peserta_didik WHERE jenis_keluar_id = '1' AND sekolah_id='$lvl'");
                break;            
        }
        $x = $q->row();
        return $x->jml;     
    }
     function data_log($id){
        $this->db1->where('jurusan_id',$id);
        $this->db1->from($this->table); 
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db1->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db1->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db1->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db1->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db1->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db1->order_by(key($order), $order[key($order)]);
        }
    }
      function get_datatables($id)
    {
        $this->data_log($id);
        if($_POST['length'] != -1)
        $this->db1->limit($_POST['length'], $_POST['start']);
        $query = $this->db1->get();
        return $query->result();
    }
    function count_filtered($id)
    {
        $this->data_log($id);
        $query = $this->db1->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db1->from($this->table);
        return $this->db1->count_all_results();
    }
}