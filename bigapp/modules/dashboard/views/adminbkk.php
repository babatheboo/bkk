<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="note note-success m-b-15">
            <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
            <div class="note-content">
                <h4><b>Update data lulusan</b></h4>
                <p>Data lulusan tahun <b>2019</b> dan <b>2020</b> <b>sudah bisa disinkronisasi</b>. Silahkan lakukan sinkronisasi di menu <b>Manajemen Siswa</b>. Terima kasih</p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fas fa-users"></i></div>
            <div class="stats-info">
                <h4>JUMLAH LULUSAN</h4>
                <p>
                    <?php
                    $sess = $this->session->userdata('role_');
                    $dt = $this->db->query("SELECT count(peserta_didik_id) as jumlah from ref.peserta_didik where sekolah_id='$sess'")->row();
                    echo number_format($dt->jumlah,0);
                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fas fa-building"></i></div>
            <div class="stats-info">
                <h4>JUMLAH INDUSTRI MITRA</h4>
                <p>
                    <?php
                    $sess = $this->session->userdata('role_');
                    $dt = $this->db->query("SELECT count(industri_id) as jumlah from view_mitra where sekolah_id='$sess'")->row();
                    echo number_format($dt->jumlah,0);
                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fas fa-sitemap"></i></div>
            <div class="stats-info">
                <h4>TOTAL LOWONGAN TERBUKA</h4>
                <p>
                    <?php
                    $sess = $this->session->userdata('role_');
                    $jm = $this->db->query("select count(id) as jumlah from lowongan where sekolah_pembuat='$sess'")->result();
                    foreach ($jm as $key) {
                    echo number_format($key->jumlah);
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('forum_list');
?>