<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>..:: Admin Page BKK ::..</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontpage/img/favicon.ico">

  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet"/>
  <link href="<?php echo base_url();?>assets/backend/plugins/animate/animate.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/css/default/style.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/css/default/style-responsive.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/css/default/theme/default.css" rel="stylesheet" id="theme" />
  <!-- ================== END BASE CSS STYLE ================== -->

  <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
  <link href="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/DataTables/media/css/data-table.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/backend/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
  <link href="<?php echo base_url();?>assets/backend/plugins/prettify/prettify.css" rel="stylesheet"/>
  <!-- ================== END PAGE LEVEL STYLE ================== -->

  <!-- ================== BEGIN BASE JS ================== -->
  <script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
  <!-- <script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-migrate-3.0.0.min.js"></script> -->
  <script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-migrate-1.4.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/backend/crossbrowserjs/html5shiv.js"></script>
        <script src="<?php echo base_url();?>assets/backend/crossbrowserjs/respond.min.js"></script>
        <script src="<?php echo base_url();?>assets/backend/crossbrowserjs/excanvas.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/js-cookie/js.cookie.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery.blockUI.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/bootbox/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/theme/default.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/apps.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/app.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="<?php echo base_url();?>assets/backend/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/prettify/prettify.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->

    <script>
      $(document).ready(function() {
         App.init();

     });
 </script>
</head>
<body>

    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <?php
        $this->load->view('dashboard/menu_atas');
        ?>
        <?php
//        	if($this->session->userdata('level')=="1"){
//        		$sid = $this->session->userdata('role_');
//        		$b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
//        		$valid = "";
//        		foreach ($b as $key) {
//        			$valid = $key->valid;
//        		}
//        		if($valid=="1"){
        $this->load->view('dashboard/menu_samping');
//        		} 
//        	} else {
//        		$this->load->view('dashboard/menu_samping');
//        	}

        ?>
        <div class="sidebar-bg"></div>
        <div id="content" class="content">
            <h1 class="page-header"><?php echo $halaman;?> <small><?php echo $judul;?></small></h1>
            <?php
            $this->load->view($content);
            ?>
        </div>
        <?php 
        $this->load->view('dashboard/footer');
        ?>
    </div>


    <!-- ================== BEGIN BASE JS ================== -->

    <!-- ================== END BASE JS ================== -->
    <!-- <div id="cso" style="position:fixed;bottom:0;right:0;margin-top: -2.5em;text-align:center">
        <a href="javascript:void()" onclick="helpdesk()"><img src="<?php echo base_url();?>assets/frontpage/img/helpdesk.png" alt="helpdesk" style="width:80px;height:80px"></a> 
    </div> -->
    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i)
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i)
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i)
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i)
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i)
            },
            any: function() {
                return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()
            }
        };

        function helpdesk() {
            isMobile.any() ? window.location.href = "whatsapp://send?phone=628119252424" : window.open("https://web.whatsapp.com/send?phone=628119252424", "_blank")
        }
    </script>	

</body>
</html>
