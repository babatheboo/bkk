<?php 
$uid = $this->session->userdata('user_id');
$dt = $this->db->query("SELECT * FROM app.user_dinas WHERE user_id='$uid'")->row();
$this->db->query("REFRESH MATERIALIZED VIEW mview_kerja_prov");
$this->db->query("REFRESH MATERIALIZED VIEW mview_wira_prov");
$this->db->query("REFRESH MATERIALIZED VIEW mview_kuliah_prov");
$kodeprov = $dt->kode_prov;
$bln = date('n');
if($bln<7){
  $t1 = date('Y') - 3;
  $t2 = $t1+1;
  $t3 = $t1+2;
  $t4 = $t1+3;
}else{
  $t1 = date('Y') - 3;
  $t2 = $t1+1;
  $t3 = $t1+2;
  $t4 = $t1+3;
}
?>



<div class="row">
  <div class="col-lg-6 col-md-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-university"></i></div>
      <div class="stats-info">
        <h4 style="text-alingment:right">TOTAL BKK TERDAFTAR</h4>
        <p>
          <?php
          $j = $this->db->query("SELECT count(a.sekolah_id) as jumlah from ref.sekolah a
            left join ref.view_sekolah_wilayah b on a.sekolah_id=b.sekolah_id
            left join view_bkk_terdaftar c on a.npsn = c.npsn
            where b.kode_prov='$kodeprov' and c.valid='1'")->row();
          echo number_format($j->jumlah,0);
          ?>
        </p>    
      </div>
    </div>
  </div>

  <div class="col-lg-6 col-md-6">
    <div class="widget widget-stats bg-blue">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-users"></i></div>
      <div class="stats-info">
        <h4 style="text-alingment:right">JUMLAH LULUSAN (<?php echo $t1 .'-'. $t3;?>)</h4>
        <p>
          <?php
          $j = $this->db->query("SELECT count(a.peserta_didik_id) as jumlah, b.kode_prov from ref.peserta_didik a left join ref.view_sekolah_wilayah b on a.sekolah_id=b.sekolah_id where b.kode_prov='$kodeprov' and left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' group by b.kode_prov order by b.kode_prov asc")->row();
          echo number_format($j->jumlah,0);
          ?>
        </p>    
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6">
    <div class="widget widget-stats bg-info">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-cog"></i></div>
      <div class="stats-info">
        <h4>JUMLAH BEKERJA</h4>
        <span><?php echo $t1 .'-'. $t3;?></span>
        <p>
          <?php
          $dt = $this->db->query("SELECT SUM(total_bekerja) as jumlah from  mview_kerja_prov WHERE kode_prov='$kodeprov' AND tahun>='$t1' AND tahun<='$t3'")->row();
          echo number_format($dt->jumlah,0);
          ?>
        </p>    
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6">
    <div class="widget widget-stats bg-info">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-user"></i></div>
      <div class="stats-info">
        <h4>JUMLAH WIRAUSAHA</h4>
        <span><?php echo $t1 .'-'. $t3;?></span>
        <p>
         <?php
         $dt = $this->db->query("SELECT SUM(total_wira) as jumlah from  mview_wira_prov WHERE kode_prov='$kodeprov'")->row();
         echo number_format($dt->jumlah,0);
         ?>

       </p>    
     </div>
   </div>
 </div>

 <div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-graduation-cap"></i></div>
    <div class="stats-info">
      <h4>JUMLAH MELANJUTKAN</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
       <?php
       $dt = $this->db->query("SELECT SUM(total_kuliah) as jumlah from  mview_kuliah_prov WHERE kode_prov='$kodeprov'")->row();
       echo number_format($dt->jumlah,0);
       ?>
     </p>    
   </div>
 </div>
</div>
<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-orange">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-info-circle"></i></div>
    <div class="stats-info">
      <h4>TIDAK TERDATA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
        <?php
        $jmls = $this->db->query("SELECT count(a.peserta_didik_id) as ttls, b.kode_prov from ref.peserta_didik a left join ref.view_sekolah_wilayah b on a.sekolah_id=b.sekolah_id where b.kode_prov='$kodeprov' and left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' group by b.kode_prov order by b.kode_prov asc")->row();
        $krj = $this->db->query("SELECT SUM(total_bekerja) as ttlker from  mview_kerja_prov WHERE kode_prov='$kodeprov' AND tahun>='$t1' AND tahun<='$t3'")->row();
        $wira = $this->db->query("SELECT SUM(total_wira) as ttlwir from  mview_wira_prov WHERE kode_prov='$kodeprov'")->row();
        $kul = $this->db->query("SELECT SUM(total_kuliah) as ttlkul from  mview_kuliah_prov WHERE kode_prov='$kodeprov'")->row();
        echo number_format($jmls->ttls - $krj->ttlker - $wira->ttlwir - $kul->ttlkul,0);
        ?>
      </p>    
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-lg-12 ui-sortable">
   <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <div class="panel-heading-btn">
        <a href="javascript:;" class="btn btn-success btn-icon btn-circle btn-sm" data-click="notif-xlsx"><i class="fa fa-file-excel"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-primary" data-click="panel-reload"><i class="fa fa-redo"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
      </div>
      <h4 class="panel-title">DATA BKK TERDAFTAR</h4>
    </div>
    <div class="panel-body">

      <div class="table-responsive"> 
        <table id="data-bkk-prov" class="table table-striped table-bordered nowrap" width="100%"> 
          <thead> 
            <tr> 
              <th style="text-align:center" width="1%">No.</th> 
              <th style="text-align:center" width="5%">NPSN</th>
              <th style="text-align:center" width="64%">Nama Sekolah</th>
              <th style="text-align:center" width="30%">Kota / Kab</th>
              <!-- <th style="text-align:center" width="5%">Jumlah Bekerja</th> -->
            </tr> 
          </thead> 
          <tbody> 
          </tbody> 
        </table> 
      </div> 
    </div>
  </div>
</div>
</div>
<div class="row">
  <div class="col-lg-12 ui-sortable">
   <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <div class="panel-heading-btn">
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
      </div>
      <h4 class="panel-title">DATA BEKERJA</h4>
    </div>
    <div class="panel-body"> 
      <div class="table-responsive"> 
        <table id="data-bekerja-prov" class="table table-striped table-bordered nowrap" width="100%"> 
          <thead> 
            <tr> 
              <th style="text-align:center" width="1%">No.</th> 
              <th style="text-align:center" width="64%">Nama Sekolah</th>
              <th style="text-align:center" width="30%">Kota / Kab.</th>
              <th style="text-align:center" width="5%">Jumlah Bekerja</th>
            </tr> 
          </thead> 
          <tbody> 
          </tbody> 
        </table> 
      </div> 
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-lg-12 ui-sortable">
   <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <div class="panel-heading-btn">
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
      </div>
      <h4 class="panel-title">DATA WIRAUSAHA</h4>
    </div>
    <div class="panel-body"> 
      <div class="table-responsive"> 
        <table id="data-wira-prov" class="table table-striped table-bordered nowrap" width="100%"> 
          <thead> 
            <tr> 
              <th style="text-align:center" width="1%">No.</th> 
              <th style="text-align:center" width="64%">Nama Sekolah</th>
              <th style="text-align:center" width="30%">Kota / Kab.</th>
              <th style="text-align:center" width="5%">Jumlah Wirausaha</th>
            </tr> 
          </thead> 
          <tbody> 
          </tbody> 
        </table> 
      </div> 
    </div>
  </div>
</div>
</div>


<div class="row">
  <div class="col-lg-12 ui-sortable">
   <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <div class="panel-heading-btn">
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
        <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
      </div>
      <h4 class="panel-title">DATA MELANJUTKAN</h4>
    </div>
    <div class="panel-body"> 
      <div class="table-responsive"> 
        <table id="data-kul-prov" class="table table-striped table-bordered nowrap" width="100%"> 
          <thead> 
            <tr> 
              <th style="text-align:center" width="1%">No.</th> 
              <th style="text-align:center" width="64%">Nama Sekolah</th>
              <th style="text-align:center" width="30%">Kota / Kab.</th>
              <th style="text-align:center" width="5%">Jumlah Melanjutkan</th>
            </tr> 
          </thead> 
          <tbody> 
          </tbody> 
        </table> 
      </div> 
    </div>
  </div>
</div>
</div>

<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>

<script type="text/javascript">
  $BASE_URL = '<?php echo base_url();?>';  
  var save_method;
  var table;
  var table1;
  var table2;
  var table3;

  $(function() {
    table = $('#data-bekerja-prov').DataTable({ 
      autoWidth: false,
      "processing": true,
      "serverSide": true,
      "responsive": true, 
      "order": [],
      "pageLength": 10,
      "ajax": {
        "url": $BASE_URL+ "dashboard/getKerjaProv",
        "type": "POST"
      },
      order: [[ 0, 'desc' ]],
      dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
      displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });

  $(function() {
    table1 = $('#data-wira-prov').DataTable({ 
      autoWidth: false,
      "processing": true,
      "serverSide": true,
      "responsive": true, 
      "order": [],
      "pageLength": 10,
      "ajax": {
        "url": $BASE_URL+ "dashboard/getWiraProv",
        "type": "POST"
      },
      order: [[ 0, 'desc' ]],
      dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
      displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });
  $(function() {
    table2 = $('#data-kul-prov').DataTable({ 
      autoWidth: false,
      "processing": true,
      "serverSide": true,
      "responsive": true, 
      "order": [],
      "pageLength": 10,
      "ajax": {
        "url": $BASE_URL+ "dashboard/getKulProv",
        "type": "POST"
      },
      order: [[ 0, 'desc' ]],
      dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
      displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });
  $(function() {
    table3 = $('#data-bkk-prov').DataTable({ 
      autoWidth: false,
      "processing": true,
      "serverSide": true,
      "responsive": true, 
      "order": [],
      "pageLength": 10,
      "ajax": {
        "url": $BASE_URL+ "dashboard/getBKKProv",
        "type": "POST"
      },
      order: [[ 0, 'desc' ]],
      dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
      displayLength: 4,               
    });
    $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
    
    $("input").change(function(){
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });
</script>