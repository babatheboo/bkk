<link href="<?php echo base_url();?>assets/backend/plugins/parsley/src/parsley.css" rel="stylesheet" />
<?php
$sid = $this->session->userdata('role_');
$a = $this->db->query("SELECT * FROM sekolah_terdaftar WHERE sekolah_id='$sid'");
$b = $a->result();
$c = $a->row();
$valid = "";
$nobkk = "";
$nosk = "";
$scan = "";
$exp = "";

foreach ($b as $key) {
	$valid = $key->valid;
	$nobkk = $key->no_izin_bkk;
	$nosk = $key->no_sk_pendirian;
	$exp = $key->tgl_kadaluarsa;
	$scan = $key->scan;
	$note = $key->notif;
}


if ($c->no_izin_bkk) {
	# code...
}
?>
<div class="alert alert-danger fade show m-b-10">
	<span class="close" data-dismiss="alert">×</span>
	Sekolah Bursa Kerja Khusus belum tervalidasi, perbaiki data BKK anda atau hubugi Call Center BKK di No : 08119252424.
</div>

<div class="row">
	<div class="col-lg-6 ui-sortable">
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<!-- begin panel-heading -->
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Data yang anda upload</h4>
			</div>
			<!-- end panel-heading -->
			<!-- begin panel-body -->
			<div class="panel-body">
				<?php
				if ($nobkk=="") {
					echo "<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>Anda belum mengupload Nomor Izin BKK</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".$nosk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".bulan_indo($exp)."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<img src='../assets/foto/scan/".$scan."' style='width:100%;'/>
					</div>
					</div>";
				}elseif ($nosk == "") {
					echo "<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>".$nobkk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>Anda belum mengupload Nomor Surat Keputusan</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".bulan_indo($exp)."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<img src='../assets/foto/scan/".$scan."' style='width:100%;'/>
					</div>
					</div>";
				}elseif ($scan=="") {
					echo "<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>".$nobkk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".$nosk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".bulan_indo($exp)."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>Anda belum mengupload Bukti Scan</strong>
					</div>
					</div>";
				}else if ($exp=="") {
					echo "<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>".$nobkk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".$nosk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>Tanggal berakhir belum diisi</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<img src='../assets/foto/scan/".$scan."' style='width:100%;'/>
					</div>
					</div>";
				}
				else{
					echo "<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor Registrasi BKK :</label>
					<div class='col-md-6 col-sm-6'>
					<strong>".$nobkk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Nomor SK Pendirian :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".$nosk."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Tanggal Berakhir :</label>
					<div class='col-md-8 col-sm-8'>
					<strong>".bulan_indo($exp)."</strong>
					</div>
					</div>
					<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Bukti Scan :</label>
					<div class='col-md-6 col-sm-6'>
					<img src='../assets/foto/scan/".$scan."' style='width:100%;'/>
					</div>
					</div>";
				}
				?>
				<div class='form-group row m-b-15'>
					<label class='control-label col-md-4 col-sm-4 text-right'>Catatan dari Admin:</label>
					<div class='col-md-6 col-sm-6 alert alert-warning'>
						<p style="text-align: justify;"><?php echo $note;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6 ui-sortable">
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<!-- begin panel-heading -->
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Perbaikan data registrasi BKK</h4>
			</div>
			<!-- end panel-heading -->
			<!-- begin panel-body -->
			<div class="panel-body">
				<div class="alert alert-secondary">
					Harap diisi dengan data yang benar, untuk menghindari pemblokiran akun anda !. Hubungi call center BKK jika anda sudah melakukan perbaikan data BKK anda.
				</div>
				<div class="panel-body panel-form">
					<form action="<?php echo base_url();?>dashboard/proses_edit" method="post" enctype="multipart/form-data" data-parsley-validate="true">						
						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">No Reg BKK * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="text" name="no_izin" title="No Izin BKK" value="<?php echo 
set_value('no_izin',isset($default['no_izin']) ? $default['no_izin'] : ''); ?>" id="no_izin" placeholder="Masukan No Registrasi BKK" 
data-parsley-required="true" data-parsley-required-message="Nomor registrasi tidak boleh kosong" class="form-control"/>
								<span style="color:red;"><?php echo form_error('no_izin');?></span>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">SK Pendirian * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="text" title="Surat Keputusan Pendirian BKK" name="sk_pendirian" value="<?php echo set_value('sk_pendirian',isset($default['sk_pendirian']) ? $default['sk_pendirian'] : ''); ?>" id="sk_pendirian" placeholder="Masukan SK Pendirian" class="form-control" data-parsley-required="true" data-parsley-required-message="Nomor SK tidak boleh kosong"/>
								<span style="color:red;"><?php echo form_error('sk_pendirian');?></span>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="control-label col-md-3 col-sm-3 text-right">Tgl Kadaluarsa * :</label>
							<div class="col-md-6 col-sm-6">
								<input type="date" title="Tanggal Kadaluarsa" name="expired" value="<?php echo set_value('expired',isset($default['expired']) ? $default['expired'] : ''); ?>" id="expired" placeholder="Masukan Tanggal Kadaluarsa" class="form-control" data-parsley-required="true" data-parsley-required-message="Tentukan tanggal kadaluarsa izin BKK"/>
								<span style="color:red;"><?php echo form_error('expired');?></span>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="control-label col-md-4 col-sm-4">Scan Dokumen Izin :<br/>
								<b><span class="small"><font color="red">* Type Format jpg | png <br/> Max Size : 1 Mb</font></span></b></label>
								<div class="col-md-3 col-sm-3">
									<input name="MAX_FILE_SIZE" value="9999999999" type="hidden">
									<input type="file" id="scan" name="scan" data-parsley-required="true" data-parsley-required-message="Pilih scan dokumen"/>
								</div>
							</div>
							<div class="form-group row m-b-15">
								<label class="control-label col-md-3 col-sm-3"></label>
								<div class="col-md-3 col-sm-3">
									<button type="submit" class="btn btn-success btn-sm">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="<?php echo base_url();?>assets/backend/plugins/parsley/dist/parsley.js"></script>
