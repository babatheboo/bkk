<script src="<?php echo base_url();?>assets/backend/js/validasi.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<style type="text/css">
  .modal { overflow-y: auto !important; }
</style>
<div class="row">
  <div class="col-lg-3 col-md-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-building"></i></div>
      <div class="stats-info">
        <h4 style="text-alingment:right">TOTAL BKK TERDAFTAR</h4>
        <p>
          <?php
          $skl = $this->db->query("SELECT count(sekolah_id) as jml from ref.sekolah")->row();
          $jm = $this->db->query("SELECT count(sekolah_id) as jumlah from sekolah_terdaftar where valid='1'")->result();
          foreach ($jm as $key) {
           echo number_format($key->jumlah) . ' <small>('. number_format($key->jumlah / $skl->jml * 100) .'%)</small>';
         }
         $bln = date('n');
         if($bln<7){
          $t1 = date('Y') - 3;
          $t2 = $t1+1;
          $t3 = $t1+2;
          $t4 = $t1+3;
        }else{
          $t1 = date('Y') - 3;
          $t2 = $t1+1;
          $t3 = $t1+2;
          $t4 = $t1+3;
        }
        ?>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-graduation-cap"></i></div>
    <div class="stats-info">
      <h4>JUMLAH LULUSAN (<?php echo $t1 .'-'. $t3;?>)</h4>
      <p>
        <?php
        $ttls = $this->db->query("SELECT count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)>='$t1' AND left(tanggal_keluar,4)<='$t3'")->row();
        echo number_format($ttls->jumlah,0);
        ?>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="far fa-lg fa-fw m-r-10 fa-handshake"></i></div>
    <div class="stats-info">
      <h4>JUMLAH INDUSTRI</h4>
      <p>
       <?php
       $dt = $this->db->query("SELECT count(industri_id) as jumlah from ref.industri")->row();
       echo number_format($dt->jumlah,0);
       ?>

     </p>    
   </div>
 </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-briefcase"></i></div>
    <div class="stats-info">
      <h4>TOTAL LOWONGAN TERBUKA</h4>
      <p>
       <?php
       $jm = $this->db->query("select count(id) as jumlah from lowongan where sekolah_pembuat is not null")->result();
       foreach ($jm as $key) {
         echo number_format($key->jumlah);
       }
       ?>
     </p>    
   </div>
 </div>
</div>
<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-cog"></i></div>
    <div class="stats-info">
      <h4>JUMLAH BEKERJA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
        <?php
        $krj = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        echo number_format($krj->total,0);
        ?><br>
        <small><?php echo ' (' . number_format($krj->total / $ttls->jumlah * 100, 2) . '%' . ')' ?></small>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-user"></i></div>
    <div class="stats-info">
      <h4>JUMLAH WIRAUSAHA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
       <?php
       $wr = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
       echo number_format($wr->total,0);
       ?><br>
       <small><?php echo ' (' . number_format($wr->total / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
     </p>    
   </div>
 </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-graduation-cap"></i></div>
    <div class="stats-info">
      <h4>JUMLAH MELANJUTKAN</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
       <?php
       $kl = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
       echo number_format($kl->total,0);
       ?><br>
       <small><?php echo ' (' . number_format($kl->total / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
     </p>
   </div>
 </div>
</div>
<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-orange">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-info-circle"></i></div>
    <div class="stats-info">
      <h4>TIDAK TERDATA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
        <?php
        $krj = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $wira = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $kul = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $ttd = $ttls->jumlah - $krj->total - $wira->total - $kul->total;
        echo number_format($ttd,0);
        ?><br>
        <small><?php echo ' (' . number_format($ttd / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
      </p>
    </div>
  </div>
</div>
<div class="col-lg-12 ui-sortable">
  <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <h4 class="panel-title">Grafik BKK Terdaftar Tahun <?php echo date('Y');?></h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
      <canvas id="terdaftarChart"></canvas>
    </div>
    <!-- end panel-body -->
  </div>
</div>
</div>

<div class="row">
  <div class="col-lg-12 ui-sortable">
    <!-- begin panel -->
    <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading ui-sortable-handle">
        <div class="panel-heading-btn">
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
          <a href="javascript:reload_table();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Validasi Data BKK</h4>
      </div>
      <div class="panel-body"> 
        <div class="table-responsive"> 
          <table id="data-validasi" class="table table-striped table-bordered nowrap" width="100%"> 
            <thead> 
              <tr> 
                <th style="text-align:center" width="1%">No.</th> 
                <th style="text-align:center" width="15%">NPSN</th>
                <th style="text-align:center" width="15%">Nama Sekolah</th>
                <th style="text-align:center" width="20%">No Reg BKK</th>
                <th style="text-align:center" width="10%">No SK Pendirian</th>
                <th style="text-align:center" width="10%">Tgl Upload</th>
                <th style="text-align:center" width="1%">Detail</th>
              </tr> 
            </thead> 
            <tbody> 
            </tbody> 
          </table> 
        </div> 
      </div>

      <!-- modal validasi -->
      <div id="valid-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
            <h4 class="modal-title" id="myModalLabel">Detail Validasi</h4>
          </div>
          <div class="modal-body">
            <strong><i class="fas fa-list-alt m-l-5"></i> No Reg: <label 
              id="izin"></label></strong> <button class="btn btn-xs btn-success" onclick="izinE(id)"><i class="fas fa-pencil-alt"></i></button><br>
              <strong><i class="fas fa-sticky-note m-l-5"></i> No SK: <label id="sk"></label></strong> <button class="btn btn-xs btn-success" onclick="skE(id)"> <i class="fas fa-pencil-alt"></i></button><br>
              <strong> <i class="fas fa-calendar-alt m-l-5"></i> Kadaluarsa: <label id="exp"></label></strong> <button class="btn btn-xs btn-success"> <i class="fas fa-pencil-alt" onclick="expE(id)"></i></button>
              <img src="" style="width: 100%;" id="fotoscan">
              <strong>Catatan:</strong><p class="modal-header" name="notif" id="notif"></p>
              <input hidden="" type="text" name="id" id="id">
              <input hidden="" type="text" name="nama" id="nama">
              <input type="text" name="info" id="info" class="form-control m-t-15" placeholder="Masukan Catatan">
            </div>
            <div class="modal-footer">
              <button type="button" onclick="catatan()" class="btn btn-default">Kirim</button>
              <button type="button" onclick="validated()" class="btn btn-primary">Validasi</button>
            </div>
          </div>
        </div>
      </div>

      <!-- modal izin -->
      <div id="izin-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
            <h4 class="modal-title" id="myModalLabel">Perbaiki Nomor Izin</h4>
          </div>
          <div class="modal-body">
            <input class="form-control" type="text" name="izinEdit" id="izinEdit">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" onclick="sipIzin(id)" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </div>
    </div>

    <!-- modal sk -->
    <div id="sk-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
          <h4 class="modal-title" id="myModalLabel">Perbaiki Nomor SK</h4>
        </div>
        <div class="modal-body">
          <input class="form-control" type="text" name="skEdit" id="skEdit">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" onclick="sipSK(id)" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
  <div id="exp-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
          <h4 class="modal-title" id="myModalLabel">Perbaiki Tanggal Kadaluarsa</h4>
        </div>
        <div class="modal-body">
          <input class="form-control" type="date" name="expEdit" id="expEdit">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" onclick="sipExp(id)" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="col-lg-12 ui-sortable">
  <?php
  $this->load->view('forum_list');
  ?>
</div>
</div>
<?php
$y = date('Y');
$tlt = $this->db->query("SELECT count(id) AS kabeh FROM sekolah_terdaftar WHERE valid='1'")->row();
$hf = $this->db->query("SELECT count(id) as ttl, EXTRACT(MONTH FROM tgl_aktivasi::date) as bulan FROM sekolah_terdaftar where left(tgl_aktivasi,4)='$y' and valid='1' group by bulan")->result();
$no = 0;
if (count($hf)>0) {
  foreach ($hf as $cuy) {
    $no++;
    echo "<input type='hidden' id='bln_".$no."' value='".$cuy->ttl."'/>";
  }
  echo "<input type='hidden' id='kabeh' value='".$tlt->kabeh."'/>";
}
?>
<script type="text/javascript">
  var kbh = $("#kabeh").val();
  var b1 = $("#bln_1").val();
  var b2 = $("#bln_2").val();
  var b3 = $("#bln_3").val();
  var b4 = $("#bln_4").val();
  var b5 = $("#bln_5").val();
  var b6 = $("#bln_6").val();
  var b7 = $("#bln_7").val();
  var b8 = $("#bln_8").val();
  var b9 = $("#bln_9").val();
  var b10 = $("#bln_10").val();
  var b11 = $("#bln_11").val();
  var b12 = $("#bln_12").val();
  var d = new Date();
  var n = d.getMonth();
  if (b1 == null) {
    b1='0';
  }
  if (b2 == null) {
    b2='0';
  }
  if (b3 == null) {
    b3='';
  }
  if (b4 == null) {
    b4='0';
  }
  if (b5 == null) {
    b5='0';
  }
  if (b6 == null) {
    b6='0';
  }
  if (b7 == null) {
    b7='0';
  }
  if (b8 == null) {
    b8='0';
  }
  if (b9 == null) {
    b9='0';
  }
  if (b10 == null) {
    b10='0';
  }
  if (b11 == null) {
    b11='0';
  }
  if (b12 == null) {
    b12='0';
  }
  var hsl = kbh-b1-b2-b3-b4-b5-b6-b7-b8-b9-b10-b11-b12;
  var ctx = document.getElementById('terdaftarChart').getContext('2d');
  var chart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
        label: 'BKK Terdaftar',
        borderColor: 'rgb(29, 71, 84)',
        data: [parseInt(hsl)+parseInt(b1),
        parseInt(hsl)+parseInt(b1)+parseInt(b2),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7)+parseInt(b8),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7)+parseInt(b8)+parseInt(b9),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7)+parseInt(b8)+parseInt(b9)+parseInt(b10),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7)+parseInt(b8)+parseInt(b9)+parseInt(b10)+parseInt(b11),

        parseInt(hsl)+parseInt(b1)+parseInt(b2)+parseInt(b3)+parseInt(b4)+parseInt(b5)+parseInt(b6)+parseInt(b7)+parseInt(b8)+parseInt(b9)+parseInt(b10)+parseInt(b11)+parseInt(b12)]
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      }
    }
  });
</script>