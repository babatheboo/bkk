<script src="<?php echo base_url();?>assets/backend/js/validasi.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<style type="text/css">
  .modal { overflow-y: auto !important; }
</style>
<div class="row">
  <div class="col-lg-3 col-md-6">
    <div class="widget widget-stats bg-red">
      <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-building"></i></div>
      <div class="stats-info">
        <h4 style="text-alingment:right">TOTAL BKK TERDAFTAR</h4>
        <p>
          <?php
          $skl = $this->db->query("SELECT count(sekolah_id) as jml from ref.sekolah")->row();
          $jm = $this->db->query("SELECT count(sekolah_id) as jumlah from sekolah_terdaftar where valid='1'")->result();
          foreach ($jm as $key) {
           echo number_format($key->jumlah) . ' <small>('. number_format($key->jumlah / $skl->jml * 100) .'%)</small>';
         }
         $bln = date('n');
         if($bln<7){
          $t1 = date('Y') - 3;
          $t2 = $t1+1;
          $t3 = $t1+2;
          $t4 = $t1+3;
        }else{
          $t1 = date('Y') - 3;
          $t2 = $t1+1;
          $t3 = $t1+2;
          $t4 = $t1+3;
        }
        ?>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-graduation-cap"></i></div>
    <div class="stats-info">
      <h4>JUMLAH LULUSAN (<?php echo $t1 .'-'. $t3;?>)</h4>
      <p>
        <?php
        $ttls = $this->db->query("SELECT count(peserta_didik_id) as jumlah from ref.peserta_didik where left(tanggal_keluar,4)>='$t1' AND left(tanggal_keluar,4)<='$t3'")->row();
        echo number_format($ttls->jumlah,0);
        ?>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="far fa-lg fa-fw m-r-10 fa-handshake"></i></div>
    <div class="stats-info">
      <h4>JUMLAH INDUSTRI</h4>
      <p>
       <?php
       $dt = $this->db->query("SELECT count(industri_id) as jumlah from ref.industri")->row();
       echo number_format($dt->jumlah,0);
       ?>

     </p>    
   </div>
 </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-red">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-briefcase"></i></div>
    <div class="stats-info">
      <h4>TOTAL LOWONGAN TERBUKA</h4>
      <p>
       <?php
       $jm = $this->db->query("select count(id) as jumlah from lowongan where sekolah_pembuat is not null")->result();
       foreach ($jm as $key) {
         echo number_format($key->jumlah);
       }
       ?>
     </p>    
   </div>
 </div>
</div>
<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-cog"></i></div>
    <div class="stats-info">
      <h4>JUMLAH BEKERJA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
        <?php
        $krj = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        echo number_format($krj->total,0);
        ?><br>
        <small><?php echo ' (' . number_format($krj->total / $ttls->jumlah * 100, 2) . '%' . ')' ?></small>
      </p>    
    </div>
  </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-user"></i></div>
    <div class="stats-info">
      <h4>JUMLAH WIRAUSAHA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
       <?php
       $wr = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
       echo number_format($wr->total,0);
       ?><br>
       <small><?php echo ' (' . number_format($wr->total / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
     </p>    
   </div>
 </div>
</div>

<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-info">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-graduation-cap"></i></div>
    <div class="stats-info">
      <h4>JUMLAH MELANJUTKAN</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
       <?php
       $kl = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
       echo number_format($kl->total,0);
       ?><br>
       <small><?php echo ' (' . number_format($kl->total / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
     </p>
   </div>
 </div>
</div>
<div class="col-lg-3 col-md-6">
  <div class="widget widget-stats bg-orange">
    <div class="stats-icon"><i class="fas fa-lg fa-fw m-r-10 fa-info-circle"></i></div>
    <div class="stats-info">
      <h4>TIDAK TERDATA</h4>
      <span><?php echo $t1 .'-'. $t3;?></span>
      <p>
        <?php
        $krj = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kerja_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $wira = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join wira_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $kul = $this->db->query("SELECT count(e.peserta_didik_id) as total from ref.peserta_didik a left join kuliah_siswa e on a.peserta_didik_id=e.peserta_didik_id where left(a.tanggal_keluar,4)>='$t1' and left(a.tanggal_keluar,4)<='$t3' order by total desc")->row();
        $ttd = $ttls->jumlah - $krj->total - $wira->total - $kul->total;
        echo number_format($ttd,0);
        ?><br>
        <small><?php echo ' (' . number_format($ttd / $ttls->jumlah * 100,2) . '%' . ')' ?></small>
      </p>
    </div>
  </div>
</div>
<div class="col-lg-12 ui-sortable">
  <div class="panel panel-inverse">
    <!-- begin panel-heading -->
    <div class="panel-heading ui-sortable-handle">
      <h4 class="panel-title">Grafik BKK Terdaftar Tahun <?php echo date('Y');?></h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
      <canvas id="myChart"></canvas>
    </div>
    <!-- end panel-body -->
  </div>
</div>
</div>
<?php
$y = date('Y');
$hf = $this->db->query("SELECT count(id) as ttl, EXTRACT(MONTH FROM tgl_aktivasi::date) as bulan FROM sekolah_terdaftar where left(tgl_aktivasi,4)='$y' and valid='1' group by bulan")->result();
$no = 0;
if (count($hf)>0) {
  foreach ($hf as $cuy) {
    $no++;
    echo "<input type='hidden' id='bln_".$no."' value='".$cuy->ttl."'/>";
  }
}
?>
<script type="text/javascript">
  var b1 = $("#bln_1").val();
  var b2 = $("#bln_2").val();
  var b3 = $("#bln_3").val();
  var b4 = $("#bln_4").val();
  var b5 = $("#bln_5").val();
  var b6 = $("#bln_6").val();
  var b7 = $("#bln_7").val();
  var b8 = $("#bln_8").val();
  var b9 = $("#bln_9").val();
  var b10 = $("#bln_10").val();
  var b11 = $("#bln_11").val();
  var b12 = $("#bln_12").val();
  var ctx = document.getElementById('myChart').getContext('2d');
  var chart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
        label: 'BKK Terdaftar',
        borderColor: 'rgb(29, 71, 84)',
        data: [b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12]
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>