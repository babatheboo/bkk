<div id="sidebar" class="sidebar">
  <div data-scrollbar="true" data-height="100%">
    <ul class="nav">
      <?php
      $lvl = $this->session->userdata('level');
      if($lvl=='1'){
        $this->db->query("refresh materialized view app.view_user_sekolah");
        $ckfoto = $this->db->get_where('sekolah_terdaftar',array('sekolah_id'=>$this->session->userdata('role_')));
        $xx = $ckfoto->row();
        $foto = $xx->logo;
      }else{
        $foto = "administrator.png";
      }
      ?>
      <li class="nav-profile">
        <div class="image1" style="text-align:center">
          <a class="fancybox" href="<?php echo base_url();?>assets/foto/bkk/<?php echo $foto;?>" style="width:80px;text-align:center;height:102px;" data-fancybox-group="gallery" title="<?php echo $this->session->userdata('nama_user');?>"><img src="<?php echo base_url();?>assets/foto/bkk/<?php echo $foto;?>" style="width:71px;" alt="" /></a>
        </div>
          <?php
          if ($this->session->userdata('level')==1) {
            $sid = $this->session->userdata('role_');
          $cks = $this->db->query("SELECT id FROM sekolah_terdaftar WHERE sekolah_id='$sid' AND valid='1' LIMIT 1")->result();
          if (count($cks)>0) {
            echo '<center><i class="fas fa-check-square text-primary"></i> Tervalidasi</center>';
          }else{
            echo '<center><i class="fas fa-minus-square text-danger"></i> Belum Tervalidasi.<br><a href="'.base_url().'validasi_bkk">Klik untuk validasi</a></center>';
          }
          }else{
            echo "";
          }
          ?>
        <div class="info" style="text-align:center">
          <?php 
          $uid = $this->session->userdata('user_id');
          $ds = $this->db->query("SELECT nama FROM app.user_sekolah WHERE user_id='$uid'");
          $dsiswa = $this->db->query("SELECT nama FROM ref.peserta_didik WHERE peserta_didik_id='$uid'");
          $dpsmk = $this->db->query("SELECT nama FROM app.user_psmk WHERE user_id='$uid'");
          $dkemnaker = $this->db->query("SELECT nama FROM app.user_kemenaker WHERE user_id='$uid'");
          $dindustri = $this->db->query("SELECT nama FROM app.user_industri WHERE user_id='$uid'");
          $ddinas = $this->db->query("SELECT nama FROM app.user_dinas WHERE user_id='$uid'");
          if(count($ds->result())>0){
            $key = $ds->row();
            echo $key->nama;
          }elseif(count($dsiswa->result())>0){
            $key = $dsiswa->row();
            echo $key->nama;
          }elseif(count($dpsmk->result())>0){
            $key = $dpsmk->row();
            echo $key->nama;
          }elseif(count($dkemnaker->result())>0){
            $key = $dkemnaker->row();
            echo $key->nama;
          }elseif(count($dindustri->result())>0){
            $key = $dindustri->row();
            echo $key->nama;
          }elseif(count($ddinas->result())>0){
            $key = $ddinas->row();
            echo $key->nama;
          }else{
            echo "Superadmin";
          }
          ?>   
          <small><?php echo $this->session->userdata('nama_');?></small>
        </div>
      </li>
    </ul>
    <ul class="nav">
      <li class="nav-header"><div style="text-align:center"><span id="date_time"><script type="text/javascript">window.onload = date_time('date_time');</script></span></div></li>
      <li <?php if($page=="dashboard"){echo "class=\"active\"";}?>><a href="<?php echo base_url();?>dashboard"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
<!--             <li><a href="<?php echo base_url();?>banner"><i class="fa fa-flag"></i> <span>Banner</span></a></li>
  <li><a href="<?php echo base_url();?>iklan"><i class="fa fa-bullhorn"></i> <span>Iklan</span></a></li> -->
  <?php
  $lv = $this->session->userdata('level');
  if($lv=="0" || $lv=="4")
  {
    $user_menu = $this->db->query("SELECT * FROM app.menu WHERE level = '$lv' OR level = '9' AND status = '1' ORDER BY urutan ASC")->result();
  }
  else
  {
    $user_menu = $this->db->query("SELECT * FROM app.menu WHERE level = '$lv' AND status = '1' ORDER BY urutan ASC")->result();
  }
  foreach($user_menu as $menu) :
    $parent  = $menu->menu_id;
    $kelas   = $menu->kelas;
    $has_sub = $this->db->get('app.sub_menu')->num_rows()>0;
    $active  = stripos($this->uri->uri_string, $menu->kelas) !== FALSE ? " active" : "";
    $link    = $menu->link == '#' ? $menu->link : site_url($menu->link); ?>
    <li 
    <?php if($kelas == $page)
    {
      if($menu->link=="#")
      {
        echo 'class = "has-sub active"';
      }
      else
      {
        echo 'class = "has-sub"';
      }
    }
    else
    {
      if($menu->link=="#")
      {
        echo 'class = "has-sub"';
      }
      else
      {
        echo 'class = ""';
      }
    }
    ?>
    >
    <a href='<?php echo $link;?>'>
      <?php
      if($menu->link=="#")
      {
        echo "<b class='caret pull-right'></b>";
      }
      else
      {
        echo "<b></b>";
      }
      ?>
      <i class="<?php echo $menu->icon;?>"></i>
      <span><?php echo $menu->nama_menu;?></span>
    </a>
    <?php if ($has_sub) : ?>
      <ul class="sub-menu">
        <?php 
        $submenu = $this->db->query("SELECT * FROM app.sub_menu WHERE parent = '$parent' AND sstatus = '1'")->result();
        foreach($submenu as $submenu) :
          $slink  = $submenu->slink == '#' ? $submenu->slink : site_url($submenu->slink); ?>
          <li <?php echo 'class="has-sub active"';?>>
            <a href='<?php echo $slink;?>'>
              <i class="<?php echo $submenu->sicon;?>"></i>
              <span><?php echo $submenu->nama_smenu;?></span>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </li>
<?php endforeach; 
if($this->session->userdata('level')=="1")
  { ?>
   <!--            <li <?php if($page=='bidik'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>bidikmisi"><i class="fa fa-bullseye"></i> <span>Bidik Misi<span class="label" style='color:yellow'><sup style="font-size:10px"> Beta</sup></span></span></a></li>        -->
   <!-- <li <?php if($page=='tentang'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>tentang"><i class="fa fa-exclamation-triangle"></i> <span>Tentang Program</span></a></li> -->
   <li <?php if($page=='tentang'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>lapbkk"><i class="fa fa-print"></i> <span>Laporan</span></a></li>

   <?php
 }
 ?>
 <?php
 if ($this->session->userdata('level')!='4') {
   ?>
   <li <?php if($page=='Bantuan'){ echo 'class="active"';}?>><a href="<?php echo base_url();?>help"><i class="fa fa-question-circle"></i> <span>Bantuan</span></a></li>
   <?php
 }
 ?>
 <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
</ul>
</div>
</div>
