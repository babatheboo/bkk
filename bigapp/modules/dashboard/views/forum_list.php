<div class="panel panel-forum">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Official Lasjurin</h4>
                </div>
                <?php
                $kf1 = $this->db->query("SELECT * FROM kategori_forum WHERE jenis='1'")->result();

                ?>
                <ul class="forum-list">
                <?php
                    foreach ($kf1 as $key) {
                        $idnya = $this->asn->dec_enc("encrypt", $key->id);
                ?>
                    <li>
                        <!-- begin media -->
                        <div class="media">
                            <img src="<?php echo base_url();?>assets/backend/img/icon/<?php echo $key->icon;?>" alt="">
                        </div>
                        <div class="info-container">
                            <div class="info">
                                <h4 class="title"><a href="<?php echo base_url();?>forum/kategori/<?php echo $idnya;?>"><?php echo $key->nama_kategori;?></a></h4>
                                <p class="desc">
                                    <?php echo $key->keterangan;?>
                                </p>
                            </div>
                            <?php
                                $x = $this->db->query("SELECT * FROM post_forum where id_kategori='$key->id' order by id desc limit 1")->result();
                                if(count($x)>0) {
                                    $tgl = "";
                                foreach ($x as $dat) {
                                    $post = $dat->tgl_dibuat;
                                    $tglPost = new DateTime($post);
                                    $now = new DateTime(date("Y-m-d H:i:s"));
                                    $intv = $tglPost->diff($now);
					               $idna = $this->asn->dec_enc("encrypt", $dat->id);
                                ?>
                                    <div class="total-count">
                                        <span class="total-post"><?php echo $this->db->query("SELECT * FROM post_forum WHERE id_kategori='$key->id'")->num_rows(); ?></span> <span class="divider">/</span> <span class="total-comment"><?php echo $this->db->query("SELECT * FROM post_komen where id_kategori='$key->id'")->num_rows();?></span>
                                    </div>
                                    <div class="latest-post">
                                        <h4 class="title"><a href="<?php echo base_url();?>forum/detil_forum/<?php echo $idna;?>"><?php echo 
$dat->judul;?></a></h4>
                                        <p class="time"><i><?php echo $intv->format('%h jam %i menit lalu');?></i><b class="user"> | 
                                            <?php 

                                            if($dat->dibuat_oleh=='c12720c7-12bc-4f44-bada-17abfe07f88a'){
                                                echo "Administrator";
                                            } else {
                                                $ck = $this->db->query("SELECT nama from ref.sekolah where sekolah_id = '".$dat->dibuat_oleh."'")->row();
                                                echo $ck->nama;
                                            }
                                            
                                            ?></b></p>
                                    </div>
                                    <?php
                                    }
                             ?>   
                                </div>
                    <?php } else { ?>
                                    <div class="total-count">
                                        <span class="total-post">0</span> <span class="divider">/</span> <span class="total-comment">0</span>
                                    </div>
                                    <div class="latest-post">
                                        Belum ada diskusi
                                    </div>
                    <?php } ?>
                 </li>
                 <?php } ?>
                </ul>
</div>

<div class="panel panel-forum">
                <!-- begin panel-heading -->
                <div class="panel-heading">
                    <h4 class="panel-title">Forum diskusi BKK</h4>
                </div>
                <?php
                $kf1 = $this->db->query("SELECT * FROM kategori_forum WHERE jenis='2'")->result();

                ?>
                <ul class="forum-list">
                <?php
                    foreach ($kf1 as $key) {
                         $idnya = $this->asn->dec_enc("encrypt", $key->id);
                ?>
                    <li>
                        <!-- begin media -->
                        <div class="media">
                            <img src="<?php echo base_url();?>assets/backend/img/icon/<?php echo $key->icon;?>" alt="">
                        </div>
                        <div class="info-container">
                            <div class="info">
                                <h4 class="title"><a href="<?php echo base_url();?>forum/kategori/<?php echo $idnya;?>"><?php echo $key->nama_kategori;?></a></h4>
                                <p class="desc">
                                    <?php echo $key->keterangan;?>
                                </p>
                            </div>
                            <?php
                                $x = $this->db->query("SELECT * FROM post_forum where id_kategori='$key->id' order by id desc limit 1")->result();
                                if(count($x)>0) {
                                    $tgl = "";
                                foreach ($x as $dat) {
                                    $post = $dat->tgl_dibuat;
                                    $tglPost = new DateTime($post);
                                    $now = new DateTime(date("Y-m-d H:i:s"));
                                    $intv = $tglPost->diff($now);
					$idna = $this->asn->dec_enc("encrypt", $dat->id);
                                ?>
                                    <div class="total-count">
                                        <span class="total-post"><?php echo $this->db->query("SELECT * FROM post_forum WHERE id_kategori='$key->id'")->num_rows(); ?></span> <span class="divider">/</span> <span class="total-comment"><?php echo $this->db->query("SELECT * FROM post_komen where id_kategori='$key->id'")->num_rows();?></span>
                                    </div>
                                    <div class="latest-post">
                                        <h4 class="title"><a href="<?php echo base_url();?>forum/detil_forum/<?php echo $idna;?>"><?php echo 
$dat->judul;?></a></h4>
                                        <p class="time"><i><?php echo $intv->format('%h jam %i menit lalu');?></i><b class="user"> | 
                                            <?php $ck = $this->db->query("SELECT nama from ref.sekolah where sekolah_id = '".$dat->dibuat_oleh."'")->row();
                                            echo $ck->nama;
                                            ?></b></p>
                                    </div>
                                    <?php
                                    }
                             ?>   
                                </div>
                    <?php } else { ?>
                                    <div class="total-count">
                                        <span class="total-post">0</span> <span class="divider">/</span> <span class="total-comment">0</span>
                                    </div>
                                    <div class="latest-post">
                                        Belum ada diskusi
                                    </div>


                    <?php } ?>
                 </li>
                 <?php } ?>
                  
                </ul>
</div>


