<div class="row">
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-university"></i></div>
			<div class="stats-info">
				<?php
				$sid = $this->session->userdata('role_');
				$kdp = $this->db->query("SELECT prov FROM app.user_dinas WHERE user_id = '$sid'")->row();
				$ckd = $this->db->query("SELECT * FROM ref.view_provinsi WHERE kode_prov='$kdp->prov'")->row();
				?>
				<h4 style="text-alingment:right">TOTAL BKK DI <br><?php echo strtoupper($ckd->prov);?></h4>
				<p>
					<?php
					$jm = $this->db->query("SELECT count(b.sekolah_id) from ref.view_sekolah_wilayah b left join sekolah_terdaftar a on b.sekolah_id=a.sekolah_id where a.valid='1' and b.kode_prov='kdp->prov'")->row();
					echo number_format($jm->count);
					?>
				</p>    
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				<h4>JUMLAH LULUSAN DI <br><?php echo strtoupper($ckd->prov);?></h4>
				<p>
					<?php
					$dt = $this->db->query("SELECT count(b.peserta_didik_id) from ref.view_sekolah_wilayah a left join ref.peserta_didik b
						on a.sekolah_id = b.sekolah_id WHERE kode_prov='$kdp->prov'")->row();
					echo number_format($dt->count,0);
					?>
				</p>    
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-building"></i></div>
			<div class="stats-info">
				<h4>JUMLAH INDUSTRI DI <br><?php echo strtoupper($ckd->prov); ?></h4>
				<p>
					<?php
					$dt = $this->db->query("SELECT count(industri_id) as jumlah from ref.industri where kode_prov='$kdp->prov'")->row();
					echo number_format($dt->jumlah,0);
					?>

				</p>    
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-sitemap"></i></div>
			<div class="stats-info">
				<h4>TOTAL LOWONGAN TERBUKA DI <br><?php echo strtoupper($ckd->prov);?></h4>
				<p>
					<?php
					$jm = $this->db->query("SELECT count(id) as jumlah from lowongan where sekolah_pembuat is not null")->result();
					foreach ($jm as $key) {
						echo number_format($key->jumlah);
					}
					?>
				</p>    
			</div>
		</div>
	</div>
</div>