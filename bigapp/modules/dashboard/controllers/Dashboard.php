<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('dashboard_model');
    $this->load->model('validasi_model');
    $this->load->model('dinas_model');
    $this->load->model('dinasw_model');
    $this->load->model('dinask_model');
    $this->load->model('bkk_dinas_model');

  }
  
  public function index(){
   if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=="0" || $this->session->userdata("level")=="1" || $this->session->userdata("level")=="4" || $this->session->userdata("level")=="5" || $this->session->userdata("level")=="6" || $this->session->userdata("level")=="7"){
     $isi['namamenu'] = "";
     $isi['page']     = "dashboard";
     $isi['kelas']    = "dashboard";
     $isi['link']     = 'dashboard';
     $isi['halaman']  = "Dashboard";
     $isi['judul']    = "Halaman Dashboard";
     $isi['now']      = date("Y-m-d");
     $level           = $this->session->userdata('level');
     switch ($level) {
      case '4': //superadmin
      $isi['content'] = "admpsmk";
      break;
      case '1': //admin bkk
          // $sess                        = $this->session->userdata('role_');
      $isi['option_kat_loker'][''] = "Semua Kategori";
//        $sid = $this->session->userdata('role_');
//        $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
//        $valid = "";
//        foreach ($b as $key) {
//          $valid = $key->valid;
//        }
//        if($valid=="1"){
      $isi['content'] = "adminbkk";
//        } else {
//          $isi['content']              = "admin_bkk";
//        }
      break;
      case '5':
      $isi['content'] = "admprov";
      break;
      case '6':
      $isi['content'] = 'admforum';
      break;
      case '7':
      $isi['content'] = 'tamu';
      break;
    }
    $this->load->view("dashboard/dashboard_view",$isi);
  }else{
    redirect('login','refresh');
  }
}

public function getValid(){
  if($this->input->is_ajax_request()){
    $list = $this->validasi_model->get_datatables();
    $data = array();
    $no   = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row    = array();
      $row[]  = $no . ".";
      $row[] = $rowx->npsn;
      $row[]  = $rowx->nama;
      $row[]  = $rowx->no_izin_bkk;
      $row[]  = $rowx->no_sk_pendirian;
      $row[]  = date('d-m-Y', strtotime($rowx->tgl_upload_ulang));
          // $row[]  = bulan_indo($tuu);
      if ($rowx->tgl_kadaluarsa=='' || $rowx->tgl_kadaluarsa==NULL) {
        $tgl = 'x';
      }else{
        $tgl = $rowx->tgl_kadaluarsa;
      }
      switch ($rowx->notif) {
        case '':
        $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" data-toggle="modal" title="Validasi" onclick="validate(\''. $rowx->scan .'\' , \''. $rowx->no_izin_bkk .'\' , \''. $rowx->no_sk_pendirian .'\' , \''. $rowx->id .'\' , \''. $rowx->nama .'\' , \''. $rowx->notif .'\' , \''. $tgl .'\')"><i 
        class="fa fa-eye"></i></a>';
        // if ($rowx->no_izin_bkk=='' && $rowx->no_sk_pendirian=='' && $rowx->tgl_kadaluarsa=='') {
          
        // }else{
          
        // }
        break;
        
        default:
        $row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" data-toggle="modal" title="Validasi" onclick="validate(\''. 
        $rowx->scan .'\' , \''. $rowx->no_izin_bkk .'\' , \''. $rowx->no_sk_pendirian .'\' , \''. $rowx->id .'\' , \''. $rowx->nama .'\' , \''. $rowx->notif .'\' , \''. $tgl .'\')"><i 
        class="fa fa-eye"></i></a>' . '<i class="fas fa-check-square"></i>';
        break;
      }
      $data[] = $row;
    }
    $output = array(
      "draw"            => $_POST['draw'],
      "recordsTotal"    => $this->validasi_model->count_all(),
      "recordsFiltered" => $this->validasi_model->count_filtered(),
      "data"            => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }
}

public function validasiData($id){
  if($this->input->is_ajax_request()){
    if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="4"){
     $skrng = date('Y-m-d');
     $vld = $this->session->userdata('role_');
     if($this->db->query("UPDATE sekolah_terdaftar set valid='1', tgl_aktivasi='$skrng', approved_by='c12720c7-12bc-4f44-bada-17abfe07f88a' WHERE id='$id'")){
      $resp = "ok";
    }   else {
      $resp = "no";
    }
    echo json_encode($resp);
  } else {
    redirect('_404','refresh');
  }
}
}

public function catatan($id){
  if($this->input->is_ajax_request()){
    if($this->session->userdata('login')==TRUE && $this->session->userdata("level")=="4" || $this->session->userdata("level")=="6"){
      $info = $this->input->post('note');
      if($this->db->query("UPDATE sekolah_terdaftar set notif='$info' WHERE id='$id'")){
        $resp = "ok";
      }   else {
        $resp = "no";
      }
      echo json_encode($resp);
    } else {
      redirect('_404','refresh');
    }
  }
}

public function proses_edit(){
  if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=="0" || $this->session->userdata("level")=="1" || $this->session->userdata("level")=="4"){
    $izin = $this->input->post('no_izin');
    $sk = $this->input->post('sk_pendirian');
    $exp = date('Y-m-d', strtotime($this->input->post('expired')));
    $sc = $_FILES['scan']['name'];
    if ($izin == "") {
      ?>
      <script type="text/javascript">
        alert("Pastikan nomor izin bkk diisi");
        window.location.href="<?php echo base_url();?>validasi_bkk";
      </script>
      <?php
    }elseif ($sk == "") {
      ?>
      <script type="text/javascript">
        alert("Pastikan nomor SK pendirian diisi");
        window.location.href="<?php echo base_url();?>validasi_bkk";
      </script>
      <?php
    }elseif ($exp == "") {
      ?>
      <script type="text/javascript">
        alert("Pilih tanggal berakhir izin BKK");
        window.location.href="<?php echo base_url();?>validasi_bkk";
      </script>
      <?php
    }elseif ($sc == "") {
      ?>
      <script type="text/javascript">
        alert("Pastikan scan dokumen sudah diupload");
        window.location.href="<?php echo base_url();?>validasi_bkk";
      </script>
      <?php
    }else{
      $acakx=rand(00,99);
      $bersihx = $_FILES['scan']['name'];
      $nmx = str_replace(" ","_","$bersihx");
      $pisahx = explode(".",$nmx);
      $nama_murnix = $pisahx[0];
      $ubahx = $acakx.$nama_murnix;
      $nama_flx = $acakx.$nmx;
      $tmpNamex = str_replace(" ", "_", $_FILES['scan']['name']);
      $nmxfilex = "file_".time();
      if($tmpNamex!=''){
       $acakx=rand(00,99);
       $bersihx = $_FILES['scan']['name'];
       $nmx = str_replace(" ","_","$bersihx");
       $pisahx = explode(".",$nmx);
       $nama_murnix = $pisahx[0];
       $ubahx = $acakx.$nama_murnix;
       $nama_flx = $acakx.$nmx;
       $tmpNamex = str_replace(" ", "_", $_FILES['scan']['name']);
       $nmxfilex = "file_".time();
       if($tmpNamex!=''){
        $config['file_name'] = $nmxfilex;
        $config['upload_path'] = './assets/foto/scan';
        $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg|JPEG';
        $config['max_size'] = '2048';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['overwrite'] = TRUE;
        $this->upload->initialize($config);
        if ($this->upload->do_upload('scan')){
          $gbr = $this->upload->data(); 
          $update_izin = $this->asn->anti($this->input->post('no_izin'));
          $update_sk = $this->asn->anti($this->input->post('sk_pendirian'));
          $skrng = date('Y-m-d');
          $this->hapusfoto_scan($this->session->userdata('role_'));
          $simpandata = array('scan'=>$gbr['file_name'], 'no_izin_bkk'=>$update_izin, 'no_sk_pendirian'=>$update_sk, 'notif'=>'', 'tgl_upload_ulang'=>$skrng, 'tgl_kadaluarsa'=>$exp, 'valid'=>'0');
          $this->db->where('sekolah_id',$this->session->userdata('role_'));
          $this->db->update('sekolah_terdaftar',$simpandata);
          redirect('validasi_bkk','refresh');
        }else{
          ?>
          <script type="text/javascript">
            alert("Ukuran file tidak boleh lebih dari 1MB");
            window.location.href="<?php echo base_url();?>validasi_bkk";
          </script>
          <?php
          echo $this->upload->display_errors();
        }
      }
    }
  }
} else {
  ?>
  <script type="text/javascript">
    alert("Pastikan Type File jpg dan ukuran file maksimal 1mb");
    window.location.href="<?php echo base_url();?>validasi_bkk";
  </script>
  <?php
}
}

public function cekIzin($id){
  if($this->input->is_ajax_request()){
    $ckdata = $this->db->get_where('sekolah_terdaftar',array('id'=>$id))->result();
    $data['wkwk']='';
    $data['no_izin']='';
    foreach ($ckdata as $key) {
      $data['wkwk']    =$id;
      $data['no_izin']=$key->no_izin_bkk;
    }
    echo json_encode($data);
  }else{
    redirect("_404","refresh");
  }
}

public function saveIzin($id){
  $ie = $this->input->post('wkwk');
  if ($this->db->query("UPDATE sekolah_terdaftar set no_izin_bkk='$ie' WHERE id='$id'")) {
    $data['response'] = "true";
  }else{
    $data['response'] = "false";
  }
  echo json_encode($data);
}

public function cekSK($id){
  if($this->input->is_ajax_request()){
    $ckdata = $this->db->get_where('sekolah_terdaftar',array('id'=>$id))->result();
    $data['hehe']='';
    $data['no_sk']='';
    foreach ($ckdata as $key) {
      $data['hehe']    =$id;
      $data['no_sk']=$key->no_sk_pendirian;
    }
    echo json_encode($data);
  }else{
    redirect("_404","refresh");
  }
}

public function saveSK($id){
  $se = $this->input->post('hehe');
  if ($this->db->query("UPDATE sekolah_terdaftar set no_sk_pendirian='$se' WHERE id='$id'")) {
    $data['response'] = "true";
  }else{
    $data['response'] = "false";
  }
  echo json_encode($data);
}

public function cekExp($id){
  if($this->input->is_ajax_request()){
    $ckdata = $this->db->get_where('sekolah_terdaftar',array('id'=>$id))->result();
    $data['haha']='';
    $data['kadaluarsa']='';
    foreach ($ckdata as $key) {
      $data['haha']    =$id;
      $data['kadaluarsa']=$key->tgl_kadaluarsa;
    }
    echo json_encode($data);
  }else{
    redirect("_404","refresh");
  }
}

public function saveExp($id){
  $sat = date('Y-m-d', strtotime($this->input->post('haha')));
  if ($this->db->query("UPDATE sekolah_terdaftar set tgl_kadaluarsa='$sat' WHERE id='$id'")) {
    $data['response'] = "true";
  }else{
    $data['response'] = "false";
  }
  echo json_encode($data);
}

function hapusfoto($x){
  $ckdata = $this->db1->query("SELECT * FROM sekolah_logo WHERE sekolah_id = '$x'");
  if(count($ckdata->result())>0){
    $hehe = $ckdata->row();
    $fotona = $hehe->logo;
    if($fotona!='no.jpg'){
      unlink('./assets/foto/bkk/' . $fotona);
    }
  }
}

function hapusfoto_scan($x){
  $ckdata = $this->db->query("SELECT * FROM sekolah_logo WHERE sekolah_id = '$x'");
  if(count($ckdata->result())>0){
    $hehe = $ckdata->row();
    $fotona = $hehe->scan;
    if($fotona!='no.jpg'){
      unlink('./assets/foto/scan/' . $fotona);
    }
  }
}


function getKerjaProv(){
  if($this->input->is_ajax_request()){
    $this->db->query("REFRESH MATERIALIZED VIEW mview_kerja_prov");
    $list = $this->dinas_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->nama_sekolah;
      $row[] = $rowx->kota;
      $row[] = number_format($rowx->bekerja);
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->dinas_model->count_all(),
      "recordsFiltered" => $this->dinas_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }
}

function getWiraProv(){
  if($this->input->is_ajax_request()){
    // $this->db->query("REFRESH MATERIALIZED VIEW mview_wira_prov");
    $list = $this->dinasw_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->nama;
      $row[] = $rowx->kota;
      $row[] = number_format($rowx->wira);
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->dinasw_model->count_all(),
      "recordsFiltered" => $this->dinasw_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }
}

function getKulProv(){
  if($this->input->is_ajax_request()){
    $this->db->query("REFRESH MATERIALIZED VIEW mview_kuliah_prov");
    $list = $this->dinask_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->nama_sekolah;
      $row[] = $rowx->kota;
      $row[] = number_format($rowx->kuliah);
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->dinask_model->count_all(),
      "recordsFiltered" => $this->dinask_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }
}

function getBKKProv(){
  if($this->input->is_ajax_request()){
    $list = $this->bkk_dinas_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $rowx) {
      $no++;
      $row = array();
      $row[] = $no . ".";
      $row[] = $rowx->npsn;
      $row[] = $rowx->nama;
      $row[] = $rowx->kota;
      $data[] = $row;
    }
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->bkk_dinas_model->count_all(),
      "recordsFiltered" => $this->bkk_dinas_model->count_filtered(),
      "data" => $data,
    );
    echo json_encode($output);
  }else{
    redirect("_404","refresh");
  }
}

function expBKK(){
  $this->load->library('excel');
  $uid = $this->session->userdata('user_id');
  $dt = $this->db->query("SELECT * FROM app.user_dinas WHERE user_id='$uid'")->row();
  $kdp = $dt->kode_prov;
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data BKK")
  ->setSubject("Terdaftar")
  ->setDescription("Laporan BKK Terdaftar")
  ->setKeywords("Data BKK");
  $style_col = array('font' => array('bold' => true),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA BKK TERDAFTAR");
  $excel->getActiveSheet()->mergeCells('A1:D1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "NPSN");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "SMK");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Kota/Kabupaten");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $bkk = $this->db->query("SELECT * FROM mview_bkk_prov WHERE kode_prov='$kdp' ORDER BY kota ASC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
    $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->npsn);
    $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama);
    $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->kota);
    $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
    $no++;
    $numrow++;
  }
  $excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
  $excel->getActiveSheet()->getColumnDimension('B')->setWidth(9);
  $excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
  $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
  $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
  $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $excel->getActiveSheet(0)->setTitle("Laporan BKK Terdaftar");
  $excel->setActiveSheetIndex(0);
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  ob_end_clean();
  header('Content-Disposition: attachment;filename="Laporan BKK Terdaftar.xlsx"');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
  header ('Cache-Control: cache, must-revalidate');
  header ('Pragma: public');
  $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
  // ob_end_clean();
  $writer->save('php://output');
  exit;
}

function expWork(){
  $bln = date('n');
  if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }else{
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }
  $uid = $this->session->userdata('user_id');
  $dt = $this->db->query("SELECT * FROM app.user_dinas WHERE user_id='$uid'")->row();
  $kdp = $dt->kode_prov;
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data Bekerja")
  ->setSubject("Kebekerjaan")
  ->setDescription("Laporan Kebekerjaan")
  ->setKeywords("Data Bekerja");
  $style_col = array('font' => array('bold' => true),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA KEBEKERJAAN");
  $excel->getActiveSheet()->mergeCells('A1:E1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "SMK");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Kota/Kabupaten");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Tahun");
  $excel->setActiveSheetIndex(0)->setCellValue('E3', "Jumlah");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
  $sid = $this->session->userdata('role_');
  $bkk = $this->db->query("SELECT nama_sekolah, kota, sum(total_bekerja) AS jml FROM mview_kerja_prov WHERE kode_prov='$kdp' AND tahun>='$t1' AND tahun<='$t3' GROUP BY nama_sekolah, kota ORDER BY jml DESC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
    $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama_sekolah);
    $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->kota);
    $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $t1 .'-'. $t3);
    $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($data->jml,0));
    $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
    $no++;
    $numrow++;
  }
  $excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
  $excel->getActiveSheet()->getColumnDimension('B')->setWidth(55);
  $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
  $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
  $excel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
  $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
  $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $excel->getActiveSheet(0)->setTitle("Laporan Kebekerjaan");
  $excel->setActiveSheetIndex(0);
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  // ob_end_clean();
  header('Content-Disposition: attachment;filename="Laporan Kebekerjaan.xlsx"');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
  header ('Cache-Control: cache, must-revalidate');
  header ('Pragma: public');
  $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
  $writer->save('php://output');
  exit;
}

function expEntr(){
  $bln = date('n');
  if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }else{
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }
  $uid = $this->session->userdata('user_id');
  $dt = $this->db->query("SELECT * FROM app.user_dinas WHERE user_id='$uid'")->row();
  $kdp = $dt->kode_prov;
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data Wirausaha")
  ->setSubject("Kewirausahaan")
  ->setDescription("Laporan Kewirausahaan")
  ->setKeywords("Data Wirausaha");
  $style_col = array('font' => array('bold' => true),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA KEWIRAUSAHAAN");
  $excel->getActiveSheet()->mergeCells('A1:E1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "SMK");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Kota/Kabupaten");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Tahun");
  $excel->setActiveSheetIndex(0)->setCellValue('E3', "Jumlah");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
  $sid = $this->session->userdata('role_');
  $bkk = $this->db->query("SELECT nama_sekolah, kota, sum(total_wira) AS jml FROM mview_wira_prov WHERE kode_prov='$kdp' AND tahun>='$t1' AND tahun<='$t3' GROUP BY nama_sekolah, kota ORDER BY jml DESC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
    $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama_sekolah);
    $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->kota);
    $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $t1 .'-'. $t3);
    $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($data->jml,0));
    $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
    $no++;
    $numrow++;
  }
  $excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
  $excel->getActiveSheet()->getColumnDimension('B')->setWidth(55);
  $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
  $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
  $excel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
  $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
  $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $excel->getActiveSheet(0)->setTitle("Laporan Kewirausahaan");
  $excel->setActiveSheetIndex(0);
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  // ob_end_clean();
  header('Content-Disposition: attachment;filename="Laporan Kewirausahaan.xlsx"');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
  header ('Cache-Control: cache, must-revalidate');
  header ('Pragma: public');
  $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
  $writer->save('php://output');
  exit;
}

function expColg(){
  $bln = date('n');
  if($bln<7){
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }else{
    $t1 = date('Y') - 3;
    $t2 = $t1+1;
    $t3 = $t1+2;
    $t4 = $t1+3;
  }
  $uid = $this->session->userdata('user_id');
  $dt = $this->db->query("SELECT * FROM app.user_dinas WHERE user_id='$uid'")->row();
  $kdp = $dt->kode_prov;
  $this->load->library('excel');
  $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
  $cacheSettings = array( ' memoryCacheSize ' => '128MB');
  PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
  $excel = new PHPExcel();
  $excel->getProperties()->setCreator('NAA')
  ->setLastModifiedBy('NAA')
  ->setTitle("Data Kuliah")
  ->setSubject("Melanjutkan")
  ->setDescription("Laporan Melanjutkan")
  ->setKeywords("Data Kuliah");
  $style_col = array('font' => array('bold' => true),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $style_row = array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),
    'borders' => array('top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
      'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)));
  $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA MELANJUTKAN");
  $excel->getActiveSheet()->mergeCells('A1:E1');
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE);
  $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15);
  $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $excel->setActiveSheetIndex(0)->setCellValue('A3', "No");
  $excel->setActiveSheetIndex(0)->setCellValue('B3', "SMK");
  $excel->setActiveSheetIndex(0)->setCellValue('C3', "Kota/Kabupaten");
  $excel->setActiveSheetIndex(0)->setCellValue('D3', "Tahun");
  $excel->setActiveSheetIndex(0)->setCellValue('E3', "Jumlah");
  $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
  $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
  $sid = $this->session->userdata('role_');
  $bkk = $this->db->query("SELECT nama_sekolah, kota, sum(total_kuliah) AS jml FROM mview_kuliah_prov WHERE kode_prov='$kdp' AND tahun>='$t1' AND tahun<='$t3' GROUP BY nama_sekolah, kota ORDER BY jml DESC")->result();
  $no = 1;
  $numrow = 4;
  foreach($bkk as $data){
    $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama_sekolah);
    $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->kota);
    $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $t1 .'-'. $t3);
    $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($data->jml,0));
    $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
    $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
    $no++;
    $numrow++;
  }
  $excel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
  $excel->getActiveSheet()->getColumnDimension('B')->setWidth(55);
  $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
  $excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
  $excel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
  $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
  $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $excel->getActiveSheet(0)->setTitle("Laporan Melanjutkan");
  $excel->setActiveSheetIndex(0);
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  // ob_end_clean();
  header('Content-Disposition: attachment;filename="Laporan Melanjutkan.xlsx"');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
  header ('Cache-Control: cache, must-revalidate');
  header ('Pragma: public');
  $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
  $writer->save('php://output');
  exit;
}
}
