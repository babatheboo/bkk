<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
        "bProcessing": true,
        "serverSide" : true,
        "ajax" : {
            url : "<?php echo base_url();?>bidikmisi/show",
            type : "post",
            error : function(){
                $("#example").css("display","none");
            }
        },
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? val : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    });
});
</script>
<div class="row">
<div class="panel panel-inverse" data-sortable-id="table-basic-6">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Pilih Siswa</h4>
    </div>
    <div class="panel-body">
       <table id="example" class="table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>NISN</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th width="2%">Pilih</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>NISN</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th width="2%">Pilih</th>
            </tr>
        </tfoot>
        
        <tbody>
        </tbody>       
        </table>
    </div>
</div>
</div>

