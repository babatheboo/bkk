<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bidik_model extends CI_Model {
    var $table = 'view_bidik';
    var $table_ = 'bidik_siswa';
    var $column_order = array('bidik_id','nisn','nama','tempat_lahir','tanggal_lahir','nama_ibu_kandung','status',null);
    var $column_search = array('nisn','nama'); 
    var $order = array('bidik_id' => 'asc');

    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    private function _get_datatables_query(){
        $this->db->from($this->table);
        $this->db->where('sekolah_id',$this->session->userdata('role_'));
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                }else{
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table_);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $this->db->from($this->table_);
        $this->db->where('bidik_id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function simpan($data){
        $this->db->insert($this->table_, $data);
        return $this->db->insert_id();
    }
    public function update($where, $data){
        $this->db->update($this->table_, $data, $where);
        return $this->db->affected_rows();
    }
    public function hapus_by_id($id){
        $this->db->where('bidik_id', $id);
        $this->db->delete($this->table);
    }
    public function hapus_user_id($id){
        $this->db->where('bidik_id', $id);
        $this->db->delete($this->table_);
    }

    function getData($datanya,$jurusan)
    {
        $qry =  $this->db1->query("SELECT * FROM view_siswa_terdaftar WHERE nama like '%$datanya%' AND jurusan_id='$jurusan'");
        return $qry->result();
    }


}