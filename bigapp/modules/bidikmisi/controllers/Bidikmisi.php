<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bidikmisi extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login();
      	$this->asn->role_akses("1");
  		$this->load->model('bidikmisi/bidik_model');
  		$this->db1 = $this->load->database('default', TRUE);
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['page'] = "bidik";
		$isi['link'] = 'bidikmisi';
		$isi['halaman'] = "Bidik Misi <sup style='color:red;text-size:12px'>Beta</sup>";
		$isi['judul'] = "";
		$isi['content'] = "bidik_view";
		$isi['option_jurusan'][''] = "Pilih Jurusan";
		$sk = $this->session->userdata('role_');
		$jurusan = $this->db1->query("SELECT * FROM ref.jurusan_sp WHERE sekolah_id='$sk'")->result();
		if(count($jurusan)>0){
			foreach ($jurusan as $row) {
				$isi['option_jurusan'][trim($row->jurusan_id)] = $row->nama_jurusan_sp;
			}
		}else{
			$isi['option_jurusan'][''] = "Jurusan Belum Tersedia";
		}
       	$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->bidik_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->nisn;
				$row[] = $rowx->nama;
				$row[] = $rowx->tempat_lahir;
				$row[] = $rowx->tanggal_lahir;
				$row[] = $rowx->nama_ibu_kandung;
				$row[] = $rowx->status;
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'user_bkk\',\'edit_data\','."'".$rowx->bidik_id."'".')"><i class="icon-pencil"></i></a><a class="btn btn-xs m-r-5 btn-danger " href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data User\',\'user_bkk\',\'hapus_data\','."'".$rowx->bidik_id."'".')"><i class="icon-remove icon-white"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->bidik_model->count_all(),
				"recordsFiltered" => $this->bidik_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}
	function caridata(){
		$datanya = strtoupper($this->input->post('term'));
		$jurusan = $this->input->post('jurusan');
		$query = $this->bidik_model->getData($datanya,$jurusan);
		$data['response'] = 'false';
		if(!empty($query)){
			$data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach( $query as $row ){
                $data['message'][] = array( 
                    'nisn'=>$row->nisn,
                    'nama' => $row->nama,
                    ''
                 );  //Add a row to array
            }
		}	
		if('IS_AJAX'){
           echo json_encode($data); //echo json string if ajax request
        }else{
            $this->_content();
        }
	}
	public function add(){
		$isi['page'] = "bidik";
		$isi['link'] = 'bidikmisi';
		$isi['halaman'] = "Bidik Misi <sup style='color:red;text-size:12px'>Beta</sup>";
		$isi['judul'] = "";
		$isi['content'] = "fbidik_view";
		$isi['option_jurusan'][''] = "Pilih Jurusan";
		$isi['option_jurusan']['0'] = "Semua Jurusan";
		$sk = $this->session->userdata('role_');
		$jurusan = $this->db1->query("SELECT * FROM ref.jurusan_sp WHERE sekolah_id='$sk'")->result();
		if(count($jurusan)>0){
			foreach ($jurusan as $row) {
				$isi['option_jurusan'][trim($row->jurusan_id)] = $row->nama_jurusan_sp;
			}
		}else{
			$isi['option_jurusan'][''] = "Jurusan Belum Tersedia";
		}
       	$this->load->view("dashboard/dashboard_view",$isi);
	}
	function show()
	{
		$requestData= $_REQUEST;
		$columns = array( 
		// datatable column index  => database column name
			0 =>'nisn', 
			1 => 'nama',
			2=> 'nama_jurusan_sp',
			3=> null
		);
		$id = $this->session->userdata('role_');
		$q = "SELECT * FROM ref.peserta_didik WHERE sekolah_id='$id'";
		$dt = $this->db1->query($q);
		$totalData = $dt->num_rows();
		$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		$sql = "SELECT nisn,nama,nama_jurusan_sp";
		$sql.=" FROM ref.peserta_didik WHERE sekolah_id='$id' AND 1=1";
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$sql.=" AND ( nisn LIKE '". strtoupper($requestData['search']['value'])."%' ";    
			$sql.=" OR nama LIKE '". strtoupper($requestData['search']['value'])."%' ";
			$sql.=" OR nama_jurusan_sp LIKE '". strtoupper($requestData['search']['value'])."%' )";
		}
		$dbs = $this->db1->query($sql);
		$totalFiltered = $dbs->num_rows();
		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['length']." OFFSET ".$requestData['start']."   ";
		$dbx = $this->db1->query($sql)->result();
		$data = array();
		foreach ($dbx as $key) {
			$nestedData=array(); 
			$nestedData[] = $key->nisn;
			$nestedData[] = $key->nama;
			$nestedData[] = $key->nama_jurusan_sp;
			$nestedData[] = '';
			$data[] = $nestedData;
		}
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
			echo json_encode($json_data);  // send data as json format
	}
	// public function proses_add(){
	// 	if($this->input->is_ajax_request()){
	// 		$method = "save";
	//         $this->_validasi($method);
	// 		$nip = $this->input->post('nip');
	// 		$tlp = $this->input->post('tlp');
	//         $nama = $this->input->post('nama');
	//         $email = $this->input->post('mail');
	//         $sess_skolah = $this->session->userdata('sekolah_sess');
	//         $data = array('sekolah_id' => $sess_skolah,
	//         	'nip'=>$nip,
	//         	'nama'=>htmlspecialchars($nama),
	//         	'email'=>$email,
	//         	'tlp' =>$tlp,
	//         );
	//         $insert = $this->bidik_model->simpan($data);
	//         $q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
	// 		$qry = $this->db1->query($q);
	// 		$key = $qry->row();
	// 		$user_id = $key->user_id;
	// 		$pass = $this->bcrypt->hash_password($nip);
	// 		$dd = array('user_id' => $user_id ,
	// 			'username'=> $nip,
	// 			'password'=>$pass,
	// 			'level' => '1',
	// 			'login' => '1' 
	// 		);
	// 		$this->db1->insert('app.username',$dd);
	//         echo json_encode(array("status" => TRUE));
 //        }else{
	//     	redirect("_404","refresh");
	//     }
 //    }
	// public function proses_edit(){
	// 	if($this->input->is_ajax_request()){
	// 		$method = "edit";
	//         $this->_validasi($method);
	//        	$nip = $this->input->post('nip');
	// 		$tlp = $this->input->post('tlp');
	//         $nama = $this->input->post('nama');
	//         $email = $this->input->post('mail');
	//         $sess_skolah = $this->session->userdata('sekolah_sess');
	//         $data = array('sekolah_id' => $sess_skolah,
	//         	'nip'=>$nip,
	//         	'nama'=>htmlspecialchars($nama),
	//         	'email'=>$email,
	//         	'tlp' =>$tlp,
	//         );
	//         $this->bidik_model->update(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	//         echo json_encode(array("status" => TRUE));
	//     }else{
	//     	redirect("_404","refresh");
	//     }
 //    }
	// public function hapus_data($id){
	// 	if($this->input->is_ajax_request()){
	// 		$ckuserid = $this->db->query("SELECT user_id FROM app.user_sekolah WHERE id = '$id'");
	// 		$row = $ckuserid->row();
	// 		$this->bidik_model->hapus_user_id($row->user_id);
	//         $this->bidik_model->hapus_by_id($id);
	//         echo json_encode(array("status" => TRUE));
	//     }else{
	//     	redirect("_404","refresh");
	//     }
 //    }
 //    public function edit_data($id){
	// 	if($this->input->is_ajax_request()){
	// 		$data = $this->bidik_model->get_by_id($id);
	// 		echo json_encode($data);
	// 	}else{
	// 		redirect("_404","refresh");
	// 	}
	// }
}