<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tentang extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login();
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['namamenu'] = "";
		$isi['page'] = "tentang";
		$isi['kelas'] = "dashboard";
		$isi['link'] = 'dashboard';
		$isi['halaman'] = "Dashboard";
		$isi['judul'] = "Halaman Dashboard";
		$isi['content'] = "tentang_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
}
