<script type="text/javascript">
$(document).ajaxStop(function(){
    $.unblockUI;
    window.location.href="<?php echo base_url();?>reg_bkk/afterProses";
}); 


$( document ).ready(function() 
{
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... proses registrasi sedang berlangsung ! '
    });

   
        $.ajax({
        url: "<?php echo base_url(); ?>reg_bkk/proses",
        dataType: 'json',
        type: 'POST',
        cache: 'false',
        data: {term:'reg'},
        success:    
        function(data){
            if(data.response =="true"){
              // $("#isi").append(data.stat);
            }
        },
        complete: function(){
            jQuery.unblockUI();
        }
        });
    

});
</script>
<div class="row">
<div class="col-md-12 ui-sortable" id="tabel">
   <h3>Proses registrasi</h3>
   <ul id="isi">


   </ul>
</div>
</div>

