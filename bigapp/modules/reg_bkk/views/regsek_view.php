<script type="text/javascript">
var host = window.location.host;
$BASE_URL = 'http://'+host+'/';

$(document).ready(function(){
    $("#sekolah").change(function(){
        var ses = $("#id_sekolah").val();
            $.ajax({
                        url: "<?php echo base_url(); ?>reg_bkk/caridata",
                        dataType: 'json',
                        type: 'POST',
                        data: {term:ses},
                        success:    
                        function(data){
                            if(data.response =="true"){
                              var sesa = data.message[0].id;
                              var a = data.message[0].value;
                              var b =  data.message[0].nss;
                              var c =  data.message[0].npsn;
                              var d =  data.message[0].alamat;
                              var e =  data.message[0].desa;
                              var f =  data.message[0].kode_pos;
                              var g =  data.message[0].lintang;
                              var h =  data.message[0].bujur;
                              var i =  data.message[0].nomor_tlp;
                              var j = data.message[0].email;

                              $.ajax({
                                        url: "<?php echo base_url(); ?>reg_bkk/vsdata",
                                        dataType: 'json',
                                        type: 'POST',
                                        data: {term:sesa},
                                        success:    
                                        function(dat){
                                        if(dat.response =="1")
                                        {
                                            $("#result").blur();
                                            $("#result").html(
                                            "<table class='table'><tr><td>Nama sekolah</td><td>:</td><td>"+ 
                                            a +    "</td></tr><td>NSS</td><td>:</td><td>"+ 
                                            b  +      "</td></tr><td>NPSN</td><td>:</td><td>"+ 
                                            c  +     "</td></tr><td>Alamat</td><td>:</td><td>"+
                                            d  +   "</td></tr><td>Desa/Kelurahan</td><td>:</td><td>"+
                                            e +     "</td></tr><td>Kode Pos</td><td>:</td><td>"+ 
                                            f + "</td></tr></tr><td>Lintang</td><td>:</td><td>"+ 
                                            g+   "</td></tr></tr><td>Bujur</td><td>:</td><td>"+ 
                                            h+     "</td></tr></tr><td>Telephone</td><td>:</td><td>"+ 
                                            i+ "</td></tr></tr><td>Email</td><td>:</td><td>"+
                                            j+     "</td></tr></table>"+
                                            " <button type=\"button\" id=\"btn\" onclick=\"doit()\" class=\"btn btn-success\"><i class=\"fa fa-save (alias)\"></i> Proses</button> "
                                            );    
                                            $.post('<?php echo base_url();?>reg_bkk/set_session', { 'fieldname' : 'sakolana', 'value' :ses});
                                            $("#cari").val(''); 
                                            return false;
                                        }
                                        else  if(dat.response =="2")
                                           {
                                             $("#result").blur();
                                             $("#cari").val("");
                                             alert("BKK sudah mendaftar, silahkan cek aktivasi bkk");
                                             return true;
                                           }

                                        else
                                        {
                                            $("#cari").val("");
                                            alert("BKK Sekolah ini sudah terdaftar");
                                            return true;
                                        }
                                    }
                                });

                            }
                        }
                });
    });
});

    function doit()
    {   
        $("#btn").hide();
        jQuery.blockUI(
        {
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 2, 
                color: '#fff' 
            },
            message : 'Proses registrasi BKK <br/> Mohon menunggu ... '
        });
        window.location = $BASE_URL+'reg_bkk/doReg/';
        setTimeout(function(){
            jQuery.unblockUI();
        },60000);
    }
</script>

<div class="row">
<div class="col-md-12 ui-sortable">

        <div class="panel panel-primary" data-sortable-id="ui-widget-15">
        <div class="panel-heading">
            <h4 class="panel-title">Pencarian BKK Sekolah</h4>
        </div>
        <div class="panel-body" >
        <div class="form-horizontal form-bordered">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3">Provonsi :</label>
                <div class="col-md-3 col-sm-3">
                    <select id="prov" class="form-control selectpicker" data-live-search='true' onchange="cek_prov('prov','daftar_sekolah','cek_prov');">
                        <?php 
                            foreach ($option_prov as $key => $value) {
                                ?>
                                <option value="<?php echo $key ?>"> <?php echo $value ?> </option>
                                <?php
                            }
                        ?>
                    </select>                                    
                </div>
            </div>
            <div class="form-group" id="form_kota" style="display: none">
                <label class="control-label col-md-3 col-sm-3">Kota :</label>
                <div class="col-md-3 col-sm-3" id="kota">
                                                       
                </div>
            </div>
            <div class="form-group" id="form_sekolah" style="display: none">
                <label class="control-label col-md-3 col-sm-3">Sekolah :</label>
                <div class="col-md-3 col-sm-3" id="sekolah">
                                                       
                </div>
            </div>  
            </div>
            <ul class="result-list" id="result"></ul> 
        </div>
    </div>
    </div>
</div>
