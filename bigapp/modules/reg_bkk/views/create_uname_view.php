<script type="text/javascript">
$(document).ready(function(){
	$('#submit').click(function(){
		saveUser();
	});
});

var host = window.location.host;
$BASE_URL = 'http://'+host+'/';
var current_page    =   1;
function saveUser(){
	<?php
            echo "csrf ='" . $this->security->get_csrf_hash() . "';";
    ?>
    var uname = jQuery("#username").val();
    var password = jQuery("#password").val();
    var nama = jQuery('#nama').val();
    var nip = jQuery('#nip').val();
    var email = jQuery('#email').val();
    var hp = jQuery('#hp').val();

    if (uname == "") {
        jQuery("#username").effect('shake','1500').attr('placeholder','Username tidak boleh kosong');
    } else if (password == "") {
        jQuery("#password").effect('shake','1500').attr('placeholder','Password tidak boleh kosong');
    } else if (password == "") {
        jQuery("#nama").effect('shake','1500').attr('placeholder','Nama tidak boleh kosong');
    } else if (password == "") {
        jQuery("#nip").effect('shake','1500').attr('placeholder','NIP/NIK tidak boleh kosong');
    } else if (password == "") {
        jQuery("#email").effect('shake','1500').attr('placeholder','NIP/NIK tidak boleh kosong');
    } else if (password == "") {
        jQuery("#hp").effect('shake','1500').attr('placeholder','No Telephone tidak boleh kosong');
    } else {
        jQuery.blockUI({
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 2, 
                color: '#fff' 
            },
            message : 'Penyimpanan data <br/> Mohon menunggu ... '
        });
        
        jQuery.ajax({
            url : $BASE_URL+'reg_bkk/simpan_userbkk',
            data : {txtuname:uname,
            		txtpass:password,
            		txtnama:nama,
            		txtnip:nip,
            		txtemail:email,
            		txthp:hp,
            		<?php echo $this->security->get_csrf_token_name(); ?>:csrf},
            type : 'POST',
            dataType: 'json',
            success:function(data){
                jQuery.unblockUI();
                if(data.response=='true'){
                    jQuery.blockUI({
                        css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: 2, 
                            color: '#fff' 
                        },
                        message : 'Berhasil disimpan!!! Sedang Memuat Halaman, Mohon menunggu ... '
                    });
                    window.location = $BASE_URL+'dashboard';
                    setTimeout(function(){
                        jQuery.unblockUI();
                    },50);
                }        
            }
        });
    }
}
</script>

<div class="row">
   <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Buat user admin BKK</h4>
            </div>
            <div class="panel-body">
                <div>
                     <form id="frm-userbkk" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" action="">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip">NIP/NIK <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="nip" name="nip" required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="email" required="required" class="form-control col-md-7 col-xs-12" name="email" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Telephone/HP<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="hp" name="hp" required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="username" value="<?php echo $npsn;?>" name="username" required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="password" name="password" value="<?php echo $pwd;?>" required="required" class="form-control col-md-7 col-xs-12" type="text">
            </div>
          </div>
                             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="button" id="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

        </form>
                </div>
            </div>
        </div>
    </div>


</div>