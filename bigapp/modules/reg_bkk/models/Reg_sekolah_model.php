<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reg_sekolah_model extends CI_Model {

public function __construct(){
  	parent::__construct();
  		$this->db1 = $this->load->database('default', TRUE);
	}

	function getData($datanya)
	{
		$qry =  $this->db1->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$datanya'");
		return $qry->result();
	}

	function get_sekolah($id)
	{
		$qry =  $this->db1->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$id'");
		return $qry->result();
	}

	function simpan_sekolah($data)
	{
		$this->db1->insert('sekolah_terdaftar',$data);
	}

	function get_siswa($id)
	{
		$q ="SELECT ref.peserta_didik.peserta_didik_id,ref.peserta_didik.nama,ref.peserta_didik.jenis_kelamin,
		ref.peserta_didik.nisn,ref.peserta_didik.nik,ref.peserta_didik.tempat_lahir,
		ref.peserta_didik.tanggal_lahir,ref.peserta_didik.agama_id, 
		ref.peserta_didik.kebutuhan_khusus_id,ref.peserta_didik.alamat_jalan,
		ref.peserta_didik.rt,ref.peserta_didik.rw,ref.peserta_didik.desa_kelurahan,
		ref.peserta_didik.kode_wilayah,ref.peserta_didik.nama_dusun, 
		ref.peserta_didik.kode_pos,ref.peserta_didik.email,ref.peserta_didik.nomor_telepon_rumah,
		ref.peserta_didik.nomor_telepon_seluler,ref.peserta_didik.nama_ayah,
		ref.peserta_didik.tahun_lahir_ayah,ref.peserta_didik.jenjang_pendidikan_ayah,
		ref.peserta_didik.pekerjaan_id_ayah,ref.peserta_didik.penghasilan_id_ayah,
		ref.peserta_didik.kebutuhan_khusus_id_ayah,ref.peserta_didik.nama_ibu_kandung, 
		ref.peserta_didik.tahun_lahir_ibu,ref.peserta_didik.jenjang_pendidikan_ibu,
		ref.peserta_didik.penghasilan_id_ibu,ref.peserta_didik.pekerjaan_id_ibu,
		ref.peserta_didik.nik_ayah, ref.peserta_didik.nik_ibu,ref.peserta_didik.nik_wali,
		ref.peserta_didik.nama_wali,ref.peserta_didik.tahun_lahir_wali,ref.peserta_didik.pekerjaan_id_wali,
		ref.peserta_didik.kewarganegaraan,ref.jurusan_sp.jurusan_id, 
		ref.registrasi_peserta_didik.jenis_keluar_id,ref.sekolah.sekolah_id 
		FROM ref.peserta_didik 
INNER JOIN ref.registrasi_peserta_didik ON ref.peserta_didik.peserta_didik_id =ref.registrasi_peserta_didik.peserta_didik_id 
		INNER JOIN ref.sekolah ON ref.registrasi_peserta_didik.sekolah_id = ref.sekolah.sekolah_id 
INNER JOIN ref.jurusan_sp ON ref.jurusan_sp.jurusan_sp_id=ref.registrasi_peserta_didik.jurusan_sp_id
		WHERE ref.registrasi_peserta_didik.jenis_keluar_id is null 
		AND ref.registrasi_peserta_didik.sekolah_id='$id'";	
	
	$qry = $this->db1->query($q);
		return $qry->result();
	}

	function get_lulusan($id)
	{
// 		$q=	"SELECT  ref.peserta_didik.peserta_didik_id,ref.peserta_didik.nama, ref.peserta_didik.jenis_kelamin, ref.peserta_didik.nisn, ref.peserta_didik.nik, ref.peserta_didik.tempat_lahir, ref.peserta_didik.tanggal_lahir, ref.peserta_didik.agama_id, ref.peserta_didik.kebutuhan_khusus_id, 
//                          ref.peserta_didik.alamat_jalan, ref.peserta_didik.rt, ref.peserta_didik.rw, ref.peserta_didik.nama_dusun, ref.peserta_didik.desa_kelurahan, ref.peserta_didik.kode_wilayah, ref.peserta_didik.kode_pos, ref.peserta_didik.nik_ayah, 
//                          ref.peserta_didik.nik_ibu, ref.peserta_didik.nomor_telepon_rumah, ref.peserta_didik.nomor_telepon_seluler, ref.peserta_didik.email, ref.peserta_didik.reg_akta_lahir, ref.peserta_didik.nama_ayah, ref.peserta_didik.tahun_lahir_ayah, 
//                          ref.peserta_didik.jenjang_pendidikan_ayah, ref.peserta_didik.pekerjaan_id_ayah, ref.peserta_didik.penghasilan_id_ayah, ref.peserta_didik.kebutuhan_khusus_id_ayah, ref.peserta_didik.nama_ibu_kandung, 
//                          ref.peserta_didik.tahun_lahir_ibu, ref.peserta_didik.jenjang_pendidikan_ibu, ref.peserta_didik.penghasilan_id_ibu, ref.peserta_didik.pekerjaan_id_ibu, ref.peserta_didik.kebutuhan_khusus_id_ibu, 
//                          ref.peserta_didik.kewarganegaraan,ref.peserta_didik.nik_wali,ref.peserta_didik.nama_wali,ref.peserta_didik.tahun_lahir_wali,ref.peserta_didik.pekerjaan_id_wali
// FROM            ref.peserta_didik INNER JOIN
//                          ref.registrasi_peserta_didik ON ref.peserta_didik.peserta_didik_id = registrasi_peserta_didik.peserta_didik_id INNER JOIN
//                          ref.sekolah ON ref.registrasi_peserta_didik.sekolah_id = ref.sekolah.sekolah_id
// WHERE ref.registrasi_peserta_didik.jenis_keluar_id='1' AND ref.registrasi_peserta_didik.sekolah_id='$id'";
// // tgl_keluar = not null	
	$q ="SELECT ref.peserta_didik.peserta_didik_id,ref.peserta_didik.nama,ref.peserta_didik.jenis_kelamin,
		ref.peserta_didik.nisn,ref.peserta_didik.nik,ref.peserta_didik.tempat_lahir,
		ref.peserta_didik.tanggal_lahir,ref.peserta_didik.agama_id, 
		ref.peserta_didik.kebutuhan_khusus_id,ref.peserta_didik.alamat_jalan,
		ref.peserta_didik.rt,ref.peserta_didik.rw,ref.peserta_didik.desa_kelurahan,
		ref.peserta_didik.kode_wilayah,ref.peserta_didik.nama_dusun, 
		ref.peserta_didik.kode_pos,ref.peserta_didik.email,ref.peserta_didik.nomor_telepon_rumah,
		ref.peserta_didik.nomor_telepon_seluler,ref.peserta_didik.nama_ayah,
		ref.peserta_didik.tahun_lahir_ayah,ref.peserta_didik.jenjang_pendidikan_ayah,
		ref.peserta_didik.pekerjaan_id_ayah,ref.peserta_didik.penghasilan_id_ayah,
		ref.peserta_didik.kebutuhan_khusus_id_ayah,ref.peserta_didik.nama_ibu_kandung, 
		ref.peserta_didik.tahun_lahir_ibu,ref.peserta_didik.jenjang_pendidikan_ibu,
		ref.peserta_didik.penghasilan_id_ibu,ref.peserta_didik.pekerjaan_id_ibu,
		ref.peserta_didik.nik_ayah, ref.peserta_didik.nik_ibu,ref.peserta_didik.nik_wali,
		ref.peserta_didik.nama_wali,ref.peserta_didik.tahun_lahir_wali,ref.peserta_didik.pekerjaan_id_wali,
		ref.peserta_didik.kewarganegaraan,ref.jurusan_sp.jurusan_id, 
		ref.registrasi_peserta_didik.jenis_keluar_id,ref.sekolah.sekolah_id 
		FROM ref.peserta_didik 
INNER JOIN ref.registrasi_peserta_didik ON ref.peserta_didik.peserta_didik_id =ref.registrasi_peserta_didik.peserta_didik_id 
		INNER JOIN ref.sekolah ON ref.registrasi_peserta_didik.sekolah_id = ref.sekolah.sekolah_id 
INNER JOIN ref.jurusan_sp ON ref.jurusan_sp.jurusan_sp_id=ref.registrasi_peserta_didik.jurusan_sp_id
		WHERE ref.registrasi_peserta_didik.jenis_keluar_id='1'  
		AND ref.registrasi_peserta_didik.sekolah_id='$id'";	
		$qry = $this->db1->query($q);
		return $qry->result();
	}

	function get_jurusan_siswa($id)
	{
$q="SELECT ref.peserta_didik.peserta_didik_id, ref.peserta_didik.nama, ref.jurusan.jurusan_id, ref.jurusan.nama_jurusan
FROM            
ref.anggota_rombel 
INNER JOIN
ref.rombongan_belajar 
ON ref.anggota_rombel.rombongan_belajar_id = ref.rombongan_belajar.rombongan_belajar_id 
INNER JOIN
ref.peserta_didik 
ON ref.anggota_rombel.peserta_didik_id = ref.peserta_didik.peserta_didik_id 
INNER JOIN
ref.kurikulum 
ON ref.rombongan_belajar.kurikulum_id = ref.kurikulum.kurikulum_id 
INNER JOIN 
ref.jurusan 
ON ref.kurikulum.jurusan_id = ref.jurusan.jurusan_id 
AND ref.kurikulum.jurusan_id = ref.jurusan.jurusan_id 
AND ref.kurikulum.jurusan_id = ref.jurusan.jurusan_id 
WHERE ref.rombongan_belajar.sekolah_id='b8577cea-d95d-4172-978f-db41b031035c' 
AND CHAR_LENGTH(ref.jurusan.jurusan_id)>5
GROUP BY 
ref.peserta_didik.peserta_didik_id, 
ref.peserta_didik.nama, 
ref.jurusan.jurusan_id, 
ref.jurusan.nama_jurusan 
ORDER BY ref.peserta_didik.peserta_didik_id ASC, ref.jurusan.jurusan_id ASC";
		$qry = $this->db1->query($q);
		return $qry->result();
	}
// program_id || jurusan_id // ambil dari sini 

	function get_rombel($id)
	{
		$q = "SELECT * FROM ref.rombongan_belajar WHERE sekolah_id='$id'";
		$qry = $this->db1->query($q);
		return $qry->result();
	}
	
}
