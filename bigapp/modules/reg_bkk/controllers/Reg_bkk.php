<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(0);
class Reg_bkk extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login();
      	$this->asn->role_akses("0|1");
  		$this->load->library('bcrypt');
  		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('reg_bkk/reg_sekolah_model');
		$this->load->library('email');
 	}
	public function index(){
		$this->_content();
	}
	
	public function _content(){
		$isi['namamenu'] = "Sekolah";
		$isi['page'] = "reg";
		$isi['kelas'] = "registrasi";
		$isi['link'] = 'reg_bkk';
		$isi['halaman'] = "Registrasi";
		$isi['judul'] = "BKK Sekolah";
		$isi['content'] = "regsek_view";
		$isi['option_prov']['']  = "Pilih Provinsi";
		$ang = $this->db->get('ref.view_provinsi')->result();
		foreach($ang as $key)
		{
			$isi['option_prov'][$key->kode_prov]=$key->prov;
		}

       	$this->load->view("dashboard/dashboard_view",$isi);
	}
	function caridata(){
		$datanya = strtoupper($this->input->post('term'));
		$query = $this->reg_sekolah_model->getData($datanya);
		$data['response'] = 'false';
		if(!empty($query)){
			$data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach( $query as $row ){
                $data['message'][] = array( 
                    'id'=>$row->sekolah_id,
                    'value' => $row->nama,
                    'alamat' => $row->alamat_jalan,
                    'nss' => $row->nss,
                    'npsn' => $row->npsn,
                    'desa' => $row->desa_kelurahan,
                    'kode_pos' => $row->kode_pos,
                    'lintang' => $row->lintang, 
                    'bujur' => $row->bujur,
                    'nomor_tlp' => $row->nomor_telepon,
                    'email' => $row->email,
                    ''
                 );  //Add a row to array
            }
		}	

		if('IS_AJAX'){
           echo json_encode($data); //echo json string if ajax request
        }else{
            $this->_content();
        }
	}
	
	function vsdata(){
		if($this->session->userdata('level')==0)
		{
			$dat['response']='0';
			$d = $this->input->post('term'); 
			$q = "SELECT * FROM sekolah_terdaftar WHERE sekolah_id='$d'";
			$qry = $this->db1->query($q)->result();
			if(empty($qry)){
				$q1 = "SELECT * FROM app.registrasi_sekolah WHERE sekolah_id='$d' and status='0'";
				$qry1 = $this->db1->query($q1)->result();
				if(empty($qry1)){
					$dat['response']='1';
				}else{
					$dat['response']='2';
				}
			}else{
				$dat['response']='0';
			}
		}
		
		echo json_encode($dat);
	}
	
	function set_session(){
		$val = $this->input->post('fieldname');
		$nil = $this->input->post('value');
		$this->session->set_userdata($val,$nil);
	}

 function doReg()
  {
    if($this->session->userdata('login')==TRUE && $this->session->userdata('level')=='0')
    {
        $isi['namamenu'] = "Sekolah";
		$isi['page'] = "registrasi";
		$isi['kelas'] = "registrasi";
		$isi['link'] = 'reg_bkk';
		$isi['halaman'] = "Registrasi";
		$isi['judul'] = "BKK Sekolah";
		$isi['content'] = "r_view";
        $this->load->view("dashboard/dashboard_view",$isi);
    }
    else
    {
      redirect('login','refresh');
    }
  }

	function proses(){
		$npsn="";
		$schoolId = $this->session->userdata('sakolana');	
		$tgl = date("Y-m-d H:i:s");
		$query = $this->reg_sekolah_model->get_sekolah($schoolId);
		foreach ($query as $row) {
			$data = array(
				'sekolah_id'=>$row->sekolah_id,
	            'tgl_aktivasi'=>$tgl,
	            'logo' => 'no.jpg',
	            'no_izin_bkk'=>"",
	            'approved_by'=>$this->session->userdata('user_id')
            );	
			$npsn=$row->npsn;
		}
		$this->db1->insert('sekolah_terdaftar',$data);

		// insert ke jurusan_sp
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$dataInsert = array(
              'jurusan_sp_id' =>pg_escape_string(trim($dataS[$a]['jurusan_sp_id'])),
              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
              'kebutuhan_khusus_id' =>pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id'])),
              'jurusan_id'=>pg_escape_string(trim($dataS[$a]['jurusan_id'])),
              'nama_jurusan_sp'=>pg_escape_string(trim($dataS[$a]['nama_jurusan_sp']))
            );
            $this->db1->insert('ref.jurusan_sp',$dataInsert);
		}

		// insert ke peserta didik
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresep&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$dataInsert = array(
              'peserta_didik_id' =>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
              'nama' =>pg_escape_string(trim($dataS[$a]['nama'])),
              'jenis_kelamin' =>pg_escape_string(trim($dataS[$a]['jenis_kelamin'])),
              'nisn' =>pg_escape_string(trim($dataS[$a]['nisn'])),
              'nik' =>pg_escape_string(trim($dataS[$a]['nik'])),
              'tempat_lahir' =>pg_escape_string(trim($dataS[$a]['tempat_lahir'])),
              'tanggal_lahir' =>pg_escape_string(trim($dataS[$a]['tanggal_lahir'])),
              'agama_id' =>intval(pg_escape_string(trim($dataS[$a]['agama_id']))),
              'kebutuhan_khusus_id' =>intval(pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id']))),
              'alamat_jalan' =>pg_escape_string(trim($dataS[$a]['alamat_jalan'])),
              'rt' =>intval(pg_escape_string(trim($dataS[$a]['rt']))),
              'rw' =>intval(pg_escape_string(trim($dataS[$a]['rw']))),
              'nama_dusun' =>pg_escape_string(trim($dataS[$a]['nama_dusun'])),
              'desa_kelurahan' =>pg_escape_string(trim($dataS[$a]['desa_kelurahan'])),
              'kode_wilayah' =>pg_escape_string(trim($dataS[$a]['kode_wilayah'])),
              'kode_pos' =>pg_escape_string(trim($dataS[$a]['kode_pos'])),
              'nomor_telepon_rumah' =>pg_escape_string(trim($dataS[$a]['nomor_telepon_rumah'])),
              'nomor_telepon_seluler' =>pg_escape_string(trim($dataS[$a]['nomor_telepon_seluler'])),
              'email' =>pg_escape_string(trim($dataS[$a]['email'])),
              'nik_ayah' =>pg_escape_string(trim($dataS[$a]['nik_ayah'])),
              'nik_ibu' =>pg_escape_string(trim($dataS[$a]['nik_ibu'])),
              'penerima_KPS' =>intval(pg_escape_string(trim($dataS[$a]['penerima_KPS']))),
              'no_KPS' =>pg_escape_string(trim($dataS[$a]['no_KPS'])),
              'layak_PIP' =>intval(pg_escape_string(trim($dataS[$a]['layak_PIP']))),
              'penerima_KIP' =>intval(pg_escape_string(trim($dataS[$a]['penerima_KIP']))),
              'no_KIP'=>pg_escape_string(trim($dataS[$a]['no_KIP'])),
              'nm_KIP' =>pg_escape_string(trim($dataS[$a]['nm_KIP'])),
              'no_KKS' =>pg_escape_string(trim($dataS[$a]['no_KKS'])),
              'id_layak_pip' =>intval(pg_escape_string(trim($dataS[$a]['id_layak_pip']))),
              'nama_ayah' =>pg_escape_string(trim($dataS[$a]['nama_ayah'])),
              'tahun_lahir_ayah' =>pg_escape_string(trim($dataS[$a]['tahun_lahir_ayah'])),
              'jenjang_pendidikan_ayah' =>intval(pg_escape_string(trim($dataS[$a]['jenjang_pendidikan_ayah']))),
              'pekerjaan_id_ayah' =>intval(pg_escape_string(trim($dataS[$a]['pekerjaan_id_ayah']))),
              'penghasilan_id_ayah' =>intval(pg_escape_string(trim($dataS[$a]['penghasilan_id_ayah']))),
              'nama_ibu_kandung' =>intval(pg_escape_string(trim($dataS[$a]['nama_ibu_kandung']))),
              'tahun_lahir_ibu' =>pg_escape_string(trim($dataS[$a]['tahun_lahir_ibu'])),
              'jenjang_pendidikan_ibu' =>intval(pg_escape_string(trim($dataS[$a]['jenjang_pendidikan_ibu']))),
              'penghasilan_id_ibu' =>intval(pg_escape_string(trim($dataS[$a]['penghasilan_id_ibu']))),
              'pekerjaan_id_ibu' =>intval(pg_escape_string(trim($dataS[$a]['pekerjaan_id_ibu'])))
            );
           $this->db1->insert('ref.peserta_didik',$dataInsert);
		}

		// insert ke registrasi peserta didik
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			if(pg_escape_string(trim($dataS[$a]['keterangan']))=="")
			{
				$ket = NULL;
			}
			else
			{
				$ket = pg_escape_string(trim($dataS[$a]['keterangan']));
			}

			if(pg_escape_string(trim($dataS[$a]['tanggal_masuk_sekolah']))=="")
			{
				$tm = NULL;
			}
			else
			{
				$tm = pg_escape_string(trim($dataS[$a]['tanggal_masuk_sekolah']));
			}

			if(pg_escape_string(trim($dataS[$a]['tanggal_keluar']))=="")
			{
				$tk = NULL;
			}
			else
			{
				$tk = pg_escape_string(trim($dataS[$a]['tanggal_keluar']));
			}

			if(pg_escape_string(trim($dataS[$a]['jurusan_sp_id']))=="")
			{
				$js = NULL;
			}
			else
			{
				$js = pg_escape_string(trim($dataS[$a]['jurusan_sp_id']));
			}

			if(pg_escape_string(trim($dataS[$a]['jenis_keluar_id']))=="")
			{
				$jk = NULL;
			}
			else
			{
				$jk = pg_escape_string(trim($dataS[$a]['jenis_keluar_id']));
			}

			$dataInsert = array(
	              'registrasi_id' =>pg_escape_string(trim($dataS[$a]['registrasi_id'])),
	              'jurusan_sp_id' =>$js,
	              'peserta_didik_id' =>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
	              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
	              'jenis_pendaftaran_id' =>pg_escape_string(trim($dataS[$a]['jenis_pendaftaran_id'])),
	              'nipd' =>pg_escape_string(trim($dataS[$a]['nipd'])),
	              'tanggal_masuk_sekolah' =>$tm,
	              'jenis_keluar_id' =>$jk,
	              'tanggal_keluar' =>$tk,
	              'keterangan'=>$ket
	            );
	         $this->db1->insert('ref.registrasi_peserta_didik',$dataInsert);
	         $data['response'] = true;
		}
		
        // insert ke rombel
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
 			
 			$dataInsert = array(
              'rombongan_belajar_id' =>pg_escape_string(trim($dataS[$a]['rombongan_belajar_id'])),
              'semester_id' =>pg_escape_string(trim($dataS[$a]['semester_id'])),
              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
              'tingkat_pendidikan_id' =>pg_escape_string(trim($dataS[$a]['tingkat_pendidikan_id'])),
              'jurusan_sp_id' =>pg_escape_string(trim($dataS[$a]['jurusan_sp_id'])),
              'nama' =>pg_escape_string(trim($dataS[$a]['nama'])),
              'kebutuhan_khusus_id' =>pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id']))
            	);
          $this->db1->insert('ref.rombongan_belajar',$dataInsert);
	         $data['response'] = true;
		}

		// insert ke anggota rombel 
	$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
 			
 			$dataInsert = array(
              'anggota_rombel_id'=>pg_escape_string(trim($dataS[$a]['anggota_rombel_id'])),
              'rombongan_belajar_id'=>pg_escape_string(trim($dataS[$a]['rombongan_belajar_id'])),
              'peserta_didik_id'=>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
              'jenis_pendaftaran_id'=>pg_escape_string(trim($dataS[$a]['jenis_pendaftaran_id']))
            );
          $this->db1->insert('ref.anggota_rombel',$dataInsert);
	         $data['response'] = true;
		}


		$cksekolah = $this->db->get_where('ref.view_lulusan_kls12',array('sekolah_id'=>$schoolId))->result();
		if(count($cksekolah)>0){
			foreach ($cksekolah as $row) {
				$x = date("d-m-Y",strtotime($row->tanggal_lahir));
				$tgl = explode("-", $x);
				$a = $tgl[0];
				$b = $tgl[1];
				$c = $tgl[2];
				$thn = substr($c, 2);
				$pass = $this->bcrypt->hash_password($a.$b.$thn);
				$s_username = array('user_id'=>$row->peserta_didik_id,
					'username'=>$row->nisn,
					'password'=>$pass,
					'level'=>'3',
					'login'=>'1');
				$this->db1->insert('app.username',$s_username);
			}
		}

		echo json_encode($data);

	}

	public function cek_prov($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_kota where kode_prov='".$kode."' ORDER BY kode_kota ASC")->result();
		$data['say'] = '
			<select id="id_kota" class="form-control selectpicker" data-live-search="true" onchange="cek_sekolah(\'id_kota\',\'daftar_sekolah\',\'cek_sekolah\');">
				<option value="">Pilih Kota</option>
		';
		
		foreach($ckdata as $key){
			$data['say'].='<option value="'.$key->kode_kota.'">'.$key->kota.'</option>';
		}
		$data['say'].='</select>';

		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}

	public function cek_sekolah($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_sekolah_wilayah where kode_kota='".$kode."' ORDER BY nsekolah ASC")->result();
		$data['say'] = '
			<select id="id_sekolah" class="form-control selectpicker" data-live-search="true" onchange="if(this.value!=\'\'){$(\'#tombol\').show();$(\'#sekolah_id\').val(this.value);}else{$(\'#tombol\').hide();}">
				<option value="">Pilih Sekolah</option>
		';
		
		foreach($ckdata as $key){
			$data['say'].='<option value="'.$key->sekolah_id.'">'.$key->nsekolah.'</option>';
		}
		$data['say'].='</select>';

		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}


	
	function generatePassword() {
		$length = 8;
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);
	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }
	    return $result;
	}

	function afterProses(){
		$d = $this->session->userdata('sakolana');
		$x = $this->db1->get_where('ref.sekolah',array('sekolah_id'=>$d))->result();
		foreach ($x as $key ) {
			$npsn = $key->npsn;
		}
		$p = $this->generatePassword();
		$isi['namamenu'] = "Sekolah";
		$isi['page'] = "registrasi";
		$isi['kelas'] = "registrasi";
		$isi['link'] = 'reg_bkk';
		$isi['halaman'] = "Registrasi";
		$isi['judul'] = "BKK Sekolah";
		$isi['content'] = "create_uname_view";
		$isi['npsn'] = $npsn;
		$isi['pwd'] = $p;
       	$this->load->view("dashboard/dashboard_view",$isi);
	}

	function simpan_userbkk(){
		$schoolId = $this->session->userdata('sakolana');
		$nama = $this->input->post('txtnama');
		$nip = $this->input->post('txtnip');
		$email = $this->input->post('txtemail');
		$hp = $this->input->post('txthp');
		$uname = $this->input->post('txtuname');
		$pass = $this->bcrypt->hash_password($this->input->post('txtpass'));
		$dat = array(
			'sekolah_id' => $schoolId,
			'nip' => $nip,
			'nama' => $nama,
			'email' =>$email,
			'tlp'=>$hp
			);
		$this->db1->insert('app.user_sekolah',$dat);

		$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
		$qry = $this->db1->query($q)->result();
		foreach ($qry as $key) {
			$user_id = $key->user_id;
		}

		$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
			);
		$this->db1->insert('app.username',$dd);

		// $config['protocol'] = 'sendmail';
		// $config['mailpath'] = '/usr/sbin/sendmail';
		// $config['charset'] = 'iso-8859-1';
		// $config['wordwrap'] = TRUE;
		// $this->email->initialize($config);
		// $this->email->from('admin-bkk@psmk.net', 'Administarator BKK Pusat');
		// $this->email->to($email);
		// $this->email->subject('Proses Aktifasi BKK - No Reply');
		// $this->email->message('
		// 		<p>Selamat BKK sekolah anda telah diaktifasi</p>
		// 		<p>Informasi login untuk BKK anda adalah</br>Username = $uname, Password = $this->input->post(\'txtpass\')</p>
		// 		<p>Selamat bergabung dan sukes selalu</p>
		// 	');

		// $this->email->send();
		$this->session->unset_userdata('sakolana');
		$data['response'] = 'true';
		echo json_encode($data);

		echo "Stop : " . date('H:i:s');
	}
	function tes(){
		$sql = "select * from ref.kelompok_bidang where untuk_smk='1' AND level_bidang_induk is null order by level_bidang_induk asc";
		$n = 0;
		$m =0;
		$o = 0;
		$qry = $this->db2->query($sql)->result();
		foreach ($qry as $key) {
			$n++;
			echo $n  .".  ". $key->nama_level_bidang . "<br/>";
			$ji = $key->level_bidang_id;
			$sq = "select * from ref.kelompok_bidang  where untuk_smk='1' AND level_bidang_induk='$ji' order by level_bidang_induk asc";
			$qry2 = $this->db2->query($sq)->result();
			foreach ($qry2 as $keys) {
				$m++;
				echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $m ."." . $keys->nama_level_bidang . "<br/>";
				$lv =  $keys->level_bidang_id;
				 $sqq = "select * from ref.jurusan  where untuk_smk='1' AND level_bidang_id='$lv' order by level_bidang_id asc";	
				 $qry3 = $this->db2->query($sqq)->result();
				 foreach ($qry3 as $row) {
				 	$o++;
				 	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;". $o ."." . $row->nama_jurusan . "<br/>";
				 }
				 $o=0;
			}
			$m=0;
		}
	}


	function tambah($schoolId)
	{
		echo "Start : " . date('H:i:s');
		sleep(2);


		$npsn="";
		$tgl = date("Y-m-d H:i:s");
		$query = $this->reg_sekolah_model->get_sekolah($schoolId);
		foreach ($query as $row) {
			$data = array(
				'sekolah_id'=>$row->sekolah_id,
	            'tgl_aktivasi'=>$tgl,
	            'logo' => 'no.jpg',
	            'no_izin_bkk'=>"",
	            'approved_by'=>$this->session->userdata('user_id')
            );	
			$npsn=$row->npsn;
		}
		$this->db1->insert('sekolah_terdaftar',$data);

		// insert ke jurusan_sp
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$dataInsert = array(
              'jurusan_sp_id' =>pg_escape_string(trim($dataS[$a]['jurusan_sp_id'])),
              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
              'kebutuhan_khusus_id' =>pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id'])),
              'jurusan_id'=>pg_escape_string(trim($dataS[$a]['jurusan_id'])),
              'nama_jurusan_sp'=>pg_escape_string(trim($dataS[$a]['nama_jurusan_sp']))
            );
            $this->db1->insert('ref.jurusan_sp',$dataInsert);
		}

		// insert ke peserta didik
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresep&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$dataInsert = array(
              'peserta_didik_id' =>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
              'nama' =>pg_escape_string(trim($dataS[$a]['nama'])),
              'jenis_kelamin' =>pg_escape_string(trim($dataS[$a]['jenis_kelamin'])),
              'nisn' =>pg_escape_string(trim($dataS[$a]['nisn'])),
              'nik' =>pg_escape_string(trim($dataS[$a]['nik'])),
              'tempat_lahir' =>pg_escape_string(trim($dataS[$a]['tempat_lahir'])),
              'tanggal_lahir' =>pg_escape_string(trim($dataS[$a]['tanggal_lahir'])),
              'agama_id' =>intval(pg_escape_string(trim($dataS[$a]['agama_id']))),
              'kebutuhan_khusus_id' =>intval(pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id']))),
              'alamat_jalan' =>pg_escape_string(trim($dataS[$a]['alamat_jalan'])),
              'rt' =>intval(pg_escape_string(trim($dataS[$a]['rt']))),
              'rw' =>intval(pg_escape_string(trim($dataS[$a]['rw']))),
              'nama_dusun' =>pg_escape_string(trim($dataS[$a]['nama_dusun'])),
              'desa_kelurahan' =>pg_escape_string(trim($dataS[$a]['desa_kelurahan'])),
              'kode_wilayah' =>pg_escape_string(trim($dataS[$a]['kode_wilayah'])),
              'kode_pos' =>pg_escape_string(trim($dataS[$a]['kode_pos'])),
              'nomor_telepon_rumah' =>pg_escape_string(trim($dataS[$a]['nomor_telepon_rumah'])),
              'nomor_telepon_seluler' =>pg_escape_string(trim($dataS[$a]['nomor_telepon_seluler'])),
              'email' =>pg_escape_string(trim($dataS[$a]['email'])),
              'nik_ayah' =>pg_escape_string(trim($dataS[$a]['nik_ayah'])),
              'nik_ibu' =>pg_escape_string(trim($dataS[$a]['nik_ibu'])),
              'penerima_KPS' =>intval(pg_escape_string(trim($dataS[$a]['penerima_KPS']))),
              'no_KPS' =>pg_escape_string(trim($dataS[$a]['no_KPS'])),
              'layak_PIP' =>intval(pg_escape_string(trim($dataS[$a]['layak_PIP']))),
              'penerima_KIP' =>intval(pg_escape_string(trim($dataS[$a]['penerima_KIP']))),
              'no_KIP'=>pg_escape_string(trim($dataS[$a]['no_KIP'])),
              'nm_KIP' =>pg_escape_string(trim($dataS[$a]['nm_KIP'])),
              'no_KKS' =>pg_escape_string(trim($dataS[$a]['no_KKS'])),
              'id_layak_pip' =>intval(pg_escape_string(trim($dataS[$a]['id_layak_pip']))),
              'nama_ayah' =>pg_escape_string(trim($dataS[$a]['nama_ayah'])),
              'tahun_lahir_ayah' =>pg_escape_string(trim($dataS[$a]['tahun_lahir_ayah'])),
              'jenjang_pendidikan_ayah' =>intval(pg_escape_string(trim($dataS[$a]['jenjang_pendidikan_ayah']))),
              'pekerjaan_id_ayah' =>intval(pg_escape_string(trim($dataS[$a]['pekerjaan_id_ayah']))),
              'penghasilan_id_ayah' =>intval(pg_escape_string(trim($dataS[$a]['penghasilan_id_ayah']))),
              'nama_ibu_kandung' =>intval(pg_escape_string(trim($dataS[$a]['nama_ibu_kandung']))),
              'tahun_lahir_ibu' =>pg_escape_string(trim($dataS[$a]['tahun_lahir_ibu'])),
              'jenjang_pendidikan_ibu' =>intval(pg_escape_string(trim($dataS[$a]['jenjang_pendidikan_ibu']))),
              'penghasilan_id_ibu' =>intval(pg_escape_string(trim($dataS[$a]['penghasilan_id_ibu']))),
              'pekerjaan_id_ibu' =>intval(pg_escape_string(trim($dataS[$a]['pekerjaan_id_ibu'])))
            );
           $this->db1->insert('ref.peserta_didik',$dataInsert);
		}

		// insert ke registrasi peserta didik
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			if(pg_escape_string(trim($dataS[$a]['keterangan']))=="")
			{
				$ket = NULL;
			}
			else
			{
				$ket = pg_escape_string(trim($dataS[$a]['keterangan']));
			}

			if(pg_escape_string(trim($dataS[$a]['tanggal_masuk_sekolah']))=="")
			{
				$tm = NULL;
			}
			else
			{
				$tm = pg_escape_string(trim($dataS[$a]['tanggal_masuk_sekolah']));
			}

			if(pg_escape_string(trim($dataS[$a]['tanggal_keluar']))=="")
			{
				$tk = NULL;
			}
			else
			{
				$tk = pg_escape_string(trim($dataS[$a]['tanggal_keluar']));
			}

			if(pg_escape_string(trim($dataS[$a]['jurusan_sp_id']))=="")
			{
				$js = NULL;
			}
			else
			{
				$js = pg_escape_string(trim($dataS[$a]['jurusan_sp_id']));
			}

			if(pg_escape_string(trim($dataS[$a]['jenis_keluar_id']))=="")
			{
				$jk = NULL;
			}
			else
			{
				$jk = pg_escape_string(trim($dataS[$a]['jenis_keluar_id']));
			}

			$dataInsert = array(
	              'registrasi_id' =>pg_escape_string(trim($dataS[$a]['registrasi_id'])),
	              'jurusan_sp_id' =>$js,
	              'peserta_didik_id' =>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
	              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
	              'jenis_pendaftaran_id' =>pg_escape_string(trim($dataS[$a]['jenis_pendaftaran_id'])),
	              'nipd' =>pg_escape_string(trim($dataS[$a]['nipd'])),
	              'tanggal_masuk_sekolah' =>$tm,
	              'jenis_keluar_id' =>$jk,
	              'tanggal_keluar' =>$tk,
	              'keterangan'=>$ket
	            );
	         $this->db1->insert('ref.registrasi_peserta_didik',$dataInsert);
	         $data['response'] = true;
		}
		
        // insert ke rombel
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
 			if(pg_escape_string(trim($dataS[$a]['jurusan_sp_id']))=="")
			{
				$js = NULL;
			}
			else
			{
				$js = pg_escape_string(trim($dataS[$a]['jurusan_sp_id']));
			}

 			$dataInsert = array(
              'rombongan_belajar_id' =>pg_escape_string(trim($dataS[$a]['rombongan_belajar_id'])),
              'semester_id' =>pg_escape_string(trim($dataS[$a]['semester_id'])),
              'sekolah_id' =>pg_escape_string(trim($dataS[$a]['sekolah_id'])),
              'tingkat_pendidikan_id' =>pg_escape_string(trim($dataS[$a]['tingkat_pendidikan_id'])),
              'jurusan_sp_id' =>$js,
              'nama' =>pg_escape_string(trim($dataS[$a]['nama'])),
              'kebutuhan_khusus_id' =>pg_escape_string(trim($dataS[$a]['kebutuhan_khusus_id']))
            	);
          $this->db1->insert('ref.rombongan_belajar',$dataInsert);
	         $data['response'] = true;
		}

		// insert ke anggota rombel 
	$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
 			
 			$dataInsert = array(
              'anggota_rombel_id'=>pg_escape_string(trim($dataS[$a]['anggota_rombel_id'])),
              'rombongan_belajar_id'=>pg_escape_string(trim($dataS[$a]['rombongan_belajar_id'])),
              'peserta_didik_id'=>pg_escape_string(trim($dataS[$a]['peserta_didik_id'])),
              'jenis_pendaftaran_id'=>pg_escape_string(trim($dataS[$a]['jenis_pendaftaran_id']))
            );
          $this->db1->insert('ref.anggota_rombel',$dataInsert);
	         $data['response'] = true;
		}


		$cksekolah = $this->db->get_where('ref.view_lulusan_kls12',array('sekolah_id'=>$schoolId))->result();
		if(count($cksekolah)>0){
			foreach ($cksekolah as $row) {
				$x = date("d-m-Y",strtotime($row->tanggal_lahir));
				$tgl = explode("-", $x);
				$a = $tgl[0];
				$b = $tgl[1];
				$c = $tgl[2];
				$thn = substr($c, 2);
				$pass = $this->bcrypt->hash_password($a.$b.$thn);
				$s_username = array('user_id'=>$row->peserta_didik_id,
					'username'=>$row->nisn,
					'password'=>$pass,
					'level'=>'3',
					'login'=>'1');
				$this->db1->insert('app.username',$s_username);
			}
		}

		$nama = "Admin BKK";
		$nip = "20171717";
		$email = "email@adminbkk.com";
		$hp = "0888888888888";
		$uname = $npsn;
		$pass = $this->bcrypt->hash_password('admin');
		$dat = array(
			'sekolah_id' => $schoolId,
			'nip' => $nip,
			'nama' => $nama,
			'email' =>$email,
			'tlp'=>$hp
			);
		$this->db1->insert('app.user_sekolah',$dat);

		$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
		$qry = $this->db1->query($q)->result();
		foreach ($qry as $key) {
			$user_id = $key->user_id;
		}

		$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
			);
		$this->db1->insert('app.username',$dd);

	}
}
