<script src="<?php echo base_url();?>assets/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/js/table-manage-responsive.demo.min.js"></script>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js"></script>
<link href="<?php echo base_url();?>assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<script type="text/javascript">
  $(document).ready(function(){
    $(function() {
        table = $('#data-user').DataTable({ 
            autoWidth: false,
            "processing": true,
            "serverSide": true,
            "responsive": true, 
            "order": [],
            "pageLength": 10,
            "ajax": {
                "url": $BASE_URL+ "nilai_lulusan/getData",
                "type": "POST"
            },
            order: [[ 0, 'desc' ]],
            dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
            displayLength: 4,               
        });
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: '60px'
        });
        $('.dataTables_filter input[type=search]').attr('placeholder','Filter Pencarian');
        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });
});
  function tambah_data(){
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Import Nilai UAN');
} 
function edit_data(page,link,action,id){ 
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $.ajax({
        url : $BASE_URL+link+"/"+action+"/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="id"]').val(data.id);
            $('[name="nisn"]').val(data.nisn);
            $('[name="namas"]').val(data.nama);
            $('[name="mat"]').val(data.mat);
            $('[name="ind"]').val(data.ind);
            $('[name="ing"]').val(data.ing);
            $('[name="prd"]').val(data.prd);
            $('#modal_nilai').modal('show');
            $('.modal-title').text('Edit Data Nilai');
            $('[name="kode"]').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}
function save(link){
    $('#btnSave').text('Proses...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    url = $BASE_URL+link+"/proses_edit";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                $('#modal_nilai').modal('hide');
                reload_table();
            }else{
                for (var i = 0; i < data.inputerror.length; i++) {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $.gritter.add({title:"Informasi !",text: "Error adding / update data."});
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        }
    });
}
function reload_table(){
    jQuery.blockUI({
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: 2, 
            color: '#fff' 
        },
        message : 'Mohon menunggu ... '
    });
    setTimeout(function () {
        table.ajax.reload(null,false);
        jQuery.unblockUI();
    }, 100);
}
</script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn">
                    <a href="nilai_lulusan/export">
                        <button class="btn btn-default btn-xs m-r-5">
                            <i class="fa fa-download"></i> Download Formulir</button></a>&nbsp;
                            <button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
                                <i class="fa fa-upload"></i> Import Nilai</button>&nbsp;
                                <button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
                                    <i class="fa fa-refresh"></i> Reload Data</button> </div> 
                                    <h4 class="panel-title"><?php echo $halaman;?></h4> </div> <div class="panel-body"> 
                                    <div class="table-responsive"> 
                                        <table id="data-user" class="table table-striped table-bordered nowrap" width="100%"> 
                                            <thead> <tr>
                                             <th style="text-align:center" width="1%">No.</th> 
                                             <th style="text-align:center" width="10%">NISN</th>
                                             <th style="text-align:center" width="50%">Nama</th>
                                             <!-- <th style="text-align:center" width="30%">Jurusan</th> -->
                                             <th style="text-align:center" width="20%">Matematika</th> 
                                             <th style="text-align:center" width="20%">B. Indonesia</th>
                                             <th style="text-align:center" width="20%">B. Inggris</th>
                                             <th style="text-align:center" width="20%">Produktif</th> 
                                             <th style="text-align:center" width="10%">Action</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
                                         </div>
                                         <div id="modal_form" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"></h4>
                                                    </div>
                                                    <div class="modal-body">        
                                                        <div class="alert alert-info alert-styled-left">
                                                            <small><span class="text-semibold">Pastikan file berekstensi <strong>.xls / .xlsx</strong> !</span></small>
                                                        </div>              
                                                        <form action="nilai_lulusan/do_upload" id="former" method="post" enctype="multipart/form-data">
                                                            <div class="row fileupload-buttonbar">
                                                                <div class="col-md-7">
                                                                    <span class="btn btn-success fileinput-button">
                                                                        <input name="file_name" multiple="" type="file">
                                                                    </span>
                                                                </div>                 </div>
                                                                <button type="submit" id="btnSave" class="btn btn-primary">Proses
                                                                    Import</button>                 </form>                              </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="modal_nilai" class="modal fade">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">Basic modal</h4>
                                                                    </div>
                                                                    <div class="modal-body">        
                                                                        <div class="alert alert-info alert-styled-left">
                                                                            <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                                                                        </div>              
                                                                        <form action="#" id="form" class="form-horizontal">
                                                                            <input type="hidden" value="" name="id"/> 
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">NISN</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="nisn" id="nisn" placeholder="Masukan NISN" class="form-control" type="text" readonly="readonly">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Nama</label>
                                                                                    <div class="col-md-6">
                                                                                        <input name="namas" id="namas" placeholder="Masukan Nama" class="form-control" type="text" readonly="readonly">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Nilai Matematika</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="mat" id="mat" maxlength="3" onkeyup="validAngka(this)" placeholder="Masukan Makan" class="form-control" type="text">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Nilai B. Indonesia</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="ind" id="ind" maxlength="3" onkeyup="validAngka(this)" placeholder="Masukan Angka" class="form-control" type="text">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Nilai B. Inggris</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="ing" id="ing" maxlength="3" onkeyup="validAngka(this)" placeholder="Masukan Angka" class="form-control" type="text">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-body">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4">Nilai Produktif</label>
                                                                                    <div class="col-md-2">
                                                                                        <input name="prd" id="prd" maxlength="3" onkeyup="validAngka(this)" placeholder="Masukan Angka" class="form-control" type="text">
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>                 
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" id="btnSave" onclick="save('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
