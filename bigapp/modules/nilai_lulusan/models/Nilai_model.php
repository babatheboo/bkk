<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_model extends CI_Model {
    var $table = 'ref.view_nilai_uan';
    var $column_order = array(null,'id','nisn','nama','mat','ind','ing','prd',null);
    var $column_search = array('nisn','nama','mat','ind','ing','prd'); 
    var $order = array('nama' => 'asc');
public function __construct(){
    parent::__construct();
        $this->db1 = $this->load->database('default', TRUE);
    }
public function upload_data($filename){
    ini_set('memory_limit', '-1');
    $inputFileName = './assets/tmp_file/'.$filename;
    try {
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
    } catch(Exception $e) {
    die('Error loading file :' . $e->getMessage());
    }
    $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
    $numRows = count($worksheet);
    for ($i= 2; $i < ($numRows+1) ; $i++) { 
        $ins = array(
                "peserta_didik_id"=>$worksheet[$i]["A"],
                "mat"=>$worksheet[$i]["E"],
                'ing'=>$worksheet[$i]["F"],
                'ind'=>$worksheet[$i]["G"],
                'prd'=>$worksheet[$i]["H"]
       );
               //  $ins = array(
               //  "peserta_didik_id"=>$worksheet[$i]["A"],
               //  "mat"=>$worksheet[$i]["D"],
               //  'ing'=>$worksheet[$i]["E"],
               //  'ind'=>$worksheet[$i]["F"],
               //  'prd'=>$worksheet[$i]["G"]
               // );
        $this->db->insert('nilai_lulusan', $ins);
    }
}
    private function _get_datatables_query(){
        $this->db->from($this->table);
        $this->db->where('sekolah_id',$this->session->userdata('role_'));
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                }else{
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $this->db->from('ref.view_nilai_uan');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function update($where, $data){
        $this->db->update('nilai_lulusan', $data, $where);
        return $this->db->affected_rows();
    }
}
