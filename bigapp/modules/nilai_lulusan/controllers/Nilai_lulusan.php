<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_lulusan extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('1');
		$this->db1 = $this->load->database('default', TRUE);
		 $this->load->library("PHPExcel");
		 $this->load->model('nilai_lulusan/nilai_model');
 	}
 	public function index(){
		$this->_content();
	}
	public function _content(){
		$this->db1->query("REFRESH MATERIALIZED VIEW ref.view_nilai_uan");
		$isi['link'] = 'nilai_lulusan';
		$isi['halaman'] = "Data nilai UAN";
		$isi['page'] = "manaj_siswa";
		$isi['judul'] = "Nilai UAN";
		$isi['content'] = "nilai_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	
	public function export(){
            $objPHPExcel = new PHPExcel();
            $id=$this->session->userdata('role_');

//            $this->db1->group_by('npsn');
			$this->db1->query("REFRESH MATERIALIZED VIEW ref.view_belum_punya_nilai");
            $sk = $this->db1->get_where('ref.sekolah',array('sekolah_id'=>$id))->result();
            foreach ($sk as $keys) {
            	$sekolah = $keys->nama;
            }

            // $data = $this->db1->query("SELECT peserta_didik_id,nisn,nama_jurusan_sp,mat,ing,ind,prd FROM ref.view_belum_punya_nilai WHERE sekolah_id='$id'");
            $data = $this->db1->query("SELECT peserta_didik_id,nisn,nama,nama_jurusan,mat,ing,ind,prd FROM ref.view_belum_punya_nilai WHERE sekolah_id='$id' ORDER BY nama_jurusan ASC");
            $fields = $data->list_fields();
            $col = 0;
            foreach ($fields as $field){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
                $col++;
            }
            // Mengambil Data
            $row = 2;
            foreach($data->result() as $data){
                $col = 0;
                foreach ($fields as $field){
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                    $col++;
                }
                $row++;
            }
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle('Form Pengisian Nilai UAN');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            //Header
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $nfile = "Form Nilai Uan-" . $sekolah . ".xlsx";
            //Nama File
            header('Content-Disposition: attachment;filename="' . $nfile . '"');
            //Download
            $objWriter->save("php://output");

	}

	public function do_upload(){
		$config['upload_path'] = './assets/tmp_file/';
        $config['allowed_types'] = 'xlsx|xls';
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('file_name')){
            // $error = array('error' => $this->upload->display_errors());
            // print_r($error);
            ?>
            <script type="text/javascript">
            	alert("upload file gagal.Silahkan Coba Lagi.")
            	window.location.href="<?php echo base_url();?>nilai_lulusan";
            </script>
            <?php
        }else{
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->nilai_model->upload_data($filename);
            unlink('./assets/tmp_file/'.$filename);
            redirect('nilai_lulusan','refresh');
        }
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->nilai_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->nisn;
				$row[] = $rowx->nama;
				// $row[] = $rowx->nama_jurusan_sp;
				$row[] = '<center>'.$rowx->mat.'</center>';
				$row[] = '<center>'.$rowx->ind.'</center>';
				$row[] = '<center>'.$rowx->ing.'</center>';
				$row[] = '<center>'.$rowx->prd.'</center>';
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'nilai_lulusan\',\'edit_data\','."'".$rowx->id."'".')"><i class="icon-pencil"></i></a>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->nilai_model->count_all(),
				"recordsFiltered" => $this->nilai_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}
 public function edit_data($id){
		if($this->input->is_ajax_request()){
			$data = $this->nilai_model->get_by_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}
public function proses_edit(){
		if($this->input->is_ajax_request()){
			$method = "edit";
	       	$mat = $this->input->post('mat');
			$ind = $this->input->post('ind');
	        $ing = $this->input->post('ing');
	        $prd = $this->input->post('prd');
	        $data = array('mat' => $mat,
	        	'ind'=>$ind,
	        	'ing'=>$ing,
	        	'prd'=>$prd
	        );
	        $this->nilai_model->update(array('id' => $this->asn->anti($this->input->post('id'))), $data);
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
}
