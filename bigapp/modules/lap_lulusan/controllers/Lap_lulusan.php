<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lap_lulusan extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->my->login('4');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
 	}

 	public function index()
 	{
 		$isi['namamenu'] = "BKK";
		$isi['page'] = "lap";
		$isi['kelas'] = "registrasi";
		$isi['link'] = 'reg_bkk';
		$isi['halaman'] = "Laporan";
		$isi['judul'] = "Lulusan";
		$isi['content'] = "lap_view";
       	$this->load->view("dashboard/dashboard_view",$isi);
 	}

}