<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Caker extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->asn->login(); // cek session login
  		if($this->session->userdata('level')==1){
	  		$this->asn->role_akses("1");
  		}elseif($this->session->userdata('level')==2){
	  		$this->asn->role_akses("2");
  		}
  		$this->load->model('caker_model');
  		$this->load->model('riwayat_model');
  		date_default_timezone_set('Asia/Jakarta');
  		$this->db1 = $this->load->database('default', TRUE);
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['namamenu'] = "Data Pelamar";
		$isi['halaman']  = "Data Pelamar";
		$isi['judul']    = "Halaman Data Pelamar";
		$isi['page']     = "bursa";
		$isi['link']     = 'loker';
		$sess_sekolah    = $this->session->userdata('role_');
		$thn             = $this->db1->query("SELECT left(tanggal_keluar,4) as tgl_keluar FROM ref.peserta_didik WHERE sekolah_id = '$sess_sekolah' AND jenis_keluar_id = '1' GROUP BY tanggal_keluar ORDER BY tanggal_keluar ASC")->result();
		foreach ($thn as $row) {
			$isi['option_tahun_keluar'][$row->tgl_keluar] = $row->tgl_keluar;
		}
		$isi['option_tahun_keluar'][''] = "Semua Tahun Lulusan";
//		$isi['content']                 = "_content";
$sid = $this->session->userdata('role_');
        $b = $this->db->query("SELECT valid FROM sekolah_terdaftar WHERE sekolah_id='$sid'")->result();
        $valid = "";
        foreach ($b as $key) {
          $valid = $key->valid;
        }
        if($valid=="1"){
          $isi['content']              = "_content";
        } else {
          $isi['content']              = "admin_bkk";
        }

		$this->load->view('dashboard/dashboard_view',$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->caker_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->nisn;
				$row[] = $rowx->nama;
				$row[] = $rowx->jenis_kelamin;
				$row[] = $rowx->tanggal_keluar;
				$row[] = '<center><a class="btn btn-xs m-r-5 btn-primary" href="'.base_url().'caker/detil_pelamar/'.$rowx->peserta_didik_id.'" title="Info Pelamar"><i class="fa fa-search-plus"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->caker_model->count_all(),
				"recordsFiltered" => $this->caker_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }		
	}
	public function getData_riwayat(){
		if($this->input->is_ajax_request()){
			$list = $this->riwayat_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->judul;
				$row[] = $rowx->nama_perusahaan;
				$row[] = date("d-m-Y",strtotime($rowx->tgl_bid));
				$status = $rowx->status;
				// 0=Tolak,1=BaruBid,2=Panggilan,3=Tes,4=Terima
				switch ($status) {
					case '1':
						$statusna = "BARU MELAMAR";
						break;
					case '2':
						$statusna = "PANGGILAN";
						break;
					case '3':
						$statusna = "MENGIKUTI JADWAL TES";
						break;
					case '4':
						$statusna = "DITERIMA";
						break;
					default:
						$statusna = "TIDAK DITERIMA";
						break;
				}
				$row[] = $statusna;
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->riwayat_model->count_all(),
				"recordsFiltered" => $this->riwayat_model->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }		
	}
	public function detil_pelamar($id){
		if($this->session->userdata('level')!=""){
			$cksiswa = $this->db->get_where('ref.peserta_didik',array('peserta_didik_id'=>$id));
			if(count($cksiswa->result())>0){
				$isi['tombolsimpan'] = 'Simpan';
				$isi['namamenu'] = "Input Loker";
				$isi['page'] = "bursa";
				$isi['kode'] = $id;
				$this->session->set_userdata('id_siswa_sess_',$id);
				$isi['jml_kompetensi'] = $this->db->get_where('kompetensi_siswa',array('user_id'=>$id))->num_rows();
				$this->session->set_userdata('id_pelamar',$id);
				$isi['link'] = 'loker';
				$isi['tombolbatal'] = 'Batal';
				$isi['halaman'] = "Detil Pelamar";
				$isi['judul'] = "Halaman Detil Pelamar";
				$isi['content'] = "cv";
				$this->load->view("dashboard/dashboard_view",$isi);
			}else{
				redirect("_404","refresh");
			}
		}else{
			redirect("_404","refresh");
		}
	}
}
