<link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.css">
<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/theme/table-manage-responsive.demo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/h_lamar.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/form-plugins.min.js"></script>
<script>
    $(document).ready(function() {
        FormPlugins.init();
    });
</script>
<?php
$cksiswa = $this->db->query("SELECT * FROM ref.peserta_didik a JOIN ref.jurusan_sp b ON a.jurusan_id = b.jurusan_id WHERE a.peserta_didik_id = '$kode'");
$row = $cksiswa->row();
$ckjrs = $this->db->get_where('ref.jurusan_sp',array('jurusan_id'=>$row->jurusan_id));
$a = $ckjrs->row();
$ckdetilsiswa = $this->db->query("SELECT * FROM ref.peserta_didik_detil WHERE peserta_didik_id = '$kode'");
$xx = $ckdetilsiswa->row();
$xs = $ckdetilsiswa->result();
if(count($cksiswa->result())>0){
    $foto = $row->photo;
    if($foto!=NULL){
        $fotox = $foto;
    }else{
        $fotox = "no.jpg";
    }
}else{
    $fotox = "no.jpg";
}
$nama_siswa = $row->nama;
$umur = $this->db->query("SELECT date_part('year'::text, age((b.tanggal_lahir)::timestamp with time zone)) AS umur FROM ref.peserta_didik b WHERE peserta_didik_id='$kode'");
$xu = $umur->row();
?>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="panel panel-inverse"> 
            <div class="panel-heading"> 
                <div class="panel-heading-btn"> 
                </div> 
                <h4 class="panel-title">Curicullum Vitae</h4> 
            </div> 
        </div>
        <div class="profile-container">
            <div class="profile-section">
                <div class="profile-left">
                    
                </div>
                <div class="profile-center">
                    <div class="profile-info">
                        <div class="table-profile">
                            <table class="table-bordered dataTable no-footer dtr-inline" aria-describedby="data-table-responsive_info" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                            <img src="<?php echo base_url();?>assets/foto/siswa/<?php echo $fotox;?>" class="profile-center"/>
                                            <i class="fas fa-user hide"></i>
                                            <h4 class="text-center"><?php echo $row->nama;?></h4>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr class="highlight">
                                    <td class="field">Tempat Lahir</td>
                                    <td><?php echo $row->tempat_lahir;?></td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Tanggal Lahir</td>
                                    <?php
                                    if ($row->tanggal_lahir==null) {
                                        echo "<td class='text-danger'>TIDAK TERCANTUM</td>";
                                    }else{
                                        echo "<td>".date('d-m-Y',strtotime($row->tanggal_lahir))."</td>";
                                    }
                                    ?>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Umur</td>
                                    <td><?php echo $xu->umur . " Tahun";?></td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Jenis Kelamin</td>
                                    <td><?php echo $row->jenis_kelamin;?></td>
                                </tr>
                                <?php
                                if (count($xs)>0) {
                                    ?>
                                    <tr class="highlight">
                                        <td class="field">Alamat</td>
                                        <td><?php echo $xx->alamat_jalan . ' RT.' . $xx->rt . " RW." . $xx->rw;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Nama Dusun</td>
                                        <td><?php echo $xx->nama_dusun;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Kelurahan</td>
                                        <td><?php echo $xx->desa_kelurahan;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">Kode POS</td>
                                        <td><?php echo $xx->kode_pos;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">No Tlp Rumah</td>
                                        <td><?php echo $xx->nomor_telepon_rumah;?></td>
                                    </tr>
                                    <tr class="highlight">
                                        <td class="field">No Tlp Seluler</td>
                                        <td><?php echo $xx->nomor_telepon_seluler;?></td>
                                    </tr>
                                    <?php
                                }else{
                                 ?>
                                 <tr class="highlight">
                                    <td class="field">Alamat</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Nama Dusun</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Kelurahan</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">Kode POS</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">No Tlp Rumah</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="field">No Tlp Seluler</td>
                                    <td class="text-danger">TIDAK TERCANTUM</td>
                                </tr>
                            <?php }
                            ?>
                            <tr class="highlight">
                                <td class="field">E-mail</td>
                                <?php
                                if ($row->email == '') {
                                    echo "<td class='text-danger'>TIDAK TERCANTUM</td>";
                                }else{
                                    echo '<td>'.$row->email.'</td>';
                                }
                                ?>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Jurusan</td>
                                <td><?php echo $a->nama_jurusan_sp;?></td>
                            </tr>
                            <tr class="highlight">
                                <td class="field">Status Siswa</td>
                                <td>
                                    <?php
                                    if($row->jenis_keluar_id=='1'){
                                        echo "Lulus";
                                    }else{
                                        echo "Pelajar";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="divider">
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12"> <div class="panel panel-inverse"> <div class="panel-heading"> <div class="panel-heading-btn"></div> <h4 class="panel-title">Riwayat Melamar Pekerjaan</h4>
    <div id="filter_pencarian">
    </div>
</div> <div class="panel-body"> <div class="table-responsive"> <table id="data-riwayat" class="table table-striped table-bordered nowrap" width="100%"> <thead> <tr> <th style="text-align:center" width="1%">No.</th> <th style="text-align:center" width="10%">Judul Lowongan</th> <th style="text-align:center" width="40%">Nama Perusahaan</th> <th style="text-align:center" width="5%">Tanggal Melamar</th> <th style="text-align:center" width="30%">Status</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>
