<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Riwayat_model extends CI_Model {
    var $table = 'bidding_lulusan';
    var $column_order = array(null,'judul','nama_perusahaan','tgl_bid',"status",null);
    var $column_search = array('judul','nama_perusahaan','tgl_bid',"right(status,4)"); 
    var $order = array('bidding_lulusan.tgl_bid' => 'ASC');
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    private function _get_datatables_query(){
        $sess_siswa = $this->session->userdata('id_siswa_sess_');
        $this->db->select('lowongan.judul,ref.industri.nama as nama_perusahaan,bidding_lulusan.tgl_bid,bidding_lulusan.status');
        $this->db->from($this->table);
        $this->db->join('lowongan','bidding_lulusan.id_loker=lowongan.id');
        $this->db->join('ref.industri',"(lowongan.kode_perusahaan::uuid) = ref.industri.industri_id");
        $this->db->where('bidding_lulusan.id_siswa',$sess_siswa);
        $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }
}