<link href="<?php echo base_url();?>assets/backend/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/manaj_users_bkk.js"></script>
<div class="row"> <div class="col-md-12"> <div class="panel panel-inverse"> 
<div class="panel-heading"> <div class="panel-heading-btn">
<?php
$sid = $this->session->userdata('role_');
$ck = $this->db->query("SELECT npsn FROM ref.sekolah WHERE sekolah_id='$sid'")->result();
$lv = "";
foreach ($ck as $key) {
    $lv = $key->npsn;
}
$un =  $this->session->userdata('username');
$data = $this->db->query("SELECT * FROM app.user_sekolah WHERE sekolah_id='$sid'")->result();
if(count($data)<4 && $lv==$un){
?>
<button class="btn btn-primary btn-xs m-r-5" onclick="tambah_data()">
<i class="fa fa-plus-circle"></i> Tambah Data</button>&nbsp;
<?php
}
?>


<button class="btn btn-warning btn-xs m-r-5" onclick="reload_table()">
<i class="fas fa-sync-alt"></i> Reload Data</button> </div> 
<h4 class="panel-title"><?php echo $halaman;?></h4> </div> <div class="panel-body"> 
<div class="table-responsive"> 
<table id="data-user" class="table table-striped table-bordered nowrap" width="100%"> 
<thead> <tr>
  <th style="text-align:center" width="1%">No.</th> 
  <th style="text-align:center" width="10%">Username</th>
  <th style="text-align:center" width="50%">Nama</th>
  <th style="text-align:center" width="50%">BKK Sekolah</th>
  <th style="text-align:center" width="30%">E-mail</th>
  <th style="text-align:center" width="20%">Telepon</th> 
  <th style="text-align:center" width="10%">Action</th> </tr> </thead> <tbody> </tbody> </table> </div> </div> </div> </div>
</div>
<div id="modal_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Basic modal</h4>
            </div>
            <div class="modal-body">        
                <div class="alert alert-info alert-styled-left">
                    <small><span class="text-semibold">Pastikan Inputan Data Benar !</span></small>
                </div>              
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">Nama</label>
                            <div class="col-md-6">
                                <input name="nama" id="nama" placeholder="Masukan Nama Pengguna BKK" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">E-mail</label>
                            <div class="col-md-6">
                                <input name="mail" id="mail" placeholder="Masukan E-mail Pengguna BKK" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">No Telepon</label>
                            <div class="col-md-6">
                                <input name="tlp" id="tlp" placeholder="Masukan No Telepon" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">Username</label>
                            <div class="col-md-6">
                                <input name="username" id="username" placeholder="Masukan Username" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">Password</label>
                            <div class="col-md-6">
                                <input name="password" id="password" placeholder="Masukan Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    
                </form>                 
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save('<?php echo $link;?>')" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
