<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_bkk extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		date_default_timezone_set('Asia/Jakarta');
		$this->asn->login();
  		$this->asn->role_akses('1');
		$this->load->model('u_bkk');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['link'] = 'users_bkk';
		$isi['halaman'] = "Manajemen User";
		$isi['page'] = "setup";
		$isi['judul'] = "Halaman Pengguna BKK";
		$isi['content'] = "users_bkk_view";
		$this->load->view("dashboard/dashboard_view",$isi);
	}
	public function getData(){
		if($this->input->is_ajax_request()){
			$list = $this->u_bkk->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $rowx) {
				$no++;
				$row = array();
				$row[] = $no . ".";
				$row[] = $rowx->username;
				$row[] = $rowx->nama;
				$row[] = $rowx->nskolah;
				$row[] = $rowx->email;
				$row[] = $rowx->tlp;
				$row[] = '<center><a href="javascript:void(0)" title="Edit Data" onclick="edit_data(\'Data User\',\'users_bkk\',\'edit_data\','."'".$rowx->id."'".')"><i class="far fa-lg fa-fw m-r-10 fa-edit"></i></a><a style="color:red"  href="javascript:void(0)" title="Hapus Data" onclick="hapus_data(\'Data User\',\'users_bkk\',\'hapus_data\','."'".$rowx->id."'".')"><i class="fas fa-lg fa-fw m-r-10 fa-trash"></i></a></center>';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->u_bkk->count_all(),
				"recordsFiltered" => $this->u_bkk->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
	    }else{
	    	redirect("_404","refresh");
	    }
	}
	public function proses_add(){
		if($this->input->is_ajax_request()){
			$method = "save";
	        $this->_validasi($method);
			// $nip = $this->input->post('nip');
			$tlp = $this->input->post('tlp');
			$password = $this->input->post('password');
			$username = $this->input->post('username');
	        $nama = $this->input->post('nama');
	        $email = $this->input->post('mail');
	        $sess_skolah = $this->session->userdata('role_');
	        $data = array('sekolah_id' => $sess_skolah,
	        	// 'nip'=>trim($nip),
	        	'nama'=>htmlspecialchars($nama),
	        	'email'=>$email,
	        	'tlp' =>$tlp,
	        );
			$this->db->insert('app.user_sekolah',$data);
	        $q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
			$qry = $this->db->query($q);
			$key = $qry->row();
			$user_id = $key->user_id;
			//$pass = $this->bcrypt->hash_password($password);
			$pass = md5(md5($password));			
			$dd = array('user_id' => $user_id ,
				'username'=>trim($username),
				'password'=>$pass,
				'level' => '1',
				'login' => '1' 
			);
			$this->db->insert('app.username',$dd);
			$this->db->query('refresh materialized view app.view_user_sekolah');
	        echo json_encode(array("status" => TRUE));
        }else{
	    	redirect("_404","refresh");
	    }
    }
    private function _validasi($method){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
    	$nip = trim($this->input->post('nip'));
        if($method=="save"){
	        $sess_skolah = $this->session->userdata('role_');
        	$ckdata = $this->db->query("SELECT * FROM app.user_sekolah WHERE sekolah_id = '$sess_skolah'")->result();
        	if(count($ckdata)>=4){
	            $data['inputerror'][] = 'username';
	            $data['error_string'][] = 'akses user sudah batas minimum.';
	            $data['status'] = FALSE;
        	}else{
        		$ckdata = $this->db->query("SELECT * FROM app.user_sekolah WHERE nip = '$nip'")->result();
	        	if(count($ckdata)>0){
		            $data['inputerror'][] = 'nip';
		            $data['error_string'][] = 'nip sudah terdaftar sebelumnya.';
		            $data['status'] = FALSE;
		        }
        	}
	        $user = $this->input->post('username');
	        $ckusername = $this->db->query("SELECT * FROM app.username WHERE username = '$user'")->result();
	    	if(count($ckusername)>0){
	            $data['inputerror'][] = 'username';
	            $data['error_string'][] = 'username sudah terdaftar sebelumnya.';
	            $data['status'] = FALSE;
	        }
	 
	        if($this->input->post("username") == ''){
	            $data['inputerror'][] = 'username';
	            $data['error_string'][] = 'username harus di isi.';
	            $data['status'] = FALSE;
	        }
	        
    	}
    	if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'nama harus di isi.';
            $data['status'] = FALSE;
        }
        if($this->input->post('mail') == ''){
            $data['inputerror'][] = 'mail';
            $data['error_string'][] = 'email harus di isi.';
            $data['status'] = FALSE;
        }
    	if (!filter_var($this->input->post('mail'), FILTER_VALIDATE_EMAIL)) {
		  	$data['inputerror'][] = 'mail';
            $data['error_string'][] = 'penulisan email tidak valid.';
            $data['status'] = FALSE;
		}
        if($this->input->post('tlp') == ''){
            $data['inputerror'][] = 'tlp';
            $data['error_string'][] = 'no tlp harus di isi.';
            $data['status'] = FALSE;
        }
    	if (!is_numeric($this->input->post('tlp'))){
			$data['inputerror'][] = 'tlp';
            $data['error_string'][] = 'no tlp hanya karakter angka.';
            $data['status'] = FALSE;
	    }
        /*if(Strlen($this->input->post('username') <= 5)){
        	$data['inputerror'][] = 'username';
            $data['error_string'][] = 'username minimal 6 karakter.';
            $data['status'] = FALSE;	
        }*/
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }
	public function proses_edit(){
		if($this->input->is_ajax_request()){
			$method = "edit";
	        $this->_validasi($method);
	       	// $nip = $this->input->post('nip');
			$tlp = $this->input->post('tlp');
	        $nama = $this->input->post('nama');
	        $email = $this->input->post('mail');
	        $sess_skolah = $this->session->userdata('role_');
	        $data = array('sekolah_id' => $sess_skolah,
	        	// 'nip'=>$nip,
	        	'nama'=>htmlspecialchars($nama),
	        	'email'=>$email,
	        	'tlp' =>$tlp,
	        );
	        if($this->input->post('password')!=""){
	        	$password = $this->input->post('password');
			//	$pass = $this->bcrypt->hash_password($password);
$pass = md5(md5($password));	        	
$dd = array('password'=>$pass);
	        	$ckuser = $this->db->get_where('app.user_sekolah',array('id'=>$this->input->post('id')));
	        	$x = $ckuser->row();
	        	$id_user = $x->user_id;
				$this->db->where('user_id',$id_user);
				$this->db->update('app.username',$dd);
	        }
	        $this->u_bkk->update(array('id' => $this->asn->anti($this->input->post('id'))), $data);
			$this->db->query('refresh materialized view app.view_user_sekolah');
	        echo json_encode(array("status" => TRUE));
	    }else{
	    	redirect("_404","refresh");
	    }
    }
	public function hapus_data($id){
		if($this->input->is_ajax_request()){
			$ckuserid = $this->db->query("SELECT user_id FROM app.user_sekolah WHERE id = '$id'");
			if(count($ckuserid->result())>0){
				$row = $ckuserid->row();
				if($this->session->userdata('user_id')!=$row->user_id){
					$this->u_bkk->hapus_user_id($row->user_id);
			        $this->u_bkk->hapus_by_id($id);
					$this->db->query('refresh materialized view app.view_user_sekolah');
			        echo json_encode(array("status" => TRUE));
				}else{
			        echo json_encode(array("status" => FALSE));
				}
			}
	    }else{
	    	redirect("_404","refresh");
	    }
    }
    public function edit_data($id){
		if($this->input->is_ajax_request()){
			$data = $this->u_bkk->get_by_id($id);
			echo json_encode($data);
		}else{
			redirect("_404","refresh");
		}
	}
}
