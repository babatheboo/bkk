<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Daftar_siswa extends CI_Controller {
	public function __construct(){
  		parent::__construct();
  		$this->load->library('bcrypt');
		//$this->db1 = $this->load->database('default', TRUE);
		//$this->load->model('Daftar_siswa_model');
 	}
	public function index(){
		$this->_content();
	}
	public function _content(){
		$isi['page'] = "daftar_siswa";
		$isi['link'] = 'daftar_siswa';
		$isi['halaman'] = "Formulir Pendaftaran (Siswa/Lususan)";
		$isi['judul'] = "Pendaftaran (Siswa/Lususan)";
		$isi['content'] = "daftar_siswa_view";

		$isi['tombolsimpan'] = "Daftar";
		$isi['tombolbatal'] = "Batal";
		$isi['action'] = "daftar_siswa";

		$nisn= $this->input->post('nisn');
		$nama="";
		$nama_sekolah="";
		if($nisn != ''){
			$q_nisn = $this->db->query("select * from ref.view_semua where nisn = '".$nisn."' ")->result();
			foreach($q_nisn as $key){
				$nama=$key->nama;
				$nama_sekolah=$key->nama_sekolah;
			}

			$isi['default']['nama']=$nama;
			$isi['default']['nama_sekolah']=$nama_sekolah;
		}

		//$isi['list_nisn']['']="NISN";
        //$q_list_nisn = $this->db->query("select * from ref.view_semua order by nisn asc")->result();
		//foreach($q_list_nisn as $key){
		//	$isi['list_nisn'][$key->nisn]=$key->nisn;
		//}

		$this->load->view("daftar_siswa/daftar_siswa_view",$isi);
	}
	public function getData(){
			
	}

	public function cek_nisn($kode=Null){
		$ckdata = $this->db->query("select * from ref.view_semua where nisn='".$kode."'")->result();
		if(count($ckdata)==1){
			$data['say'] = "ok";
			$sekolah_id="";

			foreach($ckdata as $key){
				$data['nama']=$key->nama;
				$data['nama_sekolah']=$key->nama_sekolah;
				$data['peserta_didik_id']=$key->peserta_didik_id;
				$sekolah_id=$key->sekolah_id;
			}
			$qr="select * from ref.view_semua join public.sekolah_terdaftar on ref.view_semua.sekolah_id=public.sekolah_terdaftar.sekolah_id where ref.view_semua.sekolah_id='".$sekolah_id."' and ref.view_semua.nisn='".$kode."'";
			$ckdata2 = $this->db->query($qr)->result();
			if(count($ckdata2)==1){
				$data['sekolah_terdaftar']="terdaftar";
			}else{
				$data['sekolah_terdaftar']="Belum Terdaftar";
			}
		}else{
			$data['say'] = "NotOk";
		}
		if('IS_AJAX'){
		    echo json_encode($data); //echo json string if ajax request
		}  
	}

	public function daftar(){
		$pass = $this->bcrypt->hash_password($this->input->post('update_password'));
		$ckdata = $this->db->query("update app.username set password='".$pass."' where user_id='".$this->input->post('peserta_didik_id')."'");
		$isi['page'] = "daftar_siswa";
		$isi['link'] = 'daftar_siswa';
		$isi['halaman'] = "Formulir Pendaftaran (Siswa/Lususan)";
		$isi['judul'] = "Pendaftaran (Siswa/Lususan)";
		$isi['content'] = "daftar_siswa_view";

		$isi['tombolsimpan'] = "Daftar";
		$isi['tombolbatal'] = "Batal";
		$isi['action'] = "daftar_siswa";

		$this->load->view("daftar_siswa/daftar_siswa_sukses_view",$isi);
	}
}
