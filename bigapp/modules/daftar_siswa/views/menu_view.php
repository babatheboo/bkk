<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
    <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url();?>" class="animate">Beranda</a></li>
        <li class="dropdown active"><a href="<?php echo base_url();?>registrasi" class="dropdown-toggle" data-toggle="dropdown">Daftar</a>
            <ul class="dropdown-menu animated fadeInLeft">
                <li class="active"><a href="<?php echo base_url();?>daftar_siswa" class="animate">Siswa</a></li>
                <li ><a href="<?php echo base_url();?>daftar_sekolah" class="animate">BKK</a></li>
            </ul>
        </li>
        <li ><a href="<?php echo base_url();?>sebaran" class="animate">Sebaran</a></li>
        <li ><a href="<?php echo base_url();?>about" class="animate">Tentang BKK</a></li>
        <li ><a href="<?php echo base_url();?>dokumen/manual.pdf" class="animate">Dokumen</a></li>
        <li ><a href="<?php echo base_url();?>statistik" class="animate">Statistik</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <li ><a href="<?php echo base_url();?>login" class="animate">Login</a></li>
        </li>
    </ul>
</div>