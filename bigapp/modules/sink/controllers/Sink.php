<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sink extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('reg_bkk/reg_sekolah_model');
 	}

 	function tambah($schoolId)
	{
/* Variable */
		$hasil = array();
		$lengkap = array();
		$dumpJurSp = array();
		$dumpRegPst = array();
		$dumpPeserta = array();
		$dumpAromBel = array();
		$dumpRomBel = array();

		$npsn="";
		$sd = $this->db1->query("SELECT * FROM ref.sekolah WHERE sekolah_id='$schoolId'")->result();
		foreach ($sd as $key) {
			$npsn = $key->npsn;
		}
/* jurusan sp */
		$jsondata = file_get_contents('http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id='.$schoolId);
        $dataS = json_decode($jsondata, true);
        if(count($dataS)>0)
        {
        	for($a=0;$a<count($dataS);$a++)
	        {
	          $dataInsert = array(
	              'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
	              'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
	              'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
	              'jurusan_id'=>trim($dataS[$a]['jurusan_id']),
	              'nama_jurusan_sp'=>trim($dataS[$a]['nama_jurusan_sp'])
	            );

	           array_push($dumpJurSp, $dataInsert);
	        }
	            unset($dataInsert);
				unset($dataS);
        }
        else
        {
        	echo "Koneksi ke jurusan sp gagal ! \n" ;
        	unset($dataS);
        	exit;
        }
	    
/* registrasi peserta didik */
        $jsondata = file_get_contents('http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id='.$schoolId);
        $dataS = json_decode($jsondata, true);
        if(count($dataS)>0)
        {
		        for($a=0;$a<count($dataS);$a++)
		        {
		          if(trim($dataS[$a]['jenis_keluar_id'])=="")
		          {
		          	$jk = NULL;
		          }
		          else
		          {
		          	$jk = trim($dataS[$a]['jenis_keluar_id']);
		          }
		          if(trim($dataS[$a]['tanggal_masuk_sekolah'])=="")
		          {
		          	$tm = NULL;
		          }
		          else
		          {
		          	$tm = trim($dataS[$a]['tanggal_masuk_sekolah']);
		          }
		          if(trim($dataS[$a]['tanggal_keluar'])=="")
		          {
		          	$tk = NULL;
		          }
		          else
		          {
		          	$tk = trim($dataS[$a]['tanggal_keluar']);
		          }

		          $dataInsert = array(
		              'registrasi_id' =>trim($dataS[$a]['registrasi_id']),
		              'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
		              'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
		              'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
		              'jenis_pendaftaran_id' =>trim($dataS[$a]['jenis_pendaftaran_id']),
		              'nipd' =>trim($dataS[$a]['nipd']),
		              'tanggal_masuk_sekolah' =>$tm,
		              'jenis_keluar_id' =>$jk,
		              'tanggal_keluar' =>$tk,
		              'keterangan'=>trim($dataS[$a]['keterangan'])
		            );

		          	array_push($dumpRegPst, $dataInsert);
		        }
				
				$hasil = $this->inner_join($dumpRegPst,$dumpJurSp,'jurusan_sp_id');
				unset($dataInsert);
		        unset($dataS);

		    }
	        else
	        {
	        	echo "Koneksi registrasi peserta didik gagal ! \n" ;
	        	unset($dataS);
	        	exit;
	        }

/* peserta didik */
        $jsondata = file_get_contents('http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresep&sekolah_id='.$schoolId);
        $dataS = json_decode($jsondata, true);  
        if(count($dataS)>0)
        {

	        for($a=0;$a<count($dataS);$a++)
	        {
	          $dataInsert = array(
	              'peserta_didik_id' =>trim($dataS[$a]['peserta_didik_id']),
	              'nama' =>trim($dataS[$a]['nama']),
	              'jenis_kelamin' =>trim($dataS[$a]['jenis_kelamin']),
	              'nisn' =>trim($dataS[$a]['nisn']),
	              'nik' =>trim($dataS[$a]['nik']),
	              'tempat_lahir' =>trim($dataS[$a]['tempat_lahir']),
	              'tanggal_lahir' =>trim($dataS[$a]['tanggal_lahir']),
	              'agama_id' =>trim($dataS[$a]['agama_id']),
	              'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id']),
	              'alamat_jalan' =>trim($dataS[$a]['alamat_jalan']),
	              'rt' =>trim($dataS[$a]['rt']),
	              'rw' =>trim($dataS[$a]['rw']),
	              'nama_dusun' =>trim($dataS[$a]['nama_dusun']),
	              'desa_kelurahan' =>trim($dataS[$a]['desa_kelurahan']),
	              'kode_wilayah' =>trim($dataS[$a]['kode_wilayah']),
	              'kode_pos' =>trim($dataS[$a]['kode_pos']),
	              'nomor_telepon_rumah' =>trim($dataS[$a]['nomor_telepon_rumah']),
	              'nomor_telepon_seluler' =>trim($dataS[$a]['nomor_telepon_seluler']),
	              'email' =>trim($dataS[$a]['email']),
	              'nik_ayah' =>trim($dataS[$a]['nik_ayah']),
	              'nik_ibu' =>trim($dataS[$a]['nik_ibu']),
	              'penerima_KPS' =>trim($dataS[$a]['penerima_KPS']),
	              'no_KPS' =>trim($dataS[$a]['no_KPS']),
	              'layak_PIP' =>trim($dataS[$a]['layak_PIP']),
	              'penerima_KIP' =>trim($dataS[$a]['penerima_KIP']),
	              'no_KIP'=>trim($dataS[$a]['no_KIP']),
	              'nm_KIP' =>trim($dataS[$a]['nm_KIP']),
	              'no_KKS' =>trim($dataS[$a]['no_KKS']),
	              'id_layak_pip' =>trim($dataS[$a]['id_layak_pip']),
	              'nama_ayah' =>trim($dataS[$a]['nama_ayah']),
	              'tahun_lahir_ayah' =>trim($dataS[$a]['tahun_lahir_ayah']),
	              'jenjang_pendidikan_ayah' =>trim($dataS[$a]['jenjang_pendidikan_ayah']),
	              'pekerjaan_id_ayah' =>trim($dataS[$a]['pekerjaan_id_ayah']),
	              'penghasilan_id_ayah' =>trim($dataS[$a]['penghasilan_id_ayah']),
	              'nama_ibu_kandung' =>trim($dataS[$a]['nama_ibu_kandung']),
	              'tahun_lahir_ibu' =>trim($dataS[$a]['tahun_lahir_ibu']),
	              'jenjang_pendidikan_ibu' =>trim($dataS[$a]['jenjang_pendidikan_ibu']),
	              'penghasilan_id_ibu' =>trim($dataS[$a]['penghasilan_id_ibu']),
	              'pekerjaan_id_ibu' =>trim($dataS[$a]['pekerjaan_id_ibu'])
	            );
				array_push($dumpPeserta, $dataInsert);
	        }

       		$lengkap = $this->inner_join($dumpPeserta,$hasil,'peserta_didik_id');
       		unset($dataInsert);
		    unset($dataS);
		}
	    else
	    {
	    	echo "Koneksi ke peserta didik gagal ! \n" ;
	        unset($dataS);
	       exit;
		}

/*  anggota rombel */
	 	$jsondata = file_get_contents('http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id='.$schoolId);
        $dataS = json_decode($jsondata, true);
        if(count($dataS)>0)
        {
	        for($a=0;$a<count($dataS);$a++)
	        {
	          $dataInsert = array(
	              'anggota_rombel_id'=>trim($dataS[$a]['anggota_rombel_id']),
	              'rombongan_belajar_id'=>trim($dataS[$a]['rombongan_belajar_id']),
	              'peserta_didik_id'=>trim($dataS[$a]['peserta_didik_id']),
	              'jenis_pendaftaran_id'=>trim($dataS[$a]['jenis_pendaftaran_id'])
	            );
	          	array_push($dumpAromBel, $dataInsert);
	        }
	        unset($dataInsert);
		    unset($dataS);  
        }
	    else
	    {
	    	echo "Koneksi gagal anggota rombel gagal ! \n" ;
	        unset($dataS);
	        exit;
		}

/*  Rombongan Belajar */
	 	$jsondata = file_get_contents('http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id='.$schoolId);
        $dataS = json_decode($jsondata, true);
        
        if(count($dataS)>0)
        {
	        for($a=0;$a<count($dataS);$a++)
	        {
	          $dataInsert = array(
	              'rombongan_belajar_id' =>trim($dataS[$a]['rombongan_belajar_id']),
	              'semester_id' =>trim($dataS[$a]['semester_id']),
	              'sekolah_id' =>trim($dataS[$a]['sekolah_id']),
	              'tingkat_pendidikan_id' =>trim($dataS[$a]['tingkat_pendidikan_id']),
	              'jurusan_sp_id' =>trim($dataS[$a]['jurusan_sp_id']),
	              'nama' =>trim($dataS[$a]['nama']),
	              'kebutuhan_khusus_id' =>trim($dataS[$a]['kebutuhan_khusus_id'])
	            );
	          array_push($dumpRomBel, $dataInsert);
	        }
	        unset($dataInsert);
		    unset($dataS);
	     }
	    else
	    {
	    	echo "Koneksi gagal ! \n" ;
	        unset($dataS);
	        exit;
		}

/* 1. insert data to jurusan sp */
	$dt = $this->db1->query("SELECT * FROM ref.jurusan_sp WHERE sekolah_id='$schoolId'");
	if($dt->num_rows()>0)
	{
		$this->db1->where('sekolah_id',$schoolId);
		$this->db1->delete('ref.jurusan_sp');
	}
	$dt->free_result();

	foreach ($dumpJurSp as $key) {
		$id = trim($key['jurusan_sp_id']);
		$dt = $this->db1->query("SELECT * FROM ref.jurusan_sp WHERE jurusan_sp_id='$id' AND sekolah_id='$schoolId'");
		if($dt->num_rows()==0)
		{
			$dataInsert = array(
	          'jurusan_sp_id' =>trim($key['jurusan_sp_id']),
	          'sekolah_id' =>trim($key['sekolah_id']),
	          'kebutuhan_khusus_id' =>trim($key['kebutuhan_khusus_id']),
	          'jurusan_id'=>trim($key['jurusan_id']),
	          'nama_jurusan_sp'=>trim($key['nama_jurusan_sp'])
			);
			$this->db1->insert('ref.jurusan_sp',$dataInsert);
		}
		unset($dataInsert);
		$dt->free_result();
	}

/* 2. insert data to registrasi peserta didik */
	$dt = $this->db1->query("SELECT * FROM ref.registrasi_peserta_didik WHERE sekolah_id='$schoolId'");
	if($dt->num_rows()>0)
	{
		$this->db1->where('sekolah_id',$schoolId);
		$this->db1->delete('ref.registrasi_peserta_didik');
	}
	$dt->free_result();

	foreach ($dumpRegPst as $key) 
	{
		$id = trim($key['registrasi_id']);
		$dt = $this->db1->query("SELECT * FROM ref.registrasi_peserta_didik WHERE registrasi_id='$id' AND sekolah_id='$schoolId'");
		if($dt->num_rows()==0)
		{
		  	  if(trim($key['jenis_keluar_id'])=="")
	          {
	          	$jk = NULL;
	          }
	          else
	          {
	          	$jk = trim($key['jenis_keluar_id']);
	          }
	          if(trim($key['tanggal_masuk_sekolah'])=="")
	          {
	          	$tm = NULL;
	          }
	          else
	          {
	          	$tm = trim($key['tanggal_masuk_sekolah']);
	          }
	          if(trim($key['tanggal_keluar'])=="")
	          {
	          	$tk = NULL;
	          }
	          else
	          {
	          	$tk = trim($key['tanggal_keluar']);
	          }
	          if(trim($key['jurusan_sp_id'])=="")
	          {
	          	$jp = NULL;
	          }
	          else
	          {
	          	$jp = trim($key['jurusan_sp_id']);
	          }
	          $dataInsert = array(
	          'registrasi_id' =>trim($key['registrasi_id']),
	          'jurusan_sp_id' =>$jp,
	          'peserta_didik_id' =>trim($key['peserta_didik_id']),
	          'sekolah_id' =>trim($key['sekolah_id']),
	          'jenis_pendaftaran_id' =>trim($key['jenis_pendaftaran_id']),
	          'nipd' =>trim($key['nipd']),
	          'tanggal_masuk_sekolah' =>$tm,
	          'jenis_keluar_id' =>$jk,
	          'tanggal_keluar' =>$tk,
	          'keterangan'=>trim($key['keterangan'])
	        );
	      	$this->db1->insert('ref.registrasi_peserta_didik',$dataInsert);
	  	}
	  	unset($dataInsert);
		$dt->free_result();
  	}

/* 3. insert to peserta didik */
	$dt = $this->db1->query("SELECT * FROM ref.peserta_didik WHERE sekolah_id='$schoolId'");
	if($dt->num_rows()>0)
	{
		$this->db1->where('sekolah_id',$schoolId);
		$this->db1->delete('ref.peserta_didik');
	}
	$dt->free_result();

	foreach ($lengkap as $key) 
	{
		$id = trim($key['peserta_didik_id']);
		$dt = $this->db1->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$id' AND sekolah_id='$schoolId'");
		if($dt->num_rows()==0)
		{
	       $dataInsert = array(
	              'peserta_didik_id' =>trim($key['peserta_didik_id']),
	              'nama' =>trim($key['nama']),
	              'jenis_kelamin' =>trim($key['jenis_kelamin']),
	              'nisn' =>trim($key['nisn']),
	              'nik' =>trim($key['nik']),
	              'tempat_lahir' =>trim($key['tempat_lahir']),
	              'tanggal_lahir' =>trim($key['tanggal_lahir']),
	              'agama_id' =>trim($key['agama_id']),
	              'kebutuhan_khusus_id' =>trim($key['kebutuhan_khusus_id']),
	              'alamat_jalan' =>trim($key['alamat_jalan']),
	              'rt' =>trim($key['rt']),
	              'rw' =>trim($key['rw']),
	              'nama_dusun' =>trim($key['nama_dusun']),
	              'desa_kelurahan' =>trim($key['desa_kelurahan']),
	              'kode_wilayah' =>trim($key['kode_wilayah']),
	              'kode_pos' =>trim($key['kode_pos']),
	              'nomor_telepon_rumah' =>trim($key['nomor_telepon_rumah']),
	              'nomor_telepon_seluler' =>trim($key['nomor_telepon_seluler']),
	              'email' =>trim($key['email']),
	              'nik_ayah' =>trim($key['nik_ayah']),
	              'nik_ibu' =>trim($key['nik_ibu']),
	              'penerima_KPS' =>trim($key['penerima_KPS']),
	              'no_KPS' =>trim($key['no_KPS']),
	              'layak_PIP' =>trim($key['layak_PIP']),
	              'penerima_KIP' =>trim($key['penerima_KIP']),
	              'no_KIP'=>trim($key['no_KIP']),
	              'nm_KIP' =>trim($key['nm_KIP']),
	              'no_KKS' =>trim($key['no_KKS']),
	              'id_layak_pip' =>trim($key['id_layak_pip']),
	              'nama_ayah' =>trim($key['nama_ayah']),
	              'tahun_lahir_ayah' =>trim($key['tahun_lahir_ayah']),
	              'jenjang_pendidikan_ayah' =>trim($key['jenjang_pendidikan_ayah']),
	              'pekerjaan_id_ayah' =>trim($key['pekerjaan_id_ayah']),
	              'penghasilan_id_ayah' =>trim($key['penghasilan_id_ayah']),
	              'nama_ibu_kandung' =>trim($key['nama_ibu_kandung']),
	              'tahun_lahir_ibu' =>trim($key['tahun_lahir_ibu']),
	              'jenjang_pendidikan_ibu' =>trim($key['jenjang_pendidikan_ibu']),
	              'penghasilan_id_ibu' =>trim($key['penghasilan_id_ibu']),
	              'pekerjaan_id_ibu' =>trim($key['pekerjaan_id_ibu']),
	              'sekolah_id'=>trim($key['sekolah_id']),
	              'jenis_keluar_id'=>trim($key['jenis_keluar_id']),
	              'tanggal_masuk_sekolah'=>trim($key['tanggal_masuk_sekolah']),
	              'tanggal_keluar'=>trim($key['tanggal_keluar']),
	              'jurusan_id'=>trim($key['jurusan_id'])
	            );
			$this->db1->insert('ref.peserta_didik',$dataInsert);
	  	}
	  	unset($dataInsert);
		$dt->free_result();
  	}

/* 4. insert ke anggota rombel */
	foreach ($dumpAromBel as $key) {
		$id = trim($key['anggota_rombel_id']);
		$dt = $this->db1->query("SELECT * FROM ref.anggota_rombel WHERE anggota_rombel_id='$id'");
		if($dt->num_rows()==0)
		{
			$dataInsert = array(
              'anggota_rombel_id'=>trim($key['anggota_rombel_id']),
              'rombongan_belajar_id'=>trim($key['rombongan_belajar_id']),
              'peserta_didik_id'=>trim($key['peserta_didik_id']),
              'jenis_pendaftaran_id'=>trim($key['jenis_pendaftaran_id'])
            );
			$this->db1->insert('ref.anggota_rombel',$dataInsert);
		}
		else
		{
			$this->db1->where('anggota_rombel_id',$id);
			$this->db1->delete('ref.anggota_rombel');
			$dataInsert = array(
              'anggota_rombel_id'=>trim($key['anggota_rombel_id']),
              'rombongan_belajar_id'=>trim($key['rombongan_belajar_id']),
              'peserta_didik_id'=>trim($key['peserta_didik_id']),
              'jenis_pendaftaran_id'=>trim($key['jenis_pendaftaran_id'])
            );
			$this->db1->insert('ref.anggota_rombel',$dataInsert);

		}
		unset($dataInsert);
		$dt->free_result();
	}

/* 5. insert ke rombongan belajar */
	$dt = $this->db1->query("SELECT * FROM ref.rombongan_belajar WHERE sekolah_id='$schoolId'");
	if($dt->num_rows()>0)
	{
		$this->db1->where('sekolah_id',$schoolId);
		$this->db1->delete('ref.rombongan_belajar');
	}
	$dt->free_result();

	foreach ($dumpRomBel as $key) {
		$id = trim($key['rombongan_belajar_id']);
		$dt = $this->db1->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$id' AND sekolah_id='$schoolId'");
		if($dt->num_rows()==0)
		{
			if(trim($key['jurusan_sp_id'])=="")
			{
				$jip  = NULL;
			}
			else
			{
				$jip = trim($key['jurusan_sp_id']);
			}
			$dataInsert = array(
	              'rombongan_belajar_id' =>trim($key['rombongan_belajar_id']),
	              'semester_id' =>trim($key['semester_id']),
	              'sekolah_id' =>trim($key['sekolah_id']),
	              'tingkat_pendidikan_id' =>trim($key['tingkat_pendidikan_id']),
	              'jurusan_sp_id' =>trim($key['jurusan_sp_id']),
	              'nama' =>trim($key['nama']),
	              'kebutuhan_khusus_id' =>trim($key['kebutuhan_khusus_id'])
	            );
			$this->db1->insert('ref.rombongan_belajar',$dataInsert);
		}
		unset($dataInsert);
		$dt->free_result();
	}

/* 6. insert ke user sekolah */
		$cksekolah = $this->db->get_where('ref.peserta_didik',array('sekolah_id'=>$schoolId))->result();
		if(count($cksekolah)>0)
		{
			foreach ($cksekolah as $row) {
				$x = date("d-m-Y",strtotime($row->tanggal_lahir));
				$tgl = explode("-", $x);
				$a = $tgl[0];
				$b = $tgl[1];
				$c = $tgl[2];
				$thn = substr($c, 2);
				$pass = $this->bcrypt->hash_password($a.$b.$thn);
				$id = trim($row->peserta_didik_id);
				$s_username = array('user_id'=>trim($row->peserta_didik_id),
					'username'=>$row->nisn,
					'password'=>$pass,
					'level'=>'3',
					'login'=>'1');
				$qry = $this->db1->query("SELECT * FROM app.username WHERE user_id='$id'");
				if($qry->num_rows()==0)
				{
				$this->db1->insert('app.username',$s_username);
				}
			}
		}

		$nama = "Admin BKK";
		$nip = "20171717";
		$email = "email@adminbkk.com";
		$hp = "0888888888888";
		$uname = $npsn;
		$pass = $this->bcrypt->hash_password('admin');
		$dat = array(
			'sekolah_id' => $schoolId,
			'nip' => $nip,
			'nama' => $nama,
			'email' =>$email,
			'tlp'=>$hp
			);
		$this->db1->insert('app.user_sekolah',$dat);

		$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
		$qry = $this->db1->query($q)->result();
		foreach ($qry as $key) {
			$user_id = $key->user_id;
		}

		$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
			);

		$qry = $this->db1->query("SELECT * FROM app.username WHERE user_id='$user_id'");
		if($qry->num_rows()==0)
		{
		$this->db1->insert('app.username',$dd);
		}

		$tgla = date('Y-m-d');
		$dataInsert=array(
				'sekolah_id'=>$schoolId,
	            'tgl_aktivasi'=>$tgla,
	            'logo' => 'no.jpg',
	            'no_izin_bkk'=>"",
	            'approved_by'=>"be988f3c-f18a-46a9-a8d4-46726b00d57d"
			);
		$this->db1->insert('sekolah_terdaftar',$dataInsert);
// -------------------------------------------------------------//        
}







/* Inner Join Array */
	function inner_join(array $left, array $right, $on) 
	{
	 	$out = array();
	  	foreach ($left as $left_record) 
	  	{
	    	foreach ($right as $right_record) 
	    	{
	      		if ($left_record[$on] == $right_record[$on]) 
	      		{
	        		$out[] = array_merge($left_record, $right_record);
	      		}
	    	}
	  	}
	  	return $out;
	}

}

