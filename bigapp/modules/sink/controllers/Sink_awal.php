<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sink extends CI_Controller {
	public function __construct(){
  		parent::__construct();
		$this->db1 = $this->load->database('default', TRUE);
		$this->load->model('reg_bkk/reg_sekolah_model');
		date_default_timezone_set('Asia/Jakarta');
 	}

 	function tambah($schoolId)
	{
		$npsn="";
		$tgl = date("Y-m-d H:i:s");
		$query = $this->reg_sekolah_model->get_sekolah($schoolId);
		foreach ($query as $row) {
			$data = array(
				'sekolah_id'=>$row->sekolah_id,
	            'tgl_aktivasi'=>$tgl,
	            'logo' => 'no.jpg',
	            'no_izin_bkk'=>"",
	            'approved_by'=>$this->session->userdata('user_id')
            );	
			$npsn=$row->npsn;
		}

/* Masukan ke sekolah terdaftar */
		$qry = $this->db1->query("SELECT sekolah_id FROM sekolah_terdaftar WHERE sekolah_id='$schoolId'");
		if($qry->num_rows()==0){
			$this->db1->insert('sekolah_terdaftar',$data);
		}

/* Jurusan SP */
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=psnasuruj&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$id = trim(pg_escape_string($dataS[$a]['jurusan_sp_id']));
			$dataInsert = array(
              'jurusan_sp_id' =>trim(pg_escape_string($dataS[$a]['jurusan_sp_id'])),
              'sekolah_id' =>trim(pg_escape_string($dataS[$a]['sekolah_id'])),
              'kebutuhan_khusus_id' =>trim(pg_escape_string($dataS[$a]['kebutuhan_khusus_id'])),
              'jurusan_id'=>trim(pg_escape_string($dataS[$a]['jurusan_id'])),
              'nama_jurusan_sp'=>trim(pg_escape_string($dataS[$a]['nama_jurusan_sp']))
            );
			$qry = $this->db1->query("SELECT * FROM ref.jurusan_sp WHERE jurusan_sp_id='$id'");
			if($qry->num_rows()==0)
            {
            	$this->db1->insert('ref.jurusan_sp',$dataInsert);
            }
		}


		$now = date('Y-m-d');
		$tas = "2017-06-30";
		if($now<$tas)
		{
			$n = "2";
		}
		else
		{
			$n = "1";
		}
		$tahun = date('Y-m-d')-1 . $n;

/* Insert ke Rombongan belajar */
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmor&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		$dromb = array();
		for($a=0;$a<count($dataS);$a++)
		{
			if(trim(pg_escape_string($dataS[$a]['semester_id']))==$tahun && trim(pg_escape_string($dataS[$a]['tingkat_pendidikan_id']))=='12')
			{
				$dromb[] = trim(pg_escape_string($dataS[$a]['rombongan_belajar_id']));	
	 			if(trim(pg_escape_string($dataS[$a]['jurusan_sp_id']))=="")
				{
					$js = NULL;
				}
				else
				{
					$js = trim(pg_escape_string($dataS[$a]['jurusan_sp_id']));
				}
				$id = trim(pg_escape_string($dataS[$a]['rombongan_belajar_id']));
	 			$dataInsert = array(
	              'rombongan_belajar_id' =>trim(pg_escape_string($dataS[$a]['rombongan_belajar_id'])),
	              'semester_id' =>trim(pg_escape_string($dataS[$a]['semester_id'])),
	              'sekolah_id' =>trim(pg_escape_string($dataS[$a]['sekolah_id'])),
	              'tingkat_pendidikan_id' =>trim(pg_escape_string($dataS[$a]['tingkat_pendidikan_id'])),
	              'jurusan_sp_id' =>$js,
	              'nama' =>trim(pg_escape_string($dataS[$a]['nama'])),
	              'kebutuhan_khusus_id' =>trim(pg_escape_string($dataS[$a]['kebutuhan_khusus_id']))
	            	);
	 			$qry = $this->db1->query("SELECT * FROM ref.rombongan_belajar WHERE rombongan_belajar_id='$id'");
	 			if($qry->num_rows()==0){
	          		$this->db1->insert('ref.rombongan_belajar',$dataInsert);
	 			}
	 		}
		}

/* insert ke anggota rombel */
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=lebmora&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			$rid = trim(pg_escape_string($dataS[$a]['rombongan_belajar_id']));
			for($b=0;$b<count($dromb);$b++)
			{
				if($rid==$dromb[$b])
				{
		 			$id = trim(pg_escape_string($dataS[$a]['anggota_rombel_id']));
		 			$dataInsert = array(
		              'anggota_rombel_id'=>trim(pg_escape_string($dataS[$a]['anggota_rombel_id'])),
		              'rombongan_belajar_id'=>trim(pg_escape_string($dataS[$a]['rombongan_belajar_id'])),
		              'peserta_didik_id'=>trim(pg_escape_string($dataS[$a]['peserta_didik_id'])),
		              'jenis_pendaftaran_id'=>trim(pg_escape_string($dataS[$a]['jenis_pendaftaran_id']))
		            );
		            $qry = $this->db1->query("SELECT * FROM ref.anggota_rombel WHERE anggota_rombel_id='$id'");
		            if($qry->num_rows()==0)
		            {
		            	$this->db1->insert('ref.anggota_rombel',$dataInsert);
		            }
			    }
			}
		}

/* insert ke registrasi peserta didik */
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=lasjurin&upa=e_XcS6vAN*A8ucNe&utab=atresepger&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{
			if(trim(pg_escape_string($dataS[$a]['keterangan']))=="")
			{
				$ket = NULL;
			}
			else
			{
				$ket = trim(pg_escape_string($dataS[$a]['keterangan']));
			}

			if(trim(pg_escape_string($dataS[$a]['tanggal_masuk_sekolah']))=="")
			{
				$tma = NULL;
			}
			else
			{
				$tma = trim(pg_escape_string($dataS[$a]['tanggal_masuk_sekolah']));
			}

			if(trim(pg_escape_string($dataS[$a]['tanggal_keluar']))=="")
			{
				$tka = NULL;
			}
			else
			{
				$tka = trim(pg_escape_string($dataS[$a]['tanggal_keluar']));
			}

			if(trim(pg_escape_string($dataS[$a]['jurusan_sp_id']))=="")
			{
				$js = NULL;
			}
			else
			{
				$js = trim(pg_escape_string($dataS[$a]['jurusan_sp_id']));
			}

			if(trim(pg_escape_string($dataS[$a]['jenis_keluar_id']))=="")
			{
				$jk = NULL;
			}
			else
			{
				$jk = trim(pg_escape_string($dataS[$a]['jenis_keluar_id']));
			}

		//	$jk = trim(pg_escape_string($dataS[$a]['jenis_keluar_id']));
			$tk = trim(pg_escape_string($dataS[$a]['tanggal_keluar']));	
			$tk = substr($tk, 6,4) . "-" . substr($tk, 0,3) . substr($tk, 3,2);
			$tm = trim(pg_escape_string($dataS[$a]['tanggal_masuk_sekolah']));	
			$tm = substr($tm, 6,4) . "-" . substr($tm, 0,3) . substr($tm, 3,2);
			if($jk=="1" && $tk>='2015-05-01' || $jk=="" && $tm > '2014-01-01' && $tm < '2015-05-01')
			{
				$id = trim(pg_escape_string($dataS[$a]['registrasi_id']));
				$dataInsert = array(
	              'registrasi_id' =>trim(pg_escape_string($dataS[$a]['registrasi_id'])),
	              'jurusan_sp_id' =>$js,
	              'peserta_didik_id' =>trim(pg_escape_string($dataS[$a]['peserta_didik_id'])),
	              'sekolah_id' =>trim(pg_escape_string($dataS[$a]['sekolah_id'])),
	              'jenis_pendaftaran_id' =>trim(pg_escape_string($dataS[$a]['jenis_pendaftaran_id'])),
	              'nipd' =>trim(pg_escape_string($dataS[$a]['nipd'])),
	              'tanggal_masuk_sekolah' =>$tma,
	              'jenis_keluar_id' =>$jk,
	              'tanggal_keluar' =>$tka,
	              'keterangan'=>$ket
	            );
				$qry = $this->db1->query("SELECT * FROM ref.registrasi_peserta_didik WHERE registrasi_id='$id'");
				if($qry->num_rows()==0)
				{
		         	$this->db1->insert('ref.registrasi_peserta_didik',$dataInsert);
				}
			}
		}

/* insert ke peserta dididk */
		$response = file_get_contents("http://202.92.203.37/dapodik/webapi.php?uid=evaluasi&upa=emonev&utab=siswa&sekolah_id=".$schoolId);
		$dataS = json_decode($response,true);
		for($a=0;$a<count($dataS);$a++)
		{		 	
			$pid = trim(pg_escape_string($dataS[$a]['peserta_didik_id']));
			$dataInsert = array(
              'peserta_didik_id' =>trim(pg_escape_string($dataS[$a]['peserta_didik_id'])),
              'nama' =>trim(pg_escape_string($dataS[$a]['nama'])),
              'jenis_kelamin' =>trim(pg_escape_string($dataS[$a]['jenis_kelamin'])),
              'nisn' =>trim(pg_escape_string($dataS[$a]['nisn'])),
              'nik' =>trim(pg_escape_string($dataS[$a]['nik'])),
              'tempat_lahir' =>trim(pg_escape_string($dataS[$a]['tempat_lahir'])),
              'tanggal_lahir' =>trim(pg_escape_string($dataS[$a]['tanggal_lahir'])),
              'agama_id' =>intval(trim(pg_escape_string($dataS[$a]['agama_id']))),
              'kebutuhan_khusus_id' =>intval(trim(pg_escape_string($dataS[$a]['kebutuhan_khusus_id']))),
              'alamat_jalan' =>trim(pg_escape_string($dataS[$a]['alamat_jalan'])),
              'rt' =>intval(trim(pg_escape_string($dataS[$a]['rt']))),
              'rw' =>intval(trim(pg_escape_string($dataS[$a]['rw']))),
              'nama_dusun' =>trim(pg_escape_string($dataS[$a]['nama_dusun'])),
              'desa_kelurahan' =>trim(pg_escape_string($dataS[$a]['desa_kelurahan'])),
              'kode_wilayah' =>trim(pg_escape_string($dataS[$a]['kode_wilayah'])),
              'kode_pos' =>trim(pg_escape_string($dataS[$a]['kode_pos'])),
              'nomor_telepon_rumah' =>trim(pg_escape_string($dataS[$a]['nomor_telepon_rumah'])),
              'nomor_telepon_seluler' =>trim(pg_escape_string($dataS[$a]['nomor_telepon_seluler'])),
              'email' =>trim(pg_escape_string($dataS[$a]['email'])),
              'nik_ayah' =>trim(pg_escape_string($dataS[$a]['nik_ayah'])),
              'nik_ibu' =>trim(pg_escape_string($dataS[$a]['nik_ibu'])),
              'penerima_KPS' =>intval(trim(pg_escape_string($dataS[$a]['penerima_KPS']))),
              'no_KPS' =>trim(pg_escape_string($dataS[$a]['no_KPS'])),
              'layak_PIP' =>intval(trim(pg_escape_string($dataS[$a]['layak_PIP']))),
              'penerima_KIP' =>intval(trim(pg_escape_string($dataS[$a]['penerima_KIP']))),
              'no_KIP'=>trim(pg_escape_string($dataS[$a]['no_KIP'])),
              'nm_KIP' =>trim(pg_escape_string($dataS[$a]['nm_KIP'])),
              'no_KKS' =>trim(pg_escape_string($dataS[$a]['no_KKS'])),
              'id_layak_pip' =>intval(trim(pg_escape_string($dataS[$a]['id_layak_pip']))),
              'nama_ayah' =>trim(pg_escape_string($dataS[$a]['nama_ayah'])),
              'tahun_lahir_ayah' =>trim(pg_escape_string($dataS[$a]['tahun_lahir_ayah'])),
              'jenjang_pendidikan_ayah' =>intval(trim(pg_escape_string($dataS[$a]['jenjang_pendidikan_ayah']))),
              'pekerjaan_id_ayah' =>intval(trim(pg_escape_string($dataS[$a]['pekerjaan_id_ayah']))),
              'penghasilan_id_ayah' =>intval(trim(pg_escape_string($dataS[$a]['penghasilan_id_ayah']))),
              'nama_ibu_kandung' =>intval(trim(pg_escape_string($dataS[$a]['nama_ibu_kandung']))),
              'tahun_lahir_ibu' =>trim(pg_escape_string($dataS[$a]['tahun_lahir_ibu'])),
              'jenjang_pendidikan_ibu' =>intval(trim(pg_escape_string($dataS[$a]['jenjang_pendidikan_ibu']))),
              'penghasilan_id_ibu' =>intval(trim(pg_escape_string($dataS[$a]['penghasilan_id_ibu']))),
              'pekerjaan_id_ibu' =>intval(trim(pg_escape_string($dataS[$a]['pekerjaan_id_ibu'])))
            );
			$qry =  $this->db1->query("SELECT * FROM ref.peserta_didik WHERE peserta_didik_id='$pid'");
			if($qry->num_rows()==0)
			{
           		$this->db1->insert('ref.peserta_didik',$dataInsert);
		 	}				
		}

/* insert ke user sekolah */
		$cksekolah = $this->db->get_where('ref.view_peserta',array('sekolah_id'=>$schoolId))->result();
		if(count($cksekolah)>0)
		{
			foreach ($cksekolah as $row) {
				$x = date("d-m-Y",strtotime($row->tanggal_lahir));
				$tgl = explode("-", $x);
				$a = $tgl[0];
				$b = $tgl[1];
				$c = $tgl[2];
				$thn = substr($c, 2);
				$pass = $this->bcrypt->hash_password($a.$b.$thn);
				$id = trim($row->peserta_didik_id);
				$s_username = array('user_id'=>trim($row->peserta_didik_id),
					'username'=>$row->nisn,
					'password'=>$pass,
					'level'=>'3',
					'login'=>'1');
				$qry = $this->db1->query("SELECT * FROM app.username WHERE user_id='$id'");
				if($qry->num_rows()==0)
				{
				$this->db1->insert('app.username',$s_username);
				}
			}
		}

		$nama = "Admin BKK";
		$nip = "20171717";
		$email = "email@adminbkk.com";
		$hp = "0888888888888";
		$uname = $npsn;
		$pass = $this->bcrypt->hash_password('admin');
		$dat = array(
			'sekolah_id' => $schoolId,
			'nip' => $nip,
			'nama' => $nama,
			'email' =>$email,
			'tlp'=>$hp
			);
		$this->db1->insert('app.user_sekolah',$dat);

		$q = "SELECT * FROM app.user_sekolah ORDER BY id DESC LIMIT 1";
		$qry = $this->db1->query($q)->result();
		foreach ($qry as $key) {
			$user_id = $key->user_id;
		}

		$dd = array('user_id' => $user_id ,
					'username'=> $uname,
					'password'=>$pass,
					'level' => '1',
					'login' => '1' 
			);

		$qry = $this->db1->query("SELECT * FROM app.username WHERE user_id='$user_id'");
		if($qry->num_rows()==0)
		{
		$this->db1->insert('app.username',$dd);
		}
////
	}
 	

}

