<?php
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_HEADER => false,
  CURLOPT_URL => "https://smk:smkxtkid@www.topkarir.com/api/jobs/kerjasmk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "X-API-KEY: tkidxsmk",
    "Accept: application/json"
  ),
));
$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

$hasil = json_decode($response,true);

foreach ($hasil as $key => $value) {
 // echo "<a href='https://www.topkarir.com/lowongan/detil/" . $value['permalink'] . "' target='_blank'>" . $value['company_name'] . "</a><br/>";
	$data[]  =   array('perusahaan'=>$value['company_name'],'link'=>$value['permalink'],'judul'=>$value['job_title'],'tgl_akhir'=>$value['end']);
}

echo json_encode($data);

?>

